﻿#if _IJCAD_
using GrxCAD.Runtime;
#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
#endif
using JrxCad.Utility;
using JrxCadTest.Utility;
using System.Collections.Generic;
using System.Reflection;

namespace JrxCadTest.Command.WBlockTest
{
    public class Test : BaseTest
    {

        private string _classFullName;
        private const string EXCEPTION_INFO_STRING = "Method Name: {0}, Exception: {1} ";

        /// <summary>
        /// Start test
        /// </summary>
        public override void OnStart()
        {
            var methodInfo = MethodBase.GetCurrentMethod();
            _classFullName = methodInfo.DeclaringType?.FullName;
            Util.Editor().WriteMessage($"\n{_classFullName?.ToUpper()} START!");
        }

        /// <summary>
        /// Finish test
        /// </summary>
        public override void OnFinish()
        {
            Util.Editor().WriteMessage($"\n{_classFullName.ToUpper()} Finish!");
        }

        /// <summary>
        ///  Write error message
        /// </summary>
        private void WriteErrorMessage(string methodName)
        {
            Util.Editor().WriteMessage($"\n{methodName} ERROR!!!!");
        }

        #region Open D1 (Dialog)

        /// <summary>
        /// Remove Locked Objects => DM1 (Dialog Message)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest01", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest01", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest01", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest01()
        {
            try
            {
                OnStart();

                var wBlockCmdTest = new WBlockCmdTest();
                var blockNames = new List<string>();
                blockNames.Add("AMB034");

                var objectIds = wBlockCmdTest.GetSelectionSetByXref(blockNames);
                if (objectIds.Length == 0)
                {
                    Util.Editor().WriteMessage("\n Please open WBLOCK01.dwg to execute this test case");
                    OnFinish();
                    return;
                }
                wBlockCmdTest.ExecuteWBlockTest01(objectIds);
                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }
        #endregion

        #region valid file path

        /// <summary>
        /// Path valid => select object exist, file = exist => DM5 (validate Block)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest09", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest09", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest09", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest09()
        {
            try
            {
                OnStart();

                var wBlockCmdTest = new WBlockCmdTest();
                var filePath = @"C:\ProgramData\new block.dwg";

                var objectIds = wBlockCmdTest.CreateObjectId();
                wBlockCmdTest.ExecuteWBlockTest09(objectIds, filePath);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }


        /// <summary>
        /// Path NG => DM3 (未入力)
        /// </summary>

#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest10", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest10", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest10", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest10()
        {
            try
            {
                OnStart();

                var wBlockCmdTest = new WBlockCmdTest();

                var filePath = string.Empty;
                wBlockCmdTest.ExecuteWBlockTest10(filePath);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }


        /// <summary>
        /// Path NG => DM3 (C:\Test\<a>.dwg)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest11", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest11", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest11", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest11()
        {
            try
            {
                OnStart();

                var wBlockCmdTest = new WBlockCmdTest();
                var filePath = @"C:\Test\<a>.dwg";
                wBlockCmdTest.ExecuteWBlockTest10(filePath);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// Path NG => DM3 (.\Test\a.dwg)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest12", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest12", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest12", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest12()
        {
            try
            {
                OnStart();

                var wBlockCmdTest = new WBlockCmdTest();
                var filePath = @".\Test\a.dwg";
                wBlockCmdTest.ExecuteWBlockTest10(filePath);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// Path NG => DM3 (存在しないディレクトリ)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest13", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest13", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest13", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest13()
        {
            try
            {
                OnStart();

                var wBlockCmdTest = new WBlockCmdTest();
                var filePath = @"C:\Test";
                wBlockCmdTest.ExecuteWBlockTest10(filePath);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// Path NG => DM3 (C:\Program Files\a.dwg)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest14", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest14", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest14", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest14()
        {
            try
            {
                OnStart();

                var wBlockCmdTest = new WBlockCmdTest();
                var filePath = @"C:\Program Files\a.dwg";
                wBlockCmdTest.ExecuteWBlockTest10(filePath);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }


        /// <summary>
        /// Path NG => DM3 (259以上の文字数)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest15", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest15", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest15", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest15()
        {
            try
            {
                OnStart();

                var wBlockCmdTest = new WBlockCmdTest();
                var filePath = @"C:\WBlockTest\new blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew blocknew block.dwg";
                wBlockCmdTest.ExecuteWBlockTest10(filePath);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// Path NG => DM3 (拡張子なし　C:¥Work\a)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest16", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest16", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest16", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest16()
        {
            try
            {
                OnStart();

                var wBlockCmdTest = new WBlockCmdTest();
                var filePath = @"C:\Work\a";
                wBlockCmdTest.ExecuteWBlockTest10(filePath);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }


        /// <summary>
        /// Path NG => DM3 (ファイル名のみ　a.dwg)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest17", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest17", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest17", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest17()
        {
            try
            {
                OnStart();

                var wBlockCmdTest = new WBlockCmdTest();
                var filePath = "a.dwg";
                wBlockCmdTest.ExecuteWBlockTest10(filePath);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Path Validate ⇒	Success	⇒Selected Objects = Exist	⇒	File=None	⇒	to	Output
        /// </summary>
#if DEBUG
        //For debug in IJCAD, set command flag to CommandFlags.Session instead of CommandFlags.Modal to prevent crash.
        //Refer to: https://tknmt.hatenablog.com/entry/2019/05/29/110659
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest18", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest18", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest18", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest18()
        {
            try
            {
                OnStart();
                var wBlockCmdTest = new WBlockCmdTest();
                var filePath = @"C:\ProgramData\new block.dwg";

                var objectIds = wBlockCmdTest.CreateObjectId();
                wBlockCmdTest.ExecuteWBlockTest18(objectIds, filePath);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// Path Validate ⇒	Success	⇒	Selected Objects = None	⇒	to	DM4
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest19", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest19", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest19", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif

        public void WBlockTest19()
        {
            try
            {
                OnStart();

                var wBlockCmdTest = new WBlockCmdTest();
                var filePath = @"C:\ProgramData\new block.dwg";
                wBlockCmdTest.ExecuteWBlockTest19(filePath);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// PICKFIRST = 0, Start => to D1
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest20", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest20", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest20", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest20()
        {
            try
            {
                OnStart();
                var wBlockCmdTest = new WBlockCmdTest();
                wBlockCmdTest.ExecuteWBlockTest20();

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }


        /// <summary>
        /// PICKFIRST = 0, Select Object => Start to D1
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest21", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "WBlockTest21", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "WBlockTest21", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void WBlockTest21()
        {
            try
            {
                OnStart();
                var wBlockCmdTest = new WBlockCmdTest();
                var filePath = @"C:\ProgramData\new block.dwg";

                var objectIds = wBlockCmdTest.CreateObjectId();
                wBlockCmdTest.ExecuteWBlockTest21(objectIds, filePath);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }
    }
}
