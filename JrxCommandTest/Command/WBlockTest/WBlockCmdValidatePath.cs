﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
#endif
using JrxCad.Command.WBlock;
using JrxCadTest.Utility;
using Moq;
using static JrxCad.Command.WBlock.WBlockCmd;

namespace JrxCadTest.Command.WBlockTest
{
    public partial class WBlockCmdTest
    {

        /// <summary>
        /// validate file path is invalid
        /// </summary>
        private void ValidFilePathInput(string filePath)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var wblockUserInputMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup active command (WBlock Command)
            wblockUserInputMock.Setup(a => a.GetActiveCommand()).Returns(1);
            // setup pickfirst
            wblockUserInputMock.Setup(a => a.GetPickFirst()).Returns(0);
            // Setup execute unit test
            wblockUserInputMock.Setup(a => a.IsExecuteUnitTest()).Returns(true);

            // setup file path
            wblockUserInputMock.Setup(a => a.SetFilePath()).Returns(filePath);

            // setup object active
            wblockUserInputMock.Setup(a => a.IsObjectActive()).Returns(false);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlock = new WBlockCmd(wblockUserInputMock.Object);
            wBlock.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());

#endif

            ////////////////////////////////////////
            // Test Completed
            ////////////////////////////////////////
            wblockUserInputMock.VerifyAll();
        }

        /// <summary>
        ///  Path valid => select object exist, file = exist => DM5 
        /// </summary>
        public void ExecuteWBlockTest09(ObjectId[] objectIds, string filePath)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var wblockUserInputMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup active command (WBlock Command)
            wblockUserInputMock.Setup(a => a.GetActiveCommand()).Returns(1);

            // Setup PICKFIRST
            wblockUserInputMock.Setup(a => a.GetPickFirst()).Returns(1);
            // Get Selection Set
            var selectionSet = SelectionSet.FromObjectIds(objectIds);
            // Setup PromptSelectionResult
            wblockUserInputMock.Setup(a => a.GetPromptSelectionResult()).Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            // setup object locked
            wblockUserInputMock.Setup(a => a.EntityOnLockedLayer(It.IsAny<Transaction>(), It.IsAny<Entity>())).Returns(false);

            // Setup execute unit test
            wblockUserInputMock.Setup(a => a.IsExecuteUnitTest()).Returns(true);

            // setup file path
            wblockUserInputMock.Setup(a => a.SetFilePath()).Returns(filePath);

            // setup active object
            wblockUserInputMock.Setup(a => a.IsObjectActive()).Returns(true);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlock = new WBlockCmd(wblockUserInputMock.Object);
            wBlock.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test

#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());

#endif

            ////////////////////////////////////////
            // Test Completed
            ////////////////////////////////////////
            wblockUserInputMock.VerifyAll();
        }

        /// <summary>
        /// Path Valid => NG
        /// </summary>
        public void ExecuteWBlockTest10(string filePath)
        {
            ValidFilePathInput(filePath);
        }


        /// <summary>
        /// Path Validate ⇒	Success	⇒Selected Objects = Exist	⇒	File=None	⇒	to	Output
        /// </summary>
        public void ExecuteWBlockTest18(ObjectId[] objectIds, string filePath)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var wBlockUserInputMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup active command (WBlock Command)
            wBlockUserInputMock.Setup(a => a.GetActiveCommand()).Returns(1);

            // Setup PICKFIRST
            wBlockUserInputMock.Setup(a => a.GetPickFirst()).Returns(1);

            // Get Selection Set
            var selectionSet = SelectionSet.FromObjectIds(objectIds);
            // Setup PromptSelectionResult
            wBlockUserInputMock.Setup(a => a.GetPromptSelectionResult()).Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            // Setup unlocked
            wBlockUserInputMock.Setup(a => a.EntityOnLockedLayer(It.IsAny<Transaction>(), It.IsAny<Entity>())).Returns(false);

            // Setup execute unit test
            wBlockUserInputMock.Setup(a => a.IsExecuteUnitTest()).Returns(true);

            // Setup file path
            wBlockUserInputMock.Setup(a => a.SetFilePath()).Returns(filePath);

            // setup object active
            wBlockUserInputMock.Setup(a => a.IsObjectActive()).Returns(true);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlock = new WBlockCmd(wBlockUserInputMock.Object);
            wBlock.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test

#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());

#endif

            ////////////////////////////////////////
            // Test Completed
            ////////////////////////////////////////
            wBlockUserInputMock.VerifyAll();

        }

        /// <summary>
        ///  Path Validate ⇒	Success	⇒	Selected Objects = None	⇒	to	DM4
        /// </summary>
        public void ExecuteWBlockTest19(string filePath)
        {

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var wBlockUserInputMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup active command (WBlock Command)
            wBlockUserInputMock.Setup(a => a.GetActiveCommand()).Returns(1);
            // setup pickfirst
            wBlockUserInputMock.Setup(a => a.GetPickFirst()).Returns(0);
            // Setup execute unit test
            wBlockUserInputMock.Setup(a => a.IsExecuteUnitTest()).Returns(true);
            // Setup set file path
            wBlockUserInputMock.Setup(a => a.SetFilePath()).Returns(filePath);
            // Setup object active
            wBlockUserInputMock.Setup(a => a.IsObjectActive()).Returns(true);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlock = new WBlockCmd(wBlockUserInputMock.Object);
            wBlock.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());

#endif

            ////////////////////////////////////////
            // Test Completed
            ////////////////////////////////////////
            wBlockUserInputMock.VerifyAll();
        }
    }
}
