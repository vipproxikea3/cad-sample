﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;
#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
#endif
using JrxCad.Command.WBlock;
using JrxCadTest.Utility;
using Moq;
using static JrxCad.Command.WBlock.WBlockCmd;

namespace JrxCadTest.Command.WBlockTest
{
    public partial class WBlockCmdTest
    {
        /// <summary>
        /// PICKFIRST = 0 => D1 Opened
        /// </summary>
        public void ExecuteWBlockTest20()
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var wblockUserInputMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup active command (WBlock Command)
            wblockUserInputMock.Setup(a => a.GetActiveCommand()).Returns(1);
            // setup pickfirst
            wblockUserInputMock.Setup(a => a.GetPickFirst()).Returns(0);

            // Setup execute unit test
            wblockUserInputMock.Setup(a => a.IsExecuteUnitTest()).Returns(false);


            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlock = new WBlockCmd(wblockUserInputMock.Object);
            wBlock.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());

#endif

            ////////////////////////////////////////
            // Test Completed
            ////////////////////////////////////////
            wblockUserInputMock.VerifyAll();
        }

        /// <summary>
        /// PICKFIRST = 0 => select object => D1 Opened
        /// </summary>
        public void ExecuteWBlockTest21(ObjectId[] objectIds, string filePath)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var wblockUserInputMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup active command (WBlock Command)
            wblockUserInputMock.Setup(a => a.GetActiveCommand()).Returns(1);

            // Setup PICKFIRST
            wblockUserInputMock.Setup(a => a.GetPickFirst()).Returns(1);

            // Get Selection Set
            var selectionSet = SelectionSet.FromObjectIds(objectIds);
            // Setup PromptSelectionResult
            wblockUserInputMock.Setup(a => a.GetPromptSelectionResult()).Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            // setup object locked
            wblockUserInputMock.Setup(a => a.EntityOnLockedLayer(It.IsAny<Transaction>(), It.IsAny<Entity>())).Returns(false);

            // Setup execute unit test
            wblockUserInputMock.Setup(a => a.IsExecuteUnitTest()).Returns(false);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlock = new WBlockCmd(wblockUserInputMock.Object);
            wBlock.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());

#endif

            ////////////////////////////////////////
            // Test Completed
            ////////////////////////////////////////
            wblockUserInputMock.VerifyAll();
        }
    }
}
