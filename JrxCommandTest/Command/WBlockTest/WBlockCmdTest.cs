﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.Runtime;
#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
#endif
using JrxCad.Command.WBlock;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Moq;
using System.Collections.Generic;
using static JrxCad.Command.WBlock.WBlockCmd;

namespace JrxCadTest.Command.WBlockTest
{
    public partial class WBlockCmdTest
    {

        #region common method
        /// <summary>
        /// Get objectid list from document by xref name
        /// </summary>
        public ObjectId[] GetSelectionSetByXref(List<string> pathFileNames, bool isMultiXref = false)
        {
            var objectIds = new List<ObjectId>();
            var blockRefs = new Dictionary<string, ObjectId>();
            // SelectionSet input
            var entities = Util.Editor().SelectAll();
            if (entities == null || entities.Status != PromptStatus.OK)
            {
                return objectIds.ToArray();
            }
            using (var tr = Util.StartTransaction())
            {
                foreach (SelectedObject selectedObject in entities.Value)
                {
                    var blockRef = tr.GetObject<BlockReference>(selectedObject.ObjectId, OpenMode.ForRead);
                    if (blockRef == null || !pathFileNames.Contains(blockRef.Name))
                    {
                        continue;
                    }
                    if (blockRefs.ContainsKey(blockRef.Name))
                        blockRefs[blockRef.Name] = selectedObject.ObjectId;
                    else
                        blockRefs.Add(blockRef.Name, selectedObject.ObjectId);
                }
            }

            // sort xref item by input
            foreach (var xrefName in pathFileNames)
            {
                blockRefs.TryGetValue(xrefName, out var item);
                objectIds.Add(item);
            }

            return objectIds.ToArray();
        }

        public ObjectId[] CreateObjectId()
        {
            var collection = new List<ObjectId>();
            var startPoint = new Point3d(1498.2755, 2100.9096, 0);
            var endPoint = new Point3d(2329.7824, 1866.0859, 0);
            var line = new Line(startPoint, endPoint);
            using (var tr = Util.StartTransaction())
            {
                tr.AddNewlyCreatedDBObject(line, true, tr.CurrentSpace());
                collection.Add(line.Id);
                tr.Commit();
            }
            return collection.ToArray();
        }
        #endregion


        /// <summary>
        /// Remove Locked Objects => DM1
        /// </summary>
        public void ExecuteWBlockTest01(ObjectId[] objectIds)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var wblockUserInputMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup active command (WBlock Command)
            wblockUserInputMock.Setup(a => a.GetActiveCommand()).Returns(1);

            // Setup PICKFIRST
            wblockUserInputMock.Setup(a => a.GetPickFirst()).Returns(1);
            // Get Selection Set
            var selectionSet = SelectionSet.FromObjectIds(objectIds);
            // Setup PromptSelectionResult
            wblockUserInputMock.Setup(a => a.GetPromptSelectionResult()).Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            // setup object locked
            wblockUserInputMock.Setup(a => a.EntityOnLockedLayer(It.IsAny<Transaction>(), It.IsAny<Entity>())).Returns(true);

            // setup prevent execute OK  button
            wblockUserInputMock.Setup(a => a.IsExecuteUnitTest()).Returns(false);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlock = new WBlockCmd(wblockUserInputMock.Object);
            wBlock.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlock.GetType());

#endif

            ////////////////////////////////////////
            // Test Completed
            ////////////////////////////////////////
            wblockUserInputMock.VerifyAll();
        }

    }
}
