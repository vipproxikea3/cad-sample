﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.ArcCmd;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exception = System.Exception;
using System.Reflection;

namespace JrxCadTest.Command.ArcTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartLengthTest1", CommandFlags.Modal)]
        public void ArcCenterStartLengthTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double length = 10;
                DoTestCenterStartLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartLengthTest2", CommandFlags.Modal)]
        public void ArcCenterStartLengthTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double length = 20;
                DoTestCenterStartLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartLengthTest3", CommandFlags.Modal)]
        public void ArcCenterStartLengthTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double length = 10;
                DoTestCenterStartLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartLengthTest4", CommandFlags.Modal)]
        public void ArcCenterStartLengthTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double length = 20;
                DoTestCenterStartLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartLengthTest5", CommandFlags.Modal)]
        public void ArcCenterStartLengthTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double length = 10;
                DoTestCenterStartLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartLengthTest6", CommandFlags.Modal)]
        public void ArcCenterStartLengthTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double length = 20;
                DoTestCenterStartLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartLengthTest7", CommandFlags.Modal)]
        public void ArcCenterStartLengthTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double length = 10;
                DoTestCenterStartLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartLengthTest8", CommandFlags.Modal)]
        public void ArcCenterStartLengthTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double length = 20;
                DoTestCenterStartLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestCenterStartLength(Point3d centerPt, Point3d startPt, double length)
        {
            ExpectedResult temp = new ArcCaculator(centerPt, startPt, length).CalculateValueCenterStartLength();
            var objIds = DoTestArcCenterStartLength(centerPt, startPt, length);
            VerifyArc(objIds, Expected);
        }

        private ObjectIdCollection DoTestArcCenterStartLength(Point3d centerPt, Point3d startPt, double length)
        {
            //// Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetFirstCenterPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

            startPt = startPt.TransformBy(CoordConverter.UcsToWcs());
            JigPromptPointOptions optSecond = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondStartPoint(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.UpdatePoint(startPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", startPt));

            userInputMock.Setup(x => x.GetThirdEndPointAfterCenterStartOrSecond(It.IsAny<PointDrawJig.PointOptions>()))
               .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "L", Point3d.Origin));

            JigPromptDistanceOptions optLength = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetLength(It.IsAny<ArcCmd.JigPromptDistanceOpt>()))
                .Callback((DistanceDrawJig.DistanceOptions options) =>
                {
                    optLength = options.Options;
                    optLength.DefaultValue = 1;
                    options.UpdateDistance(length);
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", length));


            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // Execute Arc
            ArcCmd cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedArcIds;
        }
    }
}
