﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using Arc = GrxCAD.DatabaseServices.Arc;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Arc = Autodesk.AutoCAD.DatabaseServices.Arc;

#endif
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JrxCad.Helpers;
using JrxCad.Utility;
using System.Reflection;

namespace JrxCadTest.Command.ArcTest
{
    public class PropertyTest
    {
        public static void ArcPropertyTest(ObjectId id)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            using (var tr = Util.StartTransaction())
            using (var arc = tr.GetObject<Arc>(id, OpenMode.ForRead))
            {
                try
                {
                    //arc.Color.ColorIndex.Is((short)1);
                    //arc.Linetype.Is("Continuous");
                    //arc.LineWeight.Is(LineWeight.LineWeight100);
                    //arc.Transparency.Alpha.Is((byte)178);
                    //arc.LinetypeScale.Is(18);
                    //arc.PlotStyleNameId.Type.Is(PlotStyleNameType.PlotStyleNameByLayer);
                    //arc.Material.Is("Global");
                    //Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
                }
                catch (Exception)
                {
                    Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
                }
            }
        }
    }
}
