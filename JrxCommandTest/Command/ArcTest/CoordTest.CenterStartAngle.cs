﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.ArcCmd;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exception = System.Exception;
using System.Reflection;

namespace JrxCadTest.Command.ArcTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartAngleTest1", CommandFlags.Modal)]
        public void ArcCenterStartAngleTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double angle = (Math.PI / 180) * 30;
                DoTestCenterStartAngle(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartAngleTest2", CommandFlags.Modal)]
        public void ArcCenterStartAngleTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double angle = (Math.PI / 180) * 200;
                DoTestCenterStartAngle(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartAngleTest3", CommandFlags.Modal)]
        public void ArcCenterStartAngleTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double angle = (Math.PI / 180) * 30;
                DoTestCenterStartAngle(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartAngleTest4", CommandFlags.Modal)]
        public void ArcCenterStartAngleTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double angle = (Math.PI / 180) * 200;
                DoTestCenterStartAngle(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartAngleTest5", CommandFlags.Modal)]
        public void ArcCenterStartAngleTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double angle = (Math.PI / 180) * 30;
                DoTestCenterStartAngle(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartAngleTest6", CommandFlags.Modal)]
        public void ArcCenterStartAngleTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double angle = (Math.PI / 180) * 200;
                DoTestCenterStartAngle(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartAngleTest7", CommandFlags.Modal)]
        public void ArcCenterStartAngleTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double angle = (Math.PI / 180) * 30;
                DoTestCenterStartAngle(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartAngleTest8", CommandFlags.Modal)]
        public void ArcCenterStartAngleTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UC3
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double angle = (Math.PI / 180) * 200;
                DoTestCenterStartAngle(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestCenterStartAngle(Point3d centerPt, Point3d startPt, double angle)
        {
            ExpectedResult Expected = new ArcCaculator(centerPt, startPt, angle).CalculateValueCenterStartAngle();
            var objIds = DoTestArcCenterStartAngle(centerPt, startPt, angle);
            VerifyArc(objIds, Expected);
        }

        private ObjectIdCollection DoTestArcCenterStartAngle(Point3d centerPt, Point3d startPt, double angle)
        {
            //// Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            // Create Mock
            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetFirstCenterPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

            startPt = startPt.TransformBy(CoordConverter.UcsToWcs());
            JigPromptPointOptions optSecond = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondStartPoint(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.UpdatePoint(startPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", startPt));

            userInputMock.Setup(x => x.GetThirdEndPointAfterCenterStartOrSecond(It.IsAny<PointDrawJig.PointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            JigPromptAngleOptions optAngle = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetAngle(It.IsAny<ArcCmd.JigPromptAngleOpt>()))
                .Callback((AngleDrawJig.AngleOptions options) =>
                {
                    optAngle = options.Options;
                    optAngle.DefaultValue = 0.0;
                    options.UpdateAngle(angle);
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", angle));


            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // Execute Arc
            ArcCmd cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedArcIds;
        }
    }
}
