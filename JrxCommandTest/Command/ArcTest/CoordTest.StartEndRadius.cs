﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.ArcCmd;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exception = System.Exception;
using System.Reflection;

namespace JrxCadTest.Command.ArcTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndRadiusTest1", CommandFlags.Modal)]
        public void ArcStartEndRadiusTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double radius = 30;
                DoTestStartEndRadius(pt1, pt2, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndRadiusTest2", CommandFlags.Modal)]
        public void ArcStartEndRadiusTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double radius = 100;
                DoTestStartEndRadius(pt1, pt2, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndRadiusTest3", CommandFlags.Modal)]
        public void ArcStartEndRadiusTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double radius = 30;
                DoTestStartEndRadius(pt1, pt2, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndRadiusTest4", CommandFlags.Modal)]
        public void ArcStartEndRadiusTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double radius = 100;
                DoTestStartEndRadius(pt1, pt2, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndRadiusTest5", CommandFlags.Modal)]
        public void ArcStartEndRadiusTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double radius = 30;
                DoTestStartEndRadius(pt1, pt2, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndRadiusTest6", CommandFlags.Modal)]
        public void ArcStartEndRadiusTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double radius = 100;
                DoTestStartEndRadius(pt1, pt2, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndRadiusTest7", CommandFlags.Modal)]
        public void ArcStartEndRadiusTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double radius = 30;
                DoTestStartEndRadius(pt1, pt2, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndRadiusTest8", CommandFlags.Modal)]
        public void ArcStartEndRadiusTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double radius = 100;
                DoTestStartEndRadius(pt1, pt2, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestStartEndRadius(Point3d startPt, Point3d endPt, double radius)
        {
            ExpectedResult Expected = new ArcCaculator(startPt, endPt, radius).CalculateValueStartEndRadius();
            var objIds = DoTestArcStartEndRadius(startPt, endPt, radius);
            VerifyArc(objIds, Expected);
        }

        private ObjectIdCollection DoTestArcStartEndRadius(Point3d startPt, Point3d endPt, double radius)
        {
            //// Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            PromptPointOptions optStart = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optStart = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", startPt));

            userInputMock.Setup(x => x.GetSecondMidPoint(It.IsAny<PointDrawJig.PointOptions>()))
               .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "E", Point3d.Origin));

            endPt = endPt.TransformBy(CoordConverter.UcsToWcs());
            JigPromptPointOptions optCenter = null;        // for PromptOptions Test
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondEndPointAfterStart(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optCenter = options.Options;
                    optCenter.DefaultValue = new Point3d();
                    options.UpdatePoint(endPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", endPt));

            userInputMock.Setup(x => x.GetThirdCenterPoint(It.IsAny<PointDrawJig.PointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "R", Point3d.Origin));

            JigPromptDistanceOptions optRadius = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetRadius(It.IsAny<ArcCmd.JigPromptDistanceOpt>()))
                .Callback((DistanceDrawJig.DistanceOptions options) =>
                {
                    optRadius = options.Options;
                    optRadius.DefaultValue = 0.0;
                    options.UpdateDistance(radius);
                    optRadius.BasePoint = endPt.TransformBy(CoordConverter.UcsToWcs());
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", radius));


            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // Execute Arc
            ArcCmd cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedArcIds;
        }
    }
}
