﻿    #if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.ArcCmd;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exception = System.Exception;
using System.Reflection;

namespace JrxCadTest.Command.ArcTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterEndTest1", CommandFlags.Modal)]
        public void ArcStartCenterEndTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                Point3d pt3 = new Point3d(60, 50, pt1.Z);
                DoTestStartCenterEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterEndTest2", CommandFlags.Modal)]
        public void ArcStartCenterEndTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                Point3d pt3 = new Point3d(20, 30, pt1.Z);
                DoTestStartCenterEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterEndTest3", CommandFlags.Modal)]
        public void ArcStartCenterEndTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                Point3d pt3 = new Point3d(60, 50, pt1.Z);
                DoTestStartCenterEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterEndTest4", CommandFlags.Modal)]
        public void ArcStartCenterEndTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                Point3d pt3 = new Point3d(20, 30, pt1.Z);
                DoTestStartCenterEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterEndTest5", CommandFlags.Modal)]
        public void ArcStartCenterEndTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                Point3d pt3 = new Point3d(60, 50, pt1.Z);
                DoTestStartCenterEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterEndTest6", CommandFlags.Modal)]
        public void ArcStartCenterEndTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                Point3d pt3 = new Point3d(20, 30, pt1.Z);
                DoTestStartCenterEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterEndTest7", CommandFlags.Modal)]
        public void ArcStartCenterEndTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                Point3d pt3 = new Point3d(60, 50, pt1.Z);
                DoTestStartCenterEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterEndTest8", CommandFlags.Modal)]
        public void ArcStartCenterEndTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                Point3d pt3 = new Point3d(20, 30, pt1.Z);
                DoTestStartCenterEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestStartCenterEnd(Point3d startPt, Point3d centerPt, Point3d endPt)
        {
            ExpectedResult temp = new ArcCaculator(startPt, centerPt, endPt).CalculateValueStartCenterEnd();
            var objIds = DoTestArcStartCenterEnd(startPt, centerPt, endPt);
            VerifyArc(objIds, temp);
        }

        private ObjectIdCollection DoTestArcStartCenterEnd(Point3d startPt, Point3d centerPt, Point3d endPt)
        {
            //// Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            PromptPointOptions optStart = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optStart = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", startPt));

            userInputMock.Setup(x => x.GetSecondMidPoint(It.IsAny<PointDrawJig.PointOptions>()))
               .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            centerPt = centerPt.TransformBy(CoordConverter.UcsToWcs());
            JigPromptPointOptions optCenter = null;        // for PromptOptions Test
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondCenterPoint(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optCenter = options.Options;
                    optCenter.DefaultValue = new Point3d();
                    options.UpdatePoint(centerPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

            JigPromptPointOptions optEnd = null;
            endPt = endPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetThirdEndPointAfterCenterStartOrSecond(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optEnd = options.Options;
                    optEnd.DefaultValue = new Point3d();
                    options.UpdatePoint(endPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", endPt));


            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // Execute Arc
            ArcCmd cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedArcIds;
        }
    }
}
