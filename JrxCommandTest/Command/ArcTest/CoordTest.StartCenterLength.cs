﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.ArcCmd;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exception = System.Exception;
using System.Reflection;

namespace JrxCadTest.Command.ArcTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterLengthTest1", CommandFlags.Modal)]
        public void ArcStartCenterLengthTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double length = 10;
                DoTestStartCenterLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterLengthTest2", CommandFlags.Modal)]
        public void ArcStartCenterLengthTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double length = 20;
                DoTestStartCenterLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterLengthTest3", CommandFlags.Modal)]
        public void ArcStartCenterLengthTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double length = 10;
                DoTestStartCenterLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterLengthTest4", CommandFlags.Modal)]
        public void ArcStartCenterLengthTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double length = 20;
                DoTestStartCenterLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterLengthTest5", CommandFlags.Modal)]
        public void ArcStartCenterLengthTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double length = 10;
                DoTestStartCenterLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterLengthTest6", CommandFlags.Modal)]
        public void ArcStartCenterLengthTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double length = 20;
                DoTestStartCenterLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterLengthTest7", CommandFlags.Modal)]
        public void ArcStartCenterLengthTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double length = 10;
                DoTestStartCenterLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartCenterLengthTest8", CommandFlags.Modal)]
        public void ArcStartCenterLengthTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double length = 20;
                DoTestStartCenterLength(pt1, pt2, length);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestStartCenterLength(Point3d startPt, Point3d centerPt, double length)
        {
            ExpectedResult temp = new ArcCaculator(startPt, centerPt, length).CalculateValueStartCenterLength();
            var objIds = DoTestArcStartCenterLength(startPt, centerPt, length);
            VerifyArc(objIds, Expected);
        }

        private ObjectIdCollection DoTestArcStartCenterLength(Point3d startPt, Point3d centerPt, double length)
        {
            //// Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();


            PromptPointOptions optStart = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optStart = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", startPt));

            userInputMock.Setup(x => x.GetSecondMidPoint(It.IsAny<PointDrawJig.PointOptions>()))
               .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            centerPt = centerPt.TransformBy(CoordConverter.UcsToWcs());
            JigPromptPointOptions optCenter = null;        // for PromptOptions Test
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondCenterPoint(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optCenter = options.Options;
                    optCenter.DefaultValue = new Point3d();
                    options.UpdatePoint(centerPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

            userInputMock.Setup(x => x.GetThirdEndPointAfterCenterStartOrSecond(It.IsAny<PointDrawJig.PointOptions>()))
               .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "L", Point3d.Origin));

            JigPromptDistanceOptions optLength = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetLength(It.IsAny<ArcCmd.JigPromptDistanceOpt>()))
                .Callback((DistanceDrawJig.DistanceOptions options) =>
                {
                    optLength = options.Options;
                    optLength.DefaultValue = 1;
                    options.UpdateDistance(length);
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", length));


            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // Execute Arc
            ArcCmd cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedArcIds;
        }
    }
}
