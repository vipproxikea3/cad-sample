﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.ArcCmd;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exception = System.Exception;
using System.Reflection;

namespace JrxCadTest.Command.ArcTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndDirectionTest1", CommandFlags.Modal)]
        public void ArcStartEndDirectionTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double angle = (Math.PI / 180) * 30;
                DoTestStartEndDirection(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndDirectionTest2", CommandFlags.Modal)]
        public void ArcStartEndDirectionTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double angle = (Math.PI / 180) * 200;
                DoTestStartEndDirection(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndDirectionTest3", CommandFlags.Modal)]
        public void ArcStartEndDirectionTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double angle = (Math.PI / 180) * 30;
                DoTestStartEndDirection(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndDirectionTest4", CommandFlags.Modal)]
        public void ArcStartEndDirectionTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double angle = (Math.PI / 180) * 200;
                DoTestStartEndDirection(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndDirectionTest5", CommandFlags.Modal)]
        public void ArcStartEndDirectionTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double angle = (Math.PI / 180) * 30;
                DoTestStartEndDirection(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndDirectionTest6", CommandFlags.Modal)]
        public void ArcStartEndDirectionTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double angle = (Math.PI / 180) * 200;
                DoTestStartEndDirection(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndDirectionTest7", CommandFlags.Modal)]
        public void ArcStartEndDirectionTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double angle = (Math.PI / 180) * 30;
                DoTestStartEndDirection(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcStartEndDirectionTest8", CommandFlags.Modal)]
        public void ArcStartEndDirectionTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                double angle = (Math.PI / 180) * 200;
                DoTestStartEndDirection(pt1, pt2, angle);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestStartEndDirection(Point3d startPt, Point3d endPt, double angle)
        {
            ExpectedResult Expected = new ArcCaculator(startPt, endPt, angle).CalculateValueStartEndDirection();
            var objIds = DoTestArcStartEndDirection(startPt, endPt, angle);
            VerifyArc(objIds, Expected);
        }

        private ObjectIdCollection DoTestArcStartEndDirection(Point3d startPt, Point3d endPt, double angle)
        {
            //// Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            PromptPointOptions optStart = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optStart = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", startPt));

            userInputMock.Setup(x => x.GetSecondMidPoint(It.IsAny<PointDrawJig.PointOptions>()))
               .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "E", Point3d.Origin));

            endPt = endPt.TransformBy(CoordConverter.UcsToWcs());
            JigPromptPointOptions optCenter = null;        // for PromptOptions Test
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondEndPointAfterStart(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optCenter = options.Options;
                    optCenter.DefaultValue = new Point3d();
                    options.UpdatePoint(endPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", endPt));

            userInputMock.Setup(x => x.GetThirdCenterPoint(It.IsAny<PointDrawJig.PointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));

            JigPromptAngleOptions optAngle = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetDirection(It.IsAny<ArcCmd.JigPromptAngleOpt>()))
                .Callback((AngleDrawJig.AngleOptions options) =>
                {
                    optAngle = options.Options;
                    optAngle.DefaultValue = 0.0;
                    options.UpdateAngle(angle);
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", angle));


            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // Execute Arc
            ArcCmd cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedArcIds;
        }
    }
}
