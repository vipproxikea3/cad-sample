﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;

#endif
using JrxCad.Command.ArcCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using System.Reflection;

namespace JrxCadTest.Command.ArcTest
{
    public class MessageTest
    {
        public static void ArcMessageTestForThreePoint(ArcCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptPointOptions optEnd, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageStartPointFirst(optFirst);
            VerifyMessageSecondPoint(optSecond, firstPt);
            VerifyMessageEndPointAfterStartAndMid(optEnd, secondPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void ArcMessageTestForCenterStartEnd(ArcCmd cmd, PromptPointOptions optCenter, Point3d centerPt, JigPromptPointOptions optStart, JigPromptPointOptions optEnd)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageCenterPointFirst(optCenter);
            VerifyMessageStartSecond(optStart, centerPt);
            VerifyMessageEndPointAfterCenterFirstorSecond(optEnd, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }
        public static void ArcMessageTestForCenterStartAngle(ArcCmd cmd, PromptPointOptions optCenter, Point3d centerPt, JigPromptPointOptions optStart, JigPromptAngleOptions optAngle)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageCenterPointFirst(optCenter);
            VerifyMessageStartSecond(optStart, centerPt);
            VerifyMessageAngleEnd(optAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void ArcMessageTestForCenterStartLength(ArcCmd cmd, PromptPointOptions optCenter, JigPromptPointOptions optStart, JigPromptDistanceOptions optLength, Point3d centerPt, Point3d start)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageCenterPointFirst(optCenter);
            VerifyMessageStartSecond(optStart, centerPt);
            VerifyMessageLengthEnd(optLength, start);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void ArcMessageTestForStartCenterEnd(ArcCmd cmd, PromptPointOptions optStart, JigPromptPointOptions optCenter, JigPromptPointOptions optEnd, Point3d start, Point3d center)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageStartPointFirst(optStart);
            VerifyMessageCenterSecond(optCenter, start);
            VerifyMessageEndPointAfterCenterFirstorSecond(optEnd, center);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void ArcMessageTestForStartCenterAngle(ArcCmd cmd, PromptPointOptions optStart, JigPromptPointOptions optCenter, JigPromptAngleOptions optAngle, Point3d start, Point3d center)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageStartPointFirst(optStart);
            VerifyMessageCenterSecond(optCenter, start);
            VerifyMessageAngleEnd(optAngle, center);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void ArcMessageTestForStartCenterLength(ArcCmd cmd, PromptPointOptions optStart, JigPromptPointOptions optCenter, JigPromptDistanceOptions optLength, Point3d start)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageStartPointFirst(optStart);
            VerifyMessageCenterSecond(optCenter, start);
            VerifyMessageLengthEnd(optLength, start);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void ArcMessageTestForStarEndAngle(ArcCmd cmd, PromptPointOptions optStart, JigPromptPointOptions optSecond, JigPromptAngleOptions optAngle, Point3d start)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageStartPointFirst(optStart);
            VerifyMessageEndPointSecond(optSecond, start);
            VerifyMessageAngleEnd(optAngle, start);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }
        public static void ArcMessageTestForStarEndDirection(ArcCmd cmd, PromptPointOptions optStart, JigPromptPointOptions optSecond, JigPromptAngleOptions optAngle, Point3d start)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageStartPointFirst(optStart);
            VerifyMessageEndPointSecond(optSecond, start);
            VerifyMessageDirectionEnd(optAngle, start);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void ArcMessageTestForStarEndRadius(ArcCmd cmd, PromptPointOptions optStart, JigPromptPointOptions optSecond, JigPromptDistanceOptions optdistance, Point3d start, Point3d end)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageStartPointFirst(optStart);
            VerifyMessageEndPointSecond(optSecond, start);
            VerifyMessageRadiusEnd(optdistance, end);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void ArcMessageTestForContinue(ArcCmd cmd, JigPromptPointOptions optEnd,Point3d startPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageEndPointContinue(optEnd,startPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void VerifyCommandMethod(ArcCmd cmd)
        {
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "SmxARC", CommandFlags.Modal | CommandFlags.NoNewStack, cmd.GetType());
        }

        public static void VerifyMessageStartPointFirst(PromptPointOptions optFirst)
        {
            VerifyEx.VerifyPromptPointOptions(optFirst,
                new PromptPointOptions($"\nSpecify start point of arc or")
                {
                    AllowNone = true,
                    Keywords = {
                                { "C", "C", "Center" },
                           },
                });
        }

        public static void VerifyMessageCenterPointFirst(PromptPointOptions optFirst)
        {
            VerifyEx.VerifyPromptPointOptions(optFirst,
                new PromptPointOptions($"\nSpecify center point of arc"));
        }
        public static void VerifyMessageSecondPoint(JigPromptPointOptions optSecond, Point3d firstPt)
        {
            VerifyEx.VerifyJigPromptPointOptions(optSecond,
                new JigPromptPointOptions($"\nSpecify second point of arc ")
                {
                    DefaultValue = new Point3d(),
                    BasePoint = firstPt.TransformBy(CoordConverter.UcsToWcs()),
                    UseBasePoint = true,
                    Keywords =
                                {
                                    { "C", "C", "Center" },
                                    { "E", "E", "End" },

                                },
                    Cursor = CursorType.RubberBand,
                });
        }
        public static void VerifyMessageEndPointSecond(JigPromptPointOptions optSecond, Point3d startPt)
        {
            VerifyEx.VerifyJigPromptPointOptions(optSecond,
               new JigPromptPointOptions($"\nSpecify end point of arc ")
               {
                   DefaultValue = new Point3d(),
                   UseBasePoint = true,
                   BasePoint = startPt.TransformBy(CoordConverter.UcsToWcs()),
                   Cursor = CursorType.RubberBand,
               });
        }

        public static void VerifyMessageStartSecond(JigPromptPointOptions optStart, Point3d centerPt)
        {
            VerifyEx.VerifyJigPromptPointOptions(optStart,
                new JigPromptPointOptions($"\nSpecify start point of arc")
                {
                    DefaultValue = new Point3d(),
                    UseBasePoint = true,
                    Cursor = CursorType.RubberBand,
                    BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs()),
                });
        }
        public static void VerifyMessageCenterSecond(JigPromptPointOptions optCenter, Point3d startPt)
        {
            VerifyEx.VerifyJigPromptPointOptions(optCenter,
            new JigPromptPointOptions($"\nSpecify center point of arc ")
            {
                DefaultValue = new Point3d(),
                BasePoint = startPt.TransformBy(CoordConverter.UcsToWcs()),
                UseBasePoint = true,
                Cursor = CursorType.RubberBand,
            });
        }

        public static void VerifyMessageEndPointAfterStartAndMid(JigPromptPointOptions optEnd, Point3d secondPt)
        {
            VerifyEx.VerifyJigPromptPointOptions(optEnd,
                new JigPromptPointOptions($"\nSpecify end point of arc ")
                {
                    DefaultValue = new Point3d(),
                    UseBasePoint = true,
                    BasePoint = secondPt.TransformBy(CoordConverter.UcsToWcs()),
                });
        }
        public static void VerifyMessageAngleEnd(JigPromptAngleOptions optAngle, Point3d centerPt)
        {
            VerifyEx.VerifyJigPromptAngleOptions(optAngle,
                new JigPromptAngleOptions($"\nSpecify included angle of arc (hold Ctrl to switch direction):")
                {
                    DefaultValue = 0,
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.UseBasePointElevation,
                    Cursor = CursorType.RubberBand,
                    BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs()),
                    UseBasePoint = true,
                });
        }
        public static void VerifyMessageEndPointAfterCenterFirstorSecond(JigPromptPointOptions optEnd, Point3d centerPt)
        {
            VerifyEx.VerifyJigPromptPointOptions(optEnd,
                new JigPromptPointOptions($"\nSpecify end point of arc (hold Ctrl to switch direction) or ")
                {
                    DefaultValue = new Point3d(),
                    BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs()),
                    UseBasePoint = true,
                    Keywords =
                                {
                                    { "A", "A", "Angle" },
                                    { "L", "L", "chord Length" },
                                },
                    Cursor = CursorType.RubberBand,
                });
        }

        public static void VerifyMessageDirectionEnd(JigPromptAngleOptions optDirection, Point3d startPt)
        {
            VerifyEx.VerifyJigPromptAngleOptions(optDirection,
                new JigPromptAngleOptions($"\nSpecify tangent direction for the start point of arc (hold Ctrl to switch direction):")
                {
                    DefaultValue = 0,
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.UseBasePointElevation,
                    Cursor = CursorType.RubberBand,
                    BasePoint = startPt.TransformBy(CoordConverter.UcsToWcs()),
                    UseBasePoint = true,
                });
        }
        public static void VerifyMessageRadiusEnd(JigPromptDistanceOptions optRadius, Point3d endPt)
        {
            VerifyEx.VerifyJigPromptDistanceOptions(optRadius,
                new JigPromptDistanceOptions($"\nSpecify radius of arc (hold Ctrl to switch direction):")
                {
                    DefaultValue = 0,
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.UseBasePointElevation |
                                        UserInputControls.NoZeroResponseAccepted,
                    Cursor = CursorType.RubberBand,
                    BasePoint = endPt.TransformBy(CoordConverter.UcsToWcs()),
                    UseBasePoint = true,
                });
        }

        public static void VerifyMessageLengthEnd(JigPromptDistanceOptions optLength, Point3d startPt)
        {
            VerifyEx.VerifyJigPromptDistanceOptions(optLength,
                new JigPromptDistanceOptions($"\nSpecify length of chord (hold Ctrl to switch direction):")
                {
                    DefaultValue = 1,
                    UseBasePoint = true,
                    BasePoint = startPt.TransformBy(CoordConverter.UcsToWcs()),
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.UseBasePointElevation |
                                        UserInputControls.NoZeroResponseAccepted,
                    Cursor = CursorType.RubberBand,

                }); ;
        }

        public static void VerifyMessageEndPointContinue(JigPromptPointOptions optEnd, Point3d startPt)
        {
            VerifyEx.VerifyJigPromptPointOptions(optEnd,
               new JigPromptPointOptions($"\nSpecify end point of arc (hold Ctrl to switch direction)")
               {
                   DefaultValue = new Point3d(),
                   BasePoint = startPt.TransformBy(CoordConverter.UcsToWcs()),
                   UseBasePoint = true,
                   Cursor = CursorType.RubberBand,
               });
        }
    }
}
