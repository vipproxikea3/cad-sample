﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using GrxCAD.ApplicationServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;

#endif
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using JrxCad.Command.ArcCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using System;

namespace JrxCadTest.Command.ArcTest
{
    [TestClass]
    public class Test : BaseTest
    {
        private PromptResult _resValueSet;
        private PromptResult _resValueOptContinue;
        private string _classFullName;

        public override void OnStart()
        {
            PromptKeywordOptions optValueSet = new PromptKeywordOptions($"Specify the coordinate system to test")
            {
                Keywords =
                {
                    { "W", "W", "Wcs" },
                    { "1", "1", "ucs1"},
                    { "2", "2", "ucs2"},
                    { "3", "3", "ucs3"},
                },
            };
            _resValueSet = Util.Editor().GetKeywords(optValueSet);
            var methodInfo = System.Reflection.MethodBase.GetCurrentMethod();
            _classFullName = methodInfo.DeclaringType.FullName;

            Util.Editor().WriteMessage($"\n{_classFullName.ToUpper()} START!");
        }
        public override void OnFinish()
        {
            Util.Editor().WriteMessage($"\n{_classFullName.ToUpper()} COMPLETED!");
        }
        public  void OptionsContinue()
        {
            PromptKeywordOptions optContinue = new PromptKeywordOptions($"\nSpecify the case to test")
            {
                Keywords =
                {
                    { "1", "1", "case1"},
                    { "2", "2", "case2"},
                    { "3", "3", "case3"},
                    { "4", "4", "case4"},
                },
            };
            _resValueOptContinue = Util.Editor().GetKeywords(optContinue);
        }
        private void changeCoordinateSystem(string coordStr)
        {
            Matrix3d ucs = Matrix3d.Identity;
            switch (coordStr)
            {
                case "W": //WCS
                    //ucs = Matrix3d.Identity;
                    break;
                case "1": //UCS1
                    ucs = Matrix3d.Identity * Matrix3d.Displacement(new Vector3d(10, 20, 30));
                    break;
                case "2": //UCS2
                    ucs = Matrix3d.Identity * Matrix3d.Displacement(new Vector3d(10, 20, 30)) *
                          Matrix3d.Identity * Matrix3d.Rotation(Math.PI / 6, Vector3d.XAxis, Point3d.Origin);
                    break;
                case "3": //UCS3
                    ucs = Matrix3d.Identity *
                          Matrix3d.Displacement(new Vector3d(10, 20, 30)) *
                          Matrix3d.Rotation(Math.PI / 4, Vector3d.XAxis, Point3d.Origin) *
                          Matrix3d.Rotation(Math.PI / 6, Vector3d.YAxis, Point3d.Origin) *
                          Matrix3d.Rotation(Math.PI / 3, Vector3d.ZAxis, Point3d.Origin);
                    break;
                default:
                    return;
            }
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            ed.CurrentUserCoordinateSystem = ucs;
        }
        [CommandMethod("JrxCommandTest", "SmxSmxARCTESTCASETHREEPOINT", CommandFlags.Modal)]
        public void ARCTESTCASETHREEPOINT()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcThreePointCoordTest1();
                    coordTest.ArcThreePointCoordTest2();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcThreePointCoordTest3();
                    coordTest.ArcThreePointCoordTest4();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcThreePointCoordTest5();
                    coordTest.ArcThreePointCoordTest6();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcThreePointCoordTest7();
                    coordTest.ArcThreePointCoordTest8();
                    break;
                default:
                    return;
            }
                coordTest.LastArcId.IsNull.IsFalse();

            //Propertytest
           // PropertyTest.ArcPropertyTest(coordTest.LastArcId);

            //MessageTest
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);

            // Setup Start Point 
            var sequence = new MockSequence();
            PromptPointOptions optStart = null;
            var startPt = new Point3d(10, 0, 0);
            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optStart = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", startPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d SecondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondMidPoint(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.UpdatePoint(SecondPtW);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", SecondPtW));

            // Setup End Point
            var endPt = new Point3d(0, 10, 0);
            JigPromptPointOptions optEnd = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetThirdEndPointAfterStartAndMid(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optEnd = options.Options;
                    optEnd.DefaultValue = new Point3d();
                    options.UpdatePoint(endPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", endPt));

            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            var cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            MessageTest.ArcMessageTestForThreePoint(cmd, optStart, optSecond, optEnd, startPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();
            OnFinish();
        }
        #endregion Pattern1: ThreePoint

        //Case Center-Start-End
        [CommandMethod("JrxCommandTest", "SmxSmxARCTESTCASECENTERSTARTEND", CommandFlags.Modal)]
        public void ARCTESTCASECENTERSTARTEND()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcCenterStartEndCoordTest1();
                    coordTest.ArcCenterStartEndCoordTest2();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcCenterStartEndCoordTest3();
                    coordTest.ArcCenterStartEndCoordTest4();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcCenterStartEndCoordTest5();
                    coordTest.ArcCenterStartEndCoordTest6();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcCenterStartEndCoordTest7();
                    coordTest.ArcCenterStartEndCoordTest8();
                    break;
                default:
                    return;
            }
            coordTest.LastArcId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.ArcPropertyTest(coordTest.LastArcId);

            //MessageTest
            // Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            //Switch to case CenterFirst
            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            // Setup Center Point 
            PromptPointOptions optCenter = null;
            var centerPt = Point3d.Origin;
            userInputMock.Setup(x => x.GetFirstCenterPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", centerPt));

            // Setup Start Point
            JigPromptPointOptions optStart = null;
            var startPt = new Point3d(0, 10, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondStartPoint(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optStart = options.Options;
                    optStart.DefaultValue = new Point3d();
                    options.UpdatePoint(startPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", startPt));

            // Setup End Point
            JigPromptPointOptions optEnd = null;
            Point3d endPt = new Point3d(0, 0, 10);
            userInputMock.InSequence(sequence).Setup(x => x.GetThirdEndPointAfterCenterStartOrSecond(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optEnd = options.Options;
                    optEnd.DefaultValue = new Point3d();
                    options.UpdatePoint(endPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", endPt));

            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            var cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            MessageTest.ArcMessageTestForCenterStartEnd(cmd, optCenter, centerPt, optStart, optEnd);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();
            OnFinish();
        }
        #endregion 


        //Case: Center-Start-Angle
        [CommandMethod("JrxCommandTest", "SmxSmxARCTESTCASECENTERSTARTANGLE", CommandFlags.Modal)]
        public void ARCTESTCASECENTERSTARTANGLE()
        {
            OnStart();
            //Coordinate test
            CoordTest coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcCenterStartAngleCoordTest1();
                    coordTest.ArcCenterStartAngleCoordTest2();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcCenterStartAngleCoordTest3();
                    coordTest.ArcCenterStartAngleCoordTest4();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcCenterStartAngleCoordTest5();
                    coordTest.ArcCenterStartAngleCoordTest6();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcCenterStartAngleCoordTest7();
                    coordTest.ArcCenterStartAngleCoordTest8();
                    break;
                default:
                    return;
            }
            coordTest.LastArcId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.ArcPropertyTest(coordTest.LastArcId);

            //MessageTest
            // Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            //Switch to case CenterFirst
            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            // Setup Center Point 
            PromptPointOptions optCenter = null;
            var centerPt = Point3d.Origin;
            userInputMock.Setup(x => x.GetFirstCenterPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", centerPt));

            // Setup Start Point
            JigPromptPointOptions optStart = null;
            var startPt = new Point3d(0, 10, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondStartPoint(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optStart = options.Options;
                    optStart.DefaultValue = new Point3d();
                    options.UpdatePoint(startPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", startPt));

            // Switch to Angle end
            userInputMock.Setup(x => x.GetThirdEndPointAfterCenterStartOrSecond(It.IsAny<PointDrawJig.PointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            //Setup Angle
            JigPromptAngleOptions optAngle = null;
            double angle = 0.5;
            userInputMock.InSequence(sequence).Setup(x => x.GetAngle(It.IsAny<ArcCmd.JigPromptAngleOpt>()))
                .Callback((AngleDrawJig.AngleOptions options) =>
                {
                    optAngle = options.Options;
                    optAngle.DefaultValue = 0;
                    options.UpdateAngle(angle);
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "0.0", angle));

            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            var cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            MessageTest.ArcMessageTestForCenterStartAngle(cmd, optCenter, centerPt, optStart, optAngle);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();
            OnFinish();
        }
        #endregion 


        [CommandMethod("JrxCommandTest", "SmxSmxARCTESTCASECENTERSTARTLENGTH", CommandFlags.Modal)]
        public void ARCTESTCASECENTERSTARTLENGTH()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcCenterStartLengthCoordTest1();
                    coordTest.ArcCenterStartLengthCoordTest2();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcCenterStartLengthCoordTest3();
                    coordTest.ArcCenterStartLengthCoordTest4();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcCenterStartLengthCoordTest5();
                    coordTest.ArcCenterStartLengthCoordTest6();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcCenterStartLengthCoordTest7();
                    coordTest.ArcCenterStartLengthCoordTest8();
                    break;
                default:
                    return;
            }
            coordTest.LastArcId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.ArcPropertyTest(coordTest.LastArcId);

            //MessageTest
            // Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            //Switch to case CenterFirst
            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            // Setup Center Point 
            PromptPointOptions optCenter = null;
            var centerPt = Point3d.Origin;
            userInputMock.Setup(x => x.GetFirstCenterPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", centerPt));

            // Setup Start Point
            JigPromptPointOptions optStart = null;
            var startPt = new Point3d(0, 10, 0);
            Point3d startPtW = startPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondStartPoint(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optStart = options.Options;
                    optStart.DefaultValue = new Point3d();
                    options.UpdatePoint(startPtW);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", startPtW));

            // Switch to Length end
            userInputMock.Setup(x => x.GetThirdEndPointAfterCenterStartOrSecond(It.IsAny<PointDrawJig.PointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "L", Point3d.Origin));

            //Setup length
            JigPromptDistanceOptions optLength = null;
            double length = 1;
            userInputMock.InSequence(sequence).Setup(x => x.GetLength(It.IsAny<ArcCmd.JigPromptDistanceOpt>()))
                .Callback((DistanceDrawJig.DistanceOptions options) =>
                {
                    optLength = options.Options;
                    optLength.DefaultValue = 1;
                    options.UpdateDistance(length);

                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "1.0", length));



            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            var cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            MessageTest.ArcMessageTestForCenterStartLength(cmd, optCenter, optStart, optLength, centerPt, startPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();
            OnFinish();
        }
        #endregion 

        [CommandMethod("JrxCommandTest", "SmxSmxARCTESTCASESTARTCENTEREND", CommandFlags.Modal)]
        public void ARCTESTCASESTARTCENTEREND()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartCenterEndCoordTest1();
                    coordTest.ArcStartCenterEndCoordTest2();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartCenterEndCoordTest3();
                    coordTest.ArcStartCenterEndCoordTest4();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartCenterEndCoordTest5();
                    coordTest.ArcStartCenterEndCoordTest6();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartCenterEndCoordTest7();
                    coordTest.ArcStartCenterEndCoordTest8();
                    break;
                default:
                    return;
            }
            coordTest.LastArcId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.ArcPropertyTest(coordTest.LastArcId);

            //MessageTest
            // Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            //Setup start point
            PromptPointOptions optStart = null;
            var startPt = Point3d.Origin;
            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optStart = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", startPt));

            // Switch to center point second
            userInputMock.Setup(x => x.GetSecondMidPoint(It.IsAny<PointDrawJig.PointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            //Setup center point
            JigPromptPointOptions optCenter = null;
            var centerPt = new Point3d(0, 10, 0);
            Point3d centerPtW = centerPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondCenterPoint(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optCenter = options.Options;
                    optCenter.DefaultValue = new Point3d();
                    options.UpdatePoint(centerPtW);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", centerPtW));

            // Switch to  end point       
            JigPromptPointOptions optEnd = null;
            Point3d endPt = new Point3d(20, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetThirdEndPointAfterCenterStartOrSecond(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optEnd = options.Options;
                    optEnd.DefaultValue = new Point3d();
                    options.UpdatePoint(endPt);
                    optEnd.BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs());
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", endPt));

            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            var cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            MessageTest.ArcMessageTestForStartCenterEnd(cmd, optStart, optCenter, optEnd, startPt, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();
            OnFinish();
        }
        #endregion Pattern5: Start-Center-End

        [CommandMethod("JrxCommandTest", "SmxSmxARCTESTCASESTARTCENTERANGLE", CommandFlags.Modal)]
        public void ARCTESTCASESTARTCENTERANGLE()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartCenterAngleCoordTest1();
                    coordTest.ArcStartCenterAngleCoordTest2();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartCenterAngleCoordTest3();
                    coordTest.ArcStartCenterAngleCoordTest4();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartCenterAngleCoordTest5();
                    coordTest.ArcStartCenterAngleCoordTest6();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartCenterAngleCoordTest7();
                    coordTest.ArcStartCenterAngleCoordTest8();
                    break;
                default:
                    return;
            }
            coordTest.LastArcId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.ArcPropertyTest(coordTest.LastArcId);

            //MessageTest
            // Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);

            // Setup Center Point 
            var sequence = new MockSequence();

            //Switch to case CenterFirst

            //Setup start point
            PromptPointOptions optStart = null;
            var startPt = Point3d.Origin;
            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optStart = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", startPt));

            // Switch to center point second
            userInputMock.Setup(x => x.GetSecondMidPoint(It.IsAny<PointDrawJig.PointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            //Setup center point
            JigPromptPointOptions optCenter = null;
            var centerPt = new Point3d(0, 10, 0);
            Point3d centerPtW = centerPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondCenterPoint(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optCenter = options.Options;
                    optCenter.DefaultValue = new Point3d();
                    options.UpdatePoint(centerPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", centerPt));

            // Switch to end angle      
            userInputMock.Setup(x => x.GetThirdEndPointAfterCenterStartOrSecond(It.IsAny<PointDrawJig.PointOptions>()))
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            //Setup Angle
            JigPromptAngleOptions optAngle = null;
            double angle = 0.5;
            userInputMock.InSequence(sequence).Setup(x => x.GetAngle(It.IsAny<ArcCmd.JigPromptAngleOpt>()))
                .Callback((AngleDrawJig.AngleOptions options) =>
                {
                    optAngle = options.Options;
                    optAngle.DefaultValue = 0;
                    options.UpdateAngle(angle);
                    optAngle.BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs());
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "0.0", angle));

            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            var cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            MessageTest.ArcMessageTestForStartCenterAngle(cmd, optStart, optCenter, optAngle, startPt, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();
            OnFinish();
        }
        #endregion

        [CommandMethod("JrxCommandTest", "SmxSmxARCTESTCASESTARTCENTERLENGTH", CommandFlags.Modal)]
        public void ARCTESTCASESTARTCENTERLENGTH()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartCenterLengthCoordTest1();
                    coordTest.ArcStartCenterLengthCoordTest2();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartCenterLengthCoordTest3();
                    coordTest.ArcStartCenterLengthCoordTest4();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartCenterLengthCoordTest5();
                    coordTest.ArcStartCenterLengthCoordTest6();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartCenterLengthCoordTest7();
                    coordTest.ArcStartCenterLengthCoordTest8();
                    break;
                default:
                    return;
            }
            coordTest.LastArcId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.ArcPropertyTest(coordTest.LastArcId);

            //MessageTest
            // Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();


            //Setup Start Point
            PromptPointOptions optStart = null;
            var startPt = Point3d.Origin;
            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optStart = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", startPt));

            // Switch to center point second
            userInputMock.Setup(x => x.GetSecondMidPoint(It.IsAny<PointDrawJig.PointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            //Setup center point
            JigPromptPointOptions optCenter = null;
            var centerPt = new Point3d(0, 10, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondCenterPoint(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optCenter = options.Options;
                    optCenter.DefaultValue = new Point3d();
                    options.UpdatePoint(centerPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", centerPt));

            // Switch to  end angle      
            userInputMock.Setup(x => x.GetThirdEndPointAfterCenterStartOrSecond(It.IsAny<PointDrawJig.PointOptions>()))
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "L", Point3d.Origin));

            // Setup length
            JigPromptDistanceOptions optLength = null;
            double length = 1;
            userInputMock.InSequence(sequence).Setup(x => x.GetLength(It.IsAny<ArcCmd.JigPromptDistanceOpt>()))
                .Callback((DistanceDrawJig.DistanceOptions options) =>
                {
                    optLength = options.Options;
                    optLength.DefaultValue = 1;
                    options.UpdateDistance(length);
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "1.0", length));

            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            var cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            MessageTest.ArcMessageTestForStartCenterLength(cmd, optStart, optCenter, optLength, startPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();
            OnFinish();
        }
        #endregion

        [CommandMethod("JrxCommandTest", "SmxSmxARCTESTCASESTARTENDANGLE", CommandFlags.Modal)]
        public void ARCTESTCASESTARTENDANGLE()
        {
            OnStart();
            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartEndAngleCoordTest1();
                    coordTest.ArcStartEndAngleCoordTest2();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartEndAngleCoordTest3();
                    coordTest.ArcStartEndAngleCoordTest4();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartEndAngleCoordTest5();
                    coordTest.ArcStartEndAngleCoordTest6();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartEndAngleCoordTest7();
                    coordTest.ArcStartEndAngleCoordTest8();
                    break;
                default:
                    return;
            }
            coordTest.LastArcId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.ArcPropertyTest(coordTest.LastArcId);

            //MessageTest
            // Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            //Setup Start point
            PromptPointOptions optStart = null;
            var startPt = Point3d.Origin;
            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optStart = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", startPt));

            // Switch to end point second
            userInputMock.Setup(x => x.GetSecondMidPoint(It.IsAny<PointDrawJig.PointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "E", Point3d.Origin));

            //Setup end point
            JigPromptPointOptions optEnd = null;
            var endPt = new Point3d(0, 10, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondEndPointAfterStart(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optEnd = options.Options;
                    optEnd.DefaultValue = new Point3d();
                    options.UpdatePoint(endPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", endPt));

            // Switch to  end angle      
            userInputMock.Setup(x => x.GetThirdCenterPoint(It.IsAny<PointDrawJig.PointOptions>()))
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            JigPromptAngleOptions optAngle = null;
            double Angle = 0;
            userInputMock.InSequence(sequence).Setup(x => x.GetAngle(It.IsAny<ArcCmd.JigPromptAngleOpt>()))
                .Callback((AngleDrawJig.AngleOptions options) =>
                {
                    optAngle = options.Options;
                    optAngle.DefaultValue = 0;
                    options.UpdateAngle(Angle);
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "1.0", Angle));

            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            var cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            MessageTest.ArcMessageTestForStarEndAngle(cmd, optStart, optEnd, optAngle, startPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();
            OnFinish();
        }
        #endregion

        [CommandMethod("JrxCommandTest", "SmxSmxARCTESTCASESTARTENDDIRECTION", CommandFlags.Modal)]
        public void ARCTESTCASESTARTENDDIRECTION()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartEndDirectionCoordTest1();
                    coordTest.ArcStartEndDirectionCoordTest2();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartEndDirectionCoordTest3();
                    coordTest.ArcStartEndDirectionCoordTest4();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartEndDirectionCoordTest5();
                    coordTest.ArcStartEndDirectionCoordTest6();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartEndDirectionCoordTest7();
                    coordTest.ArcStartEndDirectionCoordTest8();
                    break;
                default:
                    return;
            }
            coordTest.LastArcId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.ArcPropertyTest(coordTest.LastArcId);

            //MessageTest
            // Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            //Setup Start Point
            PromptPointOptions optStart = null;
            var startPt = Point3d.Origin;
            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optStart = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", startPt));

            // Switch to end point second
            userInputMock.Setup(x => x.GetSecondMidPoint(It.IsAny<PointDrawJig.PointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "E", Point3d.Origin));

            // Setup end point
            JigPromptPointOptions optend = null;
            var endPt = new Point3d(0, 10, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondEndPointAfterStart(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optend = options.Options;
                    optend.DefaultValue = new Point3d();
                    options.UpdatePoint(endPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", endPt));

            // Switch to  end direction      
            userInputMock.Setup(x => x.GetThirdCenterPoint(It.IsAny<PointDrawJig.PointOptions>()))
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));

            JigPromptAngleOptions optDirection = null;
            double AngleDirection = 0;
            userInputMock.InSequence(sequence).Setup(x => x.GetDirection(It.IsAny<ArcCmd.JigPromptAngleOpt>()))
                .Callback((AngleDrawJig.AngleOptions options) =>
                {
                    optDirection = options.Options;
                    optDirection.DefaultValue = 0;
                    options.UpdateAngle(AngleDirection);
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "1.0", AngleDirection));

            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            var cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            MessageTest.ArcMessageTestForStarEndDirection(cmd, optStart, optend, optDirection, startPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();
            OnFinish();
        }
        #endregion

        [CommandMethod("JrxCommandTest", "SmxSmxARCTESTCASESTARTENDRADIUIS", CommandFlags.Modal)]
        public void ARCTESTCASESTARTENDRADIUIS()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartEndRadiusCoordTest1();
                    coordTest.ArcStartEndRadiusCoordTest2();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartEndRadiusCoordTest3();
                    coordTest.ArcStartEndRadiusCoordTest4();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartEndRadiusCoordTest5();
                    coordTest.ArcStartEndRadiusCoordTest6();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.ArcStartEndRadiusCoordTest7();
                    coordTest.ArcStartEndRadiusCoordTest8();
                    break;
                default:
                    return;
            }
            coordTest.LastArcId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.ArcPropertyTest(coordTest.LastArcId);

            //MessageTest
            // Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            //Setup start point
            PromptPointOptions optStart = null;
            var startPt = Point3d.Origin;
            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optStart = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", startPt));

            // Switch to end point second
            userInputMock.Setup(x => x.GetSecondMidPoint(It.IsAny<PointDrawJig.PointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "E", Point3d.Origin));

            JigPromptPointOptions optEnd = null;
            var endPt = new Point3d(0, 10, 0);
            Point3d endPtW = endPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondEndPointAfterStart(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optEnd = options.Options;
                    optEnd.DefaultValue = new Point3d();
                    options.UpdatePoint(endPtW);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", endPtW));

            // Switch to end radius      
            userInputMock.Setup(x => x.GetThirdCenterPoint(It.IsAny<PointDrawJig.PointOptions>()))
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "R", Point3d.Origin));

            JigPromptDistanceOptions optRadius = null;
            double radius = 30;
            userInputMock.InSequence(sequence).Setup(x => x.GetRadius(It.IsAny<ArcCmd.JigPromptDistanceOpt>()))
                .Callback((DistanceDrawJig.DistanceOptions options) =>
                {
                    optRadius = options.Options;
                    optRadius.DefaultValue = 0;
                    options.UpdateDistance(radius);
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "1.0", radius));

            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            var cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            MessageTest.ArcMessageTestForStarEndRadius(cmd, optStart, optEnd, optRadius, startPt, endPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();
            OnFinish();
        }
        #endregion

        #region Pattern11: Continue
        [CommandMethod("SCAD_COMMAND_TEST", "ARCTESTCASECONTINUE", CommandFlags.Modal)]
        public void ARCTESTCASEContinue()
        {
            OnStart();
            //Coordinate TEST
            var coordTest = new CoordTest();
            OptionsContinue();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);                    
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    //coordTest.ArcContinueTest1();
                    break;
                case "2": //UCS2
                   changeCoordinateSystem(_resValueSet.StringResult);
                    //coordTest.ArcContinueTest1();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    //coordTest.ArcContinueTest1();
                    break;
                default:
                    return;
            }

            switch (_resValueOptContinue.StringResult)
            {
                case "1":
                    coordTest.ArcContinueCoordTest1();
                    break;
                case "2":
                    coordTest.ArcContinueCoordTest2();
                    break;
                case "3":
                    coordTest.ArcContinueCoordTest3();
                    break;
                case "4":
                    coordTest.ArcContinueCoordTest4();
                    break;
                default:
                    return;
            }
            //Propertytest
            PropertyTest.ArcPropertyTest(coordTest.LastArcId);


            //MessageTest
            // Create Mock

            Line line = new Line(new Point3d(0, 0, 0), new Point3d(-10, -10, 0));
            line.TransformBy(CoordConverter.UcsToWcs());
            coordTest.AddEntityIntoDatabase(line);
            Entity curEnt = coordTest.GetCurEnt();
            (Point3d pt1, Point3d pt2, string key) = coordTest.GetPointFromEntity(curEnt);
            var userInputMock = new Mock<ArcCmd.ArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.None, "", Point3d.Origin));

            Point3d endPt = new Point3d(10, 0, 0);
            endPt = endPt.TransformBy(CoordConverter.UcsToWcs());
            JigPromptPointOptions optEnd = null;        // for PromptOptions Test
            userInputMock.InSequence(sequence).Setup(x => x.GetEndPointContinue(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optEnd = options.Options;
                    optEnd.DefaultValue = new Point3d();
                    options.UpdatePoint(endPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", endPt));

            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // Execute Arc
            ArcCmd cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            MessageTest.ArcMessageTestForContinue(cmd, optEnd, pt2);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            OnFinish();
        }
        #endregion
    }
}
