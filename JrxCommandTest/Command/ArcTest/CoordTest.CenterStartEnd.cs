﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.ArcCmd;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exception = System.Exception;
using System.Reflection;

namespace JrxCadTest.Command.ArcTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartEndTest1", CommandFlags.Modal)]
        public void ArcCenterStartEndTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                Point3d pt3 = new Point3d(60, 50, pt1.Z);
                DoTestCenterStartEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartEndTest2", CommandFlags.Modal)]
        public void ArcCenterStartEndTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                Point3d pt3 = new Point3d(20, 30, pt1.Z);
                DoTestCenterStartEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartEndTest3", CommandFlags.Modal)]
        public void ArcCenterStartEndTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                Point3d pt3 = new Point3d(60, 50, pt1.Z);
                DoTestCenterStartEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartEndTest4", CommandFlags.Modal)]
        public void ArcCenterStartEndTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                Point3d pt3 = new Point3d(20, 30, pt1.Z);
                DoTestCenterStartEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartEndTest5", CommandFlags.Modal)]
        public void ArcCenterStartEndTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                Point3d pt3 = new Point3d(60, 50, pt1.Z);
                DoTestCenterStartEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartEndTest6", CommandFlags.Modal)]
        public void ArcCenterStartEndTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                Point3d pt3 = new Point3d(20, 30, pt1.Z);
                DoTestThreePoint(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartEndTest7", CommandFlags.Modal)]
        public void ArcCenterStartEndTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                Point3d pt3 = new Point3d(60, 50, pt1.Z);
                DoTestCenterStartEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxSmxArcCenterStartEndTest8", CommandFlags.Modal)]
        public void ArcCenterStartEndTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(-10, -10, 0);
                Point3d pt2 = new Point3d(100, -50, pt1.Z);
                Point3d pt3 = new Point3d(20, 30, pt1.Z);
                DoTestCenterStartEnd(pt1, pt2, pt3);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }


        private void DoTestCenterStartEnd(Point3d centerPt, Point3d startPt, Point3d endPt)
        {
            ExpectedResult temp = new ArcCaculator(centerPt, startPt, endPt).CalculateValueCenterStartend();
            var objIds = DoTestArcCenterStartEnd(centerPt, startPt, endPt);
            VerifyArc(objIds, Expected);
        }

        private ObjectIdCollection DoTestArcCenterStartEnd(Point3d centerPt, Point3d startPt, Point3d endPt)
        {
            //// Create Mock
            var userInputMock = new Mock<ArcCmd.IArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetFirstCenterPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

            startPt = startPt.TransformBy(CoordConverter.UcsToWcs());
            JigPromptPointOptions optSecond = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetSecondStartPoint(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.UpdatePoint(startPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", startPt));

            JigPromptPointOptions optEnd = null;
            endPt = endPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetThirdEndPointAfterCenterStartOrSecond(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optEnd = options.Options;
                    optEnd.DefaultValue = new Point3d();
                    options.UpdatePoint(endPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", endPt));


            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // Execute Arc
            ArcCmd cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedArcIds;
        }
    }
}
