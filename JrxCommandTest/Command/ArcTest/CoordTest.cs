﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;

#endif
using System;
using JrxCad.Helpers;
using JrxCad.Utility;
using Moq;
using JrxCad.Command.ArcCmd;
using JrxCadTest.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.ArcTest
{
    public partial class CoordTest
    {
        public ObjectId LastArcId { get; set; }

        private struct ExpectedResult
        {
            public Point3d Start;
            public Point3d Center;
            public double TotalAngle;
        }

        private void VerifyArc(ObjectIdCollection ActualIds, ExpectedResult expected)
        {
            LastArcId = ActualIds[0];
            using (var tr = Util.StartTransaction())
            using (var arc = tr.GetObject<Arc>(ActualIds[0], OpenMode.ForRead))
            {
                Point3d tempStart = arc.StartPoint.TransformBy(CoordConverter.WcsToUcs());
                //tempStart = Point3dHelper.RoundCoordOfPoint(ref tempStart);
                tempStart.Is(expected.Start);
                Point3d tempCenter = arc.Center.TransformBy(CoordConverter.WcsToUcs());
                //Point3dHelper.RoundCoordOfPoint(ref tempCenter);
                tempCenter.Is(expected.Center);
                arc.TotalAngle.ToTestVal().Is(expected.TotalAngle.ToTestVal());
            }
        }
        private class ArcCaculator
        {
            private Point3d _center;
            private double _radius;
            private Point3d _firstPt, _secondPt, _endPt;
            private double _angleOrDist;
            private double _startAngle, _endAngle;
            private string _key;


            public ArcCaculator(Point3d firstPt, Point3d secontPt, double angleordist)
            {

                (_firstPt, _secondPt, _angleOrDist) = (firstPt, secontPt, angleordist);

            }
            public ArcCaculator(Point3d startPt, Point3d secondPt, Point3d endPt)
            {
                (_firstPt, _secondPt, _endPt) = (startPt, secondPt, endPt);
            }
            public ArcCaculator(Point3d startPt, Point3d secondPt, Point3d endPt, string key)
            {
                (_firstPt, _secondPt, _endPt, _key) = (startPt, secondPt, endPt,key);
            }

            private ExpectedResult Calculate()
            {
                Arc temp = new Arc(_center, _radius, _startAngle, _endAngle);
                ExpectedResult Expect = new ExpectedResult();
                Expect.Center = temp.Center;
                Expect.Start = temp.StartPoint;
                Expect.TotalAngle = temp.TotalAngle;
                temp.Dispose();
                temp = null;
                return Expect;
            }

            public ExpectedResult CalculateValueThreePoint()
            {
                var angleCheck = (_secondPt - _firstPt).GetAngleTo(_endPt - _firstPt, Vector3d.ZAxis);
                _center = Point3dHelper.GetCenterFromThreePoints(_firstPt, _secondPt, _endPt);
                _startAngle = Vector3d.XAxis.GetAngleTo(_firstPt - _center, Vector3d.ZAxis);
                _endAngle = Vector3d.XAxis.GetAngleTo(_endPt - _center, Vector3d.ZAxis);
                if (angleCheck > Math.PI)
                {
                    (_startAngle, _endAngle) = (_endAngle, _startAngle);
                }
                _radius = _center.DistanceTo(_firstPt);

                return Calculate();

            }

            public ExpectedResult CalculateValueCenterStartend()
            {
                _center = _firstPt;
                _startAngle = Vector3d.XAxis.GetAngleTo(_secondPt - _center, Vector3d.ZAxis);
                _endAngle = Vector3d.XAxis.GetAngleTo(_endPt - _center, Vector3d.ZAxis);
                _radius = _center.DistanceTo(_secondPt);
                return Calculate();

            }

            public ExpectedResult CalculateValueCenterStartAngle()
            {
                _center = _firstPt;
                _startAngle = Vector3d.XAxis.GetAngleTo(_secondPt - _center, Vector3d.ZAxis);
                _endAngle = _startAngle + _angleOrDist;
                if(_angleOrDist<0)
                {
                    (_startAngle, _endAngle) = (_endAngle, _startAngle);
                }    
                _radius = _firstPt.DistanceTo(_secondPt);
                return Calculate();
            }

            public ExpectedResult CalculateValueCenterStartLength()
            {
                _center = _firstPt;
                double angleAtCenter = 2 * System.Math.Asin(_angleOrDist / 2 / _secondPt.DistanceTo(_center));
                _startAngle = Vector3d.XAxis.GetAngleTo(_secondPt - _center, Vector3d.ZAxis);
                if (_secondPt.Y > _center.Y)
                {
                    _endAngle = angleAtCenter + _startAngle;
                }
                else
                {
                    _endAngle = angleAtCenter - 2 * Math.PI + _startAngle;
                }
                _radius = _center.DistanceTo(_secondPt);
                return Calculate();

            }

            public ExpectedResult CalculateValueStartCenterEnd()
            {
                _center = _secondPt;
                _startAngle = Vector3d.XAxis.GetAngleTo(_firstPt - _center, Vector3d.ZAxis);
                _endAngle = Vector3d.XAxis.GetAngleTo(_endPt - _center, Vector3d.ZAxis);
                _radius = _center.DistanceTo(_firstPt);
                return Calculate();

            }

            public ExpectedResult CalculateValueStartCenterAngle()
            {
                _center = _secondPt;
                _startAngle = Vector3d.XAxis.GetAngleTo(_firstPt - _center, Vector3d.ZAxis);
                _endAngle = _startAngle + _angleOrDist;
                _radius = _firstPt.DistanceTo(_secondPt);
                return Calculate();

            }

            public ExpectedResult CalculateValueStartCenterLength()
            {
                _center = _secondPt;
                double angleAtCenter = 2 * System.Math.Asin(_angleOrDist / 2 / _firstPt.DistanceTo(_center));
                _startAngle = Vector3d.XAxis.GetAngleTo(_firstPt - _center, Vector3d.ZAxis);
                if (_firstPt.Y > _center.Y)
                {
                    _endAngle = angleAtCenter + _startAngle;
                }
                else
                {
                    _endAngle = angleAtCenter - 2 * Math.PI + _startAngle;
                }
                _radius = _center.DistanceTo(_firstPt);
                return Calculate();

            }
            public ExpectedResult CalculateValueStartEndAngle()
            {
                double temp = 2 * Math.Sin(_angleOrDist / 2);
                double radius = _secondPt.DistanceTo(_firstPt) / temp;
                _center = Point3dHelper.GetCenterFrom(_firstPt, _secondPt, radius, _angleOrDist > Math.PI /*reverse or not*/);
                _radius = _center.DistanceTo(_firstPt);
                _startAngle = Vector3d.XAxis.GetAngleTo(_firstPt - _center, Vector3d.ZAxis);
                _endAngle = Vector3d.XAxis.GetAngleTo(_secondPt - _center, Vector3d.ZAxis);
                return Calculate();

            }

            public ExpectedResult CalculateValueStartEndDirection()
            {
                Vector3d startEndVec = _secondPt - _firstPt;
                Vector3d directionVec = Vector3d.XAxis.RotateBy(_angleOrDist, Vector3d.ZAxis);
                double angle1 = directionVec.GetAngleTo(startEndVec, Vector3d.ZAxis);

                //angle2 is the smaller angle between the two vectors, and always from 0 to PI
                double angle2 = Math.Min(angle1, 2 * Math.PI - angle1);
                //angleAtCenter (included Angle) is two times larger than angle2, and always from 0 to 2*PI
                double angleAtCenter = 2 * Math.Min(angle2, Math.PI - angle2);
                double temp = Math.Sin(angleAtCenter / 2);
                double radius = _secondPt.DistanceTo(_firstPt) / 2 / temp;
                if (angle1 > Math.PI)
                {
                    _center = Point3dHelper.GetCenterFrom(_firstPt, _secondPt, radius, angle2 < Math.PI / 2/*reverse or not*/);
                }
                else
                {
                    _center = Point3dHelper.GetCenterFrom(_firstPt, _secondPt, radius, angle2 > Math.PI / 2/*reverse or not*/);
                }
                _radius = _center.DistanceTo(_firstPt);
                _startAngle = Vector3d.XAxis.GetAngleTo(_firstPt - _center, Vector3d.ZAxis);
                _endAngle = Vector3d.XAxis.GetAngleTo(_secondPt - _center, Vector3d.ZAxis);

                if (angle1 > Math.PI)
                {
                    (_startAngle, _endAngle) = (_endAngle, _startAngle);
                }
                return Calculate();
            }

            public ExpectedResult CalculateValueStartEndRadius()
            {
                _center = Point3dHelper.GetCenterFrom(_firstPt, _secondPt, _angleOrDist, _angleOrDist < 0);
                _radius = Math.Abs(_angleOrDist);
                _startAngle = Vector3d.XAxis.GetAngleTo(_firstPt - _center, Vector3d.ZAxis);
                _endAngle = Vector3d.XAxis.GetAngleTo(_secondPt - _center, Vector3d.ZAxis);
                return Calculate();
            }
            public ExpectedResult CalculateValueContinue()
            {
                Vector3d startEndVec = _endPt - _secondPt;
                Vector3d tempDicrection = _firstPt - _secondPt;
                Vector3d directionVec = tempDicrection.RotateBy(-1*Math.PI / 2, Vector3d.ZAxis);
                if (_key== "arcEndToStart")
                {
                    directionVec = tempDicrection.RotateBy(1 * Math.PI / 2, Vector3d.ZAxis);
                }
                if(_key=="line")
                {
                    directionVec = _secondPt - _firstPt;
                }    
                double angle1 = directionVec.GetAngleTo(startEndVec, Vector3d.ZAxis);

                //angle2 is the smaller angle between the two vectors, and always from 0 to PI
                double angle2 = Math.Min(angle1, 2 * Math.PI - angle1);
                //angleAtCenter (included Angle) is two times larger than angle2, and always from 0 to 2*PI
                double angleAtCenter = 2 * Math.Min(angle2, Math.PI - angle2);
                double temp = Math.Sin(angleAtCenter / 2);

                double radius = _endPt.DistanceTo(_secondPt) / 2 / temp;

                if (angle1 > Math.PI)
                {
                    _center = Point3dHelper.GetCenterFrom(_secondPt, _endPt, radius, angle2 < Math.PI / 2/*reverse or not*/);
                }
                else
                {
                    _center = Point3dHelper.GetCenterFrom(_secondPt, _endPt, radius, angle2 > Math.PI / 2/*reverse or not*/);
                }

                _radius = radius;
                //_center = _center.TransformBy(CoordConverter.UcsToWcs());

                _startAngle = Vector3d.XAxis.GetAngleTo(_secondPt - _center, Vector3d.ZAxis);
                _endAngle = Vector3d.XAxis.GetAngleTo(_endPt - _center, Vector3d.ZAxis);


                if (angle1 > Math.PI)
                {
                    (_startAngle, _endAngle) = (_endAngle, _startAngle);
                }
                return Calculate();

            }
        }
    }
}
