﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using GrxCAD.Colors;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Colors;

#endif
using System;
using Moq;
using Scmd.Command.ArcCmd;
using Scmd.Helpers;
using Scmd.Utility;
using ScmdTest.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exception = System.Exception;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting.Logging;


namespace ScmdTest.Command.ArcTest
{
    public partial class CoordTest
    {

        [CommandMethod("SCAD_COMMAND_TEST", "ArcContinueCoordTest1", CommandFlags.Modal)]
        public void ArcContinueCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                Line line = new Line(new Point3d(10, 10, 10), new Point3d(50, 50, 10));
                line.TransformBy(CoordConverter.UcsToWcs());
                AddEntityIntoDatabase(line);
                line.Dispose();
                Arc arc = new Arc(new Point3d(10, 20, 30), 50, 30 * Math.PI / 180, 100 * Math.PI / 180);
                arc.TransformBy(CoordConverter.UcsToWcs());
                arc.Color = Color.FromRgb(255, 0, 0);
                AddEntityIntoDatabase(arc);
                arc.Dispose();
                Entity curEnt = GetCurEnt();

                Point3d pt1 = new Point3d(), pt2 = new Point3d();
                string key = "";
                if (curEnt == null)
                {
                    Util.Editor().WriteMessage("\nNo line or arc to continue.\n*Invalid*");
                    return;
                }
                else
                    (pt1, pt2, key) = GetPointFromEntity(curEnt);

                if ((pt1.Z - pt2.Z).IsZero() == false)
                {
                    Util.Editor().WriteMessage("\nqThe continue direction is not UCS parallel\n*Invalid*");
                    return;
                }
                Point3d pt3 = new Point3d(100, 20, pt1.Z);
                DoTestContinue(pt1, pt2, pt3, key);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");


            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("SCAD_COMMAND_TEST", "ArcContinueCoordTest2", CommandFlags.Modal)]
        public void ArcContinueCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                Arc arc = new Arc(new Point3d(10, 20, 30), 50, 30 * Math.PI / 180, 100 * Math.PI / 180);
                arc.TransformBy(CoordConverter.UcsToWcs());
                AddEntityIntoDatabase(arc);
                arc.Dispose();
                Line line = new Line(new Point3d(10, 10, 10), new Point3d(50, 50, 10));
                line.TransformBy(CoordConverter.UcsToWcs());
                line.Color = Color.FromRgb(255, 0, 0);
                AddEntityIntoDatabase(line);
                line.Dispose();
                Entity curEnt = GetCurEnt();
                Point3d pt1 = new Point3d(), pt2 = new Point3d();
                string key = "";
                if (curEnt == null)
                {
                    Util.Editor().WriteMessage("\nNo line or arc to continue.\n*Invalid*");
                    return;
                }
                else
                    (pt1, pt2, key) = GetPointFromEntity(curEnt);

                if ((pt1.Z - pt2.Z).IsZero() == false)
                {
                    Util.Editor().WriteMessage("\nqThe continue direction is not UCS parallel\n*Invalid*");
                    return;
                }
                Point3d pt3 = new Point3d(100, 20, pt1.Z);
                DoTestContinue(pt1, pt2, pt3, key);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");

            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
        [CommandMethod("SCAD_COMMAND_TEST", "ArcContinueCoordTest3", CommandFlags.Modal)]
        public void ArcContinueCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                Arc arc = new Arc(new Point3d(10, 20, 30), 50, 30 * Math.PI / 180, 100 * Math.PI / 180);
                arc.TransformBy(CoordConverter.UcsToWcs());
                AddEntityIntoDatabase(arc);
                arc.Dispose();
                Line line = new Line(new Point3d(10, 10, 10), new Point3d(50, 50, 10));
                line.TransformBy(CoordConverter.UcsToWcs());
                AddEntityIntoDatabase(line);
                line.Dispose();
                Circle circle = new Circle(new Point3d(30, 80, 30), Vector3d.ZAxis, 50);
                circle.Color = Color.FromRgb(255, 0, 0);
                AddEntityIntoDatabase(circle);
                circle.Dispose();

                Entity curEnt = GetCurEnt();
                Point3d pt1 = new Point3d(), pt2 = new Point3d();
                string key = "";

                (pt1, pt2, key) = GetPointFromEntity(curEnt);

                Point3d pt3 = new Point3d(100, 20, pt1.Z);
                DoTestContinue(pt1, pt2, pt3, key);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("SCAD_COMMAND_TEST", "ArcContinueCoordTest4", CommandFlags.Modal)]
        public void ArcContinueCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                Arc arc = new Arc(new Point3d(10, 20, 30), 50, 30 * Math.PI / 180, 100 * Math.PI / 180);
                arc.TransformBy(CoordConverter.UcsToWcs());
                AddEntityIntoDatabase(arc);
                arc.Dispose();
                Line line = new Line(new Point3d(10, 10, 10), new Point3d(50, 50, 10));
                line.TransformBy(CoordConverter.UcsToWcs());
                AddEntityIntoDatabase(line);
                line.Dispose();
                Circle circle = new Circle(new Point3d(30, 80, 30), Vector3d.ZAxis, 50);
                circle.TransformBy(CoordConverter.UcsToWcs());
                AddEntityIntoDatabase(circle);
                circle.Dispose();
                Polyline pline = new Polyline(4);
                var vertex = new Point2d(30, 30);
                pline.AddVertexAt(0, vertex, 0.0, 0.0, 0.0);
                vertex = new Point2d(20, 10);
                pline.AddVertexAt(1, vertex, 0.0, 0.0, 0.0);
                vertex = new Point2d(60, 50);
                pline.AddVertexAt(2, vertex, -1.0, 0.0, 0.0);
                vertex = new Point2d(70, 60);
                pline.AddVertexAt(3, vertex, 0.0, 0.0, 0.0);
                pline.Color = Color.FromRgb(255, 0, 0);
                pline.TransformBy(CoordConverter.UcsToWcs());
                AddEntityIntoDatabase(pline);
                pline.Dispose();

                Entity curEnt = GetCurEnt();
                Point3d pt1 = new Point3d(), pt2 = new Point3d();
                string key = "";

                (pt1, pt2, key) = GetPointFromEntity(curEnt);
                Point3d pt3 = new Point3d(100, 20, pt1.Z);
                DoTestContinue(pt1, pt2, pt3, key);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestContinue(Point3d firstPt, Point3d secondPt, Point3d endPt, string key)
        {
            ExpectedResult Expected = new ArcCaculator(firstPt, secondPt, endPt, key).CalculateValueContinue();
            var objIds = DoTestArcContinue(secondPt, endPt);
            VerifyArc(objIds, Expected);
        }

        private ObjectIdCollection DoTestArcContinue(Point3d secondPt, Point3d endPt)
        {
            //// Create Mock
            var userInputMock = new Mock<ArcCmd.ArcUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            userInputMock.Setup(x => x.GetFirstStartPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.None, "", Point3d.Origin));

            endPt = endPt.TransformBy(CoordConverter.UcsToWcs());
            JigPromptPointOptions optEnd = null;        // for PromptOptions Test
            userInputMock.InSequence(sequence).Setup(x => x.GetEndPointContinue(It.IsAny<ArcCmd.JigPromptPointOpt>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    optEnd = options.Options;
                    optEnd.DefaultValue = new Point3d();
                    options.UpdatePoint(endPt);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", endPt));

            // Setup AppendedArc
            var appendedArcIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendArcId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedArcIds.Add(id));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // Execute Arc
            ArcCmd cmd = new ArcCmd(userInputMock.Object);
            cmd.OnCommand();

            // MessageTest.ArcMessageTestForContinue(cmd, optEnd, secondPt);
            return appendedArcIds;
        }



        [DllImport(
#if _AutoCAD_
              "AcadUtils",
#elif _IJCAD_
              "GcadUtils",
#endif

         CallingConvention = CallingConvention.Cdecl,
         CharSet = CharSet.Unicode,
          EntryPoint = "GetObjectIds")]

        private static extern int GetObjectIds(out IntPtr objIds);
        private ObjectIdCollection OutputObjectIds()
        {
            try
            {
                var count = GetObjectIds(out var pointer);
                var ids = new ObjectIdCollection();
                var arr = new IntPtr[count];
                Marshal.Copy(pointer, arr, 0, count);
                Marshal.FreeCoTaskMem(pointer);
                for (var i = 0; i < count; i++)
                {
                    var id = new ObjectId(arr[i]);
                    ids.Add(id);
                }
                return ids;
            }
            catch (Exception)
            {
                //Logger.ErrorShow(ex);
                return null;
            }
        }


        public Entity GetCurEnt()
        {
            var ids = OutputObjectIds();
            Entity curEnt = null;
            for (int i = ids.Count; i > 0; i--)
            {
                using (Transaction icTrans = Util.StartTransaction())
                using (Entity ent = icTrans.GetObject(ids[i - 1], OpenMode.ForRead, true) as Entity)
                {
                    if (ent is Polyline || ent is Line || ent is Arc)
                    {
                        curEnt = icTrans.GetObject(ids[i - 1], OpenMode.ForRead, true) as Entity;
                        return curEnt;
                    }
                }
            }
            return curEnt;
        }

        public (Point3d, Point3d, string) GetPointFromEntity(Entity curEnt)
        {
            Point3d pt1 = new Point3d();
            Point3d pt2 = new Point3d();
            string key = "";

            if (curEnt is Line)
            {
                key = "line";
                pt1 = ((Line)curEnt).StartPoint;
                pt2 = ((Line)curEnt).EndPoint;

            }
            if (curEnt is Arc)
            {
                key = "arcStartToEnd";
                pt1 = ((Arc)curEnt).Center;
                pt2 = ((Arc)curEnt).EndPoint;
            }
            if (curEnt is Polyline)
            {

                var oldSegment = new DBObjectCollection();
                ((Polyline)curEnt).Explode(oldSegment);
                Entity SegmentEnd = oldSegment[oldSegment.Count - 1] as Entity;

                if (SegmentEnd is Line)
                {
                    key = "line";
                    pt1 = ((Line)SegmentEnd).StartPoint;
                    pt2 = ((Line)SegmentEnd).EndPoint;
                }
                else if (SegmentEnd is Arc)
                {
                    key = "arcStartToEnd";
                    var endPointArc = ((Arc)SegmentEnd).EndPoint;
                    pt1 = ((Arc)SegmentEnd).Center;
                    pt2 = ((Polyline)curEnt).EndPoint;

                    if ((((Arc)SegmentEnd).EndPoint - pt2).IsZeroLength())
                    {
                        key = "arcStartToEnd";
                    }
                    else
                        key = "arcEndToStart";
                }

            }
            pt1 = pt1.TransformBy(CoordConverter.WcsToUcs());
            pt2 = pt2.TransformBy(CoordConverter.WcsToUcs());
            return (pt1, pt2, key);
        }

        public void AddEntityIntoDatabase(Entity ent)
        {
            using (var tr = Util.StartTransaction())
            {
                tr.AddNewlyCreatedDBObject(ent, true, tr.CurrentSpace());
                AppendArcId(ent.Id);
                tr.Commit();
            }
        }
        public void AppendArcId(ObjectId id)
        {
        }
    }
}
