﻿#if _IJCAD_
using GrxCAD.Runtime;
#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
#endif

using JrxCad.Utility;
using JrxCadTest.Utility;
using System.Reflection;

namespace JrxCadTest.Command.XopenCmdTest
{
    public class Test : BaseTest
    {
        private const string EXCEPTION_INFO_STRING = "Method Name: {0}, Exception: {1} ";
        private const string PROCESS_UNIT_TEST_NAME = "XopenCmdTest{0}";
        private string _commandTestName = string.Empty;

        /// <summary>
        /// start tesst
        /// </summary>
        public override void OnStart()
        {
            Util.Editor().WriteMessage($"\n{_commandTestName.ToUpper()} START!");
        }

        /// <summary>
        /// Finish test
        /// </summary>
        public override void OnFinish()
        {
            Util.Editor().WriteMessage($"\n{_commandTestName.ToUpper()} Finish!");
        }

        /// <summary>
        /// Write error message
        /// </summary>
        private void WriteErrorMessage(string methodName)
        {
            Util.Editor().WriteMessage($"\n{methodName} ERROR!!!!");
        }

        /// <summary>
        /// Set process command test name
        /// </summary>
        private void SetCommandTest(string numberTest)
        {
            _commandTestName = string.Format(PROCESS_UNIT_TEST_NAME, numberTest);
        }

#if _IJCAD_
        ///For debug in IJCAD, set command flag to CommandFlags.Session instead of CommandFlags.Modal to prevent crash.
        ///Refer to: https://tknmt.hatenablog.com/entry/2019/05/29/110659
        [CommandMethod("JrxCommandTest", "XOpenCommandTest",
        CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.NoMultiple |
        CommandFlags.NoNewStack | CommandFlags.Interruptible)]
#elif _AutoCAD_
        [CommandMethod("JrxCommandTest", "XOpenCommandTest",
       CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.NoMultiple |
       CommandFlags.NoNewStack | CommandFlags.Interruptible)]
#endif
        public void XopenCommandTest()
        {
            try
            {
                var xopenCmdTest = new XopenCmdTest();

                // UI select (select xref:)
                ExecuteCommandUISelect(xopenCmdTest);

                // PICKFIRST = 0
                ExecuteCommandPickFirstEqual0(xopenCmdTest);

                // PICKFIRST = 1
                ExecutecCommandPickFirstEqual1(xopenCmdTest);

                // Open Drawing in Reference file dialog 
                ExecuteCommandOpenFileInDialog(xopenCmdTest);

                // Open Dialog and selected item
                ExecuteOpenDialogAndSelectedItem(xopenCmdTest);

                // Zoom to Xref item
                ExecuteOpenDialogAndZoomToXref(xopenCmdTest);

                // execute command for other file tesst
                ExecuteCommandTestForOtherFile(xopenCmdTest);

                // close other file, after unit test
                xopenCmdTest.SaveActiveDrawing("XOPEN-01", "XOPEN-02", "XOPEN-01_Parent");
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodInfo.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// process UI select
        /// </summary>
        private void ExecuteCommandUISelect(XopenCmdTest xopenCmdTest)
        {
            #region Object count = 0 => Select Xref:"
            SetCommandTest("01");
            OnStart();

            xopenCmdTest.ExecuteCommandTest01();

            OnFinish();
            #endregion

            #region xref count = 0 => Object is not an Xref
            SetCommandTest("02");
            OnStart();

            xopenCmdTest.ExecuteCommandTest02();
            OnFinish();
            #endregion

            #region xref = 1 => open drawing
            SetCommandTest("03");
            OnStart();

            xopenCmdTest.ExecuteCommandTest03();
            OnFinish();
            #endregion

            #region xref count > 1 	& Childあり => open Dialog
            SetCommandTest("04");
            OnStart();

            xopenCmdTest.ExecuteCommandTest04();
            OnFinish();
            #endregion

            #region xref count > 1 & Childなし => open drawing
            SetCommandTest("05");
            OnStart();

            xopenCmdTest.ExecuteCommandTest05();
            OnFinish();
            #endregion

            #region selected xref: => press Enter keyword => End
            SetCommandTest("06");
            OnStart();

            xopenCmdTest.ExecuteCommandTest06();
            OnFinish();
            #endregion

            #region selected xref: => press Esc => "Cancel*" => End
            SetCommandTest("07");
            xopenCmdTest.ExecuteCommandTest07();
            OnFinish();
            #endregion
        }

        /// <summary>
        /// process PICKFIRST = 0
        /// </summary>
        private void ExecuteCommandPickFirstEqual0(XopenCmdTest xopenCmdTest)
        {
            #region select object => xopen cmd => xref count > 1 & Childあり,  expected: select xref: 
            SetCommandTest("08");
            OnStart();

            xopenCmdTest.ExecuteCommandTest08();
            OnFinish();
            #endregion

            #region select object => xopen cmd => xref count > 1 & Childなし, expected: select xref: 
            SetCommandTest("09");
            OnStart();

            xopenCmdTest.ExecuteCommandTest09();
            OnFinish();
            #endregion

            #region select object => xopen cmd => xref count = 1, expected: select xref: 
            SetCommandTest("10");
            OnStart();

            xopenCmdTest.ExecuteCommandTest10();
            OnFinish();
            #endregion
        }

        /// <summary>
        /// process PICKFIRST = 1
        /// </summary>
        private void ExecutecCommandPickFirstEqual1(XopenCmdTest xopenCmdTest)
        {
            #region select object => xopen cmd => object count = 0, expected: select xref:
            SetCommandTest("11");
            OnStart();

            xopenCmdTest.ExecuteCommandTest11();

            OnFinish();
            #endregion

            #region select object => xopen cmd => xref count = 0, expected: select xref:
            SetCommandTest("12");
            OnStart();

            xopenCmdTest.ExecuteCommandTest12();
            OnFinish();
            #endregion


            #region select object => xopen cmd => xref count = 1, expected: open drawing file
            SetCommandTest( "13");
            OnStart();

            xopenCmdTest.ExecuteCommandTest13();
            OnFinish();
            #endregion

            #region select object => xopen cmd => xref count > 1, expected: open dialog file.
            SetCommandTest( "14");
            OnStart();

            xopenCmdTest.ExecuteCommandTest14();
            OnFinish();
            #endregion
        }

        /// <summary>
        /// open drawing file in Dialog
        /// </summary>
        private void ExecuteCommandOpenFileInDialog(XopenCmdTest xopenCmdTest)
        {
            #region Open Drawing => A.未Open図面 => A.dwg Open	⇒	カレント図面=A.dwg
            SetCommandTest( "15");
            OnStart();

            xopenCmdTest.ExecuteCommandTest15();
            OnFinish();
            #endregion

            #region Open Drawing⇒	B.Open済み	Openしない	⇒	カレント図面=B.dwg
            SetCommandTest( "16");
            OnStart();

            xopenCmdTest.ExecuteCommandTest16();
            OnFinish();
            #endregion

            #region Open Drawing ⇒	C.ReadOnly図面	⇒	Yes	⇒	C.dwg Open	⇒	カレント図面=C.dwg
            SetCommandTest( "17");
            OnStart();

            xopenCmdTest.ExecuteCommandTest17();
            OnFinish();
            #endregion

            #region  #region Open Drawing ⇒	C.ReadOnly図面	⇒	No ⇒	Openしない⇒	カレント図面変更なし
            SetCommandTest( "18");
            OnStart();

            xopenCmdTest.ExecuteCommandTest18();
            OnFinish();
            #endregion

            #region Open Drawing ⇒	A.未Open＆B.Open済み A.dwg Open	⇒	カレント図面＝A.dwg	
            SetCommandTest( "19");
            OnStart();

            xopenCmdTest.ExecuteCommandTest19();
            OnFinish();
            #endregion

            #region Open Drawing⇒	A.未Open＆B.Open済み＆C.ReadOnly図面  ⇒	Yes	⇒	A.dwg & C.dwg Open ⇒	カレント図面＝最後に開いた図面 
            SetCommandTest( "20");
            OnStart();

            xopenCmdTest.ExecuteCommandTest20();
            OnFinish();
            #endregion

            #region Open Drawing⇒	A.未Open＆B.Open済み＆C.ReadOnly図面  ⇒	No	⇒	A.dwg Open	⇒	カレント図面=A.dwg
            SetCommandTest( "21");
            OnStart();

            xopenCmdTest.ExecuteCommandTest21();
            OnFinish();
            #endregion
        }

        /// <summary>
        /// open reference file dialog and selected item
        /// </summary>
        private void ExecuteOpenDialogAndSelectedItem(XopenCmdTest xopenCmdTest)
        {
            #region open Dialog, expected: first xref selected
            SetCommandTest( "22");
            OnStart();

            xopenCmdTest.ExecuteCommandTest22();
            OnFinish();
            #endregion

            #region open dialog, expected: child xref selected
            SetCommandTest( "23");
            OnStart();

            xopenCmdTest.ExecuteCommandTest23();
            OnFinish();
            #endregion

            #region open dialog, expected: multi xref selected
            SetCommandTest( "24");
            OnStart();

            xopenCmdTest.ExecuteCommandTest24();
            OnFinish();
            #endregion

            #region open dialog, expected: parent node and children node are selected
            SetCommandTest( "25");
            OnStart();

            xopenCmdTest.ExecuteCommandTest25();
            OnFinish();
            #endregion
        }

        /// <summary>
        /// Zoom to xref item
        /// </summary>
        private void ExecuteOpenDialogAndZoomToXref(XopenCmdTest xopenCmdTest)
        {
            #region zoom to parent node
            SetCommandTest( "26");
            OnStart();

            xopenCmdTest.ExecuteCommandTest26();
            OnFinish();
            #endregion

            #region zoom to child node
            SetCommandTest( "27");
            OnStart();

            xopenCmdTest.ExecuteCommandTest27();
            OnFinish();
            #endregion

            #region zoom to parent node and child node
            SetCommandTest( "28");
            OnStart();

            xopenCmdTest.ExecuteCommandTest28();
            OnFinish();
            #endregion
        }

        /// <summary>
        ///  Execute test in XOPEN-02.dwg, XOPEN-01-parent.dwg
        /// </summary>
        private void ExecuteCommandTestForOtherFile(XopenCmdTest xopenCmdTest)
        {
            #region  Open xref item XOPEN-02.dwg (case 1)		
            SetCommandTest( "29");
            OnStart();

            xopenCmdTest.ExecuteCommandTest29();
            OnFinish();
            #endregion

            #region  Open xref item XOPEN-02.dwg (case 2)		
            SetCommandTest( "30");
            OnStart();

            xopenCmdTest.ExecuteCommandTest30();
            OnFinish();
            #endregion

            #region  Open xref item XOPEN-02.dwg (case 3)		
            SetCommandTest( "31");
            OnStart();

            xopenCmdTest.ExecuteCommandTest31();
            OnFinish();
            #endregion

            #region  Open xref item XOPEN-02.dwg (case 4)		
            SetCommandTest( "32");
            OnStart();

            xopenCmdTest.ExecuteCommandTest32();
            OnFinish();
            #endregion

            #region  Open xref item XOPEN-02.dwg (case 1)		
            SetCommandTest( "33");
            OnStart();

            xopenCmdTest.ExecuteCommandTest33();
            OnFinish();
            #endregion

            #region  Open xref item XOPEN-02.dwg (case 1)		
            SetCommandTest( "34");
            OnStart();

            xopenCmdTest.ExecuteCommandTest34();
            OnFinish();
            #endregion
        }
    }
}
