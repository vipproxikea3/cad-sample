﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using CADApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.Geometry;
using GrxCAD.ApplicationServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.ApplicationServices;
#endif

using Moq;
using JrxCadTest.Utility;
using JrxCad.Command.Xopen;
using static JrxCad.Command.Xopen.XopenCmd;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JrxCad.Command.Xopen.ViewModel;

namespace JrxCadTest.Command.XopenCmdTest
{
    public partial class XopenCmdTest
    {
        #region PICKFIRST = 0
        /// <summary>
        /// select object => xref count > 1 & Childあり => Select Xref: (xref-03)
        /// </summary>
        public void ExecuteCommandTest08()
        {
            ExecutePickFirstEqual0(XREF03_NAME);
        }

        /// <summary>
        /// select object = xref count > 1 & Childなし => Select Xref: (xref-04)
        /// </summary>
        public void ExecuteCommandTest09()
        {
            ExecutePickFirstEqual0(XREF04_NAME);
        }

        /// <summary>
        /// select objec = xref count = 1 => Select Xref: (xref-02)
        /// </summary>
        public void ExecuteCommandTest10()
        {
            ExecutePickFirstEqual0(XREF02_NAME);
        }

        /// <summary>
        /// PICKFIRST setup = 0
        /// </summary>
        private  void ExecutePickFirstEqual0(string xrefName)
        {
            if (!IsFileRequire(XOPEN_01)) return;
            var objectIds = GetSelectionSetByXref(new List<string>() { xrefName });
            var xrefItems = GetXrefItems(objectIds);

            var pickedPoint = new Point3d(-10, -10, 0);
            var nestedEntityResult = CreateEx.CreatePromptNestedEntityResult(PromptStatus.Error, string.Empty, pickedPoint);

            PromptNestedEntityOptions nestedEntityOptions = null;

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(0);

            // Setup NestedEntity cause PICKFIRST = 0, so it will get NestedEntity
            userInputXopenMock.Setup(x => x.GetNestedEntity(It.IsAny<PromptNestedEntityOptions>()))
                              .Callback((PromptNestedEntityOptions options) => { nestedEntityOptions = options; })
                              .Returns(nestedEntityResult);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            // verify command method
            VerifyCommandMethod(xopenCmd);

            if (xrefName == XREF03_NAME)
            {
                xrefItems.IsNotNull();
                xrefItems.Count.Is(2);
            }
            else
            {
                xrefItems.IsNotNull();
                xrefItems.Count.Is(1);
            }

            // verify result test
            VerifyEx.VerifyPromptNestedEntityOptions(nestedEntityOptions, new PromptNestedEntityOptions($"\n{XOpenRes.UI_Select}"));

            // verify all.
            userInputXopenMock.VerifyAll();
        }
        #endregion

        #region PickFirst = 1

        /// <summary>
        /// select object => xopen cmd => object count = 0 => select xref:
        /// </summary>
        public void ExecuteCommandTest11()
        {
            ExecutePickFirstEqual1NotXrefItem("BlockRefName");
        }

        /// <summary>
        ///  select object => xopen cmd => xref count = 0 => select xref:
        /// </summary>
        public void ExecuteCommandTest12()
        {
            ExecutePickFirstEqual1NotXrefItem("*U3");
        }

        /// <summary>
        /// select object => xopen cmd => xref count = 1 => open drawing file.
        /// </summary>
        public void ExecuteCommandTest13()
        {
            ExecutePickFirstEqual1OpenFile();
        }
        
        /// <summary>
        ///  select object => xopen cmd => xref count > 1 => open dialog
        /// </summary>
        public void ExecuteCommandTest14()
        {
            ExecutePickFirstEqual1OpenDialog();
        }

        /// <summary>
        /// PICKFIRST = 1, not exist object id, xref item
        /// </summary>
        private void ExecutePickFirstEqual1NotXrefItem(string xrefName)
        {
            if (!IsFileRequire(XOPEN_01)) return;

            var objectIds = GetSelectionSetByXref(new List<string>() { xrefName });
            var selectionSet = SelectionSet.FromObjectIds(objectIds);
            var xrefItems = GetXrefItems(objectIds);

            var pickedPoint = new Point3d(-10, -10, 0);
            var netstedEntityResult = CreateEx.CreatePromptNestedEntityResult(PromptStatus.Error, string.Empty, pickedPoint);

            PromptNestedEntityOptions nestedEntityOptions = null;

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(1);

            //Setup PromptSelectionResult
            userInputXopenMock.Setup(x => x.GetPromptSelectionResult())
                              .Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            // Setup PromptNestedEntityResult, cause when PICKFIRST = 1, but object id or xref count = 0, it will Get NestedEntity
            userInputXopenMock.Setup(x => x.GetNestedEntity(It.IsAny<PromptNestedEntityOptions>()))
                              .Callback((PromptNestedEntityOptions options) => { nestedEntityOptions = options; })
                              .Returns(netstedEntityResult);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            // Verify command test
            VerifyCommandMethod(xopenCmd);

            // verify result test
            VerifyEx.VerifyPromptNestedEntityOptions(nestedEntityOptions, new PromptNestedEntityOptions($"\n{XOpenRes.UI_Select}"));

            objectIds.IsNotNull();
            objectIds.Length.Is(1);
            xrefItems.IsNotNull();
            xrefItems.Count.Is(0);

            // verify all
            userInputXopenMock.VerifyAll();
        }

        /// <summary>
        /// PICKFIRST = 1, select object => xref count = 1 => open drawing file.
        /// </summary>
        private void ExecutePickFirstEqual1OpenFile()
        {
            if (!IsFileRequire(XOPEN_01)) return;

            List<string> fileNames = new List<string>() { XREF01_NAME };
            ObjectId[] objectIds = GetSelectionSetByXref(fileNames);

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(1);

            SelectionSet selectionSet = SelectionSet.FromObjectIds(objectIds);

            // Setup PromptSelectionResult
            userInputXopenMock.Setup(x => x.GetPromptSelectionResult())
                              .Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            string currentDocument = CADApp.DocumentManager.CurrentDocument.Name;
            string path = System.IO.Path.GetDirectoryName(currentDocument);
            string filePath = System.IO.Path.Combine(path, string.Format("{0}.dwg", XREF01_NAME));
            // setup drawing isn't opened
            userInputXopenMock.Setup(x => x.GetFullPath(It.IsAny<string>())).Returns(filePath);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod  test
            VerifyCommandMethod(xopenCmd);

            // verify all
            userInputXopenMock.VerifyAll();

            // close open file have just opened
            SaveActiveDrawing(XOPEN_01);
        }

        /// <summary>
        /// PICKFIRST = 1, select object => xref count > 1 => open dialog
        /// </summary>
        private void ExecutePickFirstEqual1OpenDialog()
        {
            if (!IsFileRequire(XOPEN_01)) return;

            var fileNames = new List<string>() { XREF03_NAME };

            var objectIds = GetSelectionSetByXref(fileNames);
            var selectionSet = SelectionSet.FromObjectIds(objectIds);
            var xrefItems = GetXrefItems(objectIds);
            WXOpenViewModel viewModel = null;
            void SetupViewModel(WXOpenViewModel vm)
            {
                vm.SetBlockImageSize(190, 100);
                vm.XRefFileItems[0].IsSelected = true;
            }

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(1);

            // Setup PromptSelectionResult
            userInputXopenMock.Setup(x => x.GetPromptSelectionResult())
                              .Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            // Setup ViewModel
            userInputXopenMock.Setup(x => x.FormXOpenShowDialog(It.IsAny<WXOpenViewModel>()))
                              .Callback((WXOpenViewModel vm) =>
                              {
                                  viewModel = vm.MemberwiseClone();
                                  SetupViewModel(vm);
                              }).Returns(false);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod test
            VerifyCommandMethod(xopenCmd);

            // Input ViewModel test
            if (viewModel is null) return;
            viewModel.IsNotNull();
            viewModel.SelectedXrefFileItems.Count.Is(1);
            viewModel.XRefFileItems.Count.Is(1);

            objectIds.IsNotNull();
            objectIds.Length.Is(1);
            xrefItems.Count.Is(2);

            userInputXopenMock.VerifyAll();

            WriteMessage(string.Format(XREF_ITEM_SELECTED, XREF03_NAME));
        }

        #endregion
    }

}
