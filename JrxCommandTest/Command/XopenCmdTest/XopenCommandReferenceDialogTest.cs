﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
using GrxCAD.Runtime;
using GrxCAD.ApplicationServices;
using CADApp = GrxCAD.ApplicationServices.Application;
#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.ApplicationServices;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
#endif

using System.Collections.Generic;
using static JrxCad.Command.Xopen.XopenCmd;
using Moq;
using JrxCad.Command.Xopen;
using JrxCadTest.Utility;
using JrxCad.Command.Xopen.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.IO;

namespace JrxCadTest.Command.XopenCmdTest
{
    public partial class XopenCmdTest
    {
        #region selected xref item
        /// <summary>
        /// Selected TreeViewItem parent (xref-03)
        /// </summary>
        public void ExecuteCommandTest22()
        {
            var xrefNames = new List<string>() { XREF03_NAME };
            ExecuteCommandSelectTreeviewItem(xrefNames);
        }

        ///// <summary>
        ///// Selected TreeViewItem children (xref-03 child)
        ///// </summary>
        public void ExecuteCommandTest23()
        {
            var xrefNames = new List<string>() { XREF03_CHILD_NAME };
            ExecuteCommandSelectTreeviewItem(xrefNames);
        }

        ///// <summary>
        ///// select multi treeview item by selection set (PICKFIRST = 1)
        ///// </summary>
        public void ExecuteCommandTest24()
        {
            var xrefNames = new List<string>() { XREF01_NAME, XREF02_NAME, XREF03_NAME, XREF04_NAME };
            ExecuteCommandSelectTreeviewItem(xrefNames);
        }

        ///// <summary>
        /////  Selected multi TreeViewItem  (xref-03 and xref-03 child)
        ///// </summary>
        public void ExecuteCommandTest25()
        {
            var xrefNames = new List<string>() { XREF03_NAME, XREF03_CHILD_NAME };
            ExecuteCommandSelectTreeviewItem(xrefNames);
        }

        /// <summary>
        /// selected treeview item by xref input
        /// </summary>
        private void ExecuteCommandSelectTreeviewItem(List<string> xrefNames)
        {
            if (!IsFileRequire(XOPEN_01)) return;

            var xrefReferenceNames = new List<string>(xrefNames);
            if (xrefNames.Contains(XREF03_CHILD_NAME))
            {
                // cause objectid xref-03 child, so it get selection container, should add xref-03 
                xrefReferenceNames.Clear();
                xrefReferenceNames.Add(XREF03_NAME);
            }
            
            var objectIds = GetSelectionSetByXref(xrefReferenceNames);
            var selectionSet = SelectionSet.FromObjectIds(objectIds);
            var xrefItems = GetXrefItems(objectIds);
            WXOpenViewModel viewModel = null;
            void SetupViewModel(WXOpenViewModel vm)
            {
                vm.SetBlockImageSize(190, 100);
                // setup selected item
                foreach (var xref in xrefItems) 
                {
                    if (xrefNames.Contains(xref.Name))
                    {
                        FindXrefItem(vm.XRefFileItems, xref.RefId);
                    }
                }
            }

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(1);

            // Setup PromptSelectionResult
            userInputXopenMock.Setup(x => x.GetPromptSelectionResult())
                              .Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            // Setup ViewModel
            userInputXopenMock.Setup(x => x.FormXOpenShowDialog(It.IsAny<WXOpenViewModel>()))
                              .Callback((WXOpenViewModel vm) =>
                              {
                                  viewModel = vm.MemberwiseClone();
                                  SetupViewModel(vm);
                              }).Returns(false);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod  test
            VerifyCommandMethod(xopenCmd);

            if (viewModel is null) return;
            var xrefCount = xrefItems.Where(x => x.ParentId.ToString() == "(0)").Count();
            // Input ViewModel test
            viewModel.IsNotNull();
            viewModel.SelectedXrefFileItems.Count.Is(xrefNames.Count);
            viewModel.XRefFileItems.Count.Is(xrefCount);

            objectIds.IsNotNull();
            objectIds.Length.Is(xrefCount);

            userInputXopenMock.VerifyAll();

            string xrefSelected = xrefNames.Count == 1 ? string.Format(XREF_ITEM_SELECTED, xrefNames[0]) : string.Format(XREF_ITEM_MULTI_SELECTED, string.Join(", ", xrefNames));
            WriteMessage(xrefSelected);
        }
        #endregion

        #region Zoom to xref
        /// <summary>
        /// Zoom to xref-03
        /// </summary>
        public void ExecuteCommandTest26()
        {
            var xrefNames = new List<string>() { XREF03_NAME };
            ExecuteCommandZoomToXref(xrefNames);
        }

        /// <summary>
        ///  Zoom to xref-03child
        /// </summary>
        public void ExecuteCommandTest27()
        {
            var xrefNames = new List<string>() { XREF03_CHILD_NAME };
            ExecuteCommandZoomToXref(xrefNames);
        }

        /// <summary>
        ///  Zoom to xref-03 and xref-03child
        /// </summary>
        public void ExecuteCommandTest28()
        {
            var xrefNames = new List<string>() { XREF03_NAME, XREF03_CHILD_NAME };
            ExecuteCommandZoomToXref(xrefNames);
        }

        /// <summary>
        /// Zoom To xref item
        /// </summary>
        private void ExecuteCommandZoomToXref(List<string> xrefNames)
        {
            if (!IsFileRequire(XOPEN_01)) return;

            var xrefReferenceNames = new List<string>(xrefNames);
            if (xrefNames.Contains(XREF03_CHILD_NAME))
            {
                // cause objectid xref-03 child, so it get selection container, should add xref-03 
                xrefReferenceNames.Clear();
                xrefReferenceNames.Add(XREF03_NAME);
            }

            var objectIds = GetSelectionSetByXref(xrefReferenceNames);
            var selectionSet = SelectionSet.FromObjectIds(objectIds);
            var xrefItems = GetXrefItems(objectIds);

            WXOpenViewModel viewModel = null;
            void SetupViewModel(WXOpenViewModel vm)
            {
                vm.SetBlockImageSize(190, 100);
                // setup selected item
                foreach (var xref in xrefItems)
                {
                    if (xrefNames.Contains(xref.Name))
                    {
                        FindXrefItem(vm.XRefFileItems, xref.RefId);
                    }
                }

                // Zoom Item
                vm.ExecuteZoomToEvent();
            }

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(1);

            // Setup PromptSelectionResult
            userInputXopenMock.Setup(x => x.GetPromptSelectionResult())
                              .Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            // Setup ViewModel
            userInputXopenMock.Setup(x => x.FormXOpenShowDialog(It.IsAny<WXOpenViewModel>()))
                              .Callback((WXOpenViewModel vm) =>
                              {
                                  viewModel = vm.MemberwiseClone();
                                  SetupViewModel(vm);
                              }).Returns(false);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod  test
            VerifyCommandMethod(xopenCmd);

            if (viewModel is null) return;
            // Input ViewModel Test
            viewModel.IsNotNull();
            viewModel.SelectedXrefFileItems.Count.Is(xrefNames.Count);
            objectIds.Length.Is(1);

            userInputXopenMock.VerifyAll();
            string zoomToXref = string.Join(", ", xrefNames);
            WriteMessage(string.Format(ZOOM_TO_XREF_ITEM, zoomToXref));
        }

        #endregion
     
    }
}
