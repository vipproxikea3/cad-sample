﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using CADApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.Runtime;
using GrxCAD.ApplicationServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
#endif

using JrxCad.Utility;
using JrxCad.Helpers;
using System.Collections.Generic;
using static JrxCad.Command.Xopen.XopenCmd;
using System.IO;
using JrxCad.Command.Xopen.ViewModel;
using JrxCadTest.Utility;
using JrxCad.Command.Xopen;

namespace JrxCadTest.Command.XopenCmdTest
{
    public partial class XopenCmdTest
    {
        #region Variable
        public const string XREF_ITEM_SELECTED = "Open Reference file Dialog, {0} is selected ";
        public const string XREF_ITEM_MULTI_SELECTED = "Open Reference file Dialog, {0} are selected";
        public const string ZOOM_TO_XREF_ITEM = "Zoom to {0}";
        public const string OPEN_DRAWING_SINGLE_FILE = "{0} is opened";
        public const string OPEN_DRAWING_MULTIPLE_FILE = "{0} are opened";
        public const string CURRENT_DRAWING = "Current drawing: {0} file";
        public const string NOT_OPEN_READ_ONLY_FILE = "{0} file is read-only, and don't open file.";
        public const string OPEN_FILE_FROM_DIALOG = "{0} is open, {1} has been opened and {2} is read-only";
        public const string FILE_HAS_BEEN_OPENED = "{0} is open and {1} has been opened";
        #endregion

        #region Common method

        /// <summary>
        /// Get objectid list from document by xref name
        /// </summary>
        public ObjectId[] GetSelectionSetByXref(List<string> pathFileNames)
        {
            List<ObjectId> objectIds = new List<ObjectId>();
            Dictionary<string, ObjectId> blockRefs = new Dictionary<string, ObjectId>();
            // SelectionSet input
            PromptSelectionResult entities = Util.Editor().SelectAll();
            if (entities != null)
            {
                using (var tr = Util.StartTransaction())
                {
                    foreach (SelectedObject selectedObject in entities.Value)
                    {
                        // get block reference include (xref and not xref
                        var blockRef = tr.GetObject<BlockReference>(selectedObject.ObjectId, OpenMode.ForRead);
                        if (blockRef != null)
                        {
                            if (blockRefs.ContainsKey(blockRef.Name))
                                blockRefs[blockRef.Name] = selectedObject.ObjectId;
                            else
                                blockRefs.Add(blockRef.Name, selectedObject.ObjectId);
                        }
                    }
                }

                // sort xref item by input
                foreach (string xrefName in pathFileNames)
                {
                    blockRefs.TryGetValue(xrefName, out var item);
                    if (item != null) objectIds.Add(item);
                }
            }

            return objectIds.ToArray();
        }

        /// <summary>
        /// Get xref item from Mock Setup
        /// </summary>
        private List<XrefItem> GetXrefItems(ObjectId[] objectIdCollection)
        {
            List<XrefItem> xrefItems = new List<XrefItem>();
            if (objectIdCollection == null || objectIdCollection.Length == 0) return xrefItems;

            using (var tr = Util.StartTransaction())
            {
                foreach (ObjectId objectId in objectIdCollection)
                {
                    if (IsXref(tr, objectId, out var blockReference))
                    {
                        // add xref item (such as parent xref item).
                        xrefItems.Add(new XrefItem(blockReference, ObjectId.Null));

                        //collection children xref item  by parent objectId
                        Collect(tr, blockReference, ref xrefItems);
                    }
                }
            }
            return xrefItems;
        }

        /// <summary>
        /// Check External Xref
        /// </summary>
        private bool IsXref(Transaction tr, ObjectId id, out BlockReference blockRef)
        {
            blockRef = tr.GetObject<BlockReference>(id, OpenMode.ForRead);
            if (blockRef == null)
            {
                return false;
            }

            var blockRefTable = tr.GetObject<BlockTableRecord>(blockRef.BlockTableRecord, OpenMode.ForRead);
            if (!blockRefTable.IsFromExternalReference)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Recursive Add Child Reference
        /// </summary>
        private void Collect(Transaction tr, BlockReference blockRef, ref List<XrefItem> xrefs)
        {
            var blockRefTable = tr.GetObject<BlockTableRecord>(blockRef.BlockTableRecord, OpenMode.ForRead);
            foreach (var item in xrefs)
            {
                if (item.RefId != blockRef.Id)
                {
                    continue;
                }
                item.DefId = blockRefTable.Id;
                item.PathName = blockRefTable.PathName;
                break;
            }

            foreach (var id in blockRefTable)
            {
                var br = tr.GetObject<BlockReference>(id, OpenMode.ForRead);
                if (br == null)
                {
                    continue;
                }

                var childBlock = tr.GetObject<BlockTableRecord>(br.BlockTableRecord, OpenMode.ForRead);
                if (!childBlock.IsFromExternalReference)
                {
                    continue;
                }

                xrefs.Add(new XrefItem(br, blockRef.Id));
                Collect(tr, br, ref xrefs);
            }
        }

        /// <summary>
        /// check current file open
        /// </summary>
        public bool IsFileRequire(string xrefNameRequire)
        {
            string currentDocument = CADApp.DocumentManager.CurrentDocument.Name;
            string path = Path.GetDirectoryName(currentDocument);
            string filePath = Path.Combine(path, string.Format("{0}.dwg", xrefNameRequire));
            if (xrefNameRequire != XOPEN_01 && File.Exists(filePath))
            {
                bool isOpened = false;
                foreach(Document doc in CADApp.DocumentManager)
                {
                    if (doc.Name == filePath) isOpened = true;
                }
                if (!isOpened)
                {
                    // open drawing file
                    CADApp.DocumentManager.MdiActiveDocument = CADApp.DocumentManager.Open(filePath, false);
                }
                return true;
            }

            if (currentDocument != filePath)
            {
                Util.Editor().WriteMessage($"\nPlease Open {xrefNameRequire}.dwg to execute command test!");
                return false;
            }
            return true;
        }

        /// <summary>
        /// write message
        /// </summary>
        public void WriteMessage(string msg)
        {
            Util.Editor().WriteMessage($"\n{msg}");
        }

        /// <summary>
        /// selected treeview item by refId (single selected or multi selected).
        /// </summary>
        private void FindXrefItem(List<XrefFileItem> items, ObjectId refId)
        {
            foreach (XrefFileItem xref in items)
            {
                if (xref.RefId == refId)
                    xref.IsSelected = true;
                if (xref.Items != null && xref.Items.Count > 0)
                {
                    FindXrefItem(xref.Items, refId);
                }
            }
        }

        /// <summary>
        /// Verify command method
        /// </summary>
        private void VerifyCommandMethod(XopenCmd xopenCmd)
        {
#if DEBUG
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "XO", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.NoMultiple |
                                         CommandFlags.NoNewStack | CommandFlags.Interruptible, xopenCmd.GetType());
#else
#if _IJCAD_
            //For debug in IJCAD, set command flag to CommandFlags.Session instead of CommandFlags.Modal to prevent crash.
            //Refer to: https://tknmt.hatenablog.com/entry/2019/05/29/110659
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "SmxXOPEN", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.NoMultiple |
                                         CommandFlags.NoNewStack | CommandFlags.Interruptible, xopenCmd.GetType());
#elif _AutoCAD_
           VerifyEx.VerifyCommandMethod("JrxCommandTest", "SmxXOPEN", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.NoMultiple |
                                        CommandFlags.NoNewStack | CommandFlags.Interruptible, xopenCmd.GetType());
#endif
#endif
        }
        #endregion

        /// <summary>
        /// Get current drawing
        /// </summary>
        private Document GetCurrentDrawing(string docName)
        {
            foreach (Document doc in CADApp.DocumentManager)
            {
                var filePath = Path.GetDirectoryName(doc.Name);
                var fileName = Path.Combine(filePath, string.Format("{0}.dwg", docName));
                if (doc.Name == fileName) return doc;
            }
            return null;
        }

        /// <summary>
        /// close all file
        /// </summary>
        public void SaveActiveDrawing(string strXopen01, string strXopen02 = null, string strXopne01Parent = null)
        {
            var xopen01Doc = GetCurrentDrawing(strXopen01);
            var xopen02Doc = GetCurrentDrawing(strXopen02);
            var xopen01Parent = GetCurrentDrawing(strXopne01Parent);
            foreach (Document doc in CADApp.DocumentManager)
            {
                if (xopen02Doc != null && xopen02Doc.Name == doc.Name) continue;
                if (xopen01Parent != null && xopen01Parent.Name == doc.Name) continue;
                if (doc.Name != xopen01Doc.Name)
                {
                    if (CADApp.DocumentManager.MdiActiveDocument != doc) CADApp.DocumentManager.MdiActiveDocument = doc;
                    int isModified = System.Convert.ToInt32(Application.GetSystemVariable("DBMOD"));
                    // no need to save if not modified
                    if (isModified == 0)
                        doc.CloseAndDiscard();
                    else
                        doc.CloseAndSave(doc.Name);
                }
            }
            CADApp.DocumentManager.MdiActiveDocument = xopen01Doc;
        }
    }
}
