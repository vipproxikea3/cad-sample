﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;
using GrxCAD.ApplicationServices;
using CADApp = GrxCAD.ApplicationServices.Application;
#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
#endif

using Moq;
using JrxCadTest.Utility;
using JrxCad.Command.Xopen;
using System.Collections.Generic;
using System.IO;
using JrxCad.Command.Xopen.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace JrxCadTest.Command.XopenCmdTest
{
    public partial class XopenCmdTest
    {
        #region Open drawing file from dialog
        /// <summary>
        ///  A.未Open図面 => A.dwg Open⇒	カレント図面=A.dwg (open file from D1).
        /// </summary>
        public void ExecuteCommandTest15()
        {
            var xrefNames = new List<string> { XREF03_NAME };
            // A. is Xref-03
            ExecuteOpenDrawingFileFromDialog(xrefNames, XREF03_NAME, string.Empty);
        }

        /// <summary>
        /// B.Open済み = Openしない⇒	カレント図面=B.dwg	
        /// </summary>
        public void ExecuteCommandTest16()
        {
            var xrefNames = new List<string> { XREF03_NAME };
            // B is xref-03child
            ExecuteOpenDrawingFileFromDialog(xrefNames, XREF03_NAME, XREF03_CHILD_NAME);
        }

        /// <summary>
        /// Execute command for drawing is opened
        /// </summary>
        private void ExecuteOpenDrawingFileFromDialog(List<string> xrefNames, string fileNameWillOpen, string fileNameOpened)
        {
            if (!IsFileRequire(XOPEN_01)) return;

            var xrefReferenceNames = new List<string>(xrefNames);
            if (xrefNames.Contains(XREF03_CHILD_NAME))
            {
                // cause objectid xref-03 child, so it get selection container, should add xref-03 
                xrefReferenceNames.Clear();
                xrefReferenceNames.Add(XREF03_NAME);
            }

            var objectIds = GetSelectionSetByXref(xrefNames);
            var selectionSet = SelectionSet.FromObjectIds(objectIds);
            var xrefItems = GetXrefItems(objectIds);

            // Initialize Input ViewModel
            WXOpenViewModel viewModel = null;
            void SetupViewModel(WXOpenViewModel vm)
            {
                vm.SetBlockImageSize(190, 100);
                // setup selected item
                foreach (var xref in xrefItems)
                {
                    if (xrefNames.Contains(xref.Name))
                    {
                        FindXrefItem(vm.XRefFileItems, xref.RefId);
                    }
                }
            }

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(1);

            // setup PromptSelectionResult
            userInputXopenMock.Setup(x => x.GetPromptSelectionResult())
                              .Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            // Setup ViewModel
            userInputXopenMock.Setup(x => x.FormXOpenShowDialog(It.IsAny<WXOpenViewModel>()))
                              .Callback((WXOpenViewModel vm) =>
                              {
                                  viewModel = vm.MemberwiseClone();
                                  SetupViewModel(vm);
                              }).Returns(true);

            var currentDocument = CADApp.DocumentManager.CurrentDocument.Name;
            if (!string.IsNullOrEmpty(fileNameOpened))
            {
                var isOpened = false;
                var fileOpened = Path.Combine(Path.GetDirectoryName(currentDocument), string.Format("{0}.dwg", fileNameOpened));
                foreach (Document doc in CADApp.DocumentManager)
                {
                    if (doc.Name == fileOpened)
                    {
                        isOpened = true;
                        break;
                    }
                }
                if (!isOpened)
                {
                    // open drawing file, if it still  not open
                    CADApp.DocumentManager.Open(fileOpened, false);
                    // active XOPEN-01
                    CADApp.DocumentManager.MdiActiveDocument = GetCurrentDrawing(XOPEN_01);
                }

                // setup drawing file is opened
                userInputXopenMock.Setup(x => x.GetFullPath(It.IsAny<string>())).Returns(fileOpened);
            }
            else
            {
                var fillWillOpen = Path.Combine(Path.GetDirectoryName(currentDocument), string.Format("{0}.dwg", fileNameWillOpen));
                // setup drawing file will open
                userInputXopenMock.Setup(x => x.GetFullPath(It.IsAny<string>())).Returns(fillWillOpen);
            }

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod test
            VerifyCommandMethod(xopenCmd);

            // Input ViewModel test
            if (viewModel is null) return;

            var xrefCount = xrefItems.Where(x => x.ParentId.ToString() == "(0)").Count();
            viewModel.IsNotNull();
            viewModel.SelectedXrefFileItems.Count.Is(xrefNames.Count);
            viewModel.XRefFileItems.Count.Is(xrefCount);
            objectIds.IsNotNull();
            objectIds.Length.Is(xrefCount);

            ////////////////////////////////////////
            // Test Completed
            ////////////////////////////////////////
            userInputXopenMock.VerifyAll();

            if (!string.IsNullOrEmpty(fileNameOpened))
                WriteMessage(string.Format(CURRENT_DRAWING, fileNameOpened));
            else
                WriteMessage(string.Format(OPEN_DRAWING_SINGLE_FILE, fileNameWillOpen));

            // close file have just been opened
            SaveActiveDrawing(XOPEN_01);
        }

        #endregion
    }
}
