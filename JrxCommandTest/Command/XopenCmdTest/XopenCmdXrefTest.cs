﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.EditorInput;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using CADApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.ApplicationServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using Autodesk.AutoCAD.ApplicationServices;
#endif

using Moq;
using JrxCad.Utility;
using JrxCadTest.Utility;
using JrxCad.Command.Xopen;
using System.Collections.Generic;
using static JrxCad.Command.Xopen.XopenCmd;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using JrxCad.Command.Xopen.ViewModel;
using JrxCad.Command.Xopen.View;
using System.Linq;

namespace JrxCadTest.Command.XopenCmdTest
{
    public partial class XopenCmdTest
    {
        #region declare
        private const string XOPEN_01 = "XOPEN-01";
        private const string XREF01_NAME = "xref-01";
        private const string XREF02_NAME = "xref-02";
        private const string XREF03_NAME = "xref-03";
        private const string XREF03_CHILD_NAME = "xref-03child";
        private const string XREF04_NAME = "xref-04";
        private const string RMNUM_NAME = "RMNUM";
        private const string XOPEN_02 = "XOPEN-02";
        private const string XOPEN_01_PARENT = "XOPEN-01_Parent";
        #endregion

        #region  open drawing file by xref
        /// <summary>
        /// Object count = 0 => Select Xref:
        /// </summary>
        public void ExecuteCommandTest01()
        {
            if (!IsFileRequire(XOPEN_01)) return;
            var pickedPoint = new Point3d(-10, -10, 0);
            var nestedEntityResultError = CreateEx.CreatePromptNestedEntityResult(PromptStatus.Error, string.Empty, pickedPoint);

            PromptNestedEntityOptions nestedEntityOptions = null;

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(0);

            //setup PromptNestedEntityResult Error
            userInputXopenMock.Setup(x => x.GetNestedEntity(It.IsAny<PromptNestedEntityOptions>()))
                              .Callback((PromptNestedEntityOptions options) => { nestedEntityOptions = options; })
                              .Returns(nestedEntityResultError);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            // verify command method
            VerifyCommandMethod(xopenCmd);

            // verify result test
            VerifyEx.VerifyPromptNestedEntityOptions(nestedEntityOptions, new PromptNestedEntityOptions($"\n{XOpenRes.UI_Select}"));

            // verify all.
            userInputXopenMock.VerifyAll();
        }

        /// <summary>
        /// xref count = 0 => Object is not an xref => Select Xref
        /// </summary>
        public void ExecuteCommandTest02()
        {
            if (!IsFileRequire(XOPEN_01)) return;
            var pickedPoint = new Point3d(-10, -10, 0);
            var nestedEntityResultError = CreateEx.CreatePromptNestedEntityResult(PromptStatus.Error, string.Empty, pickedPoint);

            pickedPoint = new Point3d(145.47781521767865, 76.006121563917418, 0);
            var nestedEntityResultOK = CreateEx.CreatePromptNestedEntityResult(PromptStatus.OK, string.Empty, pickedPoint);
            var objectIds = nestedEntityResultOK.GetContainers();

            var sequence = new MockSequence();
            PromptNestedEntityOptions nestedEntityOptions = null;

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(0);

            //setup PromptNestedEntityResult Error
            userInputXopenMock.InSequence(sequence).Setup(x => x.GetNestedEntity(It.IsAny<PromptNestedEntityOptions>()))
                              .Callback((PromptNestedEntityOptions options) => { nestedEntityOptions = options; })
                              .Returns(nestedEntityResultOK);

            userInputXopenMock.InSequence(sequence).Setup(x => x.GetNestedEntity(It.IsAny<PromptNestedEntityOptions>()))
                              .Callback((PromptNestedEntityOptions options) => { nestedEntityOptions = options; })
                              .Returns(nestedEntityResultError);

            userInputXopenMock.Setup(x => x.GetObjectId(It.IsAny<PromptNestedEntityResult>())).Returns(objectIds);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            // verify command method
            VerifyCommandMethod(xopenCmd);

            // verify result test
            VerifyEx.VerifyPromptNestedEntityOptions(nestedEntityOptions, new PromptNestedEntityOptions($"\n{XOpenRes.UI_Select}"));

            // verify all.
            userInputXopenMock.VerifyAll();
        }

        /// <summary>
        /// Xref count = 1 => open drawing (xref-01)
        /// </summary>
        public void ExecuteCommandTest03()
        {
            ExecuteOpenDrawingTest(38.626565250522162, 221.63270194079291, 0, XREF01_NAME);
        }

        /// <summary>
        /// xref count > 1 & has child => open dialog (xref-03)
        /// </summary>
        public void ExecuteCommandTest04()
        {
            ExecuteOpenDialogXrefHasChild(162.86199645181773, 222.63393052464562, 0);
        }

        /// <summary>
        /// xref count > 1 & Childなし => open drawing (xref-04)
        /// </summary>
        public void ExecuteCommandTest05()
        {
            ExecuteOpenDrawingTest(40.424404693836777, 162.69529153394842, 0, XREF04_NAME);
        }

        /// <summary>
        /// Select Xref: => Enter => End
        /// </summary>
        public void ExecuteCommandTest06()
        {
            EndXopenCommand();
        }

        /// <summary>
        /// Selected Xref: => Cancel => End
        /// </summary>
        public void ExecuteCommandTest07()
        {
            EndXopenCommand();
        }

        /// <summary>
        /// Open drawing file by pickedpoint
        /// </summary>
        private void ExecuteOpenDrawingTest(double pointX, double pointY, double pointZ, string xrefName)
        {
            if (!IsFileRequire(XOPEN_01)) return;

            var pickedPoint = new Point3d(pointX, pointY, pointZ);
            var promptNestedEntityResult = CreateEx.CreatePromptNestedEntityResult(PromptStatus.OK, "", pickedPoint);
            var objectIds = promptNestedEntityResult.GetContainers();

            // Result Xref item test
            var xrefItems = GetXrefItems(objectIds);
            PromptNestedEntityOptions promptNestedEntityOptions = null;

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(0);


            //setup PromptNestedEntityOptions
            userInputXopenMock.Setup(x => x.GetNestedEntity(It.IsAny<PromptNestedEntityOptions>()))
                              .Callback((PromptNestedEntityOptions options) => promptNestedEntityOptions = options)
                              .Returns(promptNestedEntityResult);

            // Setup object id result test
            userInputXopenMock.Setup(x => x.GetObjectId(It.IsAny<PromptNestedEntityResult>()))
                              .Returns(objectIds);

            var currentDocument = CADApp.DocumentManager.CurrentDocument.Name;
            var path = Path.GetDirectoryName(currentDocument);
            var filePath = Path.Combine(path, string.Format("{0}.dwg", xrefName));
            // setup drawing file isn't opened
            userInputXopenMock.Setup(x => x.GetFullPath(It.IsAny<string>())).Returns(filePath);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod test
            VerifyCommandMethod(xopenCmd);

            //PromptNestedEntityOptions Test
            VerifyEx.VerifyPromptNestedEntityOptions(promptNestedEntityOptions, new PromptNestedEntityOptions($"\n{XOpenRes.UI_Select}"));

            // Verify result test
            if (xrefItems != null && xrefItems.Count > 0)
            {
                xrefItems.Count.Is(1);
            }

            // verify all
            userInputXopenMock.VerifyAll();

            // save as and close drawing file
            SaveActiveDrawing(XOPEN_01);
        }

        /// <summary>
        /// Open Reference file dialog
        /// </summary>
        private void ExecuteOpenDialogXrefHasChild(double pointX, double pointY, double pointZ)
        {
            if (!IsFileRequire(XOPEN_01)) return;

            var pickedPoint = new Point3d(pointX, pointY, pointZ);
            var promptNestedEntityResult = CreateEx.CreatePromptNestedEntityResult(PromptStatus.OK, "", pickedPoint);
            var objectIds = promptNestedEntityResult.GetContainers();

            var xrefItems = GetXrefItems(objectIds);
            PromptNestedEntityOptions promptNestedEntityOptions = null;
            WXOpenViewModel viewModel = null;
            void SetupViewModel(WXOpenViewModel vm)
            {
                vm.SetBlockImageSize(190, 100);
                vm.XRefFileItems[0].IsSelected = true;
            }

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(0);

            //setup PromptNestedEntityOptions
            userInputXopenMock.Setup(x => x.GetNestedEntity(It.IsAny<PromptNestedEntityOptions>()))
                              .Callback((PromptNestedEntityOptions options) => promptNestedEntityOptions = options)
                              .Returns(promptNestedEntityResult);

            // Setup ObjectId result test
            userInputXopenMock.Setup(x => x.GetObjectId(It.IsAny<PromptNestedEntityResult>()))
                              .Returns(objectIds);

            //Setup ViewModel
            userInputXopenMock.Setup(x => x.FormXOpenShowDialog(It.IsAny<WXOpenViewModel>()))
                              .Callback((WXOpenViewModel vm) =>
                              {
                                  viewModel = vm.MemberwiseClone();
                                  SetupViewModel(vm);
                              }).Returns(false);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod test
            VerifyCommandMethod(xopenCmd);

            // Input ViewModel Test
            if (viewModel is null) return;
            viewModel.IsNotNull();
            viewModel.SelectedXrefFileItems.Count.Is(1);
            viewModel.XRefFileItems.Count.Is(1);

            // Result test
            objectIds.IsNotNull();
            objectIds.Length.Is(1);
            xrefItems.Count.Is(2);

            //PromptNestedEntityOptions Test
            VerifyEx.VerifyPromptNestedEntityOptions(promptNestedEntityOptions, new PromptNestedEntityOptions($"\n{XOpenRes.UI_Select}"));
            userInputXopenMock.VerifyAll();
            WriteMessage(string.Format(XREF_ITEM_SELECTED, XREF03_NAME));
        }

        /// <summary>
        /// End or cancel XopenCommand
        /// </summary>
        private void EndXopenCommand()
        {
            if (!IsFileRequire(XOPEN_01)) return;
            var pickedPoint = new Point3d(-10, -10, 0);
            var nestedEntityResultError = CreateEx.CreatePromptNestedEntityResult(PromptStatus.Cancel, string.Empty, pickedPoint);

            PromptNestedEntityOptions nestedEntityOptions = null;

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(0);

            //setup PromptNestedEntityResult Error
            userInputXopenMock.Setup(x => x.GetNestedEntity(It.IsAny<PromptNestedEntityOptions>()))
                              .Callback((PromptNestedEntityOptions options) => { nestedEntityOptions = options; })
                              .Returns(nestedEntityResultError);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            // verify command method
            VerifyCommandMethod(xopenCmd);

            // verify result test
            VerifyEx.VerifyPromptNestedEntityOptions(nestedEntityOptions, new PromptNestedEntityOptions($"\n{XOpenRes.UI_Select}"));

            // verify all.
            userInputXopenMock.VerifyAll();
        }
        #endregion

        #region open xref from XOPEN-02.dwg, XOPEN-01_Parent.dwg

        /// <summary>
        /// Open xref item XOPEN-02.dwg (case 1)		
        /// </summary>
        public void ExecuteCommandTest29()
        {
            ExecuteCommandOpenXref(string.Empty, 3355.2954495946469, 1468.3728669532084, 0, XOPEN_02);
        }

        /// <summary>
        /// Open xref item XOPEN-02.dwg (case 2)	
        /// </summary>
        public void ExecuteCommandTest30()
        {
            ExecuteCommandOpenXref(string.Empty, 1277.8868162522049, 1968.7452887381041, 0, XOPEN_02);
        }

        /// <summary>
        ///  Open xref item XOPEN-02.dwg (case 3)		
        /// </summary>
        public void ExecuteCommandTest31()
        {
            ExecuteCommandOpenXref(RMNUM_NAME, 1602.5715999614986, 2314.8888201704303, 0, XOPEN_02);
        }

        /// <summary>
        ///  Open xref item from XOPEN-02.dwg (case 4)
        /// </summary>
        public void ExecuteCommandTest32()
        {
            ExecuteCommandOpenXref(RMNUM_NAME, 3658.9858070236478, 2235.1619181698916, 0, XOPEN_02);
        }

        /// <summary>
        ///  Open xref item from XOPEN-02.dwg (case 5)		
        /// </summary>
        public void ExecuteCommandTest33()
        {
            ExecuteCommandOpenXref(string.Empty, 2161.61406431067, 2517.5317320268477, 0, XOPEN_02);
        }

        /// <summary>
        /// Open xref item from XOPEN-Parent.dwg
        /// </summary>
        public void ExecuteCommandTest34()
        {
            ExecuteCommandOpenXref(string.Empty, 192.5491884810142, 280.03414740057633, 0, XOPEN_01_PARENT);
        }

        /// <summary>
        /// Open xref item
        /// </summary>
        private void ExecuteCommandOpenXref(string fileName, double pointX, double pointY, double pointZ, string xrefFile)
        {
            if (!IsFileRequire(xrefFile)) return;

            var pickedPoint = new Point3d(pointX, pointY, pointZ);
            var promptNestedEntityResult = CreateEx.CreatePromptNestedEntityResult(PromptStatus.OK, "", pickedPoint);
            var objectIds = promptNestedEntityResult.GetContainers();
            var xrefItems = GetXrefItems(objectIds);
            PromptNestedEntityOptions promptNestedEntityOptions = null;

            WXOpenViewModel viewModel = null;
            void SetupViewModel(WXOpenViewModel vm)
            {
                vm.SetBlockImageSize(190, 100);
                vm.XRefFileItems[0].IsSelected = true; // first item selected after Dialog opened.
            }

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(0);

            //setup PromptNestedEntityOptions
            userInputXopenMock.Setup(x => x.GetNestedEntity(It.IsAny<PromptNestedEntityOptions>()))
                              .Callback((PromptNestedEntityOptions options) => promptNestedEntityOptions = options)
                              .Returns(promptNestedEntityResult);

            // Setup object id result test
            userInputXopenMock.Setup(x => x.GetObjectId(It.IsAny<PromptNestedEntityResult>()))
                              .Returns(objectIds);

            // when fileName isn't empty => open drawing file, otherwise open dialog
            if (!string.IsNullOrEmpty(fileName))
            {
                // setup open drawing file.
                var currentDocument = CADApp.DocumentManager.CurrentDocument.Name;
                var filePath = Path.Combine(Path.GetDirectoryName(currentDocument), string.Format("{0}.dwg", fileName));
                // setup get fullpath
                userInputXopenMock.Setup(x => x.GetFullPath(It.IsAny<string>())).Returns(filePath);
            }
            else
            {
                // SetupViewModel
                userInputXopenMock.Setup(x => x.FormXOpenShowDialog(It.IsAny<WXOpenViewModel>()))
                                  .Callback((WXOpenViewModel vm) =>
                                  {
                                      viewModel = vm.MemberwiseClone();
                                      SetupViewModel(vm);
                                  }).Returns(false);
            }


            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod test
            VerifyCommandMethod(xopenCmd);

            // Input ViewModel test
            if (string.IsNullOrEmpty(fileName) && viewModel != null)
            {
                viewModel.IsNotNull();
                viewModel.SelectedXrefFileItems.Count.Is(1);
                objectIds.IsNotNull();
            }

            //PromptNestedEntityOptions Test
            VerifyEx.VerifyPromptNestedEntityOptions(promptNestedEntityOptions, new PromptNestedEntityOptions($"\n{XOpenRes.UI_Select}"));

            userInputXopenMock.VerifyAll();

            //setting display message in command window
            if (string.IsNullOrEmpty(fileName))
            {
                WriteMessage(string.Format(XREF_ITEM_SELECTED, xrefItems[0].Name));
            }
            else
            {
                WriteMessage(string.Format(OPEN_DRAWING_SINGLE_FILE, fileName));
            }

            if (xrefFile == XOPEN_02)
            {
                // close other tab and active curent drawing.
                SaveActiveDrawing(XOPEN_02, XOPEN_01);
            }
        }
        #endregion
    }
}
