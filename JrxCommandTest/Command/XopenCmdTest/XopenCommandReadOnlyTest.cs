﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;
using GrxCAD.ApplicationServices;
using CADApp = GrxCAD.ApplicationServices.Application;
#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
#endif

using Moq;
using JrxCadTest.Utility;
using JrxCad.Command.Xopen;
using System.Collections.Generic;
using System.IO;
using static JrxCad.Command.Xopen.XopenCmd;
using System.Diagnostics;
using JrxCad.Command.Xopen.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JrxCadTest.Command.XopenCmdTest
{
    public partial class XopenCmdTest
    {
        #region Open drawing file (read-only)
        /// <summary>
        /// xref is read-only (Yes)
        /// open drawing (xref-04)
        /// </summary>
        public void ExecuteCommandTest17()
        {
            ExecuteCommandReadOnlyTest(XREF04_NAME, true);
        }

        /// <summary>
        /// xref is read-only (No)
        /// don't open drawing (xref-04)
        public void ExecuteCommandTest18()
        {
            ExecuteCommandReadOnlyTest(XREF04_NAME, false);
        }

        /// <summary>
        /// Execute command for read-only file. (this case open from command, not open from dialog)
        /// </summary>
        private void ExecuteCommandReadOnlyTest(string xrefName, bool isOpenFile)
        {
            if (!IsFileRequire(XOPEN_01)) return;

            var fileNames = new List<string>() { xrefName };
            var objectIds = GetSelectionSetByXref(fileNames);
            var selectionSet = SelectionSet.FromObjectIds(objectIds);

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(1);

            // setup PromptSelectionResult
            userInputXopenMock.Setup(x => x.GetPromptSelectionResult())
                              .Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            var currentDocument = CADApp.DocumentManager.CurrentDocument.Name;
            var path = Path.GetDirectoryName(currentDocument);
            var filePath = Path.Combine(path, string.Format("{0}.dwg", xrefName));

            // setup drawing file isn't opened
            userInputXopenMock.Setup(x => x.GetFullPath(It.IsAny<string>())).Returns(filePath);

            // Set Attribute file path is read-only
            File.SetAttributes(filePath, FileAttributes.ReadOnly);

            // setup read-only dialog
            userInputXopenMock.Setup(x => x.ShowReadOnlyDialog(It.IsAny<string>())).Returns(isOpenFile);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod test
            VerifyCommandMethod(xopenCmd);

            ////////////////////////////////////////
            // Test Completed
            ////////////////////////////////////////
            userInputXopenMock.VerifyAll();

            if (!string.IsNullOrEmpty(filePath) && File.Exists(filePath))
            {
                // reset attribute for file path
                File.SetAttributes(filePath, FileAttributes.Normal);
            }

            if (!isOpenFile)
            {
                WriteMessage(string.Format(NOT_OPEN_READ_ONLY_FILE, xrefName));
            }

            // save close file before implement unit test
            SaveActiveDrawing(XOPEN_01);
        }
        #endregion

        #region open multiple drawing file
        /// <summary>
        /// A.未Open＆B.Open済み  => A.dwg Open	⇒	カレント図面＝A.dwg
        /// </summary>
        public void ExecuteCommandTest19()
        {
            // A = Xref-01, B = Xref-03
            var xrefNames = new List<string>() { XREF01_NAME, XREF03_NAME };
            ExecuteCommandOpenMultiDrawingFile(xrefNames, XREF03_NAME);
        }

        /// <summary>
        /// A.未Open＆B.Open済み＆C.ReadOnly図面  ⇒	Yes	⇒	A.dwg & C.dwg Open ⇒	カレント図面＝最後に開いた図面 	
        /// </summary>
        public void ExecuteCommandTest20()
        {
            // A = Xref-01, B = Xref-2, C = Xref-03
            var xrefNames = new List<string>() { XREF01_NAME, XREF02_NAME, XREF03_NAME };
            ExecuteCommandOpenMultiDrawingFile(xrefNames, XREF02_NAME, XREF03_NAME, true);
        }

        /// <summary>
        /// A.未Open＆B.Open済み＆C.ReadOnly図面  ⇒	No	⇒	A.dwg Open	⇒	カレント図面=A.dwg	
        /// </summary>
        public void ExecuteCommandTest21()
        {
            // A = Xref-01, B = Xref-3, C = Xref-02
            var xrefNames = new List<string>() { XREF01_NAME, XREF02_NAME, XREF03_NAME };
            ExecuteCommandOpenMultiDrawingFile(xrefNames, XREF02_NAME, XREF03_NAME, false);
        }

        /// <summary>
        /// Execute command open drawing with case below:
        /// 1.A.未Open＆B.Open済み 
        /// 2. A.未Open＆B.Open済み＆C.ReadOnly図面  
        ///   DM-ReadOnly													
        ///   ⇒	 Yes ⇒	A.dwg & C.dwg Open	⇒	カレント図面＝最後に開いた図面 		
        ///   ⇒	 No	 ⇒	A.dwg Open ⇒	カレント図面=A.dwg
        /// </summary>
        private void ExecuteCommandOpenMultiDrawingFile(List<string> xrefNames, string xrefOpened, string xrefReadOnly = null, bool isOpenFile = false)
        {
            if (!IsFileRequire(XOPEN_01)) return;

            var objectIds = GetSelectionSetByXref(xrefNames);
            var selectionSet = SelectionSet.FromObjectIds(objectIds);
            var xrefItems = GetXrefItems(objectIds);

            WXOpenViewModel viewModel = null;
            void SetupViewModel(WXOpenViewModel vm)
            {
                vm.SetBlockImageSize(190, 100);
                // setup selected item
                foreach (var xref in xrefItems)
                {
                    if (xrefNames.Contains(xref.Name))
                    {
                        FindXrefItem(vm.XRefFileItems, xref.RefId);
                    }
                }
            }

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputXopenMock = new Mock<XopenCmd.IUserInputXopenCmd>(MockBehavior.Strict);

            // setup PICKFIRST
            userInputXopenMock.Setup(x => x.GetPickFirst()).Returns(1);

            // setup PromptSelectionResult
            userInputXopenMock.Setup(x => x.GetPromptSelectionResult())
                              .Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            // setup ViewModel
            userInputXopenMock.Setup(x => x.FormXOpenShowDialog(It.IsAny<WXOpenViewModel>()))
                              .Callback((WXOpenViewModel vm) =>
                              {
                                  viewModel = vm.MemberwiseClone();
                                  SetupViewModel(vm);
                              }).Returns(true);

            #region get file path
            var currentDocument = CADApp.DocumentManager.CurrentDocument.Name;
            var fullPaths = new List<string>();
            var fileOpened = string.Empty;
            var fileReadOnly = string.Empty;
            foreach (var xrefName in xrefNames)
            {
                var fullPath = Path.Combine(Path.GetDirectoryName(currentDocument), xrefName + ".dwg");
                if (xrefName == xrefOpened)
                {
                    fileOpened = fullPath;
                }
                if (xrefName == xrefReadOnly)
                {
                    fileReadOnly = fullPath;
                }
                fullPaths.Add(fullPath);
            }

            if (!File.Exists(fileOpened)) return;
            #endregion

            #region setup open drawing file
            var isOpened = false;
            // auto open file
            foreach (Document doc in CADApp.DocumentManager)
            {
                if (doc.Name == fileOpened)
                {
                    isOpened = true;
                    break;
                }
            }

            if (!isOpened && !string.IsNullOrEmpty(fileOpened))
            {
                CADApp.DocumentManager.Open(fileOpened, false);

                // Active XOPEN-01
                CADApp.DocumentManager.MdiActiveDocument = GetCurrentDrawing(XOPEN_01);
            }

            #endregion

            #region setup readonly file
            if (!string.IsNullOrEmpty(fileReadOnly))
            {
                // change attribute file to readonly
                File.SetAttributes(fileReadOnly, FileAttributes.ReadOnly);

                // setup show dialog
                userInputXopenMock.Setup(x => x.ShowReadOnlyDialog(It.IsAny<string>())).Returns(isOpenFile);

                // Setup file is opening or not
                userInputXopenMock.SetupSequence(x => x.GetFullPath(It.IsAny<string>()))
                                  .Returns(fullPaths[0])  // set path for A file.
                                  .Returns(fullPaths[1])  // set path for B file.
                                  .Returns(fullPaths[2]); // set path for C  file.
            }
            else
            {
                // Setup File is Opening or not
                userInputXopenMock.SetupSequence(x => x.GetFullPath(It.IsAny<string>()))
                                  .Returns(fullPaths[0]) // set path for A file
                                  .Returns(fullPaths[1]); // set path for B file
            }
            #endregion


            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // execute Xopen command
            var xopenCmd = new XopenCmd(userInputXopenMock.Object);
            xopenCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod  test
            VerifyCommandMethod(xopenCmd);

            // Input ViewModel Test
            if (viewModel is null) return;
            viewModel.IsNotNull();
            viewModel.SelectedXrefFileItems.Count.Is(xrefNames.Count);
            viewModel.XRefFileItems.Count.Is(xrefNames.Count);

            objectIds.IsNotNull();
            objectIds.Length.Is(xrefNames.Count);

            ////////////////////////////////////////
            // Test Completed
            ////////////////////////////////////////
            userInputXopenMock.VerifyAll();

            // reset attribute of file
            if (!string.IsNullOrEmpty(fileReadOnly) && File.Exists(fileReadOnly))
            {
                File.SetAttributes(fileReadOnly, FileAttributes.Normal);
                WriteMessage(string.Format(OPEN_FILE_FROM_DIALOG, xrefNames[0], xrefNames[1], xrefNames[2]));
            }
            else 
                WriteMessage(string.Format(FILE_HAS_BEEN_OPENED, xrefNames[0], xrefNames[1]));

            SaveActiveDrawing(XOPEN_01);
        }
        #endregion
    }
}
