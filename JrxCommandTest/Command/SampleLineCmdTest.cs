﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using Line = GrxCAD.DatabaseServices.Line;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Line = Autodesk.AutoCAD.DatabaseServices.Line;

#endif
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using JrxCad.Command.SampleLine;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCadTest.Utility;

namespace JrxCadTest.Command
{
    [TestClass]
    public class SampleLineCmdTest
    {
        [CommandMethod("JrxCommandTest", "SmxSmxSampleLineTest01", CommandFlags.Modal)]
        public void SampleLineTest01()
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<SampleLineCmd.IUserInput>(MockBehavior.Strict);

            // Setup StartPoint
            // [Scenario]
            //  1. Specify (-10, -10, 0)
            var startPoint = new Point3d(-10, -10, 0);
            PromptPointOptions startOptions = null;     // for PromptOptions Test
            userInputMock.Setup(x => x.GetStartPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => startOptions = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", startPoint));

            // Setup EndPoint
            // [Scenario]
            //  1. Input "M"
            //  2. Specify (100, -50, 0)
            var endPoint = new Point3d(100, -50, 0);
            JigPromptPointOptions endOptions = null;        // for PromptOptions Test
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetEndPoint(It.IsAny<SampleLineCmd.EndPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "M", Point3d.Origin));
            userInputMock.InSequence(sequence).Setup(x => x.GetEndPoint(It.IsAny<SampleLineCmd.EndPointOptions>()))
                .Callback((PointDrawJig.PointOptions options) =>
                {
                    endOptions = options.Options;
                    options.UpdatePoint(endPoint);
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", endPoint));

            // Setup AppendedLines
            var appendedLineIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendLineId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedLineIds.Add(id));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // Execute SampleLine
            var cmd = new SampleLineCmd(userInputMock.Object);
            cmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            // CommandMethod Attribute Test
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "SmxSampleLine", CommandFlags.Modal, cmd.GetType());

            // PromptOptions Test
            VerifyEx.VerifyPromptPointOptions(startOptions,
                new PromptPointOptions($"\nSpecify start point of line"));

            VerifyEx.VerifyJigPromptPointOptions(endOptions,
                new JigPromptPointOptions($"\nSpecify end point of line or")
                {
                    UseBasePoint = true,
                    BasePoint = startPoint,
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.UseBasePointElevation,
                    Cursor = CursorType.Crosshair,
                    Keywords =
                    {
                        { "C", "C", "Cyan" },
                        { "M", "M", "Magenta" },
                        { "Y", "Y", "Yellow" },
                        { "L", "L", "byLayer" },
                    },
                });

            // Result Test
            appendedLineIds.Count.Is(1);
            var objId = appendedLineIds[0];
            objId.IsNull.IsFalse();

            using (var tr = Util.StartTransaction())
            using (var line = tr.GetObject<Line>(objId, OpenMode.ForRead))
            {
                line.IsNotNull();
                line.StartPoint.Is(startPoint);
                line.EndPoint.Is(endPoint);
                line.ColorIndex.Is(6);
            }

            ////////////////////////////////////////
            // Test Completed
            ////////////////////////////////////////

            userInputMock.VerifyAll();
            Util.Editor().WriteMessage("\nTest Completed!");
        }
    }
}
