﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JrxCad.Command.AttEdit;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCad.Command.AttEdit.ViewModel;

namespace JrxCadTest.Command.AttEdit
{
    public class PropertyTest
    {
        public static void AttEditPropertyTest(ObjectId id, FormAttEditViewModel vm)
        {
            List<FormAttEdit.AttributeItem> attList = new List<FormAttEdit.AttributeItem>()
            {
            };//doing
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            using (var tr = Util.StartTransaction())
            using (var blockRef = tr.GetObject<BlockReference>(id, OpenMode.ForRead))
            {
                try
                {
                    blockRef.IsNotNull();
                    blockRef.AttributeCollection.IsNotNull();
                    var attRefList = blockRef.AttributeCollection;
                    var index = 0;
                    var startPos = vm.StartPosition;
                    for (var i = startPos; i < startPos + 15; i++)
                    {
                        if (attRefList.Count > 15 && attRefList.Count < startPos + 15) break;
                        if (attRefList.Count == i) break;
                        var attRef = tr.GetObject<AttributeReference>(attRefList[i], OpenMode.ForRead);
                        attRef.Equals(attList[index]);
                        attRef.Id.Equals(attList[index].Id).IsTrue();
                        attRef.Tag.Equals(attList[index].Tag).IsTrue();
                        attRef.TextString.Equals(attList[index].TextString.Replace("\r", "")).IsTrue();
                        attRef.HasFields.Equals(attList[index].HasFields).IsTrue();
                        attRef.IsMTextAttribute.Equals(attList[index].IsMText).IsTrue();
                        if (attRef.IsMTextAttribute) attRef.MTextAttribute.Contents.Equals(attList[index].MTextAttContents).IsTrue();
                        index++;
                    }
                    Util.Editor().WriteMessage($"\n{currentMethod} Completed!!!!");
                }
                catch (Exception)
                {
                    Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
                }

            }
        }

        public static void AttEditAllPropertyTest(ObjectId id, FormAttEditViewModel vm)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            using (var tr = Util.StartTransaction())
            using (var blockRef = tr.GetObject<BlockReference>(id, OpenMode.ForRead))
            {
                try
                {
                    blockRef.IsNotNull();
                    blockRef.AttributeCollection.IsNotNull();
                    var attRefList = blockRef.AttributeCollection;
                    var index = 0;
                    foreach (ObjectId attRefId in attRefList)
                    {
                        var attRef = tr.GetObject<AttributeReference>(attRefId, OpenMode.ForRead);
                        attRef.Id.Equals(vm.AttList.ElementAtOrDefault(index).Id).IsTrue();
                        attRef.Tag.Equals(vm.AttList.ElementAtOrDefault(index).Tag).IsTrue();
                        attRef.TextString.Equals(vm.AttList.ElementAtOrDefault(index).TextString.Replace("\r", "")).IsTrue();
                        attRef.HasFields.Equals(vm.AttList.ElementAtOrDefault(index).HasFields).IsTrue();
                        attRef.IsMTextAttribute.Equals(vm.AttList.ElementAtOrDefault(index).IsMText).IsTrue();
                        if (attRef.IsMTextAttribute) attRef.MTextAttribute.Contents.Equals(vm.AttList.ElementAtOrDefault(index).MTextAttContents).IsTrue();
                        index++;
                    }
                    Util.Editor().WriteMessage($"\n{currentMethod} Completed!!!!");
                }
                catch (Exception)
                {
                    Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
                }
            }
        }
    }
}
