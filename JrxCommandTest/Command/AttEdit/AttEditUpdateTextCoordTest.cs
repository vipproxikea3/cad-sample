﻿#if _IJCAD_
using GrxCAD.Runtime;
#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
#endif
using System;
using System.Reflection;
using JrxCad.Utility;
using Exception = Autodesk.AutoCAD.Runtime.Exception;

namespace JrxCadTest.Command.AttEdit
{
    public partial class CoordTest
    {
        //Case: update text current page
        [CommandMethod("JrxCommandTest", "SmxSmxAttEditUpdateTextCoordTest1", CommandFlags.Modal)]
        public void AttEditUpdateTextCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                var (resOptBlockRef, optBlockRef, initialViewModel, cmd) = CreateMock();
                UpdateTextInPage(initialViewModel);
                //initialViewModel.BtnOk_Click(sender: new object(), e: new EventArgs());
                PropertyTest.AttEditAllPropertyTest(resOptBlockRef.ObjectId, initialViewModel);
                initialViewModel.DisposeAll();
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        //Case: update all text
        [CommandMethod("JrxCommandTest", "SmxSmxAttEditUpdateTextCoordTest2", CommandFlags.Modal)]
        public void AttEditUpdateTextCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                var (resOptBlockRef, optBlockRef, initialViewModel, cmd) = CreateMock();
                UpdateAllText(initialViewModel);
                //initialViewModel.BtnOk_Click(sender: new object(), e: new EventArgs());
                PropertyTest.AttEditAllPropertyTest(resOptBlockRef.ObjectId, initialViewModel);
                initialViewModel.DisposeAll();
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
    }
}
