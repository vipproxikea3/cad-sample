﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
#endif
using System.Collections.Generic;
using System.Linq;
using Moq;
using JrxCad.Command.AttEdit;
using JrxCadTest.Utility;
using JrxCad.Utility;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exception = System.Exception;
using JrxCad.Command.AttEdit.ViewModel;

namespace JrxCadTest.Command.AttEdit
{
    public class Test
    {

        [CommandMethod("JrxCommandTest", "SmxSmxAttEditCmdTest", CommandFlags.Modal)]
        public void AttEditTest()
        {
            try
            {
                ////////////////////////////////////////
                // Setup User Input
                ////////////////////////////////////////
                var userInputMock = new Mock<AttEditCmd.IUserInput>();
                Point3d pickPoint = Point3d.Origin;
                var resOptBlockRef = CreateEx.CreatePromptEntityResult(
                    PromptStatus.OK,
                    "",
                    pickPoint);

                PromptEntityOptions optBlockRef = null;     // for PromptOptions Test

                userInputMock.Setup(x => x.SelectBlockRef(It.IsAny<PromptEntityOptions>()))
                    .Callback((PromptEntityOptions options) => optBlockRef = options)
                    .Returns(resOptBlockRef);

                ////////////////////////////////////////
                // Setup ViewModel
                ////////////////////////////////////////
                //void SetupViewModel(FormAttEditViewModel vm)
                //{
                //    vm.BlockRefId = resOptBlockRef.ObjectId;
                //}

                var windowInputMock = new Mock<AttEditCmd.IWindowInput>();
                FormAttEditViewModel initialViewModel = null;     // for Result Test
                //windowInputMock.Setup(x => x.FormAttEditShowDialog(It.IsAny<FormAttEditViewModel>()))
                //    .Callback((FormAttEditViewModel vm) =>
                //    {
                //        List<FormAttEdit.AttributeItem> attList = new List<FormAttEdit.AttributeItem>()
                //        {
                //            vm.AttributeItem1,
                //            vm.AttributeItem2,
                //            vm.AttributeItem3,
                //            vm.AttributeItem4,
                //            vm.AttributeItem5,
                //            vm.AttributeItem6,
                //            vm.AttributeItem7,
                //            vm.AttributeItem8,
                //            vm.AttributeItem9,
                //            vm.AttributeItem10,
                //            vm.AttributeItem11,
                //            vm.AttributeItem12,
                //            vm.AttributeItem13,
                //            vm.AttributeItem14,
                //            vm.AttributeItem15,
                //        };
                //        vm.ControlList = new List<UserControlAttEdit>();
                //        for (int i = 0; i < vm.AttMaxCountInPage; i++)
                //        {
                //            if (attList.ElementAtOrDefault(i) == null) break;
                //            UserControlAttEdit userControlAttEdit = new UserControlAttEdit();
                //            userControlAttEdit.Attribute.HasFields = attList[i].HasFields;
                //            userControlAttEdit.Attribute.FieldList = attList[i].FieldList;
                //            userControlAttEdit.Attribute.IsMText = attList[i].IsMText;
                //            userControlAttEdit.Attribute.Id = attList[i].Id;
                //            userControlAttEdit.Attribute.TextString = attList[i].TextString;
                //            userControlAttEdit.Attribute.MTextAttContents = attList[i].MTextAttContents;
                //            userControlAttEdit.Attribute.Tag = attList[i].Tag;
                //            userControlAttEdit.Attribute.Prompt = attList[i].Prompt;
                //            userControlAttEdit.Attribute.AttRef = attList[i].AttRef;
                //            vm.ControlList.Add(userControlAttEdit);
                //        }

                //        initialViewModel = vm.MemberwiseClone();
                //        SetupViewModel(vm);
                //    })
                //    .Returns(DialogResult.OK);

                ////////////////////////////////////////
                // Execute Command
                ////////////////////////////////////////
                var cmd = new AttEditCmd(userInputMock.Object, windowInputMock.Object);
                cmd.OnCommand();

                ////////////////////////////////////////
                // Test
                ////////////////////////////////////////

                MessageTest.AttEditMessageTest(cmd, optBlockRef);

                // Input ViewModel Test
                initialViewModel.IsNotNull();
                initialViewModel.BlockRefId.Equals(resOptBlockRef.ObjectId).IsTrue();
                PropertyTest.AttEditPropertyTest(resOptBlockRef.ObjectId, initialViewModel);
                PropertyTest.AttEditAllPropertyTest(resOptBlockRef.ObjectId, initialViewModel);

                initialViewModel.DisposeAll();
                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////

                userInputMock.VerifyAll();
                Util.Editor().WriteMessage("\nTest Completed!");
            }
            catch (Exception ex)
            {
                Util.Editor().WriteMessage($"\n{ex}");
            }
        }
    }
}
