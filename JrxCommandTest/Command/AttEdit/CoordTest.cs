﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
#endif
using System.Collections.Generic;
using JrxCad.Command.AttEdit;
using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Moq;
using JrxCad.Utility;
using JrxCadTest.Utility;
using JrxCad.Command.AttEdit.ViewModel;

namespace JrxCadTest.Command.AttEdit
{
    public partial class CoordTest
    {
        public (PromptEntityResult, PromptEntityOptions, FormAttEditViewModel, AttEditCmd) CreateMock()
        {
            ////////////////////////////////////////
            // Setup User Input
            ////////////////////////////////////////

            PromptPointOptions optPoint = new PromptPointOptions("\nPick the point in the block to test");
            PromptEntityResult resOptBlockRef;
            using (var tr = Util.StartTransaction())
            {
                while (true)
                {

                    var resOptPoint = Util.Editor().GetPoint(optPoint);
                    Point3d pickPoint = resOptPoint.Value;
                    resOptBlockRef = CreateEx.CreatePromptEntityResult(
                        PromptStatus.OK,
                        "",
                        pickPoint);

                    if (resOptBlockRef == null)
                    {
                        Util.Editor().WriteMessage("\nNo object found.");
                        continue;
                    }
                    break;
                }
            }

            var userInputMock = new Mock<AttEditCmd.IUserInput>();
            PromptEntityOptions optBlockRef = null; // for PromptOptions Test

            userInputMock.Setup(x => x.SelectBlockRef(It.IsAny<PromptEntityOptions>()))
                .Callback((PromptEntityOptions options) => optBlockRef = options)
                .Returns(resOptBlockRef);

            ////////////////////////////////////////
            // Setup ViewModel
            ////////////////////////////////////////
            //void SetupViewModel(FormAttEditViewModel vm)
            //{
            //    vm.BlockRefId = resOptBlockRef.ObjectId;
            //}

            var windowInputMock = new Mock<AttEditCmd.IWindowInput>();
            FormAttEditViewModel initialViewModel = null; // for Result Test
            //windowInputMock.Setup(x => x.FormAttEditShowDialog(It.IsAny<FormAttEditViewModel>()))
            //    .Callback((FormAttEditViewModel vm) =>
            //    {
            //        List<FormAttEdit.AttributeItem> attList = new List<FormAttEdit.AttributeItem>()
            //        {
            //            vm.AttributeItem1,
            //            vm.AttributeItem2,
            //            vm.AttributeItem3,
            //            vm.AttributeItem4,
            //            vm.AttributeItem5,
            //            vm.AttributeItem6,
            //            vm.AttributeItem7,
            //            vm.AttributeItem8,
            //            vm.AttributeItem9,
            //            vm.AttributeItem10,
            //            vm.AttributeItem11,
            //            vm.AttributeItem12,
            //            vm.AttributeItem13,
            //            vm.AttributeItem14,
            //            vm.AttributeItem15,
            //        };
            //        vm.ControlList = new List<UserControlAttEdit>();
            //        for (int i = 0; i < vm.AttMaxCountInPage; i++)
            //        {
            //            UserControlAttEdit userControlAttEdit = new UserControlAttEdit();
            //            if (attList.ElementAtOrDefault(i) != null)
            //            {
            //                userControlAttEdit.Attribute.HasFields = attList[i].HasFields;
            //                userControlAttEdit.Attribute.FieldList = attList[i].FieldList;
            //                userControlAttEdit.Attribute.IsMText = attList[i].IsMText;
            //                userControlAttEdit.Attribute.Id = attList[i].Id;
            //                userControlAttEdit.Attribute.TextString = attList[i].TextString;
            //                userControlAttEdit.Attribute.MTextAttContents = attList[i].MTextAttContents;
            //                userControlAttEdit.Attribute.Tag = attList[i].Tag;
            //                userControlAttEdit.Attribute.Prompt = attList[i].Prompt;
            //                userControlAttEdit.Attribute.AttRef = attList[i].AttRef;
            //            }
            //            vm.ControlList.Add(userControlAttEdit);
            //        }

            //        initialViewModel = vm.MemberwiseClone();
            //        SetupViewModel(vm);
            //    })
            //    .Returns(DialogResult.OK);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var cmd = new AttEditCmd(userInputMock.Object, windowInputMock.Object);
            cmd.OnCommand();
            initialViewModel.ReadAtt(isPrevClick: false);
            return (resOptBlockRef, optBlockRef, initialViewModel, cmd);
        }

        public void UpdateTextInPage(FormAttEditViewModel vm)
        {
            //foreach (var control in vm.ControlList)  // doing
            //{
            //    if (control.Attribute.HasFields || control.Attribute.IsMText) continue;
            //    control.Attribute.TextString += RandomString(new Random().Next(1, 5));
            //    if (control.Attribute.IsMText)
            //    {
            //        control.txtM.Text = control.Attribute.TextString;
            //    }
            //    else
            //    {
            //        control.txtD.Text = control.Attribute.TextString;
            //    }
            //}
        }

        public void UpdateAllText(FormAttEditViewModel vm)
        {
            foreach (var att in vm.AttList)
            {
                if (att.HasFields || att.IsMText) continue;
                att.TextString += RandomString(new Random().Next(1, 5));
            }
            vm.ReadAtt(isPrevClick: false);
        }

        public void UpdateMText(FormAttEditViewModel vm)
        {
            //foreach (var control in vm.ControlList) // doing
            //{
            //    if (!control.Attribute.IsMText) continue;
            //    control.BtnMtextEdit_Click(sender: new object(), e: new EventArgs());
            //    break;
            //}
        }

        private readonly static Random random = new Random((int)DateTime.Now.Ticks);
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }
    }
}
