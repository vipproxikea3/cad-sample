﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.EditorInput;
#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
#endif
using System.Reflection;
using JrxCad.Command.AttEdit;
using JrxCadTest.Utility;
using JrxCad.Utility;

namespace JrxCadTest.Command.AttEdit
{
    public class MessageTest
    {
        public static void AttEditMessageTest(AttEditCmd cmd, PromptEntityOptions optBlockRef)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageSelectBlockRef(optBlockRef);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void VerifyCommandMethod(AttEditCmd cmd)
        {
            VerifyEx.VerifyCommandMethod("JrxCommand", "SmxAttEditNew", CommandFlags.Modal | CommandFlags.NoNewStack, cmd.GetType());
        }

        public static void VerifyMessageSelectBlockRef(PromptEntityOptions optBlockRef)
        {
            VerifyEx.VerifyPromptEntityOptions(optBlockRef,
                new PromptEntityOptions($"\nSelect block reference")
                {
                    AllowNone = true
                });
        }
    }
}
