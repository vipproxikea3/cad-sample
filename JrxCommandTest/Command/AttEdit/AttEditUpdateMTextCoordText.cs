﻿#if _IJCAD_
using GrxCAD.Runtime;
#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
#endif
using JrxCad.Utility;
using System;
using System.Reflection;
using Exception = Autodesk.AutoCAD.Runtime.Exception;
namespace JrxCadTest.Command.AttEdit
{
    public partial class CoordTest
    {
        //Case: update mtext
        [CommandMethod("JrxCommandTest", "SmxSmxAttEditUpdateMTextCoordTest1", CommandFlags.Modal)]
        public void AttEditUpdateMTextCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                var (resOptBlockRef, optBlockRef, initialViewModel, cmd) = CreateMock();
                UpdateMText(initialViewModel);
                //initialViewModel.BtnOk_Click(sender: new object(), e: new EventArgs());
                PropertyTest.AttEditAllPropertyTest(resOptBlockRef.ObjectId, initialViewModel);
                initialViewModel.DisposeAll();
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }
    }
}
