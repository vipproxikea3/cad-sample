﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
#endif
using JrxCad.Helpers;
using JrxCad.Utility;
using System.Collections.Generic;

namespace JrxCadTest.Command.MinusWBlock
{
    public partial class MinusWBlockCmdTest
    {
        public ObjectId[] CreateObjectId()
        {
            var collection = new List<ObjectId>();
            var startPoint = new Point3d(1498.2755, 2100.9096, 0);
            var endPoint = new Point3d(2329.7824, 1866.0859, 0);
            var line = new Line(startPoint, endPoint);
            using (var tr = Util.StartTransaction())
            {
                tr.AddNewlyCreatedDBObject(line, true, tr.CurrentSpace());
                collection.Add(line.Id);
                tr.Commit();
            }
            return collection.ToArray();
        }
    }
}
