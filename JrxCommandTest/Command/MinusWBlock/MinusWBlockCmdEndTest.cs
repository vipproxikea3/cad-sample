﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.Runtime;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
#endif
using JrxCadTest.Utility;
using Moq;
using System;
using System.IO;
using JrxCad.Command.MinusWBlock;
using JrxCad.Command.WBlock.Model;
using static JrxCad.Command.WBlock.WBlockCmd;

namespace JrxCadTest.Command.MinusWBlock
{
    public partial class MinusWBlockCmdTest
    {
        /// <summary>
        /// UI-1 (Target block name) and press Ese,  "Cancel" => to End
        /// </summary>
        public void ExecuteMinusWBlockCmdTest01(string fileName)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup pick-first
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(0);

            // set object conversion mode
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // Setup PromptStringOptions
            minusWBlockMock.Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, fileName));

            // Setup Keyword
            minusWBlockMock.Setup(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.Cancel, string.Empty));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wBlockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();

        }

        /// <summary>
        /// UI-2 (specify insertion base point or [MOde]) and press Ese,  "Cancel" => to End
        /// </summary>
        public void ExecuteMinusWBlockCmdTest02(string fileName, double pointX, double pointY, double pointZ)
        {
            var pickedPoint = new Point3d(pointX, pointY, pointZ);
            var collection = CreateObjectId();

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup PICKFIRST
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(1);

            // Get Selection Set
            var selectionSet = SelectionSet.FromObjectIds(collection);

            // Setup PromptSelectionResult
            minusWBlockMock.Setup(x => x.GetPromptSelectionResult()).Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            // setup object locked
            minusWBlockMock.Setup(x => x.EntityOnLockedLayer(It.IsAny<Transaction>(), It.IsAny<Entity>())).Returns(false);

            // Setup Mode (Retain, Convert to Block, Delete)
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // Setup PromptStringOptions
            minusWBlockMock.Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                               .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, fileName));

            // Setup PromptPoint Result
            minusWBlockMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                           .Returns(CreateEx.CreatePromptPointResult(PromptStatus.Cancel, string.Empty, pickedPoint));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wBlockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();

        }

        /// <summary>
        /// UI-3 (select objects:) and press Ese,  "Cancel" => to End
        /// </summary>
        public void ExecuteMinusWBlockCmdTest03(string fileName, double pointX, double pointY, double pointZ)
        {
            var pickedPoint = new Point3d(pointX, pointY, pointZ);
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup PickFirst
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(0);

            // Setup Mode (Retain, Convert to Block, Delete)
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // setup promtresult (GetKeyWord)
            minusWBlockMock.Setup(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.None, string.Empty));

            // Setup PromptStringOptions
            minusWBlockMock.Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                               .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, fileName));

            // Setup PromptPoint Result
            minusWBlockMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                           .Returns(CreateEx.CreatePromptPointResult(PromptStatus.OK, string.Empty, pickedPoint));

            // Setup PromptSelectionOptions
            minusWBlockMock.Setup(x => x.GetSelection(It.IsAny<PromptSelectionOptions>()))
                           .Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.Cancel, null));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wBlockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();
        }


        /// <summary>
        ///  FN.UI-1 (Input file name) and press Ese,  "Cancel" => to End
        /// </summary>
        public void ExecuteMinusWBlockCmdTest04()
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup PICKFIRST
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(0);

            // Setup Mode (Retain, Convert to Block, Delete)
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // Setup PromptStringOptions
            minusWBlockMock.Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                               .Returns(CreateEx.CreatePromptResult(PromptStatus.Cancel, string.Empty));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wBlockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();
        }

        /// <summary>
        ///  FN.UI-2 (File name is exist) and press Ese,  "Cancel" => to End
        /// </summary>
        public void ExecuteMinusWBlockCmdTest05(string fileName)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup PICKFIRST
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(0);

            // Setup Mode (Retain, Convert to Block, Delete)
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // setup file name input
            minusWBlockMock.Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, fileName));

            // create file name in disk
            var localFile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + fileName;
            if (!File.Exists(localFile))
            {
                File.Create(localFile);
            }

            // setup get keyword
            minusWBlockMock.Setup(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.Cancel, string.Empty));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wBlockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////
            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();
        }

        /// <summary>
        /// DXF.UI-1, pressed Esc => Cancel command			
        /// </summary>
        public void ExecuteMinusWBlockCmdTest06(string fileName)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup PICKFIRST
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(0);

            // Setup Mode (Retain, Convert to Block, Delete)
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // setup file name input
            minusWBlockMock.Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, fileName));

            // Setup DXF.UI-1 = promptstatus = cancel
            minusWBlockMock.Setup(x => x.GetInteger(It.IsAny<MinusWBlockCmd.IntegerOpts>()))
                           .Returns(CreateEx.CreatePromptIntegerResult(PromptStatus.Cancel, string.Empty, 0));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wBlockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////
            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();
        }

        /// <summary>
        /// DXF.UI-2, Pressed Ese => Cancel command
        /// </summary>
        public void ExecuteMinusWBlockCmdTest07(string fileName)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup PICKFIRST
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(0);

            // Setup Mode (Retain, Convert to Block, Delete)
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // setup file name input
            minusWBlockMock.Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, fileName));

            // Setup DXF.UI-1 = promptstatus = keyword, string result = 0
            minusWBlockMock.Setup(x => x.GetInteger(It.IsAny<MinusWBlockCmd.IntegerOpts>()))
                           .Returns(CreateEx.CreatePromptIntegerResult(PromptStatus.Keyword, "P", 0));

            // Setup DXF.UI-2 = promtstatus = cancel
            minusWBlockMock.Setup(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.Cancel, string.Empty));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wBlockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////
            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();
        }

        /// <summary>
        /// MD.UI-1, pressed Esc => Cancel command
        /// </summary>
        /// <param name="fileName"></param>
        public void ExecuteMinuseWBlockCmdTest08(string fileName)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup PICKFIRST
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(0);

            // Setup Mode (Retain, Convert to Block, Delete)
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // setup file name input
            minusWBlockMock.Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, fileName));

            // setup get point (basepoint)
            minusWBlockMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                           .Returns(CreateEx.CreatePromptPointResult(PromptStatus.Keyword, "O", Point3d.Origin));

            minusWBlockMock.SetupSequence(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.None, string.Empty)) // return keyword in ValidTargetObject method
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.Cancel, string.Empty)); // return keyword in HandleModeObjects method

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wBlockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wBlockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////
            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wBlockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();
        }
    }
}
