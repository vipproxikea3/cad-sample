﻿#if _IJCAD_
using GrxCAD.Runtime;
#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
#endif
using JrxCad.Utility;
using JrxCadTest.Utility;
using System.Reflection;

namespace JrxCadTest.Command.MinusWBlock
{
    public class Test : BaseTest
    {

        private string _classFullName;
        private const string EXCEPTION_INFO_STRING = "Method Name: {0}, Exception: {1} ";

        /// <summary>
        /// Start test
        /// </summary>
        public override void OnStart()
        {
            var methodInfo = MethodBase.GetCurrentMethod();
            _classFullName = methodInfo.DeclaringType?.FullName;
            Util.Editor().WriteMessage($"\n{_classFullName?.ToUpper()} START!");
        }

        /// <summary>
        /// Finish test
        /// </summary>
        public override void OnFinish()
        {
            Util.Editor().WriteMessage($"\n{_classFullName.ToUpper()} Finish!");
        }

        /// <summary>
        ///  Write error message
        /// </summary>
        private void WriteErrorMessage(string methodName)
        {
            Util.Editor().WriteMessage($"\n{methodName} ERROR!!!!");
        }


        #region End command

        /// <summary>
        /// UI-1, End command
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest01", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest01", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest01", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest01()
        {
            try
            {
                OnStart();
                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdTest01("target.dwg");

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// UI-2, End command
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest02", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest02", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest02", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest02()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdTest02("newblock.dwg", 1498.2755, 2329.7824, 0);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// UI-3, End command
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest03", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest03", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest03", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest03()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdTest03("target.dwg", 1498.2755, 2329.7824, 0);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }


        /// <summary>
        /// FN.UI-1, End command
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest04", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest04", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest04", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest04()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdTest04();

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// FN.UI-2, End commad
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest05", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest05", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest05", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest05()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdTest05("file exist.dwg");

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// DXF.UI-1, End command
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest06", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest06", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest06", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest06()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdTest06("target.dxf");

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// DXF.UI-2, End command
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest07", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest07", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest07", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest07()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdTest07("target1.dxf");

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// MD.UI-1, End command
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest08", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest08", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest08", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest08()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinuseWBlockCmdTest08("test2.dwg");

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }
        #endregion

        #region Validate UI-1
        /// <summary>
        /// Block "abc" not found. CMDECHO=1
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest09", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest09", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest09", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest09()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdTest09("target09.dwg", "abc");

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }
        #endregion

        #region Validate UI-3

        /// <summary>
        /// UI-3 (Select object:) 未選択で右クリック
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest10", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest10", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest10", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest10()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdTest10("target10.dwg", 1498.2755, 2329.7824, 0, false);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// UI- (Select object:) 選択ありで右クリック
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest11", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest11", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest09", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest11()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdTest10("target11.dwg", 1498.2755, 2329.7824, 0, true);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        #endregion

        #region  validate FN.UI-1

        /// <summary>
        ///The specified path is not valid. Unsupported characters : " < > | : * ? \ also '.' can't be used as relative path. (CMDECHO=1)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest12", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest12", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest12", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest12()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdValidPath(@"Test\<a>.dwg");

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// File name can not be blank (CMDECHO=1)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest13", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest13", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest13", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest13()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdValidPath(string.Empty);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }


        /// <summary>
        /// The specified directory does not exist. Please verify the correct path was given. 'C:\Work\abc.dwg' (CMDECHO=1)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest14", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest14", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest14", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest14()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdValidPath(@"C:\Work\abc.dwg");

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }


        /// <summary>
        /// Unable to access the specified directory.'C:\Program Files' (CMDECHO=1)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest15", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest15", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest15", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest15()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdValidPath(@"C:\Program Files\abc.dwg");

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// MYDOCUMENTSPREFIX\a.dwg に出力される
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest16", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest16", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest16", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest16()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdOutput("a.dwg", 1498.2755, 2329.7824, 0);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        ///a.dwg(存在する)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest17", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest17", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest17", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest17()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdValidPath("a.dwg", true);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// MYDOCUMENTSPREFIX\b.dwg に出力される
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest18", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest18", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest18", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest18()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdOutput("b", 1498.2755, 2329.7824, 0);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// ｂ(b.dwg存在する)
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest19", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest19", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest19", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest19()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdValidPath("b", true);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }


        /// <summary>
        /// The specified path is long.Specify less than 259 characters. length : 268 [CMDECHO=1] パスの文字数オーバー
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest20", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest20", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest20", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest20()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                string path = @"C:\ProgramData\newblock1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111.dwg";
                minusWBlockCmdTest.ExecuteMinusWBlockCmdValidPath(path);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }


        /// <summary>
        /// 入力したFileがOpen済み
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest21", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest21", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest21", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest21()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdValidPath("a.dwg", false, true);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }


        /// <summary>
        /// 他のプロセスで使用されている
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest22", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest22", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest22", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest22()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdAccessTest(@"a.dwg");

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }

        /// <summary>
        /// 読み取り専用
        /// </summary>
#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest23", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest23", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommandTest", "MinusWBlockCmdTest23", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public void MinusWBlockCmdTest23()
        {
            try
            {
                OnStart();

                var minusWBlockCmdTest = new MinusWBlockCmdTest();
                minusWBlockCmdTest.ExecuteMinusWBlockCmdAccessTest(@"a.dwg", true);

                OnFinish();
            }
            catch (Exception ex)
            {
                WriteErrorMessage(string.Format(EXCEPTION_INFO_STRING, MethodBase.GetCurrentMethod().Name, ex.Message));
            }
        }
        #endregion
    }
}
