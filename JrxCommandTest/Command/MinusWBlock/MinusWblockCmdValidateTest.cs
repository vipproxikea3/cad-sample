﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Runtime;
using GrxCAD.Geometry;
using CADApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.DatabaseServices;
using GrxCAD.ApplicationServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using Autodesk.AutoCAD.ApplicationServices;
#endif

using Moq;
using static JrxCad.Command.WBlock.WBlockCmd;
using JrxCad.Command.MinusWBlock;
using JrxCad.Command.WBlock.Model;
using JrxCadTest.Utility;
using System;
using System.IO;
using System.Diagnostics;

namespace JrxCadTest.Command.MinusWBlock
{
    public partial class MinusWBlockCmdTest
    {

        /// <summary>
        /// validate UI-1: enter block name (CMDECHO=1)		
        /// </summary>
        /// <param name="fileName"></param>
        public void ExecuteMinusWBlockCmdTest09(string fileName, string blockName)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup pick-first
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(0);

            // set object conversion mode
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // Setup PromptStringOptions
            minusWBlockMock.Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, fileName));

            // Setup keyword (Target object)
            minusWBlockMock.SetupSequence(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, blockName)) // return block name not found
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.Cancel, string.Empty)); // end command to avoid repeat

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wblockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wblockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();
        }

        /// <summary>
        /// UI-3 (Select object 未選択で右クリック)
        /// </summary>
        /// <param name="fileName"></param>

        public void ExecuteMinusWBlockCmdTest10(string fileName, double pointX, double pointY, double pointZ, bool isRightSelection)
        {
            var pickedPoint = new Point3d(pointX, pointY, pointZ);
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup pick-first
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(0);

            // set object conversion mode
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // setup promtresult (GetKeyWord)
            minusWBlockMock.Setup(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.None, string.Empty));

            // Setup PromptStringOptions
            minusWBlockMock.Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                               .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, fileName));

            // Setup PromptPoint Result
            minusWBlockMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                           .Returns(CreateEx.CreatePromptPointResult(PromptStatus.OK, string.Empty, pickedPoint));

            // Setup right click and un-selection
            if (!isRightSelection)
            {
                // Setup PromptSelectionResult
                minusWBlockMock.Setup(x => x.GetSelection(It.IsAny<PromptSelectionOptions>()))
                               .Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.Error, null));
            }
            else
            {
                var objectIds = CreateObjectId();
                var selectionSet = SelectionSet.FromObjectIds(objectIds);

                // Setup PromptSelectionResult
                minusWBlockMock.Setup(x => x.GetSelection(It.IsAny<PromptSelectionOptions>()))
                                .Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

                // Setup locked layer
                minusWBlockMock.Setup(x => x.EntityOnLockedLayer(It.IsAny<Transaction>(), It.IsAny<Entity>())).Returns(false);
            }

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wblockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wblockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();

        }

        /// <summary>
        /// validate input path and file name
        /// </summary>
        public void ExecuteMinusWBlockCmdValidPath(string fileName, bool isExistFileName = false, bool isOpenFile = false)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup pick-first
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(0);

            // set object conversion mode
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // Setup PromptStringOptions
            minusWBlockMock.SetupSequence(x => x.GetString(It.IsAny<PromptStringOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, fileName)) // return file name by input
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.Cancel, string.Empty)); // end command, exit while


            var folder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (isOpenFile)
            {
                bool isOpened = false;
                foreach (Document doc in CADApp.DocumentManager)
                {
                    if (doc.Name.Equals(fileName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        isOpened = true;
                        break;
                    }
                }
                if (!isOpened)
                {
                    fileName = Path.Combine(folder, fileName);
                    CADApp.DocumentManager.Open(fileName);
                }

                // Setup GetKeyWord when file is exist in directory
                minusWBlockMock.Setup(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                               .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, "Y"));
            }

            // File name exist in Directory
            if (isExistFileName)
            {
                fileName = Path.Combine(folder, fileName);

                if (!File.Exists(fileName)) File.Create(fileName);
                // setup overide file.
                minusWBlockMock.Setup(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                               .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, "N")); // no overide
            }

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wblockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wblockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();
        }

        /// <summary>
        /// ouput dwg file
        /// </summary>
        public void ExecuteMinusWBlockCmdOutput(string fileName, double pointX, double pointY, double pointZ)
        {
            var pickedPoint = new Point3d(pointX, pointY, pointZ);
            var objectIds = CreateObjectId();
            var selectionSet = SelectionSet.FromObjectIds(objectIds);

            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup pick-first
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(0);

            // set object conversion mode
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // Setup PromptStringOptions
            minusWBlockMock.Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, fileName)); // return file name by input

            // Setup target object
            minusWBlockMock.Setup(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.None, string.Empty));
            // set basepoint
            minusWBlockMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                           .Returns(CreateEx.CreatePromptPointResult(PromptStatus.OK, string.Empty, pickedPoint));

            // setup selected object
            minusWBlockMock.Setup(x => x.GetSelection(It.IsAny<PromptSelectionOptions>()))
                           .Returns(CreateEx.CreatePromptSelectionResult(PromptStatus.OK, selectionSet));

            // setup un-lock layer
            minusWBlockMock.Setup(x => x.EntityOnLockedLayer(It.IsAny<Transaction>(), It.IsAny<Entity>())).Returns(false);

            // check exist file or not
            var filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), fileName);
            if (File.Exists(filePath)) File.Delete(filePath);

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wblockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wblockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();

        }


        /// <summary>
        ///  check dwg open in other process
        /// </summary>
        /// <param name="fileName"></param>
        public void ExecuteMinusWBlockCmdAccessTest(string fileName, bool isReadonly = false)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var minusWBlockMock = new Mock<IUserInputWBlock>(MockBehavior.Strict);

            // Setup pick-first
            minusWBlockMock.Setup(x => x.GetPickFirst()).Returns(0);

            // set object conversion mode
            minusWBlockMock.Setup(x => x.GetValueConfig()).Returns(WBlockModel.ObjectConversion.Convert);

            // Setup PromptStringOptions
            minusWBlockMock.SetupSequence(x => x.GetString(It.IsAny<PromptStringOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, fileName)) // return file name by input
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.Cancel, string.Empty)); // end command, exit while

            // Setup GetKeyWord when file is exist in directory
            minusWBlockMock.Setup(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                           .Returns(CreateEx.CreatePromptResult(PromptStatus.OK, "Y"));

            var filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), fileName);
            if (isReadonly)
            {
                // setup readonly file
                File.SetAttributes(filePath, FileAttributes.ReadOnly);
            }
            else
            {
                // setup open dwg in other process
                Process acadProcess = new Process();
                acadProcess.StartInfo.FileName = filePath;
                acadProcess.Start();

            }

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////
            var wblockCmd = new MinusWBlockCmd(minusWBlockMock.Object);
            wblockCmd.OnCommand();

            ////////////////////////////////////////
            // Test
            ////////////////////////////////////////

            //CommandMethod attribute test
#if DEBUG
#if _AutoCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());
#elif _IJCAD_
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());
#endif

#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor, wblockCmd.GetType());

#endif
            minusWBlockMock.VerifyAll();
        }
    }
}
