﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;

#endif
using JrxCad.Helpers;
using JrxCad.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace JrxCadTest.Command.MLineTest
{
    public partial class CoordTest
    {
        public ObjectId LastMLineId { get; set; }

        private void VerifyMLine(ObjectIdCollection actualIds, List<Point3d> lst)
        {
            LastMLineId = actualIds[0];
            using (var tr = Util.StartTransaction())
            using (var mLine = tr.GetObject<Mline>(actualIds[0], OpenMode.ForRead))
            {
                var index = 0;
                foreach (var pt in lst)
                {
                    var expectPoint = pt.TransformBy(CoordConverter.UcsToWcs());
                    var resPoint = mLine.VertexAt(index);
                    Point3dHelper.RoundCoordOfPoint(ref resPoint);
                    resPoint.Is(expectPoint);
                    index++;
                }

                //Check if mline is close
                if (mLine.IsClosed)
                {
                    mLine.VertexAt(mLine.NumberOfVertices).Is(lst[0]);
                }
            }
        }
    }
}
