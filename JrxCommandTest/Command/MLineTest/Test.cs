﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;

#endif
using JrxCad.Utility;
using JrxCadTest.Utility;
using System;

namespace JrxCadTest.Command.MLineTest
{
    public class Test : BaseTest
    {
        private PromptResult _resValueSet;
        private string _classFullName;

        public override void OnStart()
        {
            var optValueSet = new PromptKeywordOptions($"\nSpecify the coordinate system to test")
            {
                Keywords =
                {
                    { "W", "W", "Wcs" },
                    { "1", "1", "ucs1"},
                    { "2", "2", "ucs2"},
                    { "3", "3", "ucs3"},
                },
            };
            _resValueSet = Util.Editor().GetKeywords(optValueSet);
            var methodInfo = System.Reflection.MethodBase.GetCurrentMethod();
            _classFullName = methodInfo.DeclaringType.FullName;

            Util.Editor().WriteMessage($"\n{_classFullName.ToUpper()} START!");
        }
        public override void OnFinish()
        {
            Util.Editor().WriteMessage($"\n{_classFullName.ToUpper()} COMPLETED!");
        }

        private void ChangeCoordinateSystem(string coordStr)
        {
            var ucs = Matrix3d.Identity;
            switch (coordStr)
            {
                case "W": //WCS
                    //ucs = Matrix3d.Identity;
                    break;
                case "1": //UCS1
                    ucs = Matrix3d.Identity * Matrix3d.Displacement(new Vector3d(10, 20, 30));
                    break;
                case "2": //UCS2
                    ucs = Matrix3d.Identity * Matrix3d.Displacement(new Vector3d(10, 20, 30)) *
                          Matrix3d.Identity * Matrix3d.Rotation(Math.PI / 6, Vector3d.XAxis, Point3d.Origin);
                    break;
                case "3": //UCS3
                    ucs = Matrix3d.Identity *
                          Matrix3d.Displacement(new Vector3d(10, 20, 30)) *
                          Matrix3d.Rotation(Math.PI / 4, Vector3d.XAxis, Point3d.Origin) *
                          Matrix3d.Rotation(Math.PI / 6, Vector3d.YAxis, Point3d.Origin) *
                          Matrix3d.Rotation(Math.PI / 3, Vector3d.ZAxis, Point3d.Origin);
                    break;
                default:
                    return;
            }

            Util.Editor().CurrentUserCoordinateSystem = ucs;
        }

        [CommandMethod("JrxCommandTest", "SmxMLineTestPointPointPoint", CommandFlags.Modal)]
        public void MLineTestPointPointPoint()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    ChangeCoordinateSystem(_resValueSet.StringResult);

                    break;
                case "1": //UCS1
                    ChangeCoordinateSystem(_resValueSet.StringResult);

                    break;
                case "2": //UCS2
                    ChangeCoordinateSystem(_resValueSet.StringResult);

                    break;
                case "3": //UCS3
                    ChangeCoordinateSystem(_resValueSet.StringResult);

                    break;
                default:
                    return;
            }
            coordTest.MLinePointPointPointTest();

            //PropertyTest
            PropertyTest.MLinePropertyTest(coordTest.LastMLineId, false);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            OnFinish();
        }

        [CommandMethod("JrxCommandTest", "SmxMLineTestPointPointPointClose", CommandFlags.Modal)]
        public void MLineTestPointPointPointClose()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    ChangeCoordinateSystem(_resValueSet.StringResult);


                    break;
                case "1": //UCS1
                    ChangeCoordinateSystem(_resValueSet.StringResult);

                    break;
                case "2": //UCS2
                    ChangeCoordinateSystem(_resValueSet.StringResult);

                    break;
                case "3": //UCS3
                    ChangeCoordinateSystem(_resValueSet.StringResult);

                    break;
                default:
                    return;
            }

            coordTest.MLinePointPointPointCloseTest();
            //PropertyTest
            PropertyTest.MLinePropertyTest(coordTest.LastMLineId, true);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            OnFinish();
        }

        [CommandMethod("JrxCommandTest", "SmxMLineTestFlow", CommandFlags.Modal)]
        public void MLineTestFlow()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new FlowTest();

            coordTest.MLineFlowTest01();
            coordTest.MLineFlowTest02();
            coordTest.MLineFlowTest03();
            coordTest.MLineFlowTest04();
            coordTest.MLineFlowTest05();
            coordTest.MLineFlowTest06();
            coordTest.MLineFlowTest07();

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            OnFinish();
        }
    }
}
