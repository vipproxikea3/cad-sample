﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;
#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;

#endif
using JrxCad.Utility;
using Moq;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;
using System.Collections.Generic;
using JrxCad.Command.MLine;

namespace JrxCadTest.Command.MLineTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxMLinePointPointPointCloseTest", CommandFlags.Modal)]
        public void MLinePointPointPointCloseTest()
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;
            //Create list Point input
            var lstPointInput = new List<Point3d>();
            double zCoordinate = 0;
            lstPointInput.Add(new Point3d(0, 0, zCoordinate));
            lstPointInput.Add(new Point3d(100, 0, zCoordinate));
            lstPointInput.Add(new Point3d(50, 100, zCoordinate));

            try
            {
                ObjectIdCollection objectIdCollection = DoTestMLinePointPointPointClose(lstPointInput);
                VerifyMLine(objectIdCollection, lstPointInput);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private ObjectIdCollection DoTestMLinePointPointPointClose(List<Point3d> lstPoint)
        {
            ////////////////////////////////////////
            // Setup Mock
            ////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<MLineCmd.IUserInput>(MockBehavior.Strict);
            var notifyMock = new Mock<MLineCmd.IMLineNotify>(MockBehavior.Strict);
            var sequence = new MockSequence();

            notifyMock.Setup(x => x.AppendMLineCmM1(It.IsAny<string>()));

            // Setup startPoint
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", lstPoint[0]));

            // Setup nextPoints
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                    .Callback((PointDrawJigN.JigActions options) =>
                    {
                        options.LastValue = lstPoint[1].TransformBy(CoordConverter.UcsToWcs());
                        options.OnUpdate();
                    })
            .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", lstPoint[1].TransformBy(CoordConverter.UcsToWcs())));

            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                    .Callback((PointDrawJigN.JigActions options) =>
                    {
                        options.LastValue = lstPoint[2].TransformBy(CoordConverter.UcsToWcs());
                        options.OnUpdate();
                    })
            .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", lstPoint[2].TransformBy(CoordConverter.UcsToWcs())));

            // Terminate
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
            .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            // Setup AppendedMLines
            var appendedMLineIds = new ObjectIdCollection();
            notifyMock.Setup(x => x.Appended(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedMLineIds.Add(id));
            var cmd = new MLineCmd(userInputMock.Object, notifyMock.Object);
            cmd.OnCommand();

            return appendedMLineIds;
        }
    }
}
