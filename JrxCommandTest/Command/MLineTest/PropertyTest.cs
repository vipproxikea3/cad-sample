﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using GrxCAD.Colors;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JrxCad.Helpers;
using JrxCad.Utility;
using System.Reflection;

namespace JrxCadTest.Command.MLineTest
{
    public class PropertyTest
    {
        public static void MLinePropertyTest(ObjectId id, bool isClose)
        {
            var db = Util.Database();
            var currentMethod = MethodBase.GetCurrentMethod().Name;
            using (var tr = Util.StartTransaction())
            using (var mLine = tr.GetObject<Mline>(id, OpenMode.ForRead))
            {
                try
                {
                    mLine.Normal.Is(CoordConverter.UcsZAxis());
                    mLine.Justification.Is((MlineJustification)db.Cmljust);
                    mLine.Scale.Is(db.Cmlscale);
                    mLine.Style.Is(db.CmlstyleID);
                    mLine.IsClosed.Is(isClose);

                    mLine.Color.Is(db.Cecolor);
                    mLine.LineWeight.Is(db.Celweight);
                    mLine.LinetypeScale.Is(db.Celtscale);
                    mLine.LinetypeId.Is(db.Celtype);
                    mLine.PlotStyleNameId.Is(db.PlotStyleNameId);
#if _AutoCAD_
                    mLine.Transparency.Is(db.Cetransparency);
#elif _IJCAD_
                    var transparency = SystemVariable.GetString("CETRANSPARENCY");
                    byte resTransparency;
                    if (byte.TryParse(transparency, out resTransparency))
                    {
                        mLine.Transparency.Is(new Transparency((byte)(255 * (100 - resTransparency) / 100)));
                    }
                    else
                    {
                        if (transparency.ToLower() == "bylayer")
                        {
                            mLine.Transparency.Is(new Transparency(TransparencyMethod.ByLayer));
                        }
                        else
                        {
                            mLine.Transparency.Is(new Transparency(TransparencyMethod.ByBlock));
                        }
                    }
#endif
                    Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
                }
                catch (Exception)
                {
                    Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
                }
            }
        }
    }
}
