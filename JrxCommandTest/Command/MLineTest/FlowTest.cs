﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;
using Application = GrxCAD.ApplicationServices.Application;

#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using Application = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Registry = Autodesk.AutoCAD.Runtime.Registry;

#endif
using JrxCad.Utility;
using Moq;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;
using JrxCad.Command.MLine;
using Microsoft.Win32;

namespace JrxCadTest.Command.MLineTest
{
    public class FlowTest
    {
        [CommandMethod("JrxCommandTest", "SmxMLineFlowTest01", CommandFlags.Modal)]
        public void MLineFlowTest01()
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;

            try
            {
                DoMlineFlowTest01();
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxMLineFlowTest02", CommandFlags.Modal)]
        public void MLineFlowTest02()
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;

            try
            {
                DoMlineFlowTest02();
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxMLineFlowTest03", CommandFlags.Modal)]
        public void MLineFlowTest03()
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;

            try
            {
                DoMlineFlowTest03();
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxMLineFlowTest04", CommandFlags.Modal)]
        public void MLineFlowTest04()
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;

            try
            {
                DoMlineFlowTest04();
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxMLineFlowTest05", CommandFlags.Modal)]
        public void MLineFlowTest05()
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;

            try
            {
                DoMlineFlowTest05();
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxMLineFlowTest06", CommandFlags.Modal)]
        public void MLineFlowTest06()
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;

            try
            {
                DoMlineFlowTest06();
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxMLineFlowTest07", CommandFlags.Modal)]
        public void MLineFlowTest07()
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;

            try
            {
                DoMlineFlowTest07();
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoMlineFlowTest01()
        {
            var userInputMock = new Mock<MLineCmd.IUserInput>(MockBehavior.Strict);
            var notifyMock = new Mock<MLineCmd.IMLineNotify>(MockBehavior.Strict);
            var sequence = new MockSequence();
            // Start
            var cMM1 = "";
            notifyMock.InSequence(sequence).Setup(x => x.AppendMLineCmM1(It.IsAny<string>()))
                .Callback((string cM) =>
                {
                    cMM1 = cM;
                });
            // P1 => Specify point
            PromptPointOptions optP1 = null;
            var firstPoint = Point3d.Origin;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optP1 = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", firstPoint));

            // NP1 => Enter
            JigPromptPointOptions optNP1 = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                    {
                        optNP1 = options.Options;
                        optNP1.DefaultValue = new Point3d();
                    })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.None, "", Point3d.Origin));


            var cmd = new MLineCmd(userInputMock.Object, notifyMock.Object);
            cmd.OnCommand();

            MessageTest.MlineMessageForFlowTest01(cmd, cMM1, optP1, optNP1, firstPoint);
        }

        private void DoMlineFlowTest02()
        {
            var userInputMock = new Mock<MLineCmd.IUserInput>(MockBehavior.Strict);
            var notifyMock = new Mock<MLineCmd.IMLineNotify>(MockBehavior.Strict);
            var sequence = new MockSequence();

            var cMM1 = "";
            notifyMock.InSequence(sequence).Setup(x => x.AppendMLineCmM1(It.IsAny<string>()))
                .Callback((string cM) =>
                {
                    cMM1 = cM;
                });

            PromptPointOptions optP1 = null;
            var firstPoint = Point3d.Origin;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optP1 = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", firstPoint));

            // NP1 => Specify point
            JigPromptPointOptions optNP1 = null;
            var secondtPoint = new Point3d(10, 10, 0);
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                 .Callback((PointDrawJigN.JigActions options) =>
                  {
                      optNP1 = options.Options;
                      optNP1.DefaultValue = new Point3d();
                      optNP1.BasePoint = firstPoint;
                      options.LastValue = secondtPoint.TransformBy(CoordConverter.UcsToWcs());
                      options.OnUpdate();
                  })
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", secondtPoint.TransformBy(CoordConverter.UcsToWcs())));

            // NP2 => Enter
            JigPromptPointOptions optNP2 = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                 .Callback((PointDrawJigN.JigActions options) =>
                 {
                     optNP2 = options.Options;
                     optNP2.BasePoint = secondtPoint;
                     optNP2.DefaultValue = new Point3d();
                 })
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.None, "", Point3d.Origin));


            notifyMock.Setup(x => x.Appended(It.IsAny<ObjectId>()));
            var cmd = new MLineCmd(userInputMock.Object, notifyMock.Object);
            cmd.OnCommand();

            MessageTest.MlineMessageForFlowTest02(cmd, optNP1, optNP2, firstPoint, secondtPoint);
        }

        private void DoMlineFlowTest03()
        {
            var userInputMock = new Mock<MLineCmd.IUserInput>(MockBehavior.Strict);
            var notifyMock = new Mock<MLineCmd.IMLineNotify>(MockBehavior.Strict);
            var sequence = new MockSequence();

            var cMM1 = "";
            notifyMock.InSequence(sequence).Setup(x => x.AppendMLineCmM1(It.IsAny<string>()))
                .Callback((string cM) =>
                {
                    cMM1 = cM;
                });

            PromptPointOptions optP1 = null;
            var firstPoint = Point3d.Origin;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optP1 = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", firstPoint));

            JigPromptPointOptions optNP1 = null;
            var secondtPoint = new Point3d(10, 10, 0);
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                 .Callback((PointDrawJigN.JigActions options) =>
                 {
                     optNP1 = options.Options;
                     optNP1.DefaultValue = new Point3d();
                     options.LastValue = secondtPoint.TransformBy(CoordConverter.UcsToWcs());
                     options.OnUpdate();
                 })
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", secondtPoint.TransformBy(CoordConverter.UcsToWcs())));

            // NP2 => Specify point
            JigPromptPointOptions optNP2 = null;
            var thirdPoint = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                 .Callback((PointDrawJigN.JigActions options) =>
                 {
                     optNP2 = options.Options;
                     optNP2.DefaultValue = new Point3d();
                     optNP2.BasePoint = secondtPoint;
                     options.LastValue = thirdPoint.TransformBy(CoordConverter.UcsToWcs());
                 })
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", thirdPoint.TransformBy(CoordConverter.UcsToWcs())));

            // NP3 => Undo
            JigPromptPointOptions optNP3 = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optNP3 = options.Options;
                    optNP3.BasePoint = thirdPoint;
                    optNP3.DefaultValue = new Point3d();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "U", Point3d.Origin));

            // NP2 => Undo
            JigPromptPointOptions optNP2_1 = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                 .Callback((PointDrawJigN.JigActions options) =>
                 {
                     optNP2_1 = options.Options;
                     optNP2_1.BasePoint = secondtPoint;
                     optNP2_1.DefaultValue = new Point3d();
                 })
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "U", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                 .Callback((PointDrawJigN.JigActions options) =>
                 {
                     options.LastValue = secondtPoint.TransformBy(CoordConverter.UcsToWcs());
                 })
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", secondtPoint.TransformBy(CoordConverter.UcsToWcs())));

            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    options.LastValue = thirdPoint.TransformBy(CoordConverter.UcsToWcs());
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", thirdPoint.TransformBy(CoordConverter.UcsToWcs())));

            // NP3 => Specify point
            JigPromptPointOptions optNP3_1 = null;
            var fouthPoint = new Point3d(-10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optNP3_1 = options.Options;
                    optNP3_1.DefaultValue = new Point3d();
                    optNP3_1.BasePoint = thirdPoint;
                    options.LastValue = fouthPoint.TransformBy(CoordConverter.UcsToWcs());
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", fouthPoint.TransformBy(CoordConverter.UcsToWcs())));

            JigPromptPointOptions optNP3_2 = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optNP3_2 = options.Options;
                    optNP3_2.BasePoint = fouthPoint;
                    optNP3_2.DefaultValue = new Point3d();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "U", Point3d.Origin));

            // NP3 => Close
            JigPromptPointOptions optNP3_3 = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optNP3_3 = options.Options;
                    optNP3_3.BasePoint = thirdPoint;
                    optNP3_3.DefaultValue = new Point3d();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            notifyMock.Setup(x => x.Appended(It.IsAny<ObjectId>()));
            var cmd = new MLineCmd(userInputMock.Object, notifyMock.Object);
            cmd.OnCommand();

            MessageTest.MlineMessageForFlowTest03(cmd, optNP2, optNP3, optNP2_1, optNP3_1, optNP3_2, optNP3_3, secondtPoint, thirdPoint, fouthPoint);
        }

        private void DoMlineFlowTest04()
        {
            var userInputMock = new Mock<MLineCmd.IUserInput>(MockBehavior.Strict);
            var notifyMock = new Mock<MLineCmd.IMLineNotify>(MockBehavior.Strict);
            var sequence = new MockSequence();

            notifyMock.Setup(x => x.AppendMLineCmM1(It.IsAny<string>()));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                 .Callback((PointDrawJigN.JigActions options) =>
                 {
                     options.LastValue = new Point3d(10, 10, 0).TransformBy(CoordConverter.UcsToWcs());
                     options.OnUpdate();
                 })
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", new Point3d(10, 10, 0).TransformBy(CoordConverter.UcsToWcs())));

            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    options.LastValue = (new Point3d(10, 0, 0)).TransformBy(CoordConverter.UcsToWcs());
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", (new Point3d(10, 0, 0)).TransformBy(CoordConverter.UcsToWcs())));

            // NP3 => Enter
            JigPromptPointOptions optNP3 = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquirePoint(It.IsAny<PointDrawJigN.JigActions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optNP3 = options.Options;
                    optNP3.DefaultValue = new Point3d();
                    optNP3.BasePoint = new Point3d(10, 0, 0);
                    options.LastValue = (new Point3d(100, -50, 0)).TransformBy(CoordConverter.UcsToWcs());
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.None, "", Point3d.Origin));

            notifyMock.Setup(x => x.Appended(It.IsAny<ObjectId>()));

            var cmd = new MLineCmd(userInputMock.Object, notifyMock.Object);
            cmd.OnCommand();

            MessageTest.MlineMessageForFlowTest04(cmd, optNP3, new Point3d(10, 0, 0));
        }

        private void DoMlineFlowTest05()
        {
            var userInputMock = new Mock<MLineCmd.IUserInput>(MockBehavior.Strict);
            var notifyMock = new Mock<MLineCmd.IMLineNotify>(MockBehavior.Strict);
            var sequence = new MockSequence();

            notifyMock.InSequence(sequence).Setup(x => x.AppendMLineCmM1(It.IsAny<string>()));

            // OP => Justification
            PromptPointOptions optP1 = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optP1 = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "J", Point3d.Origin));


            // OP1 => Top(T)
            PromptKeywordOptions optOP1 = null;
            string justifyTop = "top";
            userInputMock.InSequence(sequence).Setup(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                .Callback((PromptKeywordOptions options) =>
                {
                    optOP1 = options;
                    optOP1.Keywords.Default = "zero";
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, justifyTop, Point3d.Origin));

            var cMM1Top = "";
            notifyMock.InSequence(sequence).Setup(x => x.AppendMLineCmM1(It.IsAny<string>()))
                .Callback((string cM) =>
                {
                    cMM1Top = cM;
                });

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "J", Point3d.Origin));

            // OP1 => Zero(Z)
            PromptKeywordOptions optOP1_1 = null;
            var justifyZero = "zero";
            userInputMock.InSequence(sequence).Setup(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
                .Callback((PromptKeywordOptions options) =>
                {
                    optOP1_1 = options;
                    optOP1_1.Keywords.Default = "top";
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, justifyZero, Point3d.Origin));

            var cMM1Zero = "";
            notifyMock.InSequence(sequence).Setup(x => x.AppendMLineCmM1(It.IsAny<string>()))
                .Callback((string cM) =>
                {
                    cMM1Zero = cM;
                });

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "J", Point3d.Origin));

            // OP1 => Bottom(B)
            PromptKeywordOptions optOP1_2 = null;
            var justifyBottom = "bottom";
            userInputMock.InSequence(sequence).Setup(x => x.GetKeyword(It.IsAny<PromptKeywordOptions>()))
               .Callback((PromptKeywordOptions options) =>
               {
                   optOP1_2 = options;
                   optOP1_2.Keywords.Default = "zero";
               })
               .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, justifyBottom, Point3d.Origin));

            var cMM1Bottom = "";
            notifyMock.InSequence(sequence).Setup(x => x.AppendMLineCmM1(It.IsAny<string>()))
                .Callback((string cM) =>
                {
                    cMM1Bottom = cM;
                });

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optP1 = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.None, "", Point3d.Origin));

            var cmd = new MLineCmd(userInputMock.Object, notifyMock.Object);
            cmd.OnCommand();

            MessageTest.MlineMessageForFlowTest05(cmd, optP1, optOP1, optOP1_1, optOP1_2, cMM1Top, cMM1Zero, cMM1Bottom);
        }

        private void DoMlineFlowTest06()
        {
            var userInputMock = new Mock<MLineCmd.IUserInput>(MockBehavior.Strict);
            var notifyMock = new Mock<MLineCmd.IMLineNotify>(MockBehavior.Strict);
            var sequence = new MockSequence();

            notifyMock.Setup(x => x.AppendMLineCmM1(It.IsAny<string>()));

            // OP => Scale(S)
            PromptPointOptions optP1 = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optP1 = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "S", Point3d.Origin));

            // OP2 => Specify value
            PromptDoubleOptions optOP2 = null;
            double scaleValue = 30;
            userInputMock.InSequence(sequence).Setup(x => x.GetDouble(It.IsAny<PromptDoubleOptions>()))
                .Callback((PromptDoubleOptions options) =>
                {
                    optOP2 = options;
                    options.DefaultValue = scaleValue;
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", scaleValue));

            var cMM1Scale = "";
            notifyMock.InSequence(sequence).Setup(x => x.AppendMLineCmM1(It.IsAny<string>()))
                .Callback((string cM) =>
                {
                    cMM1Scale = cM;
                });

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
             .Callback((PromptPointOptions options) => optP1 = options)
             .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.None, "", Point3d.Origin));

            var cmd = new MLineCmd(userInputMock.Object, notifyMock.Object);
            cmd.OnCommand();

            MessageTest.MlineMessageForFlowTest06(cmd, optP1, optOP2, cMM1Scale);
        }

        private void DoMlineFlowTest07()
        {
            var initialDirectory = CheckInitial();

            var userInputMock = new Mock<MLineCmd.IUserInput>(MockBehavior.Strict);
            var notifyMock = new Mock<MLineCmd.IMLineNotify>(MockBehavior.Strict);
            var sequence = new MockSequence();

            notifyMock.InSequence(sequence).Setup(x => x.AppendMLineCmM1(It.IsAny<string>()));

            // OP => Style(ST)
            PromptPointOptions optP1 = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optP1 = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "ST", Point3d.Origin));

            // OP3 => Input "?"
            PromptStringOptions optOP3 = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                .Callback((PromptStringOptions options) =>
                {
                    optOP3 = options;
                    optOP3.DefaultValue = "";
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "?", Point3d.Origin));

            var cMOP4 = "";
            notifyMock.InSequence(sequence).Setup(x => x.AppendMLineCmOp4(It.IsAny<string>()))
                 .Callback((string cM) =>
                 {
                     cMOP4 = cM;
                 });

            // OP3 => Input existing style
            PromptStringOptions optOP3_1 = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
               .Callback((PromptStringOptions options) =>
               {
                   optOP3_1 = options;
                   optOP3_1.DefaultValue = "";
               })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "standard", Point3d.Origin));

            var cMM1 = "";
            notifyMock.InSequence(sequence).Setup(x => x.AppendMLineCmM1(It.IsAny<string>()))
                .Callback((string cM) => cMM1 = cM);

            PromptPointOptions optP1_1 = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optP1_1 = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "ST", Point3d.Origin));

            // OP3 => Input unknown style
            PromptStringOptions optOP3_2 = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                .Callback((PromptStringOptions options) =>
                {
                    optOP3_2 = options;
                    optOP3_2.DefaultValue = "";
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "mystyle", Point3d.Origin));

            // DLG1 => Open
            PromptOpenFileOptions optDLG1 = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetFileName(It.IsAny<PromptOpenFileOptions>()))
                .Callback((PromptOpenFileOptions options) => optDLG1 = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, @"C:\Users\nqanh\AppData\Roaming\Autodesk\AutoCAD 2022\R24.1\enu\Support\acad.mln", Point3d.Origin));

            PromptStringOptions optOP3_3 = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetString(It.IsAny<PromptStringOptions>()))
                .Callback((PromptStringOptions options) =>
                {
                    optOP3_3 = options;
                    optOP3_3.DefaultValue = "";
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.None, "", Point3d.Origin));

            notifyMock.InSequence(sequence).Setup(x => x.AppendMLineCmM1(It.IsAny<string>()));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                 .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.None, "", Point3d.Origin));

            var cmd = new MLineCmd(userInputMock.Object, notifyMock.Object);
            cmd.OnCommand();

            MessageTest.MlineMessageForFlowTest07(cmd, optP1, optOP3, cMOP4, optOP3_1, cMM1, optP1_1, optOP3_2, optDLG1, initialDirectory, optOP3_3);
        }

        private bool CheckInitial()
        {
            var Key = Registry.CurrentUser;
            var productKey = HostApplicationServices.Current.UserRegistryProductRootKey;
            var profile = Application.GetSystemVariable("CPROFILE");
            var subKey = $@"{productKey}\Profiles\{profile}" + @"\Dialogs\MlineFileDialog";
            var key = Key.OpenSubKey(subKey);
            var value = key?.GetValue("InitialDirectory");
            var initialDirectory = value?.ToString();

            return initialDirectory != null;
        }
    }
}
