﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.Runtime;
using Application = GrxCAD.ApplicationServices.Application;
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using Application = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
using Registry = Autodesk.AutoCAD.Runtime.Registry;

#endif
using JrxCad.Command.MLine;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using Microsoft.Win32;
using System.IO;

namespace JrxCadTest.Command.MLineTest
{
    public class MessageTest
    {
        public static void MlineMessageForFlowTest01(MLineCmd cmd, string cMM1, PromptPointOptions optP1, JigPromptPointOptions optNP1, Point3d firstPoint)
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyCMM1(cMM1);
            VerifyP1Option(optP1);
            VerifyNP1Option(optNP1, firstPoint);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void MlineMessageForFlowTest02(MLineCmd cmd, JigPromptPointOptions optNP1, JigPromptPointOptions optNP2, Point3d firstPoint, Point3d secondPoint)
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyNP1Option(optNP1, firstPoint);
            VerifyNP2Option(optNP2, secondPoint);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void MlineMessageForFlowTest03(MLineCmd cmd, JigPromptPointOptions optNP2, JigPromptPointOptions optNP3, JigPromptPointOptions optNP2_1, JigPromptPointOptions optNP3_1, JigPromptPointOptions optNP3_2, JigPromptPointOptions optNP3_3, Point3d secondPoint, Point3d thirdPoint, Point3d fouthPoint)
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyNP2Option(optNP2, secondPoint);
            VerifyNP3Option(optNP3, thirdPoint);
            VerifyNP2Option(optNP2_1, secondPoint);
            VerifyNP3Option(optNP3_1, thirdPoint);
            VerifyNP3Option(optNP3_2, fouthPoint);
            VerifyNP3Option(optNP3_3, thirdPoint);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void MlineMessageForFlowTest04(MLineCmd cmd, JigPromptPointOptions optNP3, Point3d point)
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyNP3Option(optNP3, point);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void MlineMessageForFlowTest05(MLineCmd cmd, PromptPointOptions optP1, PromptKeywordOptions optOP1, PromptKeywordOptions optOP1_1, PromptKeywordOptions optOP1_2, string cMM1Top, string cMM1Zero, string cMM1Bottom)
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyP1Option(optP1);
            VerifyOP1Option(optOP1, "zero");
            VerifyOP1Option(optOP1_1, "top");
            VerifyOP1Option(optOP1_2, "bottom");
            VerifyCMM1(cMM1Top);
            VerifyCMM1(cMM1Zero);
            VerifyCMM1(cMM1Bottom);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void MlineMessageForFlowTest06(MLineCmd cmd, PromptPointOptions optP1, PromptDoubleOptions optOP2, string cMM1Scale)
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyP1Option(optP1);
            VerifyOP2Option(optOP2);
            VerifyCMM1(cMM1Scale);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void MlineMessageForFlowTest07(MLineCmd cmd, PromptPointOptions optP1, PromptStringOptions optOP3, string cMOP4, PromptStringOptions optOP3_1, string cMM1, PromptPointOptions optP1_1, PromptStringOptions optOP3_2, PromptOpenFileOptions optDLG1, bool initialDirectory, PromptStringOptions optOP3_3)
        {
            var currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyP1Option(optP1);
            VerifyOP3Option(optOP3);
            VerifyCMOP4(cMOP4);
            VerifyOP3Option(optOP3_1);
            VerifyCMM1(cMM1);
            VerifyP1Option(optP1_1);
            VerifyOP3Option(optOP3_2);
            VerifyDLG1Option(optDLG1, initialDirectory);
            VerifyOP3Option(optOP3_3);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void VerifyCommandMethod(MLineCmd cmd)
        {
            VerifyEx.VerifyCommandMethod("JrxCommand", "SmxMLine", CommandFlags.Modal | CommandFlags.NoNewStack, cmd.GetType());
        }

        public static void VerifyCMM1(string cMM1)
        {
            if (cMM1 != MLineRes.CM_M1)
            {
                throw new Exception();
            }

        }

        public static void VerifyCMOP4(string cMOP4)
        {
            if (cMOP4 != MLineRes.CM_OP4)
            {
                throw new Exception();
            }

        }

        public static void VerifyP1Option(PromptPointOptions option)
        {
            VerifyEx.VerifyPromptPointOptions(option,
               new PromptPointOptions($"\n{MLineRes.UI_P1}")
               {
                   AllowNone = true,
                   Keywords = {
                       { "J", MLineRes.UI_P1_Justification, MLineRes.UI_P1_Justification},
                       {"S", MLineRes.UI_P1_Scale, MLineRes.UI_P1_Scale },
                       { "ST", MLineRes.UI_P1_Style, MLineRes.UI_P1_Style}}
               });
        }

        public static void VerifyNP1Option(JigPromptPointOptions option, Point3d point)
        {
            VerifyEx.VerifyJigPromptPointOptions(option,
                new JigPromptPointOptions($"\n{MLineRes.UI_NP1}")
                {
                    UseBasePoint = true,
                    BasePoint = point.TransformBy(CoordConverter.UcsToWcs()),
                    DefaultValue = new Point3d(),
                    UserInputControls = UserInputControls.NullResponseAccepted | UserInputControls.UseBasePointElevation,
                    Cursor = CursorType.Crosshair,
                    AppendKeywordsToMessage = false,
                    Keywords = { { "U", MLineRes.UI_Key_Undo, MLineRes.UI_Key_Undo } }
                });
        }

        public static void VerifyNP2Option(JigPromptPointOptions option, Point3d point)
        {
            VerifyEx.VerifyJigPromptPointOptions(option,
                new JigPromptPointOptions($"\n{MLineRes.UI_NP2}")
                {
                    UseBasePoint = true,
                    BasePoint = point.TransformBy(CoordConverter.UcsToWcs()),
                    DefaultValue = new Point3d(),
                    UserInputControls = UserInputControls.NullResponseAccepted | UserInputControls.UseBasePointElevation,
                    Cursor = CursorType.Crosshair,
                    Keywords = { { "U", MLineRes.UI_Key_Undo, MLineRes.UI_Key_Undo } }
                });
        }

        public static void VerifyNP3Option(JigPromptPointOptions option, Point3d point)
        {
            VerifyEx.VerifyJigPromptPointOptions(option,
                new JigPromptPointOptions($"\n{MLineRes.UI_NP3}")
                {
                    UseBasePoint = true,
                    BasePoint = point.TransformBy(CoordConverter.UcsToWcs()),
                    DefaultValue = new Point3d(),
                    UserInputControls = UserInputControls.NullResponseAccepted | UserInputControls.UseBasePointElevation,
                    Cursor = CursorType.Crosshair,
                    Keywords = {
                        { "C", MLineRes.UI_Key_Close, MLineRes.UI_Key_Close },
                        { "U", MLineRes.UI_Key_Undo, MLineRes.UI_Key_Undo }}
                });
        }

        public static void VerifyOP1Option(PromptKeywordOptions option, string defStr)
        {
            var optJust = new PromptKeywordOptions($"\n{MLineRes.UI_OP1}")
            {
                Keywords = {
                    { "top", MLineRes.UI_OP1_Top, MLineRes.UI_OP1_Top },
                    {"zero", MLineRes.UI_OP1_Zero, MLineRes.UI_OP1_Zero },
                    {"bottom", MLineRes.UI_OP1_Bottom, MLineRes.UI_OP1_Bottom} }
            };
            optJust.Keywords.Default = defStr;

            VerifyEx.VerifyPromptKeywordOptions(option, optJust);
        }

        public static void VerifyOP2Option(PromptDoubleOptions option)
        {
            VerifyEx.VerifyPromptDoubleOptions(option, new PromptDoubleOptions($"\n{MLineRes.UI_OP2}")
            {
                DefaultValue = option.DefaultValue
            });

        }

        public static void VerifyOP3Option(PromptStringOptions option)
        {
            VerifyEx.VerifyPromptStringOptions(option, new PromptStringOptions($"\n{MLineRes.UI_OP3}")
            { DefaultValue = "" });
        }

        public static void VerifyDLG1Option(PromptOpenFileOptions option, bool initialDirectory)
        {
            option.DialogCaption.Is(MLineRes.DialogCaption);
            option.DialogName.Is("MlineFileDialog");
            option.Filter.Is("|*.mln");
            option.AllowUrls.Is(false);

            if (initialDirectory)
            {
                var Key = Registry.CurrentUser;
                var productKey = HostApplicationServices.Current.UserRegistryProductRootKey;
                var profile = Application.GetSystemVariable("CPROFILE");
                var subKey = $@"{productKey}\Profiles\{profile}" + @"\Dialogs\MlineFileDialog";
                var key = Key.OpenSubKey(subKey);
                var value1 = key?.GetValue("FileNameMRU0");
                var fileNameMRU0 = value1?.ToString();
                if (fileNameMRU0 != null)
                {
                    option.InitialFileName.Is(fileNameMRU0);
                    option.InitialDirectory.IsNull();
                }
                else
                {
                    option.InitialFileName.IsNull();
                    option.InitialDirectory.IsNull();
                }
            }
            else
            {
                var supportPath = SystemVariable.GetString("ACADPREFIX");

                foreach (var item in supportPath.Split(';'))
                {
                    var dir = new DirectoryInfo(item);
                    var filePaths = dir.GetFiles("*.mln");
                    if (filePaths != null)
                    {
                        option.InitialDirectory.Is(item + "\\");
                        option.InitialFileName.Is(filePaths[0].Name);
                        return;
                    }
                }
                option.InitialFileName.IsNull();
                option.InitialDirectory.IsNull();
            }
        }
    }
}
