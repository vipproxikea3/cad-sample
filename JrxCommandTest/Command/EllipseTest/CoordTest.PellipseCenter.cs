﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxPellipseCenterCoordTest1", CommandFlags.Modal)]
        public void PellipseCenterCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;

                DoTestPellipseCenter(pt1, pt2, distance);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseCenterCoordTest2", CommandFlags.Modal)]
        public void PellipseCenterCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;

                DoTestPellipseCenter(pt1, pt2, distance);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseCenterCoordTest3", CommandFlags.Modal)]
        public void PellipseCenterCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;

                DoTestPellipseCenter(pt1, pt2, distance);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseCenterCoordTest4", CommandFlags.Modal)]
        public void PellipseCenterCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;

                DoTestPellipseCenter(pt1, pt2, distance);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseCenterCoordTest5", CommandFlags.Modal)]
        public void PellipseCenterCoordTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;

                DoTestPellipseCenter(pt1, pt2, distance);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseCenterCoordTest6", CommandFlags.Modal)]
        public void PellipseCenterCoordTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;

                DoTestPellipseCenter(pt1, pt2, distance);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseCenterCoordTest7", CommandFlags.Modal)]
        public void PellipseCenterCoordTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;

                DoTestPellipseCenter(pt1, pt2, distance);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseCenterCoordTest8", CommandFlags.Modal)]
        public void PellipseCenterCoordTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;

                DoTestPellipseCenter(pt1, pt2, distance);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseCenterCoordTest9", CommandFlags.Modal)]
        public void PellipseCenterCoordTest9()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;

                DoTestPellipseCenter(pt1, pt2, distance);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseCenterCoordTest10", CommandFlags.Modal)]
        public void PellipseCenterCoordTest10()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;

                DoTestPellipseCenter(pt1, pt2, distance);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseCenterCoordTest11", CommandFlags.Modal)]
        public void PellipseCenterCoordTest11()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;

                DoTestPellipseCenter(pt1, pt2, distance);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseCenterCoordTest12", CommandFlags.Modal)]
        public void PellipseCenterCoordTest12()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;

                DoTestPellipseCenter(pt1, pt2, distance);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestPellipseCenter(Point3d centerPt, Point3d secondPt, double distance)
        {
            PellipseExpectedResult expectedResult = new EllipseCalculator(centerPt, secondPt, distance, true).CalculateValuePellipseCenter();
            var objIds = DoTestEllipsePellipseCenter(centerPt, secondPt, distance);
            VerifyPellipse(objIds, expectedResult);
        }

        private ObjectIdCollection DoTestEllipsePellipseCenter(Point3d centerPt, Point3d endPt, double distance)
        {
            using (new SystemVariable.Temp("PELLIPSE", 1))
            {
                //// Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));


                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                endPt = endPt.TransformBy(CoordConverter.UcsToWcs());
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PointDrawJigN.JigActions options) =>
                    {
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", endPt));

                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", distance));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();
                return appendedEllipseIds;
            }
        }
    }
}
