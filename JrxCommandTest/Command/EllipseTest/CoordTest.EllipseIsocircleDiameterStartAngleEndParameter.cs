﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartAngEndParamCoordTest1", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartAngEndParamCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                double diameter = 30;
                double startAng = 30;
                double endParam = 40;

                DoTestEllipseIsocircleDiameterStartAngEndParam(pt1, diameter, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartAngEndParamCoordTest2", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartAngEndParamCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(30, 40, 50);
                double diameter = 34;
                double startAng = 210;
                double endParam = 30;

                DoTestEllipseIsocircleDiameterStartAngEndParam(pt1, diameter, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartAngEndParamCoordTest3", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartAngEndParamCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 50, 30);
                double diameter = 20;
                double startAng = 150;
                double endParam = 200;

                DoTestEllipseIsocircleDiameterStartAngEndParam(pt1, diameter, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartAngEndParamCoordTest4", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartAngEndParamCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                double diameter = 30;
                double startAng = 30;
                double endParam = 40;

                DoTestEllipseIsocircleDiameterStartAngEndParam(pt1, diameter, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartAngEndParamCoordTest5", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartAngEndParamCoordTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(30, 40, 50);
                double diameter = 34;
                double startAng = 210;
                double endParam = 30;

                DoTestEllipseIsocircleDiameterStartAngEndParam(pt1, diameter, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartAngEndParamCoordTest6", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartAngEndParamCoordTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 50, 30);
                double diameter = 20;
                double startAng = 150;
                double endParam = 200;

                DoTestEllipseIsocircleDiameterStartAngEndParam(pt1, diameter, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartAngEndParamCoordTest7", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartAngEndParamCoordTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                double diameter = 30;
                double startAng = 30;
                double endParam = 40;

                DoTestEllipseIsocircleDiameterStartAngEndParam(pt1, diameter, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartAngEndParamCoordTest8", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartAngEndParamCoordTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(30, 40, 50);
                double diameter = 34;
                double startAng = 210;
                double endParam = 30;

                DoTestEllipseIsocircleDiameterStartAngEndParam(pt1, diameter, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartAngEndParamCoordTest9", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartAngEndParamCoordTest9()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 50, 30);
                double diameter = 20;
                double startAng = 150;
                double endParam = 200;

                DoTestEllipseIsocircleDiameterStartAngEndParam(pt1, diameter, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartAngEndParamCoordTest10", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartAngEndParamCoordTest10()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                double diameter = 30;
                double startAng = 30;
                double endParam = 40;

                DoTestEllipseIsocircleDiameterStartAngEndParam(pt1, diameter, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartAngEndParamCoordTest11", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartAngEndParamCoordTest11()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(30, 40, 50);
                double diameter = 34;
                double startAng = 210;
                double endParam = 30;

                DoTestEllipseIsocircleDiameterStartAngEndParam(pt1, diameter, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartAngEndParamCoordTest12", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartAngEndParamCoordTest12()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 50, 30);
                double diameter = 20;
                double startAng = 150;
                double endParam = 200;

                DoTestEllipseIsocircleDiameterStartAngEndParam(pt1, diameter, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestEllipseIsocircleDiameterStartAngEndParam(Point3d centerPt, double diameter, double startAng, double endParam)
        {
            ExpectedResult expectedResult = new EllipseCalculator(centerPt, diameter, Math.PI * startAng / 180, Math.PI * endParam / 180)
                    .CalculateValueEllipseIsocircleDiameterStartAngEndParam();
            var objIds = DoTestEllipseArcEllipseIsocircleDiameterStartAngEndParam(centerPt, diameter, Math.PI * startAng / 180, Math.PI * endParam / 180);
            VerifyEllipseArc(objIds, expectedResult);
        }

        private ObjectIdCollection DoTestEllipseArcEllipseIsocircleDiameterStartAngEndParam(Point3d centerPt, double diameter, double startAng, double endParam)
        {
            //// Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", diameter));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + startAng, startAng));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + endParam, endParam));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedEllipseIds;
        }
    }
}
