﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using CADApp = GrxCAD.ApplicationServices.Application;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;

#endif
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using System;

namespace JrxCadTest.Command.EllipseTest
{
    [TestClass]
    public class Test : BaseTest
    {
        private PromptResult _resValueSet;
        private PromptResult _resValueIsoplaneSet;
        private string _classFullName;

        public override void OnStart()
        {
            PromptKeywordOptions optValueSet = new PromptKeywordOptions($"Specify the coordinate system to test:")
            {
                Keywords =
                {
                    { "W", "W", "Wcs" },
                    { "1", "1", "ucs1"},
                    { "2", "2", "ucs2"},
                    { "3", "3", "ucs3"},
                },
            };
            _resValueSet = Util.Editor().GetKeywords(optValueSet);

            var methodInfo = System.Reflection.MethodBase.GetCurrentMethod();
            _classFullName = methodInfo.DeclaringType.FullName;

            Util.Editor().WriteMessage($"\n{_classFullName.ToUpper()} START!");
        }
        public void AfterStart()
        {
            PromptKeywordOptions optValueIsoplaneSet = new PromptKeywordOptions($"\nSpecify the isometric plane to test:")
            {
                Keywords =
                {
                    { "L", "Left", "Left" },
                    { "T", "Top", "Top"},
                    { "R", "Right", "Right"},
                },
            };
            _resValueIsoplaneSet = Util.Editor().GetKeywords(optValueIsoplaneSet);

            var methodInfo = System.Reflection.MethodBase.GetCurrentMethod();
            _classFullName = methodInfo.DeclaringType.FullName;
        }
        public override void OnFinish()
        {
            Util.Editor().WriteMessage($"\n{_classFullName.ToUpper()} COMPLETED!");
        }

        private void changeCoordinateSystem(string coordStr)
        {
            Matrix3d ucs = Matrix3d.Identity;
            switch (coordStr)
            {
                case "W": //WCS
                    break;
                case "1": //UCS1
                    ucs = Matrix3d.Identity * Matrix3d.Displacement(new Vector3d(10, 20, 30));
                    break;
                case "2": //UCS2
                    ucs = Matrix3d.Identity * Matrix3d.Rotation(Math.PI / 6, Vector3d.XAxis, Point3d.Origin);
                    break;
                case "3": //UCS3
                    ucs = Matrix3d.Identity *
                    Matrix3d.Displacement(new Vector3d(10, 20, 30)) *
                    Matrix3d.Rotation(Math.PI / 4, Vector3d.XAxis, Point3d.Origin) *
                    Matrix3d.Rotation(Math.PI / 6, Vector3d.YAxis, Point3d.Origin) *
                    Matrix3d.Rotation(Math.PI / 3, Vector3d.ZAxis, Point3d.Origin);
                    break;
                default:
                    return;
            }
            Editor ed = CADApp.DocumentManager.MdiActiveDocument.Editor;
            ed.CurrentUserCoordinateSystem = ucs;
        }

        private void changeIsometricPlane(string isoplaneStr)
        {
            switch (isoplaneStr)
            {
                case "L": //Isoplane Left
                    CADApp.SetSystemVariable("SNAPISOPAIR", 0);
                    break;
                case "T": //Isoplane top
                    CADApp.SetSystemVariable("SNAPISOPAIR", 1);
                    break;
                case "R": //Isoplane right
                    CADApp.SetSystemVariable("SNAPISOPAIR", 2);
                    break;
                default:
                    return;
            }
        }

        #region Pattern1: Axis - End - Distance
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST1", CommandFlags.Modal)]
        public void EllipseTestPattern1()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseAxisEndCoordTest1();
                    coordTest.EllipseAxisEndCoordTest2();
                    coordTest.EllipseAxisEndCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseAxisEndCoordTest4();
                    coordTest.EllipseAxisEndCoordTest5();
                    coordTest.EllipseAxisEndCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseAxisEndCoordTest7();
                    coordTest.EllipseAxisEndCoordTest8();
                    coordTest.EllipseAxisEndCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseAxisEndCoordTest10();
                    coordTest.EllipseAxisEndCoordTest11();
                    coordTest.EllipseAxisEndCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            //Todo: Setup Rotation

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest1(cmd, optFirst, optSecond, optDistance, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();
            OnFinish();
        }
        #endregion Pattern1: Axis - End - Distance

        #region Pattern2: Axis - End - Rotation
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST2", CommandFlags.Modal)]
        public void EllipseTestPattern2()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseAxisEndRotationCoordTest1();
                    coordTest.EllipseAxisEndRotationCoordTest2();
                    coordTest.EllipseAxisEndRotationCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseAxisEndRotationCoordTest4();
                    coordTest.EllipseAxisEndRotationCoordTest5();
                    coordTest.EllipseAxisEndRotationCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseAxisEndRotationCoordTest7();
                    coordTest.EllipseAxisEndRotationCoordTest8();
                    coordTest.EllipseAxisEndRotationCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseAxisEndRotationCoordTest10();
                    coordTest.EllipseAxisEndRotationCoordTest11();
                    coordTest.EllipseAxisEndRotationCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optRotation = options.Options;
                        optRotation.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest2(cmd, optFirst, optSecond, optRotation, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();
            OnFinish();
        }
        #endregion Pattern2: Axis - End - Rotation

        #region Pattern3: Center - Distance
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST3", CommandFlags.Modal)]
        public void EllipseTestPattern3()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseCenterCoordTest1();
                    coordTest.EllipseCenterCoordTest2();
                    coordTest.EllipseCenterCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseCenterCoordTest4();
                    coordTest.EllipseCenterCoordTest5();
                    coordTest.EllipseCenterCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseCenterCoordTest7();
                    coordTest.EllipseCenterCoordTest8();
                    coordTest.EllipseCenterCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseCenterCoordTest10();
                    coordTest.EllipseCenterCoordTest11();
                    coordTest.EllipseCenterCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup Center Point
            var sequence = new MockSequence();
            userInputMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            //Todo: Setup Rotation

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest3(cmd, optCenter, optSecond, optDistance, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern3: Center - Distance

        #region Pattern4: Center - Rotation
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST4", CommandFlags.Modal)]
        public void EllipseTestPattern4()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseCenterRotationCoordTest1();
                    coordTest.EllipseCenterRotationCoordTest2();
                    coordTest.EllipseCenterRotationCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseCenterRotationCoordTest4();
                    coordTest.EllipseCenterRotationCoordTest5();
                    coordTest.EllipseCenterRotationCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseCenterRotationCoordTest7();
                    coordTest.EllipseCenterRotationCoordTest8();
                    coordTest.EllipseCenterRotationCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseCenterRotationCoordTest10();
                    coordTest.EllipseCenterRotationCoordTest11();
                    coordTest.EllipseCenterRotationCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup Center Point
            var sequence = new MockSequence();
            userInputMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optRotation = options.Options;
                        optRotation.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest4(cmd, optCenter, optSecond, optRotation, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern4: Center - Rotation

        #region Pattern5: Elliptical Arc Axis - End - Distance - StartAngle - EndAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST5", CommandFlags.Modal)]
        public void EllipseTestPattern5()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartEndAngCoordTest1();
                    coordTest.EllipseArcAxisEndStartEndAngCoordTest2();
                    coordTest.EllipseArcAxisEndStartEndAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartEndAngCoordTest4();
                    coordTest.EllipseArcAxisEndStartEndAngCoordTest5();
                    coordTest.EllipseArcAxisEndCoordStartEndAngTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartEndAngCoordTest7();
                    coordTest.EllipseArcAxisEndStartEndAngCoordTest8();
                    coordTest.EllipseArcAxisEndStartEndAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartEndAngCoordTest10();
                    coordTest.EllipseArcAxisEndStartEndAngCoordTest11();
                    coordTest.EllipseArcAxisEndStartEndAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            // Setup Start Angle
            var startAng = 30;
            JigPromptAngleOptions optStartAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

            // Setup End Angle
            var endAng = 70;
            JigPromptAngleOptions optEndAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optEndAng = options.Options;
                    optEndAng.DefaultValue = Math.PI * 2;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest5(cmd, optFirst, optSecond, optStartAng, optEndAng, optDistance, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern5: Elliptical Arc Axis - End - Distance - StartAngle - EndAngle

        #region Pattern6: Elliptical Arc Axis - End - Distance - StartAngle - EndParameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST6", CommandFlags.Modal)]
        public void EllipseTestPattern6()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartAngEndParamCoordTest1();
                    coordTest.EllipseArcAxisEndStartAngEndParamCoordTest2();
                    coordTest.EllipseArcAxisEndStartAngEndParamCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartAngEndParamCoordTest4();
                    coordTest.EllipseArcAxisEndStartAngEndParamCoordTest5();
                    coordTest.EllipseArcAxisEndStartAngEndParamCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartAngEndParamCoordTest7();
                    coordTest.EllipseArcAxisEndStartAngEndParamCoordTest8();
                    coordTest.EllipseArcAxisEndStartAngEndParamCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartAngEndParamCoordTest10();
                    coordTest.EllipseArcAxisEndStartAngEndParamCoordTest11();
                    coordTest.EllipseArcAxisEndStartAngEndParamCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            // Setup Start Angle
            var startAng = 30;
            JigPromptAngleOptions optStartAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

            // Setup End Parameter
            var endParam = 70;
            JigPromptAngleOptions optEndParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optEndParam = options.Options;
                    optEndParam.DefaultValue = Math.PI * 2;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endParam));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest6(cmd, optFirst, optSecond, optStartAng, optEndParam, optDistance, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern6: Elliptical Arc Axis - End - Distance - StartAngle - EndParameter

        #region Pattern7: Elliptical Arc Axis - End - Distance - StartAngle - IncludedAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST7", CommandFlags.Modal)]
        public void EllipseTestPattern7()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartAngIncludedAngCoordTest1();
                    coordTest.EllipseArcAxisEndStartAngIncludedAngCoordTest2();
                    coordTest.EllipseArcAxisEndStartAngIncludedAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartAngIncludedAngCoordTest4();
                    coordTest.EllipseArcAxisEndStartAngIncludedAngCoordTest5();
                    coordTest.EllipseArcAxisEndStartAngIncludedAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartAngIncludedAngCoordTest7();
                    coordTest.EllipseArcAxisEndStartAngIncludedAngCoordTest8();
                    coordTest.EllipseArcAxisEndStartAngIncludedAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartAngIncludedAngCoordTest10();
                    coordTest.EllipseArcAxisEndStartAngIncludedAngCoordTest11();
                    coordTest.EllipseArcAxisEndStartAngIncludedAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            // Setup Start Angle
            var startAng = 30;
            JigPromptAngleOptions optStartAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

            // Setup Included Angle
            var includedAng = 70;
            JigPromptAngleOptions optIncludedAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optIncludedAng = options.Options;
                    optIncludedAng.DefaultValue = Math.PI;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", includedAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest7(cmd, optFirst, optSecond, optStartAng, optIncludedAng, optDistance, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern7: Elliptical Arc Axis - End - Distance - StartAngle - IncludedAngle

        #region Pattern8: Elliptical Arc Axis - End - Distance - StartParameter - EndParameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST8", CommandFlags.Modal)]
        public void EllipseTestPattern8()
        {
            OnStart();
            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartEndParamCoordTest1();
                    coordTest.EllipseArcAxisEndStartEndParamCoordTest2();
                    coordTest.EllipseArcAxisEndStartEndParamCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartEndParamCoordTest4();
                    coordTest.EllipseArcAxisEndStartEndParamCoordTest5();
                    coordTest.EllipseArcAxisEndStartEndParamCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartEndParamCoordTest7();
                    coordTest.EllipseArcAxisEndStartEndParamCoordTest8();
                    coordTest.EllipseArcAxisEndStartEndParamCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartEndParamCoordTest10();
                    coordTest.EllipseArcAxisEndStartEndParamCoordTest11();
                    coordTest.EllipseArcAxisEndStartEndParamCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            // Setup Start Parameter
            var startParam = 30;
            JigPromptAngleOptions optStartParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

            //Setup End Parameter
            var endParam = 70;
            JigPromptAngleOptions optEndParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optEndParam = options.Options;
                    optEndParam.DefaultValue = Math.PI * 2;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endParam));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest8(cmd, optFirst, optSecond, optStartParam, optEndParam, optDistance, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern8: Elliptical Arc Axis - End - Distance - StartParameter - EndParameter

        #region Pattern9: Elliptical Arc Axis - End - Distance - StartParameter - EndAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST9", CommandFlags.Modal)]
        public void EllipseTestPattern9()
        {
            OnStart();
            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartParamEndAngCoordTest1();
                    coordTest.EllipseArcAxisEndStartParamEndAngCoordTest2();
                    coordTest.EllipseArcAxisEndStartParamEndAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartParamEndAngCoordTest4();
                    coordTest.EllipseArcAxisEndStartParamEndAngCoordTest5();
                    coordTest.EllipseArcAxisEndStartParamEndAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartParamEndAngCoordTest7();
                    coordTest.EllipseArcAxisEndStartParamEndAngCoordTest8();
                    coordTest.EllipseArcAxisEndStartParamEndAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartParamEndAngCoordTest10();
                    coordTest.EllipseArcAxisEndStartParamEndAngCoordTest11();
                    coordTest.EllipseArcAxisEndStartParamEndAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            // Setup Start Parameter
            var startParam = 30;
            JigPromptAngleOptions optStartParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

            // Setup End Angle
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "A", 0));
            var endAng = 70;
            JigPromptAngleOptions optEndAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndAng = options.Options;
                        optEndAng.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest9(cmd, optFirst, optSecond, optStartParam, optEndAng, optDistance, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern9: Elliptical Arc Axis - End - Distance - StartParameter - EndAngle

        #region Pattern10: Elliptical Arc Axis - End - Distance - StartParameter - IncludedAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST10", CommandFlags.Modal)]
        public void EllipseTestPattern10()
        {
            OnStart();
            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartParamIncludedAngCoordTest1();
                    coordTest.EllipseArcAxisEndStartParamIncludedAngCoordTest2();
                    coordTest.EllipseArcAxisEndStartParamIncludedAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartParamIncludedAngCoordTest4();
                    coordTest.EllipseArcAxisEndStartParamIncludedAngCoordTest5();
                    coordTest.EllipseArcAxisEndStartParamIncludedAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartParamIncludedAngCoordTest7();
                    coordTest.EllipseArcAxisEndStartParamIncludedAngCoordTest8();
                    coordTest.EllipseArcAxisEndStartParamIncludedAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndStartParamIncludedAngCoordTest10();
                    coordTest.EllipseArcAxisEndStartParamIncludedAngCoordTest11();
                    coordTest.EllipseArcAxisEndStartParamIncludedAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            // Setup Start Parameter
            var startParam = 30;
            JigPromptAngleOptions optStartParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

            // Setup Included Angle
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
            var includedAng = 70;
            JigPromptAngleOptions optIncludedAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optIncludedAng = options.Options;
                    optIncludedAng.DefaultValue = Math.PI;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", includedAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest10(cmd, optFirst, optSecond, optStartParam, optIncludedAng, optDistance, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern10: Elliptical Arc Axis - End - Distance - StartParameter - IncludedAngle

        #region Pattern11: Elliptical Arc Axis - End - Rotation - StartAngle - EndAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST11", CommandFlags.Modal)]
        public void EllipseTestPattern11()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartEndAngCoordTest1();
                    coordTest.EllipseArcAxisEndRotationStartEndAngCoordTest2();
                    coordTest.EllipseArcAxisEndRotationStartEndAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartEndAngCoordTest4();
                    coordTest.EllipseArcAxisEndRotationStartEndAngCoordTest5();
                    coordTest.EllipseArcAxisEndRotationStartEndAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartEndAngCoordTest7();
                    coordTest.EllipseArcAxisEndRotationStartEndAngCoordTest8();
                    coordTest.EllipseArcAxisEndRotationStartEndAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartEndAngCoordTest10();
                    coordTest.EllipseArcAxisEndRotationStartEndAngCoordTest11();
                    coordTest.EllipseArcAxisEndRotationStartEndAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optRotation = options.Options;
                        optRotation.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup Start Angle
            var startAng = 30;
            JigPromptAngleOptions optStartAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

            // Setup End Angle
            var endAng = 70;
            JigPromptAngleOptions optEndAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndAng = options.Options;
                        optEndAng.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest11(cmd, optFirst, optSecond, optStartAng, optEndAng, optRotation, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern11: Elliptical Arc Axis - End - Rotation - StartAngle - EndAngle

        #region Pattern12: Elliptical Arc Axis - End - Rotation - StartAngle - EndParameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST12", CommandFlags.Modal)]
        public void EllipseTestPattern12()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartAngEndParamCoordTest1();
                    coordTest.EllipseArcAxisEndRotationStartAngEndParamCoordTest2();
                    coordTest.EllipseArcAxisEndRotationStartAngEndParamCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartAngEndParamCoordTest4();
                    coordTest.EllipseArcAxisEndRotationStartAngEndParamCoordTest5();
                    coordTest.EllipseArcAxisEndRotationStartAngEndParamCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartAngEndParamCoordTest7();
                    coordTest.EllipseArcAxisEndRotationStartAngEndParamCoordTest8();
                    coordTest.EllipseArcAxisEndRotationStartAngEndParamCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartAngEndParamCoordTest10();
                    coordTest.EllipseArcAxisEndRotationStartAngEndParamCoordTest11();
                    coordTest.EllipseArcAxisEndRotationStartAngEndParamCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optRotation = options.Options;
                    optRotation.DefaultValue = 0;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup Start Angle 
            var startAng = 30;
            JigPromptAngleOptions optStartAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

            // Setup Start Parameter
            var endParam = 70;
            JigPromptAngleOptions optEndParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optEndParam = options.Options;
                    optEndParam.DefaultValue = Math.PI * 2;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endParam));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest12(cmd, optFirst, optSecond, optStartAng, optEndParam, optRotation, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern12: Elliptical Arc Axis - End - Distance - StartAngle - EndParameter

        #region Pattern13: Elliptical Arc Axis - End - Rotation - StartAngle - IncludedAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST13", CommandFlags.Modal)]
        public void EllipseTestPattern13()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartAngIncludedAngCoordTest1();
                    coordTest.EllipseArcAxisEndRotationStartAngIncludedAngCoordTest2();
                    coordTest.EllipseArcAxisEndRotationStartAngIncludedAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartAngIncludedAngCoordTest4();
                    coordTest.EllipseArcAxisEndRotationStartAngIncludedAngCoordTest5();
                    coordTest.EllipseArcAxisEndRotationStartAngIncludedAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartAngIncludedAngCoordTest7();
                    coordTest.EllipseArcAxisEndRotationStartAngIncludedAngCoordTest8();
                    coordTest.EllipseArcAxisEndRotationStartAngIncludedAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartAngIncludedAngCoordTest10();
                    coordTest.EllipseArcAxisEndRotationStartAngIncludedAngCoordTest11();
                    coordTest.EllipseArcAxisEndRotationStartAngIncludedAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optRotation = options.Options;
                    optRotation.DefaultValue = 0;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup Start Angle
            var startAng = 30;
            JigPromptAngleOptions optStartAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

            // Setup Included Angle
            var includedAng = 70;
            JigPromptAngleOptions optIncludedAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optIncludedAng = options.Options;
                    optIncludedAng.DefaultValue = Math.PI;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", includedAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest13(cmd, optFirst, optSecond, optStartAng, optIncludedAng, optRotation, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern13: Elliptical Arc Axis - End - Distance - StartAngle - IncludedAngle

        #region Pattern14: Elliptical Arc Axis - End - Rotation - StartParameter - EndParameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST14", CommandFlags.Modal)]
        public void EllipseTestPattern14()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartEndParamCoordTest1();
                    coordTest.EllipseArcAxisEndRotationStartEndParamCoordTest2();
                    coordTest.EllipseArcAxisEndRotationStartEndParamCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartEndParamCoordTest4();
                    coordTest.EllipseArcAxisEndRotationStartEndParamCoordTest5();
                    coordTest.EllipseArcAxisEndRotationStartEndParamCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartEndParamCoordTest7();
                    coordTest.EllipseArcAxisEndRotationStartEndParamCoordTest8();
                    coordTest.EllipseArcAxisEndRotationStartEndParamCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartEndParamCoordTest10();
                    coordTest.EllipseArcAxisEndRotationStartEndParamCoordTest11();
                    coordTest.EllipseArcAxisEndRotationStartEndParamCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optRotation = options.Options;
                    optRotation.DefaultValue = 0;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup Start Param
            var startParam = 30;
            JigPromptAngleOptions optStartParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

            // Setup End Param
            var endParam = 70;
            JigPromptAngleOptions optEndParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optEndParam = options.Options;
                    optEndParam.DefaultValue = Math.PI * 2;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endParam));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest14(cmd, optFirst, optSecond, optStartParam, optEndParam, optRotation, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern14: Elliptical Arc Axis - End - Rotation - StartParameter - EndParameter

        #region Pattern15: Elliptical Arc Axis - End - Rotation - StartParameter - EndAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST15", CommandFlags.Modal)]
        public void EllipseTestPattern15()
        {
            OnStart();
            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartParamEndAngCoordTest1();
                    coordTest.EllipseArcAxisEndRotationStartParamEndAngCoordTest2();
                    coordTest.EllipseArcAxisEndRotationStartParamEndAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartParamEndAngCoordTest4();
                    coordTest.EllipseArcAxisEndRotationStartParamEndAngCoordTest5();
                    coordTest.EllipseArcAxisEndRotationStartParamEndAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartParamEndAngCoordTest7();
                    coordTest.EllipseArcAxisEndRotationStartParamEndAngCoordTest8();
                    coordTest.EllipseArcAxisEndRotationStartParamEndAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartParamEndAngCoordTest10();
                    coordTest.EllipseArcAxisEndRotationStartParamEndAngCoordTest11();
                    coordTest.EllipseArcAxisEndRotationStartParamEndAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optRotation = options.Options;
                    optRotation.DefaultValue = 0;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup Start Param
            var startParam = 30;
            JigPromptAngleOptions optStartParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

            // Setup End Ang
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "A", 0));
            var endAng = 70;
            JigPromptAngleOptions optEndAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndAng = options.Options;
                        optEndAng.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest15(cmd, optFirst, optSecond, optStartParam, optEndAng, optRotation, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern15: Elliptical Arc Axis - End - Rotation - StartParameter - EndAngle

        #region Pattern16: Elliptical Arc Axis - End - Rotation - StartParameter - IncludedAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST16", CommandFlags.Modal)]
        public void EllipseTestPattern16()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartParamIncludedAngCoordTest1();
                    coordTest.EllipseArcAxisEndRotationStartParamIncludedAngCoordTest2();
                    coordTest.EllipseArcAxisEndRotationStartParamIncludedAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartParamIncludedAngCoordTest4();
                    coordTest.EllipseArcAxisEndRotationStartParamIncludedAngCoordTest5();
                    coordTest.EllipseArcAxisEndRotationStartParamIncludedAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartParamIncludedAngCoordTest7();
                    coordTest.EllipseArcAxisEndRotationStartParamIncludedAngCoordTest8();
                    coordTest.EllipseArcAxisEndRotationStartParamIncludedAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcAxisEndRotationStartParamIncludedAngCoordTest10();
                    coordTest.EllipseArcAxisEndRotationStartParamIncludedAngCoordTest11();
                    coordTest.EllipseArcAxisEndRotationStartParamIncludedAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup First Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            PromptPointOptions optFirst = null;
            var firstPt = new Point3d(10, 0, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optFirst = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optRotation = options.Options;
                    optRotation.DefaultValue = 0;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup Start Param
            var startParam = 30;
            JigPromptAngleOptions optStartParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

            // Setup Included Ang
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
            var includedAng = 70;
            JigPromptAngleOptions optIncludedAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optIncludedAng = options.Options;
                    optIncludedAng.DefaultValue = Math.PI;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", includedAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest16(cmd, optFirst, optSecond, optStartParam, optIncludedAng, optRotation, firstPt, secondPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern16: Elliptical Arc Axis - End - Rotation - StartParameter - IncludedAngle

        #region Pattern17: Elliptical Arc Center - Distance - StartAngle - EndAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST17", CommandFlags.Modal)]
        public void EllipseTestPattern17()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartEndAngCoordTest1();
                    coordTest.EllipseArcCenterStartEndAngCoordTest2();
                    coordTest.EllipseArcCenterStartEndAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartEndAngCoordTest4();
                    coordTest.EllipseArcCenterStartEndAngCoordTest5();
                    coordTest.EllipseArcCenterStartEndAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartEndAngCoordTest7();
                    coordTest.EllipseArcCenterStartEndAngCoordTest8();
                    coordTest.EllipseArcCenterStartEndAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartEndAngCoordTest10();
                    coordTest.EllipseArcCenterStartEndAngCoordTest11();
                    coordTest.EllipseArcCenterStartEndAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup Center Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "10, 0, 0", centerPt));

            // Setup End Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            // Setup Start Ang
            var startAng = 30;
            JigPromptAngleOptions optStartAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

            // Setup End Ang
            var endAng = 70;
            JigPromptAngleOptions optEndAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndAng = options.Options;
                        optEndAng.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest17(cmd, optCenter, optSecond, optStartAng, optEndAng, optDistance, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern17: Elliptical Arc Center - Distance - StartAngle - EndAngle

        #region Pattern18: Elliptical Arc Center - Distance - StartAngle - EndParameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST18", CommandFlags.Modal)]
        public void EllipseTestPattern18()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartAngEndParamCoordTest1();
                    coordTest.EllipseArcCenterStartAngEndParamCoordTest2();
                    coordTest.EllipseArcCenterStartAngEndParamCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartAngEndParamCoordTest4();
                    coordTest.EllipseArcCenterStartAngEndParamCoordTest5();
                    coordTest.EllipseArcCenterStartAngEndParamCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartAngEndParamCoordTest7();
                    coordTest.EllipseArcCenterStartAngEndParamCoordTest8();
                    coordTest.EllipseArcCenterStartAngEndParamCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartAngEndParamCoordTest10();
                    coordTest.EllipseArcCenterStartAngEndParamCoordTest11();
                    coordTest.EllipseArcCenterStartAngEndParamCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup Center Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "10, 0, 0", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            // Setup Start Ang
            var startAng = 30;
            JigPromptAngleOptions optStartAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

            //Setup End Param
            var endParam = 70;
            JigPromptAngleOptions optEndParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optEndParam = options.Options;
                    optEndParam.DefaultValue = Math.PI * 2;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endParam));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest18(cmd, optCenter, optSecond, optStartAng, optEndParam, optDistance, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern18: Elliptical Arc Center - Distance - StartAngle - EndParameter

        #region Pattern19: Elliptical Arc Center - Distance - StartAngle - IncludedAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST19", CommandFlags.Modal)]
        public void EllipseTestPattern19()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartAngIncludedAngCoordTest1();
                    coordTest.EllipseArcCenterStartAngIncludedAngCoordTest2();
                    coordTest.EllipseArcCenterStartAngIncludedAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartAngIncludedAngCoordTest4();
                    coordTest.EllipseArcCenterStartAngIncludedAngCoordTest5();
                    coordTest.EllipseArcCenterStartAngIncludedAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartAngIncludedAngCoordTest7();
                    coordTest.EllipseArcCenterStartAngIncludedAngCoordTest8();
                    coordTest.EllipseArcCenterStartAngIncludedAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartAngIncludedAngCoordTest10();
                    coordTest.EllipseArcCenterStartAngIncludedAngCoordTest11();
                    coordTest.EllipseArcCenterStartAngIncludedAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup Center Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "10, 0, 0", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            //Setup Start Ang
            var startAng = 30;
            JigPromptAngleOptions optStartAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

            //Setup Included Ang
            var includedAng = 70;
            JigPromptAngleOptions optIncludedAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optIncludedAng = options.Options;
                    optIncludedAng.DefaultValue = Math.PI;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", includedAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest19(cmd, optCenter, optSecond, optStartAng, optIncludedAng, optDistance, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern19: Elliptical Arc Center - Distance - StartAngle - IncludedAngle

        #region Pattern20: Elliptical Arc Center - Distance - StartParameter - EndParameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST20", CommandFlags.Modal)]
        public void EllipseTestPattern20()
        {
            OnStart();
            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartEndParamCoordTest1();
                    coordTest.EllipseArcCenterStartEndParamCoordTest2();
                    coordTest.EllipseArcCenterStartEndParamCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartEndParamCoordTest4();
                    coordTest.EllipseArcCenterStartEndParamCoordTest5();
                    coordTest.EllipseArcCenterStartEndParamCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartEndParamCoordTest7();
                    coordTest.EllipseArcCenterStartEndParamCoordTest8();
                    coordTest.EllipseArcCenterStartEndParamCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartEndParamCoordTest10();
                    coordTest.EllipseArcCenterStartEndParamCoordTest11();
                    coordTest.EllipseArcCenterStartEndParamCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup Center Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "10, 0, 0", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            // Setup Start Param
            var startParam = 30;
            JigPromptAngleOptions optStartParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        //optStartParam.BasePoint = centerPt;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

            // Setup End Param
            var endParam = 70;
            JigPromptAngleOptions optEndParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optEndParam = options.Options;
                    optEndParam.DefaultValue = Math.PI * 2;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endParam));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest20(cmd, optCenter, optSecond, optStartParam, optEndParam, optDistance, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern20: Elliptical Arc Center - Distance - StartParameter - EndParameter

        #region Pattern21: Elliptical Arc Center - Distance - StartParameter - EndAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST21", CommandFlags.Modal)]
        public void EllipseTestPattern21()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartParamEndAngCoordTest1();
                    coordTest.EllipseArcCenterStartParamEndAngCoordTest2();
                    coordTest.EllipseArcCenterStartParamEndAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartParamEndAngCoordTest4();
                    coordTest.EllipseArcCenterStartParamEndAngCoordTest5();
                    coordTest.EllipseArcCenterStartParamEndAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartParamEndAngCoordTest7();
                    coordTest.EllipseArcCenterStartParamEndAngCoordTest8();
                    coordTest.EllipseArcCenterStartParamEndAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartParamEndAngCoordTest10();
                    coordTest.EllipseArcCenterStartParamEndAngCoordTest11();
                    coordTest.EllipseArcCenterStartParamEndAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup Center Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "10, 0, 0", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            // Setup Start Param
            var startParam = 30;
            JigPromptAngleOptions optStartParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

            // Setup End Ang
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "A", 0));
            var endAng = 70;
            JigPromptAngleOptions optEndAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndAng = options.Options;
                        optEndAng.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest21(cmd, optCenter, optSecond, optStartParam, optEndAng, optDistance, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern21: Elliptical Arc Center - Distance - StartParameter - EndAngle

        #region Pattern22: Elliptical Arc Center - Distance - StartParameter - IncludedAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST22", CommandFlags.Modal)]
        public void EllipseTestPattern22()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartParamIncludedAngCoordTest1();
                    coordTest.EllipseArcCenterStartParamIncludedAngCoordTest2();
                    coordTest.EllipseArcCenterStartParamIncludedAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartParamIncludedAngCoordTest4();
                    coordTest.EllipseArcCenterStartParamIncludedAngCoordTest5();
                    coordTest.EllipseArcCenterStartParamIncludedAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartParamIncludedAngCoordTest7();
                    coordTest.EllipseArcCenterStartParamIncludedAngCoordTest8();
                    coordTest.EllipseArcCenterStartParamIncludedAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterStartParamIncludedAngCoordTest10();
                    coordTest.EllipseArcCenterStartParamIncludedAngCoordTest11();
                    coordTest.EllipseArcCenterStartParamIncludedAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup Center Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "10, 0, 0", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Distance
            var distance = 10;
            JigPromptDistanceOptions optDistance = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    optDistance = options.Options;
                    optDistance.DefaultValue = 1;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

            // Setup Start Param
            var startParam = 30;
            JigPromptAngleOptions optStartParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

            // Setup Included Ang
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
            var includedAng = 70;
            JigPromptAngleOptions optIncludedAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optIncludedAng = options.Options;
                    optIncludedAng.DefaultValue = Math.PI;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", includedAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest22(cmd, optCenter, optSecond, optStartParam, optIncludedAng, optDistance, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern22: Elliptical Arc Center - Distance - StartParameter - IncludedAngle

        #region Pattern23: Elliptical Arc Center - Rotation - StartAngle - EndAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST23", CommandFlags.Modal)]
        public void EllipseTestPattern23()
        {
            OnStart();
            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartEndAngCoordTest1();
                    coordTest.EllipseArcCenterRotationStartEndAngCoordTest2();
                    coordTest.EllipseArcCenterRotationStartEndAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartEndAngCoordTest4();
                    coordTest.EllipseArcCenterRotationStartEndAngCoordTest5();
                    coordTest.EllipseArcCenterRotationStartEndAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartEndAngCoordTest7();
                    coordTest.EllipseArcCenterRotationStartEndAngCoordTest8();
                    coordTest.EllipseArcCenterRotationStartEndAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartEndAngCoordTest10();
                    coordTest.EllipseArcCenterRotationStartEndAngCoordTest11();
                    coordTest.EllipseArcCenterRotationStartEndAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup Center Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "10, 0, 0", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));


            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optRotation = options.Options;
                        optRotation.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup Start Ang
            var startAng = 30;
            JigPromptAngleOptions optStartAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

            // Setup End Ang
            var endAng = 70;
            JigPromptAngleOptions optEndAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndAng = options.Options;
                        optEndAng.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest23(cmd, optCenter, optSecond, optStartAng, optEndAng, optRotation, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern23: Elliptical Arc Center - Rotation - StartAngle - EndAngle

        #region Pattern24: Elliptical Arc Center - Rotation - StartAngle - EndParameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST24", CommandFlags.Modal)]
        public void EllipseTestPattern24()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartAngEndParamCoordTest1();
                    coordTest.EllipseArcCenterRotationStartAngEndParamCoordTest2();
                    coordTest.EllipseArcCenterRotationStartAngEndParamCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartAngEndParamCoordTest4();
                    coordTest.EllipseArcCenterRotationStartAngEndParamCoordTest5();
                    coordTest.EllipseArcCenterRotationStartAngEndParamCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartAngEndParamCoordTest7();
                    coordTest.EllipseArcCenterRotationStartAngEndParamCoordTest8();
                    coordTest.EllipseArcCenterRotationStartAngEndParamCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartAngEndParamCoordTest10();
                    coordTest.EllipseArcCenterRotationStartAngEndParamCoordTest11();
                    coordTest.EllipseArcCenterRotationStartAngEndParamCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup Center Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "10, 0, 0", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optRotation = options.Options;
                    optRotation.DefaultValue = 0;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup Start Ang
            var startAng = 30;
            JigPromptAngleOptions optStartAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

            // Setup End Param
            var endParam = 70;
            JigPromptAngleOptions optEndParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optEndParam = options.Options;
                    optEndParam.DefaultValue = Math.PI * 2;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endParam));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest24(cmd, optCenter, optSecond, optStartAng, optEndParam, optRotation, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern24: Elliptical Arc Center - Rotation - StartAngle - EndParameter

        #region Pattern25: Elliptical Arc Center - Rotation - StartAngle - IncludedAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST25", CommandFlags.Modal)]
        public void EllipseTestPattern25()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartAngIncludedAngCoordTest1();
                    coordTest.EllipseArcCenterRotationStartAngIncludedAngCoordTest2();
                    coordTest.EllipseArcCenterRotationStartAngIncludedAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartAngIncludedAngCoordTest4();
                    coordTest.EllipseArcCenterRotationStartAngIncludedAngCoordTest5();
                    coordTest.EllipseArcCenterRotationStartAngIncludedAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartAngIncludedAngCoordTest7();
                    coordTest.EllipseArcCenterRotationStartAngIncludedAngCoordTest8();
                    coordTest.EllipseArcCenterRotationStartAngIncludedAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartAngIncludedAngCoordTest10();
                    coordTest.EllipseArcCenterRotationStartAngIncludedAngCoordTest11();
                    coordTest.EllipseArcCenterRotationStartAngIncludedAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup Center Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "10, 0, 0", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optRotation = options.Options;
                    optRotation.DefaultValue = 0;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup Start Ang
            var startAng = 30;
            JigPromptAngleOptions optStartAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

            // Setup Included Ang
            var includedAng = 70;
            JigPromptAngleOptions optIncludedAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optIncludedAng = options.Options;
                    optIncludedAng.DefaultValue = Math.PI;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", includedAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest25(cmd, optCenter, optSecond, optStartAng, optIncludedAng, optRotation, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern25: Elliptical Arc Center - Rotation - StartAngle - IncludedAngle

        #region Pattern26: Elliptical Arc Center - Rotation - StartParameter - EndParameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST26", CommandFlags.Modal)]
        public void EllipseTestPattern26()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartEndParamCoordTest1();
                    coordTest.EllipseArcCenterRotationStartEndParamCoordTest2();
                    coordTest.EllipseArcCenterRotationStartEndParamCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartEndParamCoordTest4();
                    coordTest.EllipseArcCenterRotationStartEndParamCoordTest5();
                    coordTest.EllipseArcCenterRotationStartEndParamCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartEndParamCoordTest7();
                    coordTest.EllipseArcCenterRotationStartEndParamCoordTest8();
                    coordTest.EllipseArcCenterRotationStartEndParamCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartEndParamCoordTest10();
                    coordTest.EllipseArcCenterRotationStartEndParamCoordTest11();
                    coordTest.EllipseArcCenterRotationStartEndParamCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            // Setup Center Point
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "10, 0, 0", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optRotation = options.Options;
                    optRotation.DefaultValue = 0;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup Start Param
            var startParam = 30;
            JigPromptAngleOptions optStartParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

            // Setup End Param
            var endParam = 70;
            JigPromptAngleOptions optEndParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optEndParam = options.Options;
                    optEndParam.DefaultValue = Math.PI * 2;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endParam));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest26(cmd, optCenter, optSecond, optStartParam, optEndParam, optRotation, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern26: Elliptical Arc Center - Rotation - StartParameter - EndParameter

        #region Pattern27: Elliptical Arc Center - Rotation - StartParameter - EndAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST27", CommandFlags.Modal)]
        public void EllipseTestPattern27()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartParamEndAngCoordTest1();
                    coordTest.EllipseArcCenterRotationStartParamEndAngCoordTest2();
                    coordTest.EllipseArcCenterRotationStartParamEndAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartParamEndAngCoordTest4();
                    coordTest.EllipseArcCenterRotationStartParamEndAngCoordTest5();
                    coordTest.EllipseArcCenterRotationStartParamEndAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartParamEndAngCoordTest7();
                    coordTest.EllipseArcCenterRotationStartParamEndAngCoordTest8();
                    coordTest.EllipseArcCenterRotationStartParamEndAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartParamEndAngCoordTest10();
                    coordTest.EllipseArcCenterRotationStartParamEndAngCoordTest11();
                    coordTest.EllipseArcCenterRotationStartParamEndAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            // Setup Center Point
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "10, 0, 0", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optRotation = options.Options;
                    optRotation.DefaultValue = 0;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup Start Param
            var startParam = 30;
            JigPromptAngleOptions optStartParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

            // Setup End Ang
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "A", 0));
            var endAng = 70;
            JigPromptAngleOptions optEndAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndAng = options.Options;
                        optEndAng.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest27(cmd, optCenter, optSecond, optStartParam, optEndAng, optRotation, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern27: Elliptical Arc Center - Rotation - StartParameter - EndAngle

        #region Pattern28: Elliptical Arc Center - Rotation - StartParameter - IncludedAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST28", CommandFlags.Modal)]
        public void EllipseTestPattern28()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartParamIncludedAngCoordTest1();
                    coordTest.EllipseArcCenterRotationStartParamIncludedAngCoordTest2();
                    coordTest.EllipseArcCenterRotationStartParamIncludedAngCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartParamIncludedAngCoordTest4();
                    coordTest.EllipseArcCenterRotationStartParamIncludedAngCoordTest5();
                    coordTest.EllipseArcCenterRotationStartParamIncludedAngCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartParamIncludedAngCoordTest7();
                    coordTest.EllipseArcCenterRotationStartParamIncludedAngCoordTest8();
                    coordTest.EllipseArcCenterRotationStartParamIncludedAngCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.EllipseArcCenterRotationStartParamIncludedAngCoordTest10();
                    coordTest.EllipseArcCenterRotationStartParamIncludedAngCoordTest11();
                    coordTest.EllipseArcCenterRotationStartParamIncludedAngCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            // Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

            // Setup Center Point
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            var centerPt = new Point3d(10, 0, 0);
            PromptPointOptions optCenter = null;
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PromptPointOptions options) => optCenter = options)
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "10, 0, 0", centerPt));

            // Setup Second Point
            JigPromptPointOptions optSecond = null;
            var secondPt = new Point3d(0, 10, 0);
            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    optSecond = options.Options;
                    optSecond.DefaultValue = new Point3d();
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

            // Setup Rotation
            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            var rotation = 10;
            JigPromptAngleOptions optRotation = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optRotation = options.Options;
                    optRotation.DefaultValue = 0;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

            // Setup Start Param
            var startParam = 30;
            JigPromptAngleOptions optStartParam = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

            // Setup Included Ang
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
            var includedAng = 70;
            JigPromptAngleOptions optIncludedAng = null;
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    optIncludedAng = options.Options;
                    optIncludedAng.DefaultValue = Math.PI;
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", includedAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();

            MessageTest.EllipseMessageTest28(cmd, optCenter, optSecond, optStartParam, optIncludedAng, optRotation, centerPt);

            //////////////////////////////////////////
            //// Test Completed
            //////////////////////////////////////////
            userInputMock.VerifyAll();

            OnFinish();
        }
        #endregion Pattern28: Elliptical Arc Center - Rotation - StartParameter - IncludedAngle

        #region Pattern29: Ellipse Isocircle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST29", CommandFlags.Modal)]
        public void EllipseTestPattern29()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleCoordTest1();
                        coordTest.EllipseIsocircleCoordTest2();
                        coordTest.EllipseIsocircleCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleCoordTest4();
                        coordTest.EllipseIsocircleCoordTest5();
                        coordTest.EllipseIsocircleCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleCoordTest7();
                        coordTest.EllipseIsocircleCoordTest8();
                        coordTest.EllipseIsocircleCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleCoordTest10();
                        coordTest.EllipseIsocircleCoordTest11();
                        coordTest.EllipseIsocircleCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 0, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Radius
                var radius = 10;
                JigPromptDistanceOptions optRadius = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optRadius = options.Options;
                        optRadius.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", radius));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest29(cmd, optFirst, optCenter, optRadius, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern29: Ellipse Isocircle

        #region Pattern30: Ellipse Isocircle - Diameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST30", CommandFlags.Modal)]
        public void EllipseTestPattern30()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterCoordTest1();
                        coordTest.EllipseIsocircleDiameterCoordTest2();
                        coordTest.EllipseIsocircleDiameterCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterCoordTest4();
                        coordTest.EllipseIsocircleDiameterCoordTest5();
                        coordTest.EllipseIsocircleDiameterCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterCoordTest7();
                        coordTest.EllipseIsocircleDiameterCoordTest8();
                        coordTest.EllipseIsocircleDiameterCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterCoordTest10();
                        coordTest.EllipseIsocircleDiameterCoordTest11();
                        coordTest.EllipseIsocircleDiameterCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 0, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Diameter
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));
                var diameter = 10;
                JigPromptDistanceOptions optDiameter = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optDiameter = options.Options;
                        optDiameter.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", diameter));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest30(cmd, optFirst, optCenter, optDiameter, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern30: Ellipse Isocircle - Diameter

        #region Pattern31: Ellipse Isocircle - StartAngle - EndAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST31", CommandFlags.Modal)]
        public void EllipseTestPattern31()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartEndAngCoordTest1();
                        coordTest.EllipseIsocircleStartEndAngCoordTest2();
                        coordTest.EllipseIsocircleStartEndAngCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartEndAngCoordTest4();
                        coordTest.EllipseIsocircleStartEndAngCoordTest5();
                        coordTest.EllipseIsocircleStartEndAngCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartEndAngCoordTest7();
                        coordTest.EllipseIsocircleStartEndAngCoordTest8();
                        coordTest.EllipseIsocircleStartEndAngCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartEndAngCoordTest10();
                        coordTest.EllipseIsocircleStartEndAngCoordTest11();
                        coordTest.EllipseIsocircleStartEndAngCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 10, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Radius
                var radius = 10;
                JigPromptDistanceOptions optRadius = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optRadius = options.Options;
                        optRadius.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", radius));

                // Setup Start Ang
                var startAng = 30;
                JigPromptAngleOptions optStartAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

                // Setup End Ang
                var endAng = 70;
                JigPromptAngleOptions optEndAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndAng = options.Options;
                        optEndAng.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endAng));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest31(cmd, optFirst, optCenter, optRadius, optStartAng, optEndAng, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern31: Ellipse Isocircle - StartAngle - EndAngle

        #region Pattern32: Ellipse Isocircle - StartAngle - EndParameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST32", CommandFlags.Modal)]
        public void EllipseTestPattern32()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartAngEndParamCoordTest1();
                        coordTest.EllipseIsocircleStartAngEndParamCoordTest2();
                        coordTest.EllipseIsocircleStartAngEndParamCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartAngEndParamCoordTest4();
                        coordTest.EllipseIsocircleStartAngEndParamCoordTest5();
                        coordTest.EllipseIsocircleStartAngEndParamCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartAngEndParamCoordTest7();
                        coordTest.EllipseIsocircleStartAngEndParamCoordTest8();
                        coordTest.EllipseIsocircleStartAngEndParamCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartAngEndParamCoordTest10();
                        coordTest.EllipseIsocircleStartAngEndParamCoordTest11();
                        coordTest.EllipseIsocircleStartAngEndParamCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 10, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Radius
                var radius = 10;
                JigPromptDistanceOptions optRadius = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optRadius = options.Options;
                        optRadius.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", radius));

                // Setup Start Ang
                var startAng = 30;
                JigPromptAngleOptions optStartAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

                // Setup End Param
                var endParam = 70;
                JigPromptAngleOptions optEndParam = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndParam = options.Options;
                        optEndParam.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endParam));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest32(cmd, optFirst, optCenter, optRadius, optStartAng, optEndParam, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern32: Ellipse Isocircle - StartAngle - EndParameter

        #region Pattern33: Ellipse Isocircle - StartAngle - IncludedAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST33", CommandFlags.Modal)]
        public void EllipseTestPattern33()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartAngIncludedAngCoordTest1();
                        coordTest.EllipseIsocircleStartAngIncludedAngCoordTest2();
                        coordTest.EllipseIsocircleStartAngIncludedAngCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartAngIncludedAngCoordTest4();
                        coordTest.EllipseIsocircleStartAngIncludedAngCoordTest5();
                        coordTest.EllipseIsocircleStartAngIncludedAngCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartAngIncludedAngCoordTest7();
                        coordTest.EllipseIsocircleStartAngIncludedAngCoordTest8();
                        coordTest.EllipseIsocircleStartAngIncludedAngCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartAngIncludedAngCoordTest10();
                        coordTest.EllipseIsocircleStartAngIncludedAngCoordTest11();
                        coordTest.EllipseIsocircleStartAngIncludedAngCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 10, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Radius
                var radius = 10;
                JigPromptDistanceOptions optRadius = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optRadius = options.Options;
                        optRadius.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", radius));

                // Setup Start Ang
                var startAng = 30;
                JigPromptAngleOptions optStartAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

                // Setup Included Ang
                var includedAng = 70;
                JigPromptAngleOptions optIncludedAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optIncludedAng = options.Options;
                        optIncludedAng.DefaultValue = Math.PI;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", includedAng));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest33(cmd, optFirst, optCenter, optRadius, optStartAng, optIncludedAng, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern33: Ellipse Isocircle - StartAngle - IncludedAngle

        #region Pattern34: Ellipse Isocircle - StartParameter - EndParameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST34", CommandFlags.Modal)]
        public void EllipseTestPattern34()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartEndParamCoordTest1();
                        coordTest.EllipseIsocircleStartEndParamCoordTest2();
                        coordTest.EllipseIsocircleStartEndParamCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartEndParamCoordTest4();
                        coordTest.EllipseIsocircleStartEndParamCoordTest5();
                        coordTest.EllipseIsocircleStartEndParamCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartEndParamCoordTest7();
                        coordTest.EllipseIsocircleStartEndParamCoordTest8();
                        coordTest.EllipseIsocircleStartEndParamCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartEndParamCoordTest10();
                        coordTest.EllipseIsocircleStartEndParamCoordTest11();
                        coordTest.EllipseIsocircleStartEndParamCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 10, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Radius
                var radius = 10;
                JigPromptDistanceOptions optRadius = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optRadius = options.Options;
                        optRadius.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", radius));

                // Setup Start Param
                var startParam = 30;
                JigPromptAngleOptions optStartParam = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

                // Setup End Param
                var endParam = 70;
                JigPromptAngleOptions optEndParam = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndParam = options.Options;
                        optEndParam.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endParam));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest34(cmd, optFirst, optCenter, optRadius, optStartParam, optEndParam, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern34: Ellipse Isocircle - StartParameter - EndParameter

        #region Pattern35: Ellipse Isocircle - StartParameter - EndAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST35", CommandFlags.Modal)]
        public void EllipseTestPattern35()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartParamEndAngCoordTest1();
                        coordTest.EllipseIsocircleStartParamEndAngCoordTest2();
                        coordTest.EllipseIsocircleStartParamEndAngCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartParamEndAngCoordTest4();
                        coordTest.EllipseIsocircleStartParamEndAngCoordTest5();
                        coordTest.EllipseIsocircleStartParamEndAngCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartParamEndAngCoordTest7();
                        coordTest.EllipseIsocircleStartParamEndAngCoordTest8();
                        coordTest.EllipseIsocircleStartParamEndAngCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartParamEndAngCoordTest10();
                        coordTest.EllipseIsocircleStartParamEndAngCoordTest11();
                        coordTest.EllipseIsocircleStartParamEndAngCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 10, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Radius
                var radius = 10;
                JigPromptDistanceOptions optRadius = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optRadius = options.Options;
                        optRadius.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", radius));

                // Setup Start Param
                var startParam = 30;
                JigPromptAngleOptions optStartParam = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

                // Setup End Ang
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "A", 0));
                var endAng = 70;
                JigPromptAngleOptions optEndAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndAng = options.Options;
                        optEndAng.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endAng));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest35(cmd, optFirst, optCenter, optRadius, optStartParam, optEndAng, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern35: Ellipse Isocircle - StartParameter - EndAngle

        #region Pattern36: Ellipse Isocircle - StartParameter - IncludedAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST36", CommandFlags.Modal)]
        public void EllipseTestPattern36()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartParamIncludedAngCoordTest1();
                        coordTest.EllipseIsocircleStartParamIncludedAngCoordTest2();
                        coordTest.EllipseIsocircleStartParamIncludedAngCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartParamIncludedAngCoordTest4();
                        coordTest.EllipseIsocircleStartParamIncludedAngCoordTest5();
                        coordTest.EllipseIsocircleStartParamIncludedAngCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartParamIncludedAngCoordTest7();
                        coordTest.EllipseIsocircleStartParamIncludedAngCoordTest8();
                        coordTest.EllipseIsocircleStartParamIncludedAngCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleStartParamIncludedAngCoordTest10();
                        coordTest.EllipseIsocircleStartParamIncludedAngCoordTest11();
                        coordTest.EllipseIsocircleStartParamIncludedAngCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 10, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Radius
                var radius = 10;
                JigPromptDistanceOptions optRadius = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optRadius = options.Options;
                        optRadius.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", radius));

                // Setup Start Param
                var startParam = 30;
                JigPromptAngleOptions optStartParam = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

                // Setup Included Ang
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
                var includedAng = 70;
                JigPromptAngleOptions optIncludedAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optIncludedAng = options.Options;
                        optIncludedAng.DefaultValue = Math.PI;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", includedAng));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest36(cmd, optFirst, optCenter, optRadius, optStartParam, optIncludedAng, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern36: Ellipse Isocircle - StartParameter - IncludedAngle

        #region Pattern37: Ellipse Isocircle - Diameter - StartAngle - EndAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST37", CommandFlags.Modal)]
        public void EllipseTestPattern37()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartEndAngCoordTest1();
                        coordTest.EllipseIsocircleDiameterStartEndAngCoordTest2();
                        coordTest.EllipseIsocircleDiameterStartEndAngCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartEndAngCoordTest4();
                        coordTest.EllipseIsocircleDiameterStartEndAngCoordTest5();
                        coordTest.EllipseIsocircleDiameterStartEndAngCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartEndAngCoordTest7();
                        coordTest.EllipseIsocircleDiameterStartEndAngCoordTest8();
                        coordTest.EllipseIsocircleDiameterStartEndAngCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartEndAngCoordTest10();
                        coordTest.EllipseIsocircleDiameterStartEndAngCoordTest11();
                        coordTest.EllipseIsocircleDiameterStartEndAngCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 10, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Diameter
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));
                var diameter = 10;
                JigPromptDistanceOptions optDiameter = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optDiameter = options.Options;
                        optDiameter.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", diameter));

                // Setup Start Ang
                var startAng = 30;
                JigPromptAngleOptions optStartAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

                // Setup End Ang
                var endAng = 70;
                JigPromptAngleOptions optEndAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndAng = options.Options;
                        optEndAng.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endAng));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest37(cmd, optFirst, optCenter, optDiameter, optStartAng, optEndAng, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern37: Ellipse Isocircle - Diameter - StartAngle - EndAngle

        #region Pattern38: Ellipse Isocircle - Diameter - StartAngle - EndParameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST38", CommandFlags.Modal)]
        public void EllipseTestPattern38()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartAngEndParamCoordTest1();
                        coordTest.EllipseIsocircleDiameterStartAngEndParamCoordTest2();
                        coordTest.EllipseIsocircleDiameterStartAngEndParamCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartAngEndParamCoordTest4();
                        coordTest.EllipseIsocircleDiameterStartAngEndParamCoordTest5();
                        coordTest.EllipseIsocircleDiameterStartAngEndParamCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartAngEndParamCoordTest7();
                        coordTest.EllipseIsocircleDiameterStartAngEndParamCoordTest8();
                        coordTest.EllipseIsocircleDiameterStartAngEndParamCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartAngEndParamCoordTest10();
                        coordTest.EllipseIsocircleDiameterStartAngEndParamCoordTest11();
                        coordTest.EllipseIsocircleDiameterStartAngEndParamCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 10, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Diameter
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));
                var diameter = 10;
                JigPromptDistanceOptions optDiameter = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optDiameter = options.Options;
                        optDiameter.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", diameter));

                // Setup Start Ang
                var startAng = 30;
                JigPromptAngleOptions optStartAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

                // Setup End Param
                var endParam = 70;
                JigPromptAngleOptions optEndParam = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndParam = options.Options;
                        optEndParam.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endParam));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest38(cmd, optFirst, optCenter, optDiameter, optStartAng, optEndParam, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern38: Ellipse Isocircle - Diameter - StartAngle - EndParameter

        #region Pattern39: Ellipse Isocircle - Diameter - StartAngle - IncludedAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST39", CommandFlags.Modal)]
        public void EllipseTestPattern39()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartAngIncludedAngCoordTest1();
                        coordTest.EllipseIsocircleDiameterStartAngIncludedAngCoordTest2();
                        coordTest.EllipseIsocircleDiameterStartAngIncludedAngCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartAngIncludedAngCoordTest4();
                        coordTest.EllipseIsocircleDiameterStartAngIncludedAngCoordTest5();
                        coordTest.EllipseIsocircleDiameterStartAngIncludedAngCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartAngIncludedAngCoordTest7();
                        coordTest.EllipseIsocircleDiameterStartAngIncludedAngCoordTest8();
                        coordTest.EllipseIsocircleDiameterStartAngIncludedAngCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartAngIncludedAngCoordTest10();
                        coordTest.EllipseIsocircleDiameterStartAngIncludedAngCoordTest11();
                        coordTest.EllipseIsocircleDiameterStartAngIncludedAngCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 10, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Diameter
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));
                var diameter = 10;
                JigPromptDistanceOptions optDiameter = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optDiameter = options.Options;
                        optDiameter.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", diameter));

                // Setup Start Ang
                var startAng = 30;
                JigPromptAngleOptions optStartAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartAng = options.Options;
                        optStartAng.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startAng));

                // Setup Included Ang
                var includedAng = 70;
                JigPromptAngleOptions optIncludedAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optIncludedAng = options.Options;
                        optIncludedAng.DefaultValue = Math.PI;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", includedAng));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest39(cmd, optFirst, optCenter, optDiameter, optStartAng, optIncludedAng, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern39: Ellipse Isocircle - Diameter - StartAngle - IncludedAngle

        #region Pattern40: Ellipse Isocircle - Diameter - StartParameter - EndParameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST40", CommandFlags.Modal)]
        public void EllipseTestPattern40()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartEndParamCoordTest1();
                        coordTest.EllipseIsocircleDiameterStartEndParamCoordTest2();
                        coordTest.EllipseIsocircleDiameterStartEndParamCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartEndParamCoordTest4();
                        coordTest.EllipseIsocircleDiameterStartEndParamCoordTest5();
                        coordTest.EllipseIsocircleDiameterStartEndParamCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartEndParamCoordTest7();
                        coordTest.EllipseIsocircleDiameterStartEndParamCoordTest8();
                        coordTest.EllipseIsocircleDiameterStartEndParamCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartEndParamCoordTest10();
                        coordTest.EllipseIsocircleDiameterStartEndParamCoordTest11();
                        coordTest.EllipseIsocircleDiameterStartEndParamCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 10, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Diameter
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));
                var diameter = 10;
                JigPromptDistanceOptions optDiameter = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optDiameter = options.Options;
                        optDiameter.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", diameter));

                // Setup Start Param
                var startParam = 30;
                JigPromptAngleOptions optStartParam = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

                // Setup End Param
                var endParam = 70;
                JigPromptAngleOptions optEndParam = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndParam = options.Options;
                        optEndParam.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endParam));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest40(cmd, optFirst, optCenter, optDiameter, optStartParam, optEndParam, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern40: Ellipse Isocircle - Diameter - StartParameter - EndParameter

        #region Pattern41: Ellipse Isocircle - Diameter - StartParameter - EndAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST41", CommandFlags.Modal)]
        public void EllipseTestPattern41()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartParamEndAngCoordTest1();
                        coordTest.EllipseIsocircleDiameterStartParamEndAngCoordTest2();
                        coordTest.EllipseIsocircleDiameterStartParamEndAngCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartParamEndAngCoordTest4();
                        coordTest.EllipseIsocircleDiameterStartParamEndAngCoordTest5();
                        coordTest.EllipseIsocircleDiameterStartParamEndAngCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartParamEndAngCoordTest7();
                        coordTest.EllipseIsocircleDiameterStartParamEndAngCoordTest8();
                        coordTest.EllipseIsocircleDiameterStartParamEndAngCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartParamEndAngCoordTest10();
                        coordTest.EllipseIsocircleDiameterStartParamEndAngCoordTest11();
                        coordTest.EllipseIsocircleDiameterStartParamEndAngCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 10, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Diameter
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));
                var diameter = 10;
                JigPromptDistanceOptions optDiameter = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optDiameter = options.Options;
                        optDiameter.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", diameter));

                // Setup Start Param
                var startParam = 30;
                JigPromptAngleOptions optStartParam = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

                // Setup End Ang
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "A", 0));
                var endAng = 70;
                JigPromptAngleOptions optEndAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optEndAng = options.Options;
                        optEndAng.DefaultValue = Math.PI * 2;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", endAng));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest41(cmd, optFirst, optCenter, optDiameter, optStartParam, optEndAng, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern41: Ellipse EllipseIsocircle - Diameter - StartParameter - EndAngle

        #region Pattern42: Ellipse Isocircle - Diameter - StartParameter - IncludedAngle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST42", CommandFlags.Modal)]
        public void EllipseTestPattern42()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartParamIncludedAngCoordTest1();
                        coordTest.EllipseIsocircleDiameterStartParamIncludedAngCoordTest2();
                        coordTest.EllipseIsocircleDiameterStartParamIncludedAngCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartParamIncludedAngCoordTest4();
                        coordTest.EllipseIsocircleDiameterStartParamIncludedAngCoordTest5();
                        coordTest.EllipseIsocircleDiameterStartParamIncludedAngCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartParamIncludedAngCoordTest7();
                        coordTest.EllipseIsocircleDiameterStartParamIncludedAngCoordTest8();
                        coordTest.EllipseIsocircleDiameterStartParamIncludedAngCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.EllipseIsocircleDiameterStartParamIncludedAngCoordTest10();
                        coordTest.EllipseIsocircleDiameterStartParamIncludedAngCoordTest11();
                        coordTest.EllipseIsocircleDiameterStartParamIncludedAngCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.EllipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

                PromptPointOptions optFirst = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                var centerPt = new Point3d(10, 10, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Diameter
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));
                var diameter = 10;
                JigPromptDistanceOptions optDiameter = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optDiameter = options.Options;
                        optDiameter.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", diameter));

                // Setup Start Param
                var startParam = 30;
                JigPromptAngleOptions optStartParam = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optStartParam = options.Options;
                        optStartParam.DefaultValue = 0;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "30", startParam));

                // Setup Included Ang
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
                var includedAng = 70;
                JigPromptAngleOptions optIncludedAng = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        optIncludedAng = options.Options;
                        optIncludedAng.DefaultValue = Math.PI;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "70", includedAng));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest42(cmd, optFirst, optCenter, optDiameter, optStartParam, optIncludedAng, centerPt);

                ////////////////////////////////////////
                // Test Completed
                ////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern42: Ellipse Isocircle - Diameter - StartParameter - IncludedAngle

        #region Pattern43: PELLIPSE Axis - End - Distance
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST43", CommandFlags.Modal)]
        public void EllipseTestPattern43()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseAxisEndCoordTest1();
                    coordTest.PellipseAxisEndCoordTest2();
                    coordTest.PellipseAxisEndCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseAxisEndCoordTest4();
                    coordTest.PellipseAxisEndCoordTest5();
                    coordTest.PellipseAxisEndCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseAxisEndCoordTest7();
                    coordTest.PellipseAxisEndCoordTest8();
                    coordTest.PellipseAxisEndCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseAxisEndCoordTest10();
                    coordTest.PellipseAxisEndCoordTest11();
                    coordTest.PellipseAxisEndCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.PellipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////
            using (new SystemVariable.Temp("PELLIPSE", 1))
            {
                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup First Point
                var sequence = new MockSequence();
                PromptPointOptions optFirst = null;
                var firstPt = new Point3d(10, 0, 0);
                userInputMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

                // Setup Second Point
                JigPromptPointOptions optSecond = null;
                var secondPt = new Point3d(0, 10, 0);
                Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PointDrawJigN.JigActions options) =>
                    {
                        optSecond = options.Options;
                        optSecond.DefaultValue = new Point3d();
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

                // Setup Distance
                var distance = 10;
                JigPromptDistanceOptions optDistance = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optDistance = options.Options;
                        optDistance.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

                //Todo: Setup Rotation

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest43(cmd, optFirst, optSecond, optDistance, firstPt, secondPt);

                //////////////////////////////////////////
                //// Test Completed
                //////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern43: Axis - End - Distance

        #region Pattern44: PELLIPSE Axis - End - Rotation
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST44", CommandFlags.Modal)]
        public void EllipseTestPattern44()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseAxisEndRotationCoordTest1();
                    coordTest.PellipseAxisEndRotationCoordTest2();
                    coordTest.PellipseAxisEndRotationCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseAxisEndRotationCoordTest4();
                    coordTest.PellipseAxisEndRotationCoordTest5();
                    coordTest.PellipseAxisEndRotationCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseAxisEndRotationCoordTest7();
                    coordTest.PellipseAxisEndRotationCoordTest8();
                    coordTest.PellipseAxisEndRotationCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseAxisEndRotationCoordTest10();
                    coordTest.PellipseAxisEndRotationCoordTest11();
                    coordTest.PellipseAxisEndRotationCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.PellipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////
            using (new SystemVariable.Temp("PELLIPSE", 1))
            {
                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup First Point
                var sequence = new MockSequence();
                PromptPointOptions optFirst = null;
                var firstPt = new Point3d(10, 0, 0);
                userInputMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", firstPt));

                // Setup Second Point
                JigPromptPointOptions optSecond = null;
                var secondPt = new Point3d(0, 10, 0);
                Point3d secondPtW = secondPt.TransformBy(CoordConverter.UcsToWcs());
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PointDrawJigN.JigActions options) =>
                    {
                        optSecond = options.Options;
                        optSecond.DefaultValue = new Point3d();
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPtW));

                // Setup Rotation
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                        .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
                var rotation = 10;
                JigPromptAngleOptions optRotation = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                        .Callback((AngleDrawJigN.JigActions options) =>
                        {
                            optRotation = options.Options;
                            optRotation.DefaultValue = 0;
                            options.OnUpdate();
                        })
                        .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest44(cmd, optFirst, optSecond, optRotation, firstPt, secondPt);

                //////////////////////////////////////////
                //// Test Completed
                //////////////////////////////////////////
                userInputMock.VerifyAll();
                OnFinish();
            }
        }
        #endregion Pattern1: Axis - End - Rotation

        #region Pattern45: PELLIPSE Center - Distance
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST45", CommandFlags.Modal)]
        public void EllipseTestPattern45()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseCenterCoordTest1();
                    coordTest.PellipseCenterCoordTest2();
                    coordTest.PellipseCenterCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseCenterCoordTest4();
                    coordTest.PellipseCenterCoordTest5();
                    coordTest.PellipseCenterCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseCenterCoordTest7();
                    coordTest.PellipseCenterCoordTest8();
                    coordTest.PellipseCenterCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseCenterCoordTest10();
                    coordTest.PellipseCenterCoordTest11();
                    coordTest.PellipseCenterCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.PellipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////
            using (new SystemVariable.Temp("PELLIPSE", 1))
            {
                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();
                userInputMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

                var centerPt = new Point3d(10, 0, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Second Point
                JigPromptPointOptions optSecond = null;
                var secondPt = new Point3d(0, 10, 0);
                secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PointDrawJigN.JigActions options) =>
                    {
                        optSecond = options.Options;
                        optSecond.DefaultValue = new Point3d();
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

                // Setup Distance
                var distance = 10;
                JigPromptDistanceOptions optDistance = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        optDistance = options.Options;
                        optDistance.DefaultValue = 1;
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", distance));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest45(cmd, optCenter, optSecond, optDistance, centerPt);

                //////////////////////////////////////////
                //// Test Completed
                //////////////////////////////////////////
                userInputMock.VerifyAll();

                OnFinish();
            }
        }
        #endregion Pattern45: PELLIPSE Center - Distance

        #region Pattern46: PELLIPSE Center - Rotation
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST46", CommandFlags.Modal)]
        public void EllipseTestPattern46()
        {
            OnStart();

            //Coordinate TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                case "W": //WCS
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseCenterRotationCoordTest1();
                    coordTest.PellipseCenterRotationCoordTest2();
                    coordTest.PellipseCenterRotationCoordTest3();
                    break;
                case "1": //UCS1
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseCenterRotationCoordTest4();
                    coordTest.PellipseCenterRotationCoordTest5();
                    coordTest.PellipseCenterRotationCoordTest6();
                    break;
                case "2": //UCS2
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseCenterRotationCoordTest7();
                    coordTest.PellipseCenterRotationCoordTest8();
                    coordTest.PellipseCenterRotationCoordTest9();
                    break;
                case "3": //UCS3
                    changeCoordinateSystem(_resValueSet.StringResult);
                    coordTest.PellipseCenterRotationCoordTest10();
                    coordTest.PellipseCenterRotationCoordTest11();
                    coordTest.PellipseCenterRotationCoordTest12();
                    break;
                default:
                    return;
            }
            coordTest.LastEllipseId.IsNull.IsFalse();

            //Propertytest
            PropertyTest.PellipsePropertyTest(coordTest.LastEllipseId);

            //MessageTest
            //////////////////////////////////////////
            //// Setup Mock
            //////////////////////////////////////////

            using (new SystemVariable.Temp("PELLIPSE", 1))
            {
                // Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                // Setup Center Point
                var sequence = new MockSequence();
                userInputMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

                var centerPt = new Point3d(10, 0, 0);
                PromptPointOptions optCenter = null;
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PromptPointOptions options) => optCenter = options)
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                // Setup Second Point
                JigPromptPointOptions optSecond = null;
                var secondPt = new Point3d(0, 10, 0);
                secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PointDrawJigN.JigActions options) =>
                    {
                        optSecond = options.Options;
                        optSecond.DefaultValue = new Point3d();
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "0,0,0", secondPt));

                // Setup Rotation
                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                        .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
                var rotation = 10;
                JigPromptAngleOptions optRotation = null;
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                        .Callback((AngleDrawJigN.JigActions options) =>
                        {
                            optRotation = options.Options;
                            optRotation.DefaultValue = 0;
                            options.OnUpdate();
                        })
                        .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "10", rotation));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();

                MessageTest.EllipseMessageTest46(cmd, optCenter, optSecond, optRotation, centerPt);

                //////////////////////////////////////////
                //// Test Completed
                //////////////////////////////////////////
                userInputMock.VerifyAll();

                OnFinish();
            }

        }
        #endregion Pattern46: PELLIPSE Center - Rotation

        #region Pattern47: PELLIPSE Isocircle
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST47", CommandFlags.Modal)]
        public void EllipseTestPattern47()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.PellipseIsocircleCoordTest1();
                        coordTest.PellipseIsocircleCoordTest2();
                        coordTest.PellipseIsocircleCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.PellipseIsocircleCoordTest4();
                        coordTest.PellipseIsocircleCoordTest5();
                        coordTest.PellipseIsocircleCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.PellipseIsocircleCoordTest7();
                        coordTest.PellipseIsocircleCoordTest8();
                        coordTest.PellipseIsocircleCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.PellipseIsocircleCoordTest10();
                        coordTest.PellipseIsocircleCoordTest11();
                        coordTest.PellipseIsocircleCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.PellipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////

                using (new SystemVariable.Temp("PELLIPSE", 1))
                {
                    // Create Mock
                    var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                    // Setup Center Point
                    var sequence = new MockSequence();
                    PromptPointOptions optFirst = null;
                    userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                        .Callback((PromptPointOptions options) => optFirst = options)
                        .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                    var centerPt = new Point3d(10, 0, 0);
                    PromptPointOptions optCenter = null;
                    userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                        .Callback((PromptPointOptions options) => optCenter = options)
                        .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                    // Setup Radius
                    var radius = 10;
                    JigPromptDistanceOptions optRadius = null;
                    userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                        .Callback((DistanceDrawJigN.JigActions options) =>
                        {
                            optRadius = options.Options;
                            optRadius.DefaultValue = 1;
                            options.OnUpdate();
                        })
                        .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", radius));

                    // Setup AppendedEllipses
                    var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                    userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                        .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                    // Execute Ellipse
                    var cmd = new EllipseCmd(userInputMock.Object);
                    cmd.OnCommand();

                    MessageTest.EllipseMessageTest47(cmd, optFirst, optCenter, optRadius, centerPt);

                    ////////////////////////////////////////
                    // Test Completed
                    ////////////////////////////////////////
                    userInputMock.VerifyAll();
                    OnFinish();
                }
            }
        }
        #endregion Pattern47: PELLIPSE Isocircle

        #region Pattern48: PELLIPSE Isocircle - Diameter
        [CommandMethod("JrxCommandTest", "SmxELLIPSETEST48", CommandFlags.Modal)]
        public void EllipseTestPattern48()
        {
            OnStart();

            AfterStart();

            using (new SystemVariable.Temp("SNAPSTYL", 1))
            {
                changeIsometricPlane(_resValueIsoplaneSet.StringResult);

                //Coordinate TEST
                var coordTest = new CoordTest();
                switch (_resValueSet.StringResult)
                {
                    case "W": //WCS
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.PellipseIsocircleDiameterCoordTest1();
                        coordTest.PellipseIsocircleDiameterCoordTest2();
                        coordTest.PellipseIsocircleDiameterCoordTest3();
                        break;
                    case "1": //UCS1
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.PellipseIsocircleDiameterCoordTest4();
                        coordTest.PellipseIsocircleDiameterCoordTest5();
                        coordTest.PellipseIsocircleDiameterCoordTest6();
                        break;
                    case "2": //UCS2
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.PellipseIsocircleDiameterCoordTest7();
                        coordTest.PellipseIsocircleDiameterCoordTest8();
                        coordTest.PellipseIsocircleDiameterCoordTest9();
                        break;
                    case "3": //UCS3
                        changeCoordinateSystem(_resValueSet.StringResult);
                        coordTest.PellipseIsocircleDiameterCoordTest10();
                        coordTest.PellipseIsocircleDiameterCoordTest11();
                        coordTest.PellipseIsocircleDiameterCoordTest12();
                        break;
                    default:
                        return;
                }
                coordTest.LastEllipseId.IsNull.IsFalse();

                //Propertytest
                PropertyTest.PellipsePropertyTest(coordTest.LastEllipseId);

                //MessageTest
                //////////////////////////////////////////
                //// Setup Mock
                //////////////////////////////////////////
                using (new SystemVariable.Temp("PELLIPSE", 1))
                {
                    // Create Mock
                    var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);

                    // Setup Center Point
                    var sequence = new MockSequence();

                    PromptPointOptions optFirst = null;
                    userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                        .Callback((PromptPointOptions options) => optFirst = options)
                        .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                    var centerPt = new Point3d(10, 0, 0);
                    PromptPointOptions optCenter = null;
                    userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                        .Callback((PromptPointOptions options) => optCenter = options)
                        .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                    // Setup Diameter
                    userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                        .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));
                    var diameter = 10;
                    JigPromptDistanceOptions optDiameter = null;
                    userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                        .Callback((DistanceDrawJigN.JigActions options) =>
                        {
                            optDiameter = options.Options;
                            optDiameter.DefaultValue = 1;
                            options.OnUpdate();
                        })
                        .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", diameter));

                    // Setup AppendedEllipses
                    var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                    userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                        .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                    // Execute Ellipse
                    var cmd = new EllipseCmd(userInputMock.Object);
                    cmd.OnCommand();

                    MessageTest.EllipseMessageTest48(cmd, optFirst, optCenter, optDiameter, centerPt);

                    ////////////////////////////////////////
                    // Test Completed
                    ////////////////////////////////////////
                    userInputMock.VerifyAll();
                    OnFinish();
                }
            }
        }
        #endregion Pattern48: PELLIPSE Isocircle - Diameter
    }
}
