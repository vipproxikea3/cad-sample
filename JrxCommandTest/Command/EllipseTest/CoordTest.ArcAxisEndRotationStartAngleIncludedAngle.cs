﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartAngIncludedAngCoordTest1", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartAngIncludedAngCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double rotation = 30;
                double startAng = 30;
                double includedAng = 40;

                DoTestArcAxisEndRotationStartAngIncludedAng(pt1, pt2, rotation, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartAngIncludedAngCoordTest2", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartAngIncludedAngCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double rotation = 34;
                double startAng = 210;
                double includedAng = 30;

                DoTestArcAxisEndRotationStartAngIncludedAng(pt1, pt2, rotation, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartAngIncludedAngCoordTest3", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartAngIncludedAngCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double rotation = 20;
                double startAng = 150;
                double includedAng = 200;

                DoTestArcAxisEndRotationStartAngIncludedAng(pt1, pt2, rotation, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartAngIncludedAngCoordTest4", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartAngIncludedAngCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double rotation = 30;
                double startAng = 30;
                double includedAng = 40;

                DoTestArcAxisEndRotationStartAngIncludedAng(pt1, pt2, rotation, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartAngIncludedAngCoordTest5", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartAngIncludedAngCoordTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double rotation = 34;
                double startAng = 210;
                double includedAng = 30;

                DoTestArcAxisEndRotationStartAngIncludedAng(pt1, pt2, rotation, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartAngIncludedAngCoordTest6", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartAngIncludedAngCoordTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double rotation = 20;
                double startAng = 150;
                double includedAng = 200;

                DoTestArcAxisEndRotationStartAngIncludedAng(pt1, pt2, rotation, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartAngIncludedAngCoordTest7", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartAngIncludedAngCoordTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double rotation = 30;
                double startAng = 30;
                double includedAng = 40;

                DoTestArcAxisEndRotationStartAngIncludedAng(pt1, pt2, rotation, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartAngIncludedAngCoordTest8", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartAngIncludedAngCoordTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double rotation = 34;
                double startAng = 210;
                double includedAng = 30;

                DoTestArcAxisEndRotationStartAngIncludedAng(pt1, pt2, rotation, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartAngIncludedAngCoordTest9", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartAngIncludedAngCoordTest9()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double rotation = 20;
                double startAng = 150;
                double includedAng = 200;

                DoTestArcAxisEndRotationStartAngIncludedAng(pt1, pt2, rotation, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartAngIncludedAngCoordTest10", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartAngIncludedAngCoordTest10()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double rotation = 30;
                double startAng = 30;
                double includedAng = 40;

                DoTestArcAxisEndRotationStartAngIncludedAng(pt1, pt2, rotation, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartAngIncludedAngCoordTest11", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartAngIncludedAngCoordTest11()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double rotation = 34;
                double startAng = 210;
                double includedAng = 30;

                DoTestArcAxisEndRotationStartAngIncludedAng(pt1, pt2, rotation, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartAngIncludedAngCoordTest12", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartAngIncludedAngCoordTest12()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double rotation = 20;
                double startAng = 150;
                double includedAng = 200;

                DoTestArcAxisEndRotationStartAngIncludedAng(pt1, pt2, rotation, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestArcAxisEndRotationStartAngIncludedAng(Point3d firstPt, Point3d secondPt, double rotation, double startAng, double includedAng)
        {
            ExpectedResult expectedResult = new EllipseCalculator(firstPt, secondPt, rotation, Math.PI * startAng / 180,
                Math.PI * includedAng / 180).CalculateValueArcAxisEndRotationStartAngIncludedAng();

            var objIds = DoTestEllipseArcAxisEndRotationEndIncludedAng(firstPt, secondPt, rotation, Math.PI * startAng / 180, Math.PI * includedAng / 180);
            VerifyEllipseArc(objIds, expectedResult);
        }

        private ObjectIdCollection DoTestEllipseArcAxisEndRotationEndIncludedAng(Point3d firstPt, Point3d secondPt, double rotation,
            double startAng, double includedAng)
        {
            //// Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", firstPt));

            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", secondPt));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", rotation));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + startAng, startAng));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + includedAng, includedAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedEllipseIds;
        }
    }
}
