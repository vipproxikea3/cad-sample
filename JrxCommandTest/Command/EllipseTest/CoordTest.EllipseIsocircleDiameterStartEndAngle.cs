﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartEndAngCoordTest1", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartEndAngCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                double diameter = 30;
                double startAng = 30;
                double endAng = 40;

                DoTestEllipseIsocircleDiameterStartEndAng(pt1, diameter, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartEndAngCoordTest2", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartEndAngCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(30, 40, 50);
                double diameter = 34;
                double startAng = 210;
                double endAng = 30;

                DoTestEllipseIsocircleDiameterStartEndAng(pt1, diameter, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartEndAngCoordTest3", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartEndAngCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 50, 30);
                double diameter = 20;
                double startAng = 150;
                double endAng = 200;

                DoTestEllipseIsocircleDiameterStartEndAng(pt1, diameter, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartEndAngCoordTest4", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartEndAngCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                double diameter = 30;
                double startAng = 30;
                double endAng = 40;

                DoTestEllipseIsocircleDiameterStartEndAng(pt1, diameter, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartEndAngCoordTest5", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartEndAngCoordTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(30, 40, 50);
                double diameter = 34;
                double startAng = 210;
                double endAng = 30;

                DoTestEllipseIsocircleDiameterStartEndAng(pt1, diameter, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartEndAngCoordTest6", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartEndAngCoordTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 50, 30);
                double diameter = 20;
                double startAng = 150;
                double endAng = 200;

                DoTestEllipseIsocircleDiameterStartEndAng(pt1, diameter, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartEndAngCoordTest7", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartEndAngCoordTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                double diameter = 30;
                double startAng = 30;
                double endAng = 40;

                DoTestEllipseIsocircleDiameterStartEndAng(pt1, diameter, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartEndAngCoordTest8", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartEndAngCoordTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(30, 40, 50);
                double diameter = 34;
                double startAng = 210;
                double endAng = 30;

                DoTestEllipseIsocircleDiameterStartEndAng(pt1, diameter, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartEndAngCoordTest9", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartEndAngCoordTest9()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 50, 30);
                double diameter = 20;
                double startAng = 150;
                double endAng = 200;

                DoTestEllipseIsocircleDiameterStartEndAng(pt1, diameter, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartEndAngCoordTest10", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartEndAngCoordTest10()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                double diameter = 30;
                double startAng = 30;
                double endAng = 40;

                DoTestEllipseIsocircleDiameterStartEndAng(pt1, diameter, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartEndAngCoordTest11", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartEndAngCoordTest11()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(30, 40, 50);
                double diameter = 34;
                double startAng = 210;
                double endAng = 30;

                DoTestEllipseIsocircleDiameterStartEndAng(pt1, diameter, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleDiameterStartEndAngCoordTest12", CommandFlags.Modal)]
        public void EllipseIsocircleDiameterStartEndAngCoordTest12()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 50, 30);
                double diameter = 20;
                double startAng = 150;
                double endAng = 200;

                DoTestEllipseIsocircleDiameterStartEndAng(pt1, diameter, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestEllipseIsocircleDiameterStartEndAng(Point3d centerPt, double diameter, double startAng, double endAng)
        {
            ExpectedResult expectedResult = new EllipseCalculator(centerPt, diameter, Math.PI * startAng / 180, Math.PI * endAng / 180)
                    .CalculateValueEllipseIsocircleDiameterStartEndAng();
            var objIds = DoTestEllipseArcEllipseIsocircleDiameterStartEndAng(centerPt, diameter, Math.PI * startAng / 180, Math.PI * endAng / 180);
            VerifyEllipseArc(objIds, expectedResult);
        }

        private ObjectIdCollection DoTestEllipseArcEllipseIsocircleDiameterStartEndAng(Point3d centerPt, double diameter, double startAng, double endAng)
        {
            //// Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", diameter));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + startAng, startAng));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + endAng, endAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedEllipseIds;
        }
    }
}
