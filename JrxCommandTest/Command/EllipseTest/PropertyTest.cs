﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using Ellipse = GrxCAD.DatabaseServices.Ellipse;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Ellipse = Autodesk.AutoCAD.DatabaseServices.Ellipse;

#endif
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JrxCad.Helpers;
using JrxCad.Utility;
using System.Reflection;

namespace JrxCadTest.Command.EllipseTest
{
    public class PropertyTest
    {
        public static void EllipsePropertyTest(ObjectId id)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            using (var tr = Util.StartTransaction())
            using (var ellipse = tr.GetObject<Ellipse>(id, OpenMode.ForRead))
            {
                try
                {
                    ellipse.Color.ColorIndex.Is((short)1);
                    ellipse.Linetype.Is("Continuous");
                    ellipse.LineWeight.Is(LineWeight.LineWeight100);
                    ellipse.Transparency.Alpha.Is((byte)178);
                    ellipse.LinetypeScale.Is(18);
                    ellipse.PlotStyleNameId.Type.Is(PlotStyleNameType.PlotStyleNameByLayer);
                    Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
                }
                catch (AssertFailedException)
                {
                    Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
                }
                catch (Exception)
                {
                    Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
                }
            }
        }
        public static void PellipsePropertyTest(ObjectId id)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            using (var tr = Util.StartTransaction())
            using (var ellipse = tr.GetObject<Polyline2d>(id, OpenMode.ForRead))
            {
                try
                {
                    ellipse.Color.ColorIndex.Is((short)1);
                    ellipse.Linetype.Is("Continuous");
                    ellipse.LineWeight.Is(LineWeight.LineWeight100);
                    ellipse.Transparency.Alpha.Is((byte)178);
                    ellipse.LinetypeScale.Is(18);
                    ellipse.Thickness.Is(10);
                    ellipse.Material.Is("Global");
                    ellipse.PlotStyleNameId.Type.Is(PlotStyleNameType.PlotStyleNameByLayer);
                    Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
                }
                catch (Exception)
                {
                    Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
                }
            }
        }
    }
}
