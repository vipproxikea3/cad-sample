﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartParamIncludedAngCoordTest1", CommandFlags.Modal)]
        public void EllipseArcCenterStartParamIncludedAngCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;
                double startParam = 30;
                double includedAng = 40;

                DoTestArcCenterStartParamIncludedAng(pt1, pt2, distance, startParam, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartParamIncludedAngCoordTest2", CommandFlags.Modal)]
        public void EllipseArcCenterStartParamIncludedAngCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;
                double startParam = 210;
                double includedAng = 30;

                DoTestArcCenterStartParamIncludedAng(pt1, pt2, distance, startParam, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartParamIncludedAngCoordTest3", CommandFlags.Modal)]
        public void EllipseArcCenterStartParamIncludedAngCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;
                double startParam = 150;
                double includedAng = 200;

                DoTestArcCenterStartParamIncludedAng(pt1, pt2, distance, startParam, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartParamIncludedAngCoordTest4", CommandFlags.Modal)]
        public void EllipseArcCenterStartParamIncludedAngCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;
                double startParam = 30;
                double includedAng = 40;

                DoTestArcCenterStartParamIncludedAng(pt1, pt2, distance, startParam, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartParamIncludedAngCoordTest5", CommandFlags.Modal)]
        public void EllipseArcCenterStartParamIncludedAngCoordTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;
                double startParam = 210;
                double includedAng = 30;

                DoTestArcCenterStartParamIncludedAng(pt1, pt2, distance, startParam, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartParamIncludedAngCoordTest6", CommandFlags.Modal)]
        public void EllipseArcCenterStartParamIncludedAngCoordTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;
                double startParam = 150;
                double includedAng = 200;

                DoTestArcCenterStartParamIncludedAng(pt1, pt2, distance, startParam, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartParamIncludedAngCoordTest7", CommandFlags.Modal)]
        public void EllipseArcCenterStartParamIncludedAngCoordTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;
                double startParam = 30;
                double includedAng = 40;

                DoTestArcCenterStartParamIncludedAng(pt1, pt2, distance, startParam, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartParamIncludedAngCoordTest8", CommandFlags.Modal)]
        public void EllipseArcCenterStartParamIncludedAngCoordTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;
                double startParam = 210;
                double includedAng = 30;

                DoTestArcCenterStartParamIncludedAng(pt1, pt2, distance, startParam, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartParamIncludedAngCoordTest9", CommandFlags.Modal)]
        public void EllipseArcCenterStartParamIncludedAngCoordTest9()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;
                double startParam = 150;
                double includedAng = 200;

                DoTestArcCenterStartParamIncludedAng(pt1, pt2, distance, startParam, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartParamIncludedAngCoordTest10", CommandFlags.Modal)]
        public void EllipseArcCenterStartParamIncludedAngCoordTest10()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;
                double startParam = 30;
                double includedAng = 40;

                DoTestArcCenterStartParamIncludedAng(pt1, pt2, distance, startParam, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartParamIncludedAngCoordTest11", CommandFlags.Modal)]
        public void EllipseArcCenterStartParamIncludedAngCoordTest11()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;
                double startParam = 210;
                double includedAng = 30;

                DoTestArcCenterStartParamIncludedAng(pt1, pt2, distance, startParam, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartParamIncludedAngCoordTest12", CommandFlags.Modal)]
        public void EllipseArcCenterStartParamIncludedAngCoordTest12()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;
                double startParam = 150;
                double includedAng = 200;

                DoTestArcCenterStartParamIncludedAng(pt1, pt2, distance, startParam, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestArcCenterStartParamIncludedAng(Point3d centerPt, Point3d endPt, double distance, double startParam, double includedAng)
        {
            ExpectedResult expectedResult = new EllipseCalculator(centerPt, endPt, distance, Math.PI * startParam / 180,
                Math.PI * includedAng / 180, true).CalculateValueArcCenterStartParamIncludedAng();

            var objIds = DoTestEllipseArcCenterStartParamIncludedAng(centerPt, endPt, distance, Math.PI * startParam / 180, Math.PI * includedAng / 180);
            VerifyEllipseArc(objIds, expectedResult);
        }

        private ObjectIdCollection DoTestEllipseArcCenterStartParamIncludedAng(Point3d centerPt, Point3d endPt, double distance,
            double startParam, double includedAng)
        {
            //// Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
               .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

            endPt = endPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", endPt));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", distance));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + startParam, startParam));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + includedAng, includedAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedEllipseIds;
        }
    }
}
