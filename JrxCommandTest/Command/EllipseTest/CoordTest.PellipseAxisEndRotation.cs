﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxPellipseAxisEndRotationCoordTest1", CommandFlags.Modal)]
        public void PellipseAxisEndRotationCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double rotation = 30;

                DoTestPellipseAxisEndRotation(pt1, pt2, rotation);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseAxisEndRotationCoordTest2", CommandFlags.Modal)]
        public void PellipseAxisEndRotationCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double rotation = 34;

                DoTestPellipseAxisEndRotation(pt1, pt2, rotation);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseAxisEndRotationCoordTest3", CommandFlags.Modal)]
        public void PellipseAxisEndRotationCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double rotation = 20;

                DoTestPellipseAxisEndRotation(pt1, pt2, rotation);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseAxisEndRotationCoordTest4", CommandFlags.Modal)]
        public void PellipseAxisEndRotationCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double rotation = 30;

                DoTestPellipseAxisEndRotation(pt1, pt2, rotation);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseAxisEndRotationCoordTest5", CommandFlags.Modal)]
        public void PellipseAxisEndRotationCoordTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double rotation = 34;

                DoTestPellipseAxisEndRotation(pt1, pt2, rotation);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseAxisEndRotationCoordTest6", CommandFlags.Modal)]
        public void PellipseAxisEndRotationCoordTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double rotation = 20;

                DoTestPellipseAxisEndRotation(pt1, pt2, rotation);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseAxisEndRotationCoordTest7", CommandFlags.Modal)]
        public void PellipseAxisEndRotationCoordTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double rotation = 30;

                DoTestPellipseAxisEndRotation(pt1, pt2, rotation);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseAxisEndRotationCoordTest8", CommandFlags.Modal)]
        public void PellipseAxisEndRotationCoordTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double rotation = 34;

                DoTestPellipseAxisEndRotation(pt1, pt2, rotation);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseAxisEndRotationCoordTest9", CommandFlags.Modal)]
        public void PellipseAxisEndRotationCoordTest9()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double rotation = 20;

                DoTestPellipseAxisEndRotation(pt1, pt2, rotation);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseAxisEndRotationCoordTest10", CommandFlags.Modal)]
        public void PellipseAxisEndRotationCoordTest10()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double rotation = 30;

                DoTestPellipseAxisEndRotation(pt1, pt2, rotation);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseAxisEndRotationCoordTest11", CommandFlags.Modal)]
        public void PellipseAxisEndRotationCoordTest11()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double rotation = 34;

                DoTestPellipseAxisEndRotation(pt1, pt2, rotation);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseAxisEndRotationCoordTest12", CommandFlags.Modal)]
        public void PellipseAxisEndRotationCoordTest12()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double rotation = 20;

                DoTestPellipseAxisEndRotation(pt1, pt2, rotation);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestPellipseAxisEndRotation(Point3d firstPt, Point3d secondPt, double rotation)
        {
            PellipseExpectedResult expectedResult = new EllipseCalculator(firstPt, secondPt, rotation).CalculateValuePellipseAxisEndRotation();
            var objIds = DoTestEllipsePellipseAxisEndRotation(firstPt, secondPt, rotation);
            VerifyPellipse(objIds, expectedResult);
        }

        private ObjectIdCollection DoTestEllipsePellipseAxisEndRotation(Point3d firstPt, Point3d secondPt, double rotation)
        {
            using (new SystemVariable.Temp("PELLIPSE", 1))
            {
                //// Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
                var sequence = new MockSequence();

                userInputMock.Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", firstPt));

                secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Callback((PointDrawJigN.JigActions options) =>
                    {
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", secondPt));

                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                        .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
                userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                        .Callback((AngleDrawJigN.JigActions options) =>
                        {
                            options.OnUpdate();
                        })
                        .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", rotation));


                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();
                return appendedEllipseIds;
            }
        }
    }
}
