﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using JrxCad.Helpers;
using JrxCad.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        private struct ExpectedResult
        {
            public Vector3d MajorAxisVec;
            public double MajorRadius;
            public double MinorRadius;
            public Point3d CenterPt;
            public double EndAngleToStartAngle;
        }
        private struct PellipseExpectedResult
        {
            public Point3d StartPoint;
            public Point3d EndPoint;
            public double Area;
            public double Length;
            public double Elevation;
        }
        public ObjectId LastEllipseId { get; set; }
        private class EllipseCalculator
        {
            private Point3d _firstPt, _secondPt, _centerPt;
            private double _distanceOrRotation;
            private double _radiusOrDiameter;
            private double _startAngOrParam = 0;
            private double _endAngOrParamOrIncludedAng = 2 * Math.PI;

            private Vector3d _majorVec;
            private double _ratio;
            private int _defSnapIsoPair = SystemVariable.GetInt("SNAPISOPAIR");
            private double _AngOfMajorAxis = 0;

            public EllipseCalculator(Point3d pt1, Point3d pt2, double distanceOrRotation, bool centerFirst = false)
            {
                if (centerFirst)
                {
                    (_centerPt, _secondPt, _distanceOrRotation) = (pt1, pt2, distanceOrRotation);
                    _firstPt = _centerPt;
                }
                else
                {
                    (_firstPt, _secondPt, _distanceOrRotation) = (pt1, pt2, distanceOrRotation);
                }
            }

            public EllipseCalculator(Point3d pt1, Point3d pt2, double distanceOrRotation, double startAngOrParam, double endAngOrParamOrIncludedAng, bool centerFirst = false)
            {
                if (centerFirst)
                {
                    (_centerPt, _secondPt, _distanceOrRotation, _startAngOrParam, _endAngOrParamOrIncludedAng) = (pt1, pt2, distanceOrRotation, startAngOrParam, endAngOrParamOrIncludedAng);
                    _firstPt = _centerPt;
                }
                else
                {
                    (_firstPt, _secondPt, _distanceOrRotation, _startAngOrParam, _endAngOrParamOrIncludedAng) = (pt1, pt2, distanceOrRotation, startAngOrParam, endAngOrParamOrIncludedAng);
                }
            }

            public EllipseCalculator(Point3d pt1, double radiusOrDiameter)
            {
                (_centerPt, _radiusOrDiameter) = (pt1, radiusOrDiameter);
                _firstPt = _centerPt;
            }

            public EllipseCalculator(Point3d pt1, double radiusOrDiameter, double startAngOrParam, double endAngOrParamOrIncludedAng)
            {
                (_centerPt, _radiusOrDiameter, _startAngOrParam, _endAngOrParamOrIncludedAng) = (pt1, radiusOrDiameter, startAngOrParam, endAngOrParamOrIncludedAng);
                _firstPt = _centerPt;
            }

            private ExpectedResult Calculate()
            {
                Ellipse temp = new Ellipse(_centerPt, Vector3d.ZAxis, _majorVec, _ratio, _startAngOrParam, _endAngOrParamOrIncludedAng);
                temp.TransformBy(CoordConverter.UcsToWcs());

                ExpectedResult Expect = new ExpectedResult();
                Expect.CenterPt = _centerPt;
                Point3d tempPt = new Point3d(temp.MajorAxis.X, temp.MajorAxis.Y, temp.MajorAxis.Z);
                tempPt = tempPt.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = tempPt.GetAsVector();
                Expect.MajorAxisVec = new Vector3d(_majorVec.X.ToTestVal(), _majorVec.Y.ToTestVal(), _majorVec.Z.ToTestVal());
                Expect.MajorRadius = temp.MajorRadius.ToTestVal();
                Expect.MinorRadius = temp.MinorRadius.ToTestVal();
                Expect.EndAngleToStartAngle = (temp.EndAngle - temp.StartAngle).ToTestVal();
                temp.Dispose();
                temp = null;
                return Expect;
            }

            public PellipseExpectedResult CalculatePellipse()
            {
                Ellipse temp = new Ellipse(_centerPt, Vector3d.ZAxis, _majorVec, _ratio, _startAngOrParam, _endAngOrParamOrIncludedAng);
                temp.TransformBy(CoordConverter.UcsToWcs());

                var firstOrCenterPtW = _firstPt.TransformBy(CoordConverter.UcsToWcs());
                Polyline2d polyline2d = temp.ToPolyline(firstOrCenterPtW).ConvertTo(false);

                PellipseExpectedResult Expect = new PellipseExpectedResult();
                Expect.Area = polyline2d.Area.ToTestVal();
                Expect.Length = polyline2d.Length.ToTestVal();
                Expect.Elevation = polyline2d.Elevation.ToTestVal();
                Expect.StartPoint = polyline2d.StartPoint;
                Expect.EndPoint = polyline2d.EndPoint;
                temp.Dispose();
                temp = null;
                return Expect;
            }

            public ExpectedResult CalculateValueAxisEnd()
            {
                double curDis = (_firstPt - _secondPt).Length / 2;
                double majorAxis = Math.Max(_distanceOrRotation, curDis);
                double minorAxis = Math.Min(_distanceOrRotation, curDis);

                _majorVec = (_firstPt - _secondPt) / 2;
                _ratio = minorAxis / majorAxis;
                if (_distanceOrRotation == majorAxis)
                {
                    _majorVec = _majorVec.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                    _majorVec /= _ratio;
                }

                _centerPt = Point3dHelper.CenterPoint(_firstPt, _secondPt);
                return Calculate();
            }

            public ExpectedResult CalculateValueAxisEndRotation()
            {
                _majorVec = (_firstPt - _secondPt) / 2;

                if (Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180)) > 0.0101)
                {
                    _ratio = Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180));
                }

                _centerPt = Point3dHelper.CenterPoint(_firstPt, _secondPt);
                return Calculate();
            }

            public ExpectedResult CalculateValueCenter()
            {
                double curDis = (_secondPt - _centerPt).Length;
                double majorAxis = Math.Max(_distanceOrRotation, curDis);
                double minorAxis = Math.Min(_distanceOrRotation, curDis);

                _majorVec = _secondPt - _centerPt;
                _ratio = minorAxis / majorAxis;
                if (_distanceOrRotation == majorAxis)
                {
                    _majorVec = _majorVec.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                    _majorVec /= _ratio;
                }

                return Calculate();
            }

            public ExpectedResult CalculateValueCenterRotation()
            {
                _majorVec = _secondPt - _centerPt;

                if (Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180)) > 0.0101)
                {
                    _ratio = Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180));
                }

                return Calculate();
            }

            public ExpectedResult CalculateValueArcAxisEndStartEndAng()
            {
                return CalculateValueAxisEnd();
            }

            public ExpectedResult CalculateValueArcAxisEndStartAngEndParam()
            {
                double curDis = (_firstPt - _secondPt).Length / 2;
                double majorAxis = Math.Max(_distanceOrRotation, curDis);
                double minorAxis = Math.Min(_distanceOrRotation, curDis);

                _majorVec = (_firstPt - _secondPt) / 2;
                _ratio = minorAxis / majorAxis;
                if (_distanceOrRotation == majorAxis)
                {
                    _majorVec = _majorVec.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                    _majorVec /= _ratio;
                }

                _centerPt = Point3dHelper.CenterPoint(_firstPt, _secondPt);
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcAxisEndStartAngIncludedAng()
            {
                _endAngOrParamOrIncludedAng = _endAngOrParamOrIncludedAng + _startAngOrParam;
                return CalculateValueAxisEnd();
            }

            public ExpectedResult CalculateValueArcAxisEndStartEndParam()
            {
                double curDis = (_firstPt - _secondPt).Length / 2;
                double majorAxis = Math.Max(_distanceOrRotation, curDis);
                double minorAxis = Math.Min(_distanceOrRotation, curDis);

                _majorVec = (_firstPt - _secondPt) / 2;
                _ratio = minorAxis / majorAxis;
                if (_distanceOrRotation == majorAxis)
                {
                    _majorVec = _majorVec.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                    _majorVec /= _ratio;
                }

                _centerPt = Point3dHelper.CenterPoint(_firstPt, _secondPt);
                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcAxisEndStartParamEndAng()
            {
                double curDis = (_firstPt - _secondPt).Length / 2;
                double majorAxis = Math.Max(_distanceOrRotation, curDis);
                double minorAxis = Math.Min(_distanceOrRotation, curDis);

                _majorVec = (_firstPt - _secondPt) / 2;
                _ratio = minorAxis / majorAxis;
                if (_distanceOrRotation == majorAxis)
                {
                    _majorVec = _majorVec.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                    _majorVec /= _ratio;
                }

                _centerPt = Point3dHelper.CenterPoint(_firstPt, _secondPt);
                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcAxisEndStartParamIncludedAng()
            {
                double curDis = (_firstPt - _secondPt).Length / 2;
                double majorAxis = Math.Max(_distanceOrRotation, curDis);
                double minorAxis = Math.Min(_distanceOrRotation, curDis);

                _majorVec = (_firstPt - _secondPt) / 2;
                _ratio = minorAxis / majorAxis;
                if (_distanceOrRotation == majorAxis)
                {
                    _majorVec = _majorVec.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                    _majorVec /= _ratio;
                }

                _centerPt = Point3dHelper.CenterPoint(_firstPt, _secondPt);
                _endAngOrParamOrIncludedAng = _endAngOrParamOrIncludedAng + _startAngOrParam;
                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcAxisEndRotationStartEndAng()
            {
                return CalculateValueAxisEndRotation();
            }

            public ExpectedResult CalculateValueEllipseArcAxisEndRotationStartAngEndParam()
            {

                _majorVec = (_firstPt - _secondPt) / 2;

                if (Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180)) > 0.0101)
                {
                    _ratio = Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180));
                }

                _centerPt = Point3dHelper.CenterPoint(_firstPt, _secondPt);
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcAxisEndRotationStartAngIncludedAng()
            {
                _endAngOrParamOrIncludedAng = _endAngOrParamOrIncludedAng + _startAngOrParam;
                return CalculateValueAxisEndRotation();
            }

            public ExpectedResult CalculateValueArcAxisEndRotationStartEndParam()
            {
                _majorVec = (_firstPt - _secondPt) / 2;

                if (Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180)) > 0.0101)
                {
                    _ratio = Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180));
                }

                _centerPt = Point3dHelper.CenterPoint(_firstPt, _secondPt);
                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcAxisEndRotationStartParamEndAng()
            {
                _majorVec = (_firstPt - _secondPt) / 2;

                if (Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180)) > 0.0101)
                {
                    _ratio = Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180));
                }

                _centerPt = Point3dHelper.CenterPoint(_firstPt, _secondPt);
                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcAxisEndRotationStartParamIncludedAng()
            {
                _majorVec = (_firstPt - _secondPt) / 2;

                if (Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180)) > 0.0101)
                {
                    _ratio = Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180));
                }

                _centerPt = Point3dHelper.CenterPoint(_firstPt, _secondPt);
                _endAngOrParamOrIncludedAng = _endAngOrParamOrIncludedAng + _startAngOrParam;
                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcCenterStartEndAng()
            {
                return CalculateValueCenter();
            }

            public ExpectedResult CalculateValueArcCenterStartAngEndParam()
            {
                double curDis = (_secondPt - _centerPt).Length;
                double majorAxis = Math.Max(_distanceOrRotation, curDis);
                double minorAxis = Math.Min(_distanceOrRotation, curDis);

                _majorVec = _secondPt - _centerPt;
                _ratio = minorAxis / majorAxis;
                if (_distanceOrRotation == majorAxis)
                {
                    _majorVec = _majorVec.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                    _majorVec /= _ratio;
                }

                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcCenterStartAngIncludedAng()
            {
                _endAngOrParamOrIncludedAng = _endAngOrParamOrIncludedAng + _startAngOrParam;
                return CalculateValueCenter();
            }

            public ExpectedResult CalculateValueArcCenterStartEndParam()
            {
                double curDis = (_secondPt - _centerPt).Length;
                double majorAxis = Math.Max(_distanceOrRotation, curDis);
                double minorAxis = Math.Min(_distanceOrRotation, curDis);

                _majorVec = _secondPt - _centerPt;
                _ratio = minorAxis / majorAxis;
                if (_distanceOrRotation == majorAxis)
                {
                    _majorVec = _majorVec.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                    _majorVec /= _ratio;
                }

                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcCenterStartParamEndAng()
            {
                double curDis = (_secondPt - _centerPt).Length;
                double majorAxis = Math.Max(_distanceOrRotation, curDis);
                double minorAxis = Math.Min(_distanceOrRotation, curDis);

                _majorVec = _secondPt - _centerPt;
                _ratio = minorAxis / majorAxis;
                if (_distanceOrRotation == majorAxis)
                {
                    _majorVec = _majorVec.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                    _majorVec /= _ratio;
                }

                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcCenterStartParamIncludedAng()
            {
                double curDis = (_secondPt - _centerPt).Length;
                double majorAxis = Math.Max(_distanceOrRotation, curDis);
                double minorAxis = Math.Min(_distanceOrRotation, curDis);

                _majorVec = _secondPt - _centerPt;
                _ratio = minorAxis / majorAxis;
                if (_distanceOrRotation == majorAxis)
                {
                    _majorVec = _majorVec.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                    _majorVec /= _ratio;
                }

                _endAngOrParamOrIncludedAng = _endAngOrParamOrIncludedAng + _startAngOrParam;
                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcCenterRotationStartEndAng()
            {
                return CalculateValueCenterRotation();
            }

            public ExpectedResult CalculateValueArcCenterRotationStartAngEndParam()
            {
                _majorVec = _secondPt - _centerPt;

                if (Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180)) > 0.0101)
                {
                    _ratio = Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180));
                }

                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcCenterRotationStartAngIncludedAng()
            {
                _endAngOrParamOrIncludedAng = _endAngOrParamOrIncludedAng + _startAngOrParam;
                return CalculateValueCenterRotation();
            }

            public ExpectedResult CalculateValueArcCenterRotationStartEndParam()
            {
                _majorVec = _secondPt - _centerPt;

                if (Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180)) > 0.0101)
                {
                    _ratio = Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180));
                }

                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcCenterRotationStartParamEndAng()
            {
                _majorVec = _secondPt - _centerPt;

                if (Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180)) > 0.0101)
                {
                    _ratio = Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180));
                }

                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));

                return Calculate();
            }

            public ExpectedResult CalculateValueArcCenterRotationStartParamIncludedAng()
            {
                _majorVec = _secondPt - _centerPt;

                if (Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180)) > 0.0101)
                {
                    _ratio = Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180));
                }

                _endAngOrParamOrIncludedAng = _endAngOrParamOrIncludedAng + _startAngOrParam;
                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueEllipseIsocircle()
            {
                switch (_defSnapIsoPair)
                {
                    case 0:
                        _AngOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case 1:
                        _AngOfMajorAxis = 0;
                        break;
                    case 2:
                        _AngOfMajorAxis = Math.PI / 3;
                        break;
                }

                _ratio = Math.Tan(Math.PI / 6);
                _secondPt = new Point3d(_centerPt.X + Math.Sqrt((double)3 / 2), _centerPt.Y, _centerPt.Z);
                Line line = new Line(_centerPt, _secondPt);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(_AngOfMajorAxis, CoordConverter.UCSZAxis(), _centerPt));
                line.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = line.StartPoint - line.EndPoint;
                _majorVec *= _radiusOrDiameter;

                return Calculate();
            }

            public ExpectedResult CalculateValueEllipseIsocircleDiameter()
            {
                switch (_defSnapIsoPair)
                {
                    case 0:
                        _AngOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case 1:
                        _AngOfMajorAxis = 0;
                        break;
                    case 2:
                        _AngOfMajorAxis = Math.PI / 3;
                        break;
                }

                _ratio = Math.Tan(Math.PI / 6);
                _secondPt = new Point3d(_centerPt.X + Math.Sqrt((double)3 / 2), _centerPt.Y, _centerPt.Z);
                Line line = new Line(_centerPt, _secondPt);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(_AngOfMajorAxis, CoordConverter.UCSZAxis(), _centerPt));
                line.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = line.StartPoint - line.EndPoint;
                _majorVec = _majorVec * _radiusOrDiameter / 2;

                return Calculate();
            }

            public ExpectedResult CalculateValueEllipseIsocircleStartEndAng()
            {
                return CalculateValueEllipseIsocircle();
            }

            public ExpectedResult CalculateValueEllipseIsocircleStartAngEndParam()
            {
                switch (_defSnapIsoPair)
                {
                    case 0:
                        _AngOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case 1:
                        _AngOfMajorAxis = 0;
                        break;
                    case 2:
                        _AngOfMajorAxis = Math.PI / 3;
                        break;
                }

                _ratio = Math.Tan(Math.PI / 6);
                _secondPt = new Point3d(_centerPt.X + Math.Sqrt((double)3 / 2), _centerPt.Y, _centerPt.Z);
                Line line = new Line(_centerPt, _secondPt);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(_AngOfMajorAxis, CoordConverter.UCSZAxis(), _centerPt));
                line.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = line.StartPoint - line.EndPoint;
                _majorVec *= _radiusOrDiameter;

                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueEllipseIsocircleStartAngIncludedAng()
            {
                _endAngOrParamOrIncludedAng = _endAngOrParamOrIncludedAng + _startAngOrParam;
                return CalculateValueEllipseIsocircle();
            }

            public ExpectedResult CalculateValueEllipseIsocircleStartEndParam()
            {
                switch (_defSnapIsoPair)
                {
                    case 0:
                        _AngOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case 1:
                        _AngOfMajorAxis = 0;
                        break;
                    case 2:
                        _AngOfMajorAxis = Math.PI / 3;
                        break;
                }

                _ratio = Math.Tan(Math.PI / 6);
                _secondPt = new Point3d(_centerPt.X + Math.Sqrt((double)3 / 2), _centerPt.Y, _centerPt.Z);
                Line line = new Line(_centerPt, _secondPt);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(_AngOfMajorAxis, CoordConverter.UCSZAxis(), _centerPt));
                line.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = line.StartPoint - line.EndPoint;
                _majorVec *= _radiusOrDiameter;

                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueEllipseIsocircleStartParamEndAng()
            {
                switch (_defSnapIsoPair)
                {
                    case 0:
                        _AngOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case 1:
                        _AngOfMajorAxis = 0;
                        break;
                    case 2:
                        _AngOfMajorAxis = Math.PI / 3;
                        break;
                }

                _ratio = Math.Tan(Math.PI / 6);
                _secondPt = new Point3d(_centerPt.X + Math.Sqrt((double)3 / 2), _centerPt.Y, _centerPt.Z);
                Line line = new Line(_centerPt, _secondPt);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(_AngOfMajorAxis, CoordConverter.UCSZAxis(), _centerPt));
                line.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = line.StartPoint - line.EndPoint;
                _majorVec *= _radiusOrDiameter;

                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));

                return Calculate();
            }

            public ExpectedResult CalculateValueEllipseIsocircleStartParamIncludedAng()
            {
                switch (_defSnapIsoPair)
                {
                    case 0:
                        _AngOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case 1:
                        _AngOfMajorAxis = 0;
                        break;
                    case 2:
                        _AngOfMajorAxis = Math.PI / 3;
                        break;
                }

                _ratio = Math.Tan(Math.PI / 6);
                _secondPt = new Point3d(_centerPt.X + Math.Sqrt((double)3 / 2), _centerPt.Y, _centerPt.Z);
                Line line = new Line(_centerPt, _secondPt);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(_AngOfMajorAxis, CoordConverter.UCSZAxis(), _centerPt));
                line.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = line.StartPoint - line.EndPoint;
                _majorVec *= _radiusOrDiameter;

                _endAngOrParamOrIncludedAng = _endAngOrParamOrIncludedAng + _startAngOrParam;
                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueEllipseIsocircleDiameterStartEndAng()
            {
                return CalculateValueEllipseIsocircleDiameter();
            }

            public ExpectedResult CalculateValueEllipseIsocircleDiameterStartAngEndParam()
            {
                switch (_defSnapIsoPair)
                {
                    case 0:
                        _AngOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case 1:
                        _AngOfMajorAxis = 0;
                        break;
                    case 2:
                        _AngOfMajorAxis = Math.PI / 3;
                        break;
                }

                _ratio = Math.Tan(Math.PI / 6);
                _secondPt = new Point3d(_centerPt.X + Math.Sqrt((double)3 / 2), _centerPt.Y, _centerPt.Z);
                Line line = new Line(_centerPt, _secondPt);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(_AngOfMajorAxis, CoordConverter.UCSZAxis(), _centerPt));
                line.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = line.StartPoint - line.EndPoint;
                _majorVec = _majorVec * _radiusOrDiameter / 2;

                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueEllipseIsocircleDiameterStartAngIncludedAng()
            {
                _endAngOrParamOrIncludedAng = _endAngOrParamOrIncludedAng + _startAngOrParam;
                return CalculateValueEllipseIsocircleDiameter();
            }

            public ExpectedResult CalculateValueEllipseIsocircleDiameterStartEndParam()
            {
                switch (_defSnapIsoPair)
                {
                    case 0:
                        _AngOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case 1:
                        _AngOfMajorAxis = 0;
                        break;
                    case 2:
                        _AngOfMajorAxis = Math.PI / 3;
                        break;
                }

                _ratio = Math.Tan(Math.PI / 6);
                _secondPt = new Point3d(_centerPt.X + Math.Sqrt((double)3 / 2), _centerPt.Y, _centerPt.Z);
                Line line = new Line(_centerPt, _secondPt);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(_AngOfMajorAxis, CoordConverter.UCSZAxis(), _centerPt));
                line.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = line.StartPoint - line.EndPoint;
                _majorVec = _majorVec * _radiusOrDiameter / 2;

                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public ExpectedResult CalculateValueEllipseIsocircleDiameterStartParamEndAng()
            {
                switch (_defSnapIsoPair)
                {
                    case 0:
                        _AngOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case 1:
                        _AngOfMajorAxis = 0;
                        break;
                    case 2:
                        _AngOfMajorAxis = Math.PI / 3;
                        break;
                }

                _ratio = Math.Tan(Math.PI / 6);
                _secondPt = new Point3d(_centerPt.X + Math.Sqrt((double)3 / 2), _centerPt.Y, _centerPt.Z);
                Line line = new Line(_centerPt, _secondPt);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(_AngOfMajorAxis, CoordConverter.UCSZAxis(), _centerPt));
                line.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = line.StartPoint - line.EndPoint;
                _majorVec = _majorVec * _radiusOrDiameter / 2;

                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));

                return Calculate();
            }

            public ExpectedResult CalculateValueEllipseIsocircleDiameterStartParamIncludedAng()
            {
                switch (_defSnapIsoPair)
                {
                    case 0:
                        _AngOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case 1:
                        _AngOfMajorAxis = 0;
                        break;
                    case 2:
                        _AngOfMajorAxis = Math.PI / 3;
                        break;
                }

                _ratio = Math.Tan(Math.PI / 6);
                _secondPt = new Point3d(_centerPt.X + Math.Sqrt((double)3 / 2), _centerPt.Y, _centerPt.Z);
                Line line = new Line(_centerPt, _secondPt);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(_AngOfMajorAxis, CoordConverter.UCSZAxis(), _centerPt));
                line.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = line.StartPoint - line.EndPoint;
                _majorVec = _majorVec * _radiusOrDiameter / 2;

                _endAngOrParamOrIncludedAng = _endAngOrParamOrIncludedAng + _startAngOrParam;
                _startAngOrParam = Math.Atan2(Math.Sin(_startAngOrParam) * _ratio, Math.Cos(_startAngOrParam));
                _endAngOrParamOrIncludedAng = Math.Atan2(Math.Sin(_endAngOrParamOrIncludedAng) * _ratio, Math.Cos(_endAngOrParamOrIncludedAng));

                return Calculate();
            }

            public PellipseExpectedResult CalculateValuePellipseAxisEnd()
            {
                double curDis = (_firstPt - _secondPt).Length / 2;
                double majorAxis = Math.Max(_distanceOrRotation, curDis);
                double minorAxis = Math.Min(_distanceOrRotation, curDis);

                _majorVec = (_firstPt - _secondPt) / 2;
                _ratio = minorAxis / majorAxis;
                if (_distanceOrRotation == majorAxis)
                {
                    _majorVec = _majorVec.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                    _majorVec /= _ratio;
                }

                _centerPt = Point3dHelper.CenterPoint(_firstPt, _secondPt);
                return CalculatePellipse();
            }

            public PellipseExpectedResult CalculateValuePellipseAxisEndRotation()
            {
                _majorVec = (_firstPt - _secondPt) / 2;

                if (Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180)) > 0.0101)
                {
                    _ratio = Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180));
                }

                _centerPt = Point3dHelper.CenterPoint(_firstPt, _secondPt);
                return CalculatePellipse();
            }

            public PellipseExpectedResult CalculateValuePellipseCenter()
            {
                double curDis = (_secondPt - _centerPt).Length;
                double majorAxis = Math.Max(_distanceOrRotation, curDis);
                double minorAxis = Math.Min(_distanceOrRotation, curDis);

                _majorVec = _secondPt - _centerPt;
                _ratio = minorAxis / majorAxis;
                if (_distanceOrRotation == majorAxis)
                {
                    _majorVec = _majorVec.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                    _majorVec /= _ratio;
                }

                return CalculatePellipse();
            }

            public PellipseExpectedResult CalculateValuePellipseCenterRotation()
            {
                _majorVec = _secondPt - _centerPt;

                if (Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180)) > 0.0101)
                {
                    _ratio = Math.Abs(Math.Cos(_distanceOrRotation * Math.PI / 180));
                }

                return CalculatePellipse();
            }

            public PellipseExpectedResult CalculateValuePellipseIsocircle()
            {
                switch (_defSnapIsoPair)
                {
                    case 0:
                        _AngOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case 1:
                        _AngOfMajorAxis = 0;
                        break;
                    case 2:
                        _AngOfMajorAxis = Math.PI / 3;
                        break;
                }

                _ratio = Math.Tan(Math.PI / 6);
                _secondPt = new Point3d(_centerPt.X + Math.Sqrt((double)3 / 2), _centerPt.Y, _centerPt.Z);
                Line line = new Line(_centerPt, _secondPt);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(_AngOfMajorAxis, CoordConverter.UCSZAxis(), _centerPt));
                line.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = line.StartPoint - line.EndPoint;
                _majorVec *= _radiusOrDiameter;

                return CalculatePellipse();
            }

            public PellipseExpectedResult CalculateValuePellipseIsocircleDiameter()
            {
                switch (_defSnapIsoPair)
                {
                    case 0:
                        _AngOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case 1:
                        _AngOfMajorAxis = 0;
                        break;
                    case 2:
                        _AngOfMajorAxis = Math.PI / 3;
                        break;
                }

                _ratio = Math.Tan(Math.PI / 6);
                _secondPt = new Point3d(_centerPt.X + Math.Sqrt((double)3 / 2), _centerPt.Y, _centerPt.Z);
                Line line = new Line(_centerPt, _secondPt);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(_AngOfMajorAxis, CoordConverter.UCSZAxis(), _centerPt));
                line.TransformBy(CoordConverter.WcsToUcs());
                _majorVec = line.StartPoint - line.EndPoint;
                _majorVec = _majorVec * _radiusOrDiameter / 2;

                return CalculatePellipse();
            }
        }

        private void VerifyEllipse(ObjectIdCollection actualIds, ExpectedResult expected)
        {
            LastEllipseId = actualIds[0];
            using (var tr = Util.StartTransaction())
            using (var ellipse = tr.GetObject<Ellipse>(actualIds[0], OpenMode.ForRead))
            {
                ellipse.Center.TransformBy(CoordConverter.WcsToUcs()).Is(expected.CenterPt);

                var ellipseMajorRadius = ellipse.MajorRadius.ToTestVal();
                ellipseMajorRadius.Is(expected.MajorRadius);

                var ellipseMinorRadius = ellipse.MinorRadius.ToTestVal();
                ellipseMinorRadius.Is(expected.MinorRadius);

                Point3d ellipseMajorAxis = new Point3d(ellipse.MajorAxis.X, ellipse.MajorAxis.Y, ellipse.MajorAxis.Z);
                ellipseMajorAxis = ellipseMajorAxis.TransformBy(CoordConverter.WcsToUcs());
                ellipseMajorAxis = Point3dHelper.RoundCoordOfPoint(ref ellipseMajorAxis);
                Vector3d temp = new Vector3d(ellipseMajorAxis.X, ellipseMajorAxis.Y, ellipseMajorAxis.Z);
                temp.Is(expected.MajorAxisVec);
            }
        }

        private void VerifyEllipseArc(ObjectIdCollection actualIds, ExpectedResult expected)
        {
            LastEllipseId = actualIds[0];
            using (var tr = Util.StartTransaction())
            using (var ellipse = tr.GetObject<Ellipse>(actualIds[0], OpenMode.ForRead))
            {
                ellipse.Center.TransformBy(CoordConverter.WcsToUcs()).Is(expected.CenterPt);

                var ellipseMajorRadius = ellipse.MajorRadius.ToTestVal();
                ellipseMajorRadius.Is(expected.MajorRadius);

                var ellipseMinorRadius = ellipse.MinorRadius.ToTestVal();
                ellipseMinorRadius.Is(expected.MinorRadius);

                Point3d ellipseMajorAxis = new Point3d(ellipse.MajorAxis.X, ellipse.MajorAxis.Y, ellipse.MajorAxis.Z);
                ellipseMajorAxis = ellipseMajorAxis.TransformBy(CoordConverter.WcsToUcs());
                ellipseMajorAxis = Point3dHelper.RoundCoordOfPoint(ref ellipseMajorAxis);
                Vector3d temp = new Vector3d(ellipseMajorAxis.X, ellipseMajorAxis.Y, ellipseMajorAxis.Z);
                temp.Is(expected.MajorAxisVec);

                (ellipse.EndAngle - ellipse.StartAngle).ToTestVal().Is(expected.EndAngleToStartAngle);
            }
        }

        private void VerifyPellipse(ObjectIdCollection actualIds, PellipseExpectedResult expected)
        {
            LastEllipseId = actualIds[0];
            using (var tr = Util.StartTransaction())
            using (var ellipse = tr.GetObject<Polyline2d>(actualIds[0], OpenMode.ForRead))
            {
                var startPoint = ellipse.StartPoint;
                startPoint.Is(expected.StartPoint);
                var endPoint = ellipse.EndPoint;
                endPoint.Is(expected.EndPoint);
                var area = ellipse.Area.ToTestVal();
                area.Is(expected.Area);
                var length = ellipse.Length.ToTestVal();
                length.Is(expected.Length);
                var elevation = ellipse.Elevation.ToTestVal();
                elevation.Is(expected.Elevation);
            }
        }
    }
}
