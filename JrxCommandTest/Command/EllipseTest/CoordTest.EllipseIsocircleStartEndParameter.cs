﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleStartEndParamCoordTest1", CommandFlags.Modal)]
        public void EllipseIsocircleStartEndParamCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                double radius = 30;
                double startParam = 30;
                double endParam = 40;

                DoTestEllipseIsocircleStartEndParam(pt1, radius, startParam, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleStartEndParamCoordTest2", CommandFlags.Modal)]
        public void EllipseIsocircleStartEndParamCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(30, 40, 50);
                double radius = 34;
                double startParam = 210;
                double endParam = 30;

                DoTestEllipseIsocircleStartEndParam(pt1, radius, startParam, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleStartEndParamCoordTest3", CommandFlags.Modal)]
        public void EllipseIsocircleStartEndParamCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 50, 30);
                double radius = 20;
                double startParam = 150;
                double endParam = 200;

                DoTestEllipseIsocircleStartEndParam(pt1, radius, startParam, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleStartEndParamCoordTest4", CommandFlags.Modal)]
        public void EllipseIsocircleStartEndParamCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                double radius = 30;
                double startParam = 30;
                double endParam = 40;

                DoTestEllipseIsocircleStartEndParam(pt1, radius, startParam, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleStartEndParamCoordTest5", CommandFlags.Modal)]
        public void EllipseIsocircleStartEndParamCoordTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(30, 40, 50);
                double radius = 34;
                double startParam = 210;
                double endParam = 30;

                DoTestEllipseIsocircleStartEndParam(pt1, radius, startParam, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleStartEndParamCoordTest6", CommandFlags.Modal)]
        public void EllipseIsocircleStartEndParamCoordTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 50, 30);
                double radius = 20;
                double startParam = 150;
                double endParam = 200;

                DoTestEllipseIsocircleStartEndParam(pt1, radius, startParam, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleStartEndParamCoordTest7", CommandFlags.Modal)]
        public void EllipseIsocircleStartEndParamCoordTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                double radius = 30;
                double startParam = 30;
                double endParam = 40;

                DoTestEllipseIsocircleStartEndParam(pt1, radius, startParam, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleStartEndParamCoordTest8", CommandFlags.Modal)]
        public void EllipseIsocircleStartEndParamCoordTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(30, 40, 50);
                double radius = 34;
                double startParam = 210;
                double endParam = 30;

                DoTestEllipseIsocircleStartEndParam(pt1, radius, startParam, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleStartEndParamCoordTest9", CommandFlags.Modal)]
        public void EllipseIsocircleStartEndParamCoordTest9()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 50, 30);
                double radius = 20;
                double startParam = 150;
                double endParam = 200;

                DoTestEllipseIsocircleStartEndParam(pt1, radius, startParam, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleStartEndParamCoordTest10", CommandFlags.Modal)]
        public void EllipseIsocircleStartEndParamCoordTest10()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                double radius = 30;
                double startParam = 30;
                double endParam = 40;

                DoTestEllipseIsocircleStartEndParam(pt1, radius, startParam, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleStartEndParamCoordTest11", CommandFlags.Modal)]
        public void EllipseIsocircleStartEndParamCoordTest11()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(30, 40, 50);
                double radius = 34;
                double startParam = 210;
                double endParam = 30;

                DoTestEllipseIsocircleStartEndParam(pt1, radius, startParam, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseIsocircleStartEndParamCoordTest12", CommandFlags.Modal)]
        public void EllipseIsocircleStartEndParamCoordTest12()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 50, 30);
                double radius = 20;
                double startParam = 150;
                double endParam = 200;

                DoTestEllipseIsocircleStartEndParam(pt1, radius, startParam, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestEllipseIsocircleStartEndParam(Point3d centerPt, double radius, double startParam, double endParam)
        {
            ExpectedResult expectedResult = new EllipseCalculator(centerPt, radius, Math.PI * startParam / 180, Math.PI * endParam / 180)
                    .CalculateValueEllipseIsocircleStartEndParam();
            var objIds = DoTestEllipseArcEllipseIsocircleStartEndParam(centerPt, radius, Math.PI * startParam / 180, Math.PI * endParam / 180);
            VerifyEllipseArc(objIds, expectedResult);
        }

        private ObjectIdCollection DoTestEllipseArcEllipseIsocircleStartEndParam(Point3d centerPt, double radius, double startParam, double endParam)
        {
            //// Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", radius));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + startParam, startParam));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + endParam, endParam));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedEllipseIds;
        }
    }
}
