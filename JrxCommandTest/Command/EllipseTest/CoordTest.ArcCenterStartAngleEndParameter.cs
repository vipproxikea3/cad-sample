﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngEndParamCoordTest1", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngEndParamCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;
                double startAng = 30;
                double endParam = 40;

                DoTestArcCenterStartAngEndParam(pt1, pt2, distance, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngEndParamCoordTest2", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngEndParamCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;
                double startAng = 210;
                double endParam = 30;

                DoTestArcCenterStartAngEndParam(pt1, pt2, distance, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngEndParamCoordTest3", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngEndParamCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;
                double startAng = 150;
                double endParam = 200;

                DoTestArcCenterStartAngEndParam(pt1, pt2, distance, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngEndParamCoordTest4", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngEndParamCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;
                double startAng = 30;
                double endParam = 40;

                DoTestArcCenterStartAngEndParam(pt1, pt2, distance, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngEndParamCoordTest5", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngEndParamCoordTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;
                double startAng = 210;
                double endParam = 30;

                DoTestArcCenterStartAngEndParam(pt1, pt2, distance, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngEndParamCoordTest6", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngEndParamCoordTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;
                double startAng = 150;
                double endParam = 200;

                DoTestArcCenterStartAngEndParam(pt1, pt2, distance, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngEndParamCoordTest7", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngEndParamCoordTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;
                double startAng = 30;
                double endParam = 40;

                DoTestArcCenterStartAngEndParam(pt1, pt2, distance, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngEndParamCoordTest8", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngEndParamCoordTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;
                double startAng = 210;
                double endParam = 30;

                DoTestArcCenterStartAngEndParam(pt1, pt2, distance, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngEndParamCoordTest9", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngEndParamCoordTest9()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;
                double startAng = 150;
                double endParam = 200;

                DoTestArcCenterStartAngEndParam(pt1, pt2, distance, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngEndParamCoordTest10", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngEndParamCoordTest10()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;
                double startAng = 30;
                double endParam = 40;

                DoTestArcCenterStartAngEndParam(pt1, pt2, distance, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngEndParamCoordTest11", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngEndParamCoordTest11()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;
                double startAng = 210;
                double endParam = 30;

                DoTestArcCenterStartAngEndParam(pt1, pt2, distance, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngEndParamCoordTest12", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngEndParamCoordTest12()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;
                double startAng = 150;
                double endParam = 200;

                DoTestArcCenterStartAngEndParam(pt1, pt2, distance, startAng, endParam);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestArcCenterStartAngEndParam(Point3d centerPt, Point3d endPt, double distance, double startAng, double endParam)
        {
            ExpectedResult expectedResult = new EllipseCalculator(centerPt, endPt, distance, Math.PI * startAng / 180,
                Math.PI * endParam / 180, true).CalculateValueArcCenterStartAngEndParam();

            var objIds = DoTestEllipseArcCenterStartAngEndParam(centerPt, endPt, distance, Math.PI * startAng / 180, Math.PI * endParam / 180);
            VerifyEllipseArc(objIds, expectedResult);
        }

        private ObjectIdCollection DoTestEllipseArcCenterStartAngEndParam(Point3d firstPt, Point3d secondPt, double distance, double startAng, double endParam)
        {
            //// Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", firstPt));

            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", secondPt));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", distance));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + startAng, startAng));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "P", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + endParam, endParam));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedEllipseIds;
        }
    }
}
