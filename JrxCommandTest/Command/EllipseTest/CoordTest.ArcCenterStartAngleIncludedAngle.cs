﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngIncludedAngCoordTest1", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngIncludedAngCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;
                double startAng = 30;
                double includedAng = 40;

                DoTestArcCenterStartAngIncludedAng(pt1, pt2, distance, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngIncludedAngCoordTest2", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngIncludedAngCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;
                double startAng = 210;
                double includedAng = 30;

                DoTestArcCenterStartAngIncludedAng(pt1, pt2, distance, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngIncludedAngCoordTest3", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngIncludedAngCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;
                double startAng = 150;
                double includedAng = 200;

                DoTestArcCenterStartAngIncludedAng(pt1, pt2, distance, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngIncludedAngCoordTest4", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngIncludedAngCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;
                double startAng = 30;
                double includedAng = 40;

                DoTestArcCenterStartAngIncludedAng(pt1, pt2, distance, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngIncludedAngCoordTest5", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngIncludedAngCoordTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;
                double startAng = 210;
                double includedAng = 30;

                DoTestArcCenterStartAngIncludedAng(pt1, pt2, distance, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngIncludedAngCoordTest6", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngIncludedAngCoordTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;
                double startAng = 150;
                double includedAng = 200;

                DoTestArcCenterStartAngIncludedAng(pt1, pt2, distance, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngIncludedAngCoordTest7", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngIncludedAngCoordTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;
                double startAng = 30;
                double includedAng = 40;

                DoTestArcCenterStartAngIncludedAng(pt1, pt2, distance, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngIncludedAngCoordTest8", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngIncludedAngCoordTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;
                double startAng = 210;
                double includedAng = 30;

                DoTestArcCenterStartAngIncludedAng(pt1, pt2, distance, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngIncludedAngCoordTest9", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngIncludedAngCoordTest9()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;
                double startAng = 150;
                double includedAng = 200;

                DoTestArcCenterStartAngIncludedAng(pt1, pt2, distance, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngIncludedAngCoordTest10", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngIncludedAngCoordTest10()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double distance = 30;
                double startAng = 30;
                double includedAng = 40;

                DoTestArcCenterStartAngIncludedAng(pt1, pt2, distance, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngIncludedAngCoordTest11", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngIncludedAngCoordTest11()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double distance = 34;
                double startAng = 210;
                double includedAng = 30;

                DoTestArcCenterStartAngIncludedAng(pt1, pt2, distance, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcCenterStartAngIncludedAngCoordTest12", CommandFlags.Modal)]
        public void EllipseArcCenterStartAngIncludedAngCoordTest12()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double distance = 20;
                double startAng = 150;
                double includedAng = 200;

                DoTestArcCenterStartAngIncludedAng(pt1, pt2, distance, startAng, includedAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestArcCenterStartAngIncludedAng(Point3d centerPt, Point3d endPt, double distance, double startAng, double includedAng)
        {
            ExpectedResult expectedResult = new EllipseCalculator(centerPt, endPt, distance, Math.PI * startAng / 180,
                Math.PI * includedAng / 180, true).CalculateValueArcCenterStartAngIncludedAng();

            var objIds = DoTestEllipseArcCenterStartAngIncludedAng(centerPt, endPt, distance, Math.PI * startAng / 180, Math.PI * includedAng / 180);
            VerifyEllipseArc(objIds, expectedResult);
        }

        private ObjectIdCollection DoTestEllipseArcCenterStartAngIncludedAng(Point3d firstPt, Point3d secondPt, double distance, double startAng, double includedAng)
        {
            //// Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "C", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", firstPt));

            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", secondPt));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                .Callback((DistanceDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", distance));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + startAng, startAng));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "I", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + includedAng, includedAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedEllipseIds;
        }
    }
}
