﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartEndAngCoordTest1", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartEndAngCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double rotation = 30;
                double startAng = 30;
                double endAng = 40;

                DoTestArcAxisEndRotationStartEndAng(pt1, pt2, rotation, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartEndAngCoordTest2", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartEndAngCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double rotation = 34;
                double startAng = 210;
                double endAng = 30;

                DoTestArcAxisEndRotationStartEndAng(pt1, pt2, rotation, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartEndAngCoordTest3", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartEndAngCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double rotation = 20;
                double startAng = 150;
                double endAng = 200;

                DoTestArcAxisEndRotationStartEndAng(pt1, pt2, rotation, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartEndAngCoordTest4", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartEndAngCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double rotation = 30;
                double startAng = 30;
                double endAng = 40;

                DoTestArcAxisEndRotationStartEndAng(pt1, pt2, rotation, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartEndAngCoordTest5", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartEndAngCoordTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double rotation = 34;
                double startAng = 210;
                double endAng = 30;

                DoTestArcAxisEndRotationStartEndAng(pt1, pt2, rotation, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartEndAngCoordTest6", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartEndAngCoordTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double rotation = 20;
                double startAng = 150;
                double endAng = 200;

                DoTestArcAxisEndRotationStartEndAng(pt1, pt2, rotation, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartEndAngCoordTest7", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartEndAngCoordTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double rotation = 30;
                double startAng = 30;
                double endAng = 40;

                DoTestArcAxisEndRotationStartEndAng(pt1, pt2, rotation, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartEndAngCoordTest8", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartEndAngCoordTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double rotation = 34;
                double startAng = 210;
                double endAng = 30;

                DoTestArcAxisEndRotationStartEndAng(pt1, pt2, rotation, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartEndAngCoordTest9", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartEndAngCoordTest9()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double rotation = 20;
                double startAng = 150;
                double endAng = 200;

                DoTestArcAxisEndRotationStartEndAng(pt1, pt2, rotation, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartEndAngCoordTest10", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartEndAngCoordTest10()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                Point3d pt2 = new Point3d(30, 40, pt1.Z);
                double rotation = 30;
                double startAng = 30;
                double endAng = 40;

                DoTestArcAxisEndRotationStartEndAng(pt1, pt2, rotation, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartEndAngCoordTest11", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartEndAngCoordTest11()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(30, 40, 50);
                Point3d pt2 = new Point3d(92, 38, pt1.Z);
                double rotation = 34;
                double startAng = 210;
                double endAng = 30;

                DoTestArcAxisEndRotationStartEndAng(pt1, pt2, rotation, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxEllipseArcAxisEndRotationStartEndAngCoordTest12", CommandFlags.Modal)]
        public void EllipseArcAxisEndRotationStartEndAngCoordTest12()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 50, 30);
                Point3d pt2 = new Point3d(40, 70, pt1.Z);
                double rotation = 20;
                double startAng = 150;
                double endAng = 200;

                DoTestArcAxisEndRotationStartEndAng(pt1, pt2, rotation, startAng, endAng);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestArcAxisEndRotationStartEndAng(Point3d firstPt, Point3d secondPt, double rotation, double startAng, double endAng)
        {
            ExpectedResult expectedResult = new EllipseCalculator(firstPt, secondPt, rotation, Math.PI * startAng / 180,
                Math.PI * endAng / 180).CalculateValueArcAxisEndRotationStartEndAng();

            var objIds = DoTestEllipseArcAxisEndRotationStartEndAng(firstPt, secondPt, rotation, Math.PI * startAng / 180, Math.PI * endAng / 180);
            VerifyEllipseArc(objIds, expectedResult);
        }

        private ObjectIdCollection DoTestEllipseArcAxisEndRotationStartEndAng(Point3d firstPt, Point3d secondPt, double rotation,
            double startAng, double endAng)
        {
            //// Create Mock
            var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
            var sequence = new MockSequence();

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "A", Point3d.Origin));

            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", firstPt));

            secondPt = secondPt.TransformBy(CoordConverter.UcsToWcs());
            userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                .Callback((PointDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", secondPt));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.Keyword, "R", 0));
            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                    .Callback((AngleDrawJigN.JigActions options) =>
                    {
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", rotation));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + startAng, startAng));

            userInputMock.InSequence(sequence).Setup(x => x.AcquireAngle(It.IsAny<EllipseCmd.AngleOpts>()))
                .Callback((AngleDrawJigN.JigActions options) =>
                {
                    options.OnUpdate();
                })
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "" + endAng, endAng));

            // Setup AppendedEllipses
            var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
            userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                .Callback((ObjectId id) => appendedEllipseIds.Add(id));

            // Execute Ellipse
            var cmd = new EllipseCmd(userInputMock.Object);
            cmd.OnCommand();
            return appendedEllipseIds;
        }
    }
}
