﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleDiameterCoordTest1", CommandFlags.Modal)]
        public void PellipseIsocircleDiameterCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                double diameter = 30;

                DoTestPellipseIsocircleDiameter(pt1, diameter);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleDiameterCoordTest2", CommandFlags.Modal)]
        public void PellipseIsocircleDiameterCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(30, 40, 50);
                double diameter = 34;

                DoTestPellipseIsocircleDiameter(pt1, diameter);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleDiameterCoordTest3", CommandFlags.Modal)]
        public void PellipseIsocircleDiameterCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 50, 30);
                double diameter = 20;

                DoTestPellipseIsocircleDiameter(pt1, diameter);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleDiameterCoordTest4", CommandFlags.Modal)]
        public void PellipseIsocircleDiameterCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                double diameter = 30;

                DoTestPellipseIsocircleDiameter(pt1, diameter);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleDiameterCoordTest5", CommandFlags.Modal)]
        public void PellipseIsocircleDiameterCoordTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(30, 40, 50);
                double diameter = 34;

                DoTestPellipseIsocircleDiameter(pt1, diameter);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleDiameterCoordTest6", CommandFlags.Modal)]
        public void PellipseIsocircleDiameterCoordTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 50, 30);
                double diameter = 20;

                DoTestPellipseIsocircleDiameter(pt1, diameter);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleDiameterCoordTest7", CommandFlags.Modal)]
        public void PellipseIsocircleDiameterCoordTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                double diameter = 30;

                DoTestPellipseIsocircleDiameter(pt1, diameter);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleDiameterCoordTest8", CommandFlags.Modal)]
        public void PellipseIsocircleDiameterCoordTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(30, 40, 50);
                double diameter = 34;

                DoTestPellipseIsocircleDiameter(pt1, diameter);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleDiameterCoordTest9", CommandFlags.Modal)]
        public void PellipseIsocircleDiameterCoordTest9()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 50, 30);
                double diameter = 20;

                DoTestPellipseIsocircleDiameter(pt1, diameter);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleDiameterCoordTest10", CommandFlags.Modal)]
        public void PellipseIsocircleDiameterCoordTest10()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                double diameter = 30;

                DoTestPellipseIsocircleDiameter(pt1, diameter);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleDiameterCoordTest11", CommandFlags.Modal)]
        public void PellipseIsocircleDiameterCoordTest11()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(30, 40, 50);
                double diameter = 34;

                DoTestPellipseIsocircleDiameter(pt1, diameter);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleDiameterCoordTest12", CommandFlags.Modal)]
        public void PellipseIsocircleDiameterCoordTest12()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 50, 30);
                double diameter = 20;

                DoTestPellipseIsocircleDiameter(pt1, diameter);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestPellipseIsocircleDiameter(Point3d centerPt, double diameter)
        {
            PellipseExpectedResult expectedResult = new EllipseCalculator(centerPt, diameter)
                    .CalculateValuePellipseIsocircleDiameter();
            var objIds = DoTestEllipsePellipseIsocircleDiameter(centerPt, diameter);
            VerifyPellipse(objIds, expectedResult);
        }

        private ObjectIdCollection DoTestEllipsePellipseIsocircleDiameter(Point3d centerPt, double diameter)
        {
            using (new SystemVariable.Temp("PELLIPSE", 1))
            {
                //// Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "D", Point3d.Origin));

                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", diameter));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();
                return appendedEllipseIds;
            }
        }
    }
}
