﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using System.Reflection;
using JrxCad.Helpers;
using System;

namespace JrxCadTest.Command.EllipseTest
{
    public class MessageTest
    {
        public static void EllipseMessageTest1(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptDistanceOptions optDistance, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageDistance(optDistance, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest2(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optRotation, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageRotation(optRotation, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest3(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptDistanceOptions optDistance, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageDistance(optDistance, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest4(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optRotation, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageRotation(optRotation, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest5(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optEndAng, JigPromptDistanceOptions optDistance, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageDistance(optDistance, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageStartAngle(optStartAng, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageEndAngle(optEndAng, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest6(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optEndParameter, JigPromptDistanceOptions optDistance, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageDistance(optDistance, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageStartAngle(optStartAng, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageEndParameter(optEndParameter, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest7(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optIncludedAngle, JigPromptDistanceOptions optDistance, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageDistance(optDistance, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageStartAngle(optStartAng, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageIncludedAngle(optIncludedAngle, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest8(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optEndParameter, JigPromptDistanceOptions optDistance, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageDistance(optDistance, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageStartParameter(optStartParameter, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageEndParameter(optEndParameter, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest9(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optEndAngle, JigPromptDistanceOptions optDistance, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageDistance(optDistance, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageStartParameter(optStartParameter, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageEndAngle(optEndAngle, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest10(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optIncludedAngle, JigPromptDistanceOptions optDistance, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageDistance(optDistance, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageStartParameter(optStartParameter, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageIncludedAngle(optIncludedAngle, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest11(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optEndAng, JigPromptAngleOptions optRotation, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageRotation(optRotation, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageStartAngle(optStartAng, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageEndAngle(optEndAng, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest12(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optEndParameter, JigPromptAngleOptions optRotation, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageRotation(optRotation, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageStartAngle(optStartAng, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageEndParameter(optEndParameter, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest13(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optIncludedAngle, JigPromptAngleOptions optRotation, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageRotation(optRotation, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageStartAngle(optStartAng, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageIncludedAngle(optIncludedAngle, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest14(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optEndParameter, JigPromptAngleOptions optRotation, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageRotation(optRotation, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageStartParameter(optStartParameter, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageEndParameter(optEndParameter, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest15(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optEndAngle, JigPromptAngleOptions optRotation, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageRotation(optRotation, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageStartParameter(optStartParameter, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageEndAngle(optEndAngle, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest16(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optIncludedAngle, JigPromptAngleOptions optRotation, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcAxisEndPoint(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageRotation(optRotation, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageStartParameter(optStartParameter, Point3dHelper.CenterPoint(firstPt, secondPt));
            VerifyMessageIncludedAngle(optIncludedAngle, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest17(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optEndAng, JigPromptDistanceOptions optDistance, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageDistance(optDistance, centerPt);
            VerifyMessageStartAngle(optStartAng, centerPt);
            VerifyMessageEndAngle(optEndAng, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest18(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optEndParameter, JigPromptDistanceOptions optDistance, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageDistance(optDistance, centerPt);
            VerifyMessageStartAngle(optStartAng, centerPt);
            VerifyMessageEndParameter(optEndParameter, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest19(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optIncludedAngle, JigPromptDistanceOptions optDistance, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageDistance(optDistance, centerPt);
            VerifyMessageStartAngle(optStartAng, centerPt);
            VerifyMessageIncludedAngle(optIncludedAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest20(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optEndParameter, JigPromptDistanceOptions optDistance, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageDistance(optDistance, centerPt);
            VerifyMessageStartParameter(optStartParameter, centerPt);
            VerifyMessageEndParameter(optEndParameter, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest21(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optEndAngle, JigPromptDistanceOptions optDistance, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageDistance(optDistance, centerPt);
            VerifyMessageStartParameter(optStartParameter, centerPt);
            VerifyMessageEndAngle(optEndAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest22(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optIncludedAngle, JigPromptDistanceOptions optDistance, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageDistance(optDistance, centerPt);
            VerifyMessageStartParameter(optStartParameter, centerPt);
            VerifyMessageIncludedAngle(optIncludedAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest23(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optEndAng, JigPromptAngleOptions optRotation, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageRotation(optRotation, centerPt);
            VerifyMessageStartAngle(optStartAng, centerPt);
            VerifyMessageEndAngle(optEndAng, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest24(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optEndParameter, JigPromptAngleOptions optRotation, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageRotation(optRotation, centerPt);
            VerifyMessageStartAngle(optStartAng, centerPt);
            VerifyMessageEndParameter(optEndParameter, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest25(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optIncludedAngle, JigPromptAngleOptions optRotation, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageRotation(optRotation, centerPt);
            VerifyMessageStartAngle(optStartAng, centerPt);
            VerifyMessageIncludedAngle(optIncludedAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest26(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optEndParameter, JigPromptAngleOptions optRotation, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageRotation(optRotation, centerPt);
            VerifyMessageStartParameter(optStartParameter, centerPt);
            VerifyMessageEndParameter(optEndParameter, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest27(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optEndAngle, JigPromptAngleOptions optRotation, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageRotation(optRotation, centerPt);
            VerifyMessageStartParameter(optStartParameter, centerPt);
            VerifyMessageEndAngle(optEndAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest28(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optIncludedAngle, JigPromptAngleOptions optRotation, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageRotation(optRotation, centerPt);
            VerifyMessageStartParameter(optStartParameter, centerPt);
            VerifyMessageIncludedAngle(optIncludedAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest29(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optRadius, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageRadius(optRadius, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest30(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optDiameter, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageDiameter(optDiameter, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest31(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optRadius, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optEndAng, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageRadius(optRadius, centerPt);
            VerifyMessageStartAngle(optStartAng, centerPt);
            VerifyMessageEndAngle(optEndAng, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest32(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optRadius, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optEndParameter, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageRadius(optRadius, centerPt);
            VerifyMessageStartAngle(optStartAng, centerPt);
            VerifyMessageEndParameter(optEndParameter, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest33(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optRadius, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optIncludedAngle, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageRadius(optRadius, centerPt);
            VerifyMessageStartAngle(optStartAng, centerPt);
            VerifyMessageIncludedAngle(optIncludedAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest34(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optRadius, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optEndParameter, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageRadius(optRadius, centerPt);
            VerifyMessageStartParameter(optStartParameter, centerPt);
            VerifyMessageEndParameter(optEndParameter, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest35(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optRadius, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optEndAngle, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageRadius(optRadius, centerPt);
            VerifyMessageStartParameter(optStartParameter, centerPt);
            VerifyMessageEndAngle(optEndAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest36(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optRadius, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optIncludedAngle, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageRadius(optRadius, centerPt);
            VerifyMessageStartParameter(optStartParameter, centerPt);
            VerifyMessageIncludedAngle(optIncludedAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest37(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optDiameter, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optEndAng, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageDiameter(optDiameter, centerPt);
            VerifyMessageStartAngle(optStartAng, centerPt);
            VerifyMessageEndAngle(optEndAng, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest38(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optDiameter, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optEndParameter, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageDiameter(optDiameter, centerPt);
            VerifyMessageStartAngle(optStartAng, centerPt);
            VerifyMessageEndParameter(optEndParameter, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest39(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optDiameter, JigPromptAngleOptions optStartAng, JigPromptAngleOptions optIncludedAngle, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageDiameter(optDiameter, centerPt);
            VerifyMessageStartAngle(optStartAng, centerPt);
            VerifyMessageIncludedAngle(optIncludedAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest40(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optDiameter, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optEndParameter, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageDiameter(optDiameter, centerPt);
            VerifyMessageStartParameter(optStartParameter, centerPt);
            VerifyMessageEndParameter(optEndParameter, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest41(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optDiameter, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optEndAngle, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageDiameter(optDiameter, centerPt);
            VerifyMessageStartParameter(optStartParameter, centerPt);
            VerifyMessageEndAngle(optEndAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest42(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optDiameter, JigPromptAngleOptions optStartParameter, JigPromptAngleOptions optIncludedAngle, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageArcIsocircleAxisEndPoint(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageDiameter(optDiameter, centerPt);
            VerifyMessageStartParameter(optStartParameter, centerPt);
            VerifyMessageIncludedAngle(optIncludedAngle, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest43(EllipseCmd cmd, PromptPointOptions optFirst,
            JigPromptPointOptions optSecond, JigPromptDistanceOptions optDistance, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessagePellipse(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageDistance(optDistance, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }
        public static void EllipseMessageTest44(EllipseCmd cmd, PromptPointOptions optFirst,
             JigPromptPointOptions optSecond, JigPromptAngleOptions optRotation, Point3d firstPt, Point3d secondPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessagePellipse(optFirst);
            VerifyMessageOtherEndPoint(optSecond, firstPt);
            VerifyMessageRotation(optRotation, Point3dHelper.CenterPoint(firstPt, secondPt));
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest45(EllipseCmd cmd, PromptPointOptions optCenter,
            JigPromptPointOptions optSecond, JigPromptDistanceOptions optDistance, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageDistance(optDistance, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest46(EllipseCmd cmd, PromptPointOptions optCenter,
           JigPromptPointOptions optSecond, JigPromptAngleOptions optRotation, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessageCenterPoint(optCenter);
            VerifyMessageEndPoint(optSecond, centerPt);
            VerifyMessageRotation(optRotation, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void EllipseMessageTest47(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optRadius, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessagePellipseIsocircle(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageRadius(optRadius, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }
        public static void EllipseMessageTest48(EllipseCmd cmd, PromptPointOptions optFirst, PromptPointOptions optCenter, JigPromptDistanceOptions optRadius, Point3d centerPt)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            VerifyCommandMethod(cmd);
            VerifyMessagePellipseIsocircle(optFirst);
            VerifyMessageIsocircleCenterPoint(optCenter);
            VerifyMessageDiameter(optRadius, centerPt);
            Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
        }

        public static void VerifyCommandMethod(EllipseCmd cmd)
        {
#if DEBUG
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "ELL", CommandFlags.Modal | CommandFlags.NoNewStack, cmd.GetType());
#else
            VerifyEx.VerifyCommandMethod("JrxCommandTest", "SmxELLIPSE", CommandFlags.Modal | CommandFlags.NoNewStack, cmd.GetType());
#endif
        }

        public static void VerifyMessageAxisEndPoint(PromptPointOptions optFirst)
        {
            VerifyEx.VerifyPromptPointOptions(optFirst,
                new PromptPointOptions($"Specify axis endpoint of ellipse or ")
                {
                    Keywords =
                                {
                                    { "A", "Arc", "Arc" },
                                    { "C", "Center", "Center" },
                                },
                });
        }

        public static void VerifyMessageIsocircleAxisEndPoint(PromptPointOptions optFirst)
        {
            VerifyEx.VerifyPromptPointOptions(optFirst,
                new PromptPointOptions($"Specify axis endpoint of ellipse or ")
                {
                    Keywords =
                                {
                                    { "A", "Arc", "Arc" },
                                    { "C", "Center", "Center" },
                                    { "I", "Isocircle", "Isocircle" },
                                },
                });
        }

        public static void VerifyMessageCenterPoint(PromptPointOptions optCenter)
        {
            VerifyEx.VerifyPromptPointOptions(optCenter,
                new PromptPointOptions($"\nSpecify center of ellipse"));
        }

        public static void VerifyMessageIsocircleCenterPoint(PromptPointOptions optCenter)
        {
            VerifyEx.VerifyPromptPointOptions(optCenter,
                new PromptPointOptions($"\nSpecify center of isocircle"));
        }

        public static void VerifyMessageEndPoint(JigPromptPointOptions optSecond, Point3d firstPt)
        {
            VerifyEx.VerifyJigPromptPointOptions(optSecond,
                new JigPromptPointOptions($"\nSpecify endpoint of axis")
                {
                    DefaultValue = new Point3d(),
                    BasePoint = firstPt.TransformBy(CoordConverter.UcsToWcs()),
                    UseBasePoint = true,
                    Cursor = CursorType.RubberBand,
                    UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.UseBasePointElevation,
                });
        }

        public static void VerifyMessageOtherEndPoint(JigPromptPointOptions optSecond, Point3d firstPt)
        {
            VerifyEx.VerifyJigPromptPointOptions(optSecond,
                new JigPromptPointOptions($"\nSpecify other endpoint of axis")
                {
                    DefaultValue = new Point3d(),
                    UseBasePoint = true,
                    BasePoint = firstPt.TransformBy(CoordConverter.UcsToWcs()),
                    Cursor = CursorType.RubberBand,
                    UserInputControls = UserInputControls.UseBasePointElevation,
                });
        }

        public static void VerifyMessageDistance(JigPromptDistanceOptions optDistance, Point3d centerPt)
        {
            VerifyEx.VerifyJigPromptDistanceOptions(optDistance,
                new JigPromptDistanceOptions($"\nSpecify distance to other axis or ")
                {
                    DefaultValue = 1,
                    UseBasePoint = true,
                    BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs()),
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.UseBasePointElevation |
                                        UserInputControls.NoNegativeResponseAccepted |
                                        UserInputControls.NoZeroResponseAccepted,
                    Cursor = CursorType.RubberBand,
                    Keywords =
                                {
                                    { "R", "Rotation", "Rotation" },
                                },
                });
        }

        public static void VerifyMessageRotation(JigPromptAngleOptions optRotation, Point3d centerPt)
        {
            VerifyEx.VerifyJigPromptAngleOptions(optRotation,
                new JigPromptAngleOptions($"\nSpecify rotation around major axis")
                {
                    DefaultValue = 0,
                    Cursor = CursorType.RubberBand,
                    UseBasePoint = true,
                    BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs()),
                });
        }

        public static void VerifyMessageRadius(JigPromptDistanceOptions optRadius, Point3d centerPt)
        {
            VerifyEx.VerifyJigPromptDistanceOptions(optRadius,
                new JigPromptDistanceOptions($"\nSpecify radius of isocircle or ")
                {
                    DefaultValue = 1,
                    UseBasePoint = true,
                    BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs()),
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.UseBasePointElevation |
                                        UserInputControls.NoNegativeResponseAccepted |
                                        UserInputControls.NoZeroResponseAccepted,
                    Cursor = CursorType.RubberBand,
                    Keywords =
                                {
                                    { "D", "Diameter", "Diameter" },
                                },
                });
        }

        public static void VerifyMessageDiameter(JigPromptDistanceOptions optDiameter, Point3d centerPt)
        {
            VerifyEx.VerifyJigPromptDistanceOptions(optDiameter,
                new JigPromptDistanceOptions($"\nSpecify diameter of isocircle")
                {
                    DefaultValue = 1,
                    UseBasePoint = true,
                    BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs()),
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                                UserInputControls.UseBasePointElevation |
                                                UserInputControls.NoNegativeResponseAccepted |
                                                UserInputControls.NoZeroResponseAccepted,
                    Cursor = CursorType.RubberBand,
                });
        }

        public static void VerifyMessageArcAxisEndPoint(PromptPointOptions optFirst)
        {
            VerifyEx.VerifyPromptPointOptions(optFirst,
                    new PromptPointOptions($"\nSpecify axis endpoint of elliptical arc or ")
                    {
                        Keywords =
                                    {
                                        { "C", "Center", "Center" },
                                    },
                    });
        }

        public static void VerifyMessageArcCenterPoint(PromptPointOptions optCenter)
        {
            VerifyEx.VerifyPromptPointOptions(optCenter,
                        new PromptPointOptions($"\nSpecify center of elliptical arc"));
        }

        public static void VerifyMessageArcIsocircleAxisEndPoint(PromptPointOptions optFirst)
        {
            VerifyEx.VerifyPromptPointOptions(optFirst,
                    new PromptPointOptions($"\nSpecify axis endpoint of elliptical arc or ")
                    {
                        Keywords =
                                    {
                                        {"C", "Center", "Center"},
                                        { "I", "Isocircle", "Isocircle" },
                                    },
                    });
        }

        public static void VerifyMessageStartAngle(JigPromptAngleOptions optStartAng, Point3d centerPt)
        {
            VerifyEx.VerifyJigPromptAngleOptions(optStartAng,
                    new JigPromptAngleOptions($"\nSpecify start angle or ")
                    {
                        DefaultValue = 0,
                        UseBasePoint = true,
                        BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs()),
                        UserInputControls = UserInputControls.Accept3dCoordinates,
                        Cursor = CursorType.RubberBand,
                        Keywords =
                                    {
                                        { "P", "Parameter", "Parameter" },
                                    },
                    });
        }

        public static void VerifyMessageEndAngle(JigPromptAngleOptions optEndAng, Point3d centerPt)
        {
            VerifyEx.VerifyJigPromptAngleOptions(optEndAng,
                    new JigPromptAngleOptions($"\nSpecify end angle or ")
                    {
                        DefaultValue = Math.PI * 2,
                        UseBasePoint = true,
                        BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs()),
                        UserInputControls = UserInputControls.Accept3dCoordinates,
                        Cursor = CursorType.RubberBand,
                        Keywords =
                                    {
                                        { "P", "Parameter", "Parameter" },
                                        { "I", "Included angle", "Included angle" },
                                    },
                    });
        }

        public static void VerifyMessageStartParameter(JigPromptAngleOptions optStartParameter, Point3d centerPt)
        {
            VerifyEx.VerifyJigPromptAngleOptions(optStartParameter,
                    new JigPromptAngleOptions($"\nSpecify start parameter or ")
                    {
                        DefaultValue = 0,
                        UseBasePoint = true,
                        BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs()),
                        UserInputControls = UserInputControls.Accept3dCoordinates,
                        Cursor = CursorType.RubberBand,
                        Keywords =
                                    {
                                        { "A", "Angle", "Angle" },
                                    },
                    });
        }

        public static void VerifyMessageEndParameter(JigPromptAngleOptions optEndParameter, Point3d centerPt)
        {
            VerifyEx.VerifyJigPromptAngleOptions(optEndParameter,
                    new JigPromptAngleOptions($"\nSpecify end parameter or ")
                    {
                        DefaultValue = Math.PI * 2,
                        UseBasePoint = true,
                        BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs()),
                        UserInputControls = UserInputControls.Accept3dCoordinates,
                        Cursor = CursorType.RubberBand,
                        Keywords =
                                    {
                                        { "A", "Angle", "Angle" },
                                        { "I", "Included angle", "Included angle" },
                                    },
                    });
        }

        public static void VerifyMessageIncludedAngle(JigPromptAngleOptions optIncludedAngle, Point3d centerPt)
        {
            VerifyEx.VerifyJigPromptAngleOptions(optIncludedAngle,
                    new JigPromptAngleOptions($"\nSpecify included angle for arc")
                    {
                        DefaultValue = Math.PI,
                        UseBasePoint = true,
                        BasePoint = centerPt.TransformBy(CoordConverter.UcsToWcs()),
                        UserInputControls = UserInputControls.Accept3dCoordinates |
                                            UserInputControls.NullResponseAccepted,
                        Cursor = CursorType.RubberBand,
                    });
        }

        public static void VerifyMessagePellipse(PromptPointOptions optPellipseAxisEnd)
        {
            VerifyEx.VerifyPromptPointOptions(optPellipseAxisEnd,
                     new PromptPointOptions($"Specify axis endpoint of elliptical arc or ")
                     {
                         Keywords =
                                    {
                                        { "C", "Center", "Center" },
                                    },
                     });
        }

        public static void VerifyMessagePellipseIsocircle(PromptPointOptions optPellipseIsocircle)
        {
            VerifyEx.VerifyPromptPointOptions(optPellipseIsocircle,
                     new PromptPointOptions($"Specify axis endpoint of ellipse or ")
                     {
                         Keywords =
                                    {
                                        { "C", "Center", "Center" },
                                        { "I", "Isocircle", "Isocircle" },
                                    },
                     });
        }
    }
}
