﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using Moq;
using JrxCad.Command.EllipseCmd;
using JrxCad.Utility;
using JrxCadTest.Utility;
using Exception = System.Exception;
using System.Reflection;


namespace JrxCadTest.Command.EllipseTest
{
    public partial class CoordTest
    {
        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleCoordTest1", CommandFlags.Modal)]
        public void PellipseIsocircleCoordTest1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 20, 30);
                double radius = 30;

                DoTestPellipseIsocircle(pt1, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleCoordTest2", CommandFlags.Modal)]
        public void PellipseIsocircleCoordTest2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(30, 40, 50);
                double radius = 34;

                DoTestPellipseIsocircle(pt1, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleCoordTest3", CommandFlags.Modal)]
        public void PellipseIsocircleCoordTest3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //WCS
                Point3d pt1 = new Point3d(10, 50, 30);
                double radius = 20;

                DoTestPellipseIsocircle(pt1, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleCoordTest4", CommandFlags.Modal)]
        public void PellipseIsocircleCoordTest4()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 20, 30);
                double radius = 30;

                DoTestPellipseIsocircle(pt1, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleCoordTest5", CommandFlags.Modal)]
        public void PellipseIsocircleCoordTest5()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(30, 40, 50);
                double radius = 34;

                DoTestPellipseIsocircle(pt1, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleCoordTest6", CommandFlags.Modal)]
        public void PellipseIsocircleCoordTest6()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS1
                Point3d pt1 = new Point3d(10, 50, 30);
                double radius = 20;

                DoTestPellipseIsocircle(pt1, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleCoordTest7", CommandFlags.Modal)]
        public void PellipseIsocircleCoordTest7()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 20, 30);
                double radius = 30;

                DoTestPellipseIsocircle(pt1, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleCoordTest8", CommandFlags.Modal)]
        public void PellipseIsocircleCoordTest8()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(30, 40, 50);
                double radius = 34;

                DoTestPellipseIsocircle(pt1, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleCoordTest9", CommandFlags.Modal)]
        public void PellipseIsocircleCoordTest9()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS2
                Point3d pt1 = new Point3d(10, 50, 30);
                double radius = 20;

                DoTestPellipseIsocircle(pt1, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleCoordTest10", CommandFlags.Modal)]
        public void PellipseIsocircleCoordTest10()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 20, 30);
                double radius = 30;

                DoTestPellipseIsocircle(pt1, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleCoordTest11", CommandFlags.Modal)]
        public void PellipseIsocircleCoordTest11()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(30, 40, 50);
                double radius = 34;

                DoTestPellipseIsocircle(pt1, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        [CommandMethod("JrxCommandTest", "SmxPellipseIsocircleCoordTest12", CommandFlags.Modal)]
        public void PellipseIsocircleCoordTest12()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //UCS3
                Point3d pt1 = new Point3d(10, 50, 30);
                double radius = 20;

                DoTestPellipseIsocircle(pt1, radius);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTestPellipseIsocircle(Point3d centerPt, double radius)
        {
            PellipseExpectedResult expectedResult = new EllipseCalculator(centerPt, radius).CalculateValuePellipseIsocircle();
            var objIds = DoTestEllipsePellipseIsocircle(centerPt, radius);
            VerifyPellipse(objIds, expectedResult);
        }

        private ObjectIdCollection DoTestEllipsePellipseIsocircle(Point3d centerPt, double radius)
        {
            using (new SystemVariable.Temp("PELLIPSE", 1))
            {
                //// Create Mock
                var userInputMock = new Mock<EllipseCmd.IEllipseUserInput>(MockBehavior.Strict);
                var sequence = new MockSequence();

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.Keyword, "I", Point3d.Origin));

                userInputMock.InSequence(sequence).Setup(x => x.GetPoint(It.IsAny<PromptPointOptions>()))
                    .Returns(VerifyEx.CreatePromptPointResult(PromptStatus.OK, "", centerPt));

                userInputMock.InSequence(sequence).Setup(x => x.AcquireDistance(It.IsAny<EllipseCmd.DistanceOpts>()))
                    .Callback((DistanceDrawJigN.JigActions options) =>
                    {
                        options.OnUpdate();
                    })
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", radius));

                // Setup AppendedEllipses
                var appendedEllipseIds = new ObjectIdCollection();     // for Result Test
                userInputMock.Setup(x => x.AppendEllipseId(It.IsAny<ObjectId>()))
                    .Callback((ObjectId id) => appendedEllipseIds.Add(id));

                // Execute Ellipse
                var cmd = new EllipseCmd(userInputMock.Object);
                cmd.OnCommand();
                return appendedEllipseIds;
            }
        }
    }
}
