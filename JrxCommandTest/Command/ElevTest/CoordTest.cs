﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using JrxCad.Utility;
using JrxCad.Command.Elev;
using JrxCadTest.Utility;

namespace JrxCadTest.Command.ElevTest
{
    public class CoordTest
    {
        private class ActualResult
        {
            public readonly double Elevation;
            public readonly double Thickness;
            public ActualResult(double elevation, double thickness)
            {
                Elevation = elevation;
                Thickness = thickness;
            }
        }
        private class ExpectedResult
        {
            public readonly double Elevation;
            public readonly double Thickness;


            public ExpectedResult(double elevation, double thickness)
            {
                Elevation = elevation;
                Thickness = thickness;
            }
        }

        //Case 1: ELEVATION = 10 and THICKNESS = 0
        [CommandMethod("JrxCommandTest", "ELEVTESTCASE1", CommandFlags.Modal)]
        public void TestCase1()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                double elevation = 10;
                double thickness = 0;
                DoTest(elevation, thickness);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        //Case 2: ELEVATION = 20 and THICKNESS = 10
        [CommandMethod("JrxCommandTest", "ELEVTESTCASE2", CommandFlags.Modal)]
        public void TestCase2()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                double elevation = 20;
                double thickness = 10;
                DoTest(elevation, thickness);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        //Case 3: ELEVATION = 0 and THICKNESS = 20
        [CommandMethod("JrxCommandTest", "ELEVTESTCASE3", CommandFlags.Modal)]
        public void TestCase3()
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                double elevation = 0;
                double thickness = 20;
                DoTest(elevation, thickness);
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch (Exception)
            {
                Util.Editor().WriteMessage($"\n{currentMethod} ERROR!!!!");
            }
        }

        private void DoTest(double elevation, double thickness)
        {
            ExpectedResult expectedResult = new ExpectedResult(elevation,thickness);
            ActualResult actual = DoTestElevCmd(expectedResult);
            VertifyElevCmd(actual, expectedResult);
        }

        private ActualResult DoTestElevCmd(ExpectedResult expectedResult)
        {
            // Create Mock
            var userInputMock = new Mock<ElevCmd.IElevUserInput>(MockBehavior.Strict);

            // Setup First Distance
            var sequence = new MockSequence();
            userInputMock.InSequence(sequence).Setup(x => x.GetDistance(It.IsAny<PromptDistanceOptions>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", expectedResult.Elevation));

            // Setup Second Distance 
            userInputMock.InSequence(sequence).Setup(x => x.GetDistance(It.IsAny<PromptDistanceOptions>()))
                .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", expectedResult.Thickness));

            ////////////////////////////////////////
            // Execute Command
            ////////////////////////////////////////

            // Execute ElevCmd
            var cmd = new ElevCmd(userInputMock.Object);
            cmd.OnCommand();

            //Get Actual Result
            double elevation = SystemVariable.GetDouble("ELEVATION");
            double thickness = SystemVariable.GetDouble("THICKNESS");
            ActualResult actual = new ActualResult(elevation, thickness);
            return actual;
        }

        private void VertifyElevCmd(ActualResult actual, ExpectedResult expectedResult)
        {
            actual.Elevation.Is(expectedResult.Elevation);
            actual.Thickness.Is(expectedResult.Thickness);
        }
    }
}