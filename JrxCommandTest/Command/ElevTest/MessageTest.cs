﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;

#endif
using System.Reflection;
using JrxCad.Utility;
using JrxCad.Command.Elev;
using JrxCadTest.Utility;

namespace JrxCadTest.Command.ElevTest
{
    public class MessageTest
    {
        public static void ElevMessageTest(ElevCmd cmd, PromptDistanceOptions optFirst,
            PromptDistanceOptions optSecond)
        {
            VerifyCommandFlags(cmd);
            VerifyOptionsFirstDistanceP1(optFirst);
            VerifyOptionsSecondDistanceP2(optSecond);
        }

        public static void VerifyCommandFlags(ElevCmd cmd)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                VerifyEx.VerifyCommandMethod("SCAD_TEST_TOOLS", "SmxElev", CommandFlags.Transparent | CommandFlags.NoNewStack, cmd.GetType());
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch
            {
                Util.Editor().WriteMessage($"\n{currentMethod} error!");
            }
        }

        public static void VerifyOptionsFirstDistanceP1(PromptDistanceOptions optFirst)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                VerifyEx.VerifyPromptDistanceOptions(optFirst,
                  new PromptDistanceOptions($"\n{ElevRes.UI_P1}")
                  {
                      AllowNegative = true,
                      AllowZero = true,
                      DefaultValue = SystemVariable.GetDouble("ELEVATION"),
                  });
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch
            {
                Util.Editor().WriteMessage($"\n{currentMethod} error!");
            }

        }

        public static void VerifyOptionsSecondDistanceP2(PromptDistanceOptions optSecond)
        {
            string currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                VerifyEx.VerifyPromptDistanceOptions(optSecond,
                   new PromptDistanceOptions($"\n{ElevRes.UI_P2}")
                   {
                       AllowNegative = true,
                       AllowZero = true,
                       DefaultValue = SystemVariable.GetDouble("THICKNESS"),
                   });
                Util.Editor().WriteMessage($"\n{currentMethod} Completed!");
            }
            catch
            {
                Util.Editor().WriteMessage($"\n{currentMethod} error!");
            }

        }
    }
}
