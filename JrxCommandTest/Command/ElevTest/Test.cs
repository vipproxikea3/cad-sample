﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;

#endif
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using JrxCad.Utility;
using JrxCad.Command.Elev;
using JrxCadTest.Utility;

namespace JrxCadTest.Command.ElevTest
{
    [TestClass]
    public class Test : BaseTest
    {
        private PromptResult _resValueSet;
        private string _classFullName;

        public override void OnStart()
        {
            PromptKeywordOptions optValueSet = new PromptKeywordOptions($"Specify the case to test")
            {
                Keywords =
                {
                    { "TEXT1", "TEXT1", "TEXT1"},
                    { "TEXT2", "TEXT2", "TEXT2"},
                    { "TEXT3", "TEXT3", "TEXT3"},
                },
            };
            _resValueSet = Util.Editor().GetKeywords(optValueSet);
            var methodInfo = System.Reflection.MethodBase.GetCurrentMethod();
            if (methodInfo.DeclaringType != null) _classFullName = methodInfo.DeclaringType.FullName;

            if (_classFullName != null) Util.Editor().WriteMessage($"\n{_classFullName.ToUpper()} START!");
        }
        public override void OnFinish()
        {
            Util.Editor().WriteMessage($"\n{_classFullName.ToUpper()} COMPLETED!");
        }


        [CommandMethod("JrxCommandTest", "ELEVTEST", CommandFlags.Modal)]
        public void ElevTest()
        {
            OnStart();

            //CASE TEST
            var coordTest = new CoordTest();
            switch (_resValueSet.StringResult)
            {
                //Case 1: ELEVATION = 10 and THICKNESS = 0
                case "TEXT1":
                    coordTest.TestCase1();
                    break;

                //Case 2: ELEVATION = 20 and THICKNESS = 10
                case "TEXT2":
                    coordTest.TestCase2();
                    break;

                //Case 3: ELEVATION = 0 and THICKNESS = 20
                case "TEXT3":
                    coordTest.TestCase3();
                    break;
                default:
                    return;
            }

            //Resest elevation = 0 and thickness = 0 to DefaultValue in userInputMock equal DefaultValue in AutoCad
            using (new SystemVariable.Temp("ELEVATION", 0))
            using (new SystemVariable.Temp("THICKNESS", 0))
            {
                // Create Mock
                var userInputMock = new Mock<ElevCmd.IElevUserInput>(MockBehavior.Strict);

                // Setup First Distance 
                var sequence = new MockSequence();
                PromptDistanceOptions optFirst = null;
                double elevation = 0;
                userInputMock.Setup(x => x.GetDistance(It.IsAny<PromptDistanceOptions>()))
                    .Callback((PromptDistanceOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", elevation));

                // Setup Second Distance 
                PromptDistanceOptions optSecond = null;
                double thickness = 0;
                userInputMock.Setup(x => x.GetDistance(It.IsAny<PromptDistanceOptions>()))
                    .Callback((PromptDistanceOptions options) => optSecond = options)
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", thickness));

                ////////////////////////////////////////
                // Execute Command
                ////////////////////////////////////////

                // Execute ElevCmd
                var cmd = new ElevCmd(userInputMock.Object);
                cmd.OnCommand();

                //MessageTest
                MessageTest.ElevMessageTest(cmd, optFirst, optSecond);

                //////////////////////////////////////////
                //// Test Completed
                //////////////////////////////////////////

                userInputMock.VerifyAll();
                OnFinish();
            }
        }

        [CommandMethod("JrxCommandTest", "ELEVTESTAPI", CommandFlags.Modal)]
        public void ElevTestAPI()
        {
            using (new SystemVariable.Temp("ELEVATION", 0))
            using (new SystemVariable.Temp("THICKNESS", 0))
            {
                // Create Mock
                var userInputMock = new Mock<ElevCmd.IElevUserInput>(MockBehavior.Strict);

                // Setup First Distance 
                var sequence = new MockSequence();
                PromptDistanceOptions optFirst = null;
                double elevation = 0;
                userInputMock.Setup(x => x.GetDistance(It.IsAny<PromptDistanceOptions>()))
                    .Callback((PromptDistanceOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", elevation));

                // Setup Second Distance 
                PromptDistanceOptions optSecond = null;
                double thickness = 0;
                userInputMock.Setup(x => x.GetDistance(It.IsAny<PromptDistanceOptions>()))
                    .Callback((PromptDistanceOptions options) => optSecond = options)
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", thickness));

                ////////////////////////////////////////
                // Execute Command
                ////////////////////////////////////////

                // Execute ElevCmd
                var cmd = new ElevCmd(userInputMock.Object);
                cmd.OnCommand();

                //MessageTest
                MessageTest.VerifyOptionsFirstDistanceP1(optFirst);
                MessageTest.VerifyOptionsSecondDistanceP2(optSecond);

                //////////////////////////////////////////
                //// Test Completed
                //////////////////////////////////////////

                userInputMock.VerifyAll();
            }
        }

        [CommandMethod("JrxCommandTest", "ELEVTESTCOMMANDFLAGS", CommandFlags.Modal)]
        public void ElevTestCommandFlags()
        {
            using (new SystemVariable.Temp("ELEVATION", 0))
            using (new SystemVariable.Temp("THICKNESS", 0))
            {
                // Create Mock
                var userInputMock = new Mock<ElevCmd.IElevUserInput>(MockBehavior.Strict);

                // Setup First Distance 
                var sequence = new MockSequence();
                PromptDistanceOptions optFirst = null;
                double elevation = 0;
                userInputMock.Setup(x => x.GetDistance(It.IsAny<PromptDistanceOptions>()))
                    .Callback((PromptDistanceOptions options) => optFirst = options)
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", elevation));

                // Setup Second Distance 
                PromptDistanceOptions optSecond = null;
                double thickness = 0;
                userInputMock.Setup(x => x.GetDistance(It.IsAny<PromptDistanceOptions>()))
                    .Callback((PromptDistanceOptions options) => optSecond = options)
                    .Returns(VerifyEx.CreatePromptDoubleResult(PromptStatus.OK, "", thickness));

                ////////////////////////////////////////
                // Execute Command
                ////////////////////////////////////////

                // Execute ElevCmd
                var cmd = new ElevCmd(userInputMock.Object);
                cmd.OnCommand();

                //MessageTest
                MessageTest.VerifyCommandFlags(cmd);

                //////////////////////////////////////////
                //// Test Completed
                //////////////////////////////////////////

                userInputMock.VerifyAll();
            }
        }
    }
}
