﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;

#endif
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Reflection;
using JrxCad.Helpers;

namespace JrxCadTest.Utility
{
    public static class VerifyEx
    {
        /// <summary>
        /// Create PromptPointResult
        /// </summary>
        public static PromptPointResult CreatePromptPointResult(PromptStatus status, string result, Point3d value)
        {
            // Use reflection for internal constructor
            return Activator.CreateInstance(typeof(PromptPointResult),
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance,
                null, new object[] { status, result, value }, null) as PromptPointResult;
        }

        /// <summary>
        /// Create PromptDoubleResult
        /// </summary>
        public static PromptDoubleResult CreatePromptDoubleResult(PromptStatus status, string result, Double value)
        {
            // Use reflection for internal constructor
            return Activator.CreateInstance(typeof(PromptDoubleResult),
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance,
                null, new object[] { status, result, value }, null) as PromptDoubleResult;
        }

        /// <summary>
        /// Verify CommandMethod Attribute
        /// </summary>
        public static void VerifyCommandMethod(string groupName, string globalName, CommandFlags flags, Type type, string methodName = "OnCommand")
        {
            var method = type.GetMethod(methodName);
            method.IsNotNull();

            var att = method.GetCustomAttribute<CommandMethodAttribute>();
            att.IsNotNull();

            att.GlobalName.Is(globalName);
            att.GlobalName.Is(globalName);
            att.Flags.Is(flags);
        }

        /// <summary>
        /// Verify Keywords
        /// </summary>
        private static void VerifyKeywords(KeywordCollection keywords, KeywordCollection srcKeywords)
        {
            Keyword GetKeyword(KeywordCollection kyws, int idx)
            {
#if _IJCAD_
                return kyws.get_Item(idx);
#elif _AutoCAD_
                return kyws[idx];
#endif
            }

            keywords.Count.Is(srcKeywords.Count);
            for (int i = 0; i < keywords.Count; i++)
            {
                var k = GetKeyword(keywords, i);
                var sk = GetKeyword(srcKeywords, i);
                k.DisplayName.Is(sk.DisplayName);
                k.GlobalName.Is(sk.GlobalName);
                k.LocalName.Is(sk.LocalName);
                k.Enabled.Is(sk.Enabled);
                k.IsReadOnly.Is(sk.IsReadOnly);
                k.Visible.Is(sk.Visible);
            }
        }

        /// <summary>
        /// Verify PromptOptions
        /// </summary>
        private static void VerifyPromptOptions(PromptOptions options, PromptOptions srcOptions)
        {
            options.IsNotNull();
            options.Message.Is(srcOptions.Message);
            options.AppendKeywordsToMessage.Is(srcOptions.AppendKeywordsToMessage);
            VerifyKeywords(options.Keywords, srcOptions.Keywords);
        }

        /// <summary>
        /// Verify PromptCornerOptions
        /// </summary>
        private static void VerifyPromptCornerOptions(PromptCornerOptions options, PromptCornerOptions srcOptions)
        {
            VerifyPromptOptions(options, srcOptions);

            options.AllowNone.Is(srcOptions.AllowNone);
            options.AllowArbitraryInput.Is(srcOptions.AllowArbitraryInput);
            options.BasePoint.Is(srcOptions.BasePoint);
            options.UseDashedLine.Is(srcOptions.UseDashedLine);
            options.LimitsChecked.Is(srcOptions.LimitsChecked);
        }

        /// <summary>
        /// Verify PromptPointOptions
        /// </summary>
        public static void VerifyPromptPointOptions(PromptPointOptions options, PromptPointOptions srcOptions)
        {
            VerifyPromptCornerOptions(options, srcOptions);

            options.UseBasePoint.Is(srcOptions.UseBasePoint);
        }

        /// <summary>
        /// Verify PromptNumericalOptions
        /// </summary>
        public static void VerifyPromptNumericalOptions(PromptNumericalOptions options, PromptNumericalOptions srcOptions)
        {
            VerifyPromptOptions(options, srcOptions);

            options.AllowNone.Is(srcOptions.AllowNone);
            options.AllowArbitraryInput.Is(srcOptions.AllowArbitraryInput);
            options.AllowNegative.Is(srcOptions.AllowNegative);
            options.AllowZero.Is(srcOptions.AllowZero);
            options.UseDefaultValue.Is(srcOptions.UseDefaultValue);
        }

        /// <summary>
        /// Verify PromptDistanceOptions
        /// </summary>
        public static void VerifyPromptDistanceOptions(PromptDistanceOptions options, PromptDistanceOptions srcOptions)
        {
            VerifyPromptNumericalOptions(options, srcOptions);

            options.BasePoint.Is(srcOptions.BasePoint);
            options.UseBasePoint.Is(srcOptions.UseBasePoint);
            options.UseDashedLine.Is(srcOptions.UseDashedLine);
            options.Only2d.Is(srcOptions.Only2d);
            options.DefaultValue.IsEqual(srcOptions.DefaultValue).IsTrue();
        }

        /// <summary>
        /// Verify PromptIntegerOptions
        /// </summary>
        public static void VerifyPromptIntegerOptions(PromptIntegerOptions options, PromptIntegerOptions srcOptions)
        {
            VerifyPromptNumericalOptions(options, srcOptions);

            options.DefaultValue.Is(srcOptions.DefaultValue);
            options.LowerLimit.Is(srcOptions.LowerLimit);
            options.UpperLimit.Is(srcOptions.UpperLimit);
        }

        /// <summary>
        /// Verify PromptDoubleOptions
        /// </summary>
        public static void VerifyPromptDoubleOptions(PromptDoubleOptions options, PromptDoubleOptions srcOptions)
        {
            VerifyPromptNumericalOptions(options, srcOptions);

            options.DefaultValue.IsEqual(srcOptions.DefaultValue).IsTrue();
        }

        /// <summary>
        /// Verify PromptAngleOptions
        /// </summary>
        public static void VerifyPromptAngleOptions(PromptAngleOptions options, PromptAngleOptions srcOptions)
        {
            VerifyPromptOptions(options, srcOptions);

            options.DefaultValue.IsEqual(srcOptions.DefaultValue).IsTrue();
            options.UseDefaultValue.Is(srcOptions.UseDefaultValue);
            options.BasePoint.Is(srcOptions.BasePoint);
            options.UseBasePoint.Is(srcOptions.UseBasePoint);
            options.UseAngleBase.Is(srcOptions.UseAngleBase);
            options.AllowNone.Is(srcOptions.AllowNone);
            options.AllowArbitraryInput.Is(srcOptions.AllowArbitraryInput);
            options.AllowZero.Is(srcOptions.AllowZero);
            options.UseDashedLine.Is(srcOptions.UseDashedLine);
        }

        /// <summary>
        /// Verify PromptEntityOptions
        /// </summary>
        public static void VerifyPromptEntityOptions(PromptEntityOptions options, PromptEntityOptions srcOptions)
        {
            VerifyPromptOptions(options, srcOptions);

            options.AllowNone.Is(srcOptions.AllowNone);
            options.AllowObjectOnLockedLayer.Is(srcOptions.AllowObjectOnLockedLayer);
        }

        /// <summary>
        /// Verify PromptNestedEntityOptions
        /// </summary>
        public static void VerifyPromptNestedEntityOptions(PromptNestedEntityOptions options, PromptNestedEntityOptions srcOptions)
        {
            VerifyPromptOptions(options, srcOptions);

            options.AllowNone.Is(srcOptions.AllowNone);
            options.NonInteractivePickPoint.Is(srcOptions.NonInteractivePickPoint);
            options.UseNonInteractivePickPoint.Is(srcOptions.UseNonInteractivePickPoint);
        }

        /// <summary>
        /// Verify PromptKeywordOptions
        /// </summary>
        public static void VerifyPromptKeywordOptions(PromptKeywordOptions options, PromptKeywordOptions srcOptions)
        {
            VerifyPromptOptions(options, srcOptions);

            options.AllowArbitraryInput.Is(srcOptions.AllowArbitraryInput);
            options.AllowNone.Is(srcOptions.AllowNone);
        }

        /// <summary>
        /// Verify PromptStringOptions
        /// </summary>
        public static void VerifyPromptStringOptions(PromptStringOptions options, PromptStringOptions srcOptions)
        {
            VerifyPromptOptions(options, srcOptions);

            options.AllowSpaces.Is(srcOptions.AllowSpaces);
            options.DefaultValue.Is(srcOptions.DefaultValue);
            options.UseDefaultValue.Is(srcOptions.UseDefaultValue);
        }

        /// <summary>
        /// Verify PromptDragOptions
        /// </summary>
        public static void VerifyPromptDragOptions(PromptDragOptions options, PromptDragOptions srcOptions)
        {
            VerifyPromptOptions(options, srcOptions);

            options.AllowArbitraryInput.Is(srcOptions.AllowArbitraryInput);
            options.AllowNone.Is(srcOptions.AllowNone);
            options.Cursor.Is(srcOptions.Cursor);
        }

        /// <summary>
        /// Verify JigPromptOptions
        /// </summary>
        private static void VerifyJigPromptOptions(JigPromptOptions options, JigPromptOptions srcOptions)
        {
            VerifyPromptOptions(options, srcOptions);

            options.Cursor.Is(srcOptions.Cursor);
            options.UserInputControls.Is(srcOptions.UserInputControls);
        }

        /// <summary>
        /// Verify JigPromptStringOptions
        /// </summary>
        private static void VerifyJigPromptStringOptions(JigPromptStringOptions options, JigPromptStringOptions srcOptions)
        {
            VerifyJigPromptOptions(options, srcOptions);

            try
            {
                options.DefaultValue.Is(srcOptions.DefaultValue);
            }
            catch (NullReferenceException)
            {
                Assert.ThrowsException<NullReferenceException>(() => srcOptions.DefaultValue);
            }
        }

        /// <summary>
        /// Verify JigPromptOptions
        /// </summary>
        private static void VerifyJigGeometryPromptOptions(JigPromptGeometryOptions options, JigPromptGeometryOptions srcOptions)
        {
            VerifyJigPromptOptions(options, srcOptions);

            options.BasePoint.Is(srcOptions.BasePoint);
            options.UseBasePoint.Is(srcOptions.UseBasePoint);
        }

        /// <summary>
        /// Verify JigPromptPointOptions
        /// </summary>
        public static void VerifyJigPromptPointOptions(JigPromptPointOptions options, JigPromptPointOptions srcOptions)
        {
            VerifyJigGeometryPromptOptions(options, srcOptions);

            try
            {
                options.DefaultValue.Is(srcOptions.DefaultValue);
            }
            catch (NullReferenceException)
            {
                Assert.ThrowsException<NullReferenceException>(() => srcOptions.DefaultValue);
            }
        }

        /// <summary>
        /// Verify JigPromptAngleOptions
        /// </summary>
        public static void VerifyJigPromptAngleOptions(JigPromptAngleOptions options, JigPromptAngleOptions srcOptions)
        {
            VerifyJigGeometryPromptOptions(options, srcOptions);

            try
            {
                options.DefaultValue.IsEqual(srcOptions.DefaultValue).IsTrue();
            }
            catch (NullReferenceException)
            {
                Assert.ThrowsException<NullReferenceException>(() => srcOptions.DefaultValue);
            }
        }

        /// <summary>
        /// Verify JigPromptDistanceOptions
        /// </summary>
        public static void VerifyJigPromptDistanceOptions(JigPromptDistanceOptions options, JigPromptDistanceOptions srcOptions)
        {
            VerifyJigGeometryPromptOptions(options, srcOptions);

            try
            {
                options.DefaultValue.IsEqual(srcOptions.DefaultValue).IsTrue();
            }
            catch (NullReferenceException)
            {
                Assert.ThrowsException<NullReferenceException>(() => srcOptions.DefaultValue);
            }
        }

    }
}
