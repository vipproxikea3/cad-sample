﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;

#endif
using System;
using System.Reflection;
using JrxCad.Utility;

namespace JrxCadTest.Utility
{
    public static class CreateEx
    {
        /// <summary>
        /// MemberwiseClone
        /// </summary>
        public static T MemberwiseClone<T>(this T source) where T : class
        {
            return typeof(T).InvokeMember("MemberwiseClone",
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.InvokeMethod,
                null, source, null) as T;
        }

        /// <summary>
        /// Create PromptPointResult
        /// </summary>
        public static PromptPointResult CreatePromptPointResult(PromptStatus status, string result, Point3d value)
        {
            // Use reflection for internal constructor
            return Activator.CreateInstance(typeof(PromptPointResult),
                BindingFlags.NonPublic | BindingFlags.Instance,
                null, new object[] { status, result, value }, null) as PromptPointResult;
        }

        /// <summary>
        /// Create PromptDoubleResult
        /// </summary>
        public static PromptDoubleResult CreatePromptDoubleResult(PromptStatus status, string result, double value)
        {
            // Use reflection for internal constructor
            return Activator.CreateInstance(typeof(PromptDoubleResult),
                BindingFlags.NonPublic | BindingFlags.Instance,
                null, new object[] { status, result, value }, null) as PromptDoubleResult;
        }

        private static Vector3d GetPickBoxVector()
        {
            var pickBox = SystemVariable.GetInt("PICKBOX");
            var pt1 = Util.Editor().PointToWorld(new System.Drawing.Point(0, 0), 0);
            var pt2 = Util.Editor().PointToWorld(new System.Drawing.Point(pickBox, pickBox), 0);
            return pt2 - pt1;
        }

        /// <summary>
        /// Create PromptEntityResult
        /// </summary>
        public static PromptEntityResult CreatePromptEntityResult(PromptStatus status, string result, Point3d? pickPoint)
        {
            var id = ObjectId.Null;
            if (pickPoint != null)
            {
                var pickBoxVector = GetPickBoxVector();
                var res = Util.Editor().SelectCrossingWindow(pickPoint.Value - pickBoxVector, pickPoint.Value + pickBoxVector);
                if (res.Value == null) return null;
                if (res.Value.Count > 0)
                {
                    id = res.Value[0].ObjectId;
                }
            }

            // Use reflection for internal constructor
            return Activator.CreateInstance(typeof(PromptEntityResult),
                BindingFlags.NonPublic | BindingFlags.Instance,
                null, new object[] { status, result, id, pickPoint ?? Point3d.Origin }, null) as PromptEntityResult;
        }

        /// <summary>
        /// Create PromptNestedEntityResult
        /// </summary>
        public static PromptNestedEntityResult CreatePromptNestedEntityResult(PromptStatus status, string result, Point3d? pickPoint)
        {
            if (pickPoint != null)
            {
                var opt = new PromptNestedEntityOptions("")
                {
                    UseNonInteractivePickPoint = true,
                    NonInteractivePickPoint = pickPoint.Value,
                };
                return Util.Editor().GetNestedEntity(opt);
            }

            // Use reflection for internal constructor
            return Activator.CreateInstance(typeof(PromptNestedEntityResult),
                BindingFlags.NonPublic | BindingFlags.Instance,
                null, new object[] { status, result, ObjectId.Null, Point3d.Origin, Matrix3d.Identity, null }, null) as PromptNestedEntityResult;
        }

        /// <summary>
        /// Create PromptSelectionResult
        /// </summary>
        public static PromptSelectionResult CreatePromptSelectionResult(PromptStatus status, SelectionSet selectionSet)
        {
            if (selectionSet != null)
            {
                Util.Editor().SetImpliedSelection(selectionSet);
                return Util.Editor().SelectImplied();
            }
            var res = Activator.CreateInstance(typeof(PromptSelectionResult),
                BindingFlags.NonPublic | BindingFlags.Instance,
                null, new object[] { status, null }, null) as PromptSelectionResult;
            var field = typeof(PromptSelectionResult).GetField("m_status", BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Instance);
            field?.SetValue(res, SelectionSet.FromObjectIds(Array.Empty<ObjectId>()));
            return res;
        }

        /// <summary>
        /// Create PromptSelectionResult By CrossingWindow
        /// </summary>
        public static PromptSelectionResult CreatePromptSelectionResultByCrossingWindow(Point3d pt1, Point3d pt2, SelectionFilter filter)
        {
            return Util.Editor().SelectCrossingWindow(pt1, pt2, filter);
        }

        /// <summary>
        /// Create PromptResult
        /// </summary>
        public static PromptResult CreatePromptResult(PromptStatus status, string stringResult)
        {
            return Activator.CreateInstance(typeof(PromptResult),
                   BindingFlags.NonPublic | BindingFlags.Instance,
                   null, new object[] { status, stringResult }, null) as PromptResult;
        }

        /// <summary>
        /// Create PromtIntegerResult
        /// </summary>
        public static PromptIntegerResult CreatePromptIntegerResult(PromptStatus status, string result, int value)
        {
            return Activator.CreateInstance(typeof(PromptIntegerResult),
                 BindingFlags.NonPublic | BindingFlags.Instance,
                 null, new object[] { status, result, value }, null) as PromptIntegerResult;
        }
    }
}
