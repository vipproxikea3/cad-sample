﻿namespace JrxCadTest.Utility
{
    public abstract class BaseTest
    {
        public abstract void OnStart();
        public abstract void OnFinish();
    }
}
