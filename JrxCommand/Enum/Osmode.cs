#if _IJCAD_

#elif _AutoCAD_

#endif

using System;

namespace JrxCad.Enum
{
    [Flags]
    internal enum Osmode
    {
        /// <summary>
        /// 解除
        /// </summary>
        Non = 0,

        /// <summary>
        /// 端点
        /// </summary>
        End = 1,

        /// <summary>
        /// 中点
        /// </summary>
        Mid = 2,

        /// <summary>
        /// 中心
        /// </summary>
        Cen = 4,

        /// <summary>
        /// 点
        /// </summary>
        Nod = 8,

        /// <summary>
        /// 四半円点
        /// </summary>
        Qua = 16,

        /// <summary>
        /// 交点
        /// </summary>
        Int = 32,

        /// <summary>
        /// 挿入基点
        /// </summary>
        Ins = 64,

        /// <summary>
        /// 垂線
        /// </summary>
        Per = 128,

        /// <summary>
        /// 接線
        /// </summary>
        Tan = 256,

        /// <summary>
        /// 近接点
        /// </summary>
        Nea = 512,

        /// <summary>
        /// 図芯
        /// </summary>
        Gce = 1024,

        /// <summary>
        /// 仮想交点
        /// </summary>
        App = 2048,

        /// <summary>
        /// 延長
        /// </summary>
        Ext = 4096,

        /// <summary>
        /// 平行
        /// </summary>
        Par = 8192,

        /// <summary>
        /// すべてON
        /// </summary>
        All = 16383,

        /// <summary>
        /// すべてOFF
        /// </summary>
        AllOff = 16384,
    }
}
