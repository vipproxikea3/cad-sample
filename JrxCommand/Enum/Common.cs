﻿#if _IJCAD_

#elif _AutoCAD_

#endif

using System;

namespace JrxCad.Enum
{
    /// <summary>
    /// Scale type
    /// </summary>
    public enum ScaleType
    {
        X = 0,
        Y = 1,
        Z = 2
    }

    public enum SnapIsoPair
    {
        Left = 0,
        Top = 1,
        Right = 2
    }

    public enum PEllipse
    {
        TrueEllipse = 0,
        PolylineEllipse = 1
    }

    public enum SnapStyl
    {
        Rectangular = 0,
        Isometric = 1
    }

    public enum TILEMODE
    {
        MostRecentlyAccessedLayout = 0,
        ModelSpace = 1
    }
}
