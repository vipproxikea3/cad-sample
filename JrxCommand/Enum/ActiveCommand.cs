﻿#if _IJCAD_

#elif _AutoCAD_

#endif

using System;

namespace JrxCad.Enum
{
    /// <summary>
    /// CMDACTIVE (System Variable)
    /// </summary>
    [Flags]
    internal enum ActiveCommand
    {
        /// <summary>
        /// No active command
        /// </summary>
        None = 0,

        /// <summary>
        /// Ordinary command is active
        /// </summary>
        Ordinary = 1,

        /// <summary>
        /// Transparent command is active
        /// </summary>
        Transparent = 2,

        /// <summary>
        /// Script is active
        /// </summary>
        Script = 4,

        /// <summary>
        /// Dialog box is active
        /// </summary>
        DialogBox = 8,

        /// <summary>
        /// DDE is active
        /// </summary>
        DDE = 16,

        /// <summary>
        /// AutoLISP is active (only visible to an ObjectARX -defined command)
        /// </summary>
        AutoLISP = 32,

        /// <summary>
        /// ObjectARX command is active
        /// </summary>
        ObjectARX = 64
    }
}
