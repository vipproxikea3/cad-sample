﻿#if _IJCAD_

#elif _AutoCAD_

#endif

using System;

namespace JrxCad.Enum
{
    /// <summary>
    /// CMDECHO (System Variable)
    /// </summary>
    internal enum Echo
    {
        /// <summary>
        /// Turn off echoing
        /// </summary>
        Off = 0,

        /// <summary>
        /// Turns on echoing
        /// </summary>
        On = 1
    }
}
