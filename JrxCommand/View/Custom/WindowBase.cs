﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace JrxCad.View.Custom
{
    public class WindowBase : Window
    {
        public bool IsHorizontalScale { get; set; } = false;
        public bool IsVerticalScale { get; set; } = false;

        public WindowBase()
        {
            SourceInitialized += SourceInitializedWindow;
        }

        private void SourceInitializedWindow(object sender, EventArgs e)
        {
            if (!IsHorizontalScale && !IsVerticalScale) ResizeMode = ResizeMode.NoResize;
            else
            {
                _windowHandle = new WindowInteropHelper(this).Handle;
                HideMinimizeAndMaximizeButtons();
                if (!IsHorizontalScale) MinWidth = MaxWidth = ActualWidth;
                if (!IsVerticalScale) MinHeight = MaxHeight = ActualHeight;
                MinWidth = ActualWidth;
                MinHeight = ActualHeight;
            }
        }

        private IntPtr _windowHandle;

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private const int GWL_STYLE = -16;

        private const int WS_MAXIMIZEBOX = 0x10000; //maximize button
        private const int WS_MINIMIZEBOX = 0x20000; //minimize button

        protected void DisableMinimizeButton()
        {
            if (_windowHandle == IntPtr.Zero)
                throw new InvalidOperationException("The window has not yet been completely initialized");
            SetWindowLong(_windowHandle, GWL_STYLE, GetWindowLong(_windowHandle, GWL_STYLE) & ~WS_MINIMIZEBOX);
        }

        protected void DisableMaximizeButton()
        {
            if (_windowHandle == null)
                throw new InvalidOperationException("The window has not yet been completely initialized");
            SetWindowLong(_windowHandle, GWL_STYLE, GetWindowLong(_windowHandle, GWL_STYLE) & ~WS_MAXIMIZEBOX);
        }

        protected void HideMinimizeAndMaximizeButtons()
        {
            if (_windowHandle == null)
                throw new InvalidOperationException("The window has not yet been completely initialized");
            SetWindowLong(_windowHandle, GWL_STYLE, GetWindowLong(_windowHandle, GWL_STYLE) & ~WS_MAXIMIZEBOX & ~WS_MINIMIZEBOX);
        }
    }
}
