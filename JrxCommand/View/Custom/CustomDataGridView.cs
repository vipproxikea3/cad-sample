#if _IJCAD_

#elif _AutoCAD_

#endif
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.View.Custom
{
    public partial class CustomDataGridView<T> : DataGridView where T : new()
    {
        private List<T> _bindingList;

        public CustomDataGridView()
        {
            InitializeComponent();

            _bindingList = new List<T>();
        }

        protected override void OnBindingContextChanged(EventArgs e)
        {
            base.OnBindingContextChanged(e);

            if (DataSource is List<T> data) _bindingList = data;
        }

        private void CustomDataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (!IsCurrentCellDirty) return;

                //コミットする
                CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        public string GetString(string columnName)
        {
            var value = SelectedRows[0].Cells[columnName].Value;
            return value?.ToString();
        }

        public bool GetBool(string columnName)
        {
            var value = SelectedRows[0].Cells[columnName].Value;
            if (value == null) return false;
            if (!bool.TryParse(value as string, out var bvalue)) return false;
            return bvalue;
        }

        public T Get()
        {
            var index = SelectedRows[0].Index;
            return _bindingList[index];
        }
    }
}
