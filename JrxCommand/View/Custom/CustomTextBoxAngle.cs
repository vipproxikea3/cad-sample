﻿#if _IJCAD_
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
#endif

using System.ComponentModel;
using JrxCad.Utility;

namespace JrxCad.View.Custom
{
    public partial class CustomTextBoxAngle : CustomTextBox
    {
        private bool _isLoaded;

        private double _radian;
        public double Radian
        {
            get => _radian;
            set => SetRadian(value);
        }

        public CustomTextBoxAngle()
        {
            InitializeComponent();
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            if (DesignMode) return;
            _isLoaded = true;
            SetRadian(0);
        }

        protected override void OnValidating(CancelEventArgs e)
        {
            base.OnValidating(e);

            try
            {
                if (DesignMode) return;
                if (!Enabled || !Visible) return;

                SetRadian(Converter.StringToAngle(Text));
            }
            catch
            {
                MsgBox.Error.Show("角度が無効です。");
                e.Cancel = true;
            }
        }

        private void SetRadian(double radian)
        {
            if (!_isLoaded) return;
            _radian = radian;
            Text = Converter.AngleToString(_radian);
        }
    }
}
