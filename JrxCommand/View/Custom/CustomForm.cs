using System;
using System.Drawing;
using System.Windows.Forms;
using JrxCad.Utility;
#if _IJCAD_
using CADApp = GrxCAD.ApplicationServices.Application;
using Application = GrxCAD.ApplicationServices.Application;
#elif _AutoCAD_
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using Application = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.Internal;

#endif

namespace JrxCad.View.Custom
{
    public partial class CustomForm : Form
    {
        /// <summary>
        /// Command Name for Help
        /// </summary>
        protected virtual string HelpName { get; }

        public bool ResizeVertical { get; set; } = true;
        public bool ResizeHorizontal { get; set; } = true;

        public CustomForm()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Set MaximumSize by ResizeVertical/ResizeHorizontal
        /// </summary>
        private void SetMaximumSize()
        {
            try
            {
                var workingArea = Screen.PrimaryScreen.WorkingArea;
                var width = workingArea.Width * 2;
                var height = workingArea.Height * 2;

                if (!ResizeHorizontal) width = Width;
                if (!ResizeVertical) height = Height;

                MaximumSize = new Size(width, height);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            if (DesignMode) return;

            SetMaximumSize();
            MinimumSize = Size;

            Logger.Info($"【{Text}::{Name}】Form  Start");
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            Logger.Info($"【{Text}::{Name}】Form  End");
        }

        /// <summary>
        /// Modal
        /// </summary>
        public new DialogResult ShowDialog()
        {
            if (DesignMode) return DialogResult.OK;

            return CADApp.ShowModalDialog(Application.MainWindow.Handle, this);
        }

        /// <summary>
        /// Modal (Display in the center of owner)
        /// </summary>
        public DialogResult ShowDialog(Form owner)
        {
            if (DesignMode) return DialogResult.OK;

            return CADApp.ShowModalDialog(owner, this, false);
        }

        /// <summary>
        /// Tooltip Width 350 or less
        /// </summary>
        private void ToolTip_Popup(object sender, PopupEventArgs e)
        {
            var enabledTooltip = SystemVariable.GetBool("TOOLTIPS");
            if (!enabledTooltip)
            {
                e.Cancel = true;
                return;
            }

            var size = e.ToolTipSize;
            if (size.Width < 350) return;

            var rowCount = size.Width / 350 + 1;
            e.ToolTipSize = new Size(350, size.Height * rowCount);
        }

        /// <summary>
        /// Help
        /// </summary>
        private void BtnHelp_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(HelpName))
            {
#if _AutoCAD_
                Utils.DoHelpForCommand(HelpName);
#endif
            }
        }
    }
}
