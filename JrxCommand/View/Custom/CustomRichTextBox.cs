﻿
#if _IJCAD_
using GrxCAD.DatabaseServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
#endif
using System.Runtime.InteropServices;
using System.Threading;
using System.Text.RegularExpressions;
using JrxCad.Command.AttEdit;
using JrxCad.Utility;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace JrxCad.View.Custom
{
    public partial class CustomRichTextBox : RichTextBox
    {
        [DllImport("AcFdUi.arx", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?AcFdUiInvokeFieldDialog@@YAHAEAPEAVAcDbField@@HPEAVAcDbDatabase@@PEAVCWnd@@@Z")]
        private static extern Int32 InvokeFieldDialog(ref IntPtr fd, Int32 bEdit, IntPtr pDb, IntPtr pParent);

        public string TooltipText { get; set; }
        public FormAttEdit.AttributeItem Attribute;
        public Field[] FieldList;
        private Field currentField = null;
        private int currentStart = 0;
        public AttributeReference AttRef;
        public object[] IndexField = null;
        private int caretPos = 0;
        private int _oldTxtDLength = -1;

        private bool _isEditField = false;

        private readonly ContextMenuStrip _contextMenuStrip = new ContextMenuStrip();
        public CustomRichTextBox()
        {
            InitializeComponent();
            MouseClick += new MouseEventHandler(MouseClickEvent);
            MouseDoubleClick += new MouseEventHandler(MouseDoubleClickEvent);
            SelectionChanged += new EventHandler(SelectionChangedEvent);
            TextChanged += new EventHandler(TextChangedEvent);
            _contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(ContextMenuStrip_Opening);
            this.ContextMenuStrip = _contextMenuStrip;
        }

        private void CustomRichTextBox_Enter(object sender, EventArgs e)
        {
            SelectAll();
        }
        private void CustomRichTextBox_MouseHover(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TooltipText)) return;
            toolTip.SetToolTip(this, TooltipText);
        }

        public void SetFieldIndexAndBackColor()
        {

            var text = AttRef.getTextWithFieldCodes();
            int index = 0, nStartPos = -1, nEndPos = -1;
            IndexField = new object[FieldList.Length];

            while (Field.FindField(text, 0, ref nStartPos, ref nEndPos))
            {
                var field = FieldList[index];
                var value = field.GetStringValue();

                var escape = text.Substring(nStartPos, nEndPos - nStartPos + 1);
                var reg = new Regex(Regex.Escape(escape));
                text = reg.Replace(text, value, 1);

                var indexField = new { Start = nStartPos, End = nStartPos + value.Length };
                IndexField[index] = indexField;
                index++;
                if (index >= FieldList.Count()) break;
            }

            Text = text;
            Select(0, Text.Length);
            SelectionBackColor = Color.White;

            for (var i = 0; i < IndexField.Length; i++)
            {
                var start = (int)GetValObjDy(IndexField[i], "Start");
                var end = (int)GetValObjDy(IndexField[i], "End");
                Select(start, end - start);
                SelectionBackColor = Color.DarkGray;
            }

            ClearUndo();
        }

        private void MouseClickEvent(object sender, MouseEventArgs e)
        {
            try
            {
                caretPos = GetCharIndexFromPosition(e.Location);
                if (Attribute != null && Attribute.HasFields)
                {
                    for (var i = 0; i < IndexField.Length; i++)
                    {
                        var start = (int)GetValObjDy(IndexField[i], "Start");
                        var end = (int)GetValObjDy(IndexField[i], "End");
                        if (SelectionStart >= start &&
                            SelectionStart <= end)
                        {
                            Select(start, end - start);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        private void MouseDoubleClickEvent(object sender, MouseEventArgs e)
        {
            try
            {
                if (FieldList != null)
                {
                    var index = GetCharIndexFromPosition(e.Location);
                    for (var i = 0; i < IndexField.Length; i++)
                    {
                        var start = (int)GetValObjDy(IndexField[i], "Start");
                        var end = (int)GetValObjDy(IndexField[i], "End");

                        if (start <= index && index <= end)
                        {
                            currentField = FieldList[i];
                            currentStart = start;
                            Select(start, end - start);
                            break;
                        }

                        if (index < start)
                        {
                            Select(0, end);
                            break;
                        }

                        if (i == IndexField.Length - 1 && index > start)
                        {
                            Select(start, TextLength - start);
                            break;
                        }

                        if (i + 1 < IndexField.Length)
                        {
                            var nextStart = (int)GetValObjDy(IndexField[i + 1], "Start");
                            var nextEnd = (int)GetValObjDy(IndexField[i + 1], "End");
                            if (index > end && index < nextStart)
                            {
                                Select(start, nextEnd);
                                break;
                            }
                        }
                    }

                    if (currentField != null)
                    {
                        EditField();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        private void EditField()
        {
            _isEditField = true;
            var ptr = currentField.UnmanagedObject;
            InvokeFieldDialog(ref ptr, 1, Util.CurDoc().Database.UnmanagedObject, IntPtr.Zero);
            if (!_isEditField) return;
            SetFieldIndexAndBackColor();
            Select(currentStart, currentField.GetStringValue().Length);
            _isEditField = false;
        }

        private object GetValObjDy(object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName).GetValue(obj, null);
        }


        private void TextChangedEvent(object sender, EventArgs e)
        {
            try
            {
                if (Text.Contains("\t"))
                {
                    Text = Text.Replace("\t", "");
                    if (Attribute != null && Attribute.HasFields)
                    {
                        SetFieldIndexAndBackColor();
                    }
                }

                if (Attribute != null && Attribute.HasFields && !_isEditField)
                {
                    for (var i = 0; i < IndexField.Length; i++)
                    {
                        var start = (int)GetValObjDy(IndexField[i], "Start");
                        var end = (int)GetValObjDy(IndexField[i], "End");

                        if (_oldTxtDLength != -1 && _oldTxtDLength < Text.Length && SelectionStart <= start + 1)
                        {
                            var indexField = new { Start = start + 1, End = end + 1 };
                            IndexField[i] = indexField;
                            start++;
                            end++;
                        }

                        if (_oldTxtDLength > Text.Length && SelectionStart <= start && start != 0)
                        {
                            var indexField = new { Start = start - 1, End = end - 1 };
                            IndexField[i] = indexField;
                            start--;
                            end--;
                        }
                        if ((SelectionStart == start || SelectionStart ==
                            end + 1) && start != 0)
                        {
                            Select(SelectionStart - 1, 1);
                            SelectionBackColor = Color.White;
                            Select(SelectionStart + 1, 0);
                        }
                    }
                    _oldTxtDLength = Text.Length;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        private void SelectionChangedEvent(object sender, EventArgs e)
        {
            try
            {
                if (Attribute != null && Attribute.HasFields)
                {
                    if (Text.Length == _oldTxtDLength)
                    {
                        for (var i = 0; i < IndexField.Length; i++)
                        {
                            var start = (int)GetValObjDy(IndexField[i], "Start");
                            var end = (int)GetValObjDy(IndexField[i], "End");
                            if (SelectionStart == start + 1)
                            {
                                Thread.Sleep(200);
                                Select(end, 0);
                                return;
                            }
                            if (SelectionStart == end - 1)
                            {
                                Thread.Sleep(200);
                                Select(start, 0);
                                return;
                            }
                            if (SelectionStart < caretPos && caretPos >= start && caretPos <= end && !_isEditField)
                            {
                                Select(start, end - start);
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        private void AddItemsToMenu(string[] keys)
        {
            foreach (var key in keys)
            {
                var item = _contextMenuStrip.Items.Add(key);
                if (key == "-") continue;
                item.Name = key;
            }
        }

        #region menu event
        private void Menu_Undo(object sender, EventArgs e)
        {
            Undo();
        }
        private void Menu_Cut(object sender, EventArgs e)
        {
            if (SelectedText != "")
            {
                Cut();
            }
        }
        private void Menu_Copy(object sender, EventArgs e)
        {
            if (SelectionLength > 0)
            {
                Copy();
            }
        }
        private void Menu_Paste(object sender, EventArgs e)
        {
            Paste();
        }
        private void Menu_Delete(object sender, EventArgs e)
        {
            var SelectionIndex = SelectionStart;
            var SelectionCount = SelectionLength;
            Text = Text.Remove(SelectionStart, SelectionCount);
            SelectionStart = SelectionIndex - SelectionCount;
        }
        private void Menu_InsertField(object sender, EventArgs e)
        {
            var ptr = currentField.UnmanagedObject;
            InvokeFieldDialog(ref ptr, 1, Util.CurDoc().Database.UnmanagedObject, IntPtr.Zero);
            Text = Text.Insert(SelectionStart, currentField.GetStringValue());
        }

        private void Menu_EditField(object sender, EventArgs e)
        {
            EditField();
        }
        private void Menu_UpdateField(object sender, EventArgs e)
        {

        }
        private void Menu_ConvertFieldToText(object sender, EventArgs e)
        {
        }

        private void Menu_SelectAll(object sender, EventArgs e)
        {
            SelectAll();
        }
        #endregion


        private void ContextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var txt = _contextMenuStrip.SourceControl as CustomRichTextBox;
            if (!txt.Focused)
            {
                txt.Focus();
                txt.Select(0, 0);
            }
            _contextMenuStrip.Items.Clear();
            var isField = false;
            if (IndexField != null)
            {
                for (var i = 0; i < IndexField.Length; i++)
                {
                    var start = (int)GetValObjDy(IndexField[i], "Start");
                    var end = (int)GetValObjDy(IndexField[i], "End");
                    if (start == SelectionStart && SelectedText.Length == end - start)
                    {
                        isField = true;
                        currentField = FieldList[i];
                        currentStart = start;
                    }
                }
            }
            string[] keys;
            if (!isField)
            {
                keys = new string[] { "Undo", "-", "Cut", "Copy", "Paste", "Delete", "-", "Insert Field...", "-", "Select All" };
            }
            else
            {
                keys = new string[] { "Undo", "-", "Cut", "Copy", "Paste", "Delete", "-", "Edit Field...", "Update Field", "Convert Field To Text", "-", "Select All" };
            }
            AddItemsToMenu(keys);
            _contextMenuStrip.Items["Undo"].Click += new EventHandler(this.Menu_Undo);
            _contextMenuStrip.Items["Cut"].Click += new EventHandler(this.Menu_Cut);
            _contextMenuStrip.Items["Copy"].Click += new EventHandler(this.Menu_Copy);
            _contextMenuStrip.Items["Paste"].Click += new EventHandler(this.Menu_Paste);
            _contextMenuStrip.Items["Delete"].Click += new EventHandler(this.Menu_Delete);
            if (!isField)
            {
                _contextMenuStrip.Items["Insert Field..."].Click += new EventHandler(this.Menu_InsertField);
            }
            else
            {
                _contextMenuStrip.Items["Edit Field..."].Click += new EventHandler(this.Menu_EditField);
                _contextMenuStrip.Items["Update Field"].Click += new EventHandler(this.Menu_UpdateField);
                _contextMenuStrip.Items["Convert Field To Text"].Click += new EventHandler(this.Menu_ConvertFieldToText);
            }
            _contextMenuStrip.Items["Select All"].Click += new EventHandler(this.Menu_SelectAll);
            if (CanUndo)
            {
                _contextMenuStrip.Items["Undo"].Enabled = true;
            }
            else
            {
                _contextMenuStrip.Items["Undo"].Enabled = false;
            }

            if (SelectedText.Length == 0)
            {
                _contextMenuStrip.Items["Cut"].Enabled = false;
                _contextMenuStrip.Items["Copy"].Enabled = false;
                _contextMenuStrip.Items["Delete"].Enabled = false;
            }
            else
            {
                _contextMenuStrip.Items["Cut"].Enabled = true;
                _contextMenuStrip.Items["Copy"].Enabled = true;
                _contextMenuStrip.Items["Delete"].Enabled = true;
            }

            if (Clipboard.ContainsText())
            {
                _contextMenuStrip.Items["Paste"].Enabled = true;
            }
            else
            {
                _contextMenuStrip.Items["Paste"].Enabled = false;
            }

            if (Text.Length == 0)
            {
                _contextMenuStrip.Items["Select All"].Enabled = false;
            }
            else
            {
                _contextMenuStrip.Items["Select All"].Enabled = true;
            }
            e.Cancel = false;
        }

    }
}
