#if _IJCAD_

#elif _AutoCAD_
#endif

using System;
using System.Windows.Forms;

namespace JrxCad.View.Custom
{
    public partial class CustomTextBox : TextBox
    {
        public string TooltipText { get; set; }

        public CustomTextBox()
        {
            InitializeComponent();
        }

        private void CustomTextBox_Enter(object sender, EventArgs e)
        {
            SelectAll();
        }

        private void CustomTextBox_MouseHover(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TooltipText)) return;

            toolTip.SetToolTip(this, TooltipText);
        }
    }
}
