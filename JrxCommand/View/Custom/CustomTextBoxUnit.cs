﻿#if _IJCAD_
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
#endif

using System.ComponentModel;
using JrxCad.Utility;

namespace JrxCad.View.Custom
{
    public partial class CustomTextBoxUnit : CustomTextBox
    {
        private bool _isLoaded;

        private double _value;
        public double Value
        {
            get => _value;
            set => SetValue(value);
        }

        public CustomTextBoxUnit()
        {
            InitializeComponent();
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            if (DesignMode) return;
            _isLoaded = true;
            SetValue(0);
        }

        protected override void OnValidating(CancelEventArgs e)
        {
            base.OnValidating(e);

            try
            {
                if (DesignMode) return;
                if (!Enabled || !Visible) return;

                SetValue(Converter.StringToDistance(Text));
            }
            catch
            {
                MsgBox.Error.Show("値が無効です。");
                e.Cancel = true;
            }
        }

        private void SetValue(double value)
        {
            if (!_isLoaded) return;
            _value = value;
            Text = Converter.DistanceToString(_value);
        }
    }
}
