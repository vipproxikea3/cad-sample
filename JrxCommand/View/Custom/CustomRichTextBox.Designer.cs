﻿namespace JrxCad.View.Custom
{
    partial class CustomRichTextBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Component Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolTip = new CustomToolTip();
            this.SuspendLayout();
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 10000;
            this.toolTip.InitialDelay = 500;
            this.toolTip.ReshowDelay = 100;
            // 
            // CustomRichTextBox
            // 
            this.Enter += new System.EventHandler(this.CustomRichTextBox_Enter);
            this.MouseHover += new System.EventHandler(this.CustomRichTextBox_MouseHover);
            this.ResumeLayout(false);
        }
        #endregion
        private CustomToolTip toolTip;
    }
}
