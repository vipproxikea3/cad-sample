#if _IJCAD_

#elif _AutoCAD_

#endif

using System;
using System.Windows.Forms;
using JrxCad.Utility;

namespace JrxCad.View.Custom
{
    public partial class CustomButton : Button
    {
        /// <summary>
        /// ツールチップテキスト
        /// </summary>
        public string TooltipText { get; set; }

        public CustomButton()
        {
            InitializeComponent();
        }

        protected override void OnMouseHover(EventArgs e)
        {
            if (string.IsNullOrEmpty(TooltipText)) return;

            toolTip.SetToolTip(this, TooltipText);
        }

        protected override void OnClick(EventArgs e)
        {
            Logger.Info($"{Text}::{Name} Click");
            base.OnClick(e);
        }
    }
}
