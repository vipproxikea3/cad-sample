#if _IJCAD_

#elif _AutoCAD_

#endif

using System.Drawing;
using System.Windows.Forms;
using JrxCad.Utility;

namespace JrxCad.View.Custom
{
    public partial class CustomToolTip : ToolTip
    {
        public CustomToolTip()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tooltipの幅は350以下とする
        /// </summary>
        private void CustomToolTip_Popup(object sender, PopupEventArgs e)
        {
            if (!SystemVariable.GetBool("Tooltips"))
            {
                e.Cancel = true;
                return;
            }
            var size = e.ToolTipSize;
            if (size.Width < 350) return;

            var rowCount = size.Width / 350 + 1;
            e.ToolTipSize = new Size(350, size.Height * rowCount);
        }
    }
}
