#if _IJCAD_
using Application = GrxCAD.ApplicationServices.Application;

#elif _AutoCAD_
using Application = Autodesk.AutoCAD.ApplicationServices.Core.Application;

#endif
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;
using Button = System.Windows.Controls.Button;

namespace JrxCad.View.Message
{
    /// <summary>
    /// WindowMessage.xaml の相互作用ロジック
    /// </summary>
    public partial class AskMessage
    {
        public string Title1 { get; set; }
        public string Title2 { get; set; }
        public string Message { get; set; }

        public Dictionary<string, string> Answers { get; set; }

        public int Answer { get; set; }

        private readonly Form _owner;
        private bool _isLoaded;

        public AskMessage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Formが親の時
        /// </summary>
        /// <param name="owner">Form</param>
        public AskMessage(Form owner)
        {
            InitializeComponent();

            _owner = owner;
        }

        public new void ShowDialog()
        {
            var owner = GetWindow(this);
            Application.ShowModalWindow(owner, this, false);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Title = string.IsNullOrEmpty(Title2) 
                ? Title1 
                : $"{Title1} - {Title2}";

            txtMessage.Text = Message;
            grid.Rows = Answers.Count;
            Answer = 0;

            var index = 0;
            foreach (var answer in Answers)
            {
                var btn = new Button
                {
                    Name = "Answer" + index++,
                    Content = answer.Key,
                    Tag = answer.Value,
                    Margin = new Thickness(0)
                };

                btn.Click += Answer_OnClick;

                grid.Children.Add(btn);
            }
        }

        private void Answer_OnClick(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button btn)) return;

            Answer = int.Parse(btn.Name.Replace("Answer", ""));
            DialogResult = true;
            Close();
        }


        /// <summary>
        /// Formが親の時のCenterParentの処理
        /// </summary>
        private void Window_LocationChanged(object sender, EventArgs e)
        {
            if (_owner != null && !_isLoaded)
            {
                Left = _owner.Left + (_owner.Width - ActualWidth) / 2;
                Top = _owner.Top + (_owner.Height - ActualHeight) / 2;
            }
        }

        /// <summary>
        /// 画面が完全に初期化された後に発生
        /// </summary>
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            _isLoaded = true;
        }
    }
}
