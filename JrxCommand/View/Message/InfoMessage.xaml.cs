
using System;
using System.Windows;
using System.Windows.Forms;
#if _IJCAD_
using Application = GrxCAD.ApplicationServices.Application;

#elif _AutoCAD_
using Application = Autodesk.AutoCAD.ApplicationServices.Core.Application;

#endif

namespace JrxCad.View.Message
{
    /// <summary>
    /// WindowMessage.xaml の相互作用ロジック
    /// </summary>
    public partial class InfoMessage
    {
        public string Title1 { get; set; }
        public string Title2 { get; set; }

        public string Message { get; set; }
        public string Detail { get; set; }

        private readonly Form _owner;
        private bool _isLoaded;

        public InfoMessage()
        {
            InitializeComponent();
        }

        public void ShowDialog(DependencyObject control)
        {
            var owner = GetWindow(this);
            Application.ShowModalWindow(owner, this, false);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Title = string.IsNullOrEmpty(Title2) 
                ? Title1 
                : $"{Title1} - {Title2}";

            txtMessage.Text = Message;

            if (!string.IsNullOrEmpty(Detail))
            {
                txtDetail.Text = Detail;
            }
        }

        
        /// <summary>
        /// Formが親の時
        /// </summary>
        /// <param name="owner">Form</param>
        public InfoMessage(Form owner)
        {
            InitializeComponent();

            _owner = owner;
        }

        /// <summary>
        /// Formが親の時のCenterParentの処理
        /// </summary>
        private void Window_LocationChanged(object sender, EventArgs e)
        {
            if (_owner != null && !_isLoaded)
            {
                Left = _owner.Left + (_owner.Width - ActualWidth) / 2;
                Top = _owner.Top + (_owner.Height - ActualHeight) / 2;
            }
        }

        /// <summary>
        /// 画面が完全に初期化された後に発生
        /// </summary>
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            _isLoaded = true;
        }
    }
}
