﻿using System.Windows.Controls;

namespace JrxCad.View.CustomWpf
{
    public class CustomButtonWpf : Button
    {
        public string TooltipText { get; set; }

        protected override void OnClick()
        {
            if (string.IsNullOrEmpty(TooltipText))
            {
                this.ToolTip = TooltipText;
            }
            base.OnClick();
        }
    }
}
