﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using CADColors = GrxCAD.Colors;
using CADUtils = GrxCAD2.Internal.Utils;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using CADColors = Autodesk.AutoCAD.Colors;
using CADPreferences = Autodesk.AutoCAD.Interop.AcadPreferences;
using CADUtils = Autodesk.AutoCAD.Internal.Utils;

#endif
using System;
using System.Drawing;
using System.Windows;
using JrxCad.Utility;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using JrxCad.Helpers;
using JrxCad.Resources.Common;

namespace JrxCad.View.CustomWpf
{
    internal abstract class Preview
    {
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }

        protected Color GetBackgroundColor()
        {
#if _AutoCAD_
            var preference = (CADPreferences)CADApp.Preferences;
            var backgroundColor = preference.Display.GraphicsWinModelBackgrndColor;
            return Color.FromArgb(
                (int)(backgroundColor & 0x000000FF),
                (int)((backgroundColor & 0x0000FF00) >> 8),
                (int)((backgroundColor & 0x00FF0000) >> 16)
            );
#else
            return Color.FromArgb(255, 255, 255);
#endif
        }

        [DllImport("gdi32.dll")]
        private static extern int DeleteObject(IntPtr hObject);

        protected BitmapSource ToBitmapSource(Bitmap bitmap)
        {
            var hBitmap = IntPtr.Zero;
            try
            {
                hBitmap = bitmap.GetHbitmap();
                return Imaging.CreateBitmapSourceFromHBitmap(
                    hBitmap,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());
            }
            finally
            {
                DeleteObject(hBitmap);
            }
        }
    }

    internal class BlockImage : Preview
    {
        public Color Background { get; set; }

        public BlockImage()
        {
            Background = GetBackgroundColor();
            ImageWidth = 96;
            ImageHeight = 96;
        }

        public Bitmap Create(BlockTableRecord blockDef, bool addIcon)
        {
            try
            {
                if (blockDef == null) return null;
                var hBitmap = CADUtils.GetBlockImage(blockDef.Id, ImageWidth, ImageHeight, CADColors.Color.FromColor(Background));
                if (hBitmap == IntPtr.Zero) return null;
                var bitmap = System.Drawing.Image.FromHbitmap(hBitmap);
                return addIcon ? AddIcon(bitmap, blockDef) : bitmap;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
                return null;
            }
        }

        public BitmapSource CreateWpf(BlockTableRecord blockDef, bool addIcon)
        {
            var bitmap = Create(blockDef, addIcon);
            return bitmap == null ? null : ToBitmapSource(bitmap);
        }

        private Bitmap AddIcon(Bitmap bitmap, BlockTableRecord blockDef)
        {
            using (var graphics = Graphics.FromImage(bitmap))
            {
                var isAnnotative = blockDef.XData != null &&
                    blockDef.GetXDataForApplication("AcadAnnotative") != null;

                if (isAnnotative)
                {
                    graphics.DrawIcon(CommonRes.DiffScale,
                        0,
                        bitmap.Height - CommonRes.DiffScale.Height);
                }
                if (blockDef.IsDynamicBlock)
                {
                    graphics.DrawIcon(CommonRes.DiffScale,
                        bitmap.Width - CommonRes.DiffScale.Width,
                        bitmap.Height - CommonRes.DiffScale.Height);
                }
                return bitmap;
            }
        }
    }

    //TODO Preview DWG
    internal class DwgImage : Preview
    {
        public Color Background { get; set; }
        public double ZoomFactor { get; set; }
        public Extents ImageExtents { get; set; }

        public enum Extents
        {
            Current,     //現在の表示範囲
            ItemExtent //対象図形の表示範囲
        }

        public DwgImage()
        {
            Background = GetBackgroundColor();
            ImageExtents = Extents.ItemExtent;
        }

        /// <summary>
        /// プレビューの作成
        /// </summary>
        public Bitmap Create(EntityEx entityEx, [Optional] Extents3d? extents)
        {
            var blockId = ObjectId.Null;

            var curIndex = 1;
            var blockName = "Temp" + curIndex;

            using (var tr = Util.StartTransaction())
            {
                var bt = tr.GetObject(Util.Database().BlockTableId, OpenMode.ForRead) as BlockTable;

                while (bt.Has(blockName))
                {
                    curIndex++;
                    blockName = "Temp" + curIndex;
                }

                bt.UpgradeOpen();
                //create new
                using (var record = new BlockTableRecord())
                {
                    record.Name = blockName;

                    bt.Add(record);
                    tr.AddNewlyCreatedDBObject(record, true);

                    blockId = bt[blockName];

                    foreach (ObjectId objectId in entityEx.GetObjectIds())
                    {
                        var ent = tr.GetObject<Entity>(objectId, OpenMode.ForRead);
                        var entityCopy = ent.Clone() as Entity;
                        record.AppendEntity(entityCopy);
                    }

                    try
                    {
                        var bitmap = CADUtils.GetBlockImage(blockId, ImageWidth, ImageHeight, CADColors.Color.FromColor(Background));
                        if (bitmap == IntPtr.Zero) return null;

                        return bitmap == IntPtr.Zero ? null : System.Drawing.Image.FromHbitmap(bitmap);
                    }
                    catch (System.Exception ex)
                    {
                        Logger.ErrorShow(ex.ToString());
                        return null;
                    }
                }
            }
        }
        /// <summary>
        /// DWGのサムネイルから WPF用プレビュー作成
        /// </summary>
        public BitmapSource Create(Bitmap dwgThumbnail)
        {
            return ToBitmapSource(dwgThumbnail);
        }
    }
}
