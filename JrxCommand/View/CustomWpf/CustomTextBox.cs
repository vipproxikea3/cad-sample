﻿#if _IJCAD_
#elif _AutoCAD_
#endif
using System.Windows;
using System.Windows.Controls;

namespace JrxCad.View.CustomWpf
{
    public class CustomTextBox : TextBox
    {
        public CustomTextBox()
        {
            VerticalContentAlignment = VerticalAlignment.Center;
        }
        protected override void OnGotFocus(RoutedEventArgs e)
        {
            base.OnGotFocus(e);
            var txt = e.OriginalSource as TextBox;
            txt.SelectAll();
        }
    }
}
