#if _IJCAD_
using CadApp = GrxCAD.ApplicationServices.Application;

#elif _AutoCAD_
using CadApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.Windows;

#endif
using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Media;
using JrxCad.Utility;
using Button = System.Windows.Controls.Button;
using Exception = System.Exception;
using Window = System.Windows.Window;

namespace JrxCad.View.CustomWpf
{
    public class CustomWindow : Window
    {
        private IntPtr _windowHandle;

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private const int GwlStyle = -16;

        private const int WsMaximizeBox = 0x10000; //maximize button
        private const int WsMinimizeBox = 0x20000; //minimize button
        private const string WBlockWindowName = "WBlockWindow";
        private const string XopenWindowName = "XopenWindow";

        public bool IsHorizontalScale { get; set; } = false;
        public bool IsVerticalScale { get; set; } = false;
        public CustomWindow()
        {
            try
            {
                SourceInitialized += SourceInitializedWindow;
                FontSize = 12;
                FontFamily = new FontFamily("Arial");
                ShowInTaskbar = false;

                //色調を鮮明にする
                UseLayoutRounding = true;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        public new bool? ShowDialog()
        {
            return CadApp.ShowModalWindow(CadApp.MainWindow.Handle, this, false);
        }

        /// <summary>
        /// FormでいうPerformClick
        /// </summary>
        public void PerformClick(Button btn)
        {
            var peer = new ButtonAutomationPeer(btn);
            if (peer.GetPattern(PatternInterface.Invoke) is IInvokeProvider provider)
            {
                provider.Invoke();
            }
        }

        private void SourceInitializedWindow(object sender, EventArgs e)
        {
            if (!IsHorizontalScale && !IsVerticalScale)
            {
                ResizeMode = ResizeMode.NoResize;
            }
            else
            {
                _windowHandle = new WindowInteropHelper(this).Handle;
                HideMinimizeAndMaximizeButtons();
                if (!IsHorizontalScale)
                {
                    MinWidth = MaxWidth = ActualWidth;
                }

                if (!IsVerticalScale)
                {
                    MinHeight = MaxHeight = ActualHeight;
                }

                if (Name != WBlockWindowName && Name != XopenWindowName) MinWidth = ActualWidth;
                if (Name != XopenWindowName) MinHeight = ActualHeight;
            }
        }

        protected void DisableMinimizeButton()
        {
            if (_windowHandle == IntPtr.Zero) return;

            _ = SetWindowLong(_windowHandle, GwlStyle, GetWindowLong(_windowHandle, GwlStyle) & ~WsMinimizeBox);
        }

        protected void DisableMaximizeButton()
        {
            if (_windowHandle == IntPtr.Zero) return;

            _ = SetWindowLong(_windowHandle, GwlStyle, GetWindowLong(_windowHandle, GwlStyle) & ~WsMaximizeBox);
        }

        protected void HideMinimizeAndMaximizeButtons()
        {
            if (_windowHandle == IntPtr.Zero) return;

            _ = SetWindowLong(_windowHandle, GwlStyle, GetWindowLong(_windowHandle, GwlStyle) & ~WsMaximizeBox & ~WsMinimizeBox);
        }

        //Center dialog to owner.
        public void CenterToOwner()
        {
            try
            {
                Point PixelsToPoints(System.Drawing.Size size)
                {
                    var x = size.Width * SystemParameters.WorkArea.Width / Screen.PrimaryScreen.WorkingArea.Width;
                    var y = size.Height * SystemParameters.WorkArea.Height / Screen.PrimaryScreen.WorkingArea.Height;
                    return new Point(x, y);
                }
#if _AutoCAD_
                var mainWinPos = CadApp.MainWindow.GetLocation();
                var mainWindowSize = CadApp.MainWindow.GetSize();
#elif _IJCAD_
                var mainWinPos = CadApp.MainWindow.Location;
                var mainWindowSize = CadApp.MainWindow.Size;
#endif
                var mainActualWinPos = PixelsToPoints(new System.Drawing.Size(mainWinPos.X, mainWinPos.Y));
                var mainActualWindowSize = PixelsToPoints(mainWindowSize);
                Left = mainActualWinPos.X + (mainActualWindowSize.X - ActualWidth) / 2;
                Top = mainActualWinPos.Y + (mainActualWindowSize.Y - ActualHeight) / 2;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }
    }
}
