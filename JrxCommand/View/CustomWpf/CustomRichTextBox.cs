﻿
#if _IJCAD_
using GrxCAD.DatabaseServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
#endif
using JrxCad.Command.AttEdit;
using JrxCad.Utility;
using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace JrxCad.View.CustomWpf
{
    public class CustomRichTextBox : RichTextBox
    {
        public CustomRichTextBox()
        {
        }

        [DllImport("AcFdUi.arx", CallingConvention = CallingConvention.Cdecl, EntryPoint = "?AcFdUiInvokeFieldDialog@@YAHAEAPEAVAcDbField@@HPEAVAcDbDatabase@@PEAVCWnd@@@Z")]
        private static extern Int32 InvokeFieldDialog(ref IntPtr fd, Int32 bEdit, IntPtr pDb, IntPtr pParent);

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            MouseRightButtonUp += (obj, args) =>
            {
                AddMenuItem(obj, args);
            };
        }

        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            base.OnGotKeyboardFocus(e);
            var txt = e.OriginalSource as CustomRichTextBox;
            txt.SelectAll();
        }

        public static class MenuHeader
        {
            public const string Undo = "Undo";
            public const string Cut = "Cut";
            public const string Copy = "Copy";
            public const string Paste = "Paste";
            public const string Delete = "Delete";
            public const string InsertField = "Insert Field...";
            public const string EditField = "Edit Field...";
            public const string UpdateField = "Update Field";
            public const string ConvertFieldToText = "Convert Field To Text";
            public const string SelectAll = "Select All";
            public const string Separator = "-";
        }

        private Field _currentField = null;
        private TextPointer _caret;
        private readonly ContextMenu _contextMenu = new ContextMenu();

        public object[] IndexField = null;
        private int _currentStart = 0;
        private int _oldTxtDLength = -1;
        private bool _isEditField = false;
        private bool _isHighLight = false;

        public static readonly DependencyProperty AttributeProperty =
  DependencyProperty.Register(
  "Attribute", typeof(FormAttEdit.AttributeItem), typeof(CustomRichTextBox), new PropertyMetadata(new FormAttEdit.AttributeItem(), OnAttributeChanged)
  );

        public FormAttEdit.AttributeItem Attribute
        {
            get { return (FormAttEdit.AttributeItem)GetValue(AttributeProperty); }
            set { SetValue(AttributeProperty, value); }
        }

        public static void OnAttributeChanged(DependencyObject doj, DependencyPropertyChangedEventArgs dp)
        {
            var currentTxt = (CustomRichTextBox)doj;
            if (currentTxt.Attribute == null) return;
            currentTxt.AppendText(currentTxt.Attribute.TextString);
            currentTxt.Document.PageWidth = 1000;
            if (currentTxt.Attribute?.FieldList == null) return;
            currentTxt.SetFieldIndexAndBackColor();
        }

        public void SetFieldIndexAndBackColor()
        {
            var text = Attribute.AttRef.getTextWithFieldCodes();
            int index = 0, nStartPos = -1, nEndPos = -1;
            IndexField = new object[Attribute.FieldList.Length];

            while (Field.FindField(text, 0, ref nStartPos, ref nEndPos))
            {
                var field = Attribute.FieldList[index];
                var value = field.GetStringValue();

                var escape = text.Substring(nStartPos, nEndPos - nStartPos + 1);
                var reg = new Regex(Regex.Escape(escape));
                text = reg.Replace(text, value, 1);

                var indexField = new { Start = nStartPos, End = nStartPos + value.Length };
                IndexField[index] = indexField;
                index++;
                if (index >= Attribute.FieldList.Count()) break;
            }
            Attribute.TextString = text;
            Document.Blocks.Clear();
            AppendText(text);
            for (var i = 0; i < IndexField.Length; i++)
            {
                var startIndex = (int)GetValObjDy(IndexField[i], "Start");
                var endIndex = (int)GetValObjDy(IndexField[i], "End");
                HighlightField(startIndex, endIndex, color: Brushes.LightGray);
            }
        }

        private void EditField()
        {
            _isEditField = true;
            var ptr = _currentField.UnmanagedObject;
            InvokeFieldDialog(ref ptr, 1, Util.CurDoc().Database.UnmanagedObject, IntPtr.Zero);
            if (!_isEditField) return;
            SetFieldIndexAndBackColor();
            Select(_currentStart, _currentField.GetStringValue().Length + _currentStart);
            _isEditField = false;
        }

        private void HighlightField(int startIndex, int endIndex, SolidColorBrush color)
        {
            _isHighLight = true;
            var start = GetPositionAtCharOffset(startIndex);
            var end = GetPositionAtCharOffset(endIndex);
            var range = new TextRange(start, end);
            range.ApplyPropertyValue(TextElement.BackgroundProperty, color);
            _isHighLight = false;
        }

        private TextPointer GetPositionAtCharOffset(int position)
        {
            var contentStart = Document.ContentStart;
            var i = 0;
            while (contentStart != null)
            {
                var text = new TextRange(contentStart, contentStart.GetPositionAtOffset(i, LogicalDirection.Forward)).Text;
                if (text.Length == position)
                    break;
                i++;
                if (contentStart.GetPositionAtOffset(i, LogicalDirection.Forward) == null)
                    return contentStart.GetPositionAtOffset(i - 1, LogicalDirection.Forward);
            }
            contentStart = contentStart.GetPositionAtOffset(i, LogicalDirection.Forward);
            return contentStart;
        }

        private string GetText()
        {
            return new TextRange(Document.ContentStart, Document.ContentEnd).Text.Replace("\r\n", "");
        }

        private object GetValObjDy(object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName).GetValue(obj, null);
        }

        private void Select(int start, int end)
        {
            var startPointer = GetPositionAtCharOffset(start);
            var endPointer = GetPositionAtCharOffset(end);
            Selection.Select(startPointer, endPointer);
        }

        private int GetIndexInText(bool isMouse)
        {
            _caret = isMouse ? GetPositionFromPoint(Mouse.GetPosition(this), true) : CaretPosition;
            var indexInText = -1;
            if (_caret != null)
            {
                var range = new TextRange(Document.ContentStart, _caret);
                indexInText = range.Text.Length;
            }
            return indexInText;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            var indexInText = GetIndexInText(isMouse: true);
            if (e.ClickCount == 2)
            {
                OnMouseDoubleClick(indexInText);
                return;
            }
            if (Attribute != null && Attribute.HasFields)
            {
                for (var i = 0; i < IndexField.Length; i++)
                {
                    var start = (int)GetValObjDy(IndexField[i], "Start");
                    var end = (int)GetValObjDy(IndexField[i], "End");
                    if (indexInText >= start &&
                        indexInText <= end)
                    {
                        Select(start, end);
                        break;
                    }
                }
            }
        }

        private void OnMouseDoubleClick(int index)
        {
            if (Attribute != null && Attribute.FieldList != null)
            {
                _currentField = null;
                for (var i = 0; i < IndexField.Length; i++)
                {
                    var start = (int)GetValObjDy(IndexField[i], "Start");
                    var end = (int)GetValObjDy(IndexField[i], "End");

                    if (start <= index && index <= end)
                    {
                        _currentField = Attribute.FieldList[i];
                        _currentStart = start;
                        Select(start, end);
                        break;
                    }

                    if (index < start)
                    {
                        Select(0, end);
                        break;
                    }

                    if (i == IndexField.Length - 1 && index > start)
                    {
                        var text = GetText();
                        Select(start, text.Length);
                        break;
                    }

                    if (i + 1 < IndexField.Length)
                    {
                        var nextStart = (int)GetValObjDy(IndexField[i + 1], "Start");
                        var nextEnd = (int)GetValObjDy(IndexField[i + 1], "End");
                        if (index > end && index < nextStart)
                        {
                            Select(start, nextEnd);
                            break;
                        }
                    }
                }

                if (_currentField != null)
                {
                    Attribute.AttRef.TextString = GetText();
                    EditField();
                }
            }
        }

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            base.OnTextChanged(e);
            if (Attribute != null && Attribute.HasFields && IndexField != null && !_isEditField)
            {
                var indexInText = GetIndexInText(isMouse: false);
                var text = GetText();
                if (text == "")
                {
                    return;
                }
                for (var i = 0; i < IndexField.Length; i++)
                {
                    var start = (int)GetValObjDy(IndexField[i], "Start");
                    var end = (int)GetValObjDy(IndexField[i], "End");

                    if (_oldTxtDLength != -1 && _oldTxtDLength < text.Length && indexInText <= start + 1)
                    {
                        if (_isHighLight)
                        {
                            return;
                        }
                        IndexField[i] = new { Start = start + 1, End = end + 1 };
                        start++;
                        end++;
                    }

                    if (_oldTxtDLength > text.Length && indexInText <= start && start != 0)
                    {
                        IndexField[i] = new { Start = start - 1, End = end - 1 };
                        start--;
                        end--;
                    }

                    if ((indexInText == start || indexInText ==
                        end + 1) && start != 0 && !_isHighLight)
                    {
                        HighlightField(indexInText - 1, indexInText, Brushes.White);
                    }

                }
                _oldTxtDLength = text.Length;
            }
        }

        protected override void OnSelectionChanged(RoutedEventArgs e)
        {
            base.OnSelectionChanged(e);
            if (Attribute != null && Attribute.HasFields)
            {
                var text = GetText();
                if (text.Length == _oldTxtDLength)
                {
                    var indexInText = GetIndexInText(isMouse: false);
                    for (var i = 0; i < IndexField.Length; i++)
                    {
                        var start = (int)GetValObjDy(IndexField[i], "Start");
                        var end = (int)GetValObjDy(IndexField[i], "End");
                        if (indexInText == start + 1)
                        {
                            Thread.Sleep(200);
                            Select(end, end);
                            return;
                        }
                        if (indexInText == end - 1)
                        {
                            Thread.Sleep(200);
                            Select(start, start);
                            return;
                        }
                        if (indexInText > start &&
                        indexInText < end)
                        {
                            Select(start, end);
                            return;
                        }
                    }
                }
            }
        }

        private void AddMenuItem(object sender, MouseButtonEventArgs e)
        {
            _contextMenu.Items.Clear();
            var isField = false;
            if (IndexField != null)
            {
                var indexInText = GetIndexInText(isMouse: true);
                for (var i = 0; i < IndexField.Length; i++)
                {
                    var start = (int)GetValObjDy(IndexField[i], "Start");
                    var end = (int)GetValObjDy(IndexField[i], "End");
                    if (start <= indexInText && indexInText <= end)
                    {
                        isField = true;
                        _currentField = Attribute.FieldList[i];
                        _currentStart = start;
                        break;
                    }
                }
            }

            AddControlMenuItem(MenuHeader.Undo);
            AddControlMenuItem(MenuHeader.Separator);
            AddControlMenuItem(MenuHeader.Cut);
            AddControlMenuItem(MenuHeader.Copy);
            AddControlMenuItem(MenuHeader.Paste);
            AddControlMenuItem(MenuHeader.Delete);
            AddControlMenuItem(MenuHeader.Separator);
            if (!isField)
            {
                AddControlMenuItem(MenuHeader.InsertField);
            }
            else
            {
                AddControlMenuItem(MenuHeader.EditField);
                AddControlMenuItem(MenuHeader.UpdateField);
                AddControlMenuItem(MenuHeader.ConvertFieldToText);
            }
            AddControlMenuItem(MenuHeader.Separator);
            AddControlMenuItem(MenuHeader.SelectAll);

            _contextMenu.IsOpen = true;
        }

        private void AddControlMenuItem(string header)
        {
            var menuItem = new MenuItem();
            switch (header)
            {
                case MenuHeader.Undo:
                    menuItem.Header = MenuHeader.Undo;
                    menuItem.IsEnabled = CanUndo;
                    break;
                case MenuHeader.Cut:
                    menuItem.Header = MenuHeader.Cut;
                    menuItem.IsEnabled = Selection.Text.Length != 0;
                    break;
                case MenuHeader.Copy:
                    menuItem.Header = MenuHeader.Copy;
                    menuItem.IsEnabled = Selection.Text.Length != 0;
                    break;
                case MenuHeader.Paste:
                    menuItem.Header = MenuHeader.Paste;
                    menuItem.IsEnabled = Clipboard.ContainsText();
                    break;
                case MenuHeader.Delete:
                    menuItem.Header = MenuHeader.Delete;
                    menuItem.IsEnabled = Selection.Text.Length != 0;
                    break;
                case MenuHeader.InsertField:
                    menuItem.Header = MenuHeader.InsertField;
                    break;
                case MenuHeader.EditField:
                    menuItem.Header = MenuHeader.EditField;
                    break;
                case MenuHeader.UpdateField:
                    menuItem.Header = MenuHeader.UpdateField;
                    break;
                case MenuHeader.ConvertFieldToText:
                    menuItem.Header = MenuHeader.ConvertFieldToText;
                    break;
                case MenuHeader.SelectAll:
                    menuItem.Header = MenuHeader.SelectAll;
                    var text = GetText();
                    menuItem.IsEnabled = text.Length != 0;
                    break;
                case MenuHeader.Separator:
                    _contextMenu.Items.Add(new Separator());
                    return;
                default:
                    break;
            }

            menuItem.Click += (o, e) =>
            {
                switch (header)
                {
                    case MenuHeader.Undo:
                        ExecuteUndo(o, e);
                        break;
                    case MenuHeader.Cut:
                        ExecuteCut(o, e);
                        break;
                    case MenuHeader.Copy:
                        ExecuteCopy(o, e);
                        break;
                    case MenuHeader.Paste:
                        ExecutePaste(o, e);
                        break;
                    case MenuHeader.Delete:
                        ExecuteDelete(o, e);
                        break;
                    case MenuHeader.InsertField:
                        ExecuteInsertField(o, e);
                        break;
                    case MenuHeader.EditField:
                        ExecuteEditField(o, e);
                        break;
                    case MenuHeader.UpdateField:
                        ExecuteUpdateField(o, e);
                        break;
                    case MenuHeader.ConvertFieldToText:
                        ExecuteConvertFieldToText(o, e);
                        break;
                    case MenuHeader.SelectAll:
                        ExecuteSelectAll(o, e);
                        break;
                    default:
                        break;
                }
            };
            _contextMenu.Items.Add(menuItem);
        }

        #region menu event
        private void ExecuteUndo(object sender, EventArgs e)
        {
            Undo();
        }
        private void ExecuteCut(object sender, EventArgs e)
        {
            if (Selection.Text != "")
            {
                Cut();
            }
        }
        private void ExecuteCopy(object sender, EventArgs e)
        {
            if (Selection.Text.Length > 0)
            {
                Copy();
            }
        }
        private void ExecutePaste(object sender, EventArgs e)
        {
            Paste();
        }
        private void ExecuteDelete(object sender, EventArgs e)
        {
        }
        private void ExecuteInsertField(object sender, EventArgs e)
        {
        }

        private void ExecuteEditField(object sender, EventArgs e)
        {
            EditField();
        }
        private void ExecuteUpdateField(object sender, EventArgs e)
        {

        }
        private void ExecuteConvertFieldToText(object sender, EventArgs e)
        {
        }

        private void ExecuteSelectAll(object sender, EventArgs e)
        {
            SelectAll();
        }
        #endregion

    }
}
