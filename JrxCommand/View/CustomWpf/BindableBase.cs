#if _IJCAD_

#elif _AutoCAD_

#endif

using JrxCad.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

// ReSharper disable once IdentifierTypo
namespace JrxCad.View.CustomWpf
{
    public class BindableBase : INotifyPropertyChanged
    {
        // ReSharper disable once UnassignedGetOnlyAutoProperty
        protected virtual string HelpName { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual bool SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            return SetProperty(ref field, value, null, propertyName);
        }

        protected virtual bool SetProperty<T>(ref T field, T value, Action onChanged, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
            {
                return false;
            }

            field = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public class RelayCommand<T> : ICommand
        {
            private readonly Predicate<T> _canExecute;
            private readonly Action<T> _execute;

            public RelayCommand(Predicate<T> canExecute, Action<T> execute)
            {
                _canExecute = canExecute;
                _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            }

            public bool CanExecute(object parameter)
            {
                try
                {
                    return _canExecute == null || _canExecute((T)parameter);
                }
                catch
                {
                    return true;
                }
            }

            public void Execute(object parameter)
            {
                _execute((T)parameter);
            }

            public event EventHandler CanExecuteChanged
            {
                add => CommandManager.RequerySuggested += value;
                remove => CommandManager.RequerySuggested -= value;
            }
        }

        public class ActionCommand<T> : ICommand
        {
            public event EventHandler CanExecuteChanged;
            private readonly Action<T> _action;

            public ActionCommand(Action<T> action)
            {
                _action = action;
            }

            public bool CanExecute(object parameter) { return true; }

            public void Execute(object parameter)
            {
                if (_action != null)
                {
                    var castParameter = (T)Convert.ChangeType(parameter, typeof(T));
                    _action(castParameter);
                }
            }
        }

        public void OpenHelpWindow()
        {
            if (!string.IsNullOrEmpty(HelpName))
            {
                Util.DoHelpForCommand(HelpName);
            }
        }
    }
}
