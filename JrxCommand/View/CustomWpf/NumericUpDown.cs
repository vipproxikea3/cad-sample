using JrxCad.Utility;
using System;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using JrxCad.Helpers;
using System.Text.RegularExpressions;
using JrxCad.Resources.CustomControl;

#if _IJCAD_
using Runtime = GrxCAD.Runtime;
#elif _AutoCAD_
using Runtime = Autodesk.AutoCAD.Runtime;
#endif

namespace JrxCad.View.CustomWpf
{
    public delegate void ValidatedEventHandler(object sender, bool result, string message);
    #region Enums
    /// <summary>
    /// Numeric display & input type
    /// </summary>
    public enum NumericInputType
    {
        Common = 0,
        Point = 1,
        Distance = 2,
        Angle = 3,
        Scale = 4
    }

    /// <summary>
    /// Angle bound values
    /// </summary>
    public enum AngleMinMax
    {
        MinValue = -360,
        MaxValue = 360
    }
    #endregion

    /// <summary>
    /// Class for define textbox with Numeric up/down button
    /// </summary>
    public class NumericUpDown : UserControl
    {
        #region Fields
        private int _delay = 200;
        private TextBox _txtInput;
        private readonly NumericStyleConverter _styleConverter = new NumericStyleConverter();
        public event EventHandler ValueChanged;
        public event ValidatedEventHandler OnValidated;
        #endregion

        #region Initialization
        /// <summary>
        /// Constructor
        /// </summary>
        public NumericUpDown()
        {
            InitializeComponent();

            Focusable = true;
            PreviewGotKeyboardFocus += NumericUpDown_PreviewGotKeyboardFocus;

            SizeChanged += (obj, evt) =>
            {
                if (Minimum < DefaultMinimum) Minimum = DefaultMinimum;
                if (Maximum > DefaultMaximum) Maximum = DefaultMaximum;

                switch (InputType)
                {
                    case NumericInputType.Point:
                    case NumericInputType.Distance:
                        Increment = 5 * Math.Pow(10, -Util.Luprec());
                        DecimalPlaces = Util.Luprec();
                        break;
                    case NumericInputType.Angle:
                        Increment = 5 * Math.Pow(10, -Util.Auprec());
                        DecimalPlaces = Util.Auprec();
                        break;
                    case NumericInputType.Scale:
                        Increment = 5 * Math.Pow(10, -Util.Luprec());
                        DecimalPlaces = Util.Luprec();
                        break;
                }

                _txtInput.Text = _styleConverter.Convert(Value, null, new object[] { InputType, DecimalPlaces }, null)?.ToString() ?? string.Empty;

                var minValue = _styleConverter.Convert(Minimum, null, new object[] { InputType, DecimalPlaces }, null)?.ToString();
                var maxValue = _styleConverter.Convert(Maximum, null, new object[] { InputType, DecimalPlaces }, null)?.ToString();
                if (ToolTip == null)
                {
                    ToolTip = new ToolTip
                    {
                        Tag = string.Format(CustomControl.NumericUpDown_LimitValue, minValue, maxValue)
                    };
                }
                else
                {
                    ((ToolTip)ToolTip).Tag =
                        string.Format(CustomControl.NumericUpDown_LimitValue, minValue, maxValue);
                }

                // Set binding properties
                _txtInput.SetBinding(TextBox.TextProperty, new Binding("Value")
                {
                    Source = this,
                    Mode = BindingMode.TwoWay,
                    UpdateSourceTrigger = UpdateSourceTrigger.LostFocus,
                    Converter = _styleConverter,
                    ConverterParameter = new object[] { InputType, DecimalPlaces }
                });
                _txtInput.SetBinding(TextBox.TextAlignmentProperty, new Binding("TextAlignment")
                {
                    Source = this,
                    Mode = BindingMode.TwoWay,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                });
                _txtInput.SetBinding(Control.TabIndexProperty, new Binding("TabIndex")
                {
                    Source = this,
                    Mode = BindingMode.TwoWay,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                });
                _txtInput.LostFocus += TxtInput_LostFocus;
            };
        }

        /// <summary>
        /// Initialize control layout
        /// </summary>
        private void InitializeComponent()
        {
            MinHeight = 22;
            Height = 22;
            BorderBrush = Brushes.Gray;
            BorderThickness = new Thickness(1);

            var grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(22) });

            // Textbox
            _txtInput = new TextBox
            {
                FontWeight = FontWeights.Medium,
                //FontSize = 14,
                VerticalContentAlignment = VerticalAlignment.Center,
                BorderThickness = new Thickness(0)
            };
            _txtInput.GotFocus += TxtInput_GotFocus;
            _txtInput.MouseWheel += TxtInput_MouseWheel;
            _txtInput.PreviewKeyDown += TxtInput_PreviewKeyDown;
            _txtInput.PreviewKeyUp += TxtInput_PreviewKeyUp;
            Grid.SetColumn(_txtInput, 0);
            Grid.SetRowSpan(_txtInput, 2);
            Validation.SetErrorTemplate(_txtInput, null);

            // Up button
            var cmdUp = new RepeatButton
            {
                Content = new Path
                {
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Fill = Brushes.Black,
                    Data = Geometry.Parse("M4,0 L0,4 L1,4 L4,1 L7,4 L8,4 z")
                },
                BorderThickness = new Thickness(0),
                VerticalContentAlignment = VerticalAlignment.Center,
                Background = Brushes.Transparent,

                IsTabStop = false
            };
            cmdUp.Click += CmdUp_Click;
            cmdUp.PreviewMouseLeftButtonUp += Cmd_MouseUp;
            Grid.SetColumn(cmdUp, 1);
            Grid.SetRow(cmdUp, 0);

            // Down button
            var cmdDown = new RepeatButton
            {
                Content = new Path
                {
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Fill = Brushes.Black,
                    Data = Geometry.Parse("M0,0 L1,0 L4,3 L7,0 L8,0 L4,4 z")
                },
                BorderThickness = new Thickness(0),
                VerticalContentAlignment = VerticalAlignment.Center,
                Background = Brushes.Transparent,

                IsTabStop = false
            };
            cmdDown.Click += CmdDown_Click;
            cmdDown.PreviewMouseLeftButtonUp += Cmd_MouseUp;
            Grid.SetColumn(cmdDown, 1);
            Grid.SetRow(cmdDown, 1);

            grid.Children.Add(_txtInput);
            grid.Children.Add(cmdUp);
            grid.Children.Add(cmdDown);

            AddChild(grid);
        }
        #endregion

        #region Properties

        #region TitleProperty
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(
            "Title",
            typeof(string),
            typeof(NumericUpDown),
            new PropertyMetadata(default(string)));

        /// <summary>
        /// Control Value
        /// </summary>
        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }
        #endregion

        #region ValueProperty
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            "Value",
            typeof(double),
            typeof(NumericUpDown),
            new FrameworkPropertyMetadata(default(double), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /// <summary>
        /// Control Value
        /// </summary>
        public double Value
        {
            get => (double)GetValue(ValueProperty);
            set
            {
                if (value < Minimum)
                    value = Minimum;
                if (value > Maximum)
                    value = Maximum;
                value = Math.Round(value, DecimalPlaces);
                SetValue(ValueProperty, value);
                ValueChanged?.Invoke(this, EventArgs.Empty);
                _txtInput.Text = _styleConverter.Convert(Value, null, new object[] { InputType, DecimalPlaces }, null)?.ToString() ?? string.Empty;
            }
        }
        #endregion

        #region IncrementProperty
        public static readonly DependencyProperty IncrementProperty = DependencyProperty.Register(
            "Increment",
            typeof(double),
            typeof(NumericUpDown),
            new PropertyMetadata(0.1));

        /// <summary>
        /// Value Increment/decrement
        /// </summary>
        public double Increment
        {
            get => (double)GetValue(IncrementProperty);
            set => SetValue(IncrementProperty, value);
        }
        #endregion

        #region DecimalPlacesProperty
        public static readonly DependencyProperty DecimalPlacesProperty = DependencyProperty.Register(
            "DecimalPlaces",
            typeof(int),
            typeof(NumericUpDown),
            new PropertyMetadata(2));

        /// <summary>
        /// Number of numeric after ","
        /// </summary>
        public int DecimalPlaces
        {
            get => (int)GetValue(DecimalPlacesProperty);
            set => SetValue(DecimalPlacesProperty, value);
        }
        #endregion

        #region MinimumProperty
        public static readonly DependencyProperty MinimumProperty = DependencyProperty.Register(
            "Minimum",
            typeof(double),
            typeof(NumericUpDown),
            new PropertyMetadata(double.MinValue));

        /// <summary>
        /// Minimum value control can set
        /// </summary>
        public double Minimum
        {
            get => (double)GetValue(MinimumProperty);
            set
            {
                if (value > Maximum)
                    Maximum = value;
                SetValue(MinimumProperty, value);
            }
        }
        #endregion

        #region MaximumProperty
        public static readonly DependencyProperty MaximumProperty = DependencyProperty.Register(
            "Maximum",
            typeof(double),
            typeof(NumericUpDown),
            new PropertyMetadata(double.MaxValue));

        /// <summary>
        /// Maximum value control can set
        /// </summary>
        public double Maximum
        {
            get => (double)GetValue(MaximumProperty);
            set
            {
                if (value < Minimum)
                    value = Minimum;
                SetValue(MaximumProperty, value);
            }
        }
        #endregion

        #region TextAligment Properties
        public static readonly DependencyProperty TextAlignmentProperty = DependencyProperty.Register(
            "TextAlignment",
            typeof(TextAlignment),
            typeof(NumericUpDown),
            new FrameworkPropertyMetadata(TextAlignment.Left));

        /// <summary>
        /// Textbox text display alignment
        /// </summary>
        public TextAlignment TextAlignment
        {
            get => (TextAlignment)GetValue(TextAlignmentProperty);
            set => SetValue(TextAlignmentProperty, value);
        }
        #endregion

        #region TabIndexProperty
        public new static readonly DependencyProperty TabIndexProperty = DependencyProperty.Register(
            "TabIndex",
            typeof(int),
            typeof(NumericUpDown),
            new PropertyMetadata(default(int)));

        /// <summary>
        /// Value Increment/decrement
        /// </summary>
        public new int TabIndex
        {
            get => (int)GetValue(TabIndexProperty);
            set => SetValue(TabIndexProperty, value);
        }
        #endregion

        #region ValidateExceptControlsProperty
        public static readonly DependencyProperty ValidateExceptControlsProperty = DependencyProperty.Register(
            "ValidateExceptControls",
            typeof(string),
            typeof(NumericUpDown),
            new PropertyMetadata(default(string)));

        /// <summary>
        /// Control Value
        /// </summary>
        public string ValidateExceptControls
        {
            get => (string)GetValue(ValidateExceptControlsProperty);
            set => SetValue(ValidateExceptControlsProperty, value);
        }
        #endregion

        #region Protected Properties
        protected double DefaultMinimum { get; set; } = double.MinValue;
        protected double DefaultMaximum { get; set; } = double.MaxValue;
        protected NumericInputType InputType { get; set; } = NumericInputType.Common;
        #endregion

        #endregion

        #region Events
        /// <summary>
        /// Preview got focus event
        /// </summary>
        private void NumericUpDown_PreviewGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (Equals(e.NewFocus, this))
            {
                _txtInput.Focus();
                e.Handled = true;
            }
        }

        /// <summary>
        /// Button up clicked event handler
        /// </summary>
        private void CmdUp_Click(object sender, RoutedEventArgs e)
        {
            Value += Increment;
            if (_delay > 10) _delay -= 10;
            Thread.Sleep(_delay);
        }

        /// <summary>
        /// Button down clicked event handler
        /// </summary>
        private void CmdDown_Click(object sender, RoutedEventArgs e)
        {
            Value -= Increment;
            if (_delay > 10) _delay -= 10;
            Thread.Sleep(_delay);
        }

        /// <summary>
        /// Up and down button release event
        /// </summary>
        private void Cmd_MouseUp(object sender, MouseButtonEventArgs e)
        {
            // Reset increase/decrease delay time
            _delay = 200;
        }

        /// <summary>
        /// Input text field got focus event
        /// </summary>
        private void TxtInput_GotFocus(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() => _txtInput.SelectAll()));
        }

        /// <summary>
        /// Input text field mouse wheel event
        /// </summary>
        private void TxtInput_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            Validate();
            if (e.Delta > 0)
            {
                Value += Increment;
            }
            else if (e.Delta < 0)
            {
                Value -= Increment;
            }
        }

        /// <summary>
        /// Arrow key up/down event
        /// </summary>
        private void TxtInput_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up || e.Key == Key.Down)
            {
                Validate();
                Value += e.Key == Key.Up ? Increment : Increment * -1;
                if (_delay > 10) _delay -= 10;
                Thread.Sleep(_delay);
            }
            if (e.Key == Key.Enter)
            {
                if (!Validate())
                {
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Textbox key up event handler
        /// </summary>
        private void TxtInput_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            _delay = 200;
        }

        /// <summary>
        /// Input text field lost focus, execute validate value
        /// </summary>
        private void TxtInput_LostFocus(object sender, RoutedEventArgs e)
        {
            Validate();
        }

        private bool Validate()
        {
            UIElement focusedElement = Keyboard.FocusedElement as UIElement;
#if _IJCAD_
            if (focusedElement == null)
            {
                return true;
            }
#endif
            var separators = new[] { ' ', ',' };
            string[] controls = ValidateExceptControls?.Split(separators);
            if (controls != null)
            {
                foreach (var control in controls)
                {
                    if (focusedElement.FindChildrenByName<Control>(control.Trim()) != null) return true;
                }
            }
            // Implement validation
            try
            {
                switch (InputType)
                {
                    case NumericInputType.Point:
                    case NumericInputType.Distance:
                        Value = Runtime.Converter.StringToDistance(_txtInput.Text);
                        break;
                    case NumericInputType.Angle:
                        using (new SystemVariable.Temp("ANGBASE", 0.0))
                        {
                            var radian = Runtime.Converter.StringToRawAngle(_txtInput.Text);
                            Value = radian.RadiansToDegrees();
                        }
                        break;
                    case NumericInputType.Common:
                    case NumericInputType.Scale:
                        Value = double.Parse(_txtInput.Text);
                        break;
                }
                return true;
            }
            catch (Exception)
            {
                var inputType = "";
                var valObject = "";
                switch (InputType)
                {
                    case NumericInputType.Common:
                        inputType = "double";
                        break;
                    case NumericInputType.Point:
                        inputType = "coordinates";
                        valObject = $"{Title}-coordinate";
                        break;
                    case NumericInputType.Distance:
                        inputType = "distance";
                        valObject = "Rows";
                        break;
                    case NumericInputType.Angle:
                        inputType = "angle";
                        valObject = "Angle";
                        break;
                    case NumericInputType.Scale:
                        inputType = "scale";
                        valObject = $"{Title}-scale";
                        break;
                }
                var msg = string.Format(CustomControl.NumericUpDown_InvalidValue_Msg, _txtInput.Text, inputType);
                var caption = string.Format(CustomControl.NumericUpDown_InvalidValue_Caption, valObject);
                if (OnValidated != null)
                {
                    OnValidated.Invoke(this, false, msg);
                }
                else
                {
                    MsgBox.Error.Show(msg, caption);
                }
                _ = Dispatcher.BeginInvoke(new Action(() => _txtInput.Focus()));
                return false;
            }
        }
        #endregion
    }

    #region Derived Classes
    /// <summary>
    /// Numeric control for point input field
    /// </summary>
    public class NumericPointControl : NumericUpDown
    {
        public new double Increment { get; }
        public new double DecimalPlaces { get; }
        public NumericPointControl()
        {
            Increment = 5 * Math.Pow(10, -Util.Luprec());
            DecimalPlaces = Util.Luprec();
            DefaultMinimum = double.MinValue;
            DefaultMaximum = double.MaxValue;
            InputType = NumericInputType.Point;
        }
    }

    /// <summary>
    /// Numeric control for distance input field
    /// </summary>
    public class NumericDistanceControl : NumericUpDown
    {
        public new double Increment { get; }
        public new double DecimalPlaces { get; }
        public NumericDistanceControl()
        {
            Increment = 5 * Math.Pow(10, -Util.Luprec());
            DecimalPlaces = Util.Luprec();
            DefaultMinimum = double.MinValue;
            DefaultMaximum = double.MaxValue;
            InputType = NumericInputType.Distance;
        }
    }

    /// <summary>
    /// Numeric control for angle input field
    /// </summary>
    public class NumericAngleControl : NumericUpDown
    {
        public new double Increment { get; }
        public new double DecimalPlaces { get; }
        public NumericAngleControl()
        {
            Increment = 5 * Math.Pow(10, -Util.Auprec());
            DecimalPlaces = Util.Auprec();
            DefaultMinimum = (int)AngleMinMax.MinValue;
            DefaultMaximum = (int)AngleMinMax.MaxValue;
            InputType = NumericInputType.Angle;
        }
    }

    /// <summary>
    /// Numeric control for scale input field
    /// </summary>
    public class NumericScaleControl : NumericUpDown
    {
        public new double Increment { get; }
        public new double DecimalPlaces { get; }
        public NumericScaleControl()
        {
            Increment = 5 * Math.Pow(10, -Util.Luprec());
            DecimalPlaces = Util.Luprec();
            DefaultMinimum = double.MinValue;
            DefaultMaximum = double.MaxValue;
            InputType = NumericInputType.Scale;
        }
    }
    #endregion

    #region Converter
    /// <summary>
    /// Value display converter class
    /// </summary>
    public class NumericStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                object[] typeAndPrec = (object[])parameter;
                if (typeAndPrec != null)
                {
                    NumericInputType inputType = (NumericInputType)typeAndPrec[0];
                    var prec = (int)typeAndPrec[1];
                    switch (inputType)
                    {
                        case NumericInputType.Point:
                        case NumericInputType.Distance:
                        case NumericInputType.Scale:
                            if (value != null)
                            {
                                return Runtime.Converter.DistanceToString((double)value,
                                    Runtime.DistanceUnitFormat.Current, prec);
                            }

                            return null;
                        case NumericInputType.Angle:
                            using (new SystemVariable.Temp("ANGBASE", 0.0))
                            {
                                if (value != null)
                                {
                                    var angle = (double)value;
                                    var radian = Runtime.Converter.StringToRawAngle(angle.ToString(CultureInfo.CurrentCulture));
                                    return Runtime.Converter.RawAngleToString(radian);
                                }
                                return null;
                            }

                        case NumericInputType.Common:
                            return value;
                    }
                }

                return value;
            }
            catch (Exception)
            {
                return DependencyProperty.UnsetValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
    #endregion
}
