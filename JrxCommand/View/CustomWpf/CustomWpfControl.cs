#if _IJCAD_

#elif _AutoCAD_

#endif

using System.Windows.Controls;
using System.Windows.Media;
using JrxCad.Utility;
using Color = System.Windows.Media.Color;

namespace JrxCad.View.CustomWpf
{
    public class CustomWpfControl : UserControl
    {
        public static Color Color1 = Colors.WhiteSmoke; //濃い
        public static Color Color2 = Colors.WhiteSmoke; //中間
        public static Color Color3 = Colors.WhiteSmoke; //薄い
        public static Color FontColor = Colors.Black;

        public CustomWpfControl()
        {
            //フォント ここでセットすれば全体のフォントを制御できるが、外しておく
            //FontFamily = new FontFamily("MS PGothic");
            //FontSize = 12.0;

            //色調を鮮明にする
            UseLayoutRounding = true;

            Foreground = new SolidColorBrush(Colors.White);
        }

        public virtual void SetColor()
        {
            //TODO:Templateで対応したほうがいいか？
            var colorTheme = SystemVariable.GetInt("ColorTheme");

            if (colorTheme == 1)
            {
                //ライト
                Color1 = Color.FromRgb(217, 217, 217);
                Color2 = Color.FromRgb(238, 238, 238);
                Color3 = Color.FromRgb(245, 245, 245);
                FontColor = Colors.Black;
            }
            else
            {
                //ダーク
                Color1 = Color.FromRgb(46, 52, 64);
                Color2 = Color.FromRgb(69, 79, 97);
                Color3 = Color.FromRgb(59, 68, 83);
                FontColor = Colors.White;
            }

        }
    }
}
