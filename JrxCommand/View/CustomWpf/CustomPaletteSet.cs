#if _IJCAD_
using GrxCAD.Windows;
using GrxCAD.ApplicationServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.Windows;
using Autodesk.AutoCAD.ApplicationServices;

#endif
using System;
using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.View.CustomWpf
{
    public class CustomPaletteSet : PaletteSet
    {
        /// <summary>
        /// 一時的にPGがパレットを非表示にする場合true
        /// </summary>
        public bool IsTemporaryHidden { get; set; }

        public CustomPaletteSet(string name, string cmd, Guid toolID) : base(name, cmd, toolID)
        {
            Initialize();
        }

        public CustomPaletteSet(string name, Guid toolID) : base(name, toolID)
        {
            Initialize();
        }

        public CustomPaletteSet(string name) : base(name)
        {
            Initialize();
        }

        /// <summary>
        /// 起動時に行う初期処理
        /// </summary>
        protected virtual void Initialize()
        {
#if _AutoCAD_
            Saving += OnSaving;
#endif
        }

        protected virtual void OnSaving(object sender, PalettePersistEventArgs e)
        {
        }

        /// <summary>
        /// カレント図面が変更された時に行われる処理
        /// </summary>
        public virtual void Update()
        {
        }

        /// <summary>
        /// カレント図面変更イベント取得時
        /// </summary>
        public void ChangeCurrentDrawing(Document doc)
        {
            try
            {
                if (doc == null)
                {
                    //スタートタブ選択時ここを通る
                    if (Visible)
                    {
                        Visible = false;
                        IsTemporaryHidden = true;
                    }
                    return;
                }

                if (Visible)
                {
                    //DWGタブからDWGタブに遷移した時
                    Update();
                }
                else if (IsTemporaryHidden)
                {
                    //スタートタブからDWGタブに遷移した時
                    Visible = true;
                    IsTemporaryHidden = false;
                    Update();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }
    }
}
