#if _IJCAD_

#elif _AutoCAD_

#endif

using System;
using System.Runtime.InteropServices;
using JrxCad.Utility;

namespace JrxCad.View
{
    /// <summary>
    /// 高解像度制御クラス
    /// </summary>
    public static class ScreenDpi
    {
        /// <summary>
        /// ピクセルサイズ
        /// </summary>
        private const int PixelsX = 88;

        [DllImport("user32.dll")]
        private static extern bool SetProcessDPIAware();

        [DllImport("user32.dll")]
        private static extern IntPtr GetWindowDC(IntPtr hwnd);

        [DllImport("gdi32.dll")]
        private static extern int GetDeviceCaps(IntPtr hdc, int index);

        [DllImport("user32.dll")]
        private static extern int ReleaseDC(IntPtr hwnd, IntPtr hdc);

        /// <summary>
        /// プロセス全体の高解像度対応
        /// </summary>
        /// <remarks>起動時に1回だけ実行</remarks>
        public static void Initialize()
        {
            try
            {
                SetProcessDPIAware();
                GetScale();
            }
            catch (Exception ex)
            {
                Logger.Warn(ex);
            }
        }

        private static void GetScale()
        {
            var dc = GetWindowDC(IntPtr.Zero);
            var pixelX = GetDeviceCaps(dc, PixelsX);
            ReleaseDC(IntPtr.Zero, dc);

            var scale = pixelX / (decimal)96.0;
            if (scale <= decimal.Zero) scale = 1;

            Logger.Notice($"DisplayScale:{scale * 100}% ({pixelX}/96)");
        }
    }
}
