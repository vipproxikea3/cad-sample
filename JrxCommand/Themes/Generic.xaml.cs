#if _IJCAD_

#elif _AutoCAD_

#endif

using System.Windows;
using System.Windows.Controls;

namespace JrxCad.Themes
{
    public partial class Generic
    {
        /// <summary>
        /// TextBoxにカーソルが遷移した時文字列全選択
        /// </summary>
        public void TextFocus(object sender, RoutedEventArgs e)
        {
            if (!(sender is TextBox textBox)) return;
            textBox.SelectAll();
        }
    }
}
