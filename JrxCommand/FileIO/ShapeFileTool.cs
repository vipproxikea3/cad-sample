﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.FileIO
{
    public class ShapeFileTool
    {
        #region データ関係
        public struct ShapeData
        {
            public UInt16 ShapeCode;
            public string Name;
            public byte[] Data;
        }

        public enum ShapeType
        {
            Undefined,
            Shape10,
            Shape11,
            UniFont10,
            BigFont10,
        }

        public ShapeType Kind { get; set; } = ShapeType.Undefined;
        #endregion

        #region SHXファイルの妥当性判定
        public bool CheckShx(string shxFile)
        {
            return DecompileShx(shxFile, out _);
        }
        #endregion

        #region SHXファイル中のシェイプ一覧抽出
        public bool GetShapeNameList(string shxFile, out List<string> shapeNames)
        {
            shapeNames = new List<string>();
            if (DecompileShx(shxFile, out var shapes))
            {
                foreach (var v in shapes.Values)
                {
                    shapeNames.Add(v.Name);
                }
            }
            return (0 < shapeNames.Count);
        }
        #endregion

        #region SHXファイルのSHP変換（おまけ）
        public bool ConvertShx2Shp(string shxFile, string shpFile)
        {
            var result = false;
            if (DecompileShx(shxFile, out var shapes))
            {
                result = WriteShp(shpFile, shapes);
            }
            return result;
        }
        #endregion


        //private 

        #region SHXファイルの解析（フォントのSHXは対象外です）
        private bool DecompileShx(string shxFile, out SortedList<ushort, ShapeData> shapes)
        {
            shapes = new SortedList<ushort, ShapeData>();
            try
            {
                // SHXファイル全体を読み込む
                var bs = File.ReadAllBytes(shxFile);
                var start = 0;
                // シグネチャの確認
                var signature = Encoding.ASCII.GetString(bs, start, 23);

                #region シェイプ1.0
                //シェイプ1.0形式の解析
                if (signature.StartsWith("AutoCAD-86 shapes 1.0"))
                {
                    Kind = ShapeType.Shape10;
                    start = 24;
                    // ushort コードの下限
                    // ushort コードの上限
                    // ushort シェイプ数
                    int num = BitConverter.ToInt16(bs, start + 4);
                    start += 6;

                    // シェイプデータの先頭アドレス
                    var offset = start + 4 * num;

                    // シェイプの個数分ループ
                    for (var i = 0; i < num; i++)
                    {
                        var shape = new ShapeData();
                        shape.ShapeCode = BitConverter.ToUInt16(bs, start);
                        int len = BitConverter.ToInt16(bs, start + 2);

                        // NUL が来るまでが名前
                        shape.Name = "";
                        var len2 = 0;
                        for (; len2 < len; len2++)
                        {
                            if (bs[offset + len2] == 0) break;
                        }
                        if (len2 > 0)
                        {
                            shape.Name = Encoding.ASCII.GetString(bs, offset, len2);
                        }
                        ++len2;

                        // NUL の後lenまでがデータ
                        shape.Data = new byte[len - len2];
                        Array.Copy(bs, offset + len2, shape.Data, 0, len - len2);

                        // シェイプをリストに追加
                        shapes.Add(shape.ShapeCode, shape);
                        start += 4;
                        offset += len;
                    }
                }
                #endregion 
                #region シェイプ1.1
                //シェイプ1.1形式の解析
                else if (signature.StartsWith("AutoCAD-86 shapes 1.1"))
                {
                    Kind = ShapeType.Shape11;
                    start = 24;
                    // ushort コードの下限
                    // ushort コードの上限
                    // ushort シェイプ数
                    int num = BitConverter.ToInt16(bs, start + 4);
                    start += 6;
                    // シェイプデータの先頭アドレス
                    var offset = start + 4 * num;
                    // シェイプの個数分ループ
                    for (var i = 0; i < num; i++)
                    {
                        var shape = new ShapeData();
                        shape.ShapeCode = BitConverter.ToUInt16(bs, start);
                        int len = BitConverter.ToInt16(bs, start + 2);
                        // NUL が来るまでが名前
                        shape.Name = "";
                        var len2 = 0;
                        for (; len2 < len; len2++)
                        {
                            if (bs[offset + len2] == 0)
                            {
                                break;
                            }
                        }
                        if (len2 > 0)
                        {
                            shape.Name = Encoding.ASCII.GetString(bs, offset, len2);
                        }
                        ++len2;
                        // NUL の後lenまでがデータ
                        shape.Data = new byte[len - len2];
                        Array.Copy(bs, offset + len2, shape.Data, 0, len - len2);
                        // シェイプをリストに追加
                        shapes.Add(shape.ShapeCode, shape);
                        start += 4;
                        offset += len;
                    }
                }
                else if (signature.StartsWith("AutoCAD-86 unifont 1.0"))
                {
                    Kind = ShapeType.UniFont10;
                }
                else if (signature.StartsWith("AutoCAD-86 bigfont 1.0"))
                {
                    Kind = ShapeType.BigFont10;
                }

                #endregion

                #region その他

                else
                {
                    //Console.Write("不正なSHXファイル {0}", signature);
                    return false;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
                return false;
            }

            return (0 < shapes.Count);
        }
        #endregion
        #region SHPファイルの出力
        private bool WriteShp(string shpfile, SortedList<ushort, ShapeData> shapes)
        {
            try
            {
                // ファイルの出力
                using (var sw = new StreamWriter(shpfile, false, Encoding.ASCII))
                {
                    // ファイルヘッダを出力
                    sw.WriteLine(";;");
                    sw.WriteLine(";; AutoCAD SHP file generated by FontCompiler.exe");
                    sw.WriteLine(";;");
                    sw.WriteLine();

                    // コードの昇順に出力
                    foreach (var pair in shapes)
                    {
                        string line;
                        var shape = pair.Value;
                        if (pair.Key == 0)
                        {
                            line = string.Format("*0,{0},{1}", shape.Data.Length, shape.Name);
                        }
                        else
                        {
                            line = string.Format("*{0:X5},{1},{2}", shape.ShapeCode, shape.Data.Length, shape.Name);
                        }
                        sw.WriteLine(line);
                        WriteShapeData(sw, pair.Value);
                        sw.WriteLine();
                    }
                    sw.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
                return false;
            }
        }
        #endregion

        #region ツール
        private string toChar(byte a) => string.Format("{0}", (int)(a > 128 ? a - 256 : a));

        private string toChar2(byte a) => (a > 128 ?
            string.Format("-{0:X3}", -(a - 256)) :
            a.ToString("X3"));

        private void WriteShapeData(StreamWriter sw, ShapeData shape)
        {
            // 文字列化する
            var lines = "";
            var len = shape.Data.Length;
            if (shape.ShapeCode == 0)
            {
                for (var i = 0; i < len; i++)
                {
                    lines += string.Format("{0},", shape.Data[i]);
                }
                lines = lines.Remove(lines.Length - 1);
            }
            else
            {
                for (var i = 0; i < len;)
                {
                    switch (shape.Data[i])
                    {
                        case 0: //終了
                            lines += "0";
                            ++i;
                            break;

                        case 1: //ペンダウン
                        case 2: //ペンアップ
                        case 5: //位置をスタックに保存
                        case 6: //位置をスタックから復元
                        case 14: //縦書きのみ次のコマンドを実行
                            lines += string.Format("{0},", shape.Data[i]);
                            ++i;
                            break;

                        case 3: //尺度で割る
                        case 4: //尺度を掛ける
                            lines += string.Format("{0},{1},", shape.Data[i], shape.Data[i + 1]);
                            i += 2;
                            break;

                        case 7://サブシェイプ呼び出し
                            // 通常フォント,ビッグフォントのサブシェイプコードは１バイト
                            lines += string.Format("{0},{1:X3},",
                                shape.Data[i],      // 7
                                shape.Data[i + 1]   // CD
                                );
                            i += 2;
                            break;

                        case 8: //XYを指定
                            lines += string.Format("{0},({1},{2}),",
                                shape.Data[i],
                                toChar(shape.Data[i + 1]),
                                toChar(shape.Data[i + 2]));
                            i += 3;
                            break;

                        case 9: //XYの連続指定
                            lines += string.Format("{0},", shape.Data[i]);
                            ++i;
                            for (var done = false; !done;)
                            {
                                lines += string.Format("({0},{1}),",
                                    toChar(shape.Data[i]),
                                    toChar(shape.Data[i + 1]));
                                if (shape.Data[i] == 0 && shape.Data[i + 1] == 0)
                                    done = true;
                                i += 2;
                            }
                            break;

                        case 10: //８分円弧 10,(半径,SC), ※SCは正負の16進数
                            lines += string.Format("{0},({1},{2}),",
                                shape.Data[i],
                                toChar(shape.Data[i + 1]),
                                toChar2(shape.Data[i + 2]));
                            i += 3;
                            break;

                        case 11: //自由円弧 11,(開始,終了,半径1,半径2,SC), ※SCは正負の16進数
                            lines += string.Format("{0},({1},{2},{3},{4},{5}),",
                                shape.Data[i],
                                toChar(shape.Data[i + 1]),
                                toChar(shape.Data[i + 2]),
                                toChar(shape.Data[i + 3]),
                                toChar(shape.Data[i + 4]),
                                toChar2(shape.Data[i + 5]));
                            i += 6;
                            break;

                        case 12: //膨らみ円弧 12,(X,Y,膨らみ),
                            lines += string.Format("{0},({1},{2},{3}),",
                                shape.Data[i],
                                toChar(shape.Data[i + 1]),
                                toChar(shape.Data[i + 2]),
                                toChar(shape.Data[i + 3]));
                            i += 4;
                            break;

                        case 13: //連続膨らみ円弧 13,(X,Y,膨らみ),..(0,0), ※終端は(0,0)
                            lines += string.Format("{0},", shape.Data[i]);
                            ++i;
                            for (var done = false; !done;)
                            {
                                if (shape.Data[i] == 0 && shape.Data[i + 1] == 0)
                                {
                                    lines += "(0,0),";
                                    done = true;
                                    i += 2;
                                }
                                else
                                {
                                    lines += string.Format("({0},{1},{2}),",
                                        toChar(shape.Data[i]),
                                        toChar(shape.Data[i + 1]),
                                        toChar(shape.Data[i + 2]));
                                    i += 3;
                                }
                            }
                            break;

                        case 15: // 不正なオペランド
                        default: //その他は16進数にする(上位4ビット:方向と下位4ビット:距離)
                            lines += string.Format("{0:X3},", shape.Data[i]);
                            ++i;
                            break;
                    }
                }
            }

            // 80桁を目途にカンマの位置で改行して出力
            for (len = 80; lines.Length > len;)
            {
                if (lines[len] == ',')
                {
                    sw.WriteLine(lines.Substring(0, len + 1));
                    lines = lines.Substring(len + 1);
                    len = 80;
                }
                else
                {
                    --len;
                }
            }
            if (lines.Length > 0)
            {
                sw.WriteLine(lines);
            }
        }
        #endregion
    }
}
