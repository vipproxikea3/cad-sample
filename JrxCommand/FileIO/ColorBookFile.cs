﻿#if _IJCAD_
using CADApp = GrxCAD.ApplicationServices.Application;
using CADException = GrxCAD.Runtime.Exception;
using CADColors = GrxCAD.Colors;
using GrxCAD.Colors;

#elif _AutoCAD_
using Autodesk.AutoCAD.Colors;

#endif

using Exception = System.Exception;
using JrxCad.Utility;

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace JrxCad.FileIO
{
    internal class ColorBook
    {
        public Color PageColor { get; set; }
        public List<string> ColorNames { get; set; }
    }

    /// <summary>
    /// acbファイル
    /// </summary>
    internal class ColorBookFile
    {
        /// <summary> カラーブック一覧 </summary>
        public Dictionary<string, List<ColorBook>> BookMap { get; private set; }

        /// <summary>
        /// カラーブックファイル読み込み
        /// </summary>
        public void Read()
        {
            var currentUser = new Hkey.CurrentUser();
            var subKey = currentUser.GetProfileSubKey() + @"\General";
            if (!currentUser.IsExist(subKey)) return;

            BookMap = new Dictionary<string, List<ColorBook>>();
            var directories = currentUser.GetValue(subKey, "ColorBookLocation");
            var paths = directories.Split(';');
            foreach (var path in paths)
            {
                if (string.IsNullOrEmpty(path)) continue;

                foreach (var acbFile in Directory.GetFiles(path))
                {
                    if (!Path.GetExtension(acbFile).Equals(".acb")) continue;

                    try
                    {
                        var colorBooks = new List<ColorBook>();

                        var xml = XElement.Load(acbFile);
                        var bookName = xml.Elements("bookName").First().Value;

                        foreach (var page in xml.Elements("colorPage"))
                        {
                            var colorBook = new ColorBook();

                            //TODO:Rgb8Encryptの算出
                            var pageColor = page.Elements("pageColor").First();
                            var rgbEncrypt = pageColor.Element("RGB8Encrypt");
                            if (rgbEncrypt != null)
                            {
                                //var r = rgbEncrypt.Element("redEncrypt")?.Value;
                                //var g = rgbEncrypt.Element("greenEncrypt")?.Value;
                                //var b = rgbEncrypt.Element("blueEncrypt")?.Value;
                            }
                            else
                            {
                                var rgb8 = pageColor.Element("RGB8");
                                if (rgb8 != null)
                                {
                                    var r = byte.Parse(rgb8.Element("red")?.Value);
                                    var g = byte.Parse(rgb8.Element("green")?.Value);
                                    var b = byte.Parse(rgb8.Element("blue")?.Value);

                                    colorBook.PageColor = Color.FromRgb(r, g, b);
                                }
                            }

                            colorBook.ColorNames = new List<string>();
                            foreach (var entry in page.Elements("colorEntry"))
                            {
                                var colorName = entry.Element("colorName")?.Value;
                                colorBook.ColorNames.Add(colorName);
                            }

                            //TODO:Rgb8Encryptの算出ができたらここ削除
                            if (rgbEncrypt != null)
                            {
                                colorBook.PageColor = Color.FromNames(colorBook.ColorNames[0], bookName);
                            }

                            colorBooks.Add(colorBook);
                        }

                        BookMap.Add(bookName, colorBooks);
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorShow(ex);
                    }
                }
            }
        }
    }
}
