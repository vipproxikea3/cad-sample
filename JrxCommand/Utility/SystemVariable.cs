#if _IJCAD_
using Application = GrxCAD.ApplicationServices.Application;
using GrxCAD.Runtime;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Application = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Geometry;
#endif

using System;
using JrxCad.Helpers;
using Exception = System.Exception;

namespace JrxCad.Utility
{
    public class SystemVariable
    {
        /// <summary>
        /// システム変数を一時的に変更する場合に,
        /// usingを使って使用する
        /// </summary>
        public class Temp : IDisposable
        {
            /// <summary>
            /// システム変数名
            /// </summary>
            private readonly string _name;

            /// <summary>
            /// システム変数の値
            /// </summary>
            private readonly object _value;

            public Temp(string name, object value)
            {
                _name = name;
                _value = GetValue(_name);
                SetValue(_name, value);
            }

            public void Dispose()
            {
                SetValue(_name, _value);
            }
        }

        #region static Angle
        /// <summary>
        /// AngBase/AngDirを考慮したdegree文字列に変換
        /// </summary>
        /// <example>HpAng/GfAng など</example>
        public static string GetAngleToString(string name)
        {
            var radian = GetDouble(name);
            var normalize = radian.NormalizeAngle();
            return Converter.AngleToString(normalize);
        }

        /// <summary>
        /// AngBase/AngDirを考慮したdegree文字列を、Database格納用radianに変換
        /// </summary>
        /// <example>HpAng/GfAng など</example>
        public static double GetAngle(string name)
        {
            var degree = GetAngleToString(name);
            var radian = Converter.StringToAngle(degree);
            return radian.NormalizeAngle();
        }

        /// <summary>
        /// Convert string to point3d
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Point3d GetPoint3d(string name)
        {
            try
            {
                var strPnt = GetString(name).Trim('(', ')').Split(',');
                var retPnt = new Point3d(double.Parse(strPnt[0]), double.Parse(strPnt[1]), double.Parse(strPnt[2]));
                return retPnt;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
                return Point3d.Origin;
            }
        }

        #endregion

        #region static Get
        /// <summary>
        /// システム変数の値 取得
        /// </summary>
        /// <param name="name">システム変数名</param>
        /// <returns>値</returns>
        public static object GetValue(string name)
        {
            return Application.GetSystemVariable(name);
        }

        /// <summary>
        /// システム変数の値 取得
        /// </summary>
        /// <param name="name">システム変数名</param>
        /// <returns>値</returns>
        public static string GetString(string name)
        {
            var value = Application.GetSystemVariable(name);
            if (value == null) return "";
            return value.ToString();
        }

        /// <summary>
        /// システム変数の値 取得
        /// </summary>
        /// <param name="name">システム変数名</param>
        /// <returns>値</returns>
        public static bool GetBool(string name)
        {
            var value = GetString(name);
            if (string.IsNullOrEmpty(value)) return false;

            return value == "1";
        }

        /// <summary>
        /// システム変数の値 取得
        /// </summary>
        /// <param name="name">システム変数名</param>
        /// <returns>値 キャストエラーの場合、例外</returns>
        public static double GetDouble(string name)
        {
            var value = GetString(name);
            return double.Parse(value);
        }

        /// <summary>
        /// システム変数の値 取得
        /// </summary>
        /// <param name="name">システム変数名</param>
        /// <returns>値 キャストエラーの場合、例外</returns>
        public static int GetInt(string name)
        {
            var value = GetString(name);
            return int.Parse(value);
        }
        #endregion

        #region static Set
        /// <summary>
        /// システム変数の値 セット
        /// </summary>
        /// <returns>値</returns>
        public static void SetValue(string name, object value)
        {
            Application.SetSystemVariable(name, value);
        }

        /// <summary>
        /// システム変数の値 セット
        /// </summary>
        /// <returns>値</returns>
        public static void SetString(string name, string value)
        {
            Application.SetSystemVariable(name, value);
        }

        /// <summary>
        /// システム変数の値 セット
        /// </summary>
        /// <returns>値</returns>
        public static void SetBool(string name, bool value)
        {
            Application.SetSystemVariable(name, value ? 1 : 0);
        }

        public static void SetInt(string name, int value)
        {
            Application.SetSystemVariable(name, value);
        }
        #endregion
    }
}
