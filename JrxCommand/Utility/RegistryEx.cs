#if _IJCAD_
using Application = GrxCAD.ApplicationServices.Application;
using GrxCAD.DatabaseServices;
#elif _AutoCAD_
using Application = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
using Registry = Autodesk.AutoCAD.Runtime.Registry;
using RegistryKey = Autodesk.AutoCAD.Runtime.RegistryKey;
#endif

using Microsoft.Win32;


namespace JrxCad.Utility
{
    internal abstract class Hkey
    {
        protected RegistryKey Key { get; set; }

        /// <summary>
        /// subKey 存在チェック
        /// </summary>
        public bool IsExist(string subKey)
        {
            var key = Key.OpenSubKey(subKey);
            return key != null;
        }

        /// <summary>
        /// subKey 作成
        /// </summary>
        public bool CreateSubKey(string subKey)
        {
            var key = Key.CreateSubKey(subKey);
            return key != null;
        }

        /// <summary>
        /// subKey 値取得
        /// </summary>
        public string GetValue(string subKey, string name)
        {
            var key = Key.OpenSubKey(subKey);
            var value = key?.GetValue(name);
            return value?.ToString();
        }

        public int GetValueInt(string subKey, string name)
        {
            if (int.TryParse(GetValue(subKey, name), out var retInt))
                return retInt;

            //Throw exception if parse error
            //The exception should be catched on the caller side
            return int.Parse(Util.GetEnv(name));
        }

        /// <summary>
        /// subKey 値セット
        /// </summary>
        public void SetValue(string subKey, string name, object value, RegistryValueKind kind)
        {
            var key = Key.OpenSubKey(subKey, true);
            key?.SetValue(name, value, kind);
        }

        /// <summary>
        /// HKEY_CURRENT_USER
        /// </summary>
        public class CurrentUser : Hkey
        {
            public CurrentUser()
            {
                Key = Registry.CurrentUser;
            }

            /// <summary>
            /// Profilesまでのサブキー
            /// HKEY_CURRENT_USER\Software\Autodesk\AutoCAD\R23.1\ACAD-3001:411\Profiles\プロファイル名
            /// </summary>
            public string GetProfileSubKey()
            {
                var productKey = HostApplicationServices.Current.UserRegistryProductRootKey;
                var profile = Application.GetSystemVariable("CPROFILE");

                return $@"{productKey}\Profiles\{profile}";
            }

            public string GetFixedProfileSubKey()
            {
                var productKey = HostApplicationServices.Current.UserRegistryProductRootKey;
                return $@"{productKey}\FixedProfile";
            }

            public string GetStringProfileValue(string name)
            {
                return GetValue(GetProfileSubKey(), name);
            }

            public string GetStringFixedProfileValue(string name)
            {
                return GetValue(GetFixedProfileSubKey(), name);
            }

            public int GetIntProfileValue(string name)
            {
                return GetValueInt(GetProfileSubKey(), name);

            }

            public int GetIntFixedProfileValue(string name)
            {
                return GetValueInt(GetFixedProfileSubKey(), name);
            }
        }

        /// <summary>
        /// HKEY_LOCAL_MACHINE
        /// </summary>
        public class LocalMachine : Hkey
        {
            public LocalMachine()
            {
                Key = Registry.LocalMachine;
            }

            public string GetIntelliJapanSubKey()
            {
                return $@"SOFTWARE\IntelliJapan";
            }
        }
    }
}
