﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;

#endif

namespace JrxCad.Utility
{
    /// <summary>
    /// DrawJigEx
    /// </summary>
    public abstract class DrawJigExN : DrawJig
    {
        public abstract class JigActionsBase
        {
            public abstract bool Draw(WorldGeometry geometry);
            public abstract void OnUpdate();
        }

        protected JigActionsBase Actions;

        protected DrawJigExN(JigActionsBase actions)
        {
            Actions = actions;
        }
        protected override bool WorldDraw(WorldDraw draw)
        {
            return Actions.Draw(draw.Geometry);
        }
    }

    /// <summary>
    /// PointDrawJig
    /// </summary>
    public class PointDrawJigN : DrawJigExN
    {
        public abstract class JigActions : JigActionsBase
        {
            public abstract JigPromptPointOptions Options { get; set; }

            public Point3d LastValue { get; set; }

            public Matrix3d GetDisplacementMatrix()
            {
                return Matrix3d.Displacement(LastValue - Options.BasePoint);
            }
        }

        protected new JigActions Actions => base.Actions as JigActions;

        public PointDrawJigN(JigActions actions) : base(actions)
        {
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            var res = prompts.AcquirePoint(Actions.Options);
            switch (res.Status)
            {
                case PromptStatus.OK:
                    {
                        Actions.LastValue = res.Value;
                        Actions.OnUpdate();
                        return SamplerStatus.OK;
                    }
                case PromptStatus.Keyword:
                case PromptStatus.None:
                    return SamplerStatus.OK;
                default:
                    return SamplerStatus.Cancel;
            }
        }
    }

    /// <summary>
    /// DistanceDrawJig
    /// </summary>
    public class DistanceDrawJigN : DrawJigExN
    {
        public abstract class JigActions : JigActionsBase
        {
            public abstract JigPromptDistanceOptions Options { get; set; }

            public double LastValue { get; set; }

            public Matrix3d GetDisplacementMatrix(Vector3d direction)
            {
                return Matrix3d.Displacement(direction.GetNormal() * LastValue);
            }
        }

        protected new JigActions Actions => base.Actions as JigActions;

        public DistanceDrawJigN(JigActions actions) : base(actions)
        {
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            var res = prompts.AcquireDistance(Actions.Options);
            switch (res.Status)
            {
                case PromptStatus.OK:
                    {
                        Actions.LastValue = res.Value;
                        Actions.OnUpdate();
                        return SamplerStatus.OK;
                    }
                case PromptStatus.Keyword:
                case PromptStatus.None:
                    return SamplerStatus.OK;
                default:
                    return SamplerStatus.Cancel;
            }
        }
    }

    /// <summary>
    /// AngleDrawJig
    /// </summary>
    public class AngleDrawJigN : DrawJigExN
    {
        public abstract class JigActions : JigActionsBase
        {
            public abstract JigPromptAngleOptions Options { get; set; }

            public double LastValue { get; set; }

            public Matrix3d GetRotationMatrix()
            {
                var basePointU = Options.BasePoint.TransformBy(CoordConverter.WcsToUcs());
                return Matrix3d.Rotation(LastValue, Vector3d.ZAxis, basePointU);
            }
        }

        protected new JigActions Actions => base.Actions as JigActions;

        public AngleDrawJigN(JigActions actions) : base(actions)
        {
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            var res = prompts.AcquireAngle(Actions.Options);
            switch (res.Status)
            {
                case PromptStatus.OK:
                    {
                        Actions.LastValue = res.Value;
                        Actions.OnUpdate();
                        return SamplerStatus.OK;
                    }
                case PromptStatus.Keyword:
                case PromptStatus.None:
                    return SamplerStatus.OK;
                default:
                    return SamplerStatus.Cancel;
            }
        }
    }

    /// <summary>
    /// StringDrawJig
    /// </summary>
    public class StringDrawJigN : DrawJigExN
    {
        public abstract class JigActions : JigActionsBase
        {
            public abstract JigPromptStringOptions Options { get; set; }
            public abstract void UpdateString(string str);
        }

        protected new JigActions Actions => base.Actions as JigActions;

        public StringDrawJigN(JigActions actions) : base(actions)
        {
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            var res = prompts.AcquireString(Actions.Options);
            switch (res.Status)
            {
                case PromptStatus.OK:
                    {
                        Actions.UpdateString(res.StringResult);
                        return SamplerStatus.OK;
                    }
                case PromptStatus.Keyword:
                case PromptStatus.None:
                    return SamplerStatus.OK;
                default:
                    return SamplerStatus.Cancel;
            }
        }
    }

    /// <summary>
    /// IntegerDrawJig
    /// </summary>
    public class IntegerDrawJigN : DrawJigExN
    {
        public abstract class JigActions : JigActionsBase
        {
            public abstract PromptIntegerOptions Options { get; set; }
            public abstract void UpdateInteger(int integer);
        }

        protected new JigActions Actions => base.Actions as JigActions;

        public IntegerDrawJigN(JigActions actions) : base(actions)
        {
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            var res = Util.Editor().GetInteger(Actions.Options);
            switch (res.Status)
            {
                case PromptStatus.OK:
                    {
                        Actions.UpdateInteger(res.Value);
                        return SamplerStatus.OK;
                    }
                case PromptStatus.Keyword:
                case PromptStatus.None:
                    return SamplerStatus.OK;
                default:
                    return SamplerStatus.Cancel;
            }
        }
    }
}

