#if _IJCAD_
using GrxCAD.ApplicationServices;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using Exception = GrxCAD.Runtime.Exception;

#elif _AutoCAD_
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using DatabaseServices = Autodesk.AutoCAD.DatabaseServices;

#endif

using System;
using System.Collections.Generic;
using System.Linq;
using JrxCad.Helpers;
using JrxCad.Resources.Common;

namespace JrxCad.Utility
{
    public class EntityEx : IDisposable
    {
        public int ObjectOnLockedLayer { get; set; }

        public List<DBObject> DbObjects { get; set; } = new List<DBObject>();
        public EntityEx()
        {
            ObjectOnLockedLayer = 0;
        }

        public EntityEx(Transaction tr, SelectionSet ss, OpenMode mode)
        {
            foreach (var id in ss.GetObjectIds())
            {
                var dBObject = tr.GetObject(id, mode);
                if (dBObject != null)
                {
                    DbObjects.Add(dBObject);
                }
            }
        }

        public void Dispose()
        {
            //if (CurrentView != null)
            //{
            //    CurrentView.Dispose();
            //}
            foreach (var dBObject in DbObjects)
            {
                dBObject.Dispose();
            }
        }

        public ObjectIdCollection GetObjectIds()
        {
            var ret = new ObjectIdCollection();
            foreach (var dBObject in DbObjects)
            {
                if (dBObject is Entity)
                {
                    ret.Add(dBObject.ObjectId);
                }
            }
            return ret;
        }

        /// <summary>
        /// 選択図形のハイライト
        /// </summary>
        public void SetHighlight()
        {
            foreach (var dBObject in DbObjects)
            {
                if (dBObject is Entity entity)
                {
                    entity.Highlight();
                }
            }
        }

        /// <summary>
        /// 選択図形のハイライト解除
        /// </summary>
        public void UnHighlight()
        {
            foreach (var dBObject in DbObjects)
            {
                if (dBObject is Entity entity)
                {
                    entity.Unhighlight();
                }
            }
        }

        /// <summary>
        /// 複数オブジェクト選択
        /// 　・ロック画層除外
        /// 　・事前選択有効
        /// 　・CommandFlags.Modal用
        /// </summary>
        public ObjectIdCollection SelectEntities(Document doc, Transaction tr)
        {
            var objectIds = new ObjectIdCollection();
            var idArrayEmpty = Array.Empty<ObjectId>();
            doc.Editor.SetImpliedSelection(idArrayEmpty);
            //オブジェクトを選択:
            var opt = new PromptSelectionOptions();
            var objectOnLockedLayerCount = 0;
            var selectedEntities = doc.Editor.GetSelection(opt);
            if (selectedEntities.Status != PromptStatus.OK)
            {
                return objectIds;
            }

            var set = selectedEntities.Value;
            foreach (SelectedObject selectedObj in set)
            {
                var entity = tr.GetObject<Entity>(selectedObj.ObjectId, OpenMode.ForWrite, false, true);
                if (entity is null) continue;
                if (Util.EntityOnLockedLayer(tr, entity))
                {
                    objectOnLockedLayerCount++;
                }
                else
                {
                    DbObjects.Add(entity);
                    _ = objectIds.Add(selectedObj.ObjectId);
                }
            }

            if (objectOnLockedLayerCount > 0)
            {
                ObjectOnLockedLayer = objectOnLockedLayerCount;
                var cmMessage = CommonRes.NUM_ENT_ON_LOCKED_LAYER_1;
                if (objectOnLockedLayerCount > 1)
                {
                    cmMessage = CommonRes.NUM_ENT_ON_LOCKED_LAYER_2;
                }
                doc.Editor.WriteMessage($"\n{cmMessage}", objectOnLockedLayerCount);
            }
            return objectIds;
        }

        /// <summary>
        /// 事前選択 取得
        /// 　・ロック画層除外
        /// 　・CommandFlags.Modal用
        /// </summary>
        public ObjectIdCollection SelectImplied(Document doc, Transaction tr)
        {
            var objectIds = new ObjectIdCollection();
            if (SystemVariable.GetInt("PICKFIRST") != 1)
            {
                return objectIds;
            }
            var retPick = doc.Editor.SelectImplied();
            if (retPick.Status != PromptStatus.OK)
            {
                return objectIds;
            }

            var set = retPick.Value;
            var objectOnLockedLayerCount = 0;
            foreach (SelectedObject selectedObj in set)
            {
                var entity = tr.GetObject<Entity>(selectedObj.ObjectId, OpenMode.ForWrite, false, true);
                if (entity is null) continue;
                if (Util.EntityOnLockedLayer(tr, entity))
                {
                    objectOnLockedLayerCount++;
                }
                else
                {
                    DbObjects.Add(entity);
                    objectIds.Add(selectedObj.ObjectId);
                }
            }
            doc.Editor.WriteMessage($"\n{CommonRes.NUM_ENT_FOUND}", set.Count);
            if (objectOnLockedLayerCount > 0)
            {
                ObjectOnLockedLayer = objectOnLockedLayerCount;
                var cmMessage = CommonRes.NUM_ENT_ON_LOCKED_LAYER_1;
                if (objectOnLockedLayerCount > 1)
                {
                    cmMessage = CommonRes.NUM_ENT_ON_LOCKED_LAYER_2;
                }
                doc.Editor.WriteMessage($"\n{cmMessage}", objectOnLockedLayerCount);
            }

            return objectIds;
        }

        /// <summary>
        /// Get base point for each object
        /// </summary>
        /// <returns></returns>
        public Point3d GetBasePoint()
        {
            var lastEntity = DbObjects.LastOrDefault();
            try
            {
                if (lastEntity is null)
                {
                    throw new NullReferenceException();
                }
                Type tp = lastEntity.GetType();
                if (tp == typeof(Viewport))
                {
                    return ((Viewport)lastEntity).GeometricExtents.MinPoint;
                }

                //Group：外接矩形の中心
                else if (tp == typeof(Group))
                {
                    var mostOuter = new Extents3d();
                    using (var tr = Util.StartTransaction())
                    {
                        foreach (var id in ((Group)lastEntity).GetAllEntityIds())
                        {
                            var ent = tr.GetObject<Entity>(id, OpenMode.ForRead);
                            if (ent != null)
                            {
                                if (ent.Bounds != null) mostOuter.AddExtents((Extents3d)ent.Bounds);
                            }
                        }
                        tr.Commit();
                    }
                    return mostOuter.CenterPoint();
                }
#if _AutoCAD_
                //Position Marker
                else if (tp == typeof(GeoPositionMarker))
                {
                    return ((GeoPositionMarker)lastEntity).Position;
                }

                //Camera
                else if (tp == typeof(Camera))
                {
                    var extents3d = lastEntity.Bounds;
                    if (extents3d != null)
                    {
                        var extents = extents3d.Value;
                        return extents.CenterPoint();
                    }
                }

                //Surface
                else if (lastEntity is DatabaseServices.Surface)
                {
                    var extents3d = (lastEntity as DatabaseServices.Surface).Bounds;
                    if (extents3d != null)
                    {
                        var extents = extents3d.Value;
                        if (tp == typeof(PlaneSurface))
                        {
                            //PlaneSurface is exploded to Line
                            using (var entitySet = new DBObjectCollection())
                            using (var planeSur = lastEntity as PlaneSurface)
                            {
                                planeSur?.Explode(entitySet);
                                lastEntity = entitySet[0] as Region;
                                using (var region = lastEntity as Region)
                                {
                                    entitySet.Clear();
                                    region?.Explode(entitySet);
                                    lastEntity = entitySet[0] as Entity;
                                    tp = lastEntity?.GetType(); //Line
                                }
                            }
                        }
                        else
                        {
                            return extents.CenterPoint();
                        }
                    }
                }
#endif
                //Solid
                else if (tp == typeof(Solid))
                {
                    return ((Solid)lastEntity).GetPointAt(0);
                }

                //Solid3d
                else if (tp == typeof(Solid3d))
                {
                    //Solid3d is exploded to Region
                    using (var entitySet = new DBObjectCollection())
                    using (var solid3d = (Solid3d)lastEntity)
                    {
                        solid3d.Explode(entitySet);
                        lastEntity = entitySet[0] as Region;
                        tp = lastEntity?.GetType();
                    }
                }

                //Region
                if (tp == typeof(Region))
                {
                    var gripPoints = new Point3dCollection();
                    ((Entity)lastEntity).GetGripPoints(gripPoints, new IntegerCollection(), new IntegerCollection());
                    if (gripPoints.Count > 0) return gripPoints[0];
                }

                //Line
                if (tp == typeof(Line))
                {
                    return ((Line)lastEntity).StartPoint;
                }

                //MLine
                else if (tp == typeof(Mline))
                {
                    return ((Mline)lastEntity).VertexAt(0);
                }

                //Circle, Arc, Ellipse
                else if (tp == typeof(Circle) || tp == typeof(Arc) || tp == typeof(Ellipse))
                {
                    if (tp == typeof(Circle))
                    {
                        return ((Circle)lastEntity).Center;
                    }
                    else if (tp == typeof(Arc))
                    {
                        return ((Arc)lastEntity).Center;
                    }
                    else
                    {
                        return ((Ellipse)lastEntity).Center;
                    }
                }

                //Leader
                else if (tp == typeof(Leader))
                {
                    return ((Leader)lastEntity).FirstVertex;
                }

                //MLeader
                else if (tp == typeof(MLeader))
                {
                    return Point3d.Origin;
                }

                //DBPoint
                else if (tp == typeof(DBPoint))
                {
                    return ((DBPoint)lastEntity).Position;
                }

                //Table
                else if (tp == typeof(Table))
                {
                    return ((BlockReference)lastEntity).Position;
                }

                //Shape
                else if (tp == typeof(Shape))
                {
                    return ((Shape)lastEntity).Position;
                }

                //Hatch
                else if (tp == typeof(Hatch))
                {
                    var extents3d = (lastEntity).Bounds;
                    if (extents3d != null)
                    {
                        var extents = extents3d.Value;
                        return extents.CenterPoint();
                    }
                }

                //Helix
                else if (tp == typeof(Helix))
                {
                    return ((Helix)lastEntity).GetAxisPoint();
                }

                //Spline
                else if (tp == typeof(Spline))
                {
                    return ((Spline)lastEntity).StartPoint;
                }

                //Ray
                else if (tp == typeof(Ray))
                {
                    return ((Ray)lastEntity).BasePoint;
                }

                //Xline
                else if (tp == typeof(Xline))
                {
                    return ((Xline)lastEntity).BasePoint;
                }

                //Wipeout
                else if (tp == typeof(Wipeout))
                {
                    return ((Wipeout)lastEntity).Orientation.Origin;
                }

                //FeatureControlFrame
                else if (tp == typeof(FeatureControlFrame))
                {
                    return ((FeatureControlFrame)lastEntity).Location;
                }

                //Trace
                else if (tp == typeof(Trace))
                {
                    return ((Trace)lastEntity).GetPointAt(0);
                }

                //ProxyEntity
                else if (tp == typeof(ProxyEntity))
                {
                    return Point3d.Origin;
                }

                //Mesh
                else if (tp == typeof(SubDMesh))
                {
                    var extents3d = lastEntity.Bounds;
                    if (extents3d != null)
                    {
                        var extents = extents3d.Value;
                        return extents.CenterPoint();
                    }
                }
                //Polyline: 1点目
                else if (tp == typeof(Polyline) || tp == typeof(Polyline2d) || tp == typeof(Polyline3d))
                {
                    return ((Curve)lastEntity).StartPoint;
                }

                //Attach: 挿入基点						
                else if (tp == typeof(DgnReference) || tp == typeof(DwfReference) || tp == typeof(Ole2Frame) ||
                    tp == typeof(PdfReference) || tp == typeof(RasterImage))
                {
                    if (tp == typeof(Ole2Frame))
                    {
                        return ((Ole2Frame)lastEntity).Location;
                    }
                    else if (tp == typeof(RasterImage))
                    {
                        return ((RasterImage)lastEntity).Orientation.Origin;
                    }
                    else //PdfReference, DwfReference, DgnReference
                    {
                        return ((UnderlayReference)lastEntity).Position;
                    }
                }

                //Block-BlockReference
                else if (lastEntity is BlockReference)
                {
                    if (tp == typeof(MInsertBlock)) //Block-MInsertBlock: 挿入基点
                    {
                        return ((MInsertBlock)lastEntity).Position;
                    }
                    else
                    {
                        // Xref - bound.MinPoint
                        // Center Line
                        // Dynamic block
                        // Center Mark
                        // Array
                        return (lastEntity as BlockReference).Position;
                    }
                }

                //Attribute: AttributeReference, AttributeDefinition: 挿入基点
                else if (tp == typeof(AttributeDefinition) || tp == typeof(AttributeReference))
                {
                    return ((DBText)lastEntity).Position;
                }

                //Dim
                else if (lastEntity is Dimension dimension)
                {
                    if (tp == typeof(ArcDimension))//ArcDimension: 円弧の始点
                    {
                        return ((ArcDimension)dimension).Leader1Point;
                    }
                    else if (tp == typeof(RotatedDimension))//RotatedDimension: 1点目
                    {
                        return ((RotatedDimension)dimension).XLine1Point;
                    }
                    else if (tp == typeof(LineAngularDimension2))//LineAngularDimension2: 2本目で選択したラインの始点 / 円弧の始点
                    {
                        return ((LineAngularDimension2)dimension).XLine1Start;
                    }
                    else if (tp == typeof(Point3AngularDimension))//Point3AngularDimension: 1点目
                    {
                        return ((Point3AngularDimension)dimension).XLine1Point;
                    }
                    else if (tp == typeof(OrdinateDimension))//OrdinateDimension: 起点
                    {
                        return ((OrdinateDimension)dimension).DefiningPoint;
                    }
                    else if (tp == typeof(DiametricDimension))//DiametricDimension: 矢印2
                    {
                        return ((DiametricDimension)dimension).ChordPoint;
                    }
                    else if (tp == typeof(RadialDimension))//RadialDimension: 中心
                    {
                        return ((RadialDimension)dimension).Center;
                    }
                    else if (tp == typeof(AlignedDimension))//AlignedDimension: 1本目の寸法補助線の起点
                    {
                        return ((AlignedDimension)dimension).XLine1Point;
                    }
                    else if (tp == typeof(RadialDimensionLarge))//RadialDimensionLarge: 矢印
                    {
                        return ((RadialDimensionLarge)dimension).ChordPoint;
                    }
                }
                //MText
                else if (tp == typeof(MText))
                {
                    return ((MText)lastEntity).Location;
                }

                //DBText
                else if (tp == typeof(DBText))
                {
                    var attPt = ((DBText)lastEntity).Justify;
                    if (attPt.Equals(AttachmentPoint.BaseLeft) || attPt.Equals(AttachmentPoint.BaseAlign) || attPt.Equals(AttachmentPoint.BaseFit))
                    {
                        return ((DBText)lastEntity).Position;
                    }
                    return ((DBText)lastEntity).AlignmentPoint;
                }

                //3d
                else if (tp == typeof(Section))
                {
                    return ((Section)lastEntity).Boundary[0];
                }

                //Light
                else if (tp == typeof(Light))
                {
                    return ((Light)lastEntity).Position;
                }

                //PolyFaceMesh is exploded to Face
                else if (tp == typeof(PolyFaceMesh))
                {
                    //get the first point of the first Face
                    using (var objectSet = new DBObjectCollection())
                    using (var polyFaceMesh = lastEntity as PolyFaceMesh)
                    {
                        polyFaceMesh?.Explode(objectSet);
                        var firstFace = objectSet[0] as Face;
                        if (firstFace != null)
                        {
                            lastEntity = firstFace;
                            tp = lastEntity.GetType();
                        }
                    }
                }

                //PolygonMesh is exploded to Face
                else if (tp == typeof(PolygonMesh))
                {
                    //get the first point of the first Face
                    using (var objectSet = new DBObjectCollection())
                    using (var polyFaceMesh = lastEntity as PolygonMesh)
                    {
                        polyFaceMesh?.Explode(objectSet);
                        var firstFace = objectSet[0] as Face;
                        if (firstFace != null)
                        {
                            lastEntity = firstFace;
                            tp = lastEntity.GetType();
                        }
                    }
                }

                //Face
                if (tp == typeof(Face))
                {
                    return ((Face)lastEntity).GetVertexAt(0);
                }

                if (lastEntity is Entity)
                {
                    return (lastEntity as Entity).GeometricExtents.MinPoint;
                }
                return Point3d.Origin;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
                return Point3d.Origin;
            }
            finally
            {
                lastEntity?.Dispose();
            }
        }

        public static ObjectId ExplodeFromGroupIfAny(Transaction trans, Entity entity)
        {
            var resId = ObjectId.Null;
            if (entity == null) return resId;
            using (var ids = entity.GetPersistentReactorIds())
            {
                foreach (ObjectId objectId in ids)
                {
                    using (var group = trans.GetObject<Group>(objectId, OpenMode.ForWrite, false))
                    {
                        if (group == null) continue;
#if _IJCAD_
                        entity.RemovePersistentReactor(group.Id);
#endif
                        resId = group.Id;
                    }
                }
            }
            return resId;
        }

        /// <summary>
        /// オブジェクトの削除
        /// </summary>
        public void Delete()
        {
            foreach (var entity in DbObjects)
            {
                entity.Erase();
            }
        }

        /// <summary>
        /// 外接矩形取得
        /// </summary>
        public Extents3d GetOutBound()
        {
            var outBound = new Extents3d();
            foreach (var entity in DbObjects)
            {
                var bound = entity.Bounds;
                if (!bound.HasValue)
                {
                    continue;
                }
                outBound.AddExtents(bound.Value);
            }
            return outBound;
        }
    }

    /// <summary>
    /// エンティティ表示状態を切り替えるクラス
    /// </summary>
    internal class ToggleVisible : IDisposable
    {
        private readonly ObjectId _id;

        private bool Toggle(ObjectId id)
        {
            using (var tr = Util.StartTransaction())
            using (var entity = tr.GetObject<Entity>(id, OpenMode.ForWrite))
            {
                if (entity != null)
                {
                    entity.Visible ^= true;     // 表示状態切替
                    tr.Commit();
                    return true;
                }
                tr.Commit();
            }

            return false;
        }

        public ToggleVisible(ObjectId id)
        {
            if (Toggle(id))
            {
                _id = id;
            }
        }

        public void Dispose()
        {
            Toggle(_id);
        }
    }

    /// <summary>
    /// エンティティ表示状態を切り替えるクラス
    /// </summary>
    internal class ToggleHighlight : IDisposable
    {
        private readonly ObjectId[] _objIds;

        private void Toggle(ObjectId[] objIds, bool on)
        {
            using (var tr = Util.StartTransaction())
            {
                foreach (var objId in objIds)
                {
                    using (var entity = tr.GetObject<Entity>(objId, OpenMode.ForWrite))
                    {
                        if (on)
                        {
                            entity.Highlight();
                        }
                        else
                        {
                            entity.Unhighlight();
                        }
                    }
                }
                tr.Commit();
            }
        }

        public ToggleHighlight(ObjectId[] objIds)
        {
            _objIds = objIds;
            Toggle(_objIds, true);
        }

        public void Dispose()
        {
            Toggle(_objIds, false);
        }
    }
}
