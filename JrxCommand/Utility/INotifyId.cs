﻿#if _IJCAD_
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
#endif

namespace JrxCad.Utility
{
    public interface INotifyId
    {
        void Appended(ObjectId id);
        void Modified(ObjectId id);
        void Erased(ObjectId id);
    }
}
