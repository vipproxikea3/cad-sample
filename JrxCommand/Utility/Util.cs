﻿#if _IJCAD_
using GrxCAD.ApplicationServices;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using Application = GrxCAD.ApplicationServices.Application;
//They are unnecessary for IJCAD
using System.IO;
using System.Windows.Forms;
using GrxCAD.Runtime;
using System.Globalization;
//

#elif _AutoCAD_
using Autodesk.AutoCAD.ApplicationServices;
using Application = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Internal;
#endif

using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using JrxCad.Helpers;
using Exception = System.Exception;

namespace JrxCad.Utility
{
    public class Util
    {
        public static Dictionary<Database, Dictionary<string, ObjectIdCollection>> ContObjectsCollection = new Dictionary<Database, Dictionary<string, ObjectIdCollection>>();

        private static void CreateCurrentDrawingCollection()
        {
            var currentDatabase = Database();
            var currentDrawingCollection = new Dictionary<string, ObjectIdCollection>();
            ContObjectsCollection.Add(currentDatabase, currentDrawingCollection);
        }

        private static void CreateCurrentSpaceCollection(Dictionary<string, ObjectIdCollection> currentDrawingCollection)
        {
            using (var tr = StartTransaction())
            using (var currentSpace = tr.GetObject(Database().CurrentSpaceId, OpenMode.ForRead) as BlockTableRecord)
            {
                if (!(currentSpace is null))
                {
                    var activeSpaceHandle = currentSpace.Handle.ToString();
                    var objectIdCollection = new ObjectIdCollection();
                    currentDrawingCollection.Add(activeSpaceHandle, objectIdCollection);
                }
            }
        }

        public static ObjectIdCollection GetCurrSpaceObjectIds()
        {
            ObjectIdCollection result = null;
            var currentKeyDatabase = GetCurrentDatabaseKey();

            if (currentKeyDatabase == null)
            {
                CreateCurrentDrawingCollection();
            }

            currentKeyDatabase = GetCurrentDatabaseKey();
            var currentDrawingCollection = ContObjectsCollection[currentKeyDatabase];

            using (var tr = StartTransaction())
            using (var currentSpace = tr.GetObject(currentKeyDatabase.CurrentSpaceId, OpenMode.ForRead) as BlockTableRecord)
            {
                if (!(currentSpace is null))
                {
                    var activeSpaceHandle = currentSpace.Handle.ToString();
                    if (!currentDrawingCollection.ContainsKey(activeSpaceHandle))
                    {
                        CreateCurrentSpaceCollection(currentDrawingCollection);
                    }
                    result = currentDrawingCollection[activeSpaceHandle];
                }
            }
            return result;
        }

        public static Database GetCurrentDatabaseKey()
        {
            var currentDatabase = Database();
            var keys = ContObjectsCollection.Keys;
            foreach (var item in keys)
            {
                if (item == currentDatabase)
                {
                    return item;
                }
            }

            return null;
        }

        public static void AddObjectIdToCollection(ObjectId objectId)
        {
            var currentSpaceObjectIdCollections = GetCurrSpaceObjectIds();
            currentSpaceObjectIdCollections.Add(objectId);
        }

        public static void CheckDrawingExits()
        {
            var exitDocuments = Application.DocumentManager;
            var listDrawingIsClose = new List<Database>();

            if (ContObjectsCollection.Count > 0)
            {
                foreach (var item in ContObjectsCollection)
                {
                    if (exitDocuments.GetDocument(item.Key) == null)
                    {
                        listDrawingIsClose.Add(item.Key);
                    }
                }
            }

            if (listDrawingIsClose.Count > 0)
            {
                foreach (var key in listDrawingIsClose)
                {
                    ContObjectsCollection.Remove(key);
                }
            }
        }

        /// <summary>
        /// check if entity on locked layer
        /// </summary>
        public static bool EntityOnLockedLayer(Transaction tr, Entity entity)
        {
            using (var record = tr.GetObject<LayerTableRecord>(entity.LayerId, OpenMode.ForRead))
            {
                if (record is null)
                {
                    return false;
                }
                return record.IsLocked;
            }
        }

        public static ObjectId CreateAnyBlock()
        {
            try
            {
                ObjectId blockId;
                var curIndex = 1;
                var blockName = "Temp" + curIndex;

                using (var tr = Util.StartTransaction())
                {
                    var bt = tr.GetObject<BlockTable>(Util.Database().BlockTableId, OpenMode.ForRead);

                    while (bt.Has(blockName))
                    {
                        curIndex++;
                        blockName = "Temp" + curIndex;
                    }

                    bt.UpgradeOpen();
                    //create new
                    using (var record = new BlockTableRecord())
                    {
                        record.Name = blockName;

                        bt.Add(record);
                        tr.AddNewlyCreatedDBObject(record, true);

                        blockId = bt[blockName];
                    }
                    tr.Commit();
                }

                return blockId;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
                return ObjectId.Null;
            }
        }

        public static bool HasAnyGroupInSelectionSet(ObjectIdCollection objectIds)
        {
            using (var trans = StartTransaction())
            {
                foreach (ObjectId objectId in objectIds)
                {
                    using (var keyEntity = trans.GetObject<Entity>(objectId, OpenMode.ForRead))
                    {
                        var reactorIds = keyEntity.GetPersistentReactorIds();
                        if (reactorIds is null || reactorIds.Count < 1) continue;
                        foreach (ObjectId reactorId in reactorIds)
                        {
                            using (var group = trans.GetObject<Group>(reactorId, OpenMode.ForWrite))
                            {
                                if (group == null) continue;
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public static Document CurDoc()
        {
            return Application.DocumentManager.MdiActiveDocument;
        }

        public static string CurDocId()
        {
            return CurDoc().UnmanagedObject.ToString();
        }

        public static Editor Editor()
        {
            return CurDoc().Editor;
        }

        public static Transaction StartTransaction()
        {
            return CurDoc().Database.TransactionManager.StartTransaction();
        }

        public static Database Database()
        {
            return CurDoc().Database;
        }
        //Type of Dimension
        public static int DimLunit()
        {
            return Database().Dimlunit;
        }
        //Type of length
        public static int Lunit()
        {
            return Database().Lunits;
        }

        //Type of Angle
        public static int Aunit()
        {
            return Database().Aunits;
        }

        //X direction of the current UCS
        public static Vector3d Ucsxdir()
        {
            return Database().Ucsxdir;
        }

        //Precision of Length
        public static int Luprec()
        {
            if (Database() == null)
                return 4;
            return Database().Luprec;
        }

        //Precision of Angle
        public static int Auprec()
        {
            if (Database() == null)
                return 0;
            return Database().Auprec;
        }

        //Precision of Inst
        public static UnitsValue Insunits()
        {
            return Database().Insunits;
        }

        public static Point3d RoundPoint(Point3d source)
        {
            return new Point3d(
                Math.Round(source.X, Luprec()),
                Math.Round(source.Y, Luprec()),
                Math.Round(source.Z, Luprec())
            );
        }

        public static int AngleNormalize(ref double angle)
        {
            var direction = angle < 0 ? -1 : 1;
            angle %= Math.PI * 2;
            return direction;
        }

        public static void DoHelpForCommand(string cmdName)
        {
#if _IJCAD_
            try
            {
                var installPath = GetInstallPathIJCad();
                if (!string.IsNullOrEmpty(installPath))
                {
                    Help.ShowHelp(null, installPath + @"\Help\GCAD_GCR_Eng.chm", HelpNavigator.Topic, "GCAD_GCR_Eng\\GCR_" + cmdName + ".htm");
                }
            }
            catch (Exception e)
            {
                Logger.ErrorShow(e);
            }
#elif _AutoCAD_
            Utils.DoHelpForCommand(cmdName);
#endif
        }

        //Find env-vars in registry instead
        [System.Security.SuppressUnmanagedCodeSecurity]
#if _IJCAD_
        [DllImport("gced.dll", EntryPoint = "gcedGetEnv", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
#elif _AutoCAD_
        [DllImport("accore.dll", EntryPoint = "acedGetEnv", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
#endif
        public static extern int acedGetEnv(string envName, string returnValue);

        public static string GetEnv(string envName)
        {
            var returnValue = new string(char.MinValue, 1024);
            var status = acedGetEnv(envName, returnValue);
            return status == -5001 ? null : returnValue;
        }

#if _IJCAD_
        [DllImport("gced.dll", EntryPoint = "gcedSetEnv", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
#elif _AutoCAD_
        [DllImport("accore.dll", EntryPoint = "acedSetEnv", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
#endif
        public static extern int acedSetEnv(string envName, string newValue);
        public static string SetEnv(string envName, string newValue)
        {
            var status = acedSetEnv(envName, newValue);
            return status == -5001 ? null : newValue;
        }

        /// <summary>
        /// EraseObjects
        /// </summary>
        /// <param name="objectIdCollection"></param>
        public static bool EraseObjects(ObjectIdCollection objectIdCollection)
        {
            try
            {
                using (var tr = StartTransaction())
                {
                    //check if entity is already in erase state.
                    for (var i = 0; i < objectIdCollection.Count; i++)
                    {
                        if (objectIdCollection[i].IsErased)
                        {
                            //GetObject, 3rd parameter openErased
                            var ent = tr.GetObject(objectIdCollection[i], OpenMode.ForWrite, true);
                            ent.Erase(false);
                            objectIdCollection[i] = ObjectId.Null;
                        }
                        else
                        {
                            var ent = tr.GetObject(objectIdCollection[i], OpenMode.ForWrite);
                            ent.Erase();
                        }
                    }
                    tr.Commit();
                }
                Editor().UpdateScreen();
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
                return false;
            }
        }

        public static string GetInstallPathIJCad()
        {
            var registryKey = new Hkey.LocalMachine();
            var subKey = registryKey.GetIntelliJapanSubKey() + @"\IJCAD 2021 PRO ENG\Setup";

            return registryKey?.GetValue(subKey, "Path");
        }
    }
}
