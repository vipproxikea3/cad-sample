﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
using CADApp = GrxCAD.ApplicationServices.Application;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;

#endif
using System;

namespace JrxCad.Utility
{
    public static class CoordConverter
    {
        // ----------------------------------------
        // UCS
        // ----------------------------------------

        /// <summary>
        /// UCS→WCS Matrix3d
        /// </summary>
        public static Matrix3d UcsToWcs()
        {
            return Util.Editor().CurrentUserCoordinateSystem;
        }

        /// <summary>
        /// WCS→UCS Matrix3d
        /// </summary>
        public static Matrix3d WcsToUcs()
        {
            return UcsToWcs().Inverse();
        }

        /// <summary>
        /// UCSの角度→WCSのベクトル(単位ベクトル)
        /// </summary>
        public static Vector3d UcsRotToWcsVec(double rotation)
        {
            var mat = Matrix3d.Rotation(rotation, Vector3d.ZAxis, Point3d.Origin);
            var vecOnPlaneX = Vector3d.XAxis.TransformBy(mat);
            return vecOnPlaneX.TransformBy(UcsToWcs());
        }

        /// <summary>
        /// WCSのベクトル→UCSの角度
        /// </summary>
        public static double WcsVecToUcsRot(Vector3d vec)
        {
            var ucs = UcsToWcs().CoordinateSystem3d;
            return vec.AngleOnPlane(new Plane(ucs.Origin, ucs.Xaxis, ucs.Yaxis));
        }

        // ----------------------------------------
        // OCS
        // ----------------------------------------

        /// <summary>
        /// OCS→WCS Matrix3d
        /// </summary>
        public static Matrix3d OcsToWcs(Vector3d normal)
        {
            return Matrix3d.PlaneToWorld(normal);
        }

        /// <summary>
        /// WCS→OCS Matrix3d
        /// </summary>
        public static Matrix3d WcsToOcs(Vector3d normal)
        {
            return OcsToWcs(normal).Inverse();
        }

        /// <summary>
        /// OCSの角度→WCSのベクトル(単位ベクトル)
        /// </summary>
        public static Vector3d OcsRotToWcsVec(Vector3d normal, double rotation)
        {
            var mat = Matrix3d.Rotation(rotation, Vector3d.ZAxis, Point3d.Origin);
            var vecOnPlaneX = Vector3d.XAxis.TransformBy(mat);
            return vecOnPlaneX.TransformBy(OcsToWcs(normal));
        }

        /// <summary>
        /// WCSのベクトル→OCSの角度
        /// </summary>
        public static double WcsVecToOcsRot(Vector3d normal, Vector3d vec)
        {
            return vec.AngleOnPlane(new Plane(Point3d.Origin, normal));
        }

        /// <summary>
        /// UCS平面からのOCS平面の高さ
        /// </summary>
        /// <exception cref="ArgumentException">
        /// OCSがUCSに平行でないとき
        /// </exception>
        public static double GetOcsUcsHeight(Vector3d normal)
        {
            var ucsToWcs = UcsToWcs();
            if (!ucsToWcs.CoordinateSystem3d.Zaxis.IsParallelTo(normal))
            {
                throw new ArgumentException("OCS is not Parallel to UCS");
            }

            var ori = Point3d.Origin.TransformBy(OcsToWcs(normal)).TransformBy(ucsToWcs.Inverse());
            return ori.Z;
        }

        /// <summary>
        /// UCSのHeight→OCSのElevation
        /// </summary>
        public static double UcsHeightToOcsElev(Vector3d normal, double heightFromUcs)
        {
            var ocsHeight = GetOcsUcsHeight(normal);
            return heightFromUcs - ocsHeight;
        }

        /// <summary>
        /// OCSのElevation→UCSのHeight
        /// </summary>
        public static double OcsElevToUcsHeight(Vector3d normal, double elevation)
        {
            var ocsHeight = GetOcsUcsHeight(normal);
            return elevation + ocsHeight;
        }

        // ----------------------------------------
        // MCS
        // ----------------------------------------

        /// <summary>
        /// MCS→WCS Matrix3d
        /// </summary>
        public static Matrix3d McsToWcs(ObjectId[] containerIds, out bool isProportional, Database db = null)
        {
            isProportional = true;

            if (db == null)
            {
                db = Util.CurDoc().Database;
            }

            using (var tr = db.TransactionManager.StartTransaction())
            {
                var mat = Matrix3d.Identity;
                foreach (var containerId in containerIds)
                {
                    using (var container = tr.GetObject(containerId, OpenMode.ForRead))
                    {
                        var br = (BlockReference)container;
                        mat = mat.PreMultiplyBy(br.BlockTransform);
                        if (!br.ScaleFactors.IsProportional())
                        {
                            isProportional = false;
                        }
                    }
                }

                return mat;
            }
        }

        /// <summary>
        /// WCS→MCS Matrix3d
        /// </summary>
        public static Matrix3d WcsToMcs(ObjectId[] containerIds, out bool isProportional, Database db = null)
        {
            return McsToWcs(containerIds, out isProportional, db).Inverse();
        }

        // ----------------------------------------
        // DCS
        // ----------------------------------------

        /// <summary>
        /// DCS→WCS Matrix3d
        /// </summary>
        public static Matrix3d DcsToWcs(ViewTableRecord view)
        {
            var dcsToWcs = Matrix3d.Rotation(-view.ViewTwist, view.ViewDirection, view.Target) *
                           Matrix3d.Displacement(view.Target - Point3d.Origin) *
                           Matrix3d.PlaneToWorld(view.ViewDirection);
            return dcsToWcs;
        }

        /// <summary>
        /// WCS→DCS Matrix3d
        /// </summary>
        public static Matrix3d WcsToDcs(ViewTableRecord view)
        {
            return DcsToWcs(view).Inverse();
        }

        // ----------------------------------------
        // PSDCS
        // ----------------------------------------

        /// <summary>
        /// PSDCS→WCS Matrix3d
        /// </summary>
        public static Matrix3d PsdcsToWcs(Viewport viewport)
        {
            var psdcsToDcs = Matrix3d.Displacement(new Vector3d(viewport.ViewCenter.X, viewport.ViewCenter.Y, 0.0)) *
                             Matrix3d.Scaling(1.0 / viewport.CustomScale, Point3d.Origin) *
                             Matrix3d.Displacement(Point3d.Origin - viewport.CenterPoint);
            var dcsToWcs = Matrix3d.Rotation(-viewport.TwistAngle, viewport.ViewDirection, viewport.ViewTarget) *
                           Matrix3d.Displacement(viewport.ViewTarget - Point3d.Origin) *
                           Matrix3d.PlaneToWorld(viewport.ViewDirection);
            return dcsToWcs * psdcsToDcs;
        }

        /// <summary>
        /// WCS→PSDCS Matrix3d
        /// </summary>
        public static Matrix3d WcsToPsDcs(Viewport viewport)
        {
            return PsdcsToWcs(viewport).Inverse();
        }

        public static Vector3d UcsZAxis()
        {
            var x = CADApp.DocumentManager.MdiActiveDocument.Database.Ucsxdir;
            var y = CADApp.DocumentManager.MdiActiveDocument.Database.Ucsydir;
            return x.CrossProduct(y);
        }

    }
}
