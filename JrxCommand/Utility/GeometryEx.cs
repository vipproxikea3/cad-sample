﻿#if _IJCAD_
using Polyline = GrxCAD.DatabaseServices.Polyline;
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

#endif

using System;

namespace JrxCad.Utility
{
    public class GeometryEx
    {
        /// <summary>
        /// 指定オブジェクトの指定座標におけるCurve3d(W)を取得
        /// </summary>
        public static Curve3d GetCurve3d(Database db,
            Point3d pickedPointW, ObjectId id, ObjectId[] containerIds,
            out bool isProportional, out Type type)
        {
            isProportional = true;
            type = null;

            var mcsToWcs = CoordConverter.McsToWcs(containerIds, out isProportional, db);

            using (var tr = Util.StartTransaction())
            using (var obj = tr.GetObject(id, OpenMode.ForRead))
            {
                // Curve3dを取得
                Curve3d curve3d = null;
                type = obj.GetType();
                switch (obj)
                {
                    case Line line:
#if _AutoCAD_
                        curve3d = line.GetGeCurve();
#else
                        curve3d = new LineSegment3d(line.StartPoint, line.EndPoint);
#endif
                        break;
                    case Polyline polyline:
                        curve3d = GetSegmentCurve3d(polyline, pickedPointW.TransformBy(mcsToWcs.Inverse()), out type);
                        break;
                    case Circle circle:
#if _AutoCAD_
                        curve3d = circle.GetGeCurve();
#else
                        curve3d = new CircularArc3d(circle.Center, circle.Normal, circle.Radius);
#endif
                        break;
                    case Arc arc:
#if _AutoCAD_
                        curve3d = arc.GetGeCurve();
#else
                        curve3d = new CircularArc3d(arc.Center, arc.Normal,
                            Matrix3d.PlaneToWorld(arc.Normal).CoordinateSystem3d.Xaxis,
                            arc.Radius, arc.StartAngle, arc.EndAngle);
#endif
                        break;
                }

                curve3d?.TransformBy(mcsToWcs);

                return curve3d;
            }
        }

        /// <summary>
        /// 指定ポリラインの指定座標におけるCurve3dを取得
        /// </summary>
        public static Curve3d GetSegmentCurve3d(Polyline polyline, Point3d pickedPoint, out Type type)
        {
            // Polylineを選択した点に最も近いセグメントを探す。
            var minDist = Double.MaxValue;
            Curve3d minSeg = null;

            for (int i = 0; i < polyline.NumberOfVertices; i++)
            {
                Curve3d seg = null;

                switch (polyline.GetSegmentType(i))
                {
                    case SegmentType.Line:
                        seg = polyline.GetLineSegmentAt(i);
                        break;
                    case SegmentType.Arc:
                        seg = polyline.GetArcSegmentAt(i);
                        break;
                }

                if (seg != null)
                {
                    var dist = seg.GetDistanceTo(pickedPoint);
                    if (dist < minDist)
                    {
                        minDist = dist;
                        minSeg = seg;
                    }
                }
            }

            type = null;
            if (minSeg != null)
            {
                type = minSeg is LineSegment3d ? typeof(Line) : typeof(Arc);
            }

            return minSeg;
        }

        /// <summary>
        /// 四半円点の傾斜角を取得
        /// </summary>
        /// <param name="vecX">平行四辺形の辺(X軸側)</param>
        /// <param name="vecY">平行四辺形の辺(Y軸側)</param>
        /// <param name="ang">傾斜角</param>
        /// <returns></returns>
        public static bool GetQuadrantsAngle(Vector3d vecX, Vector3d vecY, out double ang)
        {
            // 円を斜めから見た場合、
            // 円に外接する正方形は平行四辺形になり、円はその平行四辺形に内接する楕円になる。
            // 本処理では、その楕円の四半円点(最遠点/最近点)の傾斜角を算出する。
            // ※vec0,vec1はZ=0前提

            // vec1がY軸になるように回転
            var ang1 = vecY.GetAngleTo(Vector3d.YAxis, Vector3d.ZAxis);
            var vec0Rot = vecX.TransformBy(Matrix3d.Rotation(ang1, Vector3d.ZAxis, Point3d.Origin));
            var vec1Rot = vecY.TransformBy(Matrix3d.Rotation(ang1, Vector3d.ZAxis, Point3d.Origin));

            if (Math.Abs(vec0Rot.X) < Tolerance.Global.EqualPoint)
            {
                ang = 0.0;
                return false;
            }

            // 傾斜角
            var k = vec0Rot.Y / vec0Rot.X;
            var l = vec1Rot.Y / vec0Rot.X;
            var ang2 = 0.5 * Math.Atan2(2.0 * k, 1.0 - k * k - l * l);

            ang = ang2 - ang1;
            return true;
        }
    }
}
