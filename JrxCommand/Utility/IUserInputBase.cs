﻿#if _IJCAD_
using GrxCAD.EditorInput;
#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
#endif
namespace JrxCad.Utility
{
    /// <summary>
    /// IUserInputBase interface
    /// </summary>
    public interface IUserInputBase
    {
        PromptPointResult GetPoint(PromptPointOptions options);
        PromptDoubleResult GetDistance(PromptDistanceOptions options);
        PromptDoubleResult GetAngle(PromptAngleOptions options);
        PromptResult GetKeyword(PromptKeywordOptions options);
        PromptDoubleResult GetDouble(PromptDoubleOptions options);
        PromptIntegerResult GetInteger(PromptIntegerOptions options);
        PromptResult GetString(PromptStringOptions options);
        PromptPointResult GetCorner(PromptCornerOptions options);
        PromptEntityResult GetEntity(PromptEntityOptions options);
        PromptNestedEntityResult GetNestedEntity(PromptNestedEntityOptions options);
        PromptSelectionResult GetSelection(PromptSelectionOptions options);
        PromptResult AcquirePoint(PointDrawJigN.JigActions options);
        PromptResult AcquireDistance(DistanceDrawJigN.JigActions options);
        PromptResult AcquireAngle(AngleDrawJigN.JigActions options);
        PromptResult AcquireString(StringDrawJigN.JigActions options);
    }
    /// <summary>
    /// UserInputBase class
    /// </summary>
    public class UserInputBase : IUserInputBase
    {
        public PromptPointResult GetPoint(PromptPointOptions options)
        {
            return Util.Editor().GetPoint(options);
        }
        public PromptDoubleResult GetDistance(PromptDistanceOptions options)
        {
            return Util.Editor().GetDistance(options);
        }
        public PromptDoubleResult GetAngle(PromptAngleOptions options)
        {
            return Util.Editor().GetAngle(options);
        }
        public PromptResult GetKeyword(PromptKeywordOptions options)
        {
            return Util.Editor().GetKeywords(options);
        }
        public PromptDoubleResult GetDouble(PromptDoubleOptions options)
        {
            return Util.Editor().GetDouble(options);
        }
        public PromptIntegerResult GetInteger(PromptIntegerOptions options)
        {
            return Util.Editor().GetInteger(options);
        }
        public PromptResult GetString(PromptStringOptions options)
        {
            return Util.Editor().GetString(options);
        }
        public PromptPointResult GetCorner(PromptCornerOptions options)
        {
            return Util.Editor().GetCorner(options);
        }
        public PromptEntityResult GetEntity(PromptEntityOptions options)
        {
            return Util.Editor().GetEntity(options);
        }
        public PromptNestedEntityResult GetNestedEntity(PromptNestedEntityOptions options)
        {
            return Util.Editor().GetNestedEntity(options);
        }
        public PromptSelectionResult GetSelection(PromptSelectionOptions options)
        {
            return Util.Editor().GetSelection(options);
        }
        public PromptResult AcquirePoint(PointDrawJigN.JigActions options)
        {
            var jig = new PointDrawJigN(options);
            return Util.Editor().Drag(jig);
        }
        public PromptResult AcquireDistance(DistanceDrawJigN.JigActions options)
        {
            var jig = new DistanceDrawJigN(options);
            return Util.Editor().Drag(jig);
        }
        public PromptResult AcquireAngle(AngleDrawJigN.JigActions options)
        {
            var jig = new AngleDrawJigN(options);
            return Util.Editor().Drag(jig);
        }
        public PromptResult AcquireString(StringDrawJigN.JigActions options)
        {
            var jig = new StringDrawJigN(options);
            return Util.Editor().Drag(jig);
        }
    }
}