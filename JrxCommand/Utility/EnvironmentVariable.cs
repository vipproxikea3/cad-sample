﻿using JrxCad.Resources.Common;
using Exception = System.Exception;

namespace JrxCad.Utility
{
    class EnvironmentVariable
    {
        public static int GetInt(string name)
        {
            try
            {
                return int.Parse(Util.GetEnv(name));
            }
            catch (Exception)
            {
                var error = string.Format(CommonRes.ENV_VAR_FAILED_TO_GET, name);
                throw new Exception(error);
            }
        }
    }
}
