﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;

#endif

namespace JrxCad.Utility
{
    /// <summary>
    /// DrawJigEx
    /// </summary>
    public abstract class DrawJigExOld : DrawJig
    {
        public abstract class OptionsBase
        {
            public abstract bool Draw(WorldGeometry geometry);
        }

        protected OptionsBase Options;

        protected DrawJigExOld(OptionsBase options)
        {
            Options = options;
        }

        protected override bool WorldDraw(WorldDraw draw)
        {
            return Options.Draw(draw.Geometry);
        }
    }

    /// <summary>
    /// PointDrawJig
    /// </summary>
    public class PointDrawJig : DrawJigExOld
    {
        public abstract class PointOptions : OptionsBase
        {
            public abstract JigPromptPointOptions Options { get; set; }
            public abstract void UpdatePoint(Point3d pt);
        }

        protected new PointOptions Options => base.Options as PointOptions;

        public PointDrawJig(PointOptions options) : base(options)
        {
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            var res = prompts.AcquirePoint(Options.Options);
            switch (res.Status)
            {
                case PromptStatus.OK:
                    {
                        Options.UpdatePoint(res.Value);
                        return SamplerStatus.OK;
                    }
                case PromptStatus.Keyword:
                case PromptStatus.None:
                    return SamplerStatus.OK;
                default:
                    return SamplerStatus.Cancel;
            }
        }
    }

    /// <summary>
    /// DistanceDrawJig
    /// </summary>
    public class DistanceDrawJig : DrawJigExOld
    {
        public abstract class DistanceOptions : OptionsBase
        {
            public abstract JigPromptDistanceOptions Options { get; set; }
            public abstract void UpdateDistance(double dist);
        }

        protected new DistanceOptions Options => base.Options as DistanceOptions;

        public DistanceDrawJig(DistanceOptions options) : base(options)
        {
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            var res = prompts.AcquireDistance(Options.Options);
            switch (res.Status)
            {
                case PromptStatus.OK:
                    {
                        Options.UpdateDistance(res.Value);
                        return SamplerStatus.OK;
                    }
                case PromptStatus.Keyword:
                case PromptStatus.None:
                    return SamplerStatus.OK;
                default:
                    return SamplerStatus.Cancel;
            }
        }
    }

    /// <summary>
    /// AngleDrawJig
    /// </summary>
    public class AngleDrawJig : DrawJigExOld
    {
        public abstract class AngleOptions : OptionsBase
        {
            public abstract JigPromptAngleOptions Options { get; set; }
            public abstract void UpdateAngle(double angle);
        }

        protected new AngleOptions Options => base.Options as AngleOptions;

        public AngleDrawJig(AngleOptions options) : base(options)
        {
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            var res = prompts.AcquireAngle(Options.Options);
            switch (res.Status)
            {
                case PromptStatus.OK:
                    {
                        Options.UpdateAngle(res.Value);
                        return SamplerStatus.OK;
                    }
                case PromptStatus.Keyword:
                case PromptStatus.None:
                    return SamplerStatus.OK;
                default:
                    return SamplerStatus.Cancel;
            }
        }
    }

    /// <summary>
    /// StringDrawJig
    /// </summary>
    public class StringDrawJig : DrawJigExOld
    {
        public abstract class StringOptions : OptionsBase
        {
            public abstract JigPromptStringOptions Options { get; set; }
            public abstract void UpdateString(string str);
        }

        protected new StringOptions Options => base.Options as StringOptions;

        public StringDrawJig(StringOptions options) : base(options)
        {
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            var res = prompts.AcquireString(Options.Options);
            switch (res.Status)
            {
                case PromptStatus.OK:
                    {
                        Options.UpdateString(res.StringResult);
                        return SamplerStatus.OK;
                    }
                case PromptStatus.Keyword:
                case PromptStatus.None:
                    return SamplerStatus.OK;
                default:
                    return SamplerStatus.Cancel;
            }
        }
    }
    /// <summary>
    /// IntegerDrawJig
    /// </summary>
    public class IntegerDrawJigN : DrawJigExN
    {
        public abstract class JigActions : JigActionsBase
        {
            public abstract PromptIntegerOptions Options { get; set; }
            public abstract void UpdateInteger(int integer);
        }

        protected new JigActions Actions => base.Actions as JigActions;

        public IntegerDrawJigN(JigActions actions) : base(actions)
        {
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            var res = Util.Editor().GetInteger(Actions.Options);
            switch (res.Status)
            {
                case PromptStatus.OK:
                    {
                        Actions.UpdateInteger(res.Value);
                        return SamplerStatus.OK;
                    }
                case PromptStatus.Keyword:
                case PromptStatus.None:
                    return SamplerStatus.OK;
                default:
                    return SamplerStatus.Cancel;
            }
        }
    }
}
