﻿#if _IJCAD_
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;

#endif

using System;
using System.Collections.Generic;
using JrxCad.Resources.UnitIns;

namespace JrxCad.Utility
{
    public class UnitIns
    {
        /// <summary>
        /// 挿入時の係数算出
        /// </summary>
        /// <param name=UnitInsRes.Units_docUnitsUnitInsRes.Units_>カレント図面のInsunits</param>
        /// <param name=UnitInsRes.Units_insUnitsUnitInsRes.Units_>挿入対象のInsunits</param>
        /// <returns>係数</returns>
        public static double GetValueFactor(UnitsValue docUnits, UnitsValue insUnits)
        {
            if (insUnits == UnitsValue.Undefined ||
                docUnits == UnitsValue.Undefined) return 1.0;

            return UnitsConverter.GetConversionFactor(insUnits, docUnits);
        }

        /// <summary>
        /// InsUnitsの説明文字列一覧　取得
        /// </summary>
        public static Dictionary<int, string> GetMap()
        {
            return new Dictionary<int, string>
            {
                {0, UnitInsRes.Units_Undefined},
                {1, UnitInsRes.Units_Inches},
                {2, UnitInsRes.Units_Feet},
                {3, UnitInsRes.Units_Miles},
                {4, UnitInsRes.Units_Millimeters},
                {5, UnitInsRes.Units_Centimeters},
                {6, UnitInsRes.Units_Meters},
                {7, UnitInsRes.Units_Kilometers},
                {8, UnitInsRes.Units_MicroInches},
                {9, UnitInsRes.Units_Mils},
                {10, UnitInsRes.Units_Yards},
                {11, UnitInsRes.Units_Angstroms},
                {12, UnitInsRes.Units_Nanometers},
                {13, UnitInsRes.Units_Microns},
                {14, UnitInsRes.Units_Decimeters},
                {15, UnitInsRes.Units_Dekameters},
                {16, UnitInsRes.Units_Hectometers},
                {17, UnitInsRes.Units_Gigameters},
                {18, UnitInsRes.Units_Astronomical},
                {19, UnitInsRes.Units_LightYears},
                {20, UnitInsRes.Units_Parsecs},
                {21, UnitInsRes.Units_USSurveyFeet},
                {22, UnitInsRes.Units_USSurveyInch},
                {23, UnitInsRes.Units_USSurveyYard},
                {24, UnitInsRes.Units_USSurveyMile},
            };
        }

        /// <summary>
        /// InsUnitsの説明文字列　取得
        /// </summary>
        public static string GetName(UnitsValue value)
        {
            var i = (int)value;
            var map = GetMap();
            return map[i];
        }

        /// <summary>
        /// InsUnitsの説明文字列と1mに対する係数　取得
        /// </summary>
        public static (string Name, double perM) GetValue(UnitsValue value)
        {
            switch (value)
            {
                case UnitsValue.Undefined:
                    return (UnitInsRes.Units_Undefined, 1);

                case UnitsValue.Inches:
                    return (UnitInsRes.Units_Inches, 39.37 / 0.999998); //39.37007874
                case UnitsValue.MicroInches:
                    return (UnitInsRes.Units_MicroInches, 39.37 / 0.999998 / 100.0); //39370078.74
#if _AutoCAD_
                case UnitsValue.USSurveyInch:
                    return (UnitInsRes.Units_USSurveyInch, 39.37);
#endif
                case UnitsValue.Mils:
                    return (UnitInsRes.Units_Mils, 39.37 / 0.999998 * 1000.0); //39370.07874

                case UnitsValue.Feet:
                    return (UnitInsRes.Units_Feet, 39.37 / 12.0 / 0.999998); //3.280839895
#if _AutoCAD_
                case UnitsValue.USSurveyFeet:
                    return (UnitInsRes.Units_USSurveyFeet, 39.37 / 12.0); //3.2808333333

                case UnitsValue.Miles:
                    return (UnitInsRes.Units_Miles, 1.0 / 1609.344); //0.0006213712
                case UnitsValue.USSurveyMile:
                    return (UnitInsRes.Units_USSurveyMile, 1.0 / 1609.344 * 0.999998); //0.0006213699

                case UnitsValue.Yards:
                    return (UnitInsRes.Units_Yards, 1.0 / 0.9144); //1.0936132983
                case UnitsValue.USSurveyYard:
                    return (UnitInsRes.Units_USSurveyYard, 1.0 / 0.9144 * 0.999998);
#endif

                case UnitsValue.Millimeters:
                    return (UnitInsRes.Units_Millimeters, 1000);
                case UnitsValue.Centimeters:
                    return (UnitInsRes.Units_Centimeters, 100);
                case UnitsValue.Meters:
                    return (UnitInsRes.Units_Meters, 1);
                case UnitsValue.Kilometers:
                    return (UnitInsRes.Units_Kilometers, 0.001);
                case UnitsValue.Angstroms:
                    return (UnitInsRes.Units_Angstroms, 10000000000);
                case UnitsValue.Nanometers:
                    return (UnitInsRes.Units_Nanometers, 1000000000);
                case UnitsValue.Microns:
                    return (UnitInsRes.Units_Microns, 1000000);
                case UnitsValue.Decimeters:
                    return (UnitInsRes.Units_Decimeters, 10);
                case UnitsValue.Dekameters:
                    return (UnitInsRes.Units_Dekameters, 0.1);
                case UnitsValue.Hectometers:
                    return (UnitInsRes.Units_Hectometers, 0.01);
                case UnitsValue.Gigameters:
                    return (UnitInsRes.Units_Gigameters, 1.0E-9);

                case UnitsValue.Astronomical:
                    return (UnitInsRes.Units_Astronomical, 1.0 / 149597870700); //6.684587122E-12
                case UnitsValue.LightYears:
                    return (UnitInsRes.Units_LightYears, 1.0 / 9460730472580800); //1.057000834E-16
                case UnitsValue.Parsecs:
                    return (UnitInsRes.Units_Parsecs, 1.0 / 30856775810000000); //3.240779289E-17

                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }
    }
}
