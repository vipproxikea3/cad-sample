﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using log4net.Core;
using log4net;
using log4net.Config;
using Exception = System.Exception;

namespace JrxCad.Utility
{
    internal class Logger
    {
        private static ILog Log { get; set; }

        /// <summary>
        /// 初期化
        /// </summary>
        public static void Initialize()
        {
            try
            {
                var assembly = Assembly.GetExecutingAssembly();
                var appPath = Path.GetDirectoryName(assembly.Location);
                var configPath = Path.Combine(appPath, "log4net.config");
                XmlConfigurator.ConfigureAndWatch(new FileInfo(configPath));
                Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            }
            catch (Exception ex)
            {
                Util.Editor().WriteMessage($"Error:Load log4net.dll:{ex.Message}");
            }
            finally
            {
                if (Log == null) Log = NullLogger.GetInstance();
            }
        }

        /// <summary>
        /// 表示先：コマンドプロンプト　/　ログ　/　メッセージ画面
        /// </summary>
        public static void FatalShow(string message,
            [CallerLineNumber] int line = 0, [CallerMemberName] string method = "", [CallerFilePath] string path = "")
        {
            WriteMessage(path, method, line, message, Log.Fatal);
            MsgBox.Error.Show(message);
        }

        /// <summary>
        /// 表示先：コマンドプロンプト　/　ログ　/　メッセージ画面
        /// </summary>
        public static void FatalShow(Exception ex,
            [CallerLineNumber] int line = 0, [CallerMemberName] string method = "", [CallerFilePath] string path = "")
        {
            WriteMessage(path, method, line, ex, Log.Fatal);
            MsgBox.Error.Show(ex.Message);
        }

        /// <summary>
        /// 表示先：コマンドプロンプト　/　ログ　/　メッセージ画面
        /// </summary>
        public static void ErrorShow(string message,
            [CallerLineNumber] int line = 0, [CallerMemberName] string method = "", [CallerFilePath] string path = "")
        {
            WriteMessage(path, method, line, message, Log.Error);
            MsgBox.Error.Show(message);
        }

        /// <summary>
        /// 表示先：コマンドプロンプト　/　ログ　/　メッセージ画面
        /// </summary>
        public static void ErrorShow(Exception ex,
            [CallerLineNumber] int line = 0, [CallerMemberName] string method = "", [CallerFilePath] string path = "")
        {
            WriteMessage(path, method, line, ex, Log.Error);
            MsgBox.Error.Show(ex.Message);
        }

        /// <summary>
        /// 表示先：コマンドプロンプト　/　ログ
        /// </summary>
        public static void Warn(string message,
            [CallerLineNumber] int line = 0, [CallerMemberName] string method = "", [CallerFilePath] string path = "")
        {
            WriteMessage(path, method, line, message, Log.Warn);
        }

        /// <summary>
        /// 表示先：コマンドプロンプト　/　ログ
        /// </summary>
        public static void Warn(Exception ex,
            [CallerLineNumber] int line = 0, [CallerMemberName] string method = "", [CallerFilePath] string path = "")
        {
            WriteMessage(path, method, line, ex, Log.Warn);
        }

        private static void WriteMessage(string path, string method, int line, string message, Action<string> log)
        {
            try
            {
                Util.Editor().WriteMessage(message);

                var fileName = Path.GetFileNameWithoutExtension(path);
                log($"[{fileName}::{method} line{line})] {message}");
            }
            catch
            {
                log(message);
            }
        }

        private static void WriteMessage(string path, string method, int line, Exception ex, Action<string> log)
        {
            try
            {
                Util.Editor().WriteMessage(ex.Message);

                var fileName = Path.GetFileNameWithoutExtension(path);
                log($"{fileName}::{method} ({line}) {ex.Message}");

                if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                    log(ex.InnerException.Message);

                var trace = new StackTrace(ex, true);
                for (var i = 0; i < trace.FrameCount; i++)
                {
                    var frame = trace.GetFrame(i);

                    var traceMethod = frame.GetMethod();
                    var traceLine = frame.GetFileLineNumber();
                    var traceOffset = frame.GetILOffset();
                    var traceOffset16 = Convert.ToString(traceOffset, 16);

                    log($"  at {traceMethod.DeclaringType}.{traceMethod.Name} ({traceLine}) 0x{traceOffset16}");
                }
            }
            catch
            {
                log(ex.Message);
                log(ex.StackTrace);
            }
        }

        /// <summary>
        /// 表示先：ログ
        /// </summary>
        public static void Notice(string message)
        {
            Log.Logger?.Log(MethodBase.GetCurrentMethod().DeclaringType,
                Level.Notice, message, null);
        }

        /// <summary>
        /// 表示先：ログ
        /// </summary>
        public static void Info(string message)
        {
            Log.Info(message);
        }

        /// <summary>
        /// 表示先：ログ
        /// </summary>
        public static void Debug(string message)
        {
            Log.Debug(message);
        }
    }

    /// <summary>
    /// Logger(Log4Net)クラスの Nullオブジェクトパターン
    /// </summary>
    internal sealed class NullLogger : ILog
    {
        //CS0535 対応
        //(http://old.nabble.com/-jira---Created--(LOG4NET-326)-Add-a-null-logger-implementation-td33520340.html)
        public ILogger Logger => null;

        private static NullLogger instance = new NullLogger();

        public static NullLogger GetInstance()
        {
            return instance;
        }

        private NullLogger()
        {
        }

        public bool IsDebugEnabled => false;
        public bool IsInfoEnabled => false;
        public bool IsWarnEnabled => false;
        public bool IsErrorEnabled => false;
        public bool IsFatalEnabled => false;

        public void Debug(object message)
        {
        }

        public void Debug(object message, Exception exception)
        {
        }

        public void DebugFormat(string format, params object[] args)
        {
        }

        public void DebugFormat(string format, object arg0)
        {
        }

        public void DebugFormat(string format, object arg0, object arg1)
        {
        }

        public void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
        }

        public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
        }

        public void Info(object message)
        {
        }

        public void Info(object message, Exception exception)
        {
        }

        public void InfoFormat(string format, params object[] args)
        {
        }

        public void InfoFormat(string format, object arg0)
        {
        }

        public void InfoFormat(string format, object arg0, object arg1)
        {
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
        }

        public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
        }

        public void Warn(object message)
        {
        }

        public void Warn(object message, Exception exception)
        {
        }

        public void WarnFormat(string format, params object[] args)
        {
        }

        public void WarnFormat(string format, object arg0)
        {
        }

        public void WarnFormat(string format, object arg0, object arg1)
        {
        }

        public void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
        }

        public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
        }

        public void Error(object message)
        {
        }

        public void Error(object message, Exception exception)
        {
        }

        public void ErrorFormat(string format, params object[] args)
        {
        }

        public void ErrorFormat(string format, object arg0)
        {
        }

        public void ErrorFormat(string format, object arg0, object arg1)
        {
        }

        public void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
        }

        public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
        }

        public void Fatal(object message)
        {
        }

        public void Fatal(object message, Exception exception)
        {
        }

        public void FatalFormat(string format, params object[] args)
        {
        }

        public void FatalFormat(string format, object arg0)
        {
        }


        public void FatalFormat(string format, object arg0, object arg1)
        {
        }

        public void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
        }

        public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
        }
    }
}
