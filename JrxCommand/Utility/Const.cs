﻿using System;

namespace JrxCad.Utility
{
    internal static class Const
    {
        private const int Eps = 5;
        public static double EpsValue = Math.Pow(0.1, Eps);
        public const string TempLayer = "NewTempLayer";
        public static double IsometricRatio = Math.Sqrt(3.0 / 2);
    }
}
