﻿#if _IJCAD_
using CADApp = GrxCAD.ApplicationServices.Application;
using Application = GrxCAD.ApplicationServices.Application;
using CADException = GrxCAD.Runtime.Exception;
using CADColors = GrxCAD.Colors;
using GrxCAD.Runtime;
using GrxCAD.Geometry;

#elif _AutoCAD_
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using Application = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using CADException = Autodesk.AutoCAD.Runtime.Exception;
using CADColors = Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Geometry;

#endif

using Exception = System.Exception;

using System;
using System.Collections.Generic;
using System.Linq;

namespace JrxCad.Utility
{
    internal static class MathEx
    {
        public const int Eps = 5;
        public static double EpsValue = Math.Pow(0.1, Eps);

        /// <summary>
        /// 値の一致
        /// </summary>
        public static bool IsZero(double value, int eps = Eps)
        {
            return Math.Abs(value) <= Math.Pow(0.1, eps);
        }

        /// <summary>
        /// 値の一致
        /// </summary>
        public static bool IsEqual(double value1, double value2)
        {
            return IsZero(value1 - value2);
        }

        /// <summary>
        /// 座標の一致
        /// </summary>
        public static bool IsEqual(Point3d point1, Point3d point2)
        {
            return point1.IsEqualTo(point2, new Tolerance(EpsValue, EpsValue));
        }

        /// <summary>
        /// 以上
        /// </summary>
        public static bool OrMore(double value1, double value2)
        {
            return IsEqual(value1, value2) || value1 > value2;
        }

        /// <summary>
        /// 以下
        /// </summary>
        public static bool OrLess(double value1, double value2)
        {
            return IsEqual(value1, value2) || value1 < value2;
        }

        /// <summary>
        /// より大きい
        /// </summary>
        public static bool More(double value1, double value2)
        {
            return !OrLess(value1, value2);
        }

        /// <summary>
        /// より小さい
        /// </summary>
        public static bool Less(double value1, double value2)
        {
            return !OrMore(value1, value2);
        }

        /// <summary>
        /// 除算 bunsi / bunbo
        /// </summary>
        public static double Division(double bunsi, double bunbo)
        {
            if (IsZero(bunsi) || IsZero(bunbo)) return 0.0;
            return bunsi / bunbo;
        }

        /// <summary>
        /// 指定範囲内かどうか
        /// </summary>
        public static bool IsRange(double value, double range1, double range2)
        {
            var r1 = IsZero(range1) ? 0.0 : range1;
            var r2 = IsZero(range2) ? 0.0 : range2;
            var v = IsZero(value) ? 0.0 : value;

            return (r1 <= v && v <= r2) || (r2 <= v && v <= r1);
        }

        /// <summary>
        /// 四捨五入
        /// </summary>
        public static int Round(double value)
        {
            var i = (int)value;
            var sisu = value - i;
            return sisu < 0.5 ? i : i + 1;
        }

        /// <summary>
        /// 2次方程式の解　ax^2 + bx + c = 0 a≠0
        /// </summary>
        public static List<double> Func2(double a, double b, double c)
        {
            var s = b * b - 4 * a * c;
            if (IsZero(s))
            {
                var x = -b / (2 * a);
                return new List<double> { x };
            }

            if (s < 0.0) return new List<double>();

            var x1 = (-b + Math.Sqrt(s)) / (2 * a);
            var x2 = (-b - Math.Sqrt(s)) / (2 * a);
            return new List<double> { x1, x2 };
        }

        /// <summary>
        /// 2直線が平行かどうか
        /// a1x+b1y+c1=0 a2x+b2y+c2=0
        /// </summary>
        public static bool IsParallel(double a1, double b1, double a2, double b2)
        {
            return IsZero(b1) && IsZero(b2)
                ? IsEqual(a1, a2)
                : IsEqual(a1 * b2, a2 * b1);
        }

        /// <summary>
        /// 2円の交点算出
        /// </summary>
        public static List<Point3d> GetIntersect(Point3d center1, double radius1, Point3d center2, double radius2)
        {

            var a = center2.X - center1.X;
            var b = center2.Y - center1.Y;
            var c = (Math.Pow(center1.X, 2) - Math.Pow(center2.X, 2) +
                     Math.Pow(center1.Y, 2) - Math.Pow(center2.Y, 2) +
                     Math.Pow(radius2, 2) - Math.Pow(radius1, 2)) * 0.5;
            var d = a * center1.X + b * center1.Y + c;

            var s = (a * a + b * b) * radius1 * radius1 - (d * d);
            var intersects = new List<Point3d>();

            //s＝0 接点
            if (IsZero(s, 2))
            {
                var cx = (-a * d) / (a * a + b * b) + center1.X;
                var cy = (-b * d) / (a * a + b * b) + center1.Y;
                intersects.Add(new Point3d(cx, cy, 0));
                return intersects;
            }

            //s＜0 交点なし
            if (s < 0.0) return intersects;

            //s＞0 2交点
            var cx1 = (-a * d + b * Math.Sqrt(s)) / (a * a + b * b) + center1.X;
            var cy1 = (-b * d - a * Math.Sqrt(s)) / (a * a + b * b) + center1.Y;
            var cx2 = (-a * d - b * Math.Sqrt(s)) / (a * a + b * b) + center1.X;
            var cy2 = (-b * d + a * Math.Sqrt(s)) / (a * a + b * b) + center1.Y;
            intersects.Add(new Point3d(cx1, cy1, 0));
            intersects.Add(new Point3d(cx2, cy2, 0));
            return intersects;
        }

        /// <summary>
        /// 2線分の交点
        /// a1*x+b1*y+c1=0  a2*x+b2*y+c2=0
        /// </summary>
        public static Point3d GetIntersect(double a1, double b1, double c1, double a2, double b2, double c2)
        {
            var x = Division(b2 * c1 - c2 * b1, a2 * b1 - b2 * a1);
            var y = IsEqual(b1, 0.0)
                ? -1.0 * (a2 * x + c2) / b2
                : -1.0 * (a1 * x + c1) / b1;
            return new Point3d(x, y, 0);
        }

        /// <summary>
        /// 汎用正規化処理
        /// </summary>
        /// <param name="val">値</param>
        /// <param name="min">最小値</param>
        /// <param name="max">最大値</param>
        /// <returns>正規化した値</returns>
        private static double NormalizeLoopRange(double val, double min, double max)
        {
            if (min >= max || IsEqual(min, max))
                throw new ArgumentException("need min < max");

            if (val >= max || IsEqual(val, max))
            {
                return min + (val - min) % (max - min);
            }
            else if (val < min)
            {
                return max - (min - val) % (max - min);
            }
            else
            {
                return val;
            }
        }

        /// <summary>
        /// 角度の正規化(ラジアン)
        /// </summary>
        /// <param name="val">値</param>
        /// <returns>正規化角度(0～２π)</returns>
        public static double NormalizeAngle(double val)
        {
            return NormalizeLoopRange(val, 0.0, Math.PI * 2.0);
        }

        public static List<LineSegment3d> GetTangentsToCircle(CircularArc3d circle, CircularArc3d other)
        {
            // check if circles lies on the same plane
            Vector3d normal = circle.Normal;
            Plane plane = new Plane(Point3d.Origin, normal);
            double elev1 = circle.Center.TransformBy(Matrix3d.WorldToPlane(plane)).Z;
            double elev2 = other.Center.TransformBy(Matrix3d.WorldToPlane(plane)).Z;
            if (!(normal.IsParallelTo(other.Normal) && MathEx.IsEqual(elev1, elev2)))
                throw new Autodesk.AutoCAD.Runtime.Exception(
                    Autodesk.AutoCAD.Runtime.ErrorStatus.NonCoplanarGeometry);

            List<LineSegment3d> result = new List<LineSegment3d>();

            // check if a circle is not inside the other
            double dist = circle.Center.DistanceTo(other.Center);
            if (dist <= Math.Abs(circle.Radius - other.Radius))
                return result;

            Point3d center;
            Vector3d vec;

            // external tangents
            bool reverse = false;
            if (MathEx.IsEqual(circle.Radius, other.Radius))
            {
                center = circle.Center;
                normal = circle.Normal;
                vec = other.Center - center;
                Line3d perp = new Line3d(center, vec.CrossProduct(normal));
                var inters = circle.IntersectWith(perp);
                if (inters != null)
                {
                    result.Add(new LineSegment3d(inters[0], inters[0] + vec));
                    result.Add(new LineSegment3d(inters[1], inters[1] + vec));
                }
            }
            else
            {
                if (circle.Radius < other.Radius)
                {
                    reverse = true;
                    var tmp = circle;
                    circle = other;
                    other = tmp;
                }
                center = circle.Center;
                normal = circle.Normal;
                vec = other.Center - center;
                var tmp1 = new CircularArc3d(circle.Center, normal, circle.Radius - other.Radius);
                var tmp2 = new CircularArc3d(center + vec / 2.0, normal, dist / 2.0);
                var inters = tmp1.IntersectWith(tmp2);
                if (inters != null)
                {
                    var vec1 = (inters[0] - center).GetNormal();
                    result.Add(new LineSegment3d(center + vec1 * circle.Radius, other.Center + vec1 * other.Radius));
                    if (inters.Length > 1)
                    {
                        var vec2 = (inters[1] - center).GetNormal();
                        result.Add(new LineSegment3d(center + vec2 * circle.Radius, other.Center + vec2 * other.Radius));
                    }
                }
            }

            // crossing tangents
            if (!MathEx.IsZero(other.Radius) && circle.Radius + other.Radius < dist)
            {
                double ratio = (circle.Radius / (circle.Radius + other.Radius)) / 2.0;
                var tmp1 = new CircularArc3d(center + vec * ratio, normal, dist * ratio);
                var inters = circle.IntersectWith(tmp1);
                if (inters != null)
                {
                    var vec1 = (inters[0] - center).GetNormal();
                    result.Add(new LineSegment3d(center + vec1 * circle.Radius, other.Center + vec1.Negate() * other.Radius));
                    if (inters.Length > 1)
                    {
                        var vec2 = (inters[1] - center).GetNormal();
                        result.Add(new LineSegment3d(center + vec2 * circle.Radius, other.Center + vec2.Negate() * other.Radius));
                    }
                }
            }

            if (reverse) result = result.Select(a => new LineSegment3d(a.EndPoint, a.StartPoint)).ToList();
            return result;
        }

        /// <summary>
        /// ポイントのポリゴン内外判定
        /// </summary>
        /// <param name="pointTarget"></param>
        /// <param name="points"></param>
        /// <returns></returns>
        public static bool IsPointInPolygon(Point3d pointTarget, List<Point3d> points)
        {
            int iCountCrossing = 0;

            var point0 = points[0];
            bool bFlag0X = (pointTarget.X <= point0.X);
            bool bFlag0Y = (pointTarget.Y <= point0.Y);

            // レイの方向は、Ｘプラス方向
            for (int i = 1; i < points.Count + 1; i++)
            {
                var point1 = points[i % points.Count];  // 最後は始点が入る（多角形データの始点と終点が一致していないデータ対応）
                bool bFlag1X = (pointTarget.X <= point1.X);
                bool bFlag1Y = (pointTarget.Y <= point1.Y);
                if (bFlag0Y != bFlag1Y)
                {	// 線分はレイを横切る可能性あり。
                    if (bFlag0X == bFlag1X)
                    {	// 線分の２端点は対象点に対して両方右か両方左にある
                        if (bFlag0X)
                        {	// 完全に右。⇒線分はレイを横切る
                            iCountCrossing += (bFlag0Y ? -1 : 1);	// 上から下にレイを横切るときには、交差回数を１引く、下から上は１足す。
                        }
                    }
                    else
                    {	// レイと交差するかどうか、対象点と同じ高さで、対象点の右で交差するか、左で交差するかを求める。
                        if (pointTarget.X <= (point0.X + (point1.X - point0.X) * (pointTarget.Y - point0.Y) / (point1.Y - point0.Y)))
                        {	// 線分は、対象点と同じ高さで、対象点の右で交差する。⇒線分はレイを横切る
                            iCountCrossing += (bFlag0Y ? -1 : 1);	// 上から下にレイを横切るときには、交差回数を１引く、下から上は１足す。
                        }
                    }
                }
                // 次の判定のために、
                point0 = point1;
                bFlag0X = bFlag1X;
                bFlag0Y = bFlag1Y;
            }

            // クロスカウントがゼロのとき外、ゼロ以外のとき内。
            return (0 != iCountCrossing);
        }

        /// <summary>
        /// 四半円点の傾斜角を取得
        /// </summary>
        /// <param name="vec0">平行四辺形の辺(X軸側)</param>
        /// <param name="vec1">平行四辺形の辺(Y軸側)</param>
        /// <param name="ang">傾斜角</param>
        /// <returns></returns>
        public static bool GetQuadrantsAngle(Vector3d vec0, Vector3d vec1, out double ang)
        {
            // 円を斜めから見た場合、
            // 円に外接する正方形は平行四辺形になり、円はその平行四辺形に内接する楕円になる。
            // 本処理では、その楕円の四半円点(最遠点/最近点)の傾斜角を算出する。
            // ※vec0,vec1はZ=0前提

            // vec1がY軸になるように回転
            var ang1 = vec1.GetAngleTo(Vector3d.YAxis, Vector3d.ZAxis);
            var vec0Rot = vec0.TransformBy(Matrix3d.Rotation(ang1, Vector3d.ZAxis, Point3d.Origin));
            var vec1Rot = vec1.TransformBy(Matrix3d.Rotation(ang1, Vector3d.ZAxis, Point3d.Origin));

            if (Math.Abs(vec0Rot.X) < Tolerance.Global.EqualPoint)
            {
                ang = 0.0;
                return false;
            }

            // 傾斜角
            var k = vec0Rot.Y / vec0Rot.X;
            var l = vec1Rot.Y / vec0Rot.X;
            var ang2 = 0.5 * Math.Atan2(2.0 * k, 1.0 - k * k - l * l);

            ang = ang2 - ang1;
            return true;
        }
    }
}
