#if _IJCAD_
using CADApp = GrxCAD.ApplicationServices.Application;
#elif _AutoCAD_
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
#endif

using System.Windows.Forms;

namespace JrxCad.Utility
{
    public class MsgBox
    {

#if _IJCAD_
        private const string Title = "IJCAD";
#elif _AutoCAD_
        private const string Title = "AutoCAD";
#endif

        public static class Error
        {
            public static void Show(string message)
            {
                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            public static DialogResult Show(string message, string caption)
            {
                return MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static class Ask
        {
            public static bool Show(string question)
            {
                return MessageBox.Show(question, Title, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes;
            }

            public static bool Show(string question, string title, MessageBoxIcon icon)
            {
                return MessageBox.Show(question, title, MessageBoxButtons.YesNo, icon) == DialogResult.Yes;
            }
        }

        public static class Info
        {
            public static void Show(string info)
            {
                MessageBox.Show(info, Title, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public static class Alert
        {
            public static void Show(string info)
            {
                CADApp.ShowAlertDialog(info);
            }
        }
        public static class Warning
        {
            public static DialogResult Show(string message, string caption)
            {
                return MessageBox.Show(message, caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            }
        }
    }
}
