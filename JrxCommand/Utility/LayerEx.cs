#if _IJCAD_
using GrxCAD.ApplicationServices;
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;

#endif

using System;

namespace JrxCad.Utility
{
    /// <summary>
    /// 画層関連
    /// </summary>
    public class LayerEx
    {
        /// <summary>
        /// カレント画層のプロパティ取得
        /// </summary>
        public T GetProperty<T>(Document doc, Func<LayerTableRecord, T> property)
        {
            using (var tr = Util.StartTransaction())
            {
                var layer = (LayerTableRecord) tr.GetObject(doc.Database.Clayer, OpenMode.ForRead);
                return property(layer);
            }
        }

        /// <summary>
        /// カレント画層にセット
        /// </summary>
        /// <param name="layerName">画層名</param>
        public void SetCurrent(string layerName)
        {
            var doc = Util.CurDoc();
            using (doc.LockDocument())
            using (var tr = Util.StartTransaction())
            {
                var acLayerTable = (LayerTable) tr.GetObject(doc.Database.LayerTableId, OpenMode.ForRead);
                if (!acLayerTable.Has(layerName)) return;

                doc.Database.Clayer = acLayerTable[layerName];
                tr.Commit();
            }
        }

        /// <summary>
        /// カレント画層をコピーして指定名の画層作成
        /// </summary>
        /// <param name="layerName">画層名</param>
        /// <param name="createLayer">指定画層が既に図面に存在した場合、画層名を再作成</param>
        public void Create(string layerName, Func<LayerTable, string> createLayer)
        {
            var doc = Util.CurDoc();
            using (doc.LockDocument())
            using (var tr = Util.StartTransaction())
            {
                var acLayerTable = (LayerTable) tr.GetObject(doc.Database.LayerTableId, OpenMode.ForRead);
                if (acLayerTable.Has(layerName))
                {
                    layerName = createLayer(acLayerTable);
                }

                //カレント画層をコピー
                var layerTableRecord = (LayerTableRecord) tr.GetObject(doc.Database.Clayer, OpenMode.ForRead)
                                                            .Clone();
                layerTableRecord.Name = layerName;

                acLayerTable.UpgradeOpen();
                acLayerTable.Add(layerTableRecord);
                tr.AddNewlyCreatedDBObject(layerTableRecord, true);
                tr.Commit();
            }
        }

        /// <summary>
        /// 画層削除
        ///   0:Success, -1:画層なし, -2:削除不可
        /// </summary>
        /// <param name="layerName">削除する画層名</param>
        /// <returns>0:Success, -1:画層なし, -2:削除不可</returns>
        public int Delete(string layerName)
        {
            var doc = Util.CurDoc();
            using (doc.LockDocument())
            using (var tr = Util.StartTransaction())
            {
                var acLayerTable = (LayerTable) tr.GetObject(doc.Database.LayerTableId, OpenMode.ForRead);
                if (!acLayerTable.Has(layerName)) return -1;

                var objects = new ObjectIdCollection {acLayerTable[layerName]};

                //削除不可の画層を除外
                doc.Database.Purge(objects);
                if (objects.Count == 0) return -2;

                if (layerName.Equals("Defpoints", StringComparison.CurrentCultureIgnoreCase)) return -2;

                var acLyrTblRec = (LayerTableRecord) tr.GetObject(objects[0], OpenMode.ForWrite);
                acLyrTblRec.Erase(true);
                tr.Commit();
            }

            return 0;
        }

        /// <summary>
        /// Propertyの値(True/False)を切り替える
        /// ※bool型のPropertyに限る(IsOff, IsFrozen,・・・)
        /// </summary>
        /// <param name="layerName">画層名</param>
        /// <param name="propertyName">値を切り替えるProperty名</param>
        public void SwitchBool(string layerName, string propertyName)
        {
            var doc = Util.CurDoc();
            using (doc.LockDocument())
            using (var tr = Util.StartTransaction())
            {
                var acLayerTable = tr.GetObject(doc.Database.LayerTableId, OpenMode.ForRead) as LayerTable;
                if (acLayerTable == null) return;
                if (!acLayerTable.Has(layerName)) return;

                var layerRecord = (LayerTableRecord)tr.GetObject(acLayerTable[layerName], OpenMode.ForWrite);
                var property = typeof(LayerTableRecord).GetProperty(propertyName);
                if (property == null || !(property.GetValue(layerRecord) is bool boolValue)) return;

                property.SetValue(layerRecord, !boolValue);
                tr.Commit();
            }
        }

        /// <summary>
        /// Propertyの値更新
        /// </summary>
        /// <param name="layerName">画層名</param>
        /// <param name="propertyName">値を切り替えるProperty名</param>
        /// <param name="value">値</param>
        public void Update(string layerName, string propertyName, object value)
        {
            var doc = Util.CurDoc();
            using (doc.LockDocument())
            using (var tr = Util.StartTransaction())
            {
                var acLayerTable = tr.GetObject(doc.Database.LayerTableId, OpenMode.ForRead) as LayerTable;
                if (acLayerTable == null) return;
                if (!acLayerTable.Has(layerName)) return;

                var layerRecord = (LayerTableRecord)tr.GetObject(acLayerTable[layerName], OpenMode.ForWrite);
                var property = typeof(LayerTableRecord).GetProperty(propertyName);
                if (property == null) return;

                property.SetValue(layerRecord, value);
                tr.Commit();
            }
        }

        /// <summary>
        /// Propertyの値更新
        /// </summary>
        /// <param name="layerName">画層名</param>
        /// <param name="propertyName">値を切り替えるProperty名</param>
        /// <param name="value">値</param>
        public void UpdateVp(string layerName, string propertyName, object value)
        {
            var doc = Util.CurDoc();
            using (doc.LockDocument())
            using (var tr = Util.StartTransaction())
            {
                var acLayerTable = tr.GetObject(doc.Database.LayerTableId, OpenMode.ForRead) as LayerTable;
                if (acLayerTable == null) return;
                if (!acLayerTable.Has(layerName)) return;

                var record = (LayerTableRecord)tr.GetObject(acLayerTable[layerName], OpenMode.ForWrite);
                var vpId = doc.Editor.CurrentViewportObjectId == ObjectId.Null
                    ? ObjectId.Null
                    : tr.GetObject(doc.Editor.CurrentViewportObjectId, OpenMode.ForRead).ObjectId;
                var vpRecord = record.GetViewportOverrides(vpId);
                var property = typeof(LayerViewportProperties).GetProperty(propertyName);
                if (property == null) return;

                property.SetValue(vpRecord, value);
                tr.Commit();
            }
        }
    }
}
