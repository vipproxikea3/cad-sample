#if _IJCAD_
using CADColors = GrxCAD.Colors;

#elif _AutoCAD_
using CADColors = Autodesk.AutoCAD.Colors;

#endif

using System;
using Exception = System.Exception;
using Color = System.Windows.Media.Color;

namespace JrxCad.Utility
{
    internal class ColorEx
    {
        /// <summary>
        /// 「R,G,B」文字列からRGB値を取得
        /// </summary>
        public static bool ParseRgb(string rgb, out byte r, out byte g, out byte b)
        {
            r = 0;
            g = 0;
            b = 0;

            var split = rgb.Split(',');
            if (split.Length != 3) return false;

            if (!byte.TryParse(split[0], out r)) return false;
            if (!byte.TryParse(split[1], out g)) return false;
            if (!byte.TryParse(split[2], out b)) return false;

            return true;
        }

        /// <summary>
        /// Color名称からColor変換
        /// </summary>
        public static CADColors.Color GetColor(string color)
        {
            try
            {
                if (short.TryParse(color, out var colorIndex))
                {
                    return CADColors.Color.FromColorIndex(CADColors.ColorMethod.ByAci, colorIndex);
                }

                if (color.StartsWith("RGB:"))
                {
                    var rgb = color.Replace("RGB:", "").Split(',');
                    return CADColors.Color.FromRgb(byte.Parse(rgb[0]), byte.Parse(rgb[1]), byte.Parse(rgb[2]));
                }

                if (color.Contains("$"))
                {
                    var colorBook = color.Split('$');
                    return CADColors.Color.FromNames(colorBook[1], colorBook[0]);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            return CADColors.Color.FromColorIndex(CADColors.ColorMethod.ByAci, 1);
        }
    }
}
