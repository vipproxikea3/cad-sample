#if _IJCAD_
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;

#endif

using System.Diagnostics;
using System.Reflection;
using JrxCad;
using JrxCad.Utility;
using JrxCad.View;
using Exception = System.Exception;

[assembly: ExtensionApplication(typeof(ExtendApplication))]

namespace JrxCad
{
    /// <summary>
    /// アプリケーション管理部
    /// </summary>
    /// <remarks>
    /// CADにDLLが呼ばれた際に行う初期化と、DLLが破棄される際に行う
    /// 解放処理を定義します。
    /// </remarks>
    public class ExtendApplication : IExtensionApplication
    {
        /// <summary>
        /// アプリケーションの初期化処理
        /// </summary>
        public void Initialize()
        {
            try
            {
                Logger.Initialize();

                var dll = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);
                Logger.Info($"■■■　START 【Ver.{dll.FileVersion}】　■■■");
            }
            catch (Exception ex)
            {
                Logger.Warn(ex);
            }

            ScreenDpi.Initialize();
        }

        /// <summary>
        /// アプリケーションの解放処理
        /// </summary>
        public void Terminate()
        {
            Logger.Info("■■■　END　■■■");
        }
    }
}
