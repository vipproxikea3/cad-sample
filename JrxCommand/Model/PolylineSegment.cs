﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
#endif

namespace JrxCad.Model
{
    /// <summary>
    /// Represents a Polyline segment.
    /// </summary>
    public class PolylineSegment
    {
        #region Fields

        private Point2d _startPoint, _endPoint;
        private double _bulge, _startWidth, _endWidth;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the segment start point.
        /// </summary>
        public Point2d StartPoint
        {
            get { return _startPoint; }
            set { _startPoint = value; }
        }

        /// <summary>
        /// Gets or sets the segment end point.
        /// </summary>
        public Point2d EndPoint
        {
            get { return _endPoint; }
            set { _endPoint = value; }
        }

        /// <summary>
        /// Gets or sets the segment bulge.
        /// </summary>
        public double Bulge
        {
            get { return _bulge; }
            set { _bulge = value; }
        }

        /// <summary>
        /// Gets or sets the segment start width.
        /// </summary>
        public double StartWidth
        {
            get { return _startWidth; }
            set { _startWidth = value; }
        }

        /// <summary>
        /// Gets or sets the segment end width.
        /// </summary>
        public double EndWidth
        {
            get { return _endWidth; }
            set { _endWidth = value; }
        }

        /// <summary>
        /// Gets true if the segment is linear.
        /// </summary>
        public bool IsLinear
        {
            get { return _bulge == 0.0; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of PolylineSegment from two points and a bulge.
        /// </summary>
        /// <param name="startPoint">The start point of the segment.</param>
        /// <param name="endPoint">The end point of the segment.</param>
        /// <param name="bulge">The bulge of the segment.</param>
        public PolylineSegment(Point2d startPoint, Point2d endPoint, double bulge)
        {
            _startPoint = startPoint;
            _endPoint = endPoint;
            _bulge = bulge;
            _startWidth = 0;
            _endWidth = 0;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the parameter value of point.
        /// </summary>
        /// <param name="point">The Point 2d whose get the PolylineSegment parameter at.</param>
        /// <returns>A double between 0 and 1, or -1 if the point does not lie on the segment.</returns>
        public double GetParameterOf(Point2d pt)
        {
            if (IsLinear)
            {
                var line = ToLineSegment();
                return line.IsOn(pt) ? _startPoint.GetDistanceTo(pt) / line.Length : -1;
            }
            else
            {
                var arc = ToCircularArc();
                return arc.IsOn(pt) ?
                    arc.GetLength(arc.GetParameterOf(_startPoint), arc.GetParameterOf(pt)) /
                    arc.GetLength(arc.GetParameterOf(_startPoint), arc.GetParameterOf(_endPoint)) : -1;
            }
        }

        /// <summary>
        /// Converts the PolylineSegment into a LineSegment2d.
        /// </summary>
        /// <returns>A new LineSegment2d instance or null if the PolylineSegment bulge is not equal to 0.</returns>
        public LineSegment2d ToLineSegment()
        {
            return IsLinear ? new LineSegment2d(_startPoint, _endPoint) : null;
        }

        /// <summary>
        /// Converts the PolylineSegment into a CircularArc2d.
        /// </summary>
        /// <returns>A new CircularArc2d instance or null if the PolylineSegment bulge is equal to 0.</returns>
        public CircularArc2d ToCircularArc()
        {
            return IsLinear ? null : new CircularArc2d(_startPoint, _endPoint, _bulge, false);
        }

        /// <summary>
        /// Converts the PolylineSegment into a Curve2d.
        /// </summary>
        /// <returns>A new Curve2d instance.</returns>
        public Curve2d ToCurve2d()
        {
            return IsLinear ?
                (Curve2d)(new LineSegment2d(_startPoint, _endPoint)) :
                (Curve2d)(new CircularArc2d(_startPoint, _endPoint, _bulge, false));
        }

        #endregion
    }
}
