﻿#if _IJCAD_
using GrxCAD.Geometry;
using CADApp = GrxCAD.ApplicationServices.Application;
#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
#endif
using System.Collections.Generic;
using System;

namespace JrxCad.Helpers
{
    public static class Point3dHelper
    {
        /// <summary>
        /// 2点間の中点
        /// </summary>
        public static Point3d CenterPoint(this Point3d p1, Point3d p2)
        {
            return new Point3d((p1.X + p2.X) * 0.5, (p1.Y + p2.Y) * 0.5, (p1.Z + p2.Z) * 0.5);
        }

        /// <summary>
        /// p1がp2より左下側にあるかどうか
        /// </summary>
        public static bool LeftBottom(this Point3d p1, Point3d p2)
        {
            if (p1.X.IsEqual(p2.X))
            {
                return p1.Y < p2.Y;
            }

            return p1.X < p2.X;
        }

        /// <summary>
        /// 高度を取得
        /// </summary>
        public static double GetElevation(this Point3d ptW, Matrix3d ucsToWcs)
        {
            var ptU = ptW.TransformBy(ucsToWcs.Inverse());
            return ptU.Z;
        }

        /// <summary>
        /// ポイントのポリゴン内外判定
        /// </summary>
        public static bool IsPointInPolygon(this Point3d pointTarget, List<Point3d> points)
        {
            var iCountCrossing = 0;

            var point0 = points[0];
            var bFlag0X = (pointTarget.X <= point0.X);
            var bFlag0Y = (pointTarget.Y <= point0.Y);

            // レイの方向は、Ｘプラス方向
            for (var i = 1; i < points.Count + 1; i++)
            {
                var point1 = points[i % points.Count];  // 最後は始点が入る（多角形データの始点と終点が一致していないデータ対応）
                var bFlag1X = (pointTarget.X <= point1.X);
                var bFlag1Y = (pointTarget.Y <= point1.Y);
                if (bFlag0Y != bFlag1Y)
                {   // 線分はレイを横切る可能性あり。
                    if (bFlag0X == bFlag1X)
                    {   // 線分の２端点は対象点に対して両方右か両方左にある
                        if (bFlag0X)
                        {   // 完全に右。⇒線分はレイを横切る
                            iCountCrossing += (bFlag0Y ? -1 : 1);   // 上から下にレイを横切るときには、交差回数を１引く、下から上は１足す。
                        }
                    }
                    else
                    {   // レイと交差するかどうか、対象点と同じ高さで、対象点の右で交差するか、左で交差するかを求める。
                        if (pointTarget.X <= (point0.X + (point1.X - point0.X) * (pointTarget.Y - point0.Y) / (point1.Y - point0.Y)))
                        {   // 線分は、対象点と同じ高さで、対象点の右で交差する。⇒線分はレイを横切る
                            iCountCrossing += (bFlag0Y ? -1 : 1);   // 上から下にレイを横切るときには、交差回数を１引く、下から上は１足す。
                        }
                    }
                }
                // 次の判定のために、
                point0 = point1;
                bFlag0X = bFlag1X;
                bFlag0Y = bFlag1Y;
            }

            // クロスカウントがゼロのとき外、ゼロ以外のとき内。
            return (0 != iCountCrossing);
        }

        public static Vector3d getUCSZAxis()
        {

            var x = CADApp.DocumentManager.MdiActiveDocument.Database.Ucsxdir;
            var y = CADApp.DocumentManager.MdiActiveDocument.Database.Ucsydir;
            return x.CrossProduct(y);
        }

        public static Vector3d getUCSXAxis()
        {
            var x = CADApp.DocumentManager.MdiActiveDocument.Database.Ucsxdir;
            return x;
        }
        public static Point3d GetCenterFromThreePoints(Point3d startPt, Point3d secondPt, Point3d endPt)
        {
            var circularArc3d = new CircularArc3d(startPt, secondPt, endPt);
            return circularArc3d.Center;
            //var angleA = (secondPt - startPt).GetAngleTo(endPt - startPt);
            //var angleB = (startPt - secondPt).GetAngleTo(endPt - secondPt);
            //var angleC = (startPt - endPt).GetAngleTo(secondPt - endPt);
            //var sin2A = Math.Sin(2 * angleA);
            //var sin2B = Math.Sin(2 * angleB);
            //var sin2C = Math.Sin(2 * angleC);
            //var x0 = (startPt.X * sin2A + secondPt.X * sin2B + endPt.X * sin2C) / (sin2A + sin2B + sin2C);
            //var y0 = (startPt.Y * sin2A + secondPt.Y * sin2B + endPt.Y * sin2C) / (sin2A + sin2B + sin2C);
            //var z0 = (startPt.Z * sin2A + secondPt.Z * sin2B + endPt.Z * sin2C) / (sin2A + sin2B + sin2C);
            //return new Point3d(x0, y0, z0);
        }

        public static Point3d GetCenterFrom(Point3d startPt, Point3d endPt, double radius, bool reverse = false)
        {
            var StartEndVec = endPt - startPt;
            var t = System.Math.Sqrt((radius / StartEndVec.Length) * (radius / StartEndVec.Length) - 0.25);

            if (reverse) t *= -1;
            var xo = ((startPt.X + endPt.X) / 2) - (t * (endPt.Y - startPt.Y));
            var yo = ((startPt.Y + endPt.Y) / 2) + (t * (endPt.X - startPt.X));
            var zo = startPt.Z;
            return new Point3d(xo, yo, zo);
        }
        public static Point3d RoundCoordOfPoint(ref Point3d inputPoint)
        {
            return new Point3d(Math.Round(inputPoint.X, 4), Math.Round(inputPoint.Y, 4), Math.Round(inputPoint.Z, 4));
        }

        /// <summary>
        /// check if p1 and p2 are the same point
        /// </summary>
        public static bool IsSamePoint(this Point3d p1, Point3d p2)
        {
            return p1.X.IsEqual(p2.X) && p1.Y.IsEqual(p2.Y) && p1.Z.IsEqual(p2.Z);
        }
    }
}
