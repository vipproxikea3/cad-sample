﻿#if _IJCAD_
using GrxCAD.Geometry;

#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
#endif

namespace JrxCad.Helpers
{
    public static class StringHelper
    {
        /// <summary>
        /// 文字列(x,y,z)をPoint3dに変換
        /// </summary>
        public static bool TryParse3d(this string value, out Point3d point)
        {
            point = default;
            try
            {
                var split = value.Split(',');
                if (split.Length != 3) return false;

                var x = double.Parse(split[0].Replace("(", ""));
                var y = double.Parse(split[1]);
                var z = double.Parse(split[2].Replace(")", ""));
                point = new Point3d(x, y, z);

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 文字列(x,y)をPoint2dに変換
        /// </summary>
        public static bool TryParse2d(this string value, out Point2d point)
        {
            point = default;
            try
            {
                var split = value.Split(',');
                if (split.Length != 2) return false;

                var x = double.Parse(split[0].Replace("(", ""));
                var y = double.Parse(split[1].Replace(")", ""));
                point = new Point2d(x, y);

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
