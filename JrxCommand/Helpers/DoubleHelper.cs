﻿using JrxCad.Utility;
using System;
using Autodesk.AutoCAD.DatabaseServices;

namespace JrxCad.Helpers
{
    public static class DoubleHelper
    {
        /// <summary>
        /// 値の一致
        /// </summary>
        public static bool IsZero(this double value)
        {
            return Math.Abs(value) <= Const.EpsValue;
        }

        /// <summary>
        /// 値の一致
        /// </summary>
        public static bool IsEqual(this double value1, double value2)
        {
            return (value1 - value2).IsZero();
        }

        /// <summary>
        /// 以上
        /// </summary>
        public static bool OrMore(this double value1, double value2)
        {
            return value1.IsEqual(value2) || value1 > value2;
        }

        /// <summary>
        /// 以下
        /// </summary>
        public static bool OrLess(this double value1, double value2)
        {
            return value1.IsEqual(value2) || value1 < value2;
        }

        /// <summary>
        /// より大きい
        /// </summary>
        public static bool More(this double value1, double value2)
        {
            return !value1.OrLess(value2);
        }

        /// <summary>
        /// より小さい
        /// </summary>
        public static bool Less(this double value1, double value2)
        {
            return !value1.OrMore(value2);
        }

        /// <summary>
        /// 除算 bunsi / bunbo
        /// </summary>
        public static double Division(this double bunsi, double bunbo)
        {
            if (bunsi.IsZero() || bunbo.IsZero()) return 0.0;
            return bunsi / bunbo;
        }

        /// <summary>
        /// 指定範囲内かどうか
        /// </summary>
        public static bool IsRange(this double value, double range1, double range2)
        {
            var r1 = range1.IsZero() ? 0.0 : range1;
            var r2 = range2.IsZero() ? 0.0 : range2;
            var v = value.IsZero() ? 0.0 : value;

            return (r1 <= v && v <= r2) || (r2 <= v && v <= r1);
        }

        /// <summary>
        /// 四捨五入
        /// </summary>
        public static int Round(this double value)
        {
            var i = (int)value;
            var sisu = value - i;
            return sisu < 0.5 ? i : i + 1;
        }

        /// <summary>
        /// 汎用正規化処理
        /// </summary>
        /// <param name="val">値</param>
        /// <param name="min">最小値</param>
        /// <param name="max">最大値</param>
        /// <returns>正規化した値</returns>
        private static double NormalizeLoopRange(double val, double min, double max)
        {
            if (min >= max || min.IsEqual(max))
                throw new ArgumentException("need min < max");

            if (val >= max || val.IsEqual(max))
            {
                return min + (val - min) % (max - min);
            }
            else if (val < min)
            {
                return max - (min - val) % (max - min);
            }
            else
            {
                return val;
            }
        }

        /// <summary>
        /// 角度の正規化(ラジアン)
        /// </summary>
        /// <param name="radian">ラジアン</param>
        /// <returns>正規化角度(0～２π)</returns>
        public static double NormalizeAngle(this double radian)
        {
            return NormalizeLoopRange(radian, 0.0, Math.PI * 2.0);
        }

        /// <summary>
        /// Round up value to EpsValue digits
        /// </summary>
        public static double ToFloorValue(this double value)
        {
            return Math.Floor(value + Const.EpsValue);
        }

        /// <summary>
        /// Round up value to  digits
        /// </summary>
        public static double ToTestVal(this double value)
        {
            const int eps = 4; //since the property dialog only show 4 digits
            return Math.Round(value, eps);
        }

        /// <summary>
        /// Convert Angle from Degrees to Radians
        /// </summary>
        /// <param name="degree"></param>
        /// <returns></returns>
        public static double DegreesToRadians(this double degree)
        {
            return (Math.PI * degree) / 180;
        }

        /// <summary>
        /// Convert Angle from Radians to Degrees
        /// </summary>
        /// <param name="radian"></param>
        /// <returns></returns>
        public static double RadiansToDegrees(this double radian)
        {
            return (radian * 180) / Math.PI;
        }
    }
}
