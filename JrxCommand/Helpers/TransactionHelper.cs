﻿#if _IJCAD_
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
#endif
using JrxCad.Utility;

namespace JrxCad.Helpers
{
    public static class TransactionHelper
    {
        public static BlockTableRecord CurrentSpace(this Transaction tr, OpenMode openMode = OpenMode.ForWrite)
        {
            return tr.GetObject(Util.CurDoc().Database.CurrentSpaceId, openMode) as BlockTableRecord;
        }

        public static void AddNewlyCreatedDBObject(this Transaction tr, Entity entity, bool add, BlockTableRecord space)
        {
            space.AppendEntity(entity);
            tr.AddNewlyCreatedDBObject(entity, add);
        }

        public static T GetObject<T>(this Transaction tr, ObjectId id, OpenMode mode, bool openErased = false) where T : DBObject
        {
            if (id == ObjectId.Null) return null;
            return tr.GetObject(id, mode, openErased) as T;
        }

        public static T GetObject<T>(this Transaction tr, ObjectId id, OpenMode mode, bool openErased, bool forceOpenOnLockedLayer) where T : DBObject
        {
            if (id == ObjectId.Null) return null;
            return tr.GetObject(id, mode, openErased, forceOpenOnLockedLayer) as T;
        }
    }
}
