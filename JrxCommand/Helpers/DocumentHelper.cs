﻿using GrxCAD.ApplicationServices;
using GrxCAD.DatabaseServices;
using JrxCad.Utility;

namespace JrxCad.Helpers
{
    public static class DocumentHelper
    {
        public static Transaction StartTransaction(this Document doc)
        {
            return doc.Database.TransactionManager.StartTransaction();
        }

    }
}