
#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

#endif

namespace JrxCad.Helpers
{
    public static class Extents3dHelper
    {
        /// <summary>
        /// Extentsを広げる
        /// </summary>
        public static Extents3d ExpandBy(this Extents3d extents, double buffer)
        {
            extents.ExpandBy(new Vector3d(buffer, buffer, 0));
            extents.ExpandBy(new Vector3d(-buffer, -buffer, 0));
            return extents;
        }

        /// <summary>
        /// 中心点を返す
        /// </summary>
        public static Point3d CenterPoint(this Extents3d extents)
        {
            var vector = extents.MaxPoint - extents.MinPoint;
            return extents.MinPoint + vector.DivideBy(2.0);
        }

        /// <summary>
        /// 中心点を返す
        /// </summary>
        public static Point2d CenterPoint2d(this Extents3d extents)
        {
            var point = extents.CenterPoint();
            return new Point2d(point.X, point.Y);
        }

        /// <summary>
        /// 幅を返す
        /// </summary>
        public static double Width(this Extents3d extents)
        {
            return extents.MaxPoint.X - extents.MinPoint.X;
        }

        /// <summary>
        /// 高さを返す
        /// </summary>
        public static double Height(this Extents3d extents)
        {
            return extents.MaxPoint.Y - extents.MinPoint.Y;
        }
    }
}
