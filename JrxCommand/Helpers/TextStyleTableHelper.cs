﻿#if _IJCAD_
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;

#endif

using System;

namespace JrxCad.Helpers
{
    public static class TextStyleTableHelper
    {
        /// <summary>
        /// 指定ファイル名のTextStyleが存在するかどうか
        /// </summary>
        public static bool Exist(this TextStyleTable textStyleTable, string fileName)
        {
            foreach (var id in textStyleTable)
            {
                using (var textStyle = id.GetObject(OpenMode.ForRead) as TextStyleTableRecord)
                {
                    if (textStyle == null) continue;

                    if (fileName.Equals(textStyle.FileName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
