﻿#if _IJCAD_
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.GraphicsInterface;
using Autodesk.AutoCAD.Internal;
#endif
using System;
using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;
using JrxCad.Utility;

namespace JrxCad.Helpers
{
    public static class BitmapImageHelper
    {
        private static ImageBGRA32 _img;

        public static Bitmap BitmapImage2Bitmap(this BitmapImage bitmapImage, int width, int height)
        {
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                Bitmap bitmap = new Bitmap(outStream);
                Bitmap result = new Bitmap(width, height);
                using (Graphics g = Graphics.FromImage(result))
                {
                    g.DrawImage(bitmap, 0, 0, width, height);
                }

                return result;
            }
        }

        /// <summary>
        /// Remove icon cursor
        /// </summary>
        public static void RemoveCursorBadge()
        {
#if _AutoCAD_
            var cursorBadgeUtilities = new CursorBadgeUtilities();
            if (cursorBadgeUtilities.HasSupplementalCursorImage() && _img != null)
            {
                cursorBadgeUtilities.RemoveSupplementalCursorImage(_img);
                _img = null;
            }
#elif _IJCAD_
            // TODO: IJCAD does not support CursorBadgeUtilities
#endif
        }

        /// <summary>
        /// Change icon cursor
        /// </summary>
        public static void AddCursorBadge(string uri)
        {
            if (string.IsNullOrWhiteSpace(uri)) return;
#if _AutoCAD_
            if (_img == null)
            {
                var cursorIcon = new BitmapImage(new Uri(uri));
                Bitmap bitmap = cursorIcon.BitmapImage2Bitmap(16, 16);
                bitmap.RotateFlip(RotateFlipType.Rotate180FlipX);
                _img = Utils.ConvertBitmapToAcGiImageBGRA32(bitmap);
            }
            var cursorBadgeUtilities = new CursorBadgeUtilities();
            cursorBadgeUtilities.AddSupplementalCursorImage(_img, 1);
#elif _IJCAD_
            // TODO: IJCAD does not support CursorBadgeUtilities
#endif
        }

        /// <summary>
        /// Change icon cursor
        /// </summary>
        public static void AddRemoveIcon2Cursor()
        {
#if _AutoCAD_
            if (_img == null)
            {
                using (var bmp = new Bitmap(16, 16))
                using (var g = Graphics.FromImage(bmp))
                {
                    g.Clear(Color.Magenta);
                    g.RotateTransform((float)Util.Database().Angbase);
                    using (var redPen = new Pen(Color.Red, 1.8f))
                    {
                        g.DrawLine(redPen, 3f, 3f, 13f, 13f);
                        g.DrawLine(redPen, 3f, 13f, 13f, 3f);
                    }
                    bmp.RotateFlip(RotateFlipType.Rotate180FlipX);
                    _img = Utils.ConvertBitmapToAcGiImageBGRA32(bmp);
                }
            }
            var cursorBadgeUtilities = new CursorBadgeUtilities();
            cursorBadgeUtilities.AddSupplementalCursorImage(_img, 1);
#elif _IJCAD_
            // TODO: IJCAD does not support CursorBadgeUtilities
#endif
        }
    }
}
