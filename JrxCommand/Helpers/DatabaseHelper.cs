#if _IJCAD_
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
#endif

using JrxCad.Utility;

namespace JrxCad.Helpers
{
    public static class DatabaseHelper
    {
        /// <summary>
        /// アプリケーション名登録
        /// </summary>
        public static void RegAppName(this Database db, string appName)
        {
            using (var tr = Util.StartTransaction())
            using (var regTable = tr.GetObject(db.RegAppTableId, OpenMode.ForRead, false) as RegAppTable)
            {
                if (regTable != null && !regTable.Has(appName))
                {
                    // 新しく拡張情報アプリケーション名を定義します。
                    using (var regTableRecord = new RegAppTableRecord())
                    {
                        regTableRecord.Name = appName;
                        regTable.UpgradeOpen();
                        regTable.Add(regTableRecord);
                        tr.AddNewlyCreatedDBObject(regTableRecord, true);
                        tr.Commit();
                    }
                }
            }
        }
    }
}
