﻿#if _IJCAD_
using GrxCAD.Geometry;
#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
#endif
using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using JrxCad.Command.ArrayClassic.Model;

namespace JrxCad.Helpers
{
    public class CanvasHelper
    {
        private const int Radius = 50;

        /// <summary>
        /// Erase all elements that are appearing in Canvas Window
        /// </summary>
        /// <param name="canvas">Canvas Window</param>
        public static void ClearAll(Canvas canvas)
        {
            canvas.Children.Clear();
        }

        /// <summary>
        /// Initialize Axis of Polar Preview
        /// </summary>
        /// <param name="canvas">Canvas Window</param>
        public static void PolarAxisInitialization(Canvas canvas)
        {
            ClearAll(canvas);
            var circle = new Ellipse()
            {
                Width = Radius * 2,
                Height = Radius * 2,
                Stroke = Brushes.Black,
                StrokeThickness = 0.2,
                StrokeDashArray = { 5, 5 }
            };
            circle.SetValue(Canvas.LeftProperty, OxyConverter(canvas, -Radius));
            circle.SetValue(Canvas.TopProperty, OxyConverter(canvas, -Radius));
            _ = canvas.Children.Add(circle);

            var ox = new Line
            {
                Stroke = Brushes.Black,
                StrokeThickness = 0.5,
                X1 = OxyConverter(canvas, 0),
                Y1 = OxyConverter(canvas, -4),
                X2 = OxyConverter(canvas, 0),
                Y2 = OxyConverter(canvas, 4),
            };
            var oy = new Line
            {
                Stroke = Brushes.Black,
                StrokeThickness = 0.5,
                X1 = OxyConverter(canvas, -4),
                Y1 = OxyConverter(canvas, 0),
                X2 = OxyConverter(canvas, 4),
                Y2 = OxyConverter(canvas, 0),
            };
            _ = canvas.Children.Add(ox);
            _ = canvas.Children.Add(oy);
        }

        /// <summary>
        /// Initialize Axis of Rectangular' Preview
        /// </summary>
        /// <param name="canvas">Canvas Window</param>
        public static void RectAxisInitialization(Canvas canvas)
        {
            ClearAll(canvas);
            var ox = new Line
            {
                Stroke = Brushes.Black,
                StrokeDashArray = { 15, 15 },
                StrokeThickness = 0.15,
                X1 = OxyConverter(canvas, 0),
                Y1 = OxyConverter(canvas, -70),
                X2 = OxyConverter(canvas, 0),
                Y2 = OxyConverter(canvas, 70),
            };
            var oy = new Line
            {
                Stroke = Brushes.Black,
                StrokeDashArray = { 15, 15 },
                StrokeThickness = 0.15,
                X1 = OxyConverter(canvas, -70),
                Y1 = OxyConverter(canvas, 0),
                X2 = OxyConverter(canvas, 70),
                Y2 = OxyConverter(canvas, 0),
            };
            _ = canvas.Children.Add(ox);
            _ = canvas.Children.Add(oy);
        }

        /// <summary>
        /// Erase rect preview
        /// </summary>
        /// <param name="canvas">Canvas Window</param>
        public static void ClearRectPreview(Canvas canvas)
        {
            for (var i = canvas.Children.Count - 1; i >= 0; i += -1)
            {
                var child = canvas.Children[i];
                if (child is Rectangle)
                {
                    canvas.Children.Remove(child);
                }
            }
        }

        /// <summary>
        /// Coordinate converter
        /// </summary>
        /// <param name="canvas">Canvas Window</param>
        /// <param name="x">is Coordinate</param>
        /// <returns></returns>
        public static double OxyConverter(Canvas canvas, double x)
        {
            var ratio = canvas.Height * 80 / 160;
            return x + ratio;
        }

        public enum RectSize
        {
            Rectangular,
            Polar
        }

        /// <summary>
        /// Create preview item
        /// </summary>
        /// <param name="sourceItem">true if primary item</param>
        /// <param name="canvas">canvas window</param>
        /// <param name="itemVector"></param>
        /// <param name="degree">angle of items</param>
        /// <param name="rectSize">size of rectangle</param>
        /// <returns></returns>
        public static Rectangle CreateRect(Canvas canvas, bool sourceItem, Vector2d itemVector, double degree, RectSize rectSize)
        {
            double size = 4;
            switch (rectSize)
            {
                case RectSize.Rectangular:
                    size *= 1;
                    break;
                case RectSize.Polar:
                    size *= 1.8;
                    break;
                default:
                    break;
            }
            var transform = new RotateTransform(degree, size, size / 2);
            var rectangle = new Rectangle
            {
                Stroke = sourceItem ? Brushes.Black : Brushes.DarkGray,
                StrokeThickness = 0.8,
                Width = size * 2,
                Height = size,
                Fill = Brushes.Transparent,
                RenderTransform = transform
            };
            var coordinateX = size;
            var coordinateY = size / 2;
            Canvas.SetLeft(rectangle, OxyConverter(canvas, itemVector.X - coordinateX));
            Canvas.SetTop(rectangle, OxyConverter(canvas, -itemVector.Y - coordinateY));
            return rectangle;
        }

        /// <summary>
        /// draw rect' preview
        /// </summary>
        /// <param name="canvas">Canvas window</param>
        /// <param name="rectArray"></param>
        /// <param name="addDownward">is TRUE if the row offset is negative</param>
        /// <param name="addLeft">is TRUE if the column offset is negative</param>
        /// <param name="clockwise">true if angle dir is clockwise</param>
        public static void CalcRectArray(Canvas canvas, RectangularArray rectArray, int addDownward, int addLeft, bool clockwise)
        {
            ClearRectPreview(canvas);
            int rows = 15, cols = 15;
            const int colOffset = 12;
            const int rowOffset = 10;
            if (rectArray.Row < 15)
            {
                rows = rectArray.Row;
            }

            if (rectArray.Column < 15)
            {
                cols = rectArray.Column;
            }

            var primaryAngle = (clockwise ? -1 : 1) * rectArray.Angle;
            for (var i = 0; i < rows; i++)
            {
                for (var j = 0; j < cols; j++)
                {
                    var baseVector = new Vector2d(j * addLeft * colOffset, i * addDownward * rowOffset);
                    var itemVector = baseVector.RotateBy(primaryAngle);
                    _ = canvas.Children.Add(CreateRect(canvas, i == 0 && j == 0, itemVector, 0, RectSize.Rectangular));
                }
            }
        }

        /// <summary>
        /// draw polar' preview
        /// </summary>
        /// <param name="canvas">Canvas Window</param>
        /// <param name="polarArray"></param>
        /// <param name="startAngle">is angle, which create by vector and Ox (counterclockwise mode)</param>
        /// <param name="clockwise">true if angle dir is clockwise</param>
        public static void CalcPolarArray(
            Canvas canvas,
            PolarArray polarArray,
            double startAngle,
            bool clockwise
        )
        {
            ClearRectPreview(canvas);
            var vectorOx = new Vector2d(50, 0);
            var primaryBaseVector = vectorOx.RotateBy((Math.PI * 2) - startAngle);
            var numberOfItemsAfter = polarArray.Count;
            double angleItem;
            //avoid creating too many unnecessary rectangles
            if (polarArray.Count > 400)
            {
                numberOfItemsAfter = 400;
                angleItem = Math.Abs(polarArray.FillAngle) / 400;
            }
            else
            {
                angleItem = polarArray.ItemAngle;
            }

            //specified the direction of the polar array (ccw, cw?)
            angleItem *= polarArray.FillAngle < 0 ? -1 : 1;
            angleItem *= clockwise ? -1 : 1;
            //specified the direction of the polar array (ccw, cw?)

            for (var index = 0; index < numberOfItemsAfter; index++)
            {
                var itemVector = primaryBaseVector.RotateBy(angleItem * index);
                var rotAngle = polarArray.IsRotate ? -angleItem * index * 180 / Math.PI : 0;
                _ = canvas.Children.Add(CreateRect(canvas, index == 0, itemVector, rotAngle, RectSize.Polar));
            }
        }
    }
}
