﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
#endif
using JrxCad.Model;

namespace JrxCad.Helpers
{
    public static class EllipseHelper
    {
        /// <summary>
        /// Generates a polyline to approximate an ellipse.
        /// </summary>
        /// <param name="ellipse">The ellipse to be approximated</param>
        /// <returns>A new Polyline instance</returns>
        public static Polyline ToPolyline(this Ellipse ellipse, Point3d? firstPoint)
        {
            var polyline = new PolylineSegmentCollection(ellipse, firstPoint).ToPolyline();
            polyline.Closed = ellipse.Closed;
            polyline.Normal = ellipse.Normal;
            polyline.Elevation = ellipse.Center.TransformBy(Matrix3d.WorldToPlane(new Plane(Point3d.Origin, ellipse.Normal))).Z;
            return polyline;
        }
    }
}
