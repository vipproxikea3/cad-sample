﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

#endif

namespace JrxCad.Helpers
{
    public static class RegionHelper
    {
        /// <summary>
        /// 高度を取得
        /// </summary>
        public static double GetElevation(this Region region, Matrix3d ucsToWcs)
        {
            using (var idc = new DBObjectCollection())
            {
                // Explodeで分割した要素から取得
                region.Explode(idc);
                if (idc.Count == 0) return 0.0;

                if (idc[0] is Curve cur)
                {
                    var cs = cur.GetPlane().GetCoordinateSystem();
                    return cs.Origin.GetElevation(ucsToWcs);
                }

                return 0.0;
            }
        }
    }
}
