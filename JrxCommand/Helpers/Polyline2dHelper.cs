﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using AcRx = GrxCAD.Runtime;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using AcRx = Autodesk.AutoCAD.Runtime;
#endif
using System.Collections.Generic;

namespace JrxCad.Helpers
{
    public static class Polyline2dHelper
    {
        /// <summary>
        /// Gets the vertices list of the polyline 2d.
        /// </summary>
        /// <param name="polyline2d">The instance to which the method applies.</param>
        /// <returns>The vertices list.</returns>
        public static List<Vertex2d> GetVertices(this Polyline2d polyline2d)
        {
            var tr = polyline2d.Database.TransactionManager.TopTransaction;
            if (tr == null)
            {
                throw new AcRx.Exception(AcRx.ErrorStatus.NoActiveTransactions);
            }

            var vertices = new List<Vertex2d>();
            foreach (ObjectId id in polyline2d)
            {
                var vx = (Vertex2d)tr.GetObject(id, OpenMode.ForRead);
                if (vx.VertexType != Vertex2dType.SplineControlVertex)
                {
                    vertices.Add(vx);
                }
            }
            return vertices;
        }
    }
}
