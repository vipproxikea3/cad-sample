using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;

[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]

// アセンブリに関する一般情報は以下の属性セットをとおして制御されます。
// アセンブリに関連付けられている情報を変更するには、
// これらの属性値を変更してください。
#if _IJCAD_
[assembly: AssemblyTitle("Command for IJCAD 2021")]
#elif _AutoCAD_
[assembly: AssemblyTitle("Command for AutoCAD 2022")]
#endif
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Command")]
[assembly: AssemblyCopyright("(C)2021 SystemMetrix Co.,Ltd")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// ComVisible を false に設定すると、このアセンブリ内の型は COM コンポーネントから
// 参照できなくなります。COM からこのアセンブリ内の型にアクセスする必要がある場合は、
// その型の ComVisible 属性を true に設定してください。
[assembly: ComVisible(false)]

// このプロジェクトが COM に公開される場合、次の GUID が typelib の ID になります
[assembly: Guid("75b7e1f1-edc3-488d-93da-1f50bb1e9aa1")]

// アセンブリのバージョン情報は次の 4 つの値で構成されています:
//
//      メジャー バージョン
//      マイナー バージョン
//      ビルド番号
//      Revision
//
// すべての値を指定するか、次を使用してビルド番号とリビジョン番号を既定に設定できます
// 以下のように '*' を使用します:
// [assembly: AssemblyVersion("1.0.*")]
#if _IJCAD_
[assembly: AssemblyFileVersion("2021.6.10.ij2021")]
#elif _AutoCAD_
[assembly: AssemblyFileVersion("1.0.0")]
#endif
