﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;

#endif
using JrxCad.Utility;

namespace JrxCad.Command.SampleLine
{
    public partial class SampleLineCmd
    {
        private readonly IUserInput _userInput;

        /// <summary>
        /// IUserInput Interface
        /// </summary>
        public interface IUserInput
        {
            PromptPointResult GetStartPoint(PromptPointOptions options);
            PromptResult GetEndPoint(PointDrawJig.PointOptions options);
            void AppendLineId(ObjectId id);
        }

        /// <summary>
        /// UserInputImpl
        /// </summary>
        private class UserInput : IUserInput
        {
            public PromptPointResult GetStartPoint(PromptPointOptions options)
            {
                return Util.Editor().GetPoint(options);
            }

            public PromptResult GetEndPoint(PointDrawJig.PointOptions options)
            {
                var jig = new PointDrawJig(options);
                return Util.Editor().Drag(jig);
            }

            public void AppendLineId(ObjectId id)
            {
            }
        }

        /// <summary>
        /// End point options for Jig
        /// </summary>
        public class EndPointOptions : PointDrawJig.PointOptions
        {
            private readonly SampleLineCmd _cmd;
            private readonly Line _line;

            public EndPointOptions(SampleLineCmd cmd, Line line)
            {
                _cmd = cmd;
                _line = line;
            }

            public override JigPromptPointOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                return geometry.Draw(_line);
            }

            public override void UpdatePoint(Point3d pt)
            {
                _line.EndPoint = pt;
            }
        }
    }
}
