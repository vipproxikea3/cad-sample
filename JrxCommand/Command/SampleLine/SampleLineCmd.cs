﻿#if _IJCAD_
using Exception = System.Exception;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using GrxCAD.EditorInput;

#elif _AutoCAD_
using Exception = System.Exception;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;

#endif
using Autodesk.AutoCAD.Geometry;
using JrxCad.Utility;
using JrxCad.Helpers;

namespace JrxCad.Command.SampleLine
{
    public partial class SampleLineCmd : BaseCommand
    {
        public SampleLineCmd() : this(new UserInput())
        {
        }

        public SampleLineCmd(IUserInput userInput)
        {
            _userInput = userInput;
        }

        [CommandMethod("JrxCommand", "SmxSampleLine", CommandFlags.Modal)]
        public override void OnCommand()
        {
            if (!Init())
            {
                return;
            }

            try
            {
                // ----------------------------------------
                // Get start point (UCS)
                // ----------------------------------------

                var optStartPoint = new PromptPointOptions($"\nSpecify start point of line");
                var resStartPoint = _userInput.GetStartPoint(optStartPoint);
                if (resStartPoint.Status != PromptStatus.OK) return;
                var startU = resStartPoint.Value;

                // ----------------------------------------
                // Create line (UCS)
                // ----------------------------------------

                using (var line = new Line(startU, startU))
                {
                    // ----------------------------------------
                    // Transform UCS to WCS
                    // ----------------------------------------

                    line.TransformBy(CoordConverter.UcsToWcs());

                    // ----------------------------------------
                    // Get end point (WCS)
                    // ----------------------------------------

                    var optEndPoint = new EndPointOptions(this, line)
                    {
                        Options = new JigPromptPointOptions($"\nSpecify end point of line or")
                        {
                            UseBasePoint = true,
                            BasePoint = line.StartPoint,
                            UserInputControls = UserInputControls.Accept3dCoordinates |
                                                UserInputControls.UseBasePointElevation,
                            Cursor = CursorType.Crosshair,
                            Keywords =
                            {
                                { "C", "C", "Cyan" },
                                { "M", "M", "Magenta" },
                                { "Y", "Y", "Yellow" },
                                { "L", "L", "byLayer" },
                            },
                        }
                    };
                    while (true)
                    {
                        var resEndPoint = _userInput.GetEndPoint(optEndPoint);
                        if (resEndPoint.Status == PromptStatus.OK)
                        {
                            break;
                        }
                        if (resEndPoint.Status == PromptStatus.Keyword)
                        {
                            switch (resEndPoint.StringResult)
                            {
                                case "C":
                                    line.ColorIndex = 4;
                                    break;
                                case "M":
                                    line.ColorIndex = 6;
                                    break;
                                case "Y":
                                    line.ColorIndex = 2;
                                    break;
                                default:
                                    line.ColorIndex = 256;
                                    break;
                            }
                        }
                        else
                        {
                            return;
                        }
                    }

                    using (var tr = Util.StartTransaction())
                    {
                        tr.AddNewlyCreatedDBObject(line, true, tr.CurrentSpace());
                        _userInput.AppendLineId(line.Id);
                        tr.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }
    }
}
