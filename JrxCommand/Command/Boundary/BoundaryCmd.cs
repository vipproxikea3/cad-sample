﻿#if _IJCAD_
using GrxCAD.Runtime;
using CADApp = GrxCAD.ApplicationServices.Application;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;

#endif
using Exception = System.Exception;
using JrxCad.Utility;


namespace JrxCad.Command.Boundary
{
    public class BoundaryCmd : BaseCommand
    {
#if DEBUG
        [CommandMethod("JrxCommand", "SmxBO1", CommandFlags.Modal | CommandFlags.NoNewStack)]
#else
        [CommandMethod("JrxCommand", "SmxBOUNDARY", CommandFlags.Modal | CommandFlags.NoNewStack)]
#endif
        public override void OnCommand()
        {
            if (!Init())
            {
                return;
            }

            try
            {
                var WpfBoundary = new WpfBoundary();
                WpfBoundary.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }
    }
}
