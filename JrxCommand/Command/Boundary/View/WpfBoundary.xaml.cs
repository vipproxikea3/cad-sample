﻿using JrxCad.View.CustomWpf;
using JrxCad.Command.Boundary.ViewModel;

namespace JrxCad.Command.Boundary
{
    /// <summary>
    /// Interaction logic for WpfBoundary.xaml
    /// </summary>
    public partial class WpfBoundary : CustomWindow
    {
        public WpfBoundary()
        {
            InitializeComponent();
            DataContext = new BoundaryViewModel();
        }
    }
}
