﻿#if _IJCAD_
using CADApp = GrxCAD.ApplicationServices.Application;
using CADRegion = GrxCAD.DatabaseServices.Region;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;

#elif _AutoCAD_
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using CADRegion = Autodesk.AutoCAD.DatabaseServices.Region;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.Windows;

#endif
using JrxCad.Utility;
using JrxCad.View.CustomWpf;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Geometry;
using JrxCad.Helpers;
using JrxCad.Enum;
using Exception = System.Exception;

namespace JrxCad.Command.Boundary.ViewModel
{
    public class BoundaryViewModel : BindableBase
    {
        #region Private properties
        protected override string HelpName => "BOUNDARY";

        /// <summary>
        /// Count boundary created
        /// </summary>
        private int _count;

        private List<ObjectId> _activeLayer;
        private List<ObjectId> _boundaryEntity;
        private List<DBObjectCollection> _lastCreatedObject;

        private ObjectId[] _selectedObjIds;
        private ObjectId _tempLayerId;
        private ObjectId _currentLayerId;

        private string _currentLayerName;
        private string _tempLayerName;

        /// <summary>
        /// Boundary set - true: Existing , false: Current
        /// </summary>
        private bool _isDetectBoundarySet;
        private bool _hasEllipse = false;
        #endregion

        #region Public properties
        private int _selectType;
        /// <summary>
        /// 0-Region; 1-Polyline
        /// </summary>
        public int SelectType
        {
            get => _selectType;
            set => SetProperty(ref _selectType, value);
        }

        private string _selectBoSet;
        /// <summary>
        /// "Current viewport" OR "Existing set"
        /// </summary>
        public string SelectBoSet
        {
            get => _selectBoSet;
            set => SetProperty(ref _selectBoSet, value);
        }

        private bool _isLandDetect;
        /// <summary>
        /// IslandDetect: false-Off, true-ON
        /// </summary>
        public bool IsLandDetect
        {
            get => _isLandDetect;
            set => SetProperty(ref _isLandDetect, value);
        }

        private List<string> _propertyType;
        public List<string> PropertyType
        {
            get => _propertyType;
            set => SetProperty(ref _propertyType, value);
        }

        private List<string> _propertyBoSet;
        public List<string> PropertyBoSet
        {
            get => _propertyBoSet;
            set => SetProperty(ref _propertyBoSet, value);
        }

        public enum HpBound
        {
            Region = 0,
            Polyline = 1
        }

        public ICommand NewBoundarySet { get; set; }
        public ICommand OkCommand { get; set; }
        public ICommand CloseWindowCommand { get; set; }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public BoundaryViewModel()
        {
            Initialize();

            NewBoundarySet = new RelayCommand<CustomWindow>(
                (window) =>
                {
                    return window != null;
                },
                (window) =>
                {
                    btnNew_Click(window);
                }
            );

            OkCommand = new RelayCommand<CustomWindow>(
                (window) =>
                {
                    return window != null;
                },
                (window) =>
                {
                    btnOk_Click(window);
                }
            );

            CloseWindowCommand = new RelayCommand<CustomWindow>(
                (window) =>
                {
                    return window != null;
                },
                (window) =>
                {
                    CloseWindow(window);
                }
            );
        }

        /// <summary>
        /// Initialize Variables, SelectOption
        /// </summary>
        private void Initialize()
        {
            _count = 0;
            _activeLayer = new List<ObjectId>();
            _boundaryEntity = new List<ObjectId>();

            //HpIslandDetectionMode: 0-Off, 1-ON
            IsLandDetect = SystemVariable.GetBool("HpIslandDetectionMode");

            //ObjectType Select
            PropertyType = new List<string> { "Region", "Polyline" };
            //HpBound: 0-Region; 1-Polyline
            SelectType = SystemVariable.GetInt("HpBound");

            //BoundarySet Select
            PropertyBoSet = new List<string> { "Current viewport" };
            SelectBoSet = PropertyBoSet[0];
        }

        #region Interact Form
        /// <summary>
        /// Handle button 'OK'
        /// </summary>
        /// <param name="currentWindow"></param>
        private void btnOk_Click(CustomWindow currentWindow)
        {
            try
            {
                HandleTraceBoundary(currentWindow);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                CloseWindow(currentWindow);
            }
        }

        /// <summary>
        /// Handle button 'New Boundary Set'
        /// </summary>
        /// <param name="currentWindow"></param>
        private void btnNew_Click(CustomWindow currentWindow)
        {
            try
            {
                currentWindow.Hide();
                // Set up selection
                var optSelect = new PromptSelectionOptions
                {
                    MessageForAdding = "\nSelect objects",
                };

                var resSelect = Util.Editor().GetSelection(optSelect);
                if (resSelect.Status != PromptStatus.OK)
                {
                    return;
                }

                if (resSelect.Value.Count > 0)
                {
                    _selectedObjIds = new ObjectId[resSelect.Value.Count];
                    var i = 0;
                    foreach (SelectedObject so in resSelect.Value)
                    {
                        var id = so.ObjectId;
                        _selectedObjIds[i] = id;
                        i++;
                    }

                    PropertyBoSet.Add("Existing set");
                    SelectBoSet = PropertyBoSet[1];
                    currentWindow.Show();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        /// <summary>
        /// Close current window
        /// </summary>
        /// <param name="currentWindow"></param>
        private void CloseWindow(CustomWindow currentWindow)
        {
            if (currentWindow != null)
            {
                SystemVariable.SetBool("HpIslandDetectionMode", IsLandDetect);
                SystemVariable.SetValue("HpBound", SelectType);
                currentWindow.Close();
            }
        }
        #endregion

        #region MainFunction
        /// <summary>
        /// Handle Pick Point & TraceBoundary
        /// </summary>
        /// <param name="currentWindow"></param>
        private void HandleTraceBoundary(CustomWindow currentWindow)
        {
            currentWindow.Hide();

            _isDetectBoundarySet = SelectBoSet == "Existing set" && _selectedObjIds != null;

            var osmode = SystemVariable.GetInt("osmode");
            using (new SystemVariable.Temp("OSMODE", osmode | (int)Osmode.AllOff))
            {
                while (true)
                {
                    var optSelectPoint = new PromptPointOptions("\nSelect internal point")
                    {
                        AllowNone = true
                    };
                    var resSelectPoint = Util.Editor().GetPoint(optSelectPoint);

                    if (resSelectPoint.Status == PromptStatus.None) //'Enter' key
                    {
                        if (_hasEllipse)
                        {
                            var isYes = MsgBox.Ask.Show("Polyline boundary could not be derived. Create Region?");
                            if (!isYes)
                            {
                                EraseBoundaryEntity();
                                return;
                            }
                        }

                        if (_count > 0)
                        {
                            var txtMessage = (_hasEllipse || SelectType == (int)HpBound.Region) ? " region" : " polyline";
                            Util.Editor().WriteMessage($"BOUNDARY created {_count} {txtMessage}");
                            HighlightListEntity(_boundaryEntity, false); // Unhighlight
                        }
                        _lastCreatedObject = null;
                        _hasEllipse = false;
                        return;
                    }
                    else if (resSelectPoint.Status == PromptStatus.Cancel) //'Esc' key
                    {
                        EraseBoundaryEntity();
                        return;
                    }
                    else if (resSelectPoint.Status == PromptStatus.OK)
                    {
                        TracingBoundary(resSelectPoint);
                    }
                }
            }
        }

        /// <summary>
        /// TracingBoundary with Selected point
        /// </summary>
        /// <param name="resSelectPoint"></param>
        private void TracingBoundary(PromptPointResult resSelectPoint)
        {
            if (_isDetectBoundarySet)
            {
                TurnOffLayersAndCreateTemp();
            }

            //Trace boudary
            var wcs2Ucs = Util.Editor().CurrentUserCoordinateSystem.Inverse();
            var objs = Util.Editor().TraceBoundary(resSelectPoint.Value.TransformBy(wcs2Ucs), IsLandDetect);

            //TurnOn layer
            if (_isDetectBoundarySet)
            {
                TurnOnLayers();
            }

            if (objs.Count <= 0)
            {
#if _IJCAD_
                MsgBox.Error.Show("Valid hatch boundary not found");
                if (_isDetectBoundarySet)
                {
                    DeleteTempLayer();
                }
#elif _AutoCAD_
                var showDialog = TaskDialog.HideableDialogSettingsDictionary.GetResult("Boundary.BoundaryDefinitionError2");
                if (showDialog == 0)
                {
                    ShowDefinitionErrorDialog();
                }
                Util.Editor().WriteMessage("\nValid hatch boundary not found.");

                if (_isDetectBoundarySet)
                {
                    DeleteTempLayer();
                }
#endif
                objs.Dispose();
                objs = null;
                return;
            }

            if (IsDuplicate(objs))
            {
                Util.Editor().WriteMessage("\nThis area has already been specified.");
                if (_isDetectBoundarySet)
                {
                    DeleteTempLayer();
                }
                objs.Dispose();
                objs = null;
                return;
            }

            DrawBoundary(objs);
            if (_isDetectBoundarySet)
            {
                DeleteTempLayer();
            }

            HighlightListEntity(_boundaryEntity, true);
        }

        /// <summary>
        /// Draw Boundary
        /// </summary>
        /// <param name="objs"></param>
        private void DrawBoundary(DBObjectCollection objs)
        {
            using (var docloc = Util.CurDoc().LockDocument())
            using (var tr = Util.StartTransaction())
            using (var blockTable = (BlockTable)tr.GetObject(Util.CurDoc().Database.BlockTableId, OpenMode.ForWrite))
            using (var blockTableRecord = (BlockTableRecord)tr.GetObject(blockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite))
            {
                foreach (DBObject obj in objs)
                {
                    var ent = (Entity)obj;
                    if (ent != null)
                    {
                        _count++;
                        if (ent.GetType().Name == "Region" && SelectType == (int)HpBound.Polyline)
                        {
                            _hasEllipse = true;
                        }

                        if (SelectType == (int)HpBound.Polyline && !_hasEllipse)
                        {
                            var polyline = ent as Polyline;
                            if (_isDetectBoundarySet)
                            {
                                polyline.Layer = _currentLayerName;
                            }
                            _boundaryEntity.Add(blockTableRecord.AppendEntity(polyline));
                            tr.AddNewlyCreatedDBObject(polyline, true);
                        }
                        else
                        {
                            var myRegionColl = CADRegion.CreateFromCurves(objs);
                            var region = (CADRegion)myRegionColl[0];
                            if (region != null)
                            {
                                if (_isDetectBoundarySet)
                                {
                                    region.Layer = _currentLayerName;
                                }
                                _boundaryEntity.Add(blockTableRecord.AppendEntity(region));
                                tr.AddNewlyCreatedDBObject(region, true);
                            }
                        }
                    }
                }
                tr.Commit();
            }
        }

        /// <summary>
        /// Highlight Entities
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="highlight"></param>
        private void HighlightListEntity(List<ObjectId> listId, bool highlight)
        {
            if (listId.Count > 0)
            {
                using (var docloc = Util.CurDoc().LockDocument())
                using (var tr = Util.StartTransaction())
                {
                    foreach (var id in listId)
                    {
                        if (id == ObjectId.Null)
                        {
                            continue;
                        }
                        var ent = (Entity)tr.GetObject(id, OpenMode.ForWrite);
                        if (ent == null)
                        {
                            continue;
                        }

                        if (highlight)
                        {
                            ent.Highlight();
                        }
                        else
                        {
                            ent.Unhighlight();
                        }
                    }
                }
            }
        }
        #endregion

        #region Handle New Boundary Set
        /// <summary>
        /// Turn off layer, create temp layer
        /// </summary>
        private void TurnOffLayersAndCreateTemp()
        {
            using (var docloc = Util.CurDoc().LockDocument())
            using (var tr = Util.StartTransaction())
            using (var layerTable = (LayerTable)tr.GetObject(Util.CurDoc().Database.LayerTableId, OpenMode.ForRead))
            using (var blockTable = (BlockTable)tr.GetObject(Util.CurDoc().Database.BlockTableId, OpenMode.ForRead))
            using (var blockTableRecord = (BlockTableRecord)tr.GetObject(blockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite))
            {
                //Check validate Layer name
                var index = 1;
                while (true)
                {
                    _tempLayerName = Const.TempLayer + index;
                    if (layerTable.Has(_tempLayerName))
                    {
                        index++;
                    }
                    else
                    {
                        break;
                    }
                }

                //Create Temp layer
                layerTable.UpgradeOpen();
                var tempLayer = new LayerTableRecord
                {
                    Name = _tempLayerName
                };
                _tempLayerId = layerTable.Add(tempLayer);
                tr.AddNewlyCreatedDBObject(tempLayer, true);

                //Save variable of current layer
                _currentLayerName = SystemVariable.GetString("CLAYER");
                _currentLayerId = layerTable[_currentLayerName];

                //Set New layer to current
                SystemVariable.SetString("CLAYER", _tempLayerName);

                //Copy selected object to new layer
                foreach (var objectId in _selectedObjIds)
                {
                    var ent = (Entity)tr.GetObject(objectId, OpenMode.ForWrite);
                    var clone = (Entity)ent.Clone();
                    clone.Layer = _tempLayerName;
                    blockTableRecord.AppendEntity(clone);
                    tr.AddNewlyCreatedDBObject(clone, true);
                }

                //Hide all layer except TempLayer
                _activeLayer.Clear();
                foreach (var objectId in layerTable)
                {
                    if (objectId != _tempLayerId)
                    {
                        var ltr = (LayerTableRecord)tr.GetObject(objectId, OpenMode.ForWrite);
                        if (ltr != null)
                        {
                            //Check layer active
                            if (!ltr.IsOff)
                            {
                                _activeLayer.Add(objectId);
                            }
                            ltr.IsOff = true;
                        }
                    }
                }
                tr.Commit();
            }
        }

        /// <summary>
        /// Turn on Current layer
        /// </summary>
        private void TurnOnLayers()
        {
            using (var docloc = Util.CurDoc().LockDocument())
            using (var tr = Util.StartTransaction())
            {
                foreach (var obj in _activeLayer)
                {
                    var ltr = (LayerTableRecord)tr.GetObject(obj, OpenMode.ForWrite);
                    if (ltr != null)
                    {
                        ltr.IsOff = false;
                        if (obj == _currentLayerId)
                        {
                            SystemVariable.SetString("CLAYER", _currentLayerName);
                        }
                    }
                }
                tr.Commit();
            }
        }

        /// <summary>
        /// Erase Boundary
        /// </summary>
        private void EraseBoundaryEntity()
        {
            using (var tr = Util.StartTransaction())
            {
                foreach (var id in _boundaryEntity)
                {
                    var ent = (Entity)tr.GetObject(id, OpenMode.ForWrite);
                    ent.Erase();
                }
                tr.Commit();
            }
            _lastCreatedObject = null;
        }

        /// <summary>
        /// Delete Temp layer
        /// </summary>
        private void DeleteTempLayer()
        {
            using (var docloc = Util.CurDoc().LockDocument())
            using (var tr = Util.StartTransaction())
            {
                var tvs = new TypedValue[] { new TypedValue((int)DxfCode.LayerName, _tempLayerName) };
                var sf = new SelectionFilter(tvs);
                var resSelect = Util.Editor().SelectAll(sf);
                foreach (SelectedObject so in resSelect.Value)
                {
                    var ob = tr.GetObject(so.ObjectId, OpenMode.ForWrite);
                    ob.Erase();
                }

                var tempLayer = (LayerTableRecord)tr.GetObject(_tempLayerId, OpenMode.ForWrite);
                tempLayer.IsLocked = false;
                tempLayer.Erase();
                tr.Commit();
            }
        }
        #endregion

        #region Check Duplicate Boundary
        /// <summary>
        /// Check Duplicate DBCollection 
        /// </summary>
        /// <param name="objs"></param>
        /// <returns></returns>
        private bool IsDuplicate(DBObjectCollection objs)
        {
            if (_lastCreatedObject == null)
            {
                SaveCollectionForDuplicateCheck(objs);
                return false;
            }
            foreach (var item in _lastCreatedObject)
            {
                var isSame = IsSameCollection(objs, item);
                if (isSame)
                {
                    return true;
                }
            }
            SaveCollectionForDuplicateCheck(objs);
            return false;
        }

        /// <summary>
        /// Prepare DBCollection to check Duplicate
        /// </summary>
        /// <param name="objs"></param>
        private void SaveCollectionForDuplicateCheck(DBObjectCollection objs)
        {
            if (_lastCreatedObject == null)
            {
                _lastCreatedObject = new List<DBObjectCollection>();
            }
            _lastCreatedObject.Add(objs);
        }

        /// <summary>
        /// Compare 2Collection
        /// </summary>
        /// <param name="objs"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        private bool IsSameCollection(DBObjectCollection objs, DBObjectCollection target)
        {
            if (objs.Count != target.Count)
            {
                return false;
            }

            //objs.Count == target.Count
            var countTheSame = 0;
            var newObjectColl = CADRegion.CreateFromCurves(objs);
            var oldObjectColl = CADRegion.CreateFromCurves(target);
            for (var i = 0; i < objs.Count; i++)
            {
                var newObj = (Region)newObjectColl[i];
                for (var j = 0; j < objs.Count; j++)
                {
                    var oldObj = (Region)oldObjectColl[j];
                    if (IsSameRegion(newObj, oldObj))
                    {
                        countTheSame++;
                        break;
                    }
                }
            }
            return countTheSame == objs.Count;
        }

        /// <summary>
        /// Compare 2Region
        /// </summary>
        /// <param name="newObj"></param>
        /// <param name="oldObj"></param>
        /// <returns></returns>
        private bool IsSameRegion(Region newObj, Region oldObj)
        {
            if (newObj != null && oldObj != null)
            {
                var oldSegment = new DBObjectCollection();
                oldObj.Explode(oldSegment);

                var newSegment = new DBObjectCollection();
                newObj.Explode(newSegment);

                var countSegment = oldSegment.Count;
                if (countSegment != newSegment.Count)
                {
                    return false;
                }

                //oldObj.countSegment == newObj.countSegment
                var startCompareIndexOfOldObject = -1;
                for (var k = 0; k < countSegment; k++)
                {
                    if (CompareTwoSegmentsRegion(oldSegment, 0, newSegment, k))
                    {
                        startCompareIndexOfOldObject = k;
                        break;
                    }
                }

                if (startCompareIndexOfOldObject < 0)
                {
                    return false;
                }

                //Compare
                int index;
                for (index = 0; index < countSegment; index++)
                {
                    //Same direction
                    var newIndexToCompare = (startCompareIndexOfOldObject + index) % countSegment;
                    if (!CompareTwoSegmentsRegion(oldSegment, index, newSegment, newIndexToCompare))
                    {
                        //Detect different.
                        break;
                    }
                }
                if (index == countSegment)
                {
                    return true;//same
                }
                if (index < countSegment)
                {
                    for (index = 0; index < countSegment; index++)
                    {
                        //Different direction
                        var newIndexToCompare = (startCompareIndexOfOldObject - index + countSegment) % countSegment;
                        if (!CompareTwoSegmentsRegion(oldSegment, index, newSegment, newIndexToCompare))
                        {
                            //Detect different.
                            return false;//different
                        }
                    }
                }
                //Nothing difference
                return true;
            }
            else
            {
                throw new Exception("Something go wrong");
            }
        }

        /// <summary>
        /// Compare 2Segments
        /// </summary>
        /// <param name="oldObj"></param>
        /// <param name="indexOld"></param>
        /// <param name="newObj"></param>
        /// <param name="indexNew"></param>
        /// <returns></returns>
        private bool CompareTwoSegmentsRegion(DBObjectCollection oldObj, int indexOld, DBObjectCollection newObj, int indexNew)
        {
            if (oldObj[indexOld].GetType().Name != newObj[indexNew].GetType().Name)
            {
                return false;
            }

            if (oldObj[indexOld].GetType().Name == "Line")
            {
                return IsSameLine(oldObj[indexOld] as Line, newObj[indexNew] as Line);
            }
            else if (oldObj[indexOld].GetType().Name == "Arc")
            {
                return IsSameCircularArc(oldObj[indexOld] as Arc, newObj[indexNew] as Arc);
            }
            else if (oldObj[indexOld].GetType().Name == "Ellipse")
            {
                return IsSameEllipse(oldObj[indexOld] as Ellipse, newObj[indexNew] as Ellipse);
            }
            else
            {
                throw new Exception("Something go wrong");
            }
        }

        /// <summary>
        /// Compare 2CircularArc
        /// </summary>
        /// <param name="seg1"></param>
        /// <param name="seg2"></param>
        /// <returns></returns>
        private bool IsSameCircularArc(Arc seg1, Arc seg2)
        {
            if (!Point3dHelper.IsSamePoint(seg1.Center, seg2.Center))
            {
                return false;
            }
            if (!DoubleHelper.IsEqual(seg1.Radius, seg2.Radius))
            {
                return false;
            }
            using (Line line1 = new Line(seg1.StartPoint, seg1.EndPoint),
                line2 = new Line(seg2.StartPoint, seg2.EndPoint))
            {
                if (!IsSameLine(line1, line2)) return false;
            }
            var includedAngle1 = Math.Abs(seg1.EndAngle - seg1.StartAngle);
            var includedAngle2 = Math.Abs(seg2.EndAngle - seg2.StartAngle);
            return DoubleHelper.IsEqual(includedAngle1, includedAngle2);
        }

        /// <summary>
        /// Compare 2Ellipse
        /// </summary>
        /// <param name="seg1"></param>
        /// <param name="seg2"></param>
        /// <returns></returns>
        private bool IsSameEllipse(Ellipse seg1, Ellipse seg2)
        {
            if (!Point3dHelper.IsSamePoint(seg1.Center, seg2.Center))
            {
                return false;
            }
            if (!DoubleHelper.IsEqual(seg1.MajorRadius, seg2.MajorRadius))
            {
                return false;
            }
            if (!DoubleHelper.IsEqual(seg1.MinorRadius, seg2.MinorRadius))
            {
                return false;
            }
            if (!DoubleHelper.IsEqual(seg1.RadiusRatio, seg2.RadiusRatio))
            {
                return false;
            }
            using (Line line1 = new Line(seg1.StartPoint, seg1.EndPoint),
                line2 = new Line(seg2.StartPoint, seg2.EndPoint))
            {
                if (!IsSameLine(line1, line2)) return false;
            }
            var includedAngle1 = Math.Abs(seg1.EndAngle - seg1.StartAngle);
            var includedAngle2 = Math.Abs(seg2.EndAngle - seg2.StartAngle);
            return DoubleHelper.IsEqual(includedAngle1, includedAngle2);
        }

        /// <summary>
        /// Compare 2Line
        /// </summary>
        /// <param name="seg1"></param>
        /// <param name="seg2"></param>
        /// <returns></returns>
        private bool IsSameLine(Line seg1, Line seg2)
        {
            if (!((Point3dHelper.IsSamePoint(seg1.StartPoint, seg2.StartPoint) &&
                   Point3dHelper.IsSamePoint(seg1.EndPoint, seg2.EndPoint)) ||
                  (Point3dHelper.IsSamePoint(seg1.EndPoint, seg2.StartPoint) &&
                   Point3dHelper.IsSamePoint(seg1.StartPoint, seg2.EndPoint))))
            {
                return false;
            }
            return true;
        }
        #endregion

        /// <summary>
        /// Show ErrorDialog 
        /// </summary>
        private void ShowDefinitionErrorDialog()
        {
#if _IJCAD_
#elif _AutoCAD_
            var td = new TaskDialog();
            td.Width = 220;
            td.Id = "Boundary.BoundaryDefinitionError2";
            td.WindowTitle = "Boundary - Boundary Definifion Error";
            td.MainInstruction = "A closed boundary cannot be determined.";
            td.ContentText = "There might be gaps between the boundary objects, or the boundary objects might be outside of the display area. \n";
            td.VerificationText = "Do not show this message again";
            td.EnableVerificationHandler = true;

            td.CollapsedControlText = "Show details";
            td.ExpandedControlText = "Hide details";
            td.ExpandedText = "Try one or more of the following: \n\n" +
                              "\t\u2022 Zoom out until all boundaries are visible. Then specify a new pick point. \n\n" +
                              "\t\u2022 Cancel the command and modify the objects in the boundary to close the gaps. \n\n" +
                              "\t\u2022 Confirm that the XY plane of the UCS is parallel to the plane of the boundary objects. \n";
            td.ExpandedByDefault = false;
            td.AllowDialogCancellation = true;

            td.Callback = delegate (ActiveTaskDialog atd, TaskDialogCallbackArgs e, object sender)
            {
                if (e.Notification == TaskDialogNotification.VerificationClicked)
                    TaskDialog.HideableDialogSettingsDictionary.Add(td);
                return false;
            };
            td.Show(CADApp.MainWindow.Handle);
#endif
        }

    }
}
