﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
#endif
using System.Collections.Generic;
using JrxCad.Helpers;
using JrxCad.Utility;
using Exception = System.Exception;
using System;

namespace JrxCad.Command.LineCmd
{
    public enum CustomOSnapMode
    {
        None = 0,
        DeferredTangent = 1,
        DeferredPerpendicular = 2,
    }

    public partial class LineCmd : BaseCommand
    {
        private Entity3d _entity3dU1;
        private Entity3d _entity3dU2;
        private static CustomOSnapMode _customOSnapMod1StPt;
        private static CustomOSnapMode _customOSnapMod2NdPt;
        private CustomOSnapApp _customOSnap;

        public LineCmd() : this(new UserInput(), null)
        {
        }

        public LineCmd(IUserInput userInput, INotifyId notify)
        {
            _userInput = userInput;
            _notify = notify;
        }

        [CommandMethod("JrxCommand", "SmxLine", CommandFlags.Modal | CommandFlags.NoNewStack)]
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }

                // Delete ObjectIdCollection of non-exist drawing
                Util.CheckDrawingExits();

                DrawLine();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                _entity3dU1?.Dispose();
                _entity3dU2?.Dispose();
                _customOSnap?.Terminate();
                _entity3dU1 = null;
                _entity3dU2 = null;
                _customOSnap = null;
                Exit();
            }
        }

        private void DrawLine()
        {
            var countOfPoint = 0;
            var init = Point3d.Origin;
            var start = Point3d.Origin;
            Vector3d? direction = null;
            var idStack = new Stack<ObjectId>();
            while (true)
            {
                if (countOfPoint == 0)
                {
                    var firstPointAndStatus = GetFirstPoint();
                    if (!firstPointAndStatus.Item2)
                    {
                        return;
                    }

                    if (firstPointAndStatus.Item1 == null)
                    {
                        var lastPtAndDirection = GetLastPointAndDirection();
                        if (lastPtAndDirection.Item1 == null)
                        {
                            WriteMessage($"{LineRes.CM_ContER1}");
                            continue;
                        }

                        init = (Point3d)lastPtAndDirection.Item1;
                        start = init;
                        direction = lastPtAndDirection.Item2;
                    }
                    else
                    {
                        init = firstPointAndStatus.Item1.Value;
                        start = init;
                    }
                }
                else
                {
                    var end = Point3d.Origin;
                    switch (countOfPoint)
                    {
                        case 1 when _entity3dU1 != null:
                            {
                                var secondPoint = GetSecondPointTOrP(ref start, out var undo);
                                if (undo)
                                {
                                    start = init;
                                    countOfPoint--;
                                    _customOSnapMod1StPt = CustomOSnapMode.None;
                                    WriteMessage($"{LineRes.CM_ER1}");
                                }
                                else
                                {
                                    if (secondPoint == null) return;

                                    end = secondPoint.Value;
                                    init = start;

                                    idStack.Push(AppendLine(start, end));
                                    start = end;
                                    countOfPoint++;
                                }
                                _customOSnap?.Terminate();
                                _customOSnap = null;
                                _customOSnapMod2NdPt = CustomOSnapMode.None;
                                continue;
                            }
                        case 1 when direction != null:
                            {
                                var endPtOfLineDrawByLen = DrawByLength((Vector3d)direction, start, idStack);
                                if (endPtOfLineDrawByLen == null)
                                {
                                    return;
                                }
                                start = (Point3d)endPtOfLineDrawByLen;
                                countOfPoint++;
                                continue;
                            }
                    }

                    using (var line = new Line(start, start))
                    {
                        var jig = new LineJigActions($"{LineRes.UI_NextP123}", countOfPoint > 2, line);
                        var drag = _userInput.AcquirePoint(jig);
                        switch (drag.Status)
                        {
                            case PromptStatus.OK:
                                end = line.EndPoint;
                                idStack.Push(AppendLine(start, end));
                                break;
                            case PromptStatus.Keyword:
                                switch (drag.StringResult.ToUpper())
                                {
                                    case "C":
                                        // 閉じる
                                        end = init;
                                        idStack.Push(AppendLine(start, end));
                                        return;
                                    case "U":
                                        // 取り消し
                                        if (idStack.Count == 0)
                                        {
                                            start = init;
                                            WriteMessage($"\n{LineRes.CM_ER1}");
                                        }
                                        else
                                        {
                                            start = EraseLine(idStack.Pop());
                                        }
                                        countOfPoint--;
                                        continue;
                                }
                                break;
                            default:
                                return;
                        }
                        start = end;
                    }
                }

                countOfPoint++;
            }
        }

        private (Point3d?, Vector3d?) GetLastPointAndDirection()
        {
            Point3d? lastPointToContinue = null;
            Vector3d? tangentDirection = null;

            using (var tr = Util.StartTransaction())
            {
                var curObjectIds = Util.GetCurrSpaceObjectIds();
                if (curObjectIds.Count < 1)
                {
                    return (null, null);
                }

                using (var lastEntity = tr.GetObject<Entity>(curObjectIds[curObjectIds.Count - 1], OpenMode.ForRead, true, true))
                {
                    switch (lastEntity)
                    {
                        case Line line:
                            lastPointToContinue = line.EndPoint;
                            break;
                        case Polyline pLine when pLine.Closed:
                            {
                                lastPointToContinue = pLine.StartPoint;

                                var bulge = pLine.GetBulgeAt(pLine.NumberOfVertices - 1);
                                if (bulge != 0)
                                {
                                    var lastArc = pLine.GetArcSegmentAt(pLine.NumberOfVertices - 1);
                                    var angleToRote = MathEx.IsEqual((Point3d)lastPointToContinue, lastArc.StartPoint) ? Math.PI / 2 : -Math.PI / 2;
                                    tangentDirection = ((Point3d)lastPointToContinue).GetVectorTo(lastArc.Center).RotateBy(angleToRote, lastArc.Normal);
                                }

                                break;
                            }
                        case Polyline pLine:
                            {
                                lastPointToContinue = pLine.EndPoint;

                                var bulge = pLine.GetBulgeAt(pLine.NumberOfVertices - 2);
                                if (bulge != 0)
                                {
                                    var lastArc = pLine.GetArcSegmentAt(pLine.NumberOfVertices - 2);
                                    var angleToRote = MathEx.IsEqual((Point3d)lastPointToContinue, lastArc.StartPoint) ? Math.PI / 2 : -Math.PI / 2;
                                    tangentDirection = ((Point3d)lastPointToContinue).GetVectorTo(lastArc.Center).RotateBy(angleToRote, lastArc.Normal);
                                }

                                break;
                            }
                        case Arc arc:
                            {
                                var lastPoint = (Point3d)SystemVariable.GetValue("LASTPOINT");
                                #region StartPoint or EndPoint that matches lastpoint coordinates. 
                                if (arc.StartPoint.IsEqualTo(lastPoint) || arc.EndPoint.IsEqualTo(lastPoint))
                                {
                                    lastPointToContinue = MathEx.IsEqual(arc.StartPoint, lastPoint) ? arc.StartPoint : arc.EndPoint;
                                    var angleToRote = MathEx.IsEqual((Point3d)lastPointToContinue, arc.StartPoint) ? Math.PI / 2 : -Math.PI / 2;
                                    tangentDirection = ((Point3d)lastPointToContinue).GetVectorTo(arc.Center).RotateBy(angleToRote, arc.Normal);
                                    return (lastPointToContinue, tangentDirection);
                                }
                                #endregion
                                #region A vertex opened in StartPoint or EndPoint compared to the vertex of the previous shape.
                                if (curObjectIds.Count >= 2)
                                {
                                    using (var entPre = tr.GetObject<Entity>(curObjectIds[curObjectIds.Count - 2], OpenMode.ForRead, true, true))
                                    {
                                        switch (entPre)
                                        {
                                            case Line linePre:
                                                {
                                                    if (linePre.EndPoint.IsEqualTo(arc.EndPoint) || linePre.EndPoint.IsEqualTo(arc.StartPoint))
                                                    {
                                                        lastPointToContinue = linePre.EndPoint.IsEqualTo(arc.EndPoint) ? arc.StartPoint : arc.EndPoint;
                                                    }
                                                    break;
                                                }
                                            case Polyline pLinePre:
                                                {
                                                    if (pLinePre.EndPoint.IsEqualTo(arc.EndPoint) || pLinePre.EndPoint.IsEqualTo(arc.StartPoint))
                                                    {
                                                        lastPointToContinue = pLinePre.EndPoint.IsEqualTo(arc.EndPoint) ? arc.StartPoint : arc.EndPoint;
                                                    }
                                                    break;
                                                }
                                            case Arc arcPre:
                                                {
                                                    if (arcPre.StartPoint.IsEqualTo(arc.EndPoint) || arcPre.StartPoint.IsEqualTo(arc.StartPoint))
                                                    {
                                                        lastPointToContinue = arcPre.StartPoint.IsEqualTo(arc.EndPoint)
                                                            ? arc.StartPoint
                                                            : arc.EndPoint;
                                                    }
                                                    if (arcPre.EndPoint.IsEqualTo(arc.EndPoint) || arcPre.EndPoint.IsEqualTo(arc.StartPoint))
                                                    {
                                                        lastPointToContinue = arcPre.EndPoint.IsEqualTo(arc.EndPoint) ? arc.StartPoint : arc.EndPoint;
                                                    }
                                                    if (lastPointToContinue != null)
                                                    {
                                                        var angleToRote = MathEx.IsEqual((Point3d)lastPointToContinue, arcPre.StartPoint) ? Math.PI / 2 : -Math.PI / 2;
                                                        tangentDirection = ((Point3d)lastPointToContinue).GetVectorTo(arcPre.Center).RotateBy(angleToRote, arcPre.Normal);
                                                    }
                                                    break;
                                                }
                                        }
                                    }
                                    if (lastPointToContinue != null)
                                    {
                                        return (lastPointToContinue, tangentDirection);
                                    }
                                }
                                #endregion
                                #region If neither StartPoint nor EndPoint were open, select the side close to the lastpoint coordinates.
                                //lastPointToContinue = lastPoint.DistanceTo(arc.EndPoint) > lastPoint.DistanceTo(arc.StartPoint) ? arc.StartPoint : arc.EndPoint;
                                double angleToRote1;
                                if (lastPoint.DistanceTo(arc.EndPoint) > lastPoint.DistanceTo(arc.StartPoint))
                                {
                                    lastPointToContinue = arc.StartPoint;
                                    angleToRote1 = Math.PI / 2;
                                }
                                else
                                {
                                    lastPointToContinue = arc.EndPoint;
                                    angleToRote1 = -Math.PI / 2;
                                }
                                tangentDirection = ((Point3d)lastPointToContinue).GetVectorTo(arc.Center).RotateBy(angleToRote1, arc.Normal);
                                return (lastPointToContinue, tangentDirection);
                                #endregion
                            }
                    }
                }
            }

            return (lastPointToContinue, tangentDirection);
        }

        private ObjectId AppendLine(Point3d start, Point3d end)
        {
            using (var tr = Util.StartTransaction())
            using (var blkTblRec = tr.GetObject<BlockTableRecord>(Util.Database().CurrentSpaceId, OpenMode.ForWrite))
            using (var line = new Line(start, end))
            {
                line.SetDatabaseDefaults();
                line.Thickness = Util.Database().Thickness;
                blkTblRec.AppendEntity(line);
                tr.AddNewlyCreatedDBObject(line, true);
                tr.Commit();
                Util.AddObjectIdToCollection(line.ObjectId);
                if (line.Length.IsZero())
                {
                    WriteMessage($"{LineRes.CM_ER2}", string.Format($"{start.X:0.####}, {start.Y:0.####}, {start.Z:0.####}"));
                }
                _notify?.Appended(line.Id);
                return line.Id;
            }
        }

        private Point3d EraseLine(ObjectId id)
        {
            using (var tr = Util.StartTransaction())
            using (var currentLayer = tr.GetObject<LayerTableRecord>(Util.Database().Clayer, OpenMode.ForWrite))
            {
                if (currentLayer is null) return Point3d.Origin;
                var stateLocked = currentLayer.IsLocked;
                if (stateLocked)
                {
                    currentLayer.IsLocked = false;
                }
                using (var line = tr.GetObject<Line>(id, OpenMode.ForWrite))
                {
                    if (line is null) return Point3d.Origin;
                    Util.GetCurrSpaceObjectIds().Remove(line.ObjectId);
                    line.Erase();
                    if (stateLocked)
                    {
                        currentLayer.IsLocked = true;
                    }

                    tr.Commit();
                    return line.StartPoint;
                }
            }
        }

        #region Use method if continue with arc or pline' last segment is arc.
        private Point3d? DrawByLength(Vector3d direction, Point3d startPoint, Stack<ObjectId> stack)
        {
            using (var line = new Line(startPoint, startPoint))
            {
                var optEndPoint = new LineDistanceJigActions(line, direction)
                {
                    Options = new JigPromptDistanceOptions($"{LineRes.UI_ContP2}")
                    {
                        UseBasePoint = true,
                        BasePoint = startPoint,
                        UserInputControls = UserInputControls.Accept3dCoordinates |
                                                   UserInputControls.UseBasePointElevation,
                        Cursor = CursorType.CrosshairNoRotate,
                    }
                };

                var resDistance = _userInput.AcquireDistance(optEndPoint);
                if (resDistance.Status != PromptStatus.OK)
                {
                    return null;
                }

                stack.Push(AppendLine(line.StartPoint, line.EndPoint));
                return line.EndPoint;
            }
        }

        #endregion

        #region Temporary Create mock ployline and mock arc to test for case continue.
        [CommandMethod("JrxCommand", "SmxDrawPLine", CommandFlags.Modal)]
        public void DrawPline()
        {
            using (var tr = Util.StartTransaction())
            {
                var acPoly = new Polyline();
                acPoly.SetDatabaseDefaults();
                acPoly.AddVertexAt(0, new Point2d(2, 4), 0, 0, 0);
                acPoly.AddVertexAt(1, new Point2d(4, 2), 0, 0, 0);
                acPoly.AddVertexAt(2, new Point2d(8, 3), 0.5, 0, 0);
                acPoly.AddVertexAt(2, new Point2d(10, 3), 0.3, 0, 0);
                acPoly.TransformBy(CoordConverter.UcsToWcs());
                acPoly.Closed = true;
                tr.AddNewlyCreatedDBObject(acPoly, true, tr.CurrentSpace());
                tr.Commit();
                Util.AddObjectIdToCollection(acPoly.ObjectId);
            }
        }

        [CommandMethod("JrxCommand", "SmxDrawArc", CommandFlags.Modal)]
        public void DrawArc()
        {
            using (var tr = Util.StartTransaction())
            {
                var acArc = new Arc(new Point3d(14, 6, 0), 3, 1.117, 3.5605);
                acArc.TransformBy(CoordConverter.UcsToWcs());
                tr.AddNewlyCreatedDBObject(acArc, true, tr.CurrentSpace());
                tr.Commit();
                Util.AddObjectIdToCollection(acArc.ObjectId);
            }
        }

        #endregion
    }
}

