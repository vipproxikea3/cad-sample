﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;

#endif
using JrxCad.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JrxCad.Command.LineCmd
{
    public partial class LineCmd
    {
        private readonly IUserInput _userInput;
        private readonly INotifyId _notify;

        /// <summary>
        /// IUserInput Interface
        /// </summary>
        public interface IUserInput : IUserInputBase
        {
        }

        /// <summary>
        /// UserInputImpl
        /// </summary>
        private class UserInput : UserInputBase, IUserInput
        {
        }

        public class LineDistanceJigActions : DistanceDrawJigN.JigActions
        {
            private readonly Line _line;
            private readonly Vector3d _direction;

            public LineDistanceJigActions(Line line, Vector3d direction)
            {
                _line = line;
                _line.SetDatabaseDefaults();
                _direction = direction;
            }

            public override JigPromptDistanceOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                return geometry.Draw(_line);
            }

            public override void OnUpdate()
            {
                var t = LastValue / Math.Sqrt(Math.Pow(_direction.X, 2) + Math.Pow(_direction.Y, 2) + Math.Pow(_direction.Z, 2));
                _line.EndPoint = new Point3d(_line.StartPoint.X + t * _direction.X, _line.StartPoint.Y + t * _direction.Y, _line.StartPoint.Z + t * _direction.Z);
            }
        }

        private class LineJigActions : PointDrawJigN.JigActions
        {
            private readonly Line _line;

            public sealed override JigPromptPointOptions Options { get; set; }

            public LineJigActions(string message, bool appendC, Line line)
            {
                _line = line;
                _line.SetDatabaseDefaults();
                Options = new JigPromptPointOptions($"\n{message}")
                {
                    UseBasePoint = true,
                    BasePoint = line.StartPoint,
                    UserInputControls = UserInputControls.NullResponseAccepted | UserInputControls.Accept3dCoordinates
                };

                if (appendC)
                {
                    Options.Keywords.Add("C", $"{LineRes.UI_Key_Close}", $"{LineRes.UI_Key_Close}");
                }
                Options.Keywords.Add("U", $"{LineRes.UI_Key_Undo}", $"{LineRes.UI_Key_Undo}");
            }

            public override bool Draw(WorldGeometry geometry)
            {
                return geometry.Draw(_line);
            }

            public override void OnUpdate()
            {
                _line.EndPoint = LastValue;
            }
        }

        private class LineDeferredJigActions : PointDrawJigN.JigActions
        {
            private CustomOSnapMode PreviousSnapMode { get; set; }
            private Point3d FirstPointW { get; set; }
            private Entity3d FirstEntityU { get; set; }
            public Point3d Start { get; private set; }
            public Point3d End { get; private set; }
            public bool IsFindDeferred { get; private set; }

            public sealed override JigPromptPointOptions Options { get; set; }

            public LineDeferredJigActions(CustomOSnapMode previousSnapMode, Point3d startPoint, Entity3d firstEntity, bool isSnap)
            {
                PreviousSnapMode = previousSnapMode;
                FirstPointW = startPoint;
                FirstEntityU = firstEntity;
                Options = new JigPromptPointOptions($"\n{LineRes.UI_NextP123}")
                {
                    UserInputControls = UserInputControls.NullResponseAccepted
                                        | UserInputControls.Accept3dCoordinates
                                        | UserInputControls.UseBasePointElevation
                };

                if (!isSnap)
                {
                    Options.Keywords.Add("U", $"{LineRes.UI_Key_Undo}", $"{LineRes.UI_Key_Undo}");
                    Options.Keywords.Add("T", $"{LineRes.UI_Key_Tangent}", $"{LineRes.UI_Key_Tangent}");
                    Options.Keywords.Add("P", $"{LineRes.UI_Key_Perpendicular}", $"{LineRes.UI_Key_Perpendicular}");
                }
                else
                {
                    Options.Message = $"\n{LineRes.UI_to}";
                    Options.Keywords.Add("_u", $"{LineRes._u}", $"{LineRes._u}");
                    Options.AppendKeywordsToMessage = false;
                }
            }

            public override bool Draw(WorldGeometry geometry)
            {
                return geometry.WorldLine(Start, End);
            }

            public override void OnUpdate()
            {
                var ucsToWcs = CoordConverter.UcsToWcs();
                Entity3d other;
                if (FirstEntityU is CircularArc3d circular)
                {
                    other = new CircularArc3d(LastValue.TransformBy(ucsToWcs.Inverse()), circular.Normal, 0.0);
                }
                else
                {
                    //var line3d = (LineSegment3d)FirstEntityU;
                    other = new CircularArc3d(LastValue.TransformBy(ucsToWcs.Inverse()), CoordConverter.UcsZAxis(), 0.0);
                }

                (List<LineSegment3d>, string) lines;
                if (PreviousSnapMode == CustomOSnapMode.DeferredTangent)
                {
                    lines = GetTangentsToCurve((CircularArc3d)FirstEntityU, (CircularArc3d)other);
                }
                else
                {
                    lines = GetPerpendicular(FirstEntityU, (CircularArc3d)other);
                }

                if (lines.Item1 != null)
                {
                    var perpendicular = lines
                        .Item1.OrderBy(n => n.StartPoint.DistanceTo(FirstPointW)).First();
                    Start = perpendicular.StartPoint.TransformBy(ucsToWcs);
                    End = perpendicular.EndPoint.TransformBy(ucsToWcs);
                    IsFindDeferred = true;
                }
                else
                {
                    Start = FirstPointW;
                    End = LastValue;
                    IsFindDeferred = false;
                }
            }
        }
    }
}
