﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

#endif
using System.Collections.Generic;
using JrxCad.Utility;
using System;

namespace JrxCad.Command.LineCmd
{
    public class CustomOSnapInfo
    {
        public Entity3d EntityW;
        public bool BeforeSnapIsDgn;
        public ObjectId? BlockId;
        public Point3d? LastSnapPointW;

        public void SnapInfo(ObjectSnapContext context, ObjectSnapInfo result)
        {
            BlockId = context.PickedObject.BlockId;
            if (BeforeSnapIsDgn)
            {
                if (context.PickedObject is Polyline)
                {
                    BlockId = null;
                    return;
                }
                else
                {
                    BeforeSnapIsDgn = false;
                }
            }

            switch (context.PickedObject)
            {
                case Line line:
                    AddSnapPointForLine(line, context.PickPoint, result);
                    break;
                case Circle circle:
                    var pickPointCurveW = context.PickPoint.TransformBy(context.ViewTransform).TransformBy(CoordConverter.DcsToWcs(Util.Editor().GetCurrentView()));
                    AddSnapPointForCircle(circle, pickPointCurveW, result);
                    break;
                case Arc arc:
                    pickPointCurveW = context.PickPoint.TransformBy(context.ViewTransform).TransformBy(CoordConverter.DcsToWcs(Util.Editor().GetCurrentView())); ;
                    AddSnapPointForArc(arc, pickPointCurveW, result);
                    break;
                case Ray ray:
                    AddSnapPointForRay(ray, context.PickPoint, result);
                    break;
                case Xline xLine:
                    AddSnapPointForXLine(xLine, context.PickPoint, result);
                    break;
                case Trace trace:
                    AddSnapPointForTrace(trace, context.PickPoint, result);
                    break;
                case Polyline polyline:
                    using (var polylineClone = (Polyline)polyline.Clone())
                    {
                        polylineClone.TransformBy(CoordConverter.WcsToUcs());
                        var pickTopView = GetPickPointFromTopView(context.PickPoint, polylineClone.Elevation);
                        AddSnapPointForPolyLine(polyline, pickTopView, result);
                    }
                    break;
                case Polyline2d polyline2d:
                    using (var polyline2dClone = (Polyline2d)polyline2d.Clone())
                    {
                        polyline2dClone.TransformBy(CoordConverter.WcsToUcs());
                        var pickTopView = GetPickPointFromTopView(context.PickPoint, polyline2dClone.Elevation);
                        AddSnapPointForPolyLine2d(polyline2d, pickTopView, result);
                    }
                    break;
                case PolygonMesh polygonMesh:
                    var listVertex = new List<Point3d>();
                    var vertices = polygonMesh.GetEnumerator();
                    while (vertices.MoveNext())
                    {
                        if (vertices.Current != null)
                        {
                            var id = (ObjectId)vertices.Current;
                            if (id.GetObject(OpenMode.ForRead) is FaceRecord)
                            {
                                continue;
                            }
                            var vtx = (PolygonMeshVertex)id.GetObject(OpenMode.ForRead);
                            listVertex.Add(vtx.Position);
                        }
                    }

                    using (var listLine = new DBObjectCollection())
                    {
                        foreach (var t in listVertex)
                        {
                            foreach (var t1 in listVertex)
                            {
                                listLine.Add(new Line(t, t1));
                            }
                        }
                        using (var ent = CheckNearestEntityToPickPoint(listLine, context.PickPoint) as Line)
                        {
                            AddSnapPointForLine(ent, context.PickPoint, result);
                        }
                    }
                    break;
                case PolyFaceMesh polyFaceMesh:
                    {
                        var listVertexPF = new List<Point3d>();
                        var verticesAndFaces = polyFaceMesh.GetEnumerator();
                        while (verticesAndFaces.MoveNext())
                        {
                            if (verticesAndFaces.Current != null)
                            {
                                var id = (ObjectId)verticesAndFaces.Current;
                                if (id.GetObject(OpenMode.ForRead) is FaceRecord)
                                {
                                    continue;
                                }
                                var vtx = (PolyFaceMeshVertex)id.GetObject(OpenMode.ForRead);
                                listVertexPF.Add(vtx.Position);
                            }
                        }
                        using (var listLine = new DBObjectCollection())
                        {
                            foreach (var t in listVertexPF)
                            {
                                foreach (var t1 in listVertexPF)
                                {
                                    listLine.Add(new Line(t, t1));
                                }
                            }
                            using (var ent1 = CheckNearestEntityToPickPoint(listLine, context.PickPoint) as Line)
                            {
                                AddSnapPointForLine(ent1, context.PickPoint, result);
                            }
                        }
                        break;
                    }
                case DgnReference dgnReference:
                    BlockId = null;
                    AddSnapPointForDgnReference(dgnReference, context.PickPoint, result);
                    break;
                case Viewport viewport:
                    AddSnapPointForViewPort(viewport, context.PickPoint, result);
                    break;
                case Mline _:
                case Leader _:
                case Region _:
                case Polyline3d _:
                case Ole2Frame _:
                case RasterImage _:
                    AddSnapPointForEntity(context.PickedObject, context.PickPoint, result);
                    break;
            }
        }

        private void AddSnapPointForDgnReference(DgnReference dgnReference, Point3d pickPoint, ObjectSnapInfo result)
        {
            if (!CustomOSnapApp.TangentOrPerpendicular)
            {
                return;
            }
            BeforeSnapIsDgn = true;
            var p1 = new Point2d(dgnReference.Position.X, dgnReference.Position.Y);
            var p2 = new Point2d(dgnReference.Position.X, dgnReference.Position.Y + dgnReference.Height);
            var p3 = new Point2d(dgnReference.Position.X + dgnReference.Width, dgnReference.Position.Y + dgnReference.Height);
            var p4 = new Point2d(dgnReference.Position.X + dgnReference.Width, dgnReference.Position.Y);
            using (var polyline = new Polyline())
            {
                polyline.AddVertexAt(0, p1, 0, 0, 0);
                polyline.AddVertexAt(1, p2, 0, 0, 0);
                polyline.AddVertexAt(2, p3, 0, 0, 0);
                polyline.AddVertexAt(3, p4, 0, 0, 0);
                polyline.Closed = true;
                polyline.Elevation = dgnReference.Position.Z;

                AddSnapPointForPolyLine(polyline, pickPoint, result);
            }
        }

        private void AddSnapPointForViewPort(Viewport viewport, Point3d pickPoint, ObjectSnapInfo result)
        {
            if (!CustomOSnapApp.TangentOrPerpendicular)
            {
                return;
            }
            var p1 = new Point2d(viewport.CenterPoint.X - viewport.Width / 2, viewport.CenterPoint.Y + viewport.Height / 2);
            var p2 = new Point2d(viewport.CenterPoint.X + viewport.Width / 2, viewport.CenterPoint.Y + viewport.Height / 2);
            var p3 = new Point2d(viewport.CenterPoint.X + viewport.Width / 2, viewport.CenterPoint.Y - viewport.Height / 2);
            var p4 = new Point2d(viewport.CenterPoint.X - viewport.Width / 2, viewport.CenterPoint.Y - viewport.Height / 2);
            var polyline = new Polyline();
            polyline.AddVertexAt(0, p1, 0, 0, 0);
            polyline.AddVertexAt(1, p2, 0, 0, 0);
            polyline.AddVertexAt(2, p3, 0, 0, 0);
            polyline.AddVertexAt(3, p4, 0, 0, 0);
            polyline.Closed = true;
            AddSnapPointForPolyLine(polyline, pickPoint, result);
        }

        private void AddSnapPointForLine(Line line, Point3d pickPoint, ObjectSnapInfo result)
        {
            if (!CustomOSnapApp.TangentOrPerpendicular)
            {
                return;
            }
            var snapPointWCS = line.GetClosestPointTo(pickPoint, Util.Editor().GetCurrentView().ViewDirection, false);
            result.SnapPoints.Add(snapPointWCS);
            LastSnapPointW = snapPointWCS;
            EntityW?.Dispose();
            EntityW = new LineSegment3d(line.StartPoint, line.EndPoint);
        }

        private void AddSnapPointForTrace(Trace trace, Point3d pickPoint, ObjectSnapInfo result)
        {
            if (!CustomOSnapApp.TangentOrPerpendicular)
            {
                return;
            }
            using (var cvs = new DBObjectCollection())
            {
                for (short i = 0; i < 4; i++)
                {
                    var startPoint = trace.GetPointAt(i);
                    var endPoint = i == 3 ? trace.GetPointAt(0) : trace.GetPointAt((short)(i + 1));
                    cvs.Add(new Line(startPoint, endPoint));
                }
                using (var lineNearestPickPoint = CheckNearestEntityToPickPoint(cvs, pickPoint) as Line)
                {
                    AddSnapPointForLine(lineNearestPickPoint, pickPoint, result);
                }
            }
        }

        private void AddSnapPointForCircle(Circle circle, Point3d pickPoint, ObjectSnapInfo result)
        {
            var viewDirection = Util.Editor().GetCurrentView().ViewDirection;
            var snapPointWCS = circle.GetClosestPointTo(pickPoint, viewDirection, false);
            result.SnapPoints.Add(snapPointWCS.TransformBy(CoordConverter.WcsToOcs(CoordConverter.UcsZAxis())));
            LastSnapPointW = snapPointWCS;
            EntityW?.Dispose();
#if _AutoCAD_
            EntityW = circle.GetGeCurve();
#elif _IJCAD_
            EntityW = new CircularArc3d(circle.Center, circle.Normal, circle.Radius);
#endif
        }

        private void AddSnapPointForArc(Arc arc, Point3d pickPoint, ObjectSnapInfo result)
        {
            var viewDirection = Util.Editor().GetCurrentView().ViewDirection;
            var snapPointWCS = arc.GetClosestPointTo(pickPoint, viewDirection, false);
            result.SnapPoints.Add(snapPointWCS.TransformBy(CoordConverter.WcsToOcs(CoordConverter.UcsZAxis())));
            LastSnapPointW = snapPointWCS;
            EntityW?.Dispose();
#if _AutoCAD_
            EntityW = arc.GetGeCurve();
#elif _IJCAD_
            EntityW = new CircularArc3d(arc.StartPoint, arc.GetPointAtDist((arc.EndParam - arc.StartParam) / 2), arc.EndPoint);
#endif
        }

        private void AddSnapPointForRay(Ray ray, Point3d pickPoint, ObjectSnapInfo result)
        {
            if (!CustomOSnapApp.TangentOrPerpendicular)
            {
                return;
            }
            var snapPointWCS = ray.GetClosestPointTo(pickPoint, Util.Editor().GetCurrentView().ViewDirection, false);
            result.SnapPoints.Add(snapPointWCS);
            LastSnapPointW = snapPointWCS;
            EntityW?.Dispose();
            EntityW = new LineSegment3d(ray.BasePoint, ray.SecondPoint);
        }

        private void AddSnapPointForXLine(Xline xLine, Point3d pickPoint, ObjectSnapInfo result)
        {
            if (!CustomOSnapApp.TangentOrPerpendicular)
            {
                return;
            }
            var snapPointWCS = xLine.GetClosestPointTo(pickPoint, Util.Editor().GetCurrentView().ViewDirection, false);
            result.SnapPoints.Add(snapPointWCS);
            LastSnapPointW = snapPointWCS;
            EntityW?.Dispose();
            EntityW = new LineSegment3d(xLine.BasePoint, xLine.SecondPoint);
        }

        private void AddSnapPointForEntity(Entity entity, Point3d pickPoint, ObjectSnapInfo result)
        {
            using (var entities = new DBObjectCollection())
            {

                entity.Explode(entities);
                var nearestEntityToPickPoint = CheckNearestEntityToPickPoint(entities, pickPoint);
                if (nearestEntityToPickPoint is Line line)
                {
                    AddSnapPointForLine(line, pickPoint, result);
                }
                else if (nearestEntityToPickPoint is Polyline pLine)
                {
                    AddSnapPointForPolyLine(pLine, pickPoint, result);
                }
                else if (nearestEntityToPickPoint is Circle circle)
                {
                    AddSnapPointForCircle(circle, pickPoint, result);
                }
                else if (nearestEntityToPickPoint is Arc arc)
                {
                    AddSnapPointForArc(arc, pickPoint, result);
                }
            }
        }

        private void AddSnapPointForPolyLine(Polyline polyLine, Point3d pickPoint, ObjectSnapInfo result)
        {
            var nearestSegmentToPickPoint = GetNearestSegmentOfPolylineToPickPoint(polyLine, pickPoint);
            if (!CustomOSnapApp.TangentOrPerpendicular)
            {
                if (nearestSegmentToPickPoint is LineSegment3d)
                {
                    return;
                }
            }
            if (nearestSegmentToPickPoint is LineSegment3d lineSegment3d)
            {
                using (var line = new Line(lineSegment3d.StartPoint, lineSegment3d.EndPoint))
                {
                    AddSnapPointForLine(line, pickPoint, result);
                }
            }
            else if (nearestSegmentToPickPoint is CircularArc3d circular3d)
            {
                using (var circle = new Circle(circular3d.Center, circular3d.Normal, circular3d.Radius))
                {
                    var snapPointWCS = circle.GetClosestPointTo(pickPoint, false);
                    result.SnapPoints.Add(snapPointWCS);
                    LastSnapPointW = snapPointWCS;
                }
            }
            EntityW = nearestSegmentToPickPoint;
        }

        private void AddSnapPointForPolyLine2d(Polyline2d polyline2d, Point3d pickPoint, ObjectSnapInfo result)
        {
            var vertices = polyline2d.GetEnumerator();
            using (var polyLine = new Polyline())
            {
                polyLine.SetDatabaseDefaults();
                var indexV = 0;
                while (vertices.MoveNext())
                {
                    if (vertices.Current != null)
                    {
                        var id = (ObjectId)vertices.Current;
                        var vtx = (Vertex2d)id.GetObject(OpenMode.ForRead);
                        polyLine.AddVertexAt(indexV, new Point2d(vtx.Position.X, vtx.Position.Y), vtx.Bulge, vtx.StartWidth, vtx.EndWidth);
                        indexV++;
                    }
                }
                polyLine.Closed = polyline2d.Closed;
                polyLine.Elevation = polyline2d.Elevation;
                AddSnapPointForPolyLine(polyLine, pickPoint, result);
            }
        }

        private Entity3d GetNearestSegmentOfPolylineToPickPoint(Polyline pLine, Point3d pickPoint)
        {
            var lineSgm = new LineSegment3d();
            var arcSgm = new CircularArc3d();
            double distanceToLineMin = 0;
            double distanceToArcMin = 0;
            var isIncludeLineSeg = false;
            var isIncludeArcSeg = false;
            var numberOfSegment = pLine.Closed ? pLine.NumberOfVertices : pLine.NumberOfVertices - 1;
            for (int i = 0; i < numberOfSegment; i++)
            {
                var bulge = pLine.GetBulgeAt(i);
                if (bulge == 0)
                {
                    var lineSegment = pLine.GetLineSegmentAt(i);
                    var line = new Line(lineSegment.StartPoint, lineSegment.EndPoint);
                    var distanceTmp = line.GetClosestPointTo(pickPoint, false).DistanceTo(pickPoint);
                    if (!isIncludeLineSeg)
                    {
                        distanceToLineMin = distanceTmp;
                        lineSgm = lineSegment;
                        isIncludeLineSeg = true;
                        continue;
                    }

                    if (distanceTmp < distanceToLineMin)
                    {
                        lineSgm = lineSegment;
                        distanceToLineMin = distanceTmp;
                    }
                }
                else
                {
                    var arcSegment = pLine.GetArcSegmentAt(i);
                    var vecCenterToPickPoint = arcSegment.Center.GetVectorTo(pickPoint);
                    var t = arcSegment.Radius / Math.Sqrt(Math.Pow(vecCenterToPickPoint.X, 2) + Math.Pow(vecCenterToPickPoint.Y, 2) + Math.Pow(vecCenterToPickPoint.Z, 2));
                    var lineToCheckInterSec = new Line3d(arcSegment.Center, new Point3d(arcSegment.Center.X + t * vecCenterToPickPoint.X, arcSegment.Center.Y + t * vecCenterToPickPoint.Y, arcSegment.Center.Z + t * vecCenterToPickPoint.Z));
                    var inters = arcSegment.IntersectWith(lineToCheckInterSec);
                    if (inters != null && inters.Length > 0)
                    {
                        double distanceTmp;
                        if (inters.Length == 1)
                        {
                            distanceTmp = inters[0].DistanceTo(pickPoint);
                        }
                        else
                        {
                            distanceTmp = inters[0].DistanceTo(pickPoint) < inters[1].DistanceTo(pickPoint) ? inters[0].DistanceTo(pickPoint) : inters[1].DistanceTo(pickPoint);
                        }

                        if (!isIncludeArcSeg)
                        {
                            arcSgm = arcSegment;
                            distanceToArcMin = distanceTmp;
                            isIncludeArcSeg = true;
                            continue;
                        }

                        if (distanceTmp < distanceToArcMin)
                        {
                            arcSgm = arcSegment;
                            distanceToArcMin = distanceTmp;
                        }
                    }
                }
            }
            if (!isIncludeLineSeg)
            {
                return arcSgm;
            }
            if (!isIncludeArcSeg)
            {
                return lineSgm;
            }
            if (distanceToLineMin >= distanceToArcMin)
            {
                return arcSgm;
            }
            else
            {
                return lineSgm;
            }
        }

        private Point3d GetPickPointFromTopView(Point3d pickPoint, double elev)
        {
            var direc = Util.Editor().GetCurrentView().ViewDirection;
            var angle = Math.PI / 2 - Util.Editor().GetCurrentView().ViewDirection.GetAngleTo(CoordConverter.UcsZAxis());
            var dis = elev / Math.Sin(angle);
            var t = dis / Math.Sqrt(Math.Pow(direc.X, 2) + Math.Pow(direc.Y, 2) + Math.Pow(direc.Z, 2));
            var snapPoint = new Point3d(pickPoint.X + t * direc.X, pickPoint.Y + t * direc.Y, pickPoint.Z + t * direc.Z);
            return snapPoint;
        }

        private Entity CheckNearestEntityToPickPoint(DBObjectCollection dbObjectCollection, Point3d pickPoint)
        {
            var viewDirection = Util.Editor().GetCurrentView().ViewDirection;
            Entity entity = null;
            double? minAngle = null;
            Point3d snapPointTmp;
            foreach (Entity item in dbObjectCollection)
            {
                if (item is Line line)
                {
                    snapPointTmp = line.GetClosestPointTo(pickPoint, viewDirection, true);
                }
                else if (item is Polyline polyLine)
                {
                    snapPointTmp = polyLine.GetClosestPointTo(pickPoint, viewDirection, false);
                }
                else if (item is Circle circle)
                {
                    snapPointTmp = circle.GetClosestPointTo(pickPoint, viewDirection, false);
                }
                else if (item is Arc arc)
                {
                    snapPointTmp = arc.GetClosestPointTo(pickPoint, viewDirection, false);
                }
                else
                {
                    continue;
                }

                var snapPointTmpU = snapPointTmp.TransformBy(CoordConverter.WcsToUcs());
                double angle;
                if (snapPointTmpU.Z >= 0)
                {

                    angle = (pickPoint - viewDirection).GetVectorTo(snapPointTmp).GetAngleTo(viewDirection);
                }
                else
                {
                    angle = snapPointTmp.GetVectorTo(pickPoint + viewDirection).GetAngleTo(viewDirection);
                }

                if (minAngle == null)
                {
                    minAngle = angle;
                    entity = item;
                    continue;
                }

                if (angle < minAngle)
                {
                    minAngle = angle;
                    entity = item;
                }
            }

            return entity;
        }
    }
}
