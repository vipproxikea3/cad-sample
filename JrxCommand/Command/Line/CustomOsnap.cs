﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
using GrxCAD.Runtime;
using GI = GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using GI = Autodesk.AutoCAD.GraphicsInterface;

#endif
using System;

namespace JrxCad.Command.LineCmd
{
    public class CustomOSnapApp : IExtensionApplication
    {
        public CustomOSnapInfo Info = new CustomOSnapInfo();
        private readonly DrawSnapGlyph _glyph = new DrawSnapGlyph();
        private CustomObjectSnapMode _mode;
        public static bool TangentOrPerpendicular;

        public void Initialize(bool tangentOrPerpendicular)
        {
            TangentOrPerpendicular = tangentOrPerpendicular;
            Initialize();
        }

        public void Initialize()
        {
            if (!TangentOrPerpendicular)
            {
                _mode = new CustomObjectSnapMode("DeferredtangentCustom", "DeferredtangentCustom", "Deferred tangent", _glyph);
            }
            else
            {
                _mode = new CustomObjectSnapMode("PerpendicularCustom", "PerpendicularCustom", "Deferred perpendicular", _glyph);
            }

            _mode.ApplyToEntityType(RXObject.GetClass(typeof(Entity)), Info.SnapInfo);

            if (!TangentOrPerpendicular)
            {
                CustomObjectSnapMode.Activate("DeferredtangentCustom");
            }
            else
            {
                CustomObjectSnapMode.Activate("PerpendicularCustom");
            }
        }

        public void Terminate()
        {
            if (!TangentOrPerpendicular)
            {
                if (CustomObjectSnapMode.IsActive("DeferredtangentCustom"))
                {
                    CustomObjectSnapMode.Deactivate("DeferredtangentCustom");
                }
            }
            else
            {
                if (CustomObjectSnapMode.IsActive("PerpendicularCustom"))
                {
                    CustomObjectSnapMode.Deactivate("PerpendicularCustom");
                }
            }
        }
    }

    public class DrawSnapGlyph : GI.Glyph
    {
        private static Point3d _pt;

        public override void SetLocation(Point3d point)
        {
            _pt = point;
        }

        protected override void SubViewportDraw(GI.ViewportDraw vd)
        {
            var glyphPixels = CustomObjectSnapMode.GlyphSize;
            var glyphSize = vd.Viewport.GetNumPixelsInUnitSquare(_pt);

            //Circle
            var glyphHeight = (glyphPixels / glyphSize.Y) * 1.2;
            var e2W = vd.Viewport.EyeToWorldTransform;
            var dir = Vector3d.ZAxis.TransformBy(e2W);
            var pt = _pt.TransformBy(e2W);
            var r = glyphHeight * 0.6;

            if (CustomOSnapApp.TangentOrPerpendicular) // Per
            {
                //LineStraight
                var lx = pt + new Vector3d(-r, r, 0).TransformBy(e2W);
                var lo = pt + new Vector3d(-r, -r, 0).TransformBy(e2W);
                var ly = pt + new Vector3d(r, -r, 0).TransformBy(e2W);
                Point3d[] points = { lx, lo, ly };
                vd.SubEntityTraits.LineWeight = LineWeight.LineWeight030;
                vd.Geometry.Polyline(new GI.Polyline(new Point3dCollection(points), dir, IntPtr.Zero));

                //DownerLine
                var lx1 = pt + new Vector3d(-r, r - r * 0.9, 0).TransformBy(e2W);
                var lo1 = pt + new Vector3d(r - r * 0.9, r - r * 0.9, 0).TransformBy(e2W);
                var ly1 = pt + new Vector3d(r - r * 0.9, -r, 0).TransformBy(e2W);
                Point3d[] points1 = { lx1, lo1, ly1 };
                vd.SubEntityTraits.LineWeight = LineWeight.LineWeight030;
                vd.Geometry.Polyline(new GI.Polyline(new Point3dCollection(points1), dir, IntPtr.Zero));

                //...
                r = glyphHeight * 0.4;
                var style = new GI.TextStyle
                {
                    Font = new GI.FontDescriptor("Meiryo", false, false, 0, 0)
                };
                var pts = pt + new Vector3d(2 * r, -r * 1.5, 0).TransformBy(e2W);
                style.TextSize = glyphHeight * 0.7;
                vd.Geometry.Text(pts, vd.Viewport.ViewDirection, lo.GetVectorTo(ly), "...", true, style);
            }
            else
            {
                //Circle
                vd.Geometry.Circle(pt, glyphHeight * 0.5, dir);
                //UpperLine 
                var ulx = pt + new Vector3d(-r, r, 0).TransformBy(e2W);
                var uly = pt + new Vector3d(r, r, 0).TransformBy(e2W);
                Point3d[] points = { ulx, uly };
                vd.SubEntityTraits.LineWeight = LineWeight.LineWeight030;
                vd.Geometry.Polyline(new GI.Polyline(new Point3dCollection(points), dir, IntPtr.Zero));

                //...
                r = glyphHeight * 0.4;
                var style = new GI.TextStyle
                {
                    Font = new GI.FontDescriptor("Meiryo", false, false, 0, 0)
                };
                var pts = pt + new Vector3d(2 * r, -r, 0).TransformBy(e2W);
                style.TextSize = glyphHeight * 0.7;
                vd.Geometry.Text(pts, vd.Viewport.ViewDirection, ulx.GetVectorTo(uly), "...", true, style);
            }
        }
    }
}
