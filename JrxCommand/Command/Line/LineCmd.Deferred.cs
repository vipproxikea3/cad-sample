﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

#endif

using System.Collections.Generic;
using System.Linq;
using JrxCad.Helpers;
using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.Command.LineCmd
{
    public partial class LineCmd
    {
        private (Point3d?, bool) GetFirstPoint()
        {
            using (var tr = Util.StartTransaction())
            {
                while (true)
                {
                    var optStartPoint = new PromptPointOptions($"\n{LineRes.UI_P1}")
                    {
                        AllowNone = true,
                    };

                    if (_customOSnap == null)
                    {
                        optStartPoint.Keywords.Add("T", $"{LineRes.UI_Key_Tangent}", $"{LineRes.UI_Key_Tangent}");
                        optStartPoint.Keywords.Add("P", $"{LineRes.UI_Key_Perpendicular}", $"{LineRes.UI_Key_Perpendicular}");
                    }
                    else
                    {
                        optStartPoint.Message = $"\n{LineRes.UI_to}";
                        optStartPoint.Keywords.Add("_u", $"{LineRes._u}", $"{LineRes._u}");
                        optStartPoint.AppendKeywordsToMessage = false;
                    }

                    var resStartPoint = Util.Editor().GetPoint(optStartPoint);

                    switch (resStartPoint.Status)
                    {
                        case PromptStatus.None:
                        case PromptStatus.Cancel:
                            _customOSnap?.Terminate();
                            _customOSnap = null;
                            tr.Commit();
                            _customOSnapMod1StPt = CustomOSnapMode.None;
                            return (null, resStartPoint.Status == PromptStatus.None);
                        case PromptStatus.Keyword:
                            switch (resStartPoint.StringResult)
                            {
                                case "T":
                                    _customOSnap = new CustomOSnapApp();
                                    _customOSnap.Initialize(false);
                                    _customOSnapMod1StPt = CustomOSnapMode.DeferredTangent;
                                    break;
                                case "P":
                                    _customOSnap = new CustomOSnapApp();
                                    _customOSnap.Initialize(true);
                                    _customOSnapMod1StPt = CustomOSnapMode.DeferredPerpendicular;
                                    break;
                                default:
                                    _customOSnap?.Terminate();
                                    _customOSnap = null;
                                    _customOSnapMod1StPt = CustomOSnapMode.None;
                                    break;
                            }

                            continue;
                        case PromptStatus.OK:
                            break;
                    }

                    if (_customOSnap == null)
                    {
                        return (resStartPoint.Value.TransformBy(CoordConverter.UcsToWcs()), true);
                    }

                    Matrix3d? mcsToWcs = null;
                    try
                    {
                        if (_customOSnap.Info.BlockId != null)
                        {
                            using (var tmp = tr.GetObject<BlockTableRecord>((ObjectId)_customOSnap.Info.BlockId, OpenMode.ForWrite))
                            {
                                if (!(tmp is null))
                                {
                                    var blockRef = tmp.GetBlockReferenceIds(false, false);
                                    var objectC = new ObjectId[blockRef.Count];
                                    var i = 0;
                                    foreach (ObjectId item in blockRef)
                                    {
                                        objectC[i] = item;
                                        i++;
                                    }

                                    mcsToWcs = CoordConverter.McsToWcs(objectC, out _);
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    if (_customOSnap.Info.LastSnapPointW == null)
                    {
                        WriteMessage($"{LineRes.CM_ERNoPoint}");
                        _customOSnap.Terminate();
                        _customOSnap = null;
                        continue;
                    }

                    var lastSnapPoint = _customOSnap.Info.LastSnapPointW;
                    if (mcsToWcs != null)
                    {
                        lastSnapPoint = lastSnapPoint?.TransformBy((Matrix3d)mcsToWcs);
                    }

                    var isSpecifiedPoint = lastSnapPoint?.IsEqualTo(resStartPoint.Value.TransformBy(CoordConverter.UcsToWcs()));
                    if (isSpecifiedPoint == false)
                    {
                        WriteMessage($"{LineRes.CM_ERNoPoint}");
                        _customOSnap.Terminate();
                        _customOSnap = null;
                        continue;
                    }

                    _entity3dU1?.Dispose();
                    _entity3dU1 = _customOSnap.Info.EntityW;
                    if (mcsToWcs != null)
                    {
                        _entity3dU1.TransformBy((Matrix3d)mcsToWcs);
                    }

                    _entity3dU1.TransformBy(CoordConverter.WcsToUcs());
                    if (_customOSnap != null)
                    {
                        _customOSnap.Terminate();
                        _customOSnap = null;
                    }
                    tr.Commit();
                    return (lastSnapPoint, true);
                }
            }
        }

        private Point3d? GetSecondPointTOrP(ref Point3d startPoint, out bool undo)
        {
            undo = false;

            while (true)
            {
                var jig = new LineDeferredJigActions(_customOSnapMod1StPt, startPoint, _entity3dU1, _customOSnap != null);

                var drag = _userInput.AcquirePoint(jig);
                switch (drag.Status)
                {
                    case PromptStatus.OK:
                        if (_customOSnap == null)
                        {
                            if (!jig.IsFindDeferred)
                            {
                                var mess = _customOSnapMod1StPt == CustomOSnapMode.DeferredTangent ? $"{LineRes.CM_ERNoTangent}" : $"{LineRes.CM_ERNoPerpendicular}";
                                WriteMessage($"\n{mess}");
                                continue;
                            }
                            else
                            {
                                startPoint = jig.Start;
                                return jig.End;
                            }
                        }
                        else
                        {
                            // TODO: Tranform MCS
                            //
                            if (_customOSnap.Info.LastSnapPointW == null || !jig.End.IsEqualTo((Point3d)_customOSnap.Info.LastSnapPointW))
                            {
                                WriteMessage($"{LineRes.CM_ERNoPoint}");
                                _customOSnap.Terminate();
                                _customOSnap = null;
                                _customOSnapMod2NdPt = CustomOSnapMode.None;
                                continue;
                            }
                            var entity3dU2 = (Entity3d)_customOSnap.Info.EntityW.Clone();
                            entity3dU2.TransformBy(CoordConverter.WcsToUcs());

                            (List<LineSegment3d>, string) listDeferredLineU;
                            if (_customOSnapMod1StPt != _customOSnapMod2NdPt)
                            {
                                listDeferredLineU = GetBothTangAndPer(_entity3dU1, entity3dU2, _customOSnapMod1StPt);
                            }
                            else
                            {
                                listDeferredLineU = _customOSnapMod1StPt == CustomOSnapMode.DeferredTangent ?
                                    GetTangentsToCurve((CircularArc3d)_entity3dU1, (CircularArc3d)entity3dU2) :
                                    GetPerpendicular(_entity3dU1, entity3dU2);
                            }

                            _customOSnap.Terminate();
                            _customOSnap = null;

                            if (listDeferredLineU.Item1 != null)
                            {
                                var startU = startPoint.TransformBy(CoordConverter.WcsToUcs());

                                var line = listDeferredLineU.Item1.OrderBy(a => (a.StartPoint - startU).Length + (a.EndPoint - jig.End.TransformBy(CoordConverter.WcsToUcs())).Length).First();
                                startPoint = line.StartPoint.TransformBy(CoordConverter.UcsToWcs());
                                return line.EndPoint.TransformBy(CoordConverter.UcsToWcs());
                            }

                            if (listDeferredLineU.Item2 != null)
                            {
                                WriteMessage(listDeferredLineU.Item2);
                            }

                            _customOSnapMod2NdPt = CustomOSnapMode.None;
                            continue;
                        }
                    case PromptStatus.Keyword:
                        if (drag.StringResult.ToUpper() == "U" || drag.StringResult == "_u")
                        {
                            undo = true;
                            _customOSnap?.Terminate();
                            _customOSnap = null;
                            _customOSnapMod2NdPt = CustomOSnapMode.None;
                            return null;
                        }
                        else
                        {
                            var perOrTan = drag.StringResult.ToUpper() == "P";
                            _customOSnap = new CustomOSnapApp();
                            _customOSnap.Initialize(perOrTan);
                            _customOSnapMod2NdPt = perOrTan ? CustomOSnapMode.DeferredPerpendicular : CustomOSnapMode.DeferredTangent;
                        }

                        continue;
                    case PromptStatus.Cancel:
                    case PromptStatus.None:
                        _customOSnap?.Terminate();
                        _customOSnap = null;
                        _customOSnapMod2NdPt = CustomOSnapMode.None;
                        return null;
                }
            }
        }

        public static (List<LineSegment3d>, string) GetTangentsToCurve(CircularArc3d firstEntity, CircularArc3d secondEntity)
        {
            if (firstEntity is null || secondEntity is null)
            {
                return (null, null);
            }

            var listTangentToCurve = new List<LineSegment3d>();
            var isDiffZ = false;
            var centerOfSecondEntity = secondEntity.Center;
            if (!firstEntity.Center.Z.IsEqual(secondEntity.Center.Z))
            {
                secondEntity.Center = new Point3d(secondEntity.Center.X, secondEntity.Center.Y, firstEntity.Center.Z);
                isDiffZ = true;
            }

            var listTangentToCircle = MathEx.GetTangentsToCircle(firstEntity, secondEntity);
            if (isDiffZ)
            {
                secondEntity.Center = centerOfSecondEntity;
                listTangentToCircle = listTangentToCircle.ConvertAll(a => new LineSegment3d(a.StartPoint, new Point3d(a.EndPoint.X, a.EndPoint.Y, centerOfSecondEntity.Z)));
            }

            foreach (var item in listTangentToCircle)
            {
                var inX = firstEntity.Radius == 0 ? new[] { firstEntity.Center } : firstEntity.IntersectWith(item);
                var inX2 = secondEntity.Radius == 0 ? new[] { secondEntity.Center } : secondEntity.IntersectWith(item);
                if (inX == null || inX2 == null) continue;

                if (inX.Length > 0 && inX2.Length > 0)
                {
                    listTangentToCurve.Add(item);
                }
            }

            if (listTangentToCurve.Count == 0)
            {
                return (null, $"{LineRes.CM_ERNoTangent}");
            }

            return (listTangentToCurve, null);
        }

        public static (List<LineSegment3d>, string) GetPerpendicular(Entity3d firstEntity, Entity3d secondEntity)
        {
            var result = new List<LineSegment3d>();

            if (firstEntity is CircularArc3d circular1 && secondEntity is CircularArc3d circular2)
            {
                var p = new Plane(circular1.Center, circular1.Normal, circular1.Center.GetVectorTo(circular2.Center));

                var inters1 = circular1.IntersectWith(p);
                var inters2 = circular2.Radius == 0 ? new[] { circular2.Center } : circular2.IntersectWith(p);

                if (inters1 != null && inters2 != null)
                {
                    foreach (var t in inters1)
                    {
                        foreach (var t1 in inters2)
                        {
                            result.Add(new LineSegment3d(t, t1));
                        }
                    }
                }
            }
            else if ((firstEntity is CircularArc3d && secondEntity is LineSegment3d) || (firstEntity is LineSegment3d && secondEntity is CircularArc3d))
            {
                CircularArc3d circular;
                LineSegment3d lineSegment3d;
                var reverse = false;
                if (firstEntity is CircularArc3d circularArc3d)
                {
                    circular = circularArc3d;
                    lineSegment3d = (LineSegment3d)secondEntity;
                }
                else
                {
                    reverse = true;
                    circular = (CircularArc3d)secondEntity;
                    lineSegment3d = (LineSegment3d)firstEntity;
                }

                if (!lineSegment3d.StartPoint.Z.IsEqual(lineSegment3d.EndPoint.Z) && circular.Radius != 0)
                {
                    return (null, $"{LineRes.CM_ERDifferenceZ}");
                }

                var p = new Plane(circular.Center, lineSegment3d.StartPoint.GetVectorTo(lineSegment3d.EndPoint));

                var pInLine = p.ClosestPointTo(lineSegment3d.StartPoint);

                var inters = circular.Radius.IsZero() ? new[] { circular.Center } : circular.IntersectWith(p);

                for (var j = 0; j < inters.Length; j++)
                {
                    if (!reverse)
                    {
                        result.Add(new LineSegment3d(inters[j], pInLine));
                    }
                    else
                    {
                        result.Add(new LineSegment3d(pInLine, inters[j]));
                    }
                }
            }
            else if (firstEntity is LineSegment3d && secondEntity is LineSegment3d)
            {
                return (null, $"{LineRes.CM_ERLineNotValid}");
            }

            if (result.Count == 0)
            {
                return (null, $"{LineRes.CM_ERNoPerpendicular}");
            }

            return (result, null);
        }

        public static (List<LineSegment3d>, string) GetBothTangAndPer(Entity3d firstEntity, Entity3d secondEntity, CustomOSnapMode snapModeOfFirstEntity)
        {
            var result = new List<LineSegment3d>();
            var invers = snapModeOfFirstEntity != CustomOSnapMode.DeferredTangent;
            if (firstEntity is CircularArc3d firstE && secondEntity is CircularArc3d secondE)
            {
                CircularArc3d circular1;
                CircularArc3d circular2;

                if (!invers)
                {
                    circular1 = firstE;
                    circular2 = secondE;
                }
                else
                {
                    circular1 = secondE;
                    circular2 = firstE;
                }

                (List<LineSegment3d>, string) tangentFromCircular1ToCenter2;
                if (circular1.Radius == 0)
                {
                    tangentFromCircular1ToCenter2 = (new List<LineSegment3d>() { new LineSegment3d(circular1.Center, circular2.Center) }, null);
                }
                else
                {
                    tangentFromCircular1ToCenter2 = GetTangentsToCurve(circular1, new CircularArc3d(circular2.Center, circular2.Normal, 0.0));
                }

                if (tangentFromCircular1ToCenter2.Item2 == null)
                {
                    foreach (var item in tangentFromCircular1ToCenter2.Item1)
                    {
                        var p = new Plane(circular2.Center, circular2.Normal, item.StartPoint.GetVectorTo(item.EndPoint));
                        var inter1 = circular1.Radius == 0 ? new[] { circular1.Center } : circular1.IntersectWith(p);
                        var inter2 = circular2.Radius == 0 ? new[] { circular2.Center } : circular2.IntersectWith(p);
                        if (inter1 == null || inter2 == null) continue;
                        foreach (var t in inter1)
                        {
                            foreach (var t1 in inter2)
                            {
                                var lineRes = new LineSegment3d(t, t1);
                                result.Add(lineRes);
                            }
                        }
                    }

                    if (result.Count == 0)
                    {
                        return (null, $"{LineRes.CM_ERNoPerpendicular}");
                    }
                }
                else
                {
                    return (null, $"{LineRes.CM_ERNoTangent}");
                }
            }
            else if ((firstEntity is CircularArc3d && secondEntity is LineSegment3d) || (firstEntity is LineSegment3d && secondEntity is CircularArc3d))
            {
                CircularArc3d circular;
                LineSegment3d lineSegment3d;

                if (firstEntity is CircularArc3d circularArc3d)
                {
                    circular = circularArc3d;
                    lineSegment3d = (LineSegment3d)secondEntity;
                }
                else
                {
                    circular = (CircularArc3d)secondEntity;
                    lineSegment3d = (LineSegment3d)firstEntity;
                }

                if (!lineSegment3d.StartPoint.Z.IsEqual(lineSegment3d.EndPoint.Z) && circular.Radius != 0)
                {
                    return (null, $"{LineRes.CM_ERDifferenceZ}");
                }

                var vecStartToEnd = lineSegment3d.StartPoint.GetVectorTo(lineSegment3d.EndPoint);
                var plane = new Plane(circular.Center, circular.Normal, vecStartToEnd);
                var inters1 = circular.IntersectWith(plane);
                if (inters1 != null && inters1.Length > 0)
                {
                    foreach (var t in inters1)
                    {
                        var pTmp = new Plane(t, vecStartToEnd);
                        var point = pTmp.ClosestPointTo(lineSegment3d.StartPoint);
                        result.Add(new LineSegment3d(t, point));
                    }
                }
            }

            if (result.Count == 0)
            {
                return (null, null);
            }

            if (invers)
            {
                result = result.ConvertAll(a => new LineSegment3d(a.EndPoint, a.StartPoint));
            }

            return (result, null);
        }
    }
}
