﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;
using CadException = GrxCAD.Runtime.Exception;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using CadException = Autodesk.AutoCAD.Runtime.Exception;

#endif

using JrxCad.Helpers;
using JrxCad.Utility;

using Exception = System.Exception;

namespace JrxCad.Command.MSpace
{
    public class MSpaceCmd : BaseCommand
    {
#if DEBUG
        [CommandMethod("JrxCommand", "MS", CommandFlags.Modal | CommandFlags.NoTileMode | CommandFlags.NoNewStack | CommandFlags.NoBlockEditor)]
#else 
        [CommandMethod("JrxCommand", "SmxMSpace", CommandFlags.Modal | CommandFlags.NoTileMode | CommandFlags.NoNewStack | CommandFlags.NoBlockEditor)]
#endif
        public override void OnCommand()
        {
            if (!Init())
            {
                return;
            }

            try
            {
                ExecuteCommand();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        private void ExecuteCommand()
        {
            try
            {
                //レイアウトタブでモデル空間がアクティブの場合
                if (SystemVariable.GetInt("CVPORT") > 1)
                {
                    Util.Editor().WriteMessage(MSpaceRes.CM_M1);
                    return;
                }

                //レイアウトタブにビューポートがない場合
                if (!HasAnyActiveModelVPort())
                {
                    Util.Editor().WriteMessage(MSpaceRes.CM_M2);
                    return;
                }

                //レイアウトタブでペーパー空間がアクティブの場合
                Util.Editor().SwitchToModelSpace();
            }
            catch (CadException)
            {
                // ignored case: Small viewport in AutoCAD, etc...
            }
        }

        private bool HasAnyActiveModelVPort()
        {
            var hasActiveModelVPort = false;
            using (var trans = Util.StartTransaction())
            {
                var curLayoutName = LayoutManager.Current.CurrentLayout;
                var layoutDict = trans.GetObject<DBDictionary>(Util.Database().LayoutDictionaryId, OpenMode.ForRead);
                if (layoutDict is null) return false;
                var curLayout = trans.GetObject<Layout>((ObjectId)layoutDict[curLayoutName], OpenMode.ForRead);
                using (var vPortIds = curLayout.GetViewports())
                {
                    foreach (ObjectId id in vPortIds)
                    {
                        //"vPortItem.Number == 1": Number of view port is 1 (is the default viewport in database).
                        //"!vPortItem.On": The On property for at least one Viewport object on the layout should be set to TRUE
                        var vPortItem = trans.GetObject<Viewport>(id, OpenMode.ForRead);
                        if (vPortItem is null || vPortItem.Number == 1 || !vPortItem.On) continue;
                        hasActiveModelVPort = true;
                        break;
                    }
                }
                trans.Commit();
            }

            return hasActiveModelVPort;
        }
    }
}
