﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;
using GrxCAD.GraphicsInterface;
using GrxCAD.Internal;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Internal;
using Application = Autodesk.AutoCAD.ApplicationServices.Application;

#endif
using JrxCad.Utility;
using Exception = System.Exception;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using JrxCad.Helpers;
using System;
using System.Drawing;

//TODO: [MinusInsert] please re-check source below with re-sharper
namespace JrxCad.Command.MinusInsert
{
    public partial class MinusInsertCmd : BaseCommand
    {
        #region Declare

        #region Enum
        public enum Valid
        {
            Ok = 0,
            InvalidCharacterBlockName = 1,
            FileWasNotFound = 2,
            BlockNameInvalid = 3,
            BlockNameNotSpecified = 4,
            UnKnowError = 7,
        }
        public enum TypeCursor
        {
            Normal = 0,
            Scale = 1,
            Rotation = 2,
            Corner = 3,
        }
        public enum ScaleType
        {
            X = 0,
            Y = 1,
            Z = 2,
        }
        #endregion

        private const double RotationDefault = -1; //Set -1 because rotation value can't be negative
        public BlockReference BlockReference;
        public string BlockName;
        public List<string> ListBlock;
        public List<string> ListBlockAnonymous;
        public string Factor;
        public string PathName;
        public string Unit;
        public bool RepeatActive;
        public bool ExplodeActive;
        public bool UniformScaleActive;
        public bool Explodable;
        public bool ExplodeFromInputActive;
        //private ImageBGRA32 _img;
        #endregion

        public MinusInsertCmd() : this(new UserInput()) { }
        public MinusInsertCmd(IMinusInsertUserInput userInput)
        {
            _userInput = userInput;
        }

        public void Initialize()
        {
            BlockName = "";
            ListBlock = new List<string>();
            Factor = "";
            PathName = null;
            Unit = "";
            RepeatActive = false;
            ExplodeActive = false;
            UniformScaleActive = false;
            Explodable = false;
            ExplodeFromInputActive = false;
        }
#if DEBUG
        [CommandMethod("JrxCommand", "I1", CommandFlags.Modal | CommandFlags.NoNewStack | CommandFlags.Interruptible)]
#else
        [CommandMethod("JrxCommand", "-SmxINSERT", CommandFlags.Modal | CommandFlags.NoNewStack | CommandFlags.Interruptible)]
#endif
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }
                ExcuteCommand();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        /// <summary>
        /// Insert block
        /// </summary>
        public void AddBlockOrDwg(string name, string path)
        {
            ObjectId idBlockDef;
            if (!string.IsNullOrEmpty(path) && Path.GetDirectoryName(path) != "")
            {
                idBlockDef = GetBlockFromDwg(name, path);
            }
            else
            {
                var objectIdTemp = GetBlockForName(name);
                if (objectIdTemp == null)
                {
                    return;
                }
                idBlockDef = objectIdTemp.Value;
            }
            using (var blockRef = new BlockReference(Point3d.Origin, idBlockDef))
            {
                ShowCMUnit(blockRef);

                var isScaled = false;
                var isRotated = false;
                var baseVector = new Vector3d();
                if (blockRef.Annotative == AnnotativeStates.True)
                {
                    var objectContextManager = Util.Database().ObjectContextManager;
                    var objectContextCollection = objectContextManager.GetContextCollection("ACDB_ANNOTATIONSCALES");
#if _AutoCAD_
                    ObjectContexts.AddContext(blockRef, objectContextCollection.CurrentContext);
#elif _IJCAD_
                    //TODO: IJ-CAD does not support ObjectContexts
#endif
                }

                blockRef.ScaleFactors = new Scale3d(blockRef.UnitFactor);

                if (ExplodeFromInputActive)
                {
                    if (InsertionExplodeBlock(blockRef) == PromptStatus.Cancel)
                    {
                        return;
                    }
                    var oldSegment = new DBObjectCollection();
                    blockRef.Explode(oldSegment);
                    foreach (DBObject obj in oldSegment)
                    {
                        using (var tr = Util.StartTransaction())
                        {
                            var entity = (Entity)obj;
                            tr.AddNewlyCreatedDBObject(entity, true, tr.CurrentSpace());
                            tr.Commit();
                        }
                    }
                    return;
                }
                if (InsertionPointUi(blockRef, ref isScaled, ref isRotated, ref baseVector) == PromptStatus.Cancel)
                {
                    return;
                }
                if (!isScaled)
                {
                    if (ScaleUi(blockRef, baseVector) == PromptStatus.Cancel)
                    {
                        return;
                    }
                }
                if (!isRotated)
                {
                    if (RotationUi(blockRef, baseVector) == PromptStatus.Cancel)
                    {
                        return;
                    }
                }
                if (ExplodeActive)
                {
                    var oldSegment = new DBObjectCollection();
                    blockRef.Explode(oldSegment);
                    foreach (DBObject obj in oldSegment)
                    {
                        using (var tr = Util.StartTransaction())
                        {
                            var entity = (Entity)obj;
                            tr.AddNewlyCreatedDBObject(entity, true, tr.CurrentSpace());
                            tr.Commit();
                        }
                    }
                }
                else
                {
                    // TODO: ATTEDIT
                    using (var tr = Util.StartTransaction())
                    {
                        var entity = (Entity)blockRef;
                        tr.AddNewlyCreatedDBObject(entity, true, tr.CurrentSpace());
                        tr.Commit();
                    }
                }
                var currentScale = new Scale3d();
                var currentBaseVector = new Vector3d();
                var currentRotate = RotationDefault;
                while (RepeatActive)
                {
                    var blockRefRepeat = new BlockReference(Point3d.Origin, idBlockDef)
                    {
                        ScaleFactors = (currentScale == new Scale3d()) ? blockRef.ScaleFactors : currentScale,
                        Rotation = (currentRotate.IsEqual(RotationDefault)) ? blockRef.Rotation : currentRotate
                    };
                    currentBaseVector = (currentBaseVector == new Vector3d()) ? baseVector : currentBaseVector;
                    if (InsertionPointUi(blockRefRepeat, ref isScaled, ref isRotated, ref currentBaseVector) == PromptStatus.Cancel)
                    {
                        return;
                    }
                    using (var tr = Util.StartTransaction())
                    {
                        tr.AddNewlyCreatedDBObject(blockRefRepeat, true, tr.CurrentSpace());
                        tr.Commit();
                    }
                    if (ExplodeActive)
                    {
                        var oldSegment = new DBObjectCollection();
                        blockRefRepeat.Explode(oldSegment);
                        foreach (DBObject obj in oldSegment)
                        {
                            using (var tr = Util.StartTransaction())
                            {
                                var entity = (Entity)obj;
                                tr.AddNewlyCreatedDBObject(entity, true, tr.CurrentSpace());
                                tr.Commit();
                            }
                        }
                    }
                    else
                    {
                        using (var tr = Util.StartTransaction())
                        {
                            tr.AddNewlyCreatedDBObject(blockRefRepeat, true, tr.CurrentSpace());
                            tr.Commit();
                        }
                    }
                    currentScale = blockRefRepeat.ScaleFactors;
                    currentRotate = blockRefRepeat.Rotation;
                }
                SystemVariable.SetString("INSNAME", blockRef.Name);
            }
        }

        /// <summary>
        /// Handle Base point in Insertion point option
        /// </summary>
        public PromptStatus BasePoint(InsertionPointOpts optInsertionPt)
        {
            var optBasePt = new BasePointOpts(optInsertionPt.BlockReference)
            {
                Options = new JigPromptPointOptions($"\n{MinusInsertRes.UI_BasePoint}")
                {
                    UserInputControls = UserInputControls.Accept3dCoordinates,
                }
            };
            while (true)
            {
                var resBasePt = (PromptPointResult)_userInput.AcquirePoint(optBasePt);
                if (resBasePt.Status != PromptStatus.OK)
                {
                    return PromptStatus.Cancel;
                }
                switch (resBasePt.Status)
                {
                    case PromptStatus.OK:
                        {
                            var vector = resBasePt.Value - optInsertionPt.BlockReference.Position;
                            var baseVector = new Vector3d(vector.X / optInsertionPt.BlockReference.ScaleFactors.X, vector.Y / optInsertionPt.BlockReference.ScaleFactors.Y, 0);
                            optInsertionPt.BaseVector = baseVector;
                        }
                        break;
                }

                if (resBasePt.Status == PromptStatus.OK)
                {
                    break;
                }
            }
            return PromptStatus.OK;
        }

        /// <summary>
        /// Change double to E+ form
        /// </summary>
        public string ConvertFromDoubleToExp(string input)
        {
            var intPart = "";
            var expPart = "";
            if (double.Parse(input) >= Math.Pow(10, 10) && double.Parse(input) <= Math.Pow(10, 15) && !input.Contains("E"))
            {
                var exp = Math.Round(double.Parse(input)).ToString().Length - 1;
                intPart = (double.Parse(input) / Math.Pow(10, exp)).ToString();
                expPart = "E+" + exp.ToString();
                var tempIntPart = double.Parse(intPart);
                intPart = Converter.DistanceToString(tempIntPart, DistanceUnitFormat.Decimal, Util.Luprec());
                input = intPart + expPart;
            }
            else
            {
                for (var i = 0; i < input.Length; i++)
                {
                    if (input[i] == 'E')
                    {
                        intPart = input.Substring(0, i);
                        expPart = input.Substring(i);
                        break;
                    }
                }
                var tempIntPart = double.Parse(intPart != "" ? intPart : input);
                input = Converter.DistanceToString(tempIntPart, DistanceUnitFormat.Decimal, Util.Luprec());
                input += expPart;
            }
            return input;
        }

        /// <summary>
        /// Excute -INSERT
        /// </summary>
        public void ExcuteCommand()
        {
            Initialize();

            if (GetBlockName() == PromptStatus.Cancel)
            {
                return;
            }
            var valid = IsValid(BlockName, PathName);
            switch (valid)
            {
                case Valid.Ok:
                    AddBlockOrDwg(BlockName, PathName);
                    break;
                case Valid.BlockNameNotSpecified:
                    WriteMessage($"\n{MinusInsertRes.BN3}");
                    break;
                case Valid.InvalidCharacterBlockName:
                    WriteMessage($"\n{MinusInsertRes.BN3}");
                    break;
                case Valid.FileWasNotFound:
                    WriteMessage(string.Format(MinusInsertRes.BN4, $"{BlockName}.dwg"));
                    break;
            }
        }

        /// <summary>
        /// Get block by path
        /// </summary>
        private ObjectId GetBlockFromDwg(string name, string path)
        {
            using (var insertDatabase = new Database(false, false))
            {
                var isDwg = string.IsNullOrEmpty(path) || Path.GetExtension(path).ToUpper() == ".DWG";

                if (isDwg)
                {
                    insertDatabase.ReadDwgFile(path, FileOpenMode.OpenForReadAndAllShare, false, null);
                }
                else
                {
                    var logPath = Path.GetTempFileName();
                    insertDatabase.DxfIn(path, logPath);
                    File.Delete(logPath);
                }
                return Util.Database().Insert(name, insertDatabase, true);
            }
        }

        /// <summary>
        /// Get ObjectId by BlockName
        /// </summary>
        public ObjectId? GetBlockForName(string blockName)
        {
            using (var tr = Util.StartTransaction())
            {
                var blockTable = tr.GetObject<BlockTable>(Util.Database().BlockTableId, OpenMode.ForRead);
                foreach (var blockDefId in blockTable)
                {
                    var blockDef = (BlockTableRecord)tr.GetObject(blockDefId, OpenMode.ForRead, false);
                    if (blockDef == null ||
                        blockDef.IsFromExternalReference ||
                        blockDef.IsLayout ||
                        blockDef.IsAnonymous ||
                        blockDef.Name.Contains("|"))
                    {
                        continue;
                    }
                    if (blockDef.Name == blockName)
                    {
                        BlockReference = new BlockReference(Point3d.Origin, blockDefId);
                        Unit = BlockReference.BlockUnit.ToString();
                        Factor = BlockReference.UnitFactor + "";
                        Explodable = blockDef.Explodable;
                        if (blockDef.BlockScaling == BlockScaling.Uniform)
                        {
                            UniformScaleActive = true;
                        }
                        return blockDefId;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Handle BlockName
        /// </summary>
        /// <returns></returns>
        public PromptStatus GetBlockName()
        {
            var defVal = GetListBlock();
            PromptStringOptions optBlockName;
            if (defVal != "")
            {
                optBlockName = new PromptStringOptions($"\n{MinusInsertRes.BN1} <{defVal}>")
                {
                    AllowSpaces = true,
                };
            }
            else
            {
                optBlockName = new PromptStringOptions($"\n{MinusInsertRes.BN1}")
                {
                    AllowSpaces = true,
                };
            }
            var resBlockName = Util.Editor().GetString(optBlockName);
            if (resBlockName.Status == PromptStatus.OK)
            {
                var blockNameTrim = resBlockName.StringResult.Trim();
                switch (blockNameTrim)
                {
                    case "?":
                        var optBlockList = new PromptKeywordOptions($"\n{MinusInsertRes.UI_BlockList}")
                        {
                            AllowArbitraryInput = true,
                            AppendKeywordsToMessage = false,
                            Keywords =
                                {
                                    {"*", "*"},
                                }
                        };
                        optBlockList.Keywords.Default = "*";
                        var resBlockList = Util.Editor().GetKeywords(optBlockList);
                        if (resBlockList.Status == PromptStatus.OK)
                        {
                            var blockListTrim = resBlockList.StringResult.Trim();
                            switch (blockListTrim)
                            {
                                case "*":
                                    ShowCMList(ListBlock);
                                    return PromptStatus.Cancel;
                                default:
                                    var hasStart = false;
                                    for (var i = 0; i < blockListTrim.Length; i++)
                                    {
                                        if (blockListTrim[i] == '*')
                                        {
                                            hasStart = true;
                                            blockListTrim = blockListTrim.Substring(0, i);
                                            break;
                                        }
                                    }
                                    var listBlock1 = new List<string>();
                                    if (hasStart)
                                    {
                                        if (ListBlock.Contains(blockListTrim))
                                        {
                                            listBlock1.Add(blockListTrim);
                                        }
                                        else
                                        {
                                            foreach (var block in ListBlock)
                                            {
                                                if (block.Length > blockListTrim.Length)
                                                {
                                                    if (block.Substring(0, blockListTrim.Length) == blockListTrim)
                                                    {
                                                        listBlock1.Add(block);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ListBlock.Contains(blockListTrim))
                                        {
                                            listBlock1.Add(blockListTrim);
                                        }
                                    }
                                    ShowCMList(listBlock1);
                                    return PromptStatus.Cancel;
                            }
                        }
                        break;
                    case "":
                        BlockName = defVal;
                        break;
                    default:
                        BlockName = blockNameTrim;

                        if (BlockName[0] == '*')
                        {
                            if (BlockName.Contains("="))
                            {
                                ShowCMNameErr();
                                return PromptStatus.Cancel;
                            }
                            BlockName = BlockName.Substring(1, BlockName.Length - 1);
                            var valid1 = IsValid(BlockName, PathName);
                            if (valid1 != Valid.BlockNameInvalid)
                            {
                                GetBlockForName(BlockName);
                                if (!Explodable)
                                {
                                    var optExplodable = new PromptKeywordOptions($"\n{MinusInsertRes.UI_Explodable}")
                                    {
                                        AppendKeywordsToMessage = false,
                                        Keywords =
                                        {
                                            {"Y", $"{MinusInsertRes.Yes_Key}"},
                                            {"N", $"{MinusInsertRes.No_Key}"},
                                        }
                                    };
                                    optExplodable.Keywords.Default = "N";
                                    var resExplodable = Util.Editor().GetKeywords(optExplodable);
                                    if (resExplodable.Status != PromptStatus.OK)
                                    {
                                        return PromptStatus.Cancel;
                                    }
                                    if (resExplodable.StringResult == "Y")
                                    {
                                        return PromptStatus.OK;
                                    }
                                    return PromptStatus.Cancel;
                                }
                                ExplodeFromInputActive = true;
                                return PromptStatus.OK;
                            }
                        }
                        else if (BlockName.Contains("="))
                        {
                            for (var i = 0; i < BlockName.Length; i++)
                            {
                                if (BlockName[i] == '=')
                                {
                                    BlockName = BlockName.Substring(0, i) + BlockName.Substring(i + 1);
                                }
                            }
                        }
                        break;
                }
            }
            var valid = IsValid(BlockName, PathName);
            if (valid == Valid.BlockNameInvalid)
            {
                BlockName += ".dwg";
            }
            return PromptStatus.OK;
        }

        /// <summary>
        /// Get list block in DB
        /// </summary>
        private string GetListBlock()
        {
            var blockName = "";
            ListBlockAnonymous = new List<string>();
            using (var tr = Util.StartTransaction())
            {
                var blockTable = tr.GetObject<BlockTable>(Util.CurDoc().Database.BlockTableId, OpenMode.ForRead);
                foreach (var blockDefId in blockTable)
                {
                    var blockDef = (BlockTableRecord)tr.GetObject(blockDefId, OpenMode.ForRead, false);
                    if (blockDef == null ||
                        blockDef.IsFromExternalReference ||
                        blockDef.IsLayout ||
                        blockDef.IsAnonymous ||
                        blockDef.Name.Contains("|"))
                    {
                        if (blockDef != null && blockDef.IsAnonymous)
                        {
                            ListBlockAnonymous.Add(blockDef.Name);
                        }
                    }
                    else
                    {
                        ListBlock.Add(blockDef.Name);
                    }
                }

                if (ListBlock.Count != 0)
                {
                    blockName = SystemVariable.GetString("INSNAME");
                }
                ListBlock.Sort();
            }
            return blockName;
        }

        /// <summary>
        /// Validate block name, path
        /// </summary>
        private Valid IsValid(string name, string path)
        {
            try
            {
                var strTheseAreInvalidFileNameChars = new string(Path.GetInvalidFileNameChars());
                strTheseAreInvalidFileNameChars += @"<>|:*?/;,`\";
                var containsABadCharacter = new Regex("[" + Regex.Escape(strTheseAreInvalidFileNameChars) + "]");

                // block name is empty or null
                if (string.IsNullOrEmpty(name))
                {
                    return Valid.BlockNameNotSpecified;
                }

                // block name include bad character
                if (containsABadCharacter.IsMatch(name) && Path.GetExtension(name).ToUpper() != ".DWG")
                {
                    return Valid.InvalidCharacterBlockName;
                }

                // path is not exist
                if (Path.GetExtension(name).ToUpper() == ".DWG")
                {
                    if (File.Exists(name) && Path.GetDirectoryName(name) != "")
                    {
                        PathName = name;
                        BlockName = Path.GetFileNameWithoutExtension(name);
                    }
                    else
                    {
                        path = name;
                        name = Path.GetFileName(name);
                        var flag = false;
                        var currentDirectory = Util.Database().OriginalFileName;
                        currentDirectory = Path.GetDirectoryName(currentDirectory);
                        if (currentDirectory != null)
                        {
                            var fileDirectory = Path.Combine(currentDirectory, name);
                            if (File.Exists(fileDirectory))
                            {
                            }
                            else
                            {
                                var supportPaths = SystemVariable.GetString("ACADPREFIX").Split(';');
                                foreach (var supportPath in supportPaths)
                                {
                                    fileDirectory = Path.Combine(supportPath, name);
                                    if (File.Exists(fileDirectory))
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                                if (!flag)
                                {
                                    BlockName = Path.GetFileNameWithoutExtension(name);
                                    PathName = path;
                                    return Valid.FileWasNotFound;
                                }
                            }
                            PathName = fileDirectory;
                        }

                        path = PathName;
                        BlockName = Path.GetFileNameWithoutExtension(name);
                    }
                }
                // block name is not include in database
                if (!ListBlock.Contains(name) && (path == null || Path.GetDirectoryName(path) == "") && Path.GetExtension(name).ToUpper() != ".DWG")
                {
                    return Valid.BlockNameInvalid;
                }

                return Valid.Ok;
            }
            catch
            {
                return Valid.InvalidCharacterBlockName;
            }
        }

        /// <summary>
        /// Handle Insertion Explode Block
        /// </summary>
        private PromptStatus InsertionExplodeBlock(BlockReference blockReference)
        {
            var optPoint = new PromptPointOptions($"\n{MinusInsertRes.UI_InsertionExplode}");
            var resPoint = Util.Editor().GetPoint(optPoint);

            switch (resPoint.Status)
            {
                case PromptStatus.OK:
                    blockReference.Position = resPoint.Value;
                    break;
                default:
                    return PromptStatus.Cancel;
            }

            var optScale = new PromptDoubleOptions($"\n{MinusInsertRes.UI_ScaleExplode}");
            var resScale = Util.Editor().GetDouble(optScale);

            switch (resScale.Status)
            {
                case PromptStatus.OK:
                    blockReference.ScaleFactors = new Scale3d(resScale.Value * blockReference.UnitFactor,
                                                    resScale.Value * blockReference.UnitFactor,
                                                    resScale.Value * blockReference.UnitFactor);
                    break;
                default:
                    return PromptStatus.Cancel;
            }

            var angBase = SystemVariable.GetAngle("ANGBASE");
            var optRotation = new PromptAngleOptions($"\n{MinusInsertRes.UI_RotationExplode}")
            {
                DefaultValue = angBase,
                BasePoint = blockReference.Position,
                UseBasePoint = true,
            };
            var resRotation = Util.Editor().GetAngle(optRotation);

            switch (resRotation.Status)
            {
                case PromptStatus.OK:
                    blockReference.Rotation = resRotation.Value;
                    return PromptStatus.OK;
                default:
                    return PromptStatus.Cancel;
            }
        }

        /// <summary>
        /// Handle Insertion point Option
        /// </summary>
        public PromptStatus InsertionPointUi(BlockReference blockReference, ref bool isScaled, ref bool isRotated, ref Vector3d baseVector)
        {
            InsertionPointOpts optInsertionPt;
            var baseVector1 = new Vector3d(baseVector.X, baseVector.Y, baseVector.Z);
            if (UniformScaleActive)
            {
                optInsertionPt = new InsertionPointOpts(blockReference, baseVector1)
                {
                    Options = new JigPromptPointOptions($"\n{MinusInsertRes.UI_InsertionPointUniformScale}")
                    {
                        AppendKeywordsToMessage = false,
                        UserInputControls = UserInputControls.Accept3dCoordinates,
                        Keywords =
                        {
                            {"B", $"{MinusInsertRes.BasePoint_Key}"},
                            {"S", $"{MinusInsertRes.Scale_Key}"},
                            {"R", $"{MinusInsertRes.Rotate_Key}"},
                            {"E", $"{MinusInsertRes.Explode_Key}"},
                            {"RE", $"{MinusInsertRes.REpeat_Key}"},
                            {"P", $"{MinusInsertRes.P_Key}"},
                            {"PS", $"{MinusInsertRes.PScale_Key}"},
                            {"PR", $"{MinusInsertRes.PRotate_Key}"},
                        }
                    }
                };
            }
            else
            {
                optInsertionPt = new InsertionPointOpts(blockReference, baseVector1)
                {
                    Options = new JigPromptPointOptions($"\n{MinusInsertRes.UI_InsertionPoint}")
                    {
                        AppendKeywordsToMessage = false,
                        UserInputControls = UserInputControls.Accept3dCoordinates,
                        Keywords =
                        {
                            {"B", $"{MinusInsertRes.BasePoint_Key}"},
                            {"S", $"{MinusInsertRes.Scale_Key}"},
                            {"X", $"{MinusInsertRes.X_Key}"},
                            {"Y", $"{MinusInsertRes.Y_Key}"},
                            {"Z", $"{MinusInsertRes.Z_Key}"},
                            {"R", $"{MinusInsertRes.Rotate_Key}"},
                            {"E", $"{MinusInsertRes.Explode_Key}"},
                            {"RE", $"{MinusInsertRes.REpeat_Key}"},
                            {"P", $"{MinusInsertRes.P_Key}"},
                            {"PS", $"{MinusInsertRes.PScale_Key}"},
                            {"PX", $"{MinusInsertRes.PXscale_Key}"},
                            {"PY", $"{MinusInsertRes.PYscale_Key}"},
                            {"PZ", $"{MinusInsertRes.PZscale_Key}"},
                            {"PR", $"{MinusInsertRes.PRotate_Key}"},
                        }
                    }
                };
            }
            while (true)
            {
                var resInsertionPt = (PromptPointResult)_userInput.AcquirePoint(optInsertionPt);

                switch (resInsertionPt.Status)
                {
                    case PromptStatus.OK:
                        baseVector = optInsertionPt.BaseVector;
                        return PromptStatus.OK;
                    case PromptStatus.Keyword:
                        switch (resInsertionPt.StringResult)
                        {
                            case "B":
                                {
                                    if (BasePoint(optInsertionPt) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                            case "S":
                                {
                                    if (ScaleS(optInsertionPt.BlockReference, false) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                    isScaled = true;
                                }
                                break;
                            case "X":
                                {
                                    if (Scale(optInsertionPt.BlockReference, ScaleType.X, false) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                    isScaled = true;
                                }
                                break;
                            case "Y":
                                {
                                    if (Scale(optInsertionPt.BlockReference, ScaleType.Y, false) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                    isScaled = true;
                                }
                                break;
                            case "Z":
                                {
                                    if (Scale(optInsertionPt.BlockReference, ScaleType.Z, false) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                    isScaled = true;
                                }
                                break;
                            case "R":
                                {
                                    if (Rotation(optInsertionPt, false) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                    isRotated = true;
                                }
                                break;
                            case "E":
                                {
                                    var optExplode = new PromptKeywordOptions($"\n{MinusInsertRes.UI_Explodable}")
                                    {
                                        AppendKeywordsToMessage = false,
                                        Keywords =
                                        {
                                            {"Y", $"{MinusInsertRes.Yes_Key}"},
                                            {"N", $"{MinusInsertRes.No_Key}"},
                                        }
                                    };
                                    optExplode.Keywords.Default = "Y";
                                    var resExplode = Util.Editor().GetKeywords(optExplode);
                                    if (resExplode.Status != PromptStatus.OK)
                                    {
                                        return PromptStatus.Cancel;
                                    }
                                    ExplodeActive = resExplode.StringResult == "Y";
                                }
                                break;
                            case "RE":
                                {
                                    var optRepeat = new PromptKeywordOptions($"{MinusInsertRes.UI_Repeat}")
                                    {
                                        AppendKeywordsToMessage = false,
                                        Keywords =
                                        {
                                            {"Y", $"{MinusInsertRes.Yes_Key}"},
                                            {"N", $"{MinusInsertRes.No_Key}"},
                                        }
                                    };
                                    optRepeat.Keywords.Default = "Y";
                                    var resRepeat = Util.Editor().GetKeywords(optRepeat);
                                    if (resRepeat.Status == PromptStatus.Cancel)
                                    {
                                        return PromptStatus.Cancel;
                                    }
                                    if (resRepeat.Status == PromptStatus.OK)
                                    {
                                        RepeatActive = resRepeat.StringResult == "Y";
                                    }
                                }
                                break;
                            case "P":
                                {
                                    if (PreviewScale(optInsertionPt) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                            case "PS":
                                {
                                    if (ScaleS(optInsertionPt.BlockReference, true) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                            case "PX":
                                {
                                    if (Scale(optInsertionPt.BlockReference, ScaleType.X, true) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                            case "PY":
                                {
                                    if (Scale(optInsertionPt.BlockReference, ScaleType.Y, true) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                            case "PZ":
                                {
                                    if (Scale(optInsertionPt.BlockReference, ScaleType.Z, true) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                            case "PR":
                                {
                                    if (Rotation(optInsertionPt, true) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                        }
                        break;
                    default:
                        return PromptStatus.Cancel;
                }
            }
        }
        /// <summary>
        /// Preview Scale when insert point
        /// </summary>
        private PromptStatus PreviewScale(InsertionPointOpts optInsertionPt)
        {
            PromptKeywordOptions optPreview;
            if (UniformScaleActive)
            {
                optPreview = new PromptKeywordOptions($"{MinusInsertRes.UI_PreviewUniform}")
                {
                    AppendKeywordsToMessage = false,
                    Keywords =
                                            {
                                                {"PS", $"{MinusInsertRes.PScale_Key}"},
                                                {"PR", $"{MinusInsertRes.PRotate_Key}"},
                                            }
                };
            }
            else
            {
                optPreview = new PromptKeywordOptions($"{MinusInsertRes.UI_Preview}")
                {
                    AppendKeywordsToMessage = false,
                    Keywords =
                                            {
                                                {"PS", $"{MinusInsertRes.PScale_Key}"},
                                                {"PX", $"{MinusInsertRes.PXscale_Key}"},
                                                {"PY", $"{MinusInsertRes.PYscale_Key}"},
                                                {"PZ", $"{MinusInsertRes.PZscale_Key}"},
                                                {"PR", $"{MinusInsertRes.PRotate_Key}"},
                                            }
                };
            }
            var resRepeat = Util.Editor().GetKeywords(optPreview);

            switch (resRepeat.Status)
            {
                case PromptStatus.OK:
                    switch (resRepeat.StringResult)
                    {
                        case "PS":
                            {
                                if (ScaleS(optInsertionPt.BlockReference, true) == PromptStatus.Cancel)
                                    return PromptStatus.Cancel;
                            }
                            break;
                        case "PX":
                            {
                                if (Scale(optInsertionPt.BlockReference, ScaleType.X, true) == PromptStatus.Cancel)
                                    return PromptStatus.Cancel;
                            }
                            break;
                        case "PY":
                            {
                                if (Scale(optInsertionPt.BlockReference, ScaleType.Y, true) == PromptStatus.Cancel)
                                    return PromptStatus.Cancel;
                            }
                            break;
                        case "PZ":
                            {
                                if (Scale(optInsertionPt.BlockReference, ScaleType.Z, true) == PromptStatus.Cancel)
                                    return PromptStatus.Cancel;
                            }
                            break;
                        case "PR":
                            {
                                if (Rotation(optInsertionPt, true) == PromptStatus.Cancel)
                                    return PromptStatus.Cancel;
                            }
                            break;
                    }
                    return PromptStatus.OK;
                default:
                    return PromptStatus.Cancel;
            }
        }

        /// <summary>
        /// Handle Rotation in Insertion point option
        /// </summary>
        public PromptStatus Rotation(InsertionPointOpts optInsertionPt, bool isPreview)
        {
            var angBase = SystemVariable.GetAngle("ANGBASE");
            var message = isPreview ? MinusInsertRes.UI_PreviewRotation : MinusInsertRes.UI_Rotation;
            var optAngle = new PromptAngleOptions($"\n{message}")
            {
                DefaultValue = angBase
            };

            var resAngle = Util.Editor().GetAngle(optAngle);

            switch (resAngle.Status)
            {
                case PromptStatus.OK:
                    {
                        optInsertionPt.BlockReference.Rotation = resAngle.Value;
                        optInsertionPt.ResetVector();
                    }
                    return PromptStatus.OK;
                default:
                    return PromptStatus.Cancel;
            }
        }

        /// <summary>
        /// Handle Rotation Option
        /// </summary>
        public PromptStatus RotationUi(BlockReference blockReference, Vector3d baseVector)
        {
            var angBase = SystemVariable.GetAngle("ANGBASE");
            var scale = new Vector3d(baseVector.X * blockReference.ScaleFactors.X,
                                    baseVector.Y * blockReference.ScaleFactors.Y,
                                    baseVector.Z * blockReference.ScaleFactors.Z);
            var baseVector1 = scale;
            var basePoint = blockReference.Position.Add(baseVector1);

            var optRotation = new RotationOpts(blockReference, baseVector1, basePoint)
            {
                Options = new JigPromptAngleOptions($"\n{MinusInsertRes.UI_Rotation}")
                {
                    BasePoint = basePoint,
                    UseBasePoint = true,
                    DefaultValue = angBase,
                    Cursor = CursorType.RubberBand,
                    UserInputControls = UserInputControls.NullResponseAccepted,
                }
            };

            while (true)
            {
                BitmapImageHelper.AddCursorBadge(MinusInsertRes.IconRotationPath);
                var resRotation = (PromptDoubleResult)_userInput.AcquireAngle(optRotation);
                BitmapImageHelper.RemoveCursorBadge();

                switch (resRotation.Status)
                {
                    case PromptStatus.OK:
                        return PromptStatus.OK;
                    case PromptStatus.Keyword:
                        break;
                    default:
                        return PromptStatus.Cancel;
                }
            }
        }

        /// <summary>
        /// XorYorZ 方向の尺度を指定 ＜1＞
        /// </summary>
        /// <param name="direct">X or Y or Z</param>
        public PromptStatus Scale(BlockReference blockReference, ScaleType direct, bool isPreview)
        {
            var scaleFactors = blockReference.ScaleFactors;
            var message = string.Empty;
            if (isPreview)
            {
                switch (direct)
                {
                    case ScaleType.X:
                        message = $"\n{MinusInsertRes.UI_PreviewScaleX}";
                        break;
                    case ScaleType.Y:
                        message = $"\n{MinusInsertRes.UI_PreviewScaleY}";
                        break;
                    case ScaleType.Z:
                        message = $"\n{MinusInsertRes.UI_PreviewScaleZ}";
                        break;
                }
            }
            else
            {
                switch (direct)
                {
                    case ScaleType.X:
                        message = $"\n{MinusInsertRes.UI_ScaleX}";
                        break;
                    case ScaleType.Y:
                        message = $"\n{MinusInsertRes.UI_ScaleY}";
                        break;
                    case ScaleType.Z:
                        message = $"\n{MinusInsertRes.UI_ScaleZ}";
                        break;
                }
            }

            var optScale = new PromptDistanceOptions(message)
            {
                DefaultValue = 1,
                AllowZero = false,
            };

            var resScale = Util.Editor().GetDistance(optScale);
            if (resScale.Status != PromptStatus.OK)
            {
                return PromptStatus.Cancel;
            }
            switch (direct)
            {
                case ScaleType.X:
                    blockReference.ScaleFactors = new Scale3d(resScale.Value * blockReference.UnitFactor, scaleFactors.Y, scaleFactors.Z);
                    break;
                case ScaleType.Y:
                    blockReference.ScaleFactors = new Scale3d(scaleFactors.X, resScale.Value * blockReference.UnitFactor, scaleFactors.Z);
                    break;
                case ScaleType.Z:
                    blockReference.ScaleFactors = new Scale3d(scaleFactors.X, scaleFactors.Y, resScale.Value * blockReference.UnitFactor);
                    break;
            }
            return PromptStatus.OK;
        }

        /// <summary>
        /// XYZ 軸に対する尺度を指定 ＜1＞
        /// </summary>
        public PromptStatus ScaleS(BlockReference blockReference, bool isPreview)
        {
            var message = isPreview ? MinusInsertRes.UI_PreviewScaleS : MinusInsertRes.UI_ScaleXYZAxes;
            var optScaleS = new PromptDistanceOptions($"\n{message}")
            {
                DefaultValue = 1.0,
                AllowZero = false,
            };

            var resScaleS = Util.Editor().GetDistance(optScaleS);
            if (resScaleS.Status != PromptStatus.OK)
            {
                return PromptStatus.Cancel;
            }

            blockReference.ScaleFactors = new Scale3d(resScaleS.Value * blockReference.UnitFactor,
                resScaleS.Value * blockReference.UnitFactor, resScaleS.Value * blockReference.UnitFactor);
            return PromptStatus.OK;
        }

        /// <summary>
        /// Handle Scale Option
        /// </summary>
        public PromptStatus ScaleUi(BlockReference blockReference, Vector3d? baseVector)
        {
            if (baseVector != null)
            {
                var baseVectorDefault = new Vector3d(baseVector.Value.X, baseVector.Value.Y, baseVector.Value.Z);
                var basePointDefault = blockReference.Position.Add(baseVectorDefault);
                if (UniformScaleActive)
                {
                    var baseVector1 = new Vector3d(baseVector.Value.X, baseVector.Value.Y, baseVector.Value.Z);
                    var basePoint = blockReference.Position.Add(baseVector1);
                    var optScaleUniform = new ScaleOpts(blockReference, baseVector1, basePoint)
                    {
                        Options = new JigPromptDistanceOptions($"\n{MinusInsertRes.UI_ScaleXYZAxes}")
                        {
                            BasePoint = basePoint,
                            UseBasePoint = true,
                            Cursor = CursorType.RubberBand,
                            DefaultValue = 1,
                            UserInputControls = UserInputControls.NullResponseAccepted |
                                                UserInputControls.UseBasePointElevation |
                                                UserInputControls.NoZeroResponseAccepted
                        }
                    };

                    Util.Editor().PromptedForPoint += optScaleUniform.UpdateScale;
                    BitmapImageHelper.AddCursorBadge(MinusInsertRes.IconCornerPath);
                    var resScaleUniform = _userInput.AcquireDistance(optScaleUniform);
                    BitmapImageHelper.RemoveCursorBadge();

                    Util.Editor().PromptedForPoint -= optScaleUniform.UpdateScale;

                    if (optScaleUniform.UserStatus == PromptStatus.Cancel)
                    {
                        return PromptStatus.Cancel;
                    }

                    switch (resScaleUniform.Status)
                    {
                        case PromptStatus.OK:
                        case PromptStatus.Keyword:
                            break;
                        default:
                            return PromptStatus.Cancel;
                    }
                }
                else
                {
                    var baseVector1 = new Vector3d(baseVector.Value.X, baseVector.Value.Y, baseVector.Value.Z);
                    var basePoint = blockReference.Position.Add(baseVector1);

                    var optScaleCorner = new ScaleCornerOpts(blockReference, baseVector1, basePoint)
                    {
                        Options = new JigPromptStringOptions($"\n{MinusInsertRes.UI_ScaleCornerX}")
                        {
                            DefaultValue = "1",
                            AppendKeywordsToMessage = false,
                            UserInputControls = UserInputControls.Accept3dCoordinates |
                                                UserInputControls.NullResponseAccepted |
                                                UserInputControls.NoZeroResponseAccepted,
                            Keywords =
                            {
                                {"XYZ", $"{MinusInsertRes.XYZ_Key}"},
                            }
                        }
                    };
                    if (TypographicScale(optScaleCorner, blockReference, baseVector, basePointDefault) == PromptStatus.Cancel)
                    {
                        return PromptStatus.Cancel;
                    }
                }
            }

            return PromptStatus.OK;
        }

        /// <summary>
        /// Show write message name error
        /// </summary>
        public void ShowCMNameErr()
        {
            WriteMessage($"{MinusInsertRes.BN2}");
        }

        /// <summary>
        /// Show write message info list block
        /// </summary>
        public void ShowCMList(List<string> listBlock)
        {
#if _AutoCAD_
            Application.DisplayTextScreen = true;
#elif _IJCAD_
            //TODO: IJCAD doesn't support 
#endif
            WriteMessage($"\n{string.Format(MinusInsertRes.CM_List_1)}");
            foreach (var block in listBlock)
            {
                WriteMessage($"\n\t\"{block}\"");
            }
            WriteMessage($"\n{string.Format(MinusInsertRes.CM_List_2)}");
            WriteMessage(ListBlock.Count == listBlock.Count
                ? $"\n\t\t{listBlock.Count}\t\t\t{ListBlockAnonymous.Count}"
                : $"\n\t\t{listBlock.Count}\t\t\t{0}");
        }

        /// <summary>
        /// Show write message info Unit
        /// </summary>
        public void ShowCMUnit(BlockReference blockRef)
        {
            var insUnits = SystemVariable.GetInt("INSUNITS");
            var insUnitsDefSource = SystemVariable.GetInt("INSUNITSDEFSOURCE");
            var insUnitsDefTarget = SystemVariable.GetInt("INSUNITSDEFTARGET");

            WriteMessage(blockRef.BlockUnit > 0
                ? $"\n{string.Format(MinusInsertRes.CM_Unit_1, blockRef.BlockUnit)}"
                : $"\n{string.Format(MinusInsertRes.CM_Unit_1, (UnitsValue)insUnitsDefSource)}");

            WriteMessage(insUnits > 0
                ? $"\n{string.Format(MinusInsertRes.CM_Unit_2, (UnitsValue)insUnits)}"
                : $"\n{string.Format(MinusInsertRes.CM_Unit_2, (UnitsValue)insUnitsDefTarget)}");

            if ((blockRef.BlockUnit > 0 && insUnits > 0) ||
                (blockRef.BlockUnit == 0 && insUnitsDefSource == 0) ||
                (insUnits == 0 && insUnitsDefTarget == 0))
            {
                WriteMessage($"\n{string.Format(MinusInsertRes.CM_Unit_3, "1")}");
            }
            else if (blockRef.BlockUnit > 0 &&
                     insUnits == 0 &&
                     insUnitsDefTarget > 0)
            {
                var scaleFactor = UnitsConverter.GetConversionFactor(blockRef.BlockUnit, (UnitsValue)insUnitsDefTarget).ToString();
                scaleFactor = ConvertFromDoubleToExp(scaleFactor);
                WriteMessage($"\n{string.Format(MinusInsertRes.CM_Unit_3, scaleFactor)}");
            }
            else if (blockRef.BlockUnit == 0 &&
                     insUnitsDefSource > 0 &&
                     insUnits == 0 &&
                     insUnitsDefTarget > 0)
            {
                var scaleFactor = UnitsConverter.GetConversionFactor((UnitsValue)insUnitsDefSource, (UnitsValue)insUnitsDefTarget).ToString();
                scaleFactor = ConvertFromDoubleToExp(scaleFactor);
                WriteMessage($"\n{string.Format(MinusInsertRes.CM_Unit_3, scaleFactor)}");
            }
            else if (blockRef.BlockUnit == 0 &&
                     insUnitsDefSource > 0 &&
                     insUnits > 0)
            {
                var scaleFactor = UnitsConverter.GetConversionFactor((UnitsValue)insUnitsDefSource, (UnitsValue)insUnits).ToString();
                scaleFactor = ConvertFromDoubleToExp(scaleFactor);
                WriteMessage($"\n{string.Format(MinusInsertRes.CM_Unit_3, scaleFactor)}");
            }

            if (blockRef.BlockUnit > 0 && insUnits > 0)
            {
                var factor = UnitsConverter.GetConversionFactor(blockRef.BlockUnit, (UnitsValue)insUnits).ToString();
                factor = ConvertFromDoubleToExp(factor);
                WriteMessage($"\n{string.Format(MinusInsertRes.CM_Unit_4, factor)}");
            }
            else
            {
                WriteMessage($"\n{string.Format(MinusInsertRes.CM_Unit_4, "1")}");
            }
        }

        /// <summary>
        /// Typographic scale processing
        /// </summary>
        private PromptStatus TypographicScale(ScaleCornerOpts optScaleCorner, BlockReference blockReference, Vector3d? baseVector, Point3d basePointDefault)
        {
            while (true)
            {
                Util.Editor().PromptedForPoint += optScaleCorner.UpdateScale;
                BitmapImageHelper.AddCursorBadge(MinusInsertRes.IconCornerPath);
                var resScale = _userInput.AcquireString(optScaleCorner);
                BitmapImageHelper.RemoveCursorBadge();
                Util.Editor().PromptedForPoint -= optScaleCorner.UpdateScale;

                if (optScaleCorner.UserStatus == PromptStatus.Cancel)
                {
                    return PromptStatus.Cancel;
                }

                if (resScale.StringResult == "0")
                {
                    WriteMessage(string.Format(MinusInsertRes.NonzeroMessage) + "\n");
                    continue;
                }
                if (resScale.StringResult.ToUpper() != "X" && resScale.StringResult.ToUpper() != "XYZ")
                {
                    if (resScale.StringResult != "\n" && !resScale.StringResult.Contains(","))
                    {
                        var optScaleY = new PromptDoubleOptions($"\n{MinusInsertRes.UI_ScaleY_DefaultStringValue}")
                        {
                            AllowNone = true,
                            AllowNegative = true,
                            AllowZero = false,
                        };
                        var resScaleY = Util.Editor().GetDouble(optScaleY);

                        switch (resScaleY.Status)
                        {
                            case PromptStatus.OK:
                            case PromptStatus.None:
                                var scaleX = (resScale.StringResult == "") ? 1 : double.Parse(resScale.StringResult);
                                var scaleY = (resScaleY.Value == 0) ? scaleX : resScaleY.Value;
                                var scaleZ = scaleX;
                                optScaleCorner.SetScale(scaleX, scaleY, scaleZ);
                                var baseVectorScale = new Vector3d(baseVector.Value.X * scaleX,
                                                                baseVector.Value.Y * scaleY,
                                                                baseVector.Value.Z * scaleZ);
                                var vector = -baseVectorScale.RotateBy(-blockReference.Rotation, Vector3d.ZAxis);
                                blockReference.Position = basePointDefault.Add(vector);
                                break;
                            default:
                                return PromptStatus.Cancel;
                        }
                    }
                    break;
                }
                if (resScale.StringResult.ToUpper() == "X" || resScale.StringResult.ToUpper() == "XYZ")
                {
                    var optScaleX = new PromptDoubleOptions($"\n{MinusInsertRes.UI_ScaleX}")
                    {
                        DefaultValue = 1,
                        AllowNegative = true,
                        AllowZero = false,
                        AllowNone = false,
                    };
                    var resScaleX = Util.Editor().GetDouble(optScaleX);
                    if (resScaleX.Status == PromptStatus.OK)
                    {
                        var optScaleY = new PromptDoubleOptions($"\n{MinusInsertRes.UI_ScaleY_DefaultStringValue}")
                        {
                            AllowNone = true,
                            AllowNegative = true,
                            AllowZero = false,
                        };
                        var resScaleY = Util.Editor().GetDouble(optScaleY);

                        switch (resScaleY.Status)
                        {
                            case PromptStatus.OK:
                            case PromptStatus.None:
                                var optScaleZ = new PromptDoubleOptions(string.Format(MinusInsertRes.UI_ScaleZ_DefaultStringValue))
                                {
                                    AllowNone = true,
                                    AllowNegative = true,
                                    AllowZero = false,
                                };
                                var resScaleZ = Util.Editor().GetDouble(optScaleZ);
                                if (resScaleZ.Status == PromptStatus.OK || resScaleZ.Status == PromptStatus.None)
                                {
                                    var scaleX = (resScaleX.Value == 0) ? 1 : resScaleX.Value;
                                    var scaleY = (resScaleY.Value == 0) ? scaleX : resScaleY.Value;
                                    var scaleZ = (resScaleZ.Value == 0) ? scaleX : resScaleZ.Value;
                                    optScaleCorner.SetScale(scaleX, scaleY, scaleZ);
                                    var baseVectorScale = new Vector3d(baseVector.Value.X * scaleX,
                                                                        baseVector.Value.Y * scaleY,
                                                                        baseVector.Value.Z * scaleZ);
                                    var vector = -baseVectorScale.RotateBy(-blockReference.Rotation, Vector3d.ZAxis);
                                    blockReference.Position = basePointDefault.Add(vector);
                                }
                                break;
                            default:
                                return PromptStatus.Cancel;
                        }
                    }
                    else
                    {
                        return PromptStatus.Cancel;
                    }
                    break;
                }
            }
            return PromptStatus.OK;
        }
    }
}