﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;

#endif
using JrxCad.Utility;
using System;

namespace JrxCad.Command.MinusInsert
{
    public partial class MinusInsertCmd
    {
        public static IMinusInsertUserInput _userInput;

        /// <summary>
        /// IMinusInsertUserInput Interface
        /// </summary>
        public interface IMinusInsertUserInput : IUserInputBase
        {
        }

        /// <summary>
        /// UserInput 
        /// </summary>
        private class UserInput : UserInputBase, IMinusInsertUserInput
        {
        }

        public class InsertionPointOpts : PointDrawJigN.JigActions
        {
            public BlockReference BlockReference;
            public Vector3d BaseVector;
            private readonly double _prevAngle;

            public InsertionPointOpts(BlockReference blockReference, Vector3d baseVector)
            {
                BlockReference = blockReference;
                BaseVector = baseVector;
                _prevAngle = blockReference.Rotation;
            }

            public override JigPromptPointOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                geometry.Draw(BlockReference);
                return true;
            }

            public override void OnUpdate()
            {
                var scale = new Vector3d(-BaseVector.X * BlockReference.ScaleFactors.X, -BaseVector.Y * BlockReference.ScaleFactors.Y, 0.0);
                BlockReference.Position = LastValue.Add(scale);
            }

            public void ResetVector()
            {
                if (MathEx.IsEqual(_prevAngle, BlockReference.Rotation)) return;
                var angle = BlockReference.Rotation - _prevAngle;
                var prevVector = BaseVector;
                BaseVector = prevVector.RotateBy(angle, Vector3d.ZAxis);
            }
        }
        public class RotationOpts : AngleDrawJigN.JigActions
        {
            private readonly BlockReference _blockReference;
            private Vector3d _baseVector;
            private Point3d _basePoint;

            public RotationOpts(BlockReference blockReference, Vector3d baseVector, Point3d basePoint)
            {
                _blockReference = blockReference;
                _baseVector = baseVector;
                _basePoint = basePoint;
            }

            public override JigPromptAngleOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                geometry.Draw(_blockReference);
                return true;
            }

            public override void OnUpdate()
            {
                _blockReference.Rotation = LastValue;
                var vector = -_baseVector.RotateBy(_blockReference.Rotation, Vector3d.ZAxis);
                _blockReference.Position = _basePoint.Add(vector);
            }
        }

        public class ScaleOpts : DistanceDrawJigN.JigActions
        {
            private readonly BlockReference _blockReference;
            private Vector3d _baseVector;
            private Point3d _basePoint;
            public PromptStatus UserStatus;

            public ScaleOpts(BlockReference blockReference, Vector3d baseVector, Point3d basePoint)
            {
                _blockReference = blockReference;
                _baseVector = baseVector;
                _basePoint = basePoint;
            }

            public override JigPromptDistanceOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                geometry.Draw(_blockReference);
                return true;
            }

            public override void OnUpdate()
            {
            }

            public void UpdateScale(object sender, PromptPointResultEventArgs e)
            {
                var pt = e.Result.Value;
                UserStatus = e.Result.Status;

                if (pt.X < _basePoint.X && pt.Y < _basePoint.Y)
                {
                    _blockReference.Rotation = 3.14159265359;
                    _blockReference.TransformBy(Matrix3d.Mirroring(_basePoint));
                }
                else
                {
                    _blockReference.Rotation = 0;
                }

                _blockReference.ScaleFactors = new Scale3d(LastValue);

                var baseVector1 = new Vector3d(_baseVector.X * LastValue,
                    _baseVector.Y * LastValue,
                    _baseVector.Z * LastValue);

                var vector = -baseVector1.RotateBy(-_blockReference.Rotation, Vector3d.ZAxis);
                _blockReference.Position = _basePoint.Add(vector);

                if (pt.X < _basePoint.X && pt.Y < _basePoint.Y)
                {
                    _blockReference.Rotation = 3.14159265359;
                    _blockReference.TransformBy(Matrix3d.Mirroring(_basePoint));
                }
            }
        }
        public class ScaleCornerOpts : StringDrawJigN.JigActions
        {
            private readonly BlockReference _blockReference;
            private Vector3d _baseVector;
            public Vector3d Scale;
            private Point3d _basePoint;
            public PromptStatus UserStatus;

            public ScaleCornerOpts(BlockReference blockReference, Vector3d baseVector, Point3d basePoint)
            {
                _blockReference = blockReference;
                _baseVector = baseVector.RotateBy(-blockReference.Rotation, Vector3d.ZAxis);
                _basePoint = basePoint;
            }

            public override JigPromptStringOptions Options { get; set; }

            public override void UpdateString(string str)
            {
            }

            public override bool Draw(WorldGeometry geometry)
            {
                geometry.Draw(_blockReference);
                return true;
            }

            public override void OnUpdate()
            {
            }

            public void UpdateScale(object sender, PromptPointResultEventArgs e)
            {
                var pt = e.Result.Value;
                UserStatus = e.Result.Status;

                Scale = pt - _basePoint;
                var x = (Scale.X == 0) ? _blockReference.UnitFactor : Scale.X;
                var y = (Scale.Y == 0) ? _blockReference.UnitFactor : Scale.Y;
                _blockReference.ScaleFactors = new Scale3d(x, y, Math.Abs(x));

                var baseVectorScale = new Vector3d(_baseVector.X * x,
                                                 _baseVector.Y * y,
                                                 _baseVector.Z * x);
                var vector = -baseVectorScale.RotateBy(-_blockReference.Rotation, Vector3d.ZAxis);
                _blockReference.Position = _basePoint.Add(vector);
            }

            public void SetScale(double v1, double v2, double v3)
            {
                var scale = new Scale3d(v1 * _blockReference.UnitFactor,
                                        v2 * _blockReference.UnitFactor,
                                        v3 * _blockReference.UnitFactor);
                _blockReference.ScaleFactors = scale;
            }
        }

        public class BasePointOpts : PointDrawJigN.JigActions
        {
            private readonly BlockReference _blockReference;

            public BasePointOpts(BlockReference blockReference)
            {
                _blockReference = blockReference;
            }

            public override JigPromptPointOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                if (geometry != null)
                {
                    using (var drawEnt = (BlockReference)_blockReference.Clone())
                    {
                        geometry.Draw(drawEnt);
                    }
                }

                return true;
            }

            public override void OnUpdate()
            {
            }
        }
    }
}
