﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.ApplicationServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.ApplicationServices;
#endif

using System.Collections.Generic;
using JrxCad.Utility;
using System;
using Autodesk.AutoCAD.DatabaseServices;

//TODO: [ARC] please re-check source below with re-sharper
namespace JrxCad.Command.ArcCmd
{
    public abstract class EnterOptCommand : BaseCommand
    {
        public class CmdControlPoints
        {
            /// <summary>
            /// _lastCtrlPoint is the last point that user set for Line, Arc or Polyline
            /// </summary>
            public Point3d _lastCtrlPoint { get; set; }

            /// <summary>
            /// For Line, _startCtrlPoint is the first point that user set.
            /// <para>For Arc, _startCtrlPoint is the center of the arc.</para> 
            /// <para>For Polyline, after exploding it, if the last segment is a line,
            /// <c>_startCtrlPoint</c> is the point other than <c>_lastCtrlPoint</c>.</para>
            /// <para>Otherwise, if the last segment is an arc, <c>_startCtrlPoint</c> is the center of that arc.</para>
            /// </summary>
            public Point3d _startCtrlPoint { get; set; }
            public bool _arcStartToEnd;
            public bool _isLine = false;
        }

        private static Dictionary<string, List<CmdControlPoints>> _ctrlPointHistory;

        public EnterOptCommand()
        {
            if (_ctrlPointHistory == null)
            {
                _ctrlPointHistory = new Dictionary<string, List<CmdControlPoints>>();
            }
            if (!_ctrlPointHistory.ContainsKey(Util.CurDocId()))
            {
                Util.CurDoc().CommandEnded += new CommandEventHandler(docCommandEnded);
                Util.CurDoc().BeginDocumentClose += new DocumentBeginCloseEventHandler(docBeginClose);
            }
        }

        static void docCommandEnded(object sender, CommandEventArgs e)
        {
            if (e.GlobalCommandName != "UNDO")
            {
                return;
            }
        }
        static void docBeginClose(object sender, EventArgs e)
        {
            Util.CurDoc().CommandEnded -= new CommandEventHandler(docCommandEnded);
            Util.CurDoc().CloseWillStart -= new EventHandler(docBeginClose);
            _ctrlPointHistory.Remove(Util.CurDocId());
        }

        public void AddControlPoints(CmdControlPoints prop)
        {
            if (_ctrlPointHistory == null)
            {
                throw new Exception("_ctrlPointHistory must not be null");
            }

            var docId = Util.CurDocId();
            if (_ctrlPointHistory.ContainsKey(docId))
            {
                List<CmdControlPoints> currentList;
                _ctrlPointHistory.TryGetValue(docId, out currentList);
                if (currentList != null)
                {
                    currentList.Add(prop);
                }
                else
                {
                    throw new Exception("The key exists but the value is null");
                }
            }
            else
            {
                List<CmdControlPoints> temp = new List<CmdControlPoints> { prop };
                _ctrlPointHistory.Add(docId, temp);
            }
        }

        public void SetAddControlPoint(Point3d startCtrlPoint, Point3d lastCtrlPoint)
        {
            var temp = new CmdControlPoints();

            temp._startCtrlPoint = startCtrlPoint;
            temp._lastCtrlPoint = lastCtrlPoint;
            temp._arcStartToEnd = true;
            AddControlPoints(temp);
        }
        public void RemoveLastControlPoints()
        {
            if (_ctrlPointHistory == null)
            {
                return;
            }

            var docId = Util.CurDocId();
            if (!_ctrlPointHistory.ContainsKey(docId))
            {
                return;
            }

            List<CmdControlPoints> currentList;
            _ctrlPointHistory.TryGetValue(docId, out currentList);
            if (currentList != null && currentList.Count > 0)
            {
                currentList.RemoveAt(currentList.Count - 1);
            }
        }

        public CmdControlPoints GetLastControlPoints()
        {
            if (_ctrlPointHistory == null)
            {
                return null;
            }

            var docId = Util.CurDocId();

            if (!_ctrlPointHistory.ContainsKey(docId))
            {
                return null;
            }
            List<CmdControlPoints> currentList;
            _ctrlPointHistory.TryGetValue(docId, out currentList);
            if (currentList != null && currentList.Count > 0)
            {
                return currentList[currentList.Count - 1];
            }
            return null;
        }
    }
}
