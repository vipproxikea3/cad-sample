﻿#if _IJCAD_
using Exception = System.Exception;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Exception = System.Exception;

using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Internal;
using Autodesk.AutoCAD.ApplicationServices;


#endif
using JrxCad.Utility;
using JrxCad.Helpers;
using System.Windows.Media.Media3D;
using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;

//TODO: [ARC] please re-check source below with re-sharper
namespace JrxCad.Command.ArcCmd
{
    public partial class ArcCmd : EnterOptCommand
    {
        public enum ArcOpt
        {
            //_isCenterFirst = true
            CenterStartAngle = 0,
            CenterStartEnd = 1,
            CenterStartLength = 2,

            //_isCenterFirst = false && _isCenterSecond = true
            StartCenterEnd = 3,
            StartCenterLength = 4,
            StartCenterAngle = 5,

            //_isCenterFirst = false && _isCenterSecond = false && _isEndSecond = true
            StartEndCenter = 6,
            StartEndAngle = 7,
            StartEndDirection = 8,
            StartEndRadius = 9,

            //_isCenterFirst = false && _isCenterSecond = false && _isEndSecond = false
            StartMidEnd = 10,//Three point
            ContinueLine = 11,
            ContinueArcStartToEnd = 12,
            ContinueArcEndToStart = 13,
        }

        public ArcCmd() : this(new UserInput())
        {
        }

        public ArcCmd(IArcUserInput userInput)
        {
            _userInput = userInput;
        }

        /// <summary>
        /// _isCenterFirst, true if firstpoint is a center, otherwise false, default false
        /// </summary>
        private bool _isCenterFirst;

        /// <summary>
        /// _isCenterSecond, true if secondpoint is a center, otherwise false, default false
        /// </summary>
        private bool _isCenterSecond;

        private bool _isContinue;

        private ArcOpt _arcOptContinue;

        /// <summary>
        /// _isEndSecond, true if second point is a end point, otherwise false, default false
        /// </summary>
        private bool _isEndSecond;
#if DEBUG
        [CommandMethod("JrxCommand", "Ar1", CommandFlags.Modal | CommandFlags.NoNewStack)]
#else
        [CommandMethod("JrxCommand", "SmxARC", CommandFlags.Modal | CommandFlags.NoNewStack)]
#endif

        public override void OnCommand()
        {
            try
            {
                if (!Init()) return;

                _isCenterFirst = false;
                _isCenterSecond = false;
                _isEndSecond = false;
                _isContinue = false;

                //Get first point, firstPt is UCS
                //Transform firstPt to WCS, basePoint is WCS

                var firstPtU = GetFirstPoint(ref _isCenterFirst);
                if (firstPtU == null) return;
                var basePoint = firstPtU.Value.TransformBy(CoordConverter.UcsToWcs());

                //Get second point using basePoint, secondPt is WCS
                //Transform secondPt to UCS
                //Update Z to secondPt, secondPt is UCS
                var secondPtW = GetSecondPoint(basePoint, ref _isCenterSecond, ref _isEndSecond);
                if (secondPtW == null) return;
                var secondPtU = secondPtW.Value.TransformBy(CoordConverter.WcsToUcs());
                secondPtU = new Point3d(secondPtU.X, secondPtU.Y, firstPtU.Value.Z);

                //Init arc with properties in UCS
                using (var arc = CreateArc(firstPtU.Value, secondPtU))
                {
                    arc.SetDatabaseDefaults();
                    arc.Thickness = SystemVariable.GetDouble("THICKNESS");
                    //arc.LinetypeScale = SystemVariable.GetDouble("CELTSCALE");

                    //Tranform all in WCS, get offset angle after transform
                    double initAngle = arc.StartAngle, offsetAngle = 0;
                    arc.TransformBy(CoordConverter.UcsToWcs());
                    offsetAngle = arc.StartAngle - initAngle;

                    //Get third options of arc, arc in WCS, firstPt, secondPt params in UCS
                    var processFlg = GetThirdOptions(arc, firstPtU.Value, secondPtU, offsetAngle);
                    if (processFlg == ProcessFlag.CancelOrDefault) return;

                    //Add arc into DB
                    AddArcIntoDatabase(arc);

                    //Save data for the future use of Arc-Continue
                    //var temp = new CmdControlPoints();
                    //temp._startCtrlPoint = arc.Center;

                    //var firstInputPnt = _isCenterFirst ? secondPtU : firstPtU.Value;
                    //if (firstInputPnt.DistanceTo(arc.StartPoint) < firstInputPnt.DistanceTo(arc.EndPoint))
                    //{
                    //    temp._lastCtrlPoint = arc.EndPoint;
                    //    temp._arcStartToEnd = true;
                    //}
                    //else
                    //{
                    //    temp._lastCtrlPoint = arc.StartPoint;
                    //    temp._arcStartToEnd = false;

                    //}
                    //AddControlPoints(temp);

                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }



        /// <summary>
        /// Get first point of arc
        /// </summary>
        private Point3d? GetFirstPoint(ref bool isCenter)
        {
            var optFirstPt = new PromptPointOptions($"\n{ArcRes.UI_P1}")
            {
                AllowNone = true,
                Keywords = {
                                { "C", "C", "Center" },
                           },
            };
            while (true)
            {
                var resFirstPt = _isCenterFirst
                ? _userInput.GetFirstCenterPoint(optFirstPt)
                : _userInput.GetFirstStartPoint(optFirstPt);

                switch (resFirstPt.Status)
                {
                    case PromptStatus.OK:
                        return resFirstPt.Value;

                    case PromptStatus.Keyword:
                        switch (resFirstPt.StringResult)
                        {
                            case "C":
                                isCenter = true;
                                var message = $"\n{ArcRes.UI_PA1}";
                                optFirstPt = new PromptPointOptions(message);
                                break;
                            default:
                                break;
                        }

                        break;

                    case PromptStatus.None:
                        {
                            var objId = Util.GetCurrSpaceObjectIds();
                            if (objId.Count >= 1)
                            {
                                var curEnt = GetCurrentEnt(objId[objId.Count - 1]);

                                //haven't check curEnt is empty yet
                                var (startPoint, endPoint, opt) = GetPointFromEntity(curEnt);
                                SetAddControlPoint(startPoint, endPoint);
                                _arcOptContinue = opt;

                                var lastPoints = GetLastControlPoints();

                                _isContinue = true;

                                return lastPoints._startCtrlPoint;
                            }
                            else
                            {
                                Util.Editor().WriteMessage(ArcRes.CM_ContER1);
                                Util.Editor().WriteMessage($"\n{ArcRes.CM_ER1}");
                                return null;
                            }
                        }
                    default:
                        return null;
                }
            }
        }

        /// <summary>
        /// Get second point of arc
        /// </summary>
        private Point3d? GetSecondPoint(Point3d firstPt, ref bool isCenter, ref bool isEnd)
        {
            JigPromptPointOpt optSecondPt;
            if (_isCenterFirst)
            {
                optSecondPt = new JigPromptPointOpt
                {
                    Options = new JigPromptPointOptions($"\n{ArcRes.UI_PA2}")
                    {
                        BasePoint = firstPt,
                        UseBasePoint = true,
                        Cursor = CursorType.RubberBand,
                    }
                };
            }
            else if (_isContinue)
            {
                var lastPoints = GetLastControlPoints();
                return lastPoints._lastCtrlPoint;
            }

            else
            {
                optSecondPt = new JigPromptPointOpt
                {
                    Options = new JigPromptPointOptions($"\n{ArcRes.UI_P2}")
                    {
                        BasePoint = firstPt,
                        UseBasePoint = true,
                        Keywords =
                                {
                                    { "C", "C", "Center" },
                                    { "E", "E", "End" },

                                },
                        Cursor = CursorType.RubberBand,
                    }
                };
            }
            while (true)
            {
                PromptResult resSecondPt;
                if (_isCenterSecond)
                    resSecondPt = _userInput.GetSecondCenterPoint(optSecondPt);
                else if (_isEndSecond)
                    resSecondPt = _userInput.GetSecondEndPointAfterStart(optSecondPt);
                else if (_isCenterFirst)
                    resSecondPt = _userInput.GetSecondStartPoint(optSecondPt);
                else
                    resSecondPt = _userInput.GetSecondMidPoint(optSecondPt);

                switch (resSecondPt.Status)
                {
                    case PromptStatus.OK:
                        var resSecond = (PromptPointResult)resSecondPt;
                        return resSecond.Value;

                    case PromptStatus.Keyword:
                        switch (resSecondPt.StringResult)
                        {
                            case "C":
                                isCenter = true;
                                optSecondPt.Options = new JigPromptPointOptions($"\n{ArcRes.UI_PC1}")
                                {
                                    BasePoint = firstPt,
                                    UseBasePoint = true,
                                    Cursor = CursorType.RubberBand,
                                };
                                break;
                            case "E":
                                isEnd = true;
                                optSecondPt.Options = new JigPromptPointOptions($"\n{ArcRes.UI_PB1}")
                                {
                                    BasePoint = firstPt,
                                    UseBasePoint = true,
                                    Cursor = CursorType.RubberBand,
                                };
                                break;
                            default:
                                break;
                        }

                        break;

                    default:
                        return null;
                }
            }
        }

        /// <summary>
        /// calculate  radius, center,start angle, end angle of the arc if there are
        /// </summary>
        private Arc CreateArc(Point3d firstPt, Point3d secondPt)
        {
            var arc = new Arc();
            arc.Normal = Vector3d.ZAxis;

            if (_isCenterFirst)
            {
                arc.Center = firstPt;
                arc.Radius = arc.Center.DistanceTo(secondPt);
                arc.StartAngle = Vector3d.XAxis.GetAngleTo(secondPt - arc.Center, Vector3d.ZAxis);
                arc.EndAngle = arc.StartAngle;
            }
            else
            {
                if (_isCenterSecond)
                {
                    arc.Center = secondPt;
                    arc.Radius = arc.Center.DistanceTo(firstPt);
                    arc.StartAngle = Vector3d.XAxis.GetAngleTo(firstPt - arc.Center, Vector3d.ZAxis);
                    arc.EndAngle = arc.StartAngle;
                }
                else
                {
                    arc.Center = new Point3d();
                    arc.Radius = 1;
                    arc.StartAngle = 0.0;
                    arc.EndAngle = 0.0;
                }
            }
            return arc;
        }

        /// <summary>
        /// Get end opptions of arc
        /// </summary>
        private ProcessFlag GetThirdOptions(Arc arc, Point3d firstPt, Point3d secondPt, double offsetAngle)
        {
            ArcOpt optFlg;
            if (_isCenterFirst)
            {
                optFlg = ArcOpt.CenterStartEnd;
            }
            else if (_isContinue)
            {
                var lastPoints = GetLastControlPoints();
                if (lastPoints._arcStartToEnd)
                {
                    optFlg = ArcOpt.ContinueArcStartToEnd;
                    // line => ArcOpt.ContinueLine
                }
                else
                    optFlg = ArcOpt.ContinueArcEndToStart;

                optFlg = _arcOptContinue;
            }
            else
            {
                if (_isCenterSecond) optFlg = ArcOpt.StartCenterEnd;
                else
                {
                    if (_isEndSecond) optFlg = ArcOpt.StartEndCenter;
                    else optFlg = ArcOpt.StartMidEnd;
                }
            }

            var firstPtWcs = firstPt.TransformBy(CoordConverter.UcsToWcs());
            var secondPtWcs = secondPt.TransformBy(CoordConverter.UcsToWcs());

            var optThird = new JigPromptPointOpt(arc, firstPt, secondPt, optFlg, offsetAngle);
            if (_isCenterFirst || _isCenterSecond)
            {
                optThird.Options = new JigPromptPointOptions($"\n{ArcRes.UI_PA3}")
                {
                    BasePoint = _isCenterFirst ? firstPtWcs : secondPtWcs,
                    UseBasePoint = true,
                    Keywords =
                                {
                                    { "A", "A", "Angle" },
                                    { "L", "L", "chord Length" },
                                },
                    Cursor = CursorType.RubberBand,
                };
            }
            else if (_isEndSecond)
            {
                optThird.Options = new JigPromptPointOptions($"\n{ArcRes.UI_PB2}")
                {
                    BasePoint = firstPtWcs,
                    UseBasePoint = true,
                    Keywords =
                                {
                                    { "A", "A", "Angle" },
                                    { "D", "D", "Direction" },
                                    { "R", "R", "Radius" },
                                },
                };
            }
            else if (_isContinue)
            {
                optThird.Options = new JigPromptPointOptions($"\n{ArcRes.UI_ContP1}")
                {
                    BasePoint = secondPtWcs,
                    UseBasePoint = true,
                    Cursor = CursorType.RubberBand,
                };
            }
            else
            {
                optThird.Options = new JigPromptPointOptions($"\n{ArcRes.UI_PD1}")
                {
                    BasePoint = secondPtWcs,
                    UseBasePoint = true,
                };
            }
            PromptResult resEndPt;
            if (_isCenterSecond || _isCenterFirst)
            {
                resEndPt = _userInput.GetThirdEndPointAfterCenterStartOrSecond(optThird);
            }
            else if (_isEndSecond)
            {
                resEndPt = _userInput.GetThirdCenterPoint(optThird);
            }
            else if (_isContinue)
            {
                resEndPt = _userInput.GetEndPointContinue(optThird);
            }
            else
            {
                resEndPt = _userInput.GetThirdEndPointAfterStartAndMid(optThird);
            }

            if (resEndPt.Status == PromptStatus.OK)
            {
                return ProcessFlag.Finished;
            }

            if (resEndPt.Status != PromptStatus.Keyword)
            {
                return ProcessFlag.CancelOrDefault;
            }

            switch (resEndPt.StringResult)
            {
                case "A":
                    if (_isEndSecond)
                        return GetAngleArc(arc, firstPt, secondPt, ArcOpt.StartEndAngle, offsetAngle);
                    else //_isCenterFirst || _isCenterSecond = true
                        return GetAngleArc(arc, firstPt, secondPt,
                            _isCenterFirst ? ArcOpt.CenterStartAngle : ArcOpt.StartCenterAngle, offsetAngle);
                case "L":
                    return GetDistanceArc(arc, firstPt, secondPt,
                        _isCenterFirst ? ArcOpt.CenterStartLength : ArcOpt.StartCenterLength, offsetAngle);
                case "D":
                    return GetAngleArc(arc, firstPt, secondPt, ArcOpt.StartEndDirection, offsetAngle);
                case "R":
                    return GetDistanceArc(arc, firstPt, secondPt, ArcOpt.StartEndRadius, offsetAngle);
                default:
                    return ProcessFlag.CancelOrDefault;
            }
        }

        private void AddArcIntoDatabase(Arc arc)
        {
            using (var tr = Util.StartTransaction())
            {
                tr.AddNewlyCreatedDBObject(arc, true, tr.CurrentSpace());
                _userInput.AppendArcId(arc.Id);

                tr.Commit();

                //var acLine = new Line(new Point3d(0, 0, 0), new Point3d(5, 1, 0));
                //tr.AddNewlyCreatedDBObject(acLine, true, tr.CurrentSpace());

                //tr.Commit();
                Util.AddObjectIdToCollection(arc.ObjectId);
            }
        }

        /// <summary>
        /// Process get angle or direction of arc
        /// </summary>
        private ProcessFlag GetAngleArc(Arc arc, Point3d firstPt, Point3d secondPt, ArcOpt arcOpt, double offsetAngle)
        {
            var firstPtWcs = firstPt.TransformBy(CoordConverter.UcsToWcs());
            var secondPtWcs = secondPt.TransformBy(CoordConverter.UcsToWcs());

            string message;
            if (arcOpt == ArcOpt.StartEndDirection)
                message = $"\n{ArcRes.UI_PB4}";
            else //(arcOpt == ArcOpt.StartEndAngle || arcOpt == ArcOpt.StartCenterAngle || CenterStartAngle)
                message = $"\n{ArcRes.UI_PA4}";

            var optAngle = new JigPromptAngleOpt(arc, firstPt, secondPt, arcOpt, offsetAngle)
            {
                Options = new JigPromptAngleOptions(message)
                {
#if _IJCAD_
                    DefaultValue = 0,
#endif
                    BasePoint = arcOpt == ArcOpt.StartCenterAngle ? secondPtWcs : firstPtWcs,
                    UseBasePoint = true,

                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.UseBasePointElevation
                                        ,
                    Cursor = CursorType.RubberBand,

                }
            };

            var stringAngle = string.Empty;
            PromptResult resAngle;
            if (arcOpt == ArcOpt.StartEndDirection)
                resAngle = _userInput.GetDirection(optAngle);
            else //(arcOpt == ArcOpt.StartEndAngle || arcOpt == ArcOpt.StartCenterAngle || CenterStartAngle)
            {
                Util.Editor().PromptedForPoint += new PromptPointResultEventHandler(ed_PromptedForPoint);
                resAngle = _userInput.GetAngle(optAngle);
                Util.Editor().PromptedForPoint -= new PromptPointResultEventHandler(ed_PromptedForPoint);
            }
            void ed_PromptedForPoint(object sender, PromptPointResultEventArgs e)
            {
                stringAngle = e.Result.StringResult.ToString();
            }



            if (resAngle.Status == PromptStatus.OK && optAngle.IsDraw == true)
            {
                if (stringAngle != string.Empty && (arcOpt == ArcOpt.StartEndAngle || arcOpt == ArcOpt.StartCenterAngle || arcOpt == ArcOpt.CenterStartAngle))
                {
                    var angle = Converter.StringToRawAngle(stringAngle);
                    if (angle < 0)
                    {
                        (arc.StartAngle, arc.EndAngle) = (arc.EndAngle, arc.StartAngle);
                    }
                }
                return ProcessFlag.Finished;
            }
            else
            {
                if (optAngle.IsDraw == false)
                    Util.Editor().WriteMessage("*Invalid*");
                return ProcessFlag.CancelOrDefault;
            }
        }


        /// <summary>
        /// Process get radius or length of arc
        /// </summary>

        private ProcessFlag GetDistanceArc(Arc arc, Point3d firstPt, Point3d secondPt, ArcOpt arcOpt, double offsetAngle)
        {
            string message;
            message = arcOpt == ArcOpt.StartEndRadius ? $"\n{ArcRes.UI_PB5}" :
                (arcOpt == ArcOpt.CenterStartLength ? $"\n{ArcRes.UI_PB5}" : $"\n{ArcRes.UI_PC4}");

            var firstPtWcs = firstPt.TransformBy(CoordConverter.UcsToWcs());
            var secondPtWcs = secondPt.TransformBy(CoordConverter.UcsToWcs());

            var optDistance = new JigPromptDistanceOpt(arc, firstPt, secondPt, arcOpt, offsetAngle)
            {
                Options = new JigPromptDistanceOptions(message)
                {
                    UseBasePoint = true,
                    BasePoint = arcOpt == ArcOpt.StartCenterLength ? firstPtWcs : secondPtWcs,
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.UseBasePointElevation |
                                        UserInputControls.NoZeroResponseAccepted,
                    Cursor = CursorType.RubberBand,
                }
            };

            PromptResult resDistance;
            if (arcOpt == ArcOpt.StartEndRadius)
                resDistance = _userInput.GetRadius(optDistance);
            else //ArcOpt.CenterStartLength || ArcOpt.StartCenterLength
                resDistance = _userInput.GetLength(optDistance);

            if (resDistance.Status == PromptStatus.OK && optDistance.IsDraw == true)
            {
                return ProcessFlag.Finished;
            }
            else
            {
                if (optDistance.IsDraw == false)
                    Util.Editor().WriteMessage(ArcRes.CM_ER1);
                return ProcessFlag.CancelOrDefault;
            }
        }



        [DllImport(
#if _AutoCAD_         
              "AcadUtils",
#elif _IJCAD_     
              "GcadUtils",
#endif
         CallingConvention = CallingConvention.Cdecl,
         CharSet = CharSet.Unicode,
         EntryPoint = "GetObjectIds")]
        private static extern int GetObjectIds(out IntPtr objIds);
        private ObjectIdCollection OutputObjectIds()
        {
            try
            {
                var count = GetObjectIds(out var pointer);
                var ids = new ObjectIdCollection();
                var arr = new IntPtr[count];
                Marshal.Copy(pointer, arr, 0, count);
                Marshal.FreeCoTaskMem(pointer);
                for (var i = 0; i < count; i++)
                {
                    var id = new ObjectId(arr[i]);
                    ids.Add(id);
                }
                return ids;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
                return null;
            }
        }

        private Entity GetCurEnt()
        {
            var ids = OutputObjectIds();
            Entity curEnt = null;
            for (int i = ids.Count; i > 0; i--)
            {
                using (Transaction icTrans = Util.StartTransaction())
                using (Entity ent = icTrans.GetObject(ids[i - 1], OpenMode.ForRead, true) as Entity)
                {
                    if (ent is Polyline || ent is Line || ent is Arc)
                    {
                        curEnt = icTrans.GetObject(ids[i - 1], OpenMode.ForRead, true) as Entity;
                        return curEnt;
                    }
                }
            }

            return curEnt;
        }

        private Entity GetCurrentEnt(ObjectId id)
        {
            using (var tr = Util.StartTransaction())
            {
                var ent = tr.GetObject<Entity>(id, OpenMode.ForRead);

                return ent;
            }
        }

        private (Point3d, Point3d, ArcOpt) GetPointFromEntity(Entity curEnt)
        {
            var pt1 = new Point3d();
            var pt2 = new Point3d();
            var optFlg = ArcOpt.ContinueLine;

            if (curEnt is Line)
            {
                optFlg = ArcOpt.ContinueLine;
                pt1 = ((Line)curEnt).StartPoint;
                pt2 = ((Line)curEnt).EndPoint;
            }
            if (curEnt is Arc)
            {
                optFlg = ArcOpt.ContinueArcStartToEnd;
                pt1 = ((Arc)curEnt).Center;
                pt2 = ((Arc)curEnt).EndPoint;

                var tempStartPoint = ((Arc)curEnt).StartPoint;
                var tempEndPoint = ((Arc)curEnt).EndPoint;

                var lastPoint = SystemVariable.GetPoint3d("LASTPOINT");

                if (lastPoint == tempStartPoint)
                {
                    pt2 = tempStartPoint;
                }
                else if (lastPoint == tempEndPoint)
                {
                    pt2 = tempEndPoint;
                }

                if (pt2.IsSamePoint(new Point3d()))
                {
                    var objId = Util.GetCurrSpaceObjectIds();

                    if (objId.Count > 1)
                    {
                        var secCurEnt = GetCurrentEnt(objId[objId.Count - 2]);
                        if (secCurEnt is Arc)
                        {
                            pt2 = ChkMatchPoint(((Arc)secCurEnt).StartPoint, ((Arc)secCurEnt).EndPoint,
                                tempStartPoint, tempEndPoint);

                            if (pt2.IsSamePoint(new Point3d()))
                            {
                                GetClosePoint(lastPoint, tempStartPoint, tempEndPoint);
                            }
                        }
                    }
                    else
                    {
                        GetClosePoint(lastPoint, tempStartPoint, tempEndPoint);
                    }
                }
            }
            if (curEnt is Polyline)
            {
                var oldSegment = new DBObjectCollection();
                ((Polyline)curEnt).Explode(oldSegment);
                Entity SegmentEnd = oldSegment[oldSegment.Count - 1] as Entity;

                if (SegmentEnd is Line)
                {
                    optFlg = ArcOpt.ContinueLine;
                    pt1 = ((Line)SegmentEnd).StartPoint;
                    pt2 = ((Line)SegmentEnd).EndPoint;
                }
                else if (SegmentEnd is Arc)
                {
                    optFlg = ArcOpt.ContinueArcEndToStart;
                    var endPointArc = ((Arc)SegmentEnd).EndPoint;
                    pt1 = ((Arc)SegmentEnd).Center;
                    pt2 = ((Polyline)curEnt).EndPoint;

                    if ((endPointArc - pt2).IsZeroLength())
                    {
                        optFlg = ArcOpt.ContinueArcStartToEnd;
                    }
                }
            }
            return (pt1, pt2, optFlg);
        }

        public Point3d ChkMatchPoint(Point3d startPoint, Point3d endPoint, Point3d firstPoint, Point3d secPoint)
        {
            var temp = new Point3d();

            if ((firstPoint == startPoint && secPoint == endPoint) || (firstPoint == endPoint && secPoint == startPoint))
            {
                return temp;
            }
            if ((firstPoint != startPoint && secPoint != endPoint) || (firstPoint != endPoint && secPoint != startPoint))
            {
                return temp;
            }

            if (firstPoint == startPoint || firstPoint == endPoint)
            {
                temp = secPoint;
            }
            else
            {
                temp = firstPoint;
            }


            return temp;
        }

        public Point3d GetClosePoint(Point3d lastPoint, Point3d firstPoint, Point3d secPoint)
        {
            var temp = new Point3d();

            temp = (lastPoint.DistanceTo(firstPoint) < lastPoint.DistanceTo(secPoint)) ? firstPoint : secPoint;

            return temp;
        }

    }
}
