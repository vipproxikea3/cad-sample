﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;


#endif
using System.Windows.Forms;
using JrxCad.Utility;
using System;
using JrxCad.Helpers;

//TODO: [ARC] please re-check source below with re-sharper
namespace JrxCad.Command.ArcCmd
{
    public partial class ArcCmd : EnterOptCommand
    {
        private readonly IArcUserInput _userInput;
        public interface IArcUserInput
        {

            PromptPointResult GetFirstStartPoint(PromptPointOptions options);
            PromptResult GetSecondMidPoint(PointDrawJigN.JigActions options);
            PromptResult GetThirdEndPointAfterStartAndMid(PointDrawJigN.JigActions options);
            PromptPointResult GetFirstCenterPoint(PromptPointOptions options);
            PromptResult GetSecondStartPoint(PointDrawJigN.JigActions options);
            PromptResult GetThirdEndPointAfterCenterStartOrSecond(PointDrawJigN.JigActions options);
            PromptResult GetSecondEndPointAfterStart(PointDrawJigN.JigActions options);
            PromptResult GetThirdCenterPoint(PointDrawJigN.JigActions options);
            PromptResult GetAngle(AngleDrawJigN.JigActions options);
            PromptResult GetLength(DistanceDrawJigN.JigActions options);
            PromptResult GetDirection(AngleDrawJigN.JigActions options);
            PromptResult GetRadius(DistanceDrawJigN.JigActions options);
            PromptResult GetSecondCenterPoint(PointDrawJigN.JigActions options);
            PromptResult GetEndPointContinue(PointDrawJigN.JigActions options);

            void AppendArcId(ObjectId id);
        }

        private class UserInput : IArcUserInput
        {
            public PromptPointResult GetFirstStartPoint(PromptPointOptions options)
            {
                return Util.Editor().GetPoint(options);
            }

            PromptResult IArcUserInput.GetSecondMidPoint(PointDrawJigN.JigActions options)
            {
                var jig = new PointDrawJigN(options);
                return Util.Editor().Drag(jig);
            }

            PromptResult IArcUserInput.GetThirdEndPointAfterStartAndMid(PointDrawJigN.JigActions options)
            {
                var jig = new PointDrawJigN(options);
                return Util.Editor().Drag(jig);
            }

            public PromptPointResult GetFirstCenterPoint(PromptPointOptions options)
            {
                return Util.Editor().GetPoint(options);
            }

            PromptResult IArcUserInput.GetSecondStartPoint(PointDrawJigN.JigActions options)
            {
                var jig = new PointDrawJigN(options);
                return Util.Editor().Drag(jig);
            }
            PromptResult IArcUserInput.GetSecondCenterPoint(PointDrawJigN.JigActions options)
            {
                var jig = new PointDrawJigN(options);
                return Util.Editor().Drag(jig);
            }

            PromptResult IArcUserInput.GetThirdEndPointAfterCenterStartOrSecond(PointDrawJigN.JigActions options)
            {
                var jig = new PointDrawJigN(options);
                return Util.Editor().Drag(jig);
            }

            PromptResult IArcUserInput.GetSecondEndPointAfterStart(PointDrawJigN.JigActions options)
            {
                var jig = new PointDrawJigN(options);
                return Util.Editor().Drag(jig);
            }

            PromptResult IArcUserInput.GetThirdCenterPoint(PointDrawJigN.JigActions options)
            {
                var jig = new PointDrawJigN(options);
                return Util.Editor().Drag(jig);
            }

            PromptResult IArcUserInput.GetAngle(AngleDrawJigN.JigActions options)
            {
                var jig = new AngleDrawJigN(options);
                return Util.Editor().Drag(jig);
            }

            PromptResult IArcUserInput.GetLength(DistanceDrawJigN.JigActions options)
            {
                var jig = new DistanceDrawJigN(options);
                return Util.Editor().Drag(jig);
            }

            PromptResult IArcUserInput.GetDirection(AngleDrawJigN.JigActions options)
            {
                var jig = new AngleDrawJigN(options);
                return Util.Editor().Drag(jig);
            }

            PromptResult IArcUserInput.GetRadius(DistanceDrawJigN.JigActions options)
            {
                var jig = new DistanceDrawJigN(options);
                return Util.Editor().Drag(jig);
            }

            PromptResult IArcUserInput.GetEndPointContinue(PointDrawJigN.JigActions options)
            {
                var jig = new PointDrawJigN(options);
                return Util.Editor().Drag(jig);
            }

            public void AppendArcId(ObjectId id)
            {
            }
        }

        public class JigPromptPointOpt : PointDrawJigN.JigActions
        {
            private readonly Arc _arc;
            private readonly ArcOpt _arcOpt;
            private readonly Point3d _firstPtU;
            private readonly Point3d _secondPtU;
            private readonly double _defaultAngle;
            private readonly double _offsetAngle;

            public JigPromptPointOpt()
            {
                _arc = null;
            }

            public JigPromptPointOpt(Arc arc, Point3d firstPt, Point3d secondPt, ArcOpt arcOpt, double offsetAngle)
            {
                _arc = arc;
                _firstPtU = firstPt;
                _secondPtU = secondPt;
                _arcOpt = arcOpt;
                _defaultAngle = arc.StartAngle;
                _offsetAngle = offsetAngle;
            }

            public override JigPromptPointOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                return _arc != null && geometry.Draw(_arc);
            }

            public override void OnUpdate()
            {
                UpdatePoint(LastValue);
            }

            public void UpdatePoint(Point3d endPtW)
            {
                if (_arc == null)
                {
                    return;
                }
                //Transform to UCS, update Z
                var endPtU = endPtW.TransformBy(CoordConverter.WcsToUcs());
                endPtU = new Point3d(endPtU.X, endPtU.Y, _firstPtU.Z);

                switch (_arcOpt)
                {
                    case ArcOpt.CenterStartEnd:
                    case ArcOpt.StartCenterEnd:
                        {
                            var centerPtU = _arcOpt == ArcOpt.CenterStartEnd ? _firstPtU : _secondPtU;
                            _arc.StartAngle = _defaultAngle;
                            var endAngle = Vector3d.XAxis.GetAngleTo(endPtU - centerPtU, Vector3d.ZAxis);
                            _arc.EndAngle = endAngle + _offsetAngle;

                            if (Control.ModifierKeys == Keys.Control)
                            {
                                (_arc.StartAngle, _arc.EndAngle) = (_arc.EndAngle, _arc.StartAngle);
                            }
                        }
                        break;
                    case ArcOpt.StartEndCenter:
                        {
                            _arc.Center = endPtU.TransformBy(CoordConverter.UcsToWcs());
                            _arc.Radius = endPtU.DistanceTo(_firstPtU);

                            var startAngle = Vector3d.XAxis.GetAngleTo(_firstPtU - endPtU, Vector3d.ZAxis);
                            var endAngle = Vector3d.XAxis.GetAngleTo(_secondPtU - endPtU, Vector3d.ZAxis);
                            _arc.StartAngle = startAngle + _offsetAngle;
                            _arc.EndAngle = endAngle + _offsetAngle;

                            if (Control.ModifierKeys == Keys.Control)
                            {
                                (_arc.StartAngle, _arc.EndAngle) = (_arc.EndAngle, _arc.StartAngle);
                            }
                        }
                        break;
                    case ArcOpt.StartMidEnd:
                        {
                            var circularArc3d = new CircularArc3d(_firstPtU, _secondPtU, endPtU);
                            var centerPointU = circularArc3d.Center;

                            var anglecheck = (_secondPtU - _firstPtU).GetAngleTo(endPtU - _firstPtU, Vector3d.ZAxis);

                            //var centerPoint = GetCenterFromThreePoints(_firstPt, _secondPt, point3D);
                            var radius = centerPointU.DistanceTo(_firstPtU);
                            if (radius.IsZero())
                            {
                                return;
                            }
                            _arc.Center = centerPointU.TransformBy(CoordConverter.UcsToWcs());
                            _arc.Radius = radius;

                            var startAngle = Vector3d.XAxis.GetAngleTo(_firstPtU - centerPointU, Vector3d.ZAxis);
                            var endAngle = Vector3d.XAxis.GetAngleTo(endPtU - centerPointU, Vector3d.ZAxis);
                            _arc.StartAngle = startAngle + _offsetAngle;
                            _arc.EndAngle = endAngle + _offsetAngle;

                            if (anglecheck > Math.PI)
                            {
                                (_arc.StartAngle, _arc.EndAngle) = (_arc.EndAngle, _arc.StartAngle);
                            }
                        }
                        break;

                    case ArcOpt.ContinueLine:
                    case ArcOpt.ContinueArcStartToEnd:
                    case ArcOpt.ContinueArcEndToStart:
                        {
                            Vector3d startEndVec = endPtU - _secondPtU;
                            Vector3d tempDicrection = _firstPtU - _secondPtU;

                            Vector3d directionVec = tempDicrection.RotateBy(Math.PI / 2, Vector3d.ZAxis);
                            if (_arcOpt == ArcOpt.ContinueArcStartToEnd)
                            {
                                directionVec = tempDicrection.RotateBy(-1 * Math.PI / 2, Vector3d.ZAxis);
                            }
                            if (_arcOpt == ArcOpt.ContinueLine)
                            {
                                directionVec = _secondPtU - _firstPtU;
                            }
                            double angle1 = directionVec.GetAngleTo(startEndVec, Vector3d.ZAxis);

                            //angle2 is the smaller angle between the two vectors, and always from 0 to PI
                            double angle2 = Math.Min(angle1, 2 * Math.PI - angle1);
                            //angleAtCenter (included Angle) is two times larger than angle2, and always from 0 to 2*PI
                            double angleAtCenter = 2 * Math.Min(angle2, Math.PI - angle2);
                            double temp = Math.Sin(angleAtCenter / 2);

                            double radius = endPtU.DistanceTo(_secondPtU) / 2 / temp;

                            Point3d centerPointU;
                            if (angle1 > Math.PI)
                            {
                                centerPointU = CenterPointFrom(_secondPtU, endPtU, radius, angle2 < Math.PI / 2/*reverse or not*/);
                            }
                            else
                            {
                                centerPointU = CenterPointFrom(_secondPtU, endPtU, radius, angle2 > Math.PI / 2/*reverse or not*/);
                            }

                            _arc.Radius = centerPointU.DistanceTo(_secondPtU);
                            _arc.Center = centerPointU.TransformBy(CoordConverter.UcsToWcs());

                            double startAngle = Vector3d.XAxis.GetAngleTo(_secondPtU - centerPointU, Vector3d.ZAxis);
                            double endAngle = Vector3d.XAxis.GetAngleTo(endPtU - centerPointU, Vector3d.ZAxis);
                            _arc.StartAngle = startAngle + _offsetAngle;
                            _arc.EndAngle = endAngle + _offsetAngle;

                            if (angle1 > Math.PI)
                            {
                                (_arc.StartAngle, _arc.EndAngle) = (_arc.EndAngle, _arc.StartAngle);
                            }
                            if (Control.ModifierKeys == Keys.Control)
                            {
                                (_arc.StartAngle, _arc.EndAngle) = (_arc.EndAngle, _arc.StartAngle);
                            }
                        }
                        break;
                    case ArcOpt.CenterStartAngle:
                        break;
                    case ArcOpt.CenterStartLength:
                        break;
                    case ArcOpt.StartCenterLength:
                        break;
                    case ArcOpt.StartCenterAngle:
                        break;
                    case ArcOpt.StartEndAngle:
                        break;
                    case ArcOpt.StartEndDirection:
                        break;
                    case ArcOpt.StartEndRadius:
                        break;
                    default:
                        break;
                }
            }
        }

        public class JigPromptAngleOpt : AngleDrawJigN.JigActions
        {
            private readonly Arc _arc;
            private readonly ArcOpt _arcOpt;
            private readonly Point3d _firstPtU;
            private readonly Point3d _secondPtU;
            private readonly double _defaultAngle;
            public bool IsDraw = true;
            private readonly double _offsetAngle;

            public JigPromptAngleOpt(Arc arc, Point3d firstPt, Point3d secondPt, ArcOpt arcOpt, double offsetAngle)
            {
                _arc = arc;
                _firstPtU = firstPt;
                _secondPtU = secondPt;
                _arcOpt = arcOpt;
                _defaultAngle = arc.StartAngle;
                _offsetAngle = offsetAngle;
            }

            public override JigPromptAngleOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                return geometry.Draw(_arc);
            }

            public void UpdateAngle(double angle)
            {
                switch (_arcOpt)
                {
                    case ArcOpt.CenterStartAngle:
                    case ArcOpt.StartCenterAngle:
                        {
                            if (angle == 0)
                            {
                                IsDraw = false;
                                return;
                            }
                            IsDraw = true;
                            _arc.StartAngle = _defaultAngle;
                            _arc.EndAngle = angle + _arc.StartAngle;
                            if (SystemVariable.GetInt("Angdir") == 1)
                            {
                                (_arc.StartAngle, _arc.EndAngle) = (_arc.EndAngle, _arc.StartAngle);
                            }
                        }
                        break;
                    case ArcOpt.StartEndAngle:
                        {
                            var temp = 2 * Math.Sin(angle / 2);
                            if (temp.IsZero())
                            {
                                return;
                            }

                            if (angle == 0)
                            {
                                IsDraw = false;
                                return;
                            }
                            IsDraw = true;

                            var radius = _secondPtU.DistanceTo(_firstPtU) / temp;
                            var centerPointU = CenterPointFrom(_firstPtU, _secondPtU, radius, angle > Math.PI /*reverse or not*/);
                            _arc.Center = centerPointU.TransformBy(CoordConverter.UcsToWcs());
                            _arc.Radius = _firstPtU.DistanceTo(centerPointU);

                            var startAngle = Vector3d.XAxis.GetAngleTo(_firstPtU - centerPointU, Vector3d.ZAxis);
                            var endAngle = Vector3d.XAxis.GetAngleTo(_secondPtU - centerPointU, Vector3d.ZAxis);
                            _arc.StartAngle = startAngle + _offsetAngle;
                            _arc.EndAngle = endAngle + _offsetAngle;
                            if (SystemVariable.GetInt("Angdir") == 1)
                            {
                                (_arc.StartAngle, _arc.EndAngle) = (_arc.EndAngle, _arc.StartAngle);
                            }
                        }
                        break;
                    case ArcOpt.StartEndDirection:
                        {
                            var startEndVec = _secondPtU - _firstPtU;
                            if (startEndVec.IsZeroLength())
                            {
                                return;
                            }

                            var directionVec = Vector3d.XAxis.RotateBy(angle, Vector3d.ZAxis);
                            var angle1 = directionVec.GetAngleTo(startEndVec, Vector3d.ZAxis);

                            //angle2 is the smaller angle between the two vectors, and always from 0 to PI
                            var angle2 = Math.Min(angle1, (2 * Math.PI) - angle1);
                            //angleAtCenter (included Angle) is two times larger than angle2, and always from 0 to 2*PI
                            var angleAtCenter = 2 * Math.Min(angle2, Math.PI - angle2);
                            var temp = Math.Sin(angleAtCenter / 2);
                            if (temp.IsZero())
                            {
                                return;
                            }

                            var radius = _secondPtU.DistanceTo(_firstPtU) / 2 / temp;

                            var centerPointU = angle1 > Math.PI
                                ? CenterPointFrom(_firstPtU, _secondPtU, radius, angle2 < Math.PI / 2/*reverse or not*/)
                                : CenterPointFrom(_firstPtU, _secondPtU, radius, angle2 > Math.PI / 2/*reverse or not*/);
                            _arc.Radius = centerPointU.DistanceTo(_firstPtU);
                            _arc.Center = centerPointU.TransformBy(CoordConverter.UcsToWcs());

                            var startAngle = Vector3d.XAxis.GetAngleTo(_firstPtU - centerPointU, Vector3d.ZAxis);
                            var endAngle = Vector3d.XAxis.GetAngleTo(_secondPtU - centerPointU, Vector3d.ZAxis);
                            _arc.StartAngle = startAngle + _offsetAngle;
                            _arc.EndAngle = endAngle + _offsetAngle;

                            if (angle1 > Math.PI)
                            {
                                (_arc.StartAngle, _arc.EndAngle) = (_arc.EndAngle, _arc.StartAngle);
                            }
                        }
                        break;
                    case ArcOpt.CenterStartEnd:
                        break;
                    case ArcOpt.CenterStartLength:
                        break;
                    case ArcOpt.StartCenterEnd:
                        break;
                    case ArcOpt.StartCenterLength:
                        break;
                    case ArcOpt.StartEndCenter:
                        break;
                    case ArcOpt.StartEndRadius:
                        break;
                    case ArcOpt.StartMidEnd:
                        break;
                    default:
                        break;
                }

                if (Control.ModifierKeys == Keys.Control)
                {
                    (_arc.StartAngle, _arc.EndAngle) = (_arc.EndAngle, _arc.StartAngle);
                }
            }

            public override void OnUpdate()
            {
                UpdateAngle(LastValue);
            }
        }

        public class JigPromptDistanceOpt : DistanceDrawJigN.JigActions
        {
            private readonly Arc _arc;
            private readonly ArcOpt _arcOpt;
            private readonly Point3d _firstPtU;
            private readonly Point3d _secondPtU;
            private readonly double _defaultAngle;
            public bool IsDraw = true;
            private readonly double _offsetAngle;

            public JigPromptDistanceOpt(Arc arc, Point3d firstPt, Point3d secondPt, ArcOpt arcOpt, double offsetAngle)
            {
                _arc = arc;
                _firstPtU = firstPt;
                _secondPtU = secondPt;
                _arcOpt = arcOpt;
                _defaultAngle = arc.StartAngle;
                _offsetAngle = offsetAngle;
            }

            public override JigPromptDistanceOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                return geometry.Draw(_arc);
            }

            public void UpdateDistance(double dist)
            {
                switch (_arcOpt)
                {
                    case ArcOpt.StartCenterLength:
                    case ArcOpt.CenterStartLength:
                        {
                            //Check if value of dist greater than diameter
                            if (Math.Abs(dist) > 2 * _secondPtU.DistanceTo(_firstPtU))
                            {
                                IsDraw = false;
                                return;
                            }
                            IsDraw = true;

                            //Included angle
                            var angleAtCenter = 2 * Math.Asin(dist / 2 / _secondPtU.DistanceTo(_firstPtU));
                            _arc.StartAngle = _defaultAngle;

                            //UNDONE: 
                            if ((_arcOpt == ArcOpt.StartCenterLength ? _firstPtU.Y : _secondPtU.Y) >
                                ((_arcOpt == ArcOpt.StartCenterLength ? _secondPtU.Y : _firstPtU.Y)))
                            {
                                _arc.EndAngle = angleAtCenter + _arc.StartAngle;
                            }
                            else
                            {
                                _arc.EndAngle = angleAtCenter - 2 * Math.PI + _arc.StartAngle;
                            }
                        }
                        break;
                    case ArcOpt.StartEndRadius:
                        {
                            var minRadius = _firstPtU.DistanceTo(_secondPtU) / 2;
                            if (Math.Abs(dist) < minRadius)
                            {
                                IsDraw = false;
                                return;
                            }
                            IsDraw = true;
                            var centerPointU = CenterPointFrom(_firstPtU, _secondPtU, dist, dist < 0);
                            _arc.Center = centerPointU.TransformBy(CoordConverter.UcsToWcs());
                            _arc.Radius = Math.Abs(dist);

                            var startAngle = Vector3d.XAxis.GetAngleTo(_firstPtU - centerPointU, Vector3d.ZAxis);
                            var endAngle = Vector3d.XAxis.GetAngleTo(_secondPtU - centerPointU, Vector3d.ZAxis);
                            _arc.StartAngle = startAngle + _offsetAngle;
                            _arc.EndAngle = endAngle + _offsetAngle;
                        }
                        break;
                    case ArcOpt.CenterStartAngle:
                        break;
                    case ArcOpt.CenterStartEnd:
                        break;
                    case ArcOpt.StartCenterEnd:
                        break;
                    case ArcOpt.StartCenterAngle:
                        break;
                    case ArcOpt.StartEndCenter:
                        break;
                    case ArcOpt.StartEndAngle:
                        break;
                    case ArcOpt.StartEndDirection:
                        break;
                    case ArcOpt.StartMidEnd:
                        break;
                    default:
                        break;
                }
                if (Control.ModifierKeys == Keys.Control)
                {
                    (_arc.StartAngle, _arc.EndAngle) = (_arc.EndAngle, _arc.StartAngle);
                }
            }

            public override void OnUpdate()
            {
                UpdateDistance(LastValue);
            }
        }

        public static Point3d CenterPointFrom(Point3d startPt, Point3d endPt, double radius, bool reverse = false)
        {
            var startEndVec = endPt - startPt;
            var t = Math.Sqrt((radius / startEndVec.Length * (radius / startEndVec.Length)) - 0.25);

            if (reverse)
            {
                t *= -1;
            }

            var xo = ((startPt.X + endPt.X) / 2) - (t * (endPt.Y - startPt.Y));
            var yo = ((startPt.Y + endPt.Y) / 2) + (t * (endPt.X - startPt.X));
            var zo = startPt.Z;
            return new Point3d(xo, yo, zo);
        }

        public static Point3d GetCenterFromThreePoints(Point3d startPt, Point3d secondPt, Point3d endPt)
        {
            var angleA = (secondPt - startPt).GetAngleTo(endPt - startPt);
            var angleB = (startPt - secondPt).GetAngleTo(endPt - secondPt);
            var angleC = (startPt - endPt).GetAngleTo(secondPt - endPt);
            var sin2A = Math.Sin(2 * angleA);
            var sin2B = Math.Sin(2 * angleB);
            var sin2C = Math.Sin(2 * angleC);
            var x0 = ((startPt.X * sin2A) + (secondPt.X * sin2B) + (endPt.X * sin2C)) / (sin2A + sin2B + sin2C);
            var y0 = ((startPt.Y * sin2A) + (secondPt.Y * sin2B) + (endPt.Y * sin2C)) / (sin2A + sin2B + sin2C);
            var z0 = ((startPt.Z * sin2A) + (secondPt.Z * sin2B) + (endPt.Z * sin2C)) / (sin2A + sin2B + sin2C);
            return new Point3d(x0, y0, z0);
        }
    }
}
