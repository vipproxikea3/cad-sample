﻿#if _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.GraphicsInterface;
#elif _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.GraphicsInterface;
#endif
using JrxCad.Utility;
using System.Collections.Generic;
using static JrxCad.Command.WBlock.WBlockCmd;

namespace JrxCad.Command.MinusWBlock
{
    public partial class MinusWBlockCmd
    {
        private readonly IUserInputWBlock _userInput;

        public sealed class IntegerOpts : IntegerDrawJigN.JigActions
        {
            public IntegerOpts(string message, int lowerLimit, int upperLimit, int defaultValue, Dictionary<string, string> keywords)
            {
                Options = new PromptIntegerOptions(message)
                {
                    LowerLimit = lowerLimit,
                    UpperLimit = upperLimit,
                    DefaultValue = defaultValue
                };
                foreach (var keyword in keywords)
                {
                    Options.Keywords.Add(keyword.Key, keyword.Value);
                }
            }

            public override PromptIntegerOptions Options { get; set; }

            public PromptIntegerResult IntegerResult { get; set; }

            public override void UpdateInteger(int integer)
            {
            }

            public void GetKeyWord(object sender, PromptIntegerResultEventArgs e)
            {
                IntegerResult = e.Result;
            }

            public override void OnUpdate()
            {
            }

            public override bool Draw(WorldGeometry geometry)
            {
                return false;
            }
        }
    }
}
