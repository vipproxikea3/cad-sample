﻿#if _AutoCAD_
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
#elif _IJCAD_
using GrxCAD.ApplicationServices;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;
#endif
using JrxCad.Command.WBlock.Model;
using JrxCad.Helpers;
using JrxCad.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using static JrxCad.Command.WBlock.Model.WBlockModel;
using static JrxCad.Command.WBlock.WBlockCmd;
using Exception = System.Exception;

namespace JrxCad.Command.MinusWBlock
{
    public partial class MinusWBlockCmd : BaseCommand
    {
        #region declare
        private ObjectConversion _createMode;
        private int _precision;
        //TODO: save file DXF with Binary
        private bool _isBinary;
        private bool _isSaveThumbnail = true;
        public ObjectIdCollection SourceEntities;
        private WBlockModel _wBlockModel;

        private readonly string _dwgExtension = ".DWG";
        private readonly string _dxfExtension = ".DXF";
        private readonly string _general = "\\General";
        private readonly string _defaultFormatSave = "DefaultFormatForSave";
#if _AutoCAD_
        private readonly string _wBlockCreateMode = "WBlockCreateMode";
#endif
        #endregion

        public MinusWBlockCmd() : this(new UserInputWBlock())
        {
        }

        public MinusWBlockCmd(IUserInputWBlock userInput)
        {
            _userInput = userInput;
        }

#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommand", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommand", "-W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommand", "-SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }
                ExecuteCommand();
                Util.Editor().SetImpliedSelection(Array.Empty<ObjectId>());
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        /// <summary>
        /// execute command
        /// </summary>
        public void ExecuteCommand()
        {
            try
            {
                _wBlockModel = new WBlockModel(_userInput);
                var selectionSetCount = 0;
                var objectOnLockedLayerCount = 0;
                ObjectIdCollection sourceEntities;
                using (var tr = Util.StartTransaction())
                {
                    sourceEntities = _wBlockModel.SelectImplied(tr, ref selectionSetCount, ref objectOnLockedLayerCount);
                    tr.Commit();
                }

                //Message WBlock current settings
                CreateModeMessage();

                // Input file name and path
                if (ValidInputFilePath() != ProcessFlag.Finished) return;

                // Setting with file DXF
                if (HandleDxfFilePath() != ProcessFlag.Finished) return;

                // Save with block or whole drawing
                if (selectionSetCount == 0 && ValidTargetObject() != ProcessFlag.Finished) return;

                // Pick Base Point
                if (ValidBasePoint() != ProcessFlag.Finished) return;

                //Message Locked Objects
                if (selectionSetCount != 0) WriteMessage($"{string.Format(MinusWBlockRes.CM_2, selectionSetCount)}");

                //Select Objects
                if (selectionSetCount == 0)
                {
                    using (var tr = Util.StartTransaction())
                    {
                        sourceEntities = _wBlockModel.SelectEntities(Util.CurDoc(), tr, selectionSetCount);
                        if (sourceEntities == null) return;
                        tr.Commit();
                    }
                }

                // check object is locked or not
                if (objectOnLockedLayerCount == 1)
                {
                    WriteMessage($"\n{string.Format(MinusWBlockRes.CM_2_2_1, objectOnLockedLayerCount)}");
                }
                else if (objectOnLockedLayerCount > 1)
                {
                    WriteMessage($"\n{string.Format(MinusWBlockRes.CM_2_2_2, objectOnLockedLayerCount)}");
                }

                _wBlockModel.SourceEntities = sourceEntities;

                //Save with Objects
                if (_wBlockModel.SourceEntities.Count == 0) return;

                if (Path.GetExtension(_wBlockModel.FilePath).ToUpper() == _dwgExtension)
                {
                    _isSaveThumbnail = false;
                }

                if (_wBlockModel.IsValidPath(_wBlockModel.FilePath, true) != ValidPath.OK)
                {
                    WriteMessage(string.Format(MinusWBlockRes.DM_10));
                    return;
                }

                // save object
                _wBlockModel.SaveObjects(_isSaveThumbnail, UnitsValue.Inches, _precision, _isBinary);
                EraseObjects();
            }
            finally
            {
                if (SourceEntities != null)
                {
                    SourceEntities.Dispose();
                }
            }
        }

        /// <summary>
        /// Show message of Create Mode
        /// </summary>
        private void CreateModeMessage()
        {
            _createMode = _userInput.GetValueConfig();
            switch (_createMode)
            {
                case ObjectConversion.Convert:
                    WriteMessage(string.Format(MinusWBlockRes.CM_1_1));
                    break;
                case ObjectConversion.Retain:
                    WriteMessage(string.Format(MinusWBlockRes.CM_1_2));
                    break;
                case ObjectConversion.Delete:
                    WriteMessage(string.Format(MinusWBlockRes.CM_1_3));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Handle file DXF
        /// </summary>
        private ProcessFlag HandleDxfFilePath()
        {
            // check file name input
            if (string.IsNullOrEmpty(_wBlockModel.FilePath))
            {
                return ProcessFlag.CancelOrDefault;
            }

            // check handle for "DXF" extension
            if (Path.GetExtension(_wBlockModel.FilePath).ToUpper() == _dxfExtension)
            {
                var registryKey = new Hkey.CurrentUser();
                var keyPath = registryKey.GetProfileSubKey() + _general;

                // get format default 
                var defaultFormatForSave = registryKey.GetValue(keyPath, _defaultFormatSave);
                if (!string.IsNullOrEmpty(defaultFormatForSave))
                {
                    var defaultFormatForSaveInt = int.Parse(defaultFormatForSave);
                    defaultFormatForSaveInt++;
                    if (defaultFormatForSaveInt > 0 && defaultFormatForSaveInt < 10)
                    {
                        var documentSaveFormat = (DocumentSaveFormat)defaultFormatForSaveInt;
                        WriteMessage($"{string.Format(MinusWBlockRes.DXF_UI_1_1, documentSaveFormat.ToString().Substring(1, 2))}\n");
                    }
                    else if (defaultFormatForSaveInt > 10)
                    {
                        var documentSaveFormat = (DocumentSaveFormat)defaultFormatForSaveInt;
                        WriteMessage($"{string.Format(MinusWBlockRes.DXF_UI_1_1, documentSaveFormat.ToString().Substring(1, 4))}\n");
                    }
                }

                // DXF.UI-1 PromptResult
                var keywords = new Dictionary<string, string>
                {
                    { "B", MinusWBlockRes.Binary_Key },
                    { "P", MinusWBlockRes.Preview_Key }
                };

                // setting input keyword
                var promptIntegerOptions = new IntegerOpts($"{MinusWBlockRes.DXF_UI_1_2}", 0, 16, 16, keywords);
                while (true)
                {
                    _isBinary = false;

                    // Get input keyword result
                    var resIntegerResult = _userInput.GetInteger(promptIntegerOptions);
                    // check prompt status
                    switch (resIntegerResult.Status)
                    {
                        case PromptStatus.OK:
                            _precision = resIntegerResult.Value;
                            if (_precision != 16)
                            {
                                WriteMessage(MinusWBlockRes.DXF_UI_1_Warning);
                            }
                            return ProcessFlag.Finished;

                        case PromptStatus.None:
                            _precision = resIntegerResult.Value;
                            return ProcessFlag.Finished;

                        case PromptStatus.Keyword:
                            switch (resIntegerResult.StringResult)
                            {
                                case "B":
                                    _isBinary = true;
                                    return ProcessFlag.Finished;
                                case "P":
                                    // DXF.UI-2, PromptKeyWord
                                    var optSaveThumbnail = new PromptKeywordOptions($"{MinusWBlockRes.DXF_UI_2}")
                                    {
                                        Keywords =
                                        {
                                            { "Y", MinusWBlockRes.Yes_Key},
                                            { "N", MinusWBlockRes.No_Key},
                                        },
                                        AllowNone = true
                                    };

                                    optSaveThumbnail.Keywords.Default = "Y";
                                    while (true)
                                    {
                                        var promptKeywordResult = _userInput.GetKeyword(optSaveThumbnail);
                                        switch (promptKeywordResult.Status)
                                        {
                                            case PromptStatus.OK:
                                                switch (promptKeywordResult.StringResult)
                                                {
                                                    case "Y":
                                                        _isSaveThumbnail = true;
                                                        break;
                                                    case "N":
                                                        _isSaveThumbnail = false;
                                                        break;
                                                }
                                                break;
                                            default:
                                                return ProcessFlag.CancelOrDefault;
                                        }

                                        if (promptKeywordResult.Status == PromptStatus.OK)
                                        {
                                            break;
                                        }
                                    }
                                    break;
                            }
                            break;
                        case PromptStatus.Cancel:
                            return ProcessFlag.CancelOrDefault;
                    }
                }
            }
            return ProcessFlag.Finished;
        }

        /// <summary>
        /// Base point
        /// </summary>
        private ProcessFlag ValidBasePoint()
        {
            var optBasePoint = new PromptPointOptions($"{MinusWBlockRes.UI_2}")
            {
                Keywords =
                {
                    { "O", MinusWBlockRes.Mode_Key},
                }
            };
            while (true)
            {
                var resBasePoint = _userInput.GetPoint(optBasePoint);
                switch (resBasePoint.Status)
                {
                    case PromptStatus.Cancel:
                        return ProcessFlag.CancelOrDefault;
                    case PromptStatus.OK:
                        _wBlockModel.BasePoint = resBasePoint.Value;
                        return ProcessFlag.Finished;
                    case PromptStatus.Keyword:
                        if (resBasePoint.StringResult == "O")
                        {
                            if (HandleModeObjects() != ProcessFlag.Finished)
                            {
                                return ProcessFlag.CancelOrDefault;
                            }
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Setting Object conversion
        /// </summary>
        private ProcessFlag HandleModeObjects()
        {
            var optModeObjects = new PromptKeywordOptions($"{string.Format(MinusWBlockRes.ChooseMode)}")
            {
                Keywords =
                {
                    { "C", MinusWBlockRes.Convert_Key, $"{MinusWBlockRes.ChooseMode_1}" },
                    { "R", MinusWBlockRes.Retain_Key, $"{MinusWBlockRes.ChooseMode_2}" },
                    { "D", MinusWBlockRes.Delete_Key, $"{MinusWBlockRes.ChooseMode_3}" },
                },
                AllowNone = true,
            };

            _createMode = _userInput.GetValueConfig();
            switch (_createMode)
            {
                case ObjectConversion.Convert:
                    optModeObjects.Keywords.Default = "C";
                    break;
                case ObjectConversion.Retain:
                    optModeObjects.Keywords.Default = "R";
                    break;
                case ObjectConversion.Delete:
                    optModeObjects.Keywords.Default = "D";
                    break;
            }

            // get input keyword
            var resModeObjects = _userInput.GetKeyword(optModeObjects);
            while (true)
            {
                switch (resModeObjects.Status)
                {
                    case PromptStatus.Cancel:
                        return ProcessFlag.CancelOrDefault;
                    case PromptStatus.OK:
#if _AutoCAD_
                        switch (resModeObjects.StringResult)
                        {
                            case "C":
                                SystemVariable.SetInt(_wBlockCreateMode, (int)ObjectConversion.Convert);
                                _createMode = ObjectConversion.Convert;
                                return ProcessFlag.Finished;
                            case "R":
                                SystemVariable.SetInt(_wBlockCreateMode, (int)ObjectConversion.Retain);
                                _createMode = ObjectConversion.Retain;
                                return ProcessFlag.Finished;
                            case "D":
                                SystemVariable.SetInt(_wBlockCreateMode, (int)ObjectConversion.Delete);
                                _createMode = ObjectConversion.Delete;
                                return ProcessFlag.Finished;
                        }
#elif _IJCAD_

                        switch (resModeObjects.StringResult)
                        {
                            case "C":
                                _wBlockModel.SetRegistryValue(ObjectConversion.Convert);
                                _createMode = ObjectConversion.Convert;
                                return ProcessFlag.Finished;
                            case "R":
                                _wBlockModel.SetRegistryValue(ObjectConversion.Retain);
                                _createMode = ObjectConversion.Retain;
                                return ProcessFlag.Finished;
                            case "D":
                                _wBlockModel.SetRegistryValue(ObjectConversion.Delete);
                                _createMode = ObjectConversion.Delete;
                                return ProcessFlag.Finished;
                        }
#endif
                        break;
                }
            }
        }

        /// <summary>
        /// Save with block or whole drawing
        /// </summary>
        /// <returns></returns>
        private ProcessFlag ValidTargetObject()
        {
            var options = new PromptKeywordOptions($"\n{MinusWBlockRes.UI_1}")
            {
                AllowNone = true,
                Keywords =
                {
                    { "=", "="},
                    { "*", "*"},
                },
                AllowArbitraryInput = true,
                AppendKeywordsToMessage = false,
            };

            while (true)
            {
                var result = _userInput.GetKeyword(options);
                var blockName = Path.GetFileNameWithoutExtension(_wBlockModel.FilePath);
                switch (result.Status)
                {
                    case PromptStatus.None:
                        return ProcessFlag.Finished;
                    case PromptStatus.Cancel:
                        return ProcessFlag.CancelOrDefault;
                    case PromptStatus.OK:
                        switch (result.StringResult)
                        {
                            case "=":
                                if (_wBlockModel.IsValidPath(_wBlockModel.FilePath, true) != ValidPath.OK)
                                {
                                    WriteMessage(string.Format(MinusWBlockRes.DM_10));
                                    return ProcessFlag.CancelOrDefault;
                                }
                                if (_wBlockModel.SaveBlock(blockName, UnitsValue.Inches, _precision, _isBinary))
                                {
                                    if (_createMode == 0)
                                    {
                                        var unitsValue = GetUnitsValue(blockName);
                                        _wBlockModel.ConvertObjectsToBlock(_wBlockModel.SourceEntities, blockName, unitsValue);
                                        return ProcessFlag.CancelOrDefault;
                                    }
                                }
                                else
                                {
                                    WriteMessage($"{string.Format(MinusWBlockRes.DM_3, blockName)}");
                                }
                                break;
                            case "*":
                                if (_wBlockModel.IsValidPath(_wBlockModel.FilePath, true) != ValidPath.OK)
                                {
                                    WriteMessage(string.Format(MinusWBlockRes.DM_10));
                                    return ProcessFlag.CancelOrDefault;
                                }
                                _wBlockModel.SaveEntireDrawing(ref SourceEntities, _precision, _isBinary);
                                if (_createMode == 0)
                                {
                                    _wBlockModel.ConvertObjectsToBlock(SourceEntities, blockName);
                                }
                                return ProcessFlag.CancelOrDefault;
                            default:
                                if (_wBlockModel.IsValidPath(_wBlockModel.FilePath, true) != ValidPath.OK)
                                {
                                    WriteMessage(string.Format(MinusWBlockRes.DM_10));
                                    return ProcessFlag.CancelOrDefault;
                                }
                                if (_wBlockModel.SaveBlock(result.StringResult, UnitsValue.Inches, _precision))
                                {
                                    if (_createMode == 0)
                                    {
                                        var unitsValue = GetUnitsValue(result.StringResult);
                                        _wBlockModel.ConvertObjectsToBlock(_wBlockModel.SourceEntities, blockName, unitsValue);
                                        return ProcessFlag.CancelOrDefault;
                                    }
                                }
                                else
                                {
                                    WriteMessage($"{string.Format(MinusWBlockRes.DM_3, result.StringResult)}");
                                }
                                break;
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Input Path
        /// </summary>
        private ProcessFlag ValidInputFilePath()
        {
            var openFileOpt = new PromptStringOptions($"\n{MinusWBlockRes.FN_UI_1}")
            {
                AllowSpaces = true,
            };

            while (true)
            {
                // get input string result
                var promptFileNameResult = _userInput.GetString(openFileOpt);
                switch (promptFileNameResult.Status)
                {
                    case PromptStatus.Cancel:
                        return ProcessFlag.CancelOrDefault;

                    case PromptStatus.OK:
                        if (string.IsNullOrEmpty(promptFileNameResult.StringResult))
                        {
                            WriteMessage($"{MinusWBlockRes.DM_1}");
                            break;
                        }

                        // validate file name input
                        var textPath = promptFileNameResult.StringResult;
                        var valid = _wBlockModel.IsValidPath(textPath, true);
                        switch (valid)
                        {
                            case ValidPath.NotValid:
                                WriteMessage(string.Format(MinusWBlockRes.DM_2));
                                return ProcessFlag.CancelOrDefault;
                            case ValidPath.TooLong:
                                WriteMessage(string.Format(MinusWBlockRes.DM_6, textPath.Length));
                                return ProcessFlag.CancelOrDefault;
                        }

                        // append directory for filename
                        var dir = Path.GetDirectoryName(promptFileNameResult.StringResult);
                        if (string.IsNullOrEmpty(dir))
                        {
                            //TODO: confirm redmine #223#change-764
                            textPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + promptFileNameResult.StringResult;
                        }
                        else
                        {
                            textPath = promptFileNameResult.StringResult;
                        }

                        if (Path.GetExtension(textPath) != _dxfExtension.ToLower() && Path.GetExtension(textPath) != _dwgExtension.ToLower())
                        {
                            textPath += _dwgExtension.ToLower();
                        }

                        // validate file path
                        valid = _wBlockModel.IsValidPath(textPath, true);
                        switch (valid)
                        {
                            case ValidPath.NotExist:
                                // directory is not exist
                                WriteMessage(string.Format(MinusWBlockRes.DM_4));
                                WriteMessage(Path.GetDirectoryName(textPath));
                                return ProcessFlag.CancelOrDefault;

                            case ValidPath.Unauthorized:
                                // user don't authorized
                                var directoryValue = promptFileNameResult.StringResult.EndsWith(@"\") ||
                                                     !string.IsNullOrEmpty(Path.GetExtension(promptFileNameResult.StringResult)) ? promptFileNameResult.StringResult : promptFileNameResult.StringResult + "\\";
                                WriteMessage(string.Format(MinusWBlockRes.DM_5, Path.GetDirectoryName(directoryValue)));
                                return ProcessFlag.CancelOrDefault;

                            case ValidPath.TooLong:
                                // file path is too long
                                WriteMessage(string.Format(MinusWBlockRes.DM_6, textPath.Length));
                                return ProcessFlag.CancelOrDefault;

                            default:
                                // set file path
                                _wBlockModel.FilePath = textPath;
                                break;
                        }

                        if (!File.Exists(textPath)) return ProcessFlag.Finished;

                        // process input file name is exist in directory
                        var optFileExists = new PromptKeywordOptions($"{string.Format(MinusWBlockRes.FN_UI_2, promptFileNameResult.StringResult)}")
                        {
                            Keywords =
                                {
                                    { "Y", MinusWBlockRes.Yes_Key },
                                    { "N", MinusWBlockRes.No_Key},
                                },
                            AllowNone = true,
                            AppendKeywordsToMessage = false,
                        };

                        while (true)
                        {
                            // get input keyword result
                            var resFileExists = _userInput.GetKeyword(optFileExists);
                            switch (resFileExists.Status)
                            {
                                case PromptStatus.OK:
                                    switch (resFileExists.StringResult)
                                    {
                                        case "Y":
                                            _wBlockModel.FilePath = textPath;
                                            switch (valid)
                                            {
                                                case ValidPath.ReadOnly:
                                                    WriteMessage(string.Format(MinusWBlockRes.DM_9, textPath));
                                                    return ProcessFlag.CancelOrDefault;
                                                case ValidPath.InUseByOtherProcesses:
                                                    WriteMessage(string.Format(MinusWBlockRes.DM_8, textPath));
                                                    return ProcessFlag.CancelOrDefault;
                                                case ValidPath.InUseByCAD:
                                                    WriteMessage(string.Format(MinusWBlockRes.DM_7, textPath));
                                                    return ProcessFlag.CancelOrDefault;
                                            }
                                            return ProcessFlag.Finished;
                                        case "N":
                                            return ProcessFlag.CancelOrDefault;
                                    }
                                    break;
                                default:
                                    return ProcessFlag.CancelOrDefault;
                            }
                        }
                }
            }
        }

        /// <summary>
        /// Handle current objects
        /// </summary>
        public void EraseObjects()
        {
            switch (_createMode)
            {
                case ObjectConversion.Delete:
                    _wBlockModel.EraseObjects(_wBlockModel.SourceEntities);
                    break;
                case ObjectConversion.Convert:
                    var blockName = Path.GetFileNameWithoutExtension(_wBlockModel.FilePath);
                    _wBlockModel.ConvertObjectsToBlock(_wBlockModel.SourceEntities, blockName);
                    _wBlockModel.EraseObjects(_wBlockModel.SourceEntities);
                    break;
            }
        }

        /// <summary>
        /// Get UnitsValue in block reference
        /// </summary>
        public UnitsValue GetUnitsValue(string blockName)
        {
            using (var db = Util.Database())
            using (var tr = Util.StartTransaction())
            {
                var blockTable = tr.GetObject<BlockTable>(db.BlockTableId, OpenMode.ForWrite);

                var blockTableRecord = tr.GetObject<BlockTableRecord>(blockTable[BlockTableRecord.ModelSpace], OpenMode.ForRead);
                foreach (var objectId in blockTableRecord)
                {
                    var entity = tr.GetObject<Entity>(objectId, OpenMode.ForRead);
                    if (entity.GetType() != typeof(BlockReference))
                    {
                        continue;
                    }
                    var blockReference = (BlockReference)entity;

                    if (blockReference.Name == blockName)
                    {
                        tr.Commit();
                        return blockReference.BlockUnit;
                    }
                }
            }
            return UnitsValue.Inches;
        }
    }
}
