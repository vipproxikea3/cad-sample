﻿#if _IJCAD_
using Application = GrxCAD.ApplicationServices.Application;
using GrxCAD.Runtime;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Application = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;

#endif

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.GraphicsInterface;
using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.Command.Sample
{
    public class SamplePointMonitor
    {
        [CommandMethod("SamplePointMonitor")]
        public void CreateTemporaryObject()
        {
            try
            {
                var doc = Application.DocumentManager.MdiActiveDocument;
                doc.Editor.PointMonitor += Ed_PointMonitor;
                doc.Editor.GetPoint("\nポイントモニター起動中(クリックで終了)");
                doc.Editor.PointMonitor -= Ed_PointMonitor;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        private void Ed_PointMonitor(object sender, PointMonitorEventArgs e)
        {
            if (e?.Context == null)
            {
                return;
            }
            if (e.Context.DrawContext == null)
            {
                // たまにここに来る。
                return;
            }
            if (e.Context.DrawContext.Geometry == null)
            {
                return;
            }

            var geo = e.Context.DrawContext.Geometry;
            geo.Circle(e.Context.RawPoint, 1, Vector3d.ZAxis);
            geo.Circle(e.Context.ComputedPoint, 1.5, Vector3d.ZAxis);
            geo.Circle(e.Context.ObjectSnappedPoint, 2, Vector3d.ZAxis);
            geo.Circle(e.Context.LastPoint, 2.5, Vector3d.ZAxis);
            // 以下2つは、いつも(0,0,0)っぽい。
            geo.Circle(e.Context.CartesianSnappedPoint, 3, Vector3d.ZAxis);
            geo.Circle(e.Context.GrippedPoint, 3.5, Vector3d.ZAxis);
        }
    }
}
