﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;
using Polyline = GrxCAD.DatabaseServices.Polyline;
using Viewport = GrxCAD.GraphicsInterface.Viewport;

#elif _AutoCAD_
using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
using Viewport = Autodesk.AutoCAD.GraphicsInterface.Viewport;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;
using Autodesk.AutoCAD.Runtime;

#endif

using System;
using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.Command.Sample
{
    /// <summary>
    /// カスタムオブジェクトスナップ(●グリフ)
    /// </summary>
    /// <remarks>
    /// 対象図形：円、円弧、線、ポリライン、寸法、楕円
    /// </remarks>
    public class SampleCustomOSnap : BaseCommand
    {
        /// <summary>スナップされる角度</summary>
        protected static double Angle { get; set; } = Math.PI / 4.0;
        /// <summary>表示されるグリフ</summary>
        protected SampleGlyph Glyph { get; set; }
        /// <summary>カスタムオブジェクトスナップモード</summary>
        protected CustomObjectSnapMode Mode { get; set; }

        // 初期化処理
        private void Initialize()
        {
            if (Mode != null)
            {
                return;
            }

            Glyph = new SampleGlyph();
            Mode = new CustomObjectSnapMode("SAMPLEOSNAP", "SAMPLEOSNAP", "SampleOSnap", Glyph);

            // 円オブジェクトに新しいコールバックを適用する
            Mode.ApplyToEntityType(RXObject.GetClass(typeof(Circle)), SnapInfoCircle);

            // 円弧
            Mode.ApplyToEntityType(RXObject.GetClass(typeof(Arc)), SnapInfoArc);

            // 線
            Mode.ApplyToEntityType(RXObject.GetClass(typeof(Line)), SnapInfoLine);

            // ポリライン
            Mode.ApplyToEntityType(RXObject.GetClass(typeof(Polyline)), SnapInfoPolyline);

            // 寸法
            Mode.ApplyToEntityType(RXObject.GetClass(typeof(Dimension)), SnapInfoDimension);

            // 楕円
            Mode.ApplyToEntityType(RXObject.GetClass(typeof(Ellipse)), SnapInfoEllipse);
        }

        // カスタムオブジェクトスナップの有効/無効を切り替える
        [CommandMethod("JrxCommandTest", "SampleOSnap", CommandFlags.Transparent)]
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }

                if (CustomObjectSnapMode.IsActive("SAMPLEOSNAP"))
                {
                    CustomObjectSnapMode.Deactivate("SAMPLEOSNAP");
                    Util.Editor().WriteMessage("\nSAMPLEOSNAP OFF");
                }
                else
                {
                    Initialize();
                    CustomObjectSnapMode.Activate("SAMPLEOSNAP");
                    Util.Editor().WriteMessage("\nSAMPLEOSNAP ON");
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        /// <summary>
        /// カスタムオブジェクトスナップのグリフ
        /// </summary>
        protected class SampleGlyph : Glyph
        {
            /// <summary>グリフが表示される基準点</summary>
            protected Point3d BasePtD { get; set; }

            public override void SetLocation(Point3d point)
            {
                // 基準点としてスナップ点を保持
                BasePtD = point;         // pointは、DCS座標
            }

            /// <summary>
            /// グリフ高さ取得
            /// </summary>
            /// <param name="vport"></param>
            protected double GetGlyphHeight(Viewport vport)
            {
                // 1平方メートルあたりのピクセル数を取得
                var pixels = vport.GetNumPixelsInUnitSquare(BasePtD);
                // グリフサイズとピクセル数からカメラの高さに合ったグリフの高さを計算
                return (CustomObjectSnapMode.GlyphSize / pixels.Y) * 2.0;
            }

            /// <summary>
            /// ビューポート描画
            /// </summary>
            protected override void SubViewportDraw(ViewportDraw viewDraw)
            {
                var vport = viewDraw.Viewport;
                var glyphHeight = GetGlyphHeight(vport);

                // スナップ位置に黒丸のグリフを表示
                var posW = BasePtD.TransformBy(vport.EyeToWorldTransform);      // DCSからWCSに変換
                viewDraw.SubEntityTraits.FillType = FillType.FillAlways;
                viewDraw.Geometry.Circle(posW, glyphHeight / 4, vport.ViewDirection);
            }
        }

        /// <summary>
        /// 円にスナップ
        /// </summary>
        private void SnapInfoCircle(ObjectSnapContext context, ObjectSnapInfo result)
        {
            var circle = context.PickedObject as Circle;
            if (circle == null) return;

            // スナップ点を取得
            var snapPtW = circle.GetPointAtParameter(Angle);

            var wcsToPlane = Matrix3d.WorldToPlane(circle.Normal);
            result.SnapPoints.Add(snapPtW.TransformBy(wcsToPlane));     // SnapPointsには、OCS座標を入れる。
        }

        /// <summary>
        /// 円弧にスナップ
        /// </summary>
        private void SnapInfoArc(ObjectSnapContext context, ObjectSnapInfo result)
        {
            var arc = context.PickedObject as Arc;
            if (arc == null) return;

            // スナップ点を取得
            var snapPtW = arc.GetPointAtParameter(Angle);

            var wcsToPlane = Matrix3d.WorldToPlane(arc.Normal);
            result.SnapPoints.Add(snapPtW.TransformBy(wcsToPlane));     // SnapPointsには、OCS座標を入れる。
        }

        /// <summary>
        /// 線にスナップ
        /// </summary>
        private void SnapInfoLine(ObjectSnapContext context, ObjectSnapInfo result)
        {
            var line = context.PickedObject as Line;
            if (line == null) return;

            // スナップ点を取得
            var snapPtW = line.StartPoint + line.StartPoint.GetVectorTo(line.EndPoint) / 2;

            result.SnapPoints.Add(snapPtW);
        }

        /// <summary>
        /// ポリラインにスナップ
        /// </summary>
        private void SnapInfoPolyline(ObjectSnapContext context, ObjectSnapInfo result)
        {
            var polyline = context.PickedObject as Polyline;
            if (polyline == null) return;

            // スナップ点を取得
            var snapPtW = polyline.GetPointAtParameter(0.5);

            result.SnapPoints.Add(snapPtW);
        }

        /// <summary>
        /// 寸法にスナップ
        /// </summary>
        private void SnapInfoDimension(ObjectSnapContext context, ObjectSnapInfo result)
        {
            var dim = context.PickedObject as Dimension;
            if (dim == null) return;

            // スナップ点を取得
            var snapPtW = dim.TextPosition;

            result.SnapPoints.Add(snapPtW);
        }

        /// <summary>
        /// 楕円にスナップ
        /// </summary>
        private void SnapInfoEllipse(ObjectSnapContext context, ObjectSnapInfo result)
        {
            var ellipse = context.PickedObject as Ellipse;
            if (ellipse == null) return;

            // スナップ点を取得
            var snapPtW = ellipse.GetPointAtParameter(Angle);

            result.SnapPoints.Add(snapPtW);
        }
    }

    /// <summary>
    /// カスタムオブジェクトスナップ(●＋テキストグリフ)
    /// </summary>
    /// <remarks>
    /// 対象図形：円
    /// </remarks>
    public class SampleCustomOSnapText : SampleCustomOSnap
    {
        /// <summary>グリフの角度</summary>
        private static Vector3d DirW { get; set; } = Vector3d.XAxis;

        // 初期化処理
        private void Initialize()
        {
            if (Mode != null)
            {
                return;
            }

            Glyph = new SampleGlyphText();
            Mode = new CustomObjectSnapMode("SAMPLEOSNAPTEXT", "SAMPLEOSNAPTEXT", "SampleOSnapText", Glyph);

            // 円オブジェクトに新しいコールバックを適用する
            Mode.ApplyToEntityType(RXObject.GetClass(typeof(Circle)), SnapInfoCircle);
        }

        // カスタムオブジェクトスナップの有効/無効を切り替える
        [CommandMethod("JrxCommandTest", "SampleOSnapText", CommandFlags.Transparent)]
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }

                if (CustomObjectSnapMode.IsActive("SAMPLEOSNAPTEXT"))
                {
                    CustomObjectSnapMode.Deactivate("SAMPLEOSNAPTEXT");
                    Util.Editor().WriteMessage("\nSAMPLEOSNAPTEXT OFF");
                }
                else
                {
                    Initialize();
                    CustomObjectSnapMode.Activate("SAMPLEOSNAPTEXT");
                    Util.Editor().WriteMessage("\nSAMPLEOSNAPTEXT ON");
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        /// <summary>カスタムオブジェクトスナップのグリフ</summary>
        protected class SampleGlyphText : SampleGlyph
        {
            protected override void SubViewportDraw(ViewportDraw viewDraw)
            {
                var vport = viewDraw.Viewport;
                var glyphHeight = GetGlyphHeight(vport);

                // 基準点をDCSからWCSに変換
                var posW = BasePtD.TransformBy(vport.EyeToWorldTransform);

                // テキストのグリフの位置調整用ベクトル(DCSに変換して位置調整し、WCSに戻す)
                var dirD = DirW.TransformBy(vport.WorldToEyeTransform);
                var vecD = (dirD.GetNormal() * glyphHeight / 2.0) + (dirD.GetPerpendicularVector().Negate().GetNormal() * glyphHeight / 2.0);
                var vecW = vecD.TransformBy(vport.EyeToWorldTransform);

                // テキストのグリフを表示
                var font = new FontDescriptor("Meiryo", false, false, 0, 0);
                var style = new TextStyle
                {
                    Font = font,
                    TextSize = glyphHeight
                };
                viewDraw.Geometry.Text(posW + vecW, vport.ViewDirection, DirW, "☜", true, style);

                base.SubViewportDraw(viewDraw);
            }
        }

        /// <summary>
        /// 円にスナップ
        /// </summary>
        private void SnapInfoCircle(ObjectSnapContext context, ObjectSnapInfo result)
        {

            var circle = context.PickedObject as Circle;
            if (circle == null) return;

            // スナップ点を取得
            var snapPtW = circle.GetPointAtParameter(Angle);

            var wcsToPlane = Matrix3d.WorldToPlane(circle.Normal);
            result.SnapPoints.Add(snapPtW.TransformBy(wcsToPlane));   // SnapPointsには、OCS座標を入れる。
            DirW = circle.Center.GetVectorTo(snapPtW);
        }
    }
}
