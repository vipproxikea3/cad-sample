﻿#if _IJCAD_
using Application = GrxCAD.ApplicationServices.Application;
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.GraphicsInterface;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;

#endif

using System;
using System.Windows.Forms;
using JrxCad.Helpers;
using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.Command.Sample
{
    public class SampleArc2Cmd : BaseCommand
    {
        [CommandMethod("JrxCommandTest", "SampleArc2", CommandFlags.Modal)]
        public override void OnCommand()
        {
            if (!Init())
            {
                return;
            }

            try
            {
                // ----------------------------------------
                // Editor.Get*系は、UCS座標を返す。
                // ----------------------------------------

                var opt1 = new PromptPointOptions("\n円弧の中心を指定");
                var res1 = Util.Editor().GetPoint(opt1);
                if (res1.Status != PromptStatus.OK) return;
                var centerU = res1.Value;

                var opt2 = new PromptPointOptions("\n円弧の開始点を指定")
                {
                    UseBasePoint = true,
                    BasePoint = centerU,
                };
                var res2 = Util.Editor().GetPoint(opt2);
                if (res2.Status != PromptStatus.OK) return;
                var startU = res2.Value;

                // ----------------------------------------
                // UCSでオブジェクトを生成
                // ----------------------------------------

                var centerToStartU = startU - centerU;
                using (var arc = new Arc(centerU, Vector3d.ZAxis, centerToStartU.Length,
                    centerToStartU.AngleOnPlane(new Plane(Point3d.Origin, Vector3d.ZAxis)), 0))
                {
                    // ----------------------------------------
                    // オブジェクトをWCSに変換
                    // ----------------------------------------

                    arc.TransformBy(CoordConverter.UcsToWcs());

                    // ----------------------------------------
                    // Jigに渡す
                    // ----------------------------------------

                    var accept3dCoordinates = true;
                    var useBasePointElevation = true;
                    PromptResult res3;
                    while (true)
                    {
                        var jig = new AngleJig(arc, accept3dCoordinates, useBasePointElevation);
                        res3 = Util.Editor().Drag(jig);
                        if (res3.Status != PromptStatus.Keyword) break;
                        if (res3.StringResult == "A") accept3dCoordinates ^= true;
                        if (res3.StringResult == "U") useBasePointElevation ^= true;
                    }

                    if (res3.Status == PromptStatus.OK)
                    {
                        using (var tr = Util.StartTransaction())
                        {
                            tr.AddNewlyCreatedDBObject(arc, true, tr.CurrentSpace());
                            tr.Commit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        private class AngleJig : DrawJig
        {
            private Arc _arc;
            private JigPromptAngleOptions _jigOpt;
            private double _dragAng;

            private double _startAng;

            public AngleJig(Arc arc, bool accept3dCoordinates, bool useBasePointElevation)
            {
                // ----------------------------------------
                // Jig内はWCSで扱う
                // ----------------------------------------

                _arc = arc;
                _jigOpt = new JigPromptAngleOptions($"\n円弧の終了角を指定(方向を切り替えるには［Ctrl］を押す)")
                {
                    UseBasePoint = true,
                    BasePoint = arc.Center,
                    UserInputControls =
                        (accept3dCoordinates ? UserInputControls.Accept3dCoordinates : 0) |
                        (useBasePointElevation ? UserInputControls.UseBasePointElevation : 0),
                    Cursor = CursorType.RubberBand,
                    AppendKeywordsToMessage = true,
                    Keywords =
                    {
                        {"A", "A", $"Accept3dCoordinates={(accept3dCoordinates ? "ON" : "OFF")} 切替(A)"},
                        {"U", "U", $"UseBasePointElevation={(useBasePointElevation ? "ON" : "OFF")} 切替(U)"},
                    },
                };

                _startAng = _arc.StartAngle;
            }

            protected override SamplerStatus Sampler(JigPrompts prompts)
            {
                // ----------------------------------------
                // AcquireAngleはUCS角度を返す
                // ----------------------------------------

                var res = prompts.AcquireAngle(_jigOpt);
                // UserInputControls.Accept3dCoordinates
                // ありの場合：_dragPtWは、カーソル位置のUCS平面上の点のワールド座標
                // なしの場合：_dragPtWは、カーソル位置のUCS平面ではなく、OCS平面に垂直投影した点のワールド座標
                _dragAng = res.Value;
                switch (res.Status)
                {
                    case PromptStatus.OK:
                        {
#if true
                            // ----------------------------------------
                            // 一旦UCSにして処理する方法
                            // ----------------------------------------

                            // 一旦UCSにして、UCS上の角度をセットして、WCSに戻す。
                            _arc.TransformBy(CoordConverter.WcsToUcs());
                            _arc.EndAngle = _dragAng;
                            _arc.TransformBy(CoordConverter.UcsToWcs());
#else
                        // ----------------------------------------
                        // WCSのまま処理する方法
                        // ----------------------------------------

                        // WCSのまま、OCS角度をセットする。
                        var vecW = CoordConverter.UcsRotToWcsVec(_dragAng);
                        _arc.EndAngle = CoordConverter.WcsVecToOcsRot(_arc.Normal, vecW);
#endif

                            // Ctrlキーで方向を切替
                            if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
                            {
                                _arc.StartAngle = _arc.EndAngle;
                                _arc.EndAngle = _startAng;
                            }
                            else
                            {
                                _arc.StartAngle = _startAng;
                            }

                            return SamplerStatus.OK;
                        }
                    case PromptStatus.Keyword:
                    case PromptStatus.None:
                        return SamplerStatus.OK;
                    default:
                        return SamplerStatus.Cancel;
                }
            }

            protected override bool WorldDraw(WorldDraw draw)
            {
                draw.Geometry.Draw(_arc);

                // 角度表示
                void DrawText(Point3d pt, double ofs, double ang, string prefix)
                {
                    draw.Geometry.Text(new Point3d(pt.X, pt.Y - 5 - ofs, pt.Z),
                        Vector3d.ZAxis, Vector3d.XAxis, 2.5, 0.75, 0,
                        $"{prefix} {ang:f2} ({ang / Math.PI * 180:f0}°)");
                }

                DrawText(_arc.EndPoint, 0, _dragAng, "U");
                DrawText(_arc.EndPoint, 5, _arc.EndAngle, "O");

                return true;
            }
        }
    }
}
