﻿#if _IJCAD_
using Exception = System.Exception;
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
#elif _AutoCAD_
using Exception = System.Exception;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
#endif
using JrxCad.Utility;
using JrxCad.Helpers;
using System;
using JrxCad.Enum;

// ReSharper disable once CheckNamespace
namespace JrxCad.Command.EllipseCmd
{
    public partial class EllipseCmd : BaseCommand
    {
        #region Enum
        public enum EllipseOpt
        {
            Center = 0,
            StartAngle = 1,
            EndAngle = 2,
            Rotation = 3,
            StartParameter = 4,
            EndParameter = 5,
            IncludedAngle = 6,
            StartParameterEndIncludedAngle = 7,
            Radius = 8,
            Diameter = 9,
            Invalid = 10,
        }
        #endregion

        #region Declare

        /// <summary>
        /// _isCenterEndPoint, true if first endpoint is a center endpoint, otherwise false, default false
        /// </summary>
        private bool _isCenterEndPt;

        /// <summary>
        /// _isEllipseArc, true if select elliptical arc, otherwise false, default false
        /// </summary>
        private bool _isEllipseArc;

        /// <summary>
        /// _useParameter, true if select elliptical arc with parameter, otherwise false, default false
        /// </summary>
        private bool _useParameter;

        /// <summary>
        /// _isIsoCircle, true if select IsoCircle, otherwise false, default false
        /// </summary>
        private bool _isIsocircle;

        /// <summary>
        /// This variable is to save the default value of ANGBASE.
        /// </summary>
        private double _defAngBase;

        /// <summary>
        /// This variable is to save the value of SNAPSTYL.
        /// </summary>
        private int _defSnapStyl;

        /// <summary>
        /// This variable is to save the value of SNAPISOPAIR.
        /// </summary>
        private int _defSnapIsoPair;

        /// <summary>
        /// This variable is to save the value of PELLIPSE.
        /// </summary>
        private int _defPellipse;
        #endregion

        #region Constructor
        public EllipseCmd() : this(new UserInput()) { }
        public EllipseCmd(IEllipseUserInput userInput)
        {
            _userInput = userInput;
        }

        /// <summary>
        /// Init Default Global variable
        /// </summary>
        private void DefaultVariable()
        {
            _isCenterEndPt = false;
            _isEllipseArc = false;
            _isIsocircle = false;
            _useParameter = false;
            _defAngBase = SystemVariable.GetDouble("ANGBASE");
            _defSnapStyl = SystemVariable.GetInt("SNAPSTYL");
            _defSnapIsoPair = SystemVariable.GetInt("SNAPISOPAIR");
            _defPellipse = SystemVariable.GetInt("PELLIPSE");
        }
        #endregion

        #region Override
#if _AutoCAD_
        [CommandMethod("JrxCommand", "SmxELLIPSE", CommandFlags.Modal | CommandFlags.NoNewStack)]
#elif _IJCAD_
        [CommandMethod("JrxCommand", "SmxELLIPSE", CommandFlags.Modal | CommandFlags.NoNewStack | CommandFlags.Session)]
#endif
        public override void OnCommand()
        {
            try
            {
                if (!Init()) return;

                DefaultVariable();

                if (GetFirstPoint(out var firstPoint) == ProcessFlag.CancelOrDefault) return;
                if (GetSecondEndpoint(firstPoint, out var secondPoint) == ProcessFlag.CancelOrDefault) return;

                using (var ellipse = GetEllipse(firstPoint, secondPoint))
                {
                    ellipse.SetDatabaseDefaults();

                    // Transform UCS to WCS
                    ellipse.TransformBy(CoordConverter.UcsToWcs());

                    if (_isIsocircle)
                    {
                        if (GetRadiusOrDiameter(ellipse) == ProcessFlag.CancelOrDefault) return;
                    }
                    else
                    {
                        if (GetDistanceOrRotation(ellipse) == ProcessFlag.CancelOrDefault) return;
                    }

                    if (_isEllipseArc)
                    {
                        var newAngBase = Util.Ucsxdir().GetAngleTo(ellipse.MajorAxis, CoordConverter.UcsZAxis());
                        using (new SystemVariable.Temp("ANGBASE", newAngBase))
                        {
                            if (GetArcStartAngle(ellipse) == ProcessFlag.CancelOrDefault || GetArcEndAngle(ellipse) == ProcessFlag.CancelOrDefault) return;
                        }
                    }
                    //Add ellipse to DB
                    if (_isCenterEndPt)
                    {
                        AddEllipseIntoDatabase(ellipse, secondPoint.TransformBy(CoordConverter.UcsToWcs()));
                    }
                    else
                    {
                        AddEllipseIntoDatabase(ellipse, firstPoint.TransformBy(CoordConverter.UcsToWcs()));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }
        #endregion

        #region Get P1
        /// <summary>
        /// Get first end point or center of ellipse
        /// </summary>
        private ProcessFlag GetFirstPoint(out Point3d firstPoint)
        {
            firstPoint = default;
            var message = (_defPellipse == (int)PEllipse.PolylineEllipse && _defSnapStyl == (int)SnapStyl.Rectangular) ? EllipseRes.UI_P1_Poly : EllipseRes.UI_P1_Eps;
            var firstPointOptions = new PromptPointOptions(message);

            if (_defPellipse == (int)PEllipse.TrueEllipse)
            {
                firstPointOptions.Keywords.Add("A", EllipseRes.KW3, EllipseRes.KW3);
            }

            firstPointOptions.Keywords.Add("C", EllipseRes.KW1, EllipseRes.KW1);

            if (_defSnapStyl == (int)SnapStyl.Isometric)
            {
                firstPointOptions.Keywords.Add("I", EllipseRes.KW2, EllipseRes.KW2);
            }

            while (true)
            {
                var firstPointResult = _userInput.GetPoint(firstPointOptions);

                switch (firstPointResult.Status)
                {
                    case PromptStatus.OK:
                        firstPoint = firstPointResult.Value;
                        return ProcessFlag.Finished;
                    case PromptStatus.Keyword:
                        switch (firstPointResult.StringResult)
                        {
                            case "A":
                                _isEllipseArc = true;
                                firstPointOptions = new PromptPointOptions($"\n{EllipseRes.UI_PA1}")
                                {
                                    Keywords =
                                    {
                                        {"C", EllipseRes.KW1, EllipseRes.KW1},
                                    },
                                };

                                if (_defSnapStyl == (int)SnapStyl.Isometric)
                                {
                                    firstPointOptions.Keywords.Add("I", EllipseRes.KW2, EllipseRes.KW2);
                                }

                                break;
                            case "C":
                                _isCenterEndPt = true;
                                message = _isEllipseArc ? $"\n{EllipseRes.UI_PAC1}" : $"\n{EllipseRes.UI_PC1}";

                                firstPointOptions = new PromptPointOptions(message);
                                break;
                            case "I":
                                _isIsocircle = true;
                                firstPointOptions = new PromptPointOptions($"\n{EllipseRes.UI_IsoCircle1}");
                                break;
                        }
                        break;
                    default:
                        return ProcessFlag.CancelOrDefault;
                }
            }
        }
        #endregion

        #region Get P2
        /// <summary>
        /// Get (the other) endpoint of ellipse
        /// </summary>
        private ProcessFlag GetSecondEndpoint(Point3d basePoint, out Point3d resPoint)
        {
            resPoint = default;
            if (_isIsocircle)
            {
                resPoint = new Point3d(basePoint.X + Const.IsometricRatio, basePoint.Y, basePoint.Z);
                return ProcessFlag.Finished;
            }

            var basePointWcs = basePoint.TransformBy(CoordConverter.UcsToWcs());
            var message = _isCenterEndPt ? $"\n{EllipseRes.UI_PC2}" : $"\n{ EllipseRes.UI_P2}";
            var optSecondEndPt = new PointOpts()
            {
                Options = new JigPromptPointOptions(message)
                {
                    BasePoint = basePointWcs,
                    UseBasePoint = true,
                    Cursor = CursorType.RubberBand,
                    UserInputControls = UserInputControls.UseBasePointElevation
                }
            };

            while (true)
            {
                var resSecondEndPt = _userInput.AcquirePoint(optSecondEndPt);
                if (resSecondEndPt.Status != PromptStatus.OK) return ProcessFlag.CancelOrDefault;
                resPoint = optSecondEndPt.LastValue.TransformBy(CoordConverter.WcsToUcs());

                resPoint = new Point3d(resPoint.X, resPoint.Y, basePoint.Z);
                if (basePoint.IsEqualTo(resPoint)) continue;
                return ProcessFlag.Finished;
            }
        }
        #endregion

        #region Get P3
        /// <summary>
        /// Get distance to ellipse center or get rotation to calculate the distance to the center
        /// </summary>
        private ProcessFlag GetDistanceOrRotation(Ellipse ellipse)
        {
            var initialMajorAxis = ellipse.MajorAxis;

            var optDistance = new DistanceOpts(ellipse, EllipseOpt.Center, initialMajorAxis)
            {
                Options = new JigPromptDistanceOptions($"\n{EllipseRes.UI_P3}")
                {
                    UseBasePoint = true,
                    BasePoint = ellipse.Center,
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.UseBasePointElevation |
                                        UserInputControls.NoNegativeResponseAccepted |
                                        UserInputControls.NoZeroResponseAccepted,
                    Cursor = CursorType.RubberBand,
                    Keywords =
                    {
                        { "R", EllipseRes.KW4, EllipseRes.KW4 },
                    },
                }
            };

            while (true)
            {
                var resLength = _userInput.AcquireDistance(optDistance);
                switch (resLength.Status)
                {
                    case PromptStatus.OK:
                        if (optDistance.IsDraw) return ProcessFlag.Finished;

                        WriteMessage(EllipseRes.ER1);
                        return ProcessFlag.CancelOrDefault;
                    case PromptStatus.Keyword:
                        var optRotation = new AngleOpts(ellipse, EllipseOpt.Rotation, initialMajorAxis)
                        {
                            Options = new JigPromptAngleOptions($"\n{EllipseRes.UI_P4}")
                            {
                                UseBasePoint = true,
                                BasePoint = ellipse.Center,
                                Cursor = CursorType.RubberBand,
                            }
                        };
#if _IJCAD_
                        //TODO: IJCAD, AcquireAngle required defaultValue -> default value was append in message
                        optRotation.Options.DefaultValue = 0.0;
#endif

                        var resRotation = _userInput.AcquireAngle(optRotation);
                        if (resRotation.Status != PromptStatus.OK) return ProcessFlag.Finished;
                        if (optRotation.IsDraw) return ProcessFlag.Finished;

                        WriteMessage(EllipseRes.ER1);
                        return ProcessFlag.CancelOrDefault;
                    default:
                        return ProcessFlag.CancelOrDefault;
                }
            }
        }
        #endregion

        #region Get P4
        /// <summary>
        /// Get radius or diameter
        /// </summary>
        private ProcessFlag GetRadiusOrDiameter(Ellipse ellipse)
        {
            var initialMajorAxis = ellipse.MajorAxis;

            var optRadius = new DistanceOpts(ellipse, EllipseOpt.Radius, initialMajorAxis)
            {
                Options = new JigPromptDistanceOptions($"\n{EllipseRes.UI_IsoCircle2}")
                {
                    UseBasePoint = true,
                    BasePoint = ellipse.Center,
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.UseBasePointElevation |
                                        UserInputControls.NoNegativeResponseAccepted |
                                        UserInputControls.NoZeroResponseAccepted,
                    Cursor = CursorType.RubberBand,
                    Keywords =
                    {
                        { "D", EllipseRes.KW5, EllipseRes.KW5 },
                    },
                }
            };

            while (true)
            {
                var resRadius = _userInput.AcquireDistance(optRadius);
                switch (resRadius.Status)
                {
                    case PromptStatus.OK:
                        return ProcessFlag.Finished;
                    case PromptStatus.Keyword:
                        var optDiameter = new DistanceOpts(ellipse, EllipseOpt.Diameter, initialMajorAxis)
                        {
                            Options = new JigPromptDistanceOptions($"\n{EllipseRes.UI_IsoCircle3}")
                            {
                                UseBasePoint = true,
                                BasePoint = ellipse.Center,
                                UserInputControls = UserInputControls.Accept3dCoordinates |
                                                UserInputControls.UseBasePointElevation |
                                                UserInputControls.NoNegativeResponseAccepted |
                                                UserInputControls.NoZeroResponseAccepted,
                                Cursor = CursorType.RubberBand,
                            }
                        };
                        var resDiameter = _userInput.AcquireDistance(optDiameter);
                        if (resDiameter.Status != PromptStatus.OK) return ProcessFlag.CancelOrDefault;
                        return ProcessFlag.Finished;
                    default:
                        return ProcessFlag.CancelOrDefault;
                }
            }
        }
        #endregion

        #region Get angel
        /// <summary>
        /// Process to get start angle or options to get start angle
        /// </summary>
        private ProcessFlag GetArcStartAngle(Ellipse ellipse)
        {
            var optArc = new AngleOpts(ellipse, EllipseOpt.StartAngle)
            {
                Options = new JigPromptAngleOptions()
                {
                    UseBasePoint = true,
                    BasePoint = ellipse.Center,
                    UserInputControls = UserInputControls.Accept3dCoordinates,
                    Cursor = CursorType.RubberBand,
                }
            };
#if _IJCAD_
            //TODO: IJCAD, AcquireAngle required defaultValue -> default value was append in message
            optArc.Options.DefaultValue = 0.0;
#endif
            while (true)
            {
                optArc.Options.Keywords.Clear();

                if (!_useParameter)
                {
                    optArc.SetEllipseOpt(EllipseOpt.StartAngle);
                    optArc.Options.Message = $"\n{EllipseRes.UI_StartAng}";
                    optArc.Options.Keywords.Add("P", EllipseRes.KW6);
                }
                else
                {
                    optArc.SetEllipseOpt(EllipseOpt.StartParameter);
                    optArc.Options.Message = $"\n{EllipseRes.UI_StartPara}";
                    optArc.Options.Keywords.Add("A", EllipseRes.KW7);
                }

                var resAngleOrParameter = _userInput.AcquireAngle(optArc);
                switch (resAngleOrParameter.Status)
                {
                    case PromptStatus.OK:
                        return ProcessFlag.Finished;
                    case PromptStatus.Keyword:
                        _useParameter = resAngleOrParameter.StringResult.Equals("P");
                        break;
                    default:
                        return ProcessFlag.CancelOrDefault;
                }
            }
        }

        /// <summary>
        /// Process to get end angle or options to get end angle
        /// </summary>
        private ProcessFlag GetArcEndAngle(Ellipse ellipse)
        {
            var optArc = new AngleOpts(ellipse, EllipseOpt.EndAngle)
            {
                Options = new JigPromptAngleOptions($"\n{EllipseRes.UI_EndAng}")
                {
                    UseBasePoint = true,
                    BasePoint = ellipse.Center,
                    UserInputControls = UserInputControls.Accept3dCoordinates,
                    Cursor = CursorType.RubberBand,
                }
            };
#if _IJCAD_
            //TODO: IJCAD, AcquireAngle required defaultValue -> default value was append in message
            optArc.Options.DefaultValue = 0.0;
#endif

            while (true)
            {
                optArc.Options.Keywords.Clear();

                if (!_useParameter)
                {
                    optArc.Options.Message = $"\n{EllipseRes.UI_EndAng}";
                    optArc.Options.Keywords.Add("P", EllipseRes.KW6, EllipseRes.KW6);
                    optArc.Options.Keywords.Add("I", EllipseRes.KW8, EllipseRes.KW8);
                    optArc.SetEllipseOpt(EllipseOpt.EndAngle);
                }
                else
                {
                    optArc.Options.Message = $"\n{EllipseRes.UI_EndPara}";
                    optArc.Options.Keywords.Add("A", EllipseRes.KW7, EllipseRes.KW7);
                    optArc.Options.Keywords.Add("I", EllipseRes.KW8, EllipseRes.KW8);
                    optArc.SetEllipseOpt(EllipseOpt.EndParameter);
                }

                var resAngleOrParameter = _userInput.AcquireAngle(optArc);

                switch (resAngleOrParameter.Status)
                {
                    case PromptStatus.OK:
                        return ProcessFlag.Finished;
                    case PromptStatus.Keyword:
                        if (resAngleOrParameter.StringResult.Equals("I")) return IncludedAngleOpt(ellipse);

                        _useParameter = resAngleOrParameter.StringResult.Equals("P");
                        break;
                    default:
                        return ProcessFlag.CancelOrDefault;
                }
            }
        }

        /// <summary>
        /// Get included angle of elliptical arc
        /// </summary>
        private ProcessFlag IncludedAngleOpt(Ellipse ellipse)
        {
            var optArc = new AngleOpts(ellipse)
            {
                Options = new JigPromptAngleOptions()
                {
                    DefaultValue = Math.PI,
                    UseBasePoint = true,
                    BasePoint = ellipse.Center,
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.NullResponseAccepted,
                    Cursor = CursorType.RubberBand,
                }
            };

            if (_useParameter)
            {
                optArc.Options.Message = $"\n{EllipseRes.UI_IncPara}";
                optArc.SetEllipseOpt(EllipseOpt.StartParameterEndIncludedAngle);
            }
            else
            {
                optArc.Options.Message = $"\n{EllipseRes.UI_IncAng}";
                optArc.SetEllipseOpt(EllipseOpt.IncludedAngle);
            }

            using (new SystemVariable.Temp("ANGBASE", _defAngBase))
            {
                var resAngle = _userInput.AcquireAngle(optArc);
                if (resAngle.Status == PromptStatus.OK || resAngle.Status == PromptStatus.None) return ProcessFlag.Finished;
                return ProcessFlag.CancelOrDefault;
            }
        }
        #endregion

        #region Create Ellipse
        /// <summary>
        /// Create raw ellipse
        /// </summary>
        private Ellipse GetEllipse(Point3d firstPoint, Point3d secondPoint)
        {
            Point3d centerPoint;
            Vector3d majorAxisU;
            double ratio = 1;
            double angleOfMajorAxis = 0;

            if (_isIsocircle)
            {
                switch (_defSnapIsoPair)
                {
                    case (int)SnapIsoPair.Left:
                        angleOfMajorAxis = 2 * Math.PI / 3;
                        break;
                    case (int)SnapIsoPair.Top:
                        angleOfMajorAxis = 0;
                        break;
                    case (int)SnapIsoPair.Right:
                        angleOfMajorAxis = Math.PI / 3;
                        break;
                }

                centerPoint = firstPoint;
                ratio = Math.Tan(Math.PI / 6);

                var line = new Line(centerPoint, secondPoint);
                line.TransformBy(CoordConverter.UcsToWcs());
                line.TransformBy(Matrix3d.Rotation(angleOfMajorAxis, CoordConverter.UcsZAxis(), centerPoint));
                line.TransformBy(CoordConverter.WcsToUcs());
                majorAxisU = line.StartPoint - line.EndPoint;
            }
            else if (_isCenterEndPt)
            {
                centerPoint = firstPoint;
                majorAxisU = secondPoint - centerPoint;
            }
            else
            {
                centerPoint = firstPoint.CenterPoint(secondPoint);
                majorAxisU = centerPoint - secondPoint;
            }

            return new Ellipse(centerPoint, Vector3d.ZAxis, majorAxisU, ratio, 0, 2 * Math.PI);
        }

        /// <summary>
        /// Add Ellipse into Database
        /// </summary>
        private void AddEllipseIntoDatabase(Entity ellipse, Point3d firstOrCenterPtW)
        {
            using (var tr = Util.StartTransaction())
            {
                if (_defPellipse == (int)PEllipse.PolylineEllipse)
                {
                    ellipse = (ellipse as Ellipse).ToPolyline(firstOrCenterPtW).ConvertTo(false);
                    ellipse.SetDatabaseDefaults();
                    ellipse.LineWeight = (LineWeight)SystemVariable.GetInt("CELWEIGHT");
                    (ellipse as Polyline2d).Thickness = SystemVariable.GetDouble("THICKNESS");
                }
                ellipse.LinetypeScale = SystemVariable.GetDouble("CELTSCALE");
                tr.AddNewlyCreatedDBObject(ellipse, true, tr.CurrentSpace());
                _userInput.AppendEllipseId(ellipse.Id);
                tr.Commit();
            }
        }
        #endregion
    }
}
