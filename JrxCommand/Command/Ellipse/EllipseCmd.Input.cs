﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;
using JrxCad.Helpers;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;
using JrxCad.Helpers;

#endif
using JrxCad.Utility;
using System;

namespace JrxCad.Command.EllipseCmd
{
    public partial class EllipseCmd
    {
        private readonly IEllipseUserInput _userInput;

        /// <summary>
        /// IUserInput Interface
        /// </summary>
        public interface IEllipseUserInput : IUserInputBase
        {
            void AppendEllipseId(ObjectId id);
        }

        /// <summary>
        /// UserInputImpl
        /// </summary>
        private class UserInput : UserInputBase, IEllipseUserInput
        {
            public void AppendEllipseId(ObjectId id) { }
        }

        public class PointOpts : PointDrawJigN.JigActions
        {
            public override JigPromptPointOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                return true;
            }

            public override void OnUpdate()
            {
            }
        }

        public class DistanceOpts : DistanceDrawJigN.JigActions
        {
            private readonly double _minRatio = 0.01;
            private readonly Ellipse _ellipse;
            private Vector3d _centerToStart;
            private readonly EllipseOpt _option;
            public bool IsDraw { get; set; }

            public DistanceOpts(Ellipse ellipse, EllipseOpt option, Vector3d majorAxis)
            {
                _ellipse = ellipse;
                _option = option;
                _centerToStart = majorAxis;
                IsDraw = true;
            }

            public override JigPromptDistanceOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                if (_option == EllipseOpt.Invalid)
                {
                    IsDraw = false;
                    return IsDraw;
                }
                return geometry.Draw(_ellipse);
            }

            /// <summary>
            /// Update MajorAxis and RatioRadius before drawing ellipse
            /// </summary>
            public override void OnUpdate()
            {
                Vector3d majorTemp;
                switch (_option)
                {
                    case EllipseOpt.Center:
                        var ratio = Math.Min(LastValue, _centerToStart.Length) / Math.Max(LastValue, _centerToStart.Length);
                        if (ratio < _minRatio)
                        {
                            IsDraw = false;
                            return;
                        }

                        IsDraw = true;
                        if (LastValue > _centerToStart.Length)
                        {
                            majorTemp = _centerToStart.RotateBy(Math.PI / 2, CoordConverter.UcsZAxis());
                            _ellipse.Set(_ellipse.Center, CoordConverter.UcsZAxis(), majorTemp / ratio, ratio, 0, Math.PI * 2);
                        }
                        else
                        {
                            _ellipse.Set(_ellipse.Center, CoordConverter.UcsZAxis(), _centerToStart, ratio, 0, Math.PI * 2);
                        }
                        break;
                    case EllipseOpt.Radius:
                        majorTemp = _centerToStart * LastValue;
                        if (majorTemp.Length < Const.EpsValue)
                        {
                            IsDraw = false;
                            return;
                        }

                        IsDraw = true;
                        _ellipse.Set(_ellipse.Center, CoordConverter.UcsZAxis(), majorTemp, _ellipse.RadiusRatio, 0, Math.PI * 2);
                        break;
                    case EllipseOpt.Diameter:
                        majorTemp = _centerToStart * LastValue / 2;
                        if (majorTemp.Length < Const.EpsValue)
                        {
                            IsDraw = false;
                            return;
                        }

                        IsDraw = true;
                        _ellipse.Set(_ellipse.Center, CoordConverter.UcsZAxis(), majorTemp, _ellipse.RadiusRatio, 0, Math.PI * 2);
                        break;
                }
            }
        }

        public class AngleOpts : AngleDrawJigN.JigActions
        {
            private readonly Ellipse _ellipse;
            private EllipseOpt _option;
            private Vector3d _majorAxis;
            private readonly double _startAngle;
            /// <summary>
            /// not accept 89.4 degree ~ 90.6 degree
            /// </summary>
            private readonly double _minCosValueAccept = 0.0104;
            public bool IsDraw { get; set; }

            /// <summary>
            /// Use for Rotation case
            /// </summary>
            public AngleOpts(Ellipse ellipse, EllipseOpt option, Vector3d majorAxis)
            {
                _ellipse = ellipse;
                _option = option;
                _majorAxis = majorAxis;
                IsDraw = true;
                _startAngle = ellipse.StartAngle;
            }

            public AngleOpts(Ellipse ellipse, EllipseOpt option)
            {
                _ellipse = ellipse;
                _option = option;
                IsDraw = true;
                _startAngle = ellipse.StartAngle;
            }
            public AngleOpts(Ellipse ellipse)
            {
                _ellipse = ellipse;
                IsDraw = true;
                _startAngle = ellipse.StartAngle;
            }
            public void SetEllipseOpt(EllipseOpt option)
            {
                _option = option;
            }

            public override JigPromptAngleOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                if (_option == EllipseOpt.Invalid)
                {
                    IsDraw = false;
                    return IsDraw;
                }
                return geometry.Draw(_ellipse);
            }

            public override void OnUpdate()
            {
                double endAngle;
                switch (_option)
                {
                    case EllipseOpt.Rotation:
                        if (Math.Abs(Math.Cos(LastValue)) > _minCosValueAccept)
                        {
                            IsDraw = true;
                            var ratio = Math.Abs(Math.Cos(LastValue));
                            _ellipse.Set(_ellipse.Center, CoordConverter.UcsZAxis(), _majorAxis, ratio, _ellipse.StartAngle, _ellipse.EndAngle);
                        }
                        else
                        {
                            IsDraw = false;
                        }
                        break;
                    case EllipseOpt.StartAngle:
                        _ellipse.Set(_ellipse.Center, _ellipse.Normal, _ellipse.MajorAxis, _ellipse.RadiusRatio, LastValue, LastValue + Math.PI * 2);

                        break;
                    case EllipseOpt.EndAngle:
                        _ellipse.Set(_ellipse.Center, _ellipse.Normal, _ellipse.MajorAxis, _ellipse.RadiusRatio, _startAngle, LastValue);
                        break;
                    case EllipseOpt.StartParameterEndIncludedAngle:
                        var startParameter = Math.Atan2(Math.Sin(_ellipse.StartAngle), Math.Cos(_ellipse.StartAngle) * _ellipse.RadiusRatio);
                        endAngle = Math.Atan2(Math.Sin(LastValue + startParameter) * _ellipse.RadiusRatio, Math.Cos(LastValue + startParameter));
                        _ellipse.Set(_ellipse.Center, _ellipse.Normal, _ellipse.MajorAxis, _ellipse.RadiusRatio, _startAngle, endAngle);
                        break;
                    case EllipseOpt.IncludedAngle:
                        _ellipse.Set(_ellipse.Center, _ellipse.Normal, _ellipse.MajorAxis, _ellipse.RadiusRatio, _startAngle, _startAngle + LastValue);
                        break;
                    case EllipseOpt.StartParameter:
                        var startAngle = Math.Atan2(Math.Sin(LastValue) * _ellipse.RadiusRatio, Math.Cos(LastValue));
                        _ellipse.Set(_ellipse.Center, _ellipse.Normal, _ellipse.MajorAxis, _ellipse.RadiusRatio, startAngle, startAngle + (Math.PI * 2));
                        break;
                    case EllipseOpt.EndParameter:
                        endAngle = Math.Atan2(Math.Sin(LastValue) * _ellipse.RadiusRatio, Math.Cos(LastValue));
                        endAngle = endAngle < 0 ? Math.PI * 2 + endAngle : endAngle;
                        _ellipse.Set(_ellipse.Center, _ellipse.Normal, _ellipse.MajorAxis, _ellipse.RadiusRatio, _startAngle, endAngle);
                        break;
                }
            }
        }
    }
}
