using JrxCad.Utility;
using JrxCad.Enum;

namespace JrxCad.Command
{
    /// <summary>
    /// 基底コマンドクラス
    /// </summary>
    /// <remarks>
    /// コマンドを作成する際は、必ず当クラスを継承してください。
    /// </remarks>
    public abstract class BaseCommand
    {
        private string _commandName;
        private bool _isLispAndCmdZeroEcho;

        private bool _isMinusCmd;
        private int _orgFileDiaValue;

        protected enum ProcessFlag
        {
            CancelOrDefault = -1,
            Finished = 1
        }

        /// <summary>
        /// LISP起動＆CMDECHO変数==0
        /// </summary>
        protected bool IsLispAndNoCmdEcho
        {
            get
            {
                if ((SystemVariable.GetInt("CMDACTIVE") & (int)ActiveCommand.AutoLISP) == (int)ActiveCommand.AutoLISP && SystemVariable.GetInt("CMDECHO") == (int)Echo.Off)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Write a message to the command line when lisp start and CMDECHO = 0.
        /// </summary>
        public void WriteMessage(string cMessage, params object[] parameter)
        {
            if (!IsLispAndNoCmdEcho)
            {
                Util.Editor().WriteMessage($"\n{cMessage}", parameter);
            }
        }

        /// <summary>
        /// 初期化
        /// </summary>
        protected virtual bool Init()
        {
            _commandName = SystemVariable.GetString("CmdNames");

            Logger.Info($"{_commandName} Command Start");

            _isLispAndCmdZeroEcho = (SystemVariable.GetInt("CMDACTIVE") & (int)ActiveCommand.AutoLISP) == (int)ActiveCommand.AutoLISP
                                    && SystemVariable.GetInt("CMDECHO") == (int)Echo.Off;

            _isMinusCmd = _commandName.StartsWith("-");
            DoChangeSysVar();

            //TODO:コマンドごとのグレード判定処理追加
            //if (!GradeCheck(_commandName)) return false;

            return true;
        }

        /// <summary>
        /// 終了処理
        /// </summary>
        protected virtual void Exit()
        {
            DoRecoverSysVar();
            Logger.Info($"{_commandName} Command End");
        }

        /// <summary>
        /// Editor.GetFileNameForSave(options)を使う時、
        /// マイナスコマンドの時でも、FILEDIAによってダイアログが表示されるケースがある。
        /// そのため、_isLispAndCmdZeroEcho && _isMinusCmd = trueの時、FILEDIAを強制的に０にして、
        /// コマンド終了時に元の値（_orgFileDiaValue）に戻す。
        /// </summary>
        private void DoChangeSysVar()
        {
            if (!_isLispAndCmdZeroEcho || !_isMinusCmd)
            {
                return;
            }
            _orgFileDiaValue = SystemVariable.GetInt("FILEDIA");
            SystemVariable.SetValue("FILEDIA", 0);
        }
        /// <summary>
        /// コマンド終了時に元の値（_orgFileDiaValue）に戻す。
        /// </summary>
        private void DoRecoverSysVar()
        {
            if (!_isLispAndCmdZeroEcho || !_isMinusCmd)
            {
                return;
            }
            SystemVariable.SetValue("FILEDIA", _orgFileDiaValue);
        }

        /// <summary>
        /// コマンドの実行
        /// </summary>
        public abstract void OnCommand();

        /// <summary>
        /// Write message
        /// </summary>
        protected void WriteMessageFollowLispAndCmdEcho(string cMessage, params object[] parameter)
        {
            if (!_isLispAndCmdZeroEcho)
            {
                Util.Editor().WriteMessage(cMessage, parameter);
            }
        }
    }
}
