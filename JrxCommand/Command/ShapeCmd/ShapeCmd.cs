﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

#endif
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JrxCad.FileIO;
using JrxCad.Utility;
using JrxCad.Helpers;
using Exception = System.Exception;

namespace JrxCad.Command.ShapeCmd
{
    public partial class ShapeCmd : BaseCommand
    {
        public ShapeCmd() : this(new UserInput())
        {
        }

        public ShapeCmd(IShapeUserInput userInput)
        {
            _userInput = userInput;
        }

#if DEBUG
        [CommandMethod("JrxCommand", "SH", CommandFlags.Modal | CommandFlags.NoNewStack)]
#else
        [CommandMethod("JrxCommand", "SmxShape", CommandFlags.Modal | CommandFlags.NoNewStack)]
#endif

        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }

                var shapeRecords = GetShapeRecords();
                var shpName = SystemVariable.GetString("SHPNAME");

                var targetShapeItem = GetShapeName(shapeRecords, shpName);
                if (targetShapeItem == null) return;

                if (!CreateShape(targetShapeItem)) return;

                SystemVariable.SetString("SHPNAME", targetShapeItem.Name);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        private class ShapeItem
        {
            public string Name { get; set; }
            public ObjectId TextStyleId { get; set; }
        }

        private class ShapeFileItem
        {
            public string FileName { get; set; }
            public ObjectId TextStyleId { get; set; }
            public List<string> Names { get; set; }
        }

        /// <summary>
        /// Get All Shape Records From TextStyleTable
        /// </summary>
        private List<ShapeFileItem> GetShapeRecords()
        {
            var shapeRecords = new List<ShapeFileItem>();
            using (var tr = Util.StartTransaction())
            using (var textStyleTable = tr.GetObject<TextStyleTable>(Util.Database().TextStyleTableId, OpenMode.ForRead))
            {
                foreach (var id in textStyleTable)
                {
                    var textStyle = tr.GetObject<TextStyleTableRecord>(id, OpenMode.ForRead);
                    if (textStyle.IsShapeFile)
                    {
                        //// シェイプ一覧抽出
                        var fileName = textStyle.FileName;
                        if (File.Exists(fileName))
                        {
                            Collect(shapeRecords, fileName, textStyle);
                        }
                        else
                        {
                            // Check Current DWG Path Has SHX File
                            try
                            {
                                var name = fileName.Substring(fileName.LastIndexOf('\\') + 1);
                                fileName = HostApplicationServices.Current.FindFile(name, Util.Database(), FindFileHint.Default);
                                Collect(shapeRecords, fileName, textStyle);
                            }
                            catch
                            {
                                // ignored
                            }
                        }
                    }
                }
            }
            return shapeRecords;
        }

        /// <summary>
        /// Collect Shapes From SHX
        /// </summary>
        private void Collect(List<ShapeFileItem> shapeRecords, string fileName, TextStyleTableRecord textStyle)
        {
            var tool = new ShapeFileTool();
            if (tool.GetShapeNameList(fileName, out var shapeNames))
            {
                shapeRecords.Add(new ShapeFileItem
                {
                    FileName = textStyle.FileName,
                    TextStyleId = textStyle.ObjectId,
                    Names = shapeNames
                });
            }
        }

        /// <summary>
        /// Find Shape In List Records
        /// </summary>
        private ShapeItem GetShapeName(List<ShapeFileItem> shapeRecords, string defaultShapeName)
        {
            var opt = new PromptStringOptions($"\n{ShapeRes.UI_P1}")
            {
                UseDefaultValue = true,
                DefaultValue = string.IsNullOrEmpty(defaultShapeName) ? null : defaultShapeName,
            };

            while (true)
            {
                // シェイプ名を入力 または [一覧(?)]
                var res = _userInput.GetString(opt);
                if (res.Status != PromptStatus.OK)
                {
                    return null;
                }

                if (string.IsNullOrEmpty(res.StringResult))
                {
                    WriteMessage($"{ShapeRes.CM_ER2}");
                    continue;
                }

                if (res.StringResult == "?")
                {
                    PromptList(shapeRecords);
                    // "?"から一覧表示した後は終了
                    return null;
                }

                var record = shapeRecords.Find(a =>
                        a.Names.Any(b =>
                            b.Equals(res.StringResult, StringComparison.CurrentCultureIgnoreCase)));
                if (record == null || string.IsNullOrEmpty(record.FileName))
                {
                    WriteMessage($"{ShapeRes.CM_ER1}", res.StringResult.ToUpper());
                    continue;
                }

                return new ShapeItem
                {
                    Name = res.StringResult.ToUpper(),
                    TextStyleId = record.TextStyleId
                };
            }
        }

        /// <summary>
        /// Print List Shape
        /// </summary>
        private void PromptList(List<ShapeFileItem> shapeRecords)
        {
            // 一覧表示するシェイプ名を入力 <*>
            var opt2 = new PromptStringOptions($"\n{ShapeRes.UI_P5}")
            {
                UseDefaultValue = true,
                DefaultValue = "*"
            };

            var res2 = _userInput.GetString(opt2);
            if (res2.Status != PromptStatus.OK)
            {
                return;
            }

            var found = false;
            WriteMessage($"\n{ShapeRes.CM_MES1}");

            foreach (var shapeRecord in shapeRecords)
            {
                WriteMessage($"\n{ShapeRes.CM_MES5}: {shapeRecord.FileName}");
                if (res2.StringResult == "*")
                {
                    foreach (var shapeName in shapeRecord.Names)
                    {
                        WriteMessage($"\n  {shapeName}");
                    }
                    found = true;
                }
                else
                {
                    if (shapeRecord.Names.Any(shape =>
                            string.Compare(shape, res2.StringResult, StringComparison.CurrentCultureIgnoreCase) == 0))
                    {
                        WriteMessage($"\n  {res2.StringResult.ToUpper()}");
                        found = true;
                    }
                }
            }

            if (!found)
            {
                WriteMessage($"\n  {ShapeRes.CM_MES2}");
            }
        }

        /// <summary>
        /// Create Shape W InsertPoint, Scale, Rotation
        /// </summary>
        private bool CreateShape(ShapeItem target)
        {
            using (var tr = Util.StartTransaction())
            {
                var shape = new Shape(Point3d.Origin, 1.0, 0.0, 1.0);
                shape.SetDatabaseDefaults(Util.Database());
                shape.StyleId = target.TextStyleId;
                shape.Thickness = SystemVariable.GetDouble("THICKNESS");
                shape.TransformBy(CoordConverter.UcsToWcs());

                // Nameを設定しておかないとjigでプレビュー表示されない。
                // また、BlockTableRecordに登録してからでないと例外が発生する。理由は不明
                tr.AddNewlyCreatedDBObject(shape, true, tr.CurrentSpace());
                shape.Name = target.Name;

                // 挿入点を指定
                {
                    var jig = new ShapePointJigActions(shape);
                    var drag = _userInput.AcquirePoint(jig);
                    var getElevation = SystemVariable.GetDouble("ELEVATION");
                    var dragRes = shape.Position.TransformBy(CoordConverter.WcsToUcs());
                    if (!getElevation.IsEqual(dragRes.Z))
                    {
                        shape.Position = new Point3d(dragRes.X, dragRes.Y, getElevation).TransformBy(CoordConverter.UcsToWcs());
                    }
                    if (drag.Status != PromptStatus.OK) return false;
                }

                // 高さを指定 <1.0000>
                {
                    var jig = new ShapeScaleJigActions(shape);
                    var drag = _userInput.AcquireDistance(jig);
                    if (drag.Status != PromptStatus.OK) return false;
                }

                // 回転角度を指定 <0>
                {
                    var jig = new ShapeRotateJigActions(shape);
                    var drag = _userInput.AcquireAngle(jig);
                    if (drag.Status != PromptStatus.OK) return false;
                }
                tr.Commit();
            }
            return true;
        }
    }
}
