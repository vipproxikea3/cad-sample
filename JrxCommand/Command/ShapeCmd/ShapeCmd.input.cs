﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;

#endif
using System;
using JrxCad.Utility;
using JrxCad.Helpers;

namespace JrxCad.Command.ShapeCmd
{
    public partial class ShapeCmd
    {
        private readonly IShapeUserInput _userInput;

        public interface IShapeUserInput : IUserInputBase
        {
        }

        private class UserInput : UserInputBase, IShapeUserInput
        {
        }

        public class ShapePointJigActions : PointDrawJigN.JigActions
        {
            private readonly Shape _shape;
            public sealed override JigPromptPointOptions Options { get; set; }

            public ShapePointJigActions(Shape shape)
            {
                _shape = shape;
                Options = new JigPromptPointOptions
                {
                    UserInputControls = UserInputControls.NoDwgLimitsChecking |
                                        UserInputControls.Accept3dCoordinates |
                                        UserInputControls.UseBasePointElevation,
                    Message = $"\n{ShapeRes.UI_P2}"
                };
            }

            public override bool Draw(WorldGeometry geometry)
            {
                geometry.Draw(_shape);
                return true;
            }

            public override void OnUpdate()
            {
                _shape.Position = LastValue;
            }
        }

        public class ShapeScaleJigActions : DistanceDrawJigN.JigActions
        {
            private readonly Shape _shape;
            public sealed override JigPromptDistanceOptions Options { get; set; }

            public ShapeScaleJigActions(Shape shape)
            {
                _shape = shape;
                Options = new JigPromptDistanceOptions()
                {
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.NullResponseAccepted |
                                        UserInputControls.NoZeroResponseAccepted |
                                        UserInputControls.NoNegativeResponseAccepted,
                    Cursor = CursorType.RubberBand,
                    Message = $"\n{ShapeRes.UI_P3}",
                    DefaultValue = 1.0,
                    UseBasePoint = true,
                    BasePoint = shape.Position,
                };

                //ToDo
                //Autocad in WCS, shape not draw after specify insertion point
                //when hovering shape is drawn normally
            }

            public override bool Draw(WorldGeometry geometry)
            {
                // Enter Key with Default Value
                if (LastValue.IsZero())
                {
                    _shape.Size = Options.DefaultValue;
                }
                geometry.Draw(_shape);
                return true;
            }

            public override void OnUpdate()
            {
                _shape.Size = LastValue;
            }
        }

        public class ShapeRotateJigActions : AngleDrawJigN.JigActions
        {
            public sealed override JigPromptAngleOptions Options { get; set; }
            private readonly double _rotAngle;
            private readonly Shape _shape;

            public ShapeRotateJigActions(Shape shape)
            {
                _shape = shape;
                var cs = CoordConverter.UcsToWcs().CoordinateSystem3d;
                _rotAngle = cs.Xaxis.AngleOnPlane(new Plane(Point3d.Origin, cs.Zaxis));
                Options = new JigPromptAngleOptions()
                {
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.NullResponseAccepted,
                    Cursor = CursorType.RubberBand,
                    Message = $"\n{ShapeRes.UI_P4}",
                    DefaultValue = Util.Database().Angbase,
                    UseBasePoint = true,
                    BasePoint = shape.Position
                };
            }

            public override bool Draw(WorldGeometry geometry)
            {
                geometry.Draw(_shape);
                return true;
            }

            public override void OnUpdate()
            {
                // Enter Key with Default Value
                if (LastValue.IsEqual(Options.DefaultValue))
                {
                    _shape.Rotation = _rotAngle;
                }
                else
                {
                    _shape.Rotation = LastValue + _rotAngle;
                }
            }
        }
    }
}
