﻿#if _IJCAD_
using GrxCAD.Runtime;
using CadException = GrxCAD.Runtime.Exception;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using CadException = Autodesk.AutoCAD.Runtime.Exception;

#endif

using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.Command.PSpace
{
    public class PSpaceCmd : BaseCommand
    {
#if DEBUG
        [CommandMethod("JrxCommand", "PS", CommandFlags.Modal | CommandFlags.NoTileMode | CommandFlags.NoNewStack)]
#else 
        [CommandMethod("JrxCommand", "SmxPSpace", CommandFlags.Modal | CommandFlags.NoTileMode | CommandFlags.NoNewStack)]
#endif
        public override void OnCommand()
        {
            if (!Init())
            {
                return;
            }

            try
            {
                ExecuteCommand();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        private void ExecuteCommand()
        {
            try
            {
                //レイアウトタブでペーパー空間がアクティブの場合
                if (SystemVariable.GetInt("CVPORT") == 1)
                {
                    Util.Editor().WriteMessage(PSpaceRes.CM_M1);
                    return;
                }
                Util.Editor().SwitchToPaperSpace();
            }
            catch (CadException)
            {
                // ignored
            }
        }
    }
}