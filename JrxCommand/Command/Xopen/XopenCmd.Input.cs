﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;
using GrxCAD.ApplicationServices;
using CADApp = GrxCAD.ApplicationServices.Application;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;

#endif
using JrxCad.Utility;
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using JrxCad.Command.Xopen.ViewModel;
using JrxCad.Command.Xopen.View;

namespace JrxCad.Command.Xopen
{
    public partial class XopenCmd
    {
        private readonly IUserInputXopenCmd _userInput;
        private static double _xopenWidth, _xopenHeight;

        /// <summary>
        /// IUserInput Interface
        /// </summary>
        public interface IUserInputXopenCmd
        {
            /// <summary>
            /// Get PromptNestedEntityResult
            /// </summary>
            PromptNestedEntityResult GetNestedEntity(PromptNestedEntityOptions options);

            /// <summary>
            /// Get ObjectId array from PromptNestedEntityResult
            /// </summary>
            ObjectId[] GetObjectId(PromptNestedEntityResult promptNestedEntityResult);

            /// <summary>
            /// Get PICKFIRST setting
            /// </summary>
            int GetPickFirst();

            /// <summary>
            /// Get PromptSelectionResult
            /// </summary>
            PromptSelectionResult GetPromptSelectionResult();

            /// <summary>
            /// read-only dialog
            /// </summary>
            bool ShowReadOnlyDialog(string path);

            /// <summary>
            /// Get full path xref drawing file
            /// </summary>
            string GetFullPath(string path);

            bool? FormXOpenShowDialog(WXOpenViewModel viewModel);
        }

        /// <summary>
        /// userinputImpl
        /// </summary>
        private class UserInputXopenCmd : IUserInputXopenCmd
        {
            /// <summary>
            /// get PromptNestedentityResult from nestedEntity
            /// </summary>
            public PromptNestedEntityResult GetNestedEntity(PromptNestedEntityOptions options)
            {
                return Util.Editor().GetNestedEntity(options);
            }

            /// <summary>
            /// Get ObjectId collection from PromptNestedResult
            /// </summary>
            public ObjectId[] GetObjectId(PromptNestedEntityResult promptNestedEntityResult)
            {
                return promptNestedEntityResult.GetContainers();
            }

            /// <summary>
            /// Get PickFirst setting
            /// </summary>
            public int GetPickFirst()
            {
                return SystemVariable.GetInt("PICKFIRST");
            }

            /// <summary>
            /// Get PromptSelectionResult
            /// </summary>
            public PromptSelectionResult GetPromptSelectionResult()
            {
                return Util.Editor().SelectImplied();
            }

            /// <summary>
            /// show ready only dialog
            /// </summary>
            public bool ShowReadOnlyDialog(string path)
            {
                return MsgBox.Ask.Show($"{XOpenRes.DM_Message1}\n {path} {XOpenRes.DM_Message2}",
                                       $"{XOpenRes.DM_Caption}", MessageBoxIcon.Warning);
            }

            /// <summary>
            /// Get full path to check document is opened
            /// </summary>
            public string GetFullPath(string pathName)
            {
                return Uri.TryCreate(pathName, UriKind.Absolute, out var uri)
                        ? uri.LocalPath
                        : new Uri(new Uri(Util.CurDoc().Name), pathName).LocalPath;
            }

            public bool? FormXOpenShowDialog(WXOpenViewModel viewModel)
            {
                var form = new WXopen(viewModel);
                form.SetBlockImageSize();

                if (_xopenWidth > 0 && _xopenHeight > 0)
                {
                    form.FormWidth = _xopenWidth;
                    form.FormHeight = _xopenHeight;
                }
                form.ShowDialog();
                if (form.DialogResult == true)
                {
                    _xopenWidth = form.Width;
                    _xopenHeight = form.Height;
                }
                return form.DialogResult;
            }
        }
    }
}
