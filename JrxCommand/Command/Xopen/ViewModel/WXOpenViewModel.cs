﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCad.View.CustomWpf;
using static JrxCad.Command.Xopen.XopenCmd;
using JrxCad.View;
using JrxCad.Command.Xopen.View;

namespace JrxCad.Command.Xopen.ViewModel
{
    public class WXOpenViewModel : BindableBase
    {
        #region EventHandler
        public event CustomWindowEventHandler GetCustomWindow;
        public delegate void CustomWindowEventHandler(ref CustomWindow wXopen);
        #endregion

        #region variables
        private const string UNDO = "_u";
        private const string XOPEN = "XOPEN";
        private readonly List<XrefItem> _xrefItems;
        private readonly ObjectId[] _containers;
        private BlockImage _blockImage;
        #endregion

        #region Properties

        private bool _isHighLightXrefSelected;
        public bool IsHighLightXrefSelected
        {
            get { return _isHighLightXrefSelected; }
            set { SetProperty(ref _isHighLightXrefSelected, value); }
        }
        private List<XrefFileItem> _xRefFileItems;
        public List<XrefFileItem> XRefFileItems
        {
            get { return _xRefFileItems; }
            set { SetProperty(ref _xRefFileItems, value); }
        }
        private BitmapSource _previewImage;
        public BitmapSource PreviewImage
        {
            get { return _previewImage; }
            set { SetProperty(ref _previewImage, value); }
        }
        public ObservableCollection<XrefFileItem> SelectedXrefFileItems { get; set; }

        #endregion

        #region Init ViewModel
        /// <summary>
        /// Constructor ViewModel
        /// </summary>
        public WXOpenViewModel(List<XrefItem> xrefs, ObjectId[] containers)
        {
            _xrefItems = xrefs;
            _containers = containers;
            Init();
        }

        /// <summary>
        /// Set block image size
        /// </summary>
        public void SetBlockImageSize(int previewWidth, int previewHeight)
        {
            _blockImage = new BlockImage { ImageWidth = previewWidth, ImageHeight = previewHeight };
        }

        /// <summary>
        /// Init data ViewModel
        /// </summary>
        public void Init()
        {
            XRefFileItems = new List<XrefFileItem>();
            SelectedXrefFileItems = new ObservableCollection<XrefFileItem>();
            LoadXrefFileItems();
        }

        /// <summary>
        /// Load Form: TreeView + BlockImage
        /// </summary>
        private void LoadXrefFileItems()
        {
            if (_xrefItems != null)
            {
                // set treeview parent
                var xrefItemParents = _xrefItems.Where(a => a.ParentId == ObjectId.Null).ToList();
                if (xrefItemParents != null)
                {
                    foreach (var xrefItem in xrefItemParents)
                    {
                        var childNode = new XrefFileItem()
                        {
                            Title = xrefItem.Name,
                            RefId = xrefItem.RefId,
                            PathName = GetFullPath(xrefItem.PathName)
                        };
                        childNode.ItemSelectionChanged += ItemSelectionChanged;

                        // create child node by parent
                        CreateXrefFileItemNode(childNode);
                        //add into treeview
                        XRefFileItems.Add(childNode);
                    }
                }
            }
        }
        /// <summary>
        /// Select first Xref
        /// </summary>
        public void SelectXrefFile()
        {
            if (XRefFileItems != null)
            {
                //Select First Xref
                XRefFileItems[0].IsSelected = true;
            }
        }

        /// <summary>
        /// Create TreeView Node
        /// </summary>
        private void CreateXrefFileItemNode(XrefFileItem node)
        {
            if (_xrefItems != null)
            {
                var childNodes = _xrefItems.Where(a => a.ParentId == node.RefId).ToList();
                if (childNodes != null)
                {
                    foreach (XrefItem xrefItemChild in childNodes)
                    {
                        var childNode = new XrefFileItem()
                        {
                            Title = xrefItemChild.Name,
                            RefId = xrefItemChild.RefId,
                            PathName = GetFullPath(xrefItemChild.PathName)
                        };
                        childNode.ItemSelectionChanged += ItemSelectionChanged;

                        // add into tree-view
                        node.Items.Add(childNode);
                        CreateXrefFileItemNode(childNode);
                    }
                }
            }
        }


        /// <summary>
        /// selected treeview item by refId (single selected or multi selected).
        /// </summary>
        private void FindXrefItem(List<XrefFileItem> items, ObjectId refId, ref XrefFileItem xrefResult)
        {
            foreach (XrefFileItem xref in items)
            {
                if (xref.RefId == refId)
                {
                    xrefResult = xref;
                    break;
                }

                if (xref.Items != null && xref.Items.Count > 0)
                {
                    FindXrefItem(xref.Items, refId, ref xrefResult);
                }
            }
        }

        /// <summary>
        /// Item Selection Changed
        /// </summary>
        private void ItemSelectionChanged(bool isSelected, ObjectId refId)
        {
            // get xref file item
            XrefFileItem xrefFileItem = null;
            FindXrefItem(XRefFileItems, refId, ref xrefFileItem);
            if (xrefFileItem == null) return;

            if (SelectedXrefFileItems != null)
            {
                bool isExistInXrefList = SelectedXrefFileItems.Where(x => x.RefId == refId).Any();
                if (isSelected)
                {
                    // add item selected
                    if (!isExistInXrefList) SelectedXrefFileItems.Add(xrefFileItem);
                }
                else
                {
                    // remove item selected 
                    if (isExistInXrefList) SelectedXrefFileItems.Remove(xrefFileItem);
                }
            }
            OnSelectedItemsChanged();
        }


        /// <summary>
        /// xref file item selection changed
        /// </summary>
        public void OnSelectedItemsChanged()
        {
            if (SelectedXrefFileItems != null)
            {
                using (var tr = Util.StartTransaction())
                {
                    if (SelectedXrefFileItems.Count > 1)
                    {
                        // don't preview image when multi selected
                        PreviewImage = null;
                    }

                    // load preview image when single selected
                    else if (SelectedXrefFileItems.Count == 1)
                    {
                        var referFileItem = SelectedXrefFileItems[0];
                        var selectedXref = _xrefItems.First(r => r.RefId == referFileItem.RefId);
                        using (var blockDef = tr.GetObject<BlockTableRecord>(selectedXref.DefId, OpenMode.ForRead))
                        {
                            PreviewImage = BitmapToImageSource(_blockImage.Create(blockDef, true));
                        }
                    }
                    // unhighlight all xref item selected
                    UnHighLightAllXrefItem();

                    // hightlight xref item selected when have setting.
                    if (IsHighLightXrefSelected)
                    {
                        foreach (var referItem in SelectedXrefFileItems)
                        {
                            var xrefItem = _xrefItems.Find(a => a.RefId == referItem.RefId);
                            if (xrefItem != null)
                            {
                                SelectedBlockReference(tr, xrefItem, true);
                                xrefItem.IsHighLight = true;
                            }
                        }
                    }

                    tr.Commit();
                    Util.Editor().UpdateScreen();
                }
            }
        }
        #endregion

        #region Event Handler

        /// <summary>
        /// Zoom
        /// </summary>
        public void ExecuteZoomToEvent()
        {
            if (SelectedXrefFileItems.Count == 0) return;

            var bounds = new Extents3d();
            using (var tr = Util.StartTransaction())
            {
                foreach (var xref in SelectedXrefFileItems)
                {
                    // get parentId 
                    ObjectId parentId = _xrefItems.Find(x => x.RefId == xref.RefId).ParentId;
                    if (SelectedXrefFileItems.Any(x => x.RefId == parentId)) continue;

                    using (var blockRef = tr.GetObject<BlockReference>(xref.RefId, OpenMode.ForRead))
                    {
                        if (blockRef != null)
                        {
                            bounds.AddExtents(blockRef.GeometricExtents);
                            var currentContainer = GetCurrentContainers(blockRef.ObjectId);
                            var mat = CoordConverter.McsToWcs(currentContainer.ToArray(), out bool isProp);
                            bounds.TransformBy(mat);
                        }
                    }
                }
            }

            //Zoom to
            ZoomToXrefItem(bounds);
        }

        /// <summary>
        /// Open guide Xopen Command [Open Reference file dialog]
        /// </summary>
        public void ExecuteHelpEvent()
        {
            var helpName = GetHelpName();
            if (!string.IsNullOrEmpty(helpName))
            {
                Util.DoHelpForCommand(helpName);
            }
        }

        /// <summary>
        /// Close [Open Reference file] Dialog
        /// </summary>
        public void ExecuteCloseDialogEvent()
        {
            // un-highlight all xref item
            UnHighLightAllXrefItem();
        }

        /// <summary>
        /// Open Drawing file (single file or multiple file) by selected item
        /// </summary>
        public void ExecuteOpenEvent()
        {
            // un-highlight all xref item
            UnHighLightAllXrefItem();
        }
        #endregion

        #region Method
        /// <summary>
        /// Get full path (directory) of xref file.
        /// </summary>
        private string GetFullPath(string pathName)
        {
            return Uri.TryCreate(pathName, UriKind.Absolute, out var uri)
                  ? uri.LocalPath
                  : new Uri(new Uri(Util.CurDoc().Name), pathName).LocalPath;
        }

        /// <summary>
        /// un-highlight all xref item in current document
        /// </summary>
        public void UnHighLightAllXrefItem()
        {
            if (_xrefItems != null)
            {
                using (var tr = Util.StartTransaction())
                {
                    foreach (var xrefItem in _xrefItems.Where(a => a.IsHighLight).ToList())
                    {
                        SelectedBlockReference(tr, xrefItem, false);
                        xrefItem.IsHighLight = false;
                    }
                    tr.Commit();
                }
            }
        }

        /// <summary>
        ///  Selected BlockReference in current document
        /// </summary>
        private void SelectedBlockReference(Transaction tr, XrefItem xref, bool isSelected)
        {
            var blockRef = tr.GetObject<Entity>(xref.RefId, OpenMode.ForRead);
            if (blockRef != null)
            {
                var currentContainer = GetCurrentContainers(blockRef.ObjectId);
                var containerArray = currentContainer.ToArray();

                // Reverse the "Containers"
                ObjectId[] revIds = new ObjectId[currentContainer.Count + 1];
                for (int i = 0; i < currentContainer.Count; i++)
                {
                    var id = containerArray[currentContainer.Count - 1 - i];
                    revIds.SetValue(id, i);
                }
                // Add the selected to the end
                revIds.SetValue(xref.RefId, currentContainer.Count);

                blockRef = tr.GetObject<Entity>(revIds[0], OpenMode.ForRead, false);
                var subEntityId = new SubentityId(SubentityType.Null, IntPtr.Zero);
                var path = new FullSubentityPath(revIds, subEntityId);

                // highlight or unhighlight xref item
                if (isSelected)
                {
                    blockRef.Highlight(path, false);
                }
                else
                {
                    blockRef.Unhighlight(path, false);
                }
            }
        }

        /// <summary>
        /// Get Current Containers
        /// </summary>
        private List<ObjectId> GetCurrentContainers(ObjectId curId)
        {
            List<ObjectId> curentContainers = new List<ObjectId>();
            if (_xrefItems != null)
            {
                var curRef = _xrefItems.Find(r => r.RefId == curId);
                if (_containers == null) //PickFirst = 1
                {
                    while (curRef.ParentId != ObjectId.Null)
                    {
                        curRef = _xrefItems.Find(r => r.RefId == curRef.ParentId);
                        curentContainers.Add(curRef.RefId);
                    }
                }
                else
                {
                    // PromptNestedEntity
                    while (curRef.ParentId != ObjectId.Null)
                    {
                        curentContainers.Add(curRef.RefId);
                        curRef = _xrefItems.First(r => r.RefId == curRef.ParentId);
                    }
                    // add container into list
                    curentContainers.AddRange(_containers);

                    if (curentContainers.Count >= 1)
                    {
                        curentContainers.RemoveAt(0);
                    }
                }
            }

            return curentContainers;
        }

        /// <summary>
        /// Get Command's guide
        /// </summary>
        public string GetHelpName()
        {
            return XOPEN;
        }

        /// <summary>
        /// Get BitmapSource for PreviewImage property
        /// </summary>
        private BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            if (bitmap == null) return null;
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        /// <summary>
        /// Zoom to Xref item
        /// </summary>
        private void ZoomToXrefItem(Extents3d bounds)
        {
            //Zoom to
            using (var view = Util.Editor().GetCurrentView())
            {
                bounds.TransformBy(Matrix3d.Displacement(view.Target.GetAsVector()).Inverse());
                bounds.TransformBy(CoordConverter.WcsToUcs());

                view.CenterPoint = bounds.CenterPoint2d();
                view.Width = bounds.Width();
                view.Height = bounds.Height();

                Util.Editor().SetCurrentView(view);
                Util.Editor().Regen();
            }

            CustomWindow window = null;
            GetCustomWindow?.Invoke(ref window);
            if (window == null) return;

            using (Util.Editor().StartUserInteraction(window))
            {
                var point = new PromptPointOptions($"\n{XOpenRes.Message_Enter}")
                {
                    AllowNone = true,
                    AllowArbitraryInput = true
                };

                while (true)
                {
                    var res = Util.Editor().GetPoint(point);
                    switch (res.Status)
                    {
                        case PromptStatus.None:
                            return;
                        case PromptStatus.Cancel:
                            return;
                        case PromptStatus.Keyword:
                            if (res.StringResult == UNDO) // Undo
                                return;
                            else
                                continue;
                        default:
                            continue;
                    }
                }
            }
        }

        #endregion
    }

    public class XrefFileItem : ViewModelBase
    {
        #region "Event"
        public event ItemSelectionChangedHandler ItemSelectionChanged;
        public delegate void ItemSelectionChangedHandler(bool isSelected, ObjectId refId);
        #endregion

        #region Properties
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                SetProperty(ref _isSelected, value);
                ItemSelectionChanged?.Invoke(_isSelected, RefId);
            }
        }
        public string Title { get; set; }
        public ObjectId RefId { get; set; }
        public string PathName { get; set; }
        public List<XrefFileItem> Items { get; set; }
        #endregion

        #region Method
        public XrefFileItem()
        {
            Items = new List<XrefFileItem>();
        }
        #endregion
    }
}
