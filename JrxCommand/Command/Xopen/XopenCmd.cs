﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.EditorInput;
using GrxCAD.ApplicationServices;
using GrxCAD.DatabaseServices;
using CADApp = GrxCAD.ApplicationServices.Application;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;

#endif
using System;
using System.IO;
using JrxCad.Utility;
using JrxCad.Helpers;
using JrxCad.Command.Xopen.View;
using System.Collections.Generic;
using Exception = System.Exception;
using JrxCad.Command.Xopen.ViewModel;
using System.Collections.ObjectModel;

namespace JrxCad.Command.Xopen
{
    public partial class XopenCmd : BaseCommand
    {
        private ObjectId[] _currentContainers;
        private bool _hasChild;
        public XopenCmd() : this(new UserInputXopenCmd())
        {
        }

        public XopenCmd(IUserInputXopenCmd userInput)
        {
            _userInput = userInput;
        }

#if DEBUG
        [CommandMethod("JrxCommand", "XO",
        CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.NoMultiple |
        CommandFlags.NoNewStack | CommandFlags.Interruptible)]
#else
#if _IJCAD_
        [CommandMethod("JrxCommand", "SmxXOPEN",
        CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.NoMultiple |
        CommandFlags.NoNewStack | CommandFlags.Interruptible)]
#elif _AutoCAD_
        [CommandMethod("JrxCommand", "SmxXOPEN",
        CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.NoMultiple |
        CommandFlags.NoNewStack | CommandFlags.Interruptible)]
#endif
#endif
        public override void OnCommand()
        {
            if (!Init())
            {
                return;
            }

            try
            {
                var xrefItems = new List<XrefItem>();
                using (var tr = Util.StartTransaction())
                {
                    if (_userInput.GetPickFirst() == 1)
                    {
                        var retPick = _userInput.GetPromptSelectionResult();
                        if (retPick.Status == PromptStatus.OK)
                        {
                            foreach (SelectedObject selectedObject in retPick.Value)
                            {
                                if (!IsXref(tr, selectedObject.ObjectId, out var blockRef))
                                    continue;

                                xrefItems.Add(new XrefItem(blockRef, ObjectId.Null));
                                Collect(tr, blockRef, ref xrefItems);
                            }
                        }
                        _currentContainers = null;
                    }

                    if (xrefItems.Count == 0)
                    {
                        while (true)
                        {
                            var options = new PromptNestedEntityOptions($"\n{XOpenRes.UI_Select}");
                            var select = _userInput.GetNestedEntity(options);
                            if (select == null || select.Status != PromptStatus.OK) return;
                            _currentContainers = _userInput.GetObjectId(select);

                            // ignore object-id isn't xref before get block reference.
                            ObjectIdCollection container = GetXrefByContainer(tr, _currentContainers);

                            // check xref after ignore object-id isn't xref.
                            if (container.Count == 0 || !IsXref(tr, container[0], out var blockRef))
                            {
                                WriteMessage($"\n{XOpenRes.CM_NotXref}");
                                continue;
                            }
                            xrefItems.Add(new XrefItem(blockRef, ObjectId.Null));
                            Collect(tr, blockRef, ref xrefItems);
                            break;
                        }
                    }
                }

                // open file process
                OpenXrefProcess(xrefItems);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        /// <summary>
        /// process open file
        /// </summary>
        private void OpenXrefProcess(List<XrefItem> xrefItems)
        {
            if (xrefItems != null)
            {
                switch (xrefItems.Count)
                {
                    case 0:
                        return;

                    case 1:
                        CADApp.DocumentManager.MdiActiveDocument = OpenXref(xrefItems[0].PathName);
                        break;

                    default:
                        if (_hasChild)
                        {
                            var viewModel = new WXOpenViewModel(xrefItems, _currentContainers);
                            if (_userInput.FormXOpenShowDialog(viewModel) == true)
                            {
                                _hasChild = false;
                                OpenDrawingFile(viewModel.SelectedXrefFileItems, xrefItems);
                            }
                            else
                            {
                                viewModel.UnHighLightAllXrefItem();
                            }
                        }
                        else
                        {
                            OpenMultiXref(xrefItems);
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Open drawing single or multiple file.
        /// </summary>
        private void OpenDrawingFile(ObservableCollection<XrefFileItem> selectedXrefItems, List<XrefItem> xrefItems)
        {
            if (selectedXrefItems != null)
            {
                List<XrefItem> xrefOpeneds = new List<XrefItem>();
                foreach (var referItem in selectedXrefItems)
                {
                    var xrefItem = xrefItems.Find(a => a.RefId == referItem.RefId);
                    if (xrefItem != null)
                    {
                        xrefOpeneds.Add(xrefItem);
                    }
                }

                // open single or multiple drawing file
                if (xrefOpeneds.Count == 1)
                {
                    CADApp.DocumentManager.MdiActiveDocument = OpenXref(xrefOpeneds[0].PathName);
                }
                else if (xrefOpeneds.Count > 1)
                {
                    OpenMultiXref(xrefOpeneds);
                }
            }
        }

        /// <summary>
        /// Open Xref In New Tab
        /// </summary>
        private Document OpenXref(string pathName)
        {
            var path = _userInput.GetFullPath(pathName);

            // check document is opened or not, true => document isn't opened, false => document is opened
            var documentIsOpened = IsFileOpened(path);
            if (documentIsOpened != null) return documentIsOpened;

            //Check IsReadOnly
            if (IsFileInUseOrReadOnly(new FileInfo(path)))
            {
                // TODO display msg by TaskDialog
                if (_userInput.ShowReadOnlyDialog(path))
                {
                    return CADApp.DocumentManager.Open(path, true);
                }
                else
                {
                    return Util.CurDoc();
                }
            }
            return CADApp.DocumentManager.Open(path, false);
        }

        /// <summary>
        /// Open Multiple Xref
        /// </summary>
        private void OpenMultiXref(List<XrefItem> xrefs)
        {
            //Check ReadOnly + InUse
            foreach (var xref in xrefs)
            {
                xref.IsOpen = true;
                xref.FullPath = _userInput.GetFullPath(xref.PathName);

                //Check Xref is opened
                Document documentIsOpened = IsFileOpened(xref.FullPath);
                xref.IsOpen = documentIsOpened == null;

                //Not check when IsOpen = false
                if (IsFileInUseOrReadOnly(new FileInfo(xref.FullPath)) && xref.IsOpen)
                {
                    xref.IsReadOnly = true;
                    if (!_userInput.ShowReadOnlyDialog(xref.FullPath))
                    {
                        xref.IsOpen = false;
                    }
                }
            }

            // Open Xref
            foreach (var xref in xrefs)
            {
                if (xref.IsOpen)
                {
                    if (xref.IsReadOnly)
                    {
                        CADApp.DocumentManager.MdiActiveDocument = CADApp.DocumentManager.Open(xref.FullPath, true);
                    }
                    else
                    {
                        CADApp.DocumentManager.MdiActiveDocument = CADApp.DocumentManager.Open(xref.FullPath, false);
                    }
                }
            }
        }

        /// <summary>
        /// Recursive Add Child Reference
        /// </summary>
        private void Collect(Transaction tr, BlockReference blockRef, ref List<XrefItem> xrefs)
        {
            var blockRefTable = tr.GetObject<BlockTableRecord>(blockRef.BlockTableRecord, OpenMode.ForRead);
            foreach (var item in xrefs)
            {
                if (item.RefId != blockRef.Id)
                {
                    continue;
                }
                item.DefId = blockRefTable.Id;
                item.PathName = blockRefTable.PathName;
                break;
            }

            foreach (var id in blockRefTable)
            {
                var br = tr.GetObject<BlockReference>(id, OpenMode.ForRead);
                if (br == null)
                {
                    continue;
                }

                var childBlock = tr.GetObject<BlockTableRecord>(br.BlockTableRecord, OpenMode.ForRead);
                if (!childBlock.IsFromExternalReference)
                {
                    continue;
                }

                xrefs.Add(new XrefItem(br, blockRef.Id));
                _hasChild = true;
                Collect(tr, br, ref xrefs);
            }
        }

        /// <summary>
        /// Check External Xref
        /// </summary>
        public bool IsXref(Transaction tr, ObjectId id, out BlockReference blockRef)
        {
            blockRef = tr.GetObject<BlockReference>(id, OpenMode.ForRead);
            if (blockRef == null)
            {
                return false;
            }

            var blockRefTable = tr.GetObject<BlockTableRecord>(blockRef.BlockTableRecord, OpenMode.ForRead);
            if (!blockRefTable.IsFromExternalReference)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Check Document is opened or not
        /// </summary>
        public Document IsFileOpened(string fullPath)
        {
            foreach (Document doc in CADApp.DocumentManager)
            {
                if (doc.Name.Equals(fullPath, StringComparison.CurrentCultureIgnoreCase))
                {
                    return doc;
                }
            }
            return null;
        }

        /// <summary>
        /// Check Xref InUse Or ReadOnly
        /// </summary>
        public bool IsFileInUseOrReadOnly(FileInfo fi)
        {
            FileStream fs = null;
            try
            {
                fs = fi.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (Exception ex)
            {
                if (ex is IOException || ex is UnauthorizedAccessException)
                {
                    return true;
                }
                throw;
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
            // File is accessible
            return false;
        }

        /// <summary>
        /// Get xref has BlockTableRecord record and it is External reference
        /// </summary>
        public ObjectIdCollection GetXrefByContainer(Transaction tr, ObjectId[] currentContainer)
        {
            ObjectIdCollection collection = new ObjectIdCollection();
            foreach (var id in currentContainer)
            {
                var blockReference = tr.GetObject<BlockReference>(id, OpenMode.ForRead);
                if (blockReference != null)
                {
                    var record = tr.GetObject<BlockTableRecord>(blockReference.BlockTableRecord, OpenMode.ForRead);
                    if (record != null && record.IsFromExternalReference) collection.Add(id);
                }
            }
            return collection;
        }

        public class XrefItem
        {
            /// <summary>
            /// BlockReference Id
            /// </summary>
            public ObjectId RefId { get; }
            /// <summary>
            /// BlockTableRecord Id
            /// </summary>
            public ObjectId DefId { get; set; }
            public ObjectId ParentId { get; }
            public string Name { get; }
            public string PathName { get; set; }
            public string FullPath { get; set; }
            public bool IsHighLight { get; set; }
            public bool IsOpen { get; set; }
            public bool IsReadOnly { get; set; }
            public XrefItem(BlockReference blockRef, ObjectId parent)
            {
                Name = blockRef.Name;
                RefId = blockRef.Id;
                ParentId = parent;
            }
            public override bool Equals(object obj)
            {
                if (obj == null || !(obj is XrefItem)) return false;
                var xref = obj as XrefItem;
                return xref.RefId == RefId;
            }

            protected bool Equals(XrefItem other)
            {
                return RefId.Equals(other.RefId);
            }
            public override int GetHashCode()
            {
                return RefId.GetHashCode();
            }
        }
    }
}
