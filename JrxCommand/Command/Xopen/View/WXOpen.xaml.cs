﻿#if _IJCAD_
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;

#endif
using System;
using System.Windows;
using System.Collections.Generic;
using JrxCad.Command.Xopen.ViewModel;
using JrxCad.Utility;
using static JrxCad.Command.Xopen.XopenCmd;
using System.Collections.Specialized;
using JrxCad.View.CustomWpf;

namespace JrxCad.Command.Xopen.View
{
    public partial class WXopen
    {
        #region "Variable"
        public double FormWidth = 0;
        public double FormHeight = 0;
        #endregion

        public WXopen(WXOpenViewModel viewModel)
        {
            InitializeComponent();
            FormWidth = Width;
            FormHeight = Height;
            viewModel.GetCustomWindow += GetCustomWindow;

            DataContext = viewModel;
        }

        /// <summary>
        /// Get custom window
        /// </summary>
        private void GetCustomWindow(ref CustomWindow wXopen)
        {
            wXopen = this;
        }

        public void SetBlockImageSize()
        {
            CommandAction((model) => model.SetBlockImageSize((int)imgPreviewImage.Width, (int)imgPreviewImage.Height), false);
        }

        /// <summary>
        /// CommandAction
        /// </summary>
        private void CommandAction(Action<WXOpenViewModel> callback, bool dialogAttack)
        {
            try
            {
                if (!(DataContext is WXOpenViewModel model))
                {
                    return;
                }
                if (dialogAttack)
                {
                    Hide();
                    callback(model);
                    _ = ShowDialog();
                }
                else
                {
                    callback(model);
                }
            }
            catch (Exception ex)
            {
                MsgBox.Error.Show(ex.Message);
            }
        }

        /// <summary>
        /// Zoom To XrefItem is selected
        /// </summary>
        private void BtnZoomTo_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.ExecuteZoomToEvent(), false);
        }

        /// <summary>
        /// Open XrefItem is selected
        /// </summary>
        private void BtnOpenDrawing_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.ExecuteOpenEvent(), false);
            DialogResult = true;
            this.Close();
        }

        /// <summary>
        /// Cancel [Open Reference file] Dialog
        /// </summary>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.ExecuteCloseDialogEvent(), false);
            DialogResult = false;
            this.Close();
        }

        /// <summary>
        /// Open Xopen's guide
        /// </summary>
        private void BtnHelp_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.ExecuteHelpEvent(), false);
        }

        /// <summary>
        /// XOpen Loaded
        /// </summary>
        private void XopenWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!(DataContext is WXOpenViewModel model)) return;
                model.SelectXrefFile();

                Width = FormWidth;
                Height = FormHeight;
            }
            catch (Exception ex)
            {
                MsgBox.Error.Show(ex.ToString());
            }
        }
    }
}
