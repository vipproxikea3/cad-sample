﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using Exception = GrxCAD.Runtime.Exception;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;

#endif
using System;
using JrxCad.Helpers;
using JrxCad.Utility;

namespace JrxCad.Command.Erase
{
    public partial class EraseCmd
    {
        private readonly IUserInput _userInput;
        public static bool HasIconRemove { get; set; }
        /// <summary>
        /// IUserInput Interface
        /// </summary>
        public interface IUserInput : IUserInputBase
        {
            SelectionSet GetSelection();
            SelectionSet SelectImplied();
        }

        /// <summary>
        /// UserInputImpl
        /// </summary>
        private class UserInput : UserInputBase, IUserInput
        {
            public SelectionSet SelectImplied()
            {
                if (SystemVariable.GetInt("PICKFIRST") != 1)
                {
                    return SelectionSet.FromObjectIds(Array.Empty<ObjectId>());
                }
                var retPick = Util.Editor().SelectImplied();
                if (retPick.Status != PromptStatus.OK)
                {
                    return SelectionSet.FromObjectIds(Array.Empty<ObjectId>());
                }
                return retPick.Value;
            }
            public SelectionSet GetSelection()
            {
                Util.Editor().SetImpliedSelection(Array.Empty<ObjectId>());
                //オブジェクトを選択:
                var opt = new PromptSelectionOptions
                {
                    AllowSubSelections = true,
                    AllowDuplicates = true,
                };
                Util.Editor().Rollover += Editor_Rollover;
                var retPick = GetSelection(opt);
                Util.Editor().Rollover -= Editor_Rollover;
                if (retPick.Status != PromptStatus.OK)
                {
                    return SelectionSet.FromObjectIds(Array.Empty<ObjectId>());
                }
                return retPick.Value;
            }
        }

        private static void Editor_Rollover(object sender, RolloverEventArgs e)
        {
            try
            {
                var ids = e.Picked.GetObjectIds();
                if (ids?.Length > 0)
                {
                    if (!HasIconRemove)
                    {
                        BitmapImageHelper.AddRemoveIcon2Cursor();
                        HasIconRemove = true;
                        #region Change color
                        //    using (var trans = Util.StartTransaction())
                        //    {
                        //        HashSet<ObjectId> groupIds = new HashSet<ObjectId>();
                        //        foreach (var id in ids)
                        //        {
                        //            var selectedEntity = trans.GetObject<Entity>(id, OpenMode.ForWrite);
                        //            if (selectedEntity != null)
                        //            {
                        //                using (var persistentIds = selectedEntity.GetPersistentReactorIds())
                        //                {
                        //                    if (persistentIds.Count > 0) //collect if group
                        //                    {
                        //                        foreach (ObjectId persistentId in persistentIds)
                        //                        {
                        //                            groupIds.Add(persistentId);
                        //                        }
                        //                    }
                        //                    else //set color by original entity
                        //                    {
                        //                        selectedEntity.Unhighlight();
                        //                        selectedEntity.Color = Color.FromRgb(74, 81, 88);
                        //                    }
                        //                }
                        //            }
                        //        }

                        //        foreach (var groupId in groupIds) //set color by group
                        //        {
                        //            using (var group = trans.GetObject<Group>(groupId, OpenMode.ForWrite))
                        //            {
                        //                if (group == null) continue;
                        //                group.SetHighlight(false);
                        //                group.SetColor(Color.FromRgb(74, 81, 88));
                        //            }
                        //        }
                        //        Util.Editor().Regen();
                        //        trans.Commit();
                        //    }
                        #endregion
                    }
                }
                else
                {
                    BitmapImageHelper.RemoveCursorBadge();
                    HasIconRemove = false;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }
    }
}
