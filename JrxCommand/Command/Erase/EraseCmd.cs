﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;

#endif
using System.Collections.Generic;
using System.Linq;
using JrxCad.Utility;
using JrxCad.Helpers;
using Exception = System.Exception;

namespace JrxCad.Command.Erase
{
    public partial class EraseCmd : BaseCommand
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EraseCmd"/> class.
        /// </summary>
        public EraseCmd() : this(new UserInput())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EraseCmd"/> class.
        /// </summary>
        /// <param name="userInput">The user input object.</param>
        public EraseCmd(IUserInput userInput)
        {
            _userInput = userInput;
        }

        //TODO: API #422
        [CommandMethod("JrxCommand", "SmxErase", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.NoNewStack)]
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }
                var sourceObjects = new Dictionary<ObjectId, FullSubentityPath[]>();
                using (var trans = Util.StartTransaction())
                {
                    //Get selected objected before
                    SelectionSet selectionSet = _userInput.SelectImplied();
                    if (selectionSet == null || selectionSet.Count < 1)
                    {
                        selectionSet = _userInput.GetSelection();
                        if (selectionSet == null || selectionSet.Count < 1)
                        {
                            return;
                        }
                    }

                    foreach (SelectedObject selectedObj in selectionSet)
                    {
                        //check sub-entities
                        if (selectedObj.SelectionMethod == SelectionMethod.SubEntity)
                        {
                            var selectedSubEnt = selectedObj.GetSubentities();
                            var subPath = selectedSubEnt
                                .ToList()
                                .ConvertAll(item => item.FullSubentityPath)
                                .ToArray();
                            sourceObjects.Add(selectedObj.ObjectId, subPath);
                        }
                        else
                        {
                            sourceObjects.Add(selectedObj.ObjectId, null);
                        }
                    }
                    trans.Commit();
                }
                EraseObjects(sourceObjects);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                BitmapImageHelper.RemoveCursorBadge();
                Exit();
            }
        }

        public static bool EraseObjects(Dictionary<ObjectId, FullSubentityPath[]> sourceObjects)
        {
            try
            {
                using (var tr = Util.StartTransaction())
                {
                    //check if entity is already in erase state.
                    for (var i = 0; i < sourceObjects.Count; i++)
                    {
                        var objectId = sourceObjects.ElementAt(i).Key;
                        var fullSubEntityPaths = sourceObjects.ElementAt(i).Value;
                        if (fullSubEntityPaths is null)
                        {
                            if (objectId.IsErased)
                            {
                                //GetObject, 3rd parameter openErased
                                var ent = tr.GetObject(objectId, OpenMode.ForWrite, true);
                                ent.Erase(false);
                            }
                            else
                            {
                                var ent = tr.GetObject(objectId, OpenMode.ForWrite);
                                ent.Erase();
                            }
                        }
                        else
                        {
                            //delete sub-entities
                            var ent = tr.GetObject(objectId, OpenMode.ForWrite);
                            if (ent is Entity entity)
                            {
                                entity.DeleteSubentityPaths(fullSubEntityPaths);
                            }
                        }
                    }
                    tr.Commit();
                }
                Util.Editor().UpdateScreen();
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
                return false;
            }
        }
    }
}
