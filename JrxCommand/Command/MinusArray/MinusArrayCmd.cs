﻿#if _IJCAD_
using Exception = System.Exception;
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Exception = System.Exception;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Geometry;

#endif
using JrxCad.Utility;
using System;
using System.Globalization;
using JrxCad.Command.ArrayClassic.Model;
using JrxCad.Helpers;

namespace JrxCad.Command.MinusArray
{
    public partial class MinusArrayCmd : BaseCommand
    {
        //properties
        /// <summary>
        /// Default base point for Polar Array
        /// </summary>
        public Point3d DefaultBasePoint { get; set; }

        private static ArrayType _defaultArrayType = ArrayType.Rectangular;

        //constructor
        public MinusArrayCmd() : this(new UserInput())
        {
        }

        public MinusArrayCmd(IMinusArrayUserInput userInput)
        {
            _userInput = userInput;
        }
        //TODO: API #422
#if DEBUG
        [CommandMethod("JrxCommand", "-SmxARRAY", CommandFlags.Transparent | CommandFlags.UsePickSet | CommandFlags.NoNewStack)]
#else
        [CommandMethod("JrxCommand", "-SmxARRAY", CommandFlags.Transparent | CommandFlags.UsePickSet | CommandFlags.NoNewStack)]
#endif
        public override void OnCommand()
        {
            ObjectIdCollection sourceEntities = null;
            try
            {
                if (!Init())
                {
                    return;
                }
                ExecuteCommand(out sourceEntities);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                sourceEntities?.Dispose();
                Exit();
            }
        }

        public void ExecuteCommand(out ObjectIdCollection sourceEntities)
        {
            using (var trans = Util.StartTransaction())
            using (var entityEx = new EntityEx())
            {
                //Get selected objects before
                sourceEntities = entityEx.SelectImplied(Util.CurDoc(), trans);
                if (sourceEntities == null || sourceEntities.Count < 1)
                {
                    sourceEntities = entityEx.SelectEntities(Util.CurDoc(), trans);
                    if (sourceEntities == null || sourceEntities.Count < 1)
                    {
                        return;
                    }
                }
                DefaultBasePoint = entityEx.GetBasePoint();
                trans.Commit();
            }
            // ReSharper disable once UseObjectOrCollectionInitializer
            //Select type of array [Rectangular/Polar]
            var defaultKey = _defaultArrayType == ArrayType.Rectangular ? "R" : "P";
            var promptKeywordOptions = new PromptKeywordOptions($"\n{MinusArrayRes.UI_Type} <{defaultKey}>")
            {
                AllowArbitraryInput = false,
                AppendKeywordsToMessage = false,
                Keywords =
                            {
                                { "R", MinusArrayRes.UI_Type_Opt_Rect },
                                { "P", MinusArrayRes.UI_Type_Opt_Polar }
                            }
            };
            promptKeywordOptions.Keywords.Default = defaultKey;
            var promptTypeResult = _userInput.GetKeyword(promptKeywordOptions);
            if (promptTypeResult.Status != PromptStatus.OK)
            {
                return;
            }

            if (promptTypeResult.StringResult.Equals("R"))
            {
                var rectProcess = RectExecute(sourceEntities);
                if (rectProcess == ProcessFlag.CancelOrDefault)
                {
                    return;
                }
                _defaultArrayType = ArrayType.Rectangular;
            }
            else if (promptTypeResult.StringResult.Equals("P"))
            {
                var polarProcess = PolarExecute(sourceEntities);
                if (polarProcess == ProcessFlag.CancelOrDefault)
                {
                    return;
                }
                _defaultArrayType = ArrayType.Polar;
            }
        }

        private ProcessFlag RectExecute(ObjectIdCollection sourceEntities)
        {
            using (var rectArray = new RectangularArray())
            {
                var snapAngle = SystemVariable.GetDouble("SNAPANG");
                //get source objects.
                rectArray.SourceEntities = sourceEntities;
                rectArray.Angle = snapAngle; // independent angbase

                //input row & column
                if (RectSizeInput(out var row, out var column) == ProcessFlag.Finished)
                {
                    (rectArray.Row, rectArray.Column) = (row, column);
                }
                else
                {
                    return ProcessFlag.CancelOrDefault;
                }

                //validate size of rectangular array
                if (!RectArrayValidation(rectArray.SourceEntities.Count, rectArray.Row, rectArray.Column))
                {
                    return ProcessFlag.CancelOrDefault;
                }

                //input row offset & column offset
                if (OffsetInput(rectArray.Row, rectArray.Column, out var rowOffset, out var colOffset) == ProcessFlag.Finished)
                {
                    (rectArray.RowOffset, rectArray.ClmOffset) = (rowOffset, colOffset);
                }
                else
                {
                    return ProcessFlag.CancelOrDefault;
                }

                //rectangular array draw itself
                var isCompleted = rectArray.Draw(SystemVariable.GetBool("ANGDIR"), false);
                if (!isCompleted)
                {
                    return ProcessFlag.CancelOrDefault;
                }
            }
            return ProcessFlag.Finished;
        }

        private ProcessFlag PolarExecute(ObjectIdCollection sourceEntities)
        {
            using (var polarArray = new PolarArray())
            {
                //get source objects.
                polarArray.SourceEntities = sourceEntities;
                polarArray.BasePoint = DefaultBasePoint;

                //get center point and base point (optional)
                var useDefaultBasePt = true;
                if (GetCenterPtAndBasePt(ref useDefaultBasePt, out var centerPt, out var basePt) == ProcessFlag.Finished)
                {
                    if (!useDefaultBasePt) polarArray.BasePoint = basePt;
                    polarArray.CenterPoint = centerPt;
                }
                else
                {
                    return ProcessFlag.CancelOrDefault;
                }

                var curMethod = PolarMethod.CountAndFillAngle;
                if (InputByMethod(
                    out var itemCount,
                    out var fillAngle,
                    out var itemAngle,
                    polarArray.CenterPoint,
                    ref curMethod) == ProcessFlag.Finished)
                {
                    //polar array re-calculate
                    switch (curMethod)
                    {
                        case PolarMethod.CountAndFillAngle:
                            polarArray.Count = itemCount;
                            polarArray.FillAngle = fillAngle;
                            polarArray.ItemAngle = fillAngle.Division(itemCount - 1);
                            break;
                        case PolarMethod.FillAngleAndItemAngle:
                            polarArray.Count = int.Parse(
                                fillAngle.IsEqual(Math.PI * 2)
                                ? Math.Abs(Math.Floor(fillAngle.Division(itemAngle))).ToString(CultureInfo.CurrentCulture)
                                : Math.Abs(Math.Floor(fillAngle.Division(itemAngle)) + 1).ToString(CultureInfo.CurrentCulture)
                                );
                            polarArray.FillAngle = fillAngle;
                            polarArray.ItemAngle = itemAngle;
                            break;
                        case PolarMethod.CountAndItemAngle:
                            polarArray.Count = itemCount;
                            polarArray.FillAngle = itemAngle * itemCount;
                            polarArray.ItemAngle = itemAngle;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
                else
                {
                    return ProcessFlag.CancelOrDefault;
                }

                //validate size of polar array
                if (!PolarArrayValidation(polarArray.SourceEntities.Count, polarArray.Count))
                {
                    return ProcessFlag.CancelOrDefault;
                }

                //is rotate items as copied?
                if (GetRotationOfItemOption(out var isRotate) == ProcessFlag.Finished)
                {
                    polarArray.IsRotate = isRotate;
                }
                else
                {
                    return ProcessFlag.CancelOrDefault;
                }

                //polar array draw itself
                var isCompleted = polarArray.Draw(SystemVariable.GetBool("ANGDIR"), isPreview: false);
                if (!isCompleted)
                {
                    return ProcessFlag.CancelOrDefault;
                }
            }
            return ProcessFlag.Finished;
        }

        #region Validation
        private bool PolarArrayValidation(int objectCount, int totalNumberItems)
        {
            var maxArray = EnvironmentVariable.GetInt("MaxArray");
            //case nothing to do.
            if (totalNumberItems == 1)
            {
                WriteMessage(string.Format($"\n{MinusArrayRes.CM_Count1}", maxArray));
                return false;
            }
            //case MaxArray.
            if (objectCount * totalNumberItems > maxArray)
            {
                WriteMessage(string.Format(MinusArrayRes.CM_Over, maxArray, maxArray));
                return false;
            }
            return true;
        }

        private bool RectArrayValidation(int objectCount, int row, int column)
        {
            var maxArray = EnvironmentVariable.GetInt("MaxArray");
            //case nothing to do.
            if (row * column == 1)
            {
                WriteMessage(string.Format($"\n{MinusArrayRes.CM_Count1}", maxArray));
                return false;
            }
            //case MaxArray.
            if (objectCount * row * column > maxArray)
            {
                WriteMessage(string.Format(MinusArrayRes.CM_Over, maxArray, maxArray));
                return false;
            }
            return true;
        }
        #endregion
    }
}
