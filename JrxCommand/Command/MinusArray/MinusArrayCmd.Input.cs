﻿#if _IJCAD_
using Exception = System.Exception;
using GrxCAD.Runtime;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Exception = System.Exception;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Geometry;

#endif
using JrxCad.Utility;
using System;
using JrxCad.Helpers;

namespace JrxCad.Command.MinusArray
{
    public partial class MinusArrayCmd
    {
        private readonly IMinusArrayUserInput _userInput;

        /// <summary>
        /// UserInput of MinusArray
        /// </summary>
        public interface IMinusArrayUserInput : IUserInputBase
        {
        }

        public class UserInput : UserInputBase, IMinusArrayUserInput
        {
        }

        /// <summary>
        /// Methods for Polar Array
        /// </summary>
        public enum PolarMethod
        {
            CountAndFillAngle,
            FillAngleAndItemAngle,
            CountAndItemAngle
        }

        private enum ArrayType
        {
            Rectangular,
            Polar
        }

        #region Rectangular input
        private ProcessFlag RectSizeInput(out int row, out int column)
        {
            row = 1;
            column = 1;
            //get number of rows.
            var rowOptions = new PromptIntegerOptions($"\n{MinusArrayRes.UI_R_Row}")
            {
                DefaultValue = 1,
                LowerLimit = 1,
                UpperLimit = short.MaxValue,
            };
            var promptRowResult = _userInput.GetInteger(rowOptions);
            if (promptRowResult.Status != PromptStatus.OK)
            {
                return ProcessFlag.CancelOrDefault;
            }
            row = promptRowResult.Value;

            //get number of columns.
            var columnOptions = new PromptIntegerOptions($"\n{MinusArrayRes.UI_R_Column}")
            {
                DefaultValue = 1,
                LowerLimit = 1,
                UpperLimit = short.MaxValue,
            };
            var promptColumnResult = _userInput.GetInteger(columnOptions);
            if (promptColumnResult.Status != PromptStatus.OK)
            {
                return ProcessFlag.CancelOrDefault;
            }
            column = promptColumnResult.Value;
            return ProcessFlag.Finished;
        }

        private ProcessFlag OffsetInput(int row, int column, out double rowOffset, out double colOffset)
        {
            //set "1.0" to default value
            rowOffset = 1.0;
            colOffset = 1.0;
            if (row == 1)
            {
                var distanceOptions = new PromptDistanceOptions($"\n{MinusArrayRes.UI_R_ColumnOffset}");
                var promptDoubleResult = _userInput.GetDistance(distanceOptions);
                if (promptDoubleResult.Status != PromptStatus.OK)
                {
                    return ProcessFlag.CancelOrDefault;
                }
                colOffset = promptDoubleResult.Value;
                rowOffset = 0;
            }
            else
            {
                var isColOffsetOk = false;
                if (OffsetInputByCorner(ref isColOffsetOk, column, out rowOffset, out colOffset) != ProcessFlag.Finished)
                {
                    return ProcessFlag.CancelOrDefault;
                }
                //get column offset.
                if (!isColOffsetOk && column != 1)
                {
                    var distanceOptions = new PromptDistanceOptions($"\n{MinusArrayRes.UI_R_ColumnOffset}");
                    var promptDoubleResult = _userInput.GetDistance(distanceOptions);
                    if (promptDoubleResult.Status != PromptStatus.OK)
                    {
                        return ProcessFlag.CancelOrDefault;
                    }
                    colOffset = promptDoubleResult.Value;
                }
            }
            return ProcessFlag.Finished;
        }

        private ProcessFlag OffsetInputByCorner(ref bool isColOffsetOk, int column, out double rowOffset, out double colOffset)
        {
            //set "1.0" to default value
            rowOffset = 1.0;
            colOffset = 1.0;
            var pointOptions = new PromptPointOptions($"\n{MinusArrayRes.UI_R_RowOffset}")
            {
                AllowArbitraryInput = true,
            };
            while (true)
            {
                var startPointResult = _userInput.GetPoint(pointOptions);
                switch (startPointResult.Status)
                {
                    //acquire a point
                    case PromptStatus.OK:
                        var startPoint = startPointResult.Value;
                        var cornerOptions = new PromptCornerOptions($"\n{MinusArrayRes.UI_R_Corner}", startPoint)
                        {
                            UseDashedLine = true,
                            BasePoint = startPoint,
                        };
                        var promptPointResult = _userInput.GetCorner(cornerOptions);
                        if (promptPointResult.Status != PromptStatus.OK)
                        {
                            return ProcessFlag.CancelOrDefault; //endP is none.
                        }
                        var endPoint = promptPointResult.Value;
                        var distance = startPoint.DistanceTo(endPoint);
                        var angle = Vector3d.XAxis.GetAngleTo(endPoint - startPoint, Vector3d.ZAxis);
                        rowOffset = Math.Sin(angle) * distance;
                        colOffset = column == 1 ? 0 : Math.Cos(angle) * distance;
                        isColOffsetOk = true;
                        return ProcessFlag.Finished;
                    //acquire a string
                    case PromptStatus.Keyword:
                        try
                        {
                            rowOffset = double.Parse(startPointResult.StringResult);
                            return ProcessFlag.Finished;
                        }
                        catch (Exception)
                        {
                            WriteMessage($"\n{MinusArrayRes.CM_TwoPointOrNumber}");
                            break;
                        }
                    //RowOffset result is none.
                    default:
                        return ProcessFlag.CancelOrDefault;
                }
            }
        }
        #endregion

        #region Polar input
        private ProcessFlag GetCenterPtAndBasePt(ref bool useDefaultBasePt, out Point3d centerPt, out Point3d basePt)
        {
            centerPt = Point3d.Origin;
            basePt = Point3d.Origin;
            //get center point first.
            var centerPointFirstOption = new PromptPointOptions($"\n{MinusArrayRes.UI_P_Center}")
            {
                AllowArbitraryInput = false,
                AppendKeywordsToMessage = false,
                Keywords =
                    {
                        { "B", MinusArrayRes.UI_BasePoint_Key }
                    }
            };
            var centerPointFirstResult = _userInput.GetPoint(centerPointFirstOption);
            switch (centerPointFirstResult.Status)
            {
                case PromptStatus.OK:
                    centerPt = centerPointFirstResult.Value;
                    break;
                case PromptStatus.Keyword:
                    if (centerPointFirstResult.StringResult.Equals("B"))
                    {
                        //get base point.
                        var basePointOption = new PromptPointOptions($"\n{MinusArrayRes.UI_P_BasePoint}");
                        var basePointResult = _userInput.GetPoint(basePointOption);
                        if (basePointResult.Status != PromptStatus.OK)
                        {
                            return ProcessFlag.CancelOrDefault;
                        }
                        basePt = basePointResult.Value;
                        useDefaultBasePt = false;

                        //get center point.
                        var centerPointOption = new PromptPointOptions($"\n{MinusArrayRes.UI_P_Center2}");
                        var centerPointResult = _userInput.GetPoint(centerPointOption);
                        if (centerPointResult.Status != PromptStatus.OK)
                        {
                            return ProcessFlag.CancelOrDefault;
                        }
                        centerPt = centerPointResult.Value;
                    }
                    break;
                default:
                    return ProcessFlag.CancelOrDefault;
            }
            return ProcessFlag.Finished;
        }

        /// <summary>
        /// input 3 factors of polar array (total number of items, angle to fill, angle between items) by METHOD
        /// </summary>
        /// <param name="itemCount">is total number of items</param>
        /// <param name="fillAngle">is angle to fill</param>
        /// <param name="itemAngle">is angle between items</param>
        /// <param name="centerPt">is center point of polar array</param>
        /// <param name="curMethod">is current method</param>
        /// <returns></returns>
        private ProcessFlag InputByMethod(
            out int itemCount,
            out double fillAngle,
            out double itemAngle,
            Point3d centerPt,
            ref PolarMethod curMethod)
        {
            //set pre-value
            itemCount = 4;
            fillAngle = Math.PI * 2;
            itemAngle = Math.PI / 2;

            //get total number of items.
            //default method: CountAndFillAngle
            var countOption = new PromptIntegerOptions($"\n{MinusArrayRes.UI_P_Count}")
            {
                AllowNone = true,
                LowerLimit = 1,
                UpperLimit = short.MaxValue
            };
            var countResult = _userInput.GetInteger(countOption);
            switch (countResult.Status)
            {
                case PromptStatus.OK:
                    itemCount = countResult.Value;
                    //handle Method 1 and 2:
                    var defaultFillAngle = Converter.RawAngleToString(Math.PI * 2);
                    var fillAngleOption = new PromptAngleOptions($"{MinusArrayRes.UI_P_FillAngle} <{defaultFillAngle}>")
                    {
                        UseDashedLine = true,
                        UseAngleBase = true,
                        UseBasePoint = true,
                        BasePoint = centerPt,
                        AllowZero = true,
                        AllowArbitraryInput = false,
                        AllowNone = true,
                    };
                    var pureAngle = 0.0;
                    Util.Editor().PromptedForPoint += EdPromptedForPoint;
                    var fillAngleResult = _userInput.GetAngle(fillAngleOption);
                    Util.Editor().PromptedForPoint -= EdPromptedForPoint;
                    void EdPromptedForPoint(object sender, PromptPointResultEventArgs e)
                    {
                        try
                        {
                            if (e.Result?.StringResult != string.Empty)
                            {
                                pureAngle = Converter.StringToRawAngle(e.Result?.StringResult);
                            }
                        }
                        catch (Exception)
                        {
                            // ignored
                        }
                    }

                    switch (fillAngleResult.Status)
                    {
                        case PromptStatus.OK:
                            if (fillAngleResult.Value.IsEqual(0.0))
                            {
                                if (pureAngle.IsEqual(Math.PI * 2)) //input string is "360"
                                {
                                    fillAngle = Math.PI * 2;
                                }
                                else
                                {
                                    //switch to method CountAndItemAngle when fillAngle equal to zero
                                    #region CountAndItemAngle
                                    curMethod = PolarMethod.CountAndItemAngle;
                                    var btwAngleOption = new PromptAngleOptions($"{MinusArrayRes.UI_P_ItemAngle2}")
                                    {
                                        UseAngleBase = true,
                                        UseDashedLine = true,
                                        UseBasePoint = true,
                                        BasePoint = centerPt,
                                        AllowZero = false,
                                    };
                                    var btwAngleResult = _userInput.GetAngle(btwAngleOption);
                                    if (btwAngleResult.Status != PromptStatus.OK)
                                    {
                                        return ProcessFlag.CancelOrDefault;
                                    }
                                    itemAngle = btwAngleResult.Value;
                                    #endregion
                                }
                            }
                            else
                            {
                                fillAngle = fillAngleResult.Value;
                            }
                            break;
                        case PromptStatus.None:
                            fillAngle = Math.PI * 2;
                            break;
                        default:
                            return ProcessFlag.CancelOrDefault;
                    }
                    break;
                case PromptStatus.None:
                    //switch to method FillAngleAndItemAngle when itemCount is None (press enter)
                    curMethod = PolarMethod.FillAngleAndItemAngle;
                    if (InputFillAngAndItemAng(centerPt, out fillAngle, out itemAngle) != ProcessFlag.Finished)
                    {
                        return ProcessFlag.CancelOrDefault;
                    }
                    break;
                default:
                    return ProcessFlag.CancelOrDefault;
            }
            return ProcessFlag.Finished;
        }

        private ProcessFlag InputFillAngAndItemAng(Point3d centerPt, out double fillAngle, out double itemAngle)
        {
            fillAngle = Math.PI * 2;
            itemAngle = Math.PI / 2;

            //input fillAngle
            var defaultFillAngle = Converter.RawAngleToString(Math.PI * 2);
            var fillAngleOption = new PromptAngleOptions($"\n{MinusArrayRes.UI_P_FillAngle} <{defaultFillAngle}>")
            {
                UseDashedLine = true,
                UseBasePoint = true,
                BasePoint = centerPt,
                AllowZero = true,
                AllowNone = true,
            };
            var pureFillAngle = 0.0;
            void EdPromptedForPointFillAngle(object sender, PromptPointResultEventArgs e)
            {
                try
                {
                    if (e.Result?.StringResult != string.Empty)
                    {
                        pureFillAngle = Converter.StringToRawAngle(e.Result?.StringResult);
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            while (true) //purpose: input again if fillAngle equal to zero
            {
                pureFillAngle = 0.0;
                Util.Editor().PromptedForPoint += EdPromptedForPointFillAngle;
                var fillAngleResult = _userInput.GetAngle(fillAngleOption);
                Util.Editor().PromptedForPoint -= EdPromptedForPointFillAngle;
                //avoid using SWITCH in WHILE loop
                if (fillAngleResult.Status == PromptStatus.OK)
                {
                    if (fillAngleResult.Value.IsEqual(0.0))
                    {
                        if (pureFillAngle.IsEqual(Math.PI * 2)) //input string is "360"
                        {
                            fillAngle = Math.PI * 2;
                            break;
                        }
                        //value equal to 0, input again
                        WriteMessage(MinusArrayRes.CM_AngleNonZero);
                    }
                    else
                    {
                        fillAngle = fillAngleResult.Value;
                        break;
                    }
                }
                else if (fillAngleResult.Status == PromptStatus.None)
                {
                    fillAngle = Math.PI * 2;
                    break;
                }
                else
                {
                    return ProcessFlag.CancelOrDefault;
                }
            }
            //input itemAngle
            var btwAngleOption = new PromptAngleOptions($"\n{MinusArrayRes.UI_P_ItemAngle}")
            {
                UseAngleBase = true,
                UseDashedLine = true,
                UseBasePoint = true,
                BasePoint = centerPt,
                AllowZero = false,
            };
            double pureBtwAngle = 1;
            void EdPromptedForPointBtwAngle(object sender, PromptPointResultEventArgs e)
            {
                try
                {
                    if (e.Result?.StringResult != string.Empty)
                    {
                        pureBtwAngle = Converter.StringToRawAngle(e.Result?.StringResult);
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            while (true)
            {
                Util.Editor().PromptedForPoint += EdPromptedForPointBtwAngle;
                var btwAngleResult = _userInput.GetAngle(btwAngleOption);
                Util.Editor().PromptedForPoint -= EdPromptedForPointBtwAngle;
                if (btwAngleResult.Status != PromptStatus.OK)
                {
                    return ProcessFlag.CancelOrDefault;
                }
                //when method is FillAngleAndItemAngle, btwAngle must be positive.
                if (pureBtwAngle.OrLess(0.0))
                {
                    pureBtwAngle = 1;
                    WriteMessage($"\n{MinusArrayRes.CM_AngleGtZero}");
                }
                else
                {
                    itemAngle = btwAngleResult.Value;
                    break;
                }
            }
            return ProcessFlag.Finished;
        }

        private ProcessFlag GetRotationOfItemOption(out bool isRotate)
        {
            isRotate = true;
            var isRotateItemsOption = new PromptKeywordOptions($"\n{MinusArrayRes.UI_P_Rotate}")
            {
                AppendKeywordsToMessage = false,
                Keywords =
                {
                    { "Y", MinusArrayRes.UI_Rotate_Opt_Yes},
                    { "N", MinusArrayRes.UI_Rotate_Opt_No}
                }
            };
            isRotateItemsOption.Keywords.Default = "Y";
            var isRotateItemsResult = _userInput.GetKeyword(isRotateItemsOption);
            if (isRotateItemsResult.Status != PromptStatus.OK)
            {
                return ProcessFlag.CancelOrDefault;
            }
            isRotate = isRotateItemsResult.StringResult.Equals("Y");
            return ProcessFlag.Finished;
        }

        #endregion
    }
}
