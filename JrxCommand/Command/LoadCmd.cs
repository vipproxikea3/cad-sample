﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;
using OpenFileDialog = GrxCAD.Windows.OpenFileDialog;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using OpenFileDialog = Autodesk.AutoCAD.Windows.OpenFileDialog;

#endif

using System;
using System.Windows.Forms;
using Exception = System.Exception;
using JrxCad.Utility;
using JrxCad.FileIO;
using JrxCad.Helpers;

namespace JrxCad.Command
{
    public class LoadCmd : BaseCommand
    {
        [CommandMethod("JrxCommand", "SmxLoad", CommandFlags.Modal)]
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }

                var shxFilename = SelectShapeFile();
                if (string.IsNullOrEmpty(shxFilename)) return;

                Load(shxFilename);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        private string SelectShapeFile()
        {
            while (true)
            {
                var dlg = new OpenFileDialog("シェイプ ファイルを選択", null, "shx", "SelectShapeFile", OpenFileDialog.OpenFileDialogFlags.DoNotTransferRemoteFiles);
                if (dlg.ShowDialog() != DialogResult.OK) return null;
                var shxFilename = dlg.Filename;

                // SHXの妥当性（フォント系ならエラーになる）
                var tool = new ShapeFileTool();
                if (tool.CheckShx(shxFilename))
                {
                    return shxFilename;
                }

                switch (tool.Kind)
                {
                    case ShapeFileTool.ShapeType.UniFont10:
                    case ShapeFileTool.ShapeType.BigFont10:
                        Util.Editor().WriteMessage($"\n{shxFilename} はビッグフォント ファイルで、シェイプ ファイルではありません。");
                        break;
                    default:
                        Util.Editor().WriteMessage($"\nシェイプ ファイル {shxFilename} の読み込み中にエラー発生");
                        break;
                }
            }
        }

        private void Load(string shxFileName)
        {
            using (var tr = Util.StartTransaction())
            using (var textStyleTable = (TextStyleTable)tr.GetObject(Util.CurDoc().Database.TextStyleTableId, OpenMode.ForWrite))
            {
                if (textStyleTable.Exist(shxFileName)) return;

                var textStyle = new TextStyleTableRecord { IsShapeFile = true, FileName = shxFileName };
                textStyleTable.Add(textStyle);
                tr.AddNewlyCreatedDBObject(textStyle, true);
                tr.Commit();
            }
        }
    }
}
