﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

#endif
using System.Windows.Forms;
using Autodesk.AutoCAD.ApplicationServices;
using JrxCad.Helpers;
using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.Command.AttDef
{
    public class AttDefCmd : BaseCommand
    {
        [CommandMethod("JrxCommand", "SmxATTDEF", CommandFlags.Modal | CommandFlags.NoNewStack)]
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }

                using (var form = new FormAttDef())
                {
                    if (form.ShowDialog() != DialogResult.OK) return;
                    InsertAttdef(form);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        private void InsertAttdef(FormAttDef form)
        {
            try
            {
                var doc = Util.CurDoc();
                using (var tr = Util.StartTransaction())
                {
                    using (var attDef = new AttributeDefinition
                    {
                        Tag = form.txtTag.Text.ToUpper(),
                        Prompt = form.txtPrompt.Text,
                        Justify = (AttachmentPoint)form.cmbAdjust.SelectedValue,
                        TextStyleId = (ObjectId)form.cmbTextStyle.SelectedValue,
                        Height = form.txtSize.Value,
                        Rotation = form.txtRotate.Radian,
                        Invisible = form.chkInvisible.Checked,
                        Constant = form.chkConstant.Checked,
                        LockPositionInBlock = form.chkLockPos.Checked,
                        IsMTextAttributeDefinition = form.chkMText.Checked,
                    })
                    {
                        if (attDef.IsMTextAttributeDefinition)
                        {
                            var mtext = attDef.MTextAttributeDefinition;
                            mtext.Attachment = attDef.Justify;
                            mtext.TextStyleId = (ObjectId)form.cmbTextStyle.SelectedValue;
                            mtext.Height = form.txtSize.Value;
                            mtext.Rotation = form.txtRotate.Radian;
                            mtext.Width = form.txtWidth.Value;
                            attDef.MTextAttributeDefinition = mtext;
                        }
                        else
                        {
                            attDef.TextString = form.txtString.Text;
                        }

                        if (!form.chkConstant.Checked)
                        {
                            attDef.Verifiable = form.chkVerifiable.Checked;
                            attDef.Preset = form.chkPreset.Checked;
                        }

                        if (form.chkPos.Checked)
                        {
                            var jig = new EntityJigPosition(attDef);
                            var drag = doc.Editor.Drag(jig);
                            if (drag.Status != PromptStatus.OK)
                            {
                                return;
                            }
                        }
                        else
                        {
                            attDef.Position = new Point3d(form.txtPosX.Value, form.txtPosY.Value,
                                form.txtPosZ.Value);
                        }

                        if (attDef.Justify != AttachmentPoint.BaseLeft)
                        {
                            attDef.AlignmentPoint = attDef.Position;
                        }

                        tr.AddNewlyCreatedDBObject(attDef, true, tr.CurrentSpace());
                        tr.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        /// <summary>
        /// 基点変更
        /// </summary>
        private class EntityJigPosition : EntityJig
        {
            private Point3d _dragPtW;

            private AttributeDefinition _att;
            private readonly JigPromptPointOptions _jigOpt;

            public EntityJigPosition(AttributeDefinition att) : base(att)
            {
                _att = att;

                _jigOpt = new JigPromptPointOptions
                {
                    UserInputControls = UserInputControls.UseBasePointElevation |
                        UserInputControls.Accept3dCoordinates,
                    Message = $"\n始点を指定"
                };
            }

            protected override SamplerStatus Sampler(JigPrompts prompts)
            {
                var point = prompts.AcquirePoint(_jigOpt);
                _dragPtW = point.Value;
                if (point.Status == PromptStatus.OK)
                {
                    return SamplerStatus.OK;
                }

                return SamplerStatus.Cancel;
            }

            protected override bool Update()
            {
#if _CheckTool_
                if (!_att.IsWriteEnabled)
                {
                    _att.UpgradeOpen();
                }
#endif
                _att.Position = _dragPtW;
                return true;
            }
        }
    }
}
