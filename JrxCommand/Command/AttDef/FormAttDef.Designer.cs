﻿
using JrxCad.View;
using JrxCad.View.Custom;

namespace JrxCad.Command.AttDef
{
    partial class FormAttDef
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkMText = new System.Windows.Forms.CheckBox();
            this.chkLockPos = new System.Windows.Forms.CheckBox();
            this.chkPreset = new System.Windows.Forms.CheckBox();
            this.chkVerifiable = new System.Windows.Forms.CheckBox();
            this.chkConstant = new System.Windows.Forms.CheckBox();
            this.chkInvisible = new System.Windows.Forms.CheckBox();
            this.grpPos = new System.Windows.Forms.GroupBox();
            this.pnlPos = new System.Windows.Forms.Panel();
            this.label3 = new JrxCad.View.Custom.CustomLabel();
            this.label2 = new JrxCad.View.Custom.CustomLabel();
            this.label1 = new JrxCad.View.Custom.CustomLabel();
            this.txtPosZ = new JrxCad.View.Custom.CustomTextBoxUnit();
            this.txtPosY = new JrxCad.View.Custom.CustomTextBoxUnit();
            this.txtPosX = new JrxCad.View.Custom.CustomTextBoxUnit();
            this.chkPos = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pnlTextString = new System.Windows.Forms.Panel();
            this.btnTextString = new System.Windows.Forms.Button();
            this.txtString = new JrxCad.View.Custom.CustomTextBox();
            this.label6 = new JrxCad.View.Custom.CustomLabel();
            this.txtPrompt = new JrxCad.View.Custom.CustomTextBox();
            this.label5 = new JrxCad.View.Custom.CustomLabel();
            this.txtTag = new JrxCad.View.Custom.CustomTextBox();
            this.label4 = new JrxCad.View.Custom.CustomLabel();
            this.grpTextStyle = new System.Windows.Forms.GroupBox();
            this.pnlWidth = new System.Windows.Forms.Panel();
            this.btnWidth = new System.Windows.Forms.Button();
            this.txtWidth = new JrxCad.View.Custom.CustomTextBoxUnit();
            this.label11 = new JrxCad.View.Custom.CustomLabel();
            this.pnlTextSize = new System.Windows.Forms.Panel();
            this.btnTextSize = new System.Windows.Forms.Button();
            this.txtSize = new JrxCad.View.Custom.CustomTextBoxUnit();
            this.label7 = new JrxCad.View.Custom.CustomLabel();
            this.btnRotate = new System.Windows.Forms.Button();
            this.txtRotate = new JrxCad.View.Custom.CustomTextBoxAngle();
            this.label10 = new JrxCad.View.Custom.CustomLabel();
            this.chkDiffScale = new System.Windows.Forms.CheckBox();
            this.cmbTextStyle = new System.Windows.Forms.ComboBox();
            this.cmbAdjust = new System.Windows.Forms.ComboBox();
            this.label9 = new JrxCad.View.Custom.CustomLabel();
            this.label8 = new JrxCad.View.Custom.CustomLabel();
            this.chkPrev = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.grpPos.SuspendLayout();
            this.pnlPos.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.pnlTextString.SuspendLayout();
            this.grpTextStyle.SuspendLayout();
            this.pnlWidth.SuspendLayout();
            this.pnlTextSize.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnHelp
            // 
            this.btnHelp.Location = new System.Drawing.Point(377, 332);
            this.btnHelp.TabIndex = 7;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(271, 332);
            this.btnCancel.TabIndex = 6;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(165, 332);
            this.btnOk.TabIndex = 5;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkMText);
            this.groupBox1.Controls.Add(this.chkLockPos);
            this.groupBox1.Controls.Add(this.chkPreset);
            this.groupBox1.Controls.Add(this.chkVerifiable);
            this.groupBox1.Controls.Add(this.chkConstant);
            this.groupBox1.Controls.Add(this.chkInvisible);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 156);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "モード";
            // 
            // chkMText
            // 
            this.chkMText.AutoSize = true;
            this.chkMText.Location = new System.Drawing.Point(6, 128);
            this.chkMText.Name = "chkMText";
            this.chkMText.Size = new System.Drawing.Size(104, 16);
            this.chkMText.TabIndex = 5;
            this.chkMText.Text = "マルチテキスト(&U)";
            this.chkMText.UseVisualStyleBackColor = true;
            this.chkMText.CheckedChanged += new System.EventHandler(this.chkMText_CheckedChanged);
            // 
            // chkLockPos
            // 
            this.chkLockPos.AutoSize = true;
            this.chkLockPos.Checked = true;
            this.chkLockPos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLockPos.Location = new System.Drawing.Point(6, 106);
            this.chkLockPos.Name = "chkLockPos";
            this.chkLockPos.Size = new System.Drawing.Size(96, 16);
            this.chkLockPos.TabIndex = 4;
            this.chkLockPos.Text = "位置を固定(&K)";
            this.chkLockPos.UseVisualStyleBackColor = true;
            // 
            // chkPreset
            // 
            this.chkPreset.AutoSize = true;
            this.chkPreset.Location = new System.Drawing.Point(6, 84);
            this.chkPreset.Name = "chkPreset";
            this.chkPreset.Size = new System.Drawing.Size(80, 16);
            this.chkPreset.TabIndex = 3;
            this.chkPreset.Text = "プリセット(&P)";
            this.chkPreset.UseVisualStyleBackColor = true;
            // 
            // chkVerifiable
            // 
            this.chkVerifiable.AutoSize = true;
            this.chkVerifiable.Location = new System.Drawing.Point(6, 62);
            this.chkVerifiable.Name = "chkVerifiable";
            this.chkVerifiable.Size = new System.Drawing.Size(64, 16);
            this.chkVerifiable.TabIndex = 2;
            this.chkVerifiable.Text = "確認(&V)";
            this.chkVerifiable.UseVisualStyleBackColor = true;
            // 
            // chkConstant
            // 
            this.chkConstant.AutoSize = true;
            this.chkConstant.Location = new System.Drawing.Point(6, 40);
            this.chkConstant.Name = "chkConstant";
            this.chkConstant.Size = new System.Drawing.Size(64, 16);
            this.chkConstant.TabIndex = 1;
            this.chkConstant.Text = "一定(&C)";
            this.chkConstant.UseVisualStyleBackColor = true;
            this.chkConstant.CheckedChanged += new System.EventHandler(this.chkConstant_CheckedChanged);
            // 
            // chkInvisible
            // 
            this.chkInvisible.AutoSize = true;
            this.chkInvisible.Location = new System.Drawing.Point(6, 18);
            this.chkInvisible.Name = "chkInvisible";
            this.chkInvisible.Size = new System.Drawing.Size(71, 16);
            this.chkInvisible.TabIndex = 0;
            this.chkInvisible.Text = "非表示(&I)";
            this.chkInvisible.UseVisualStyleBackColor = true;
            // 
            // grpPos
            // 
            this.grpPos.Controls.Add(this.pnlPos);
            this.grpPos.Controls.Add(this.chkPos);
            this.grpPos.Location = new System.Drawing.Point(12, 174);
            this.grpPos.Name = "grpPos";
            this.grpPos.Size = new System.Drawing.Size(200, 117);
            this.grpPos.TabIndex = 0;
            this.grpPos.TabStop = false;
            this.grpPos.Text = "挿入点";
            // 
            // pnlPos
            // 
            this.pnlPos.Controls.Add(this.label3);
            this.pnlPos.Controls.Add(this.label2);
            this.pnlPos.Controls.Add(this.label1);
            this.pnlPos.Controls.Add(this.txtPosZ);
            this.pnlPos.Controls.Add(this.txtPosY);
            this.pnlPos.Controls.Add(this.txtPosX);
            this.pnlPos.Enabled = false;
            this.pnlPos.Location = new System.Drawing.Point(6, 40);
            this.pnlPos.Name = "pnlPos";
            this.pnlPos.Size = new System.Drawing.Size(161, 69);
            this.pnlPos.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label3.Location = new System.Drawing.Point(3, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "&Z：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label2.Location = new System.Drawing.Point(3, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "&Y：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "&X：";
            // 
            // txtPosZ
            // 
            this.txtPosZ.Location = new System.Drawing.Point(32, 50);
            this.txtPosZ.Name = "txtPosZ";
            this.txtPosZ.Size = new System.Drawing.Size(125, 19);
            this.txtPosZ.TabIndex = 5;
            this.txtPosZ.TooltipText = null;
            // 
            // txtPosY
            // 
            this.txtPosY.Location = new System.Drawing.Point(32, 25);
            this.txtPosY.Name = "txtPosY";
            this.txtPosY.Size = new System.Drawing.Size(125, 19);
            this.txtPosY.TabIndex = 3;
            this.txtPosY.TooltipText = null;
            // 
            // txtPosX
            // 
            this.txtPosX.Location = new System.Drawing.Point(32, 0);
            this.txtPosX.Name = "txtPosX";
            this.txtPosX.Size = new System.Drawing.Size(125, 19);
            this.txtPosX.TabIndex = 1;
            this.txtPosX.TooltipText = null;
            // 
            // chkPos
            // 
            this.chkPos.AutoSize = true;
            this.chkPos.Checked = true;
            this.chkPos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPos.Location = new System.Drawing.Point(6, 18);
            this.chkPos.Name = "chkPos";
            this.chkPos.Size = new System.Drawing.Size(109, 16);
            this.chkPos.TabIndex = 0;
            this.chkPos.Text = "画面上で指定(&S)";
            this.chkPos.UseVisualStyleBackColor = true;
            this.chkPos.CheckedChanged += new System.EventHandler(this.chkPos_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pnlTextString);
            this.groupBox3.Controls.Add(this.txtPrompt);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtTag);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(218, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(262, 100);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "属性";
            // 
            // pnlTextString
            // 
            this.pnlTextString.Controls.Add(this.btnTextString);
            this.pnlTextString.Controls.Add(this.txtString);
            this.pnlTextString.Controls.Add(this.label6);
            this.pnlTextString.Location = new System.Drawing.Point(3, 64);
            this.pnlTextString.Name = "pnlTextString";
            this.pnlTextString.Size = new System.Drawing.Size(254, 23);
            this.pnlTextString.TabIndex = 4;
            // 
            // btnTextString
            // 
            this.btnTextString.Location = new System.Drawing.Point(220, 0);
            this.btnTextString.Name = "btnTextString";
            this.btnTextString.Size = new System.Drawing.Size(27, 23);
            this.btnTextString.TabIndex = 2;
            this.btnTextString.UseVisualStyleBackColor = true;
            // 
            // txtString
            // 
            this.txtString.Location = new System.Drawing.Point(77, 2);
            this.txtString.Name = "txtString";
            this.txtString.Size = new System.Drawing.Size(142, 19);
            this.txtString.TabIndex = 1;
            this.txtString.TooltipText = null;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label6.Location = new System.Drawing.Point(3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "既定値(&L):";
            // 
            // txtPrompt
            // 
            this.txtPrompt.Location = new System.Drawing.Point(80, 41);
            this.txtPrompt.Name = "txtPrompt";
            this.txtPrompt.Size = new System.Drawing.Size(170, 19);
            this.txtPrompt.TabIndex = 3;
            this.txtPrompt.TooltipText = null;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Location = new System.Drawing.Point(6, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "プロンプト(&M):";
            // 
            // txtTag
            // 
            this.txtTag.Location = new System.Drawing.Point(80, 16);
            this.txtTag.Name = "txtTag";
            this.txtTag.Size = new System.Drawing.Size(170, 19);
            this.txtTag.TabIndex = 1;
            this.txtTag.TooltipText = null;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label4.Location = new System.Drawing.Point(6, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "名称(&T):";
            // 
            // grpTextStyle
            // 
            this.grpTextStyle.Controls.Add(this.pnlWidth);
            this.grpTextStyle.Controls.Add(this.pnlTextSize);
            this.grpTextStyle.Controls.Add(this.btnRotate);
            this.grpTextStyle.Controls.Add(this.txtRotate);
            this.grpTextStyle.Controls.Add(this.label10);
            this.grpTextStyle.Controls.Add(this.chkDiffScale);
            this.grpTextStyle.Controls.Add(this.cmbTextStyle);
            this.grpTextStyle.Controls.Add(this.cmbAdjust);
            this.grpTextStyle.Controls.Add(this.label9);
            this.grpTextStyle.Controls.Add(this.label8);
            this.grpTextStyle.Location = new System.Drawing.Point(218, 118);
            this.grpTextStyle.Name = "grpTextStyle";
            this.grpTextStyle.Size = new System.Drawing.Size(262, 173);
            this.grpTextStyle.TabIndex = 2;
            this.grpTextStyle.TabStop = false;
            this.grpTextStyle.Text = "文字設定";
            // 
            // pnlWidth
            // 
            this.pnlWidth.Controls.Add(this.btnWidth);
            this.pnlWidth.Controls.Add(this.txtWidth);
            this.pnlWidth.Controls.Add(this.label11);
            this.pnlWidth.Enabled = false;
            this.pnlWidth.Location = new System.Drawing.Point(3, 139);
            this.pnlWidth.Name = "pnlWidth";
            this.pnlWidth.Size = new System.Drawing.Size(255, 23);
            this.pnlWidth.TabIndex = 9;
            // 
            // btnWidth
            // 
            this.btnWidth.Location = new System.Drawing.Point(220, 0);
            this.btnWidth.Name = "btnWidth";
            this.btnWidth.Size = new System.Drawing.Size(27, 23);
            this.btnWidth.TabIndex = 2;
            this.btnWidth.UseVisualStyleBackColor = true;
            // 
            // txtWidth
            // 
            this.txtWidth.Location = new System.Drawing.Point(91, 2);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(128, 19);
            this.txtWidth.TabIndex = 1;
            this.txtWidth.TooltipText = null;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label11.Location = new System.Drawing.Point(3, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "境界の幅(&W):";
            // 
            // pnlTextSize
            // 
            this.pnlTextSize.Controls.Add(this.btnTextSize);
            this.pnlTextSize.Controls.Add(this.txtSize);
            this.pnlTextSize.Controls.Add(this.label7);
            this.pnlTextSize.Location = new System.Drawing.Point(3, 91);
            this.pnlTextSize.Name = "pnlTextSize";
            this.pnlTextSize.Size = new System.Drawing.Size(256, 23);
            this.pnlTextSize.TabIndex = 5;
            // 
            // btnTextSize
            // 
            this.btnTextSize.Location = new System.Drawing.Point(220, 0);
            this.btnTextSize.Name = "btnTextSize";
            this.btnTextSize.Size = new System.Drawing.Size(27, 23);
            this.btnTextSize.TabIndex = 2;
            this.btnTextSize.UseVisualStyleBackColor = true;
            // 
            // txtSize
            // 
            this.txtSize.Location = new System.Drawing.Point(91, 2);
            this.txtSize.Name = "txtSize";
            this.txtSize.Size = new System.Drawing.Size(128, 19);
            this.txtSize.TabIndex = 1;
            this.txtSize.TooltipText = null;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label7.Location = new System.Drawing.Point(3, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "文字高さ(&E):";
            // 
            // btnRotate
            // 
            this.btnRotate.Location = new System.Drawing.Point(223, 115);
            this.btnRotate.Name = "btnRotate";
            this.btnRotate.Size = new System.Drawing.Size(27, 23);
            this.btnRotate.TabIndex = 8;
            this.btnRotate.UseVisualStyleBackColor = true;
            // 
            // txtRotate
            // 
            this.txtRotate.Location = new System.Drawing.Point(94, 117);
            this.txtRotate.MaxLength = 10;
            this.txtRotate.Name = "txtRotate";
            this.txtRotate.Size = new System.Drawing.Size(128, 19);
            this.txtRotate.TabIndex = 7;
            this.txtRotate.TooltipText = null;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label10.Location = new System.Drawing.Point(6, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 12);
            this.label10.TabIndex = 6;
            this.label10.Text = "回転角度(&R):";
            // 
            // chkDiffScale
            // 
            this.chkDiffScale.AutoSize = true;
            this.chkDiffScale.Location = new System.Drawing.Point(8, 69);
            this.chkDiffScale.Name = "chkDiffScale";
            this.chkDiffScale.Size = new System.Drawing.Size(100, 16);
            this.chkDiffScale.TabIndex = 4;
            this.chkDiffScale.Text = "異尺度対応(&N)";
            this.chkDiffScale.UseVisualStyleBackColor = true;
            // 
            // cmbTextStyle
            // 
            this.cmbTextStyle.DisplayMember = "Name";
            this.cmbTextStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTextStyle.FormattingEnabled = true;
            this.cmbTextStyle.Location = new System.Drawing.Point(94, 43);
            this.cmbTextStyle.Name = "cmbTextStyle";
            this.cmbTextStyle.Size = new System.Drawing.Size(156, 20);
            this.cmbTextStyle.TabIndex = 3;
            this.cmbTextStyle.ValueMember = "Id";
            this.cmbTextStyle.SelectedIndexChanged += new System.EventHandler(this.cmbTextStyle_SelectedIndexChanged);
            // 
            // cmbAdjust
            // 
            this.cmbAdjust.DisplayMember = "Text";
            this.cmbAdjust.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAdjust.FormattingEnabled = true;
            this.cmbAdjust.Location = new System.Drawing.Point(94, 18);
            this.cmbAdjust.Name = "cmbAdjust";
            this.cmbAdjust.Size = new System.Drawing.Size(156, 20);
            this.cmbAdjust.TabIndex = 1;
            this.cmbAdjust.ValueMember = "Attach";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label9.Location = new System.Drawing.Point(6, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 12);
            this.label9.TabIndex = 2;
            this.label9.Text = "文字スタイル(&S):";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label8.Location = new System.Drawing.Point(6, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "位置合わせ(&J):";
            // 
            // chkPrev
            // 
            this.chkPrev.AutoSize = true;
            this.chkPrev.Enabled = false;
            this.chkPrev.Location = new System.Drawing.Point(18, 297);
            this.chkPrev.Name = "chkPrev";
            this.chkPrev.Size = new System.Drawing.Size(187, 16);
            this.chkPrev.TabIndex = 4;
            this.chkPrev.Text = "直前の属性定義に位置合わせ(&A)";
            this.chkPrev.UseVisualStyleBackColor = true;
            this.chkPrev.CheckedChanged += new System.EventHandler(this.chkPrev_CheckedChanged);
            // 
            // FormAttDef
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(489, 367);
            this.Controls.Add(this.chkPrev);
            this.Controls.Add(this.grpTextStyle);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.grpPos);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormAttDef";
            this.Text = "属性定義";
            this.Load += new System.EventHandler(this.FormAttDef_Load);
            this.Controls.SetChildIndex(this.btnOk, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnHelp, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.grpPos, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.Controls.SetChildIndex(this.grpTextStyle, 0);
            this.Controls.SetChildIndex(this.chkPrev, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpPos.ResumeLayout(false);
            this.grpPos.PerformLayout();
            this.pnlPos.ResumeLayout(false);
            this.pnlPos.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.pnlTextString.ResumeLayout(false);
            this.pnlTextString.PerformLayout();
            this.grpTextStyle.ResumeLayout(false);
            this.grpTextStyle.PerformLayout();
            this.pnlWidth.ResumeLayout(false);
            this.pnlWidth.PerformLayout();
            this.pnlTextSize.ResumeLayout(false);
            this.pnlTextSize.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grpPos;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox grpTextStyle;
        private System.Windows.Forms.Panel pnlPos;
        private CustomLabel label3;
        private CustomLabel label2;
        private CustomLabel label1;
        private System.Windows.Forms.Button btnTextString;
        private CustomLabel label6;
        private CustomLabel label5;
        private CustomLabel label4;
        private System.Windows.Forms.Button btnWidth;
        private CustomLabel label11;
        private System.Windows.Forms.Button btnRotate;
        private CustomLabel label10;
        private System.Windows.Forms.Button btnTextSize;
        private CustomLabel label9;
        private CustomLabel label8;
        private CustomLabel label7;
        private System.Windows.Forms.CheckBox chkPrev;
        private System.Windows.Forms.Panel pnlWidth;
        private System.Windows.Forms.Panel pnlTextSize;
        private System.Windows.Forms.Panel pnlTextString;
        public System.Windows.Forms.CheckBox chkMText;
        public System.Windows.Forms.CheckBox chkLockPos;
        public System.Windows.Forms.CheckBox chkPreset;
        public System.Windows.Forms.CheckBox chkVerifiable;
        public System.Windows.Forms.CheckBox chkConstant;
        public System.Windows.Forms.CheckBox chkInvisible;
        public CustomTextBoxUnit txtPosZ;
        public CustomTextBoxUnit txtPosY;
        public CustomTextBoxUnit txtPosX;
        public System.Windows.Forms.CheckBox chkPos;
        public CustomTextBox txtString;
        public CustomTextBox txtPrompt;
        public CustomTextBox txtTag;
        public CustomTextBoxUnit txtWidth;
        public CustomTextBoxAngle txtRotate;
        public System.Windows.Forms.CheckBox chkDiffScale;
        public System.Windows.Forms.ComboBox cmbTextStyle;
        public System.Windows.Forms.ComboBox cmbAdjust;
        public CustomTextBoxUnit txtSize;
    }
}
