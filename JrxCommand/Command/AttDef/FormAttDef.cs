﻿#if _IJCAD_
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using CADException = Autodesk.AutoCAD.Runtime.Exception;
using CADColors = Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Autodesk.AutoCAD.ApplicationServices;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCad.View.Custom;
using Exception = System.Exception;

namespace JrxCad.Command.AttDef
{
    public partial class FormAttDef : CustomForm
    {
        protected override string HelpName => "AttDef";

        private class Adjust
        {
            public string Text { get; }
            public AttachmentPoint Attach { get; }
            public bool IsMText { get; set; }

            public Adjust(string text, AttachmentPoint attach, bool isMText)
            {
                Text = text;
                Attach = attach;
                IsMText = isMText;
            }
        }

        private class TextStyle
        {
            public string Name { get; set; }
            public ObjectId Id { get; set; }
            public bool IsDiffScale { get; set; }
            public double TextSize { get; set; }
        }

        private List<Adjust> _adjusts;

        public FormAttDef()
        {
            InitializeComponent();
        }

        private void FormAttDef_Load(object sender, EventArgs e)
        {
            //位置合わせ
            _adjusts = new List<Adjust>
            {
                new Adjust("左寄せ(L)", AttachmentPoint.BaseLeft, false),
                new Adjust("中心(C)", AttachmentPoint.BaseCenter, false),
                new Adjust("右寄せ(R)", AttachmentPoint.BaseRight, false),
                new Adjust("両端揃え(A)", AttachmentPoint.BaseAlign, false),
                new Adjust("中央(M)", AttachmentPoint.BaseMid, false),
                new Adjust("フィット(F)", AttachmentPoint.BaseFit, false),
                new Adjust("左上(TL)", AttachmentPoint.TopLeft, true),
                new Adjust("上中心(TC)", AttachmentPoint.TopCenter, true),
                new Adjust("右上(TR)", AttachmentPoint.TopRight, true),
                new Adjust("左中央(ML)", AttachmentPoint.MiddleLeft, true),
                new Adjust("中央(MC)", AttachmentPoint.MiddleCenter, true),
                new Adjust("右中央(MR)", AttachmentPoint.MiddleRight, true),
                new Adjust("左下(BL)", AttachmentPoint.BottomLeft, true),
                new Adjust("下中心(BC)", AttachmentPoint.BottomCenter, true),
                new Adjust("右下(BR)", AttachmentPoint.BottomRight, true)
            };

            cmbAdjust.DataSource = _adjusts;

            //文字スタイル
            var textStyles = new List<TextStyle>();
            var doc = Util.CurDoc();
            using (var tr = Util.StartTransaction())
            using (var textStyleTable = (TextStyleTable)tr.GetObject(doc.Database.TextStyleTableId, OpenMode.ForRead))
            {
                foreach (var textStyleId in textStyleTable)
                {
                    var textStyle = tr.GetObject(textStyleId, OpenMode.ForRead) as TextStyleTableRecord;

                    //異尺度対応かどうか
                    var isDiffScale = false;
                    if (textStyle.XData != null)
                    {
                        using (var annotative = textStyle.GetXDataForApplication("AcadAnnotative"))
                        {
                            isDiffScale = annotative != null;
                        }
                    }

                    textStyles.Add(new TextStyle
                    {
                        Id = textStyle.Id,
                        Name = textStyle.Name,
                        TextSize = textStyle.TextSize,
                        IsDiffScale = isDiffScale,
                    });
                }
            }

            cmbTextStyle.DataSource = textStyles;
            cmbTextStyle.Text = SystemVariable.GetString("TextStyle");

            //モード
            var aflags = SystemVariable.GetInt("AFlags");
            chkInvisible.Checked = (aflags & 1) == 1;
            chkConstant.Checked = (aflags & 2) == 2;
            chkVerifiable.Checked = (aflags & 4) == 4;
            chkPreset.Checked = (aflags & 8) == 8;
            chkLockPos.Checked = (aflags & 16) == 16;
            chkMText.Checked = (aflags & 32) == 32;

            //TODO:前回値反映
        }

        /// <summary>
        /// OK
        /// </summary>
        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtTag.Text))
                {
                    MsgBox.Error.Show("名称は空にできません。");
                    return;
                }
                if (txtTag.Text.Contains(" ") || txtTag.Text.Contains("　"))
                {
                    MsgBox.Error.Show("名称にスペースを含むことはできません。");
                    return;
                }

                if (txtSize.Value.OrLess(0))
                {
                    MsgBox.Error.Show("文字の高さには正の値を設定する必要があります。");
                    return;
                }

                if (txtWidth.Value.Less(0))
                {
                    MsgBox.Error.Show("マルチテキスト属性の幅には正の値を設定する必要があります。");
                    return;
                }

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        /// <summary>
        /// 文字スタイル
        /// </summary>
        private void cmbTextStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var textStyle = cmbTextStyle.SelectedItem as TextStyle;
                chkDiffScale.Checked = textStyle.IsDiffScale;
                if (textStyle.TextSize.More(0))
                {
                    pnlTextSize.Enabled = false;
                    txtSize.Value = textStyle.TextSize;
                }
                else
                {
                    pnlTextSize.Enabled = true;
                    if (string.IsNullOrEmpty(txtSize.Text) || txtSize.Value.IsZero())
                    {
                        txtSize.Value = double.Parse(SystemVariable.GetString("TextSize"));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        /// <summary>
        /// 一定
        /// </summary>
        private void chkConstant_CheckedChanged(object sender, EventArgs e)
        {
            chkVerifiable.Enabled = !chkConstant.Checked;
            chkPreset.Enabled = !chkConstant.Checked;
        }

        /// <summary>
        /// マルチテキスト
        /// </summary>
        private void chkMText_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                var prevText = cmbAdjust.Text;

                if (chkMText.Checked)
                {
                    cmbAdjust.DataSource = _adjusts.Where(r => r.IsMText).ToList();
                    pnlTextString.Enabled = false;
                    pnlWidth.Enabled = true;
                }
                else
                {
                    cmbAdjust.DataSource = _adjusts;
                    pnlTextString.Enabled = true;
                    pnlWidth.Enabled = false;
                }

                cmbAdjust.Text = prevText;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        /// <summary>
        /// 画面上で指定
        /// </summary>
        private void chkPos_CheckedChanged(object sender, EventArgs e)
        {
            pnlPos.Enabled = !chkPos.Checked;
        }

        /// <summary>
        /// 直前の属性定義に位置合わせ
        /// </summary>
        private void chkPrev_CheckedChanged(object sender, EventArgs e)
        {
            grpPos.Enabled = !chkPrev.Checked;
            grpTextStyle.Enabled = !chkPrev.Checked;
        }
    }
}
