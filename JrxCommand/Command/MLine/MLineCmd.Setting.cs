﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.Colors;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using Application = Autodesk.AutoCAD.ApplicationServices.Application;
using System.Reflection;

#endif
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Exception = System.Exception;
using JrxCad.Helpers;
using JrxCad.Utility;

namespace JrxCad.Command.MLine
{
    public partial class MLineCmd
    {
        private string GetMLineJustStr()
        {
            try
            {
                var cmlJust = SystemVariable.GetInt("CMLJUST");
                switch (cmlJust)
                {
                    case (int)MlineJustification.Top:
                        return MLineRes.UI_OP1_Top;
                    case (int)MlineJustification.Zero:
                        return MLineRes.UI_OP1_Zero;
                    case (int)MlineJustification.Bottom:
                        return MLineRes.UI_OP1_Bottom;
                    default:
                        return string.Empty;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
                return string.Empty;
            }
        }

        /// <summary>
        /// Change the MLine justification.
        /// </summary>
        private void SetJustification()
        {
            try
            {
                var optJustification = new PromptKeywordOptions($"\n{MLineRes.UI_OP1}")
                {
                    Keywords = {
                    {"top", MLineRes.UI_OP1_Top, MLineRes.UI_OP1_Top },
                    {"zero", MLineRes.UI_OP1_Zero, MLineRes.UI_OP1_Zero },
                    {"bottom", MLineRes.UI_OP1_Bottom, MLineRes.UI_OP1_Bottom} }
                };
                optJustification.Keywords.Default = GetMLineJustStr().ToLower();
                var resMLineJust = _userInput.GetKeyword(optJustification);
                if (resMLineJust.Status == PromptStatus.OK)
                {
                    switch (resMLineJust.StringResult)
                    {
                        case "top":
                            SystemVariable.SetValue("CMLJUST", (int)MlineJustification.Top);
                            break;
                        case "zero":
                            SystemVariable.SetValue("CMLJUST", (int)MlineJustification.Zero);
                            break;
                        case "bottom":
                            SystemVariable.SetValue("CMLJUST", (int)MlineJustification.Bottom);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        /// <summary>
        /// Change the MLine scale.
        /// </summary>
        private void SetScale()
        {
            try
            {
                var optScale = new PromptDoubleOptions($"\n{MLineRes.UI_OP2}")
                {
                    DefaultValue = SystemVariable.GetDouble("CMLSCALE")
                };
                var resScale = _userInput.GetDouble(optScale);
                if (resScale.Status == PromptStatus.OK)
                {
                    SystemVariable.SetValue("CMLSCALE", resScale.Value);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        /// <summary>
        /// Change the MLine style.
        /// </summary>
        private void SetStyle()
        {
            var database = Util.Database();
            while (true)
            {
                var optStyle = new PromptStringOptions($"\n{MLineRes.UI_OP3}");
                var resStyle = _userInput.GetString(optStyle);
                if (resStyle.Status != PromptStatus.OK || string.IsNullOrEmpty(resStyle.StringResult))
                {
                    break;
                }

                using (var tr = Util.StartTransaction())
                using (var mLStyleDicId = tr.GetObject<DBDictionary>(database.MLStyleDictionaryId, OpenMode.ForRead))
                {
                    if (resStyle.StringResult == "?")
                    {
                        var infoMLineDic = new Dictionary<string, string>();
                        foreach (var item in mLStyleDicId)
                        {
                            using (var mLineStyle = tr.GetObject<MlineStyle>(item.Value, OpenMode.ForRead))
                            {
                                infoMLineDic.Add(mLineStyle.Name, mLineStyle.Description);
                            }
                        }

                        WriteMessage(MLineRes.CM_OP4);

                        _notify?.AppendMLineCmOp4(MLineRes.CM_OP4);

                        foreach (var item in infoMLineDic.OrderBy(n => n.Key))
                        {
                            // If the length of item.Key is less than 16 then spaces are inserted to the right
                            WriteMessage($"\n{item.Key,-16} {item.Value}");
                        }
                    }
                    else
                    {
                        var isFindMLineStyle = FindMLineStyle(database, tr, mLStyleDicId, resStyle.StringResult);
                        tr.Commit();
                        if (isFindMLineStyle)
                        {
                            break;
                        }
                    }
                }
            }
        }

        private bool FindMLineStyle(Database database, Transaction tr, DBDictionary mLStyleDicId, string styleName)
        {
            foreach (var item in mLStyleDicId)
            {
                if (string.Equals(item.Key, styleName, StringComparison.OrdinalIgnoreCase))
                {
                    database.CmlstyleID = item.Value;
                    return true;
                }
            }

            var optOpenFile = new PromptOpenFileOptions($"{MLineRes.DialogCaption}")
            {
                DialogName = "MlineFileDialog",
                Filter = "|*.mln",
                AllowUrls = false,
            };

            // Check the existence of MLineFileDialog\FileNameMRU0
            var currentUser = new Hkey.CurrentUser();
            var subKey = currentUser.GetProfileSubKey() + @"\Dialogs\MlineFileDialog";
            var initialDirectory = currentUser.GetValue(subKey, "InitialDirectory");
            if (initialDirectory != null)
            {
                var fileNameMru0 = currentUser.GetValue(subKey, "FileNameMRU0");
                if (fileNameMru0 != null)
                {
                    optOpenFile.InitialFileName = fileNameMru0;
                }
            }
            else
            {
                // Get SupportPath
                var supportPath = SystemVariable.GetString("ACADPREFIX");

                // Find the first file with the extension ".mln" in supportPath
                foreach (var item in supportPath.Split(';'))
                {
                    if (Directory.Exists(item))
                    {
                        var dir = new DirectoryInfo(item);
                        var filePaths = dir.GetFiles("*.mln");
                        if (filePaths.Length > 0)
                        {
#if _IJCAD_
                            if (!currentUser.IsExist(subKey))
                            {
                                currentUser.CreateSubKey(subKey);
                            }

                            currentUser.SetValue(subKey, "InitialFilterIndex", 0, Microsoft.Win32.RegistryValueKind.DWord);
                            currentUser.SetValue(subKey, "InitialDirectory", item + "\\", Microsoft.Win32.RegistryValueKind.String);
#endif
                            optOpenFile.InitialDirectory = item;
                            optOpenFile.InitialFileName = filePaths[0].Name;
                            break;
                        }
                    }
                }
            }

            var resOpenFile = _userInput.GetFileName(optOpenFile);
            if (resOpenFile.Status != PromptStatus.OK)
            {
                return false;
            }

            var fileName = resOpenFile.StringResult;
            if (!File.Exists(fileName))
            {
                WriteMessage(MLineRes.CM_DLG1, styleName.ToUpper(), resOpenFile.StringResult);
                return false;
            }

            currentUser.SetValue(subKey, "FileNameMRU0", fileName, Microsoft.Win32.RegistryValueKind.String);
#if _IJCAD_
            database.LoadMlineStyleFile(styleName, fileName);
            using (var newMLineStyleIdDic = tr.GetObject<DBDictionary>(database.MLStyleDictionaryId, OpenMode.ForRead))
            {
                foreach (var item in newMLineStyleIdDic)
                {
                    if (string.Equals(item.Key, styleName, StringComparison.OrdinalIgnoreCase))
                    {
                        database.CmlstyleID = item.Value;
                        return true;
                    }
                }
            }

            WriteMessage(MLineRes.CM_DLG1, styleName.ToUpper(), resOpenFile.StringResult);
#elif _AutoCAD_

            var isAddSupportPath = AddSupportPath(Path.GetDirectoryName(resOpenFile.StringResult));
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
            if (!string.Equals(styleName, fileNameWithoutExtension, System.StringComparison.OrdinalIgnoreCase))
            {
                fileName = RenameFile(styleName, resOpenFile.StringResult);
            }

            try
            {
                Util.Database().LoadMlineStyleFile(styleName, fileName);
                //tr.Commit();
                return true;
            }
            catch (Exception)
            {
                WriteMessage(MLineRes.CM_DLG1, styleName.ToUpper(), resOpenFile.StringResult);
            }
            finally
            {
                RenameFile(fileNameWithoutExtension, fileName);
                if (isAddSupportPath)
                {
                    RemovePathFromSupportPath(Path.GetDirectoryName(resOpenFile.StringResult));
                }
            }
#endif
            return false;
        }

#if _AutoCAD_

        // The "LoadMLineStyleFile" function in autoCad throw an error when the nameStyle is not the same as the nameFile
        // so we need RenameFile and AddSupportPath

        /// <summary>
        /// Rename the file selected by the user to a new name.
        /// </summary>
        /// <returns>Full path to the file after rename.</returns>
        private string RenameFile(string newName, string sourcePath)
        {
            try
            {
                var fileName = Path.GetFileNameWithoutExtension(sourcePath);
                var folderPath = Path.GetDirectoryName(sourcePath);
                if (newName == fileName || folderPath == null)
                {
                    return sourcePath;
                }

                var fileExt = Path.GetExtension(sourcePath);
                var slashOrEmpty = (folderPath[folderPath.Length - 1] != '\\') ? "\\" : "";
                var newFilePath = folderPath + slashOrEmpty + newName + fileExt;
                File.Move(sourcePath, newFilePath);
                return newFilePath;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
                return sourcePath;
            }
        }

        /// <summary>
        /// Add path to AutoCad's support path.
        /// </summary>
        /// <returns>Returns true if AutoCad's support path changes otherwise false.</returns>
        private static bool AddSupportPath(string path)
        {
            try
            {
                var aCadApp = Application.AcadApplication;
                var preferences = aCadApp.GetType().InvokeMember("Preferences", BindingFlags.GetProperty, null, aCadApp, null);
                var files = preferences.GetType().InvokeMember("Files", BindingFlags.GetProperty, null, preferences, null);
                var supportPath = (string)files.GetType().InvokeMember("SupportPath", BindingFlags.GetProperty, null, files, null);

                // Check the existence of path in supportPath
                var isSupport = supportPath.Contains(path + ";") || supportPath.Split(';')[supportPath.Split(';').Length - 1] == path;
                if (isSupport)
                {
                    return false;
                }
                supportPath += ";" + path;
                files.GetType().InvokeMember("SupportPath", BindingFlags.SetProperty, null, files, new object[] { supportPath });
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
                return false;
            }
        }

        /// <summary>
        /// Remove path from AutoCad's support path.
        /// </summary>
        private static void RemovePathFromSupportPath(string path)
        {
            try
            {
                var aCadApp = Application.AcadApplication;
                var preferences = aCadApp.GetType().InvokeMember("Preferences", BindingFlags.GetProperty, null, aCadApp, null);
                var files = preferences.GetType().InvokeMember("Files", BindingFlags.GetProperty, null, preferences, null);
                var supportPath = (string)files.GetType().InvokeMember("SupportPath", BindingFlags.GetProperty, null, files, null);

                // Remove last path in supportPath
                var newSupportPath = supportPath.Remove(supportPath.Length - path.Length - 1, path.Length + 1);
                files.GetType().InvokeMember("SupportPath", BindingFlags.SetProperty, null, files, new object[] { newSupportPath });
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }
#endif
    }
}
