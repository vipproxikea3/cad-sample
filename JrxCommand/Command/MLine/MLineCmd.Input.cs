﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.GraphicsInterface;

#endif
using JrxCad.Utility;

namespace JrxCad.Command.MLine
{
    public partial class MLineCmd
    {
        private readonly IUserInput _userInput;
        private readonly IMLineNotify _notify;

        /// <summary>
        /// IUserInput Interface
        /// </summary>
        public interface IUserInput : IUserInputBase
        {
            PromptResult GetFileName(PromptOpenFileOptions options);
        }

        public interface IMLineNotify : INotifyId
        {
            void AppendMLineCmM1(string cMess);

            void AppendMLineCmOp4(string cMess);
        }

        /// <summary>
        /// UserInputImpl
        /// </summary>
        private class UserInput : UserInputBase, IUserInput
        {
            public PromptResult GetFileName(PromptOpenFileOptions promptFile)
            {
                return Util.Editor().GetFileNameForOpen(promptFile);
            }
        }

        public class MLineJigActions : PointDrawJigN.JigActions
        {
            private readonly Mline _mLine;

            public MLineJigActions(Mline mLine)
            {
                _mLine = mLine;
                const int minVerticesToUndo = 3;
                if (_mLine.NumberOfVertices < minVerticesToUndo)
                {
                    Options = new JigPromptPointOptions($"\n{MLineRes.UI_NP1}")
                    {
                        AppendKeywordsToMessage = false,
                        Keywords = { { "U", MLineRes.UI_Key_Undo, MLineRes.UI_Key_Undo } }
                    };
                }
                else if (_mLine.NumberOfVertices == minVerticesToUndo)
                {
                    Options = new JigPromptPointOptions($"\n{MLineRes.UI_NP2}")
                    {
                        Keywords = { { "U", MLineRes.UI_Key_Undo, MLineRes.UI_Key_Undo } }
                    };
                }
                else
                {
                    Options = new JigPromptPointOptions($"\n{MLineRes.UI_NP3}")
                    {
                        Keywords = {
                        { "C", MLineRes.UI_Key_Close, MLineRes.UI_Key_Close },
                        { "U", MLineRes.UI_Key_Undo, MLineRes.UI_Key_Undo }}
                    };
                }

                Options.UseBasePoint = true;
                var indexOfBaseVertex = _mLine.NumberOfVertices - 2;
                if (indexOfBaseVertex >= 0)
                {
                    Options.BasePoint = _mLine.VertexAt(indexOfBaseVertex);
                }
                Options.UserInputControls = UserInputControls.UseBasePointElevation | UserInputControls.NullResponseAccepted;
                Options.Cursor = CursorType.Crosshair;
            }

            public sealed override JigPromptPointOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                return geometry.Draw(_mLine);
            }

            public override void OnUpdate()
            {
                _mLine.MoveVertexAt(_mLine.NumberOfVertices - 1, LastValue);
            }
        }
    }
}
