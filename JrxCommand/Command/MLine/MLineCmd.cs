﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.Runtime;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.Colors;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using Application = Autodesk.AutoCAD.ApplicationServices.Application;
using System.Reflection;

#endif
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Exception = System.Exception;
using JrxCad.Helpers;
using JrxCad.Utility;

namespace JrxCad.Command.MLine
{
    public partial class MLineCmd : BaseCommand
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MLineCmd"/> class.
        /// </summary>
        public MLineCmd() : this(new UserInput(), null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MLineCmd"/> class.
        /// </summary>
        public MLineCmd(IUserInput userInput, IMLineNotify notify)
        {
            _userInput = userInput;
            _notify = notify;
        }

        /// <summary>
        /// Command execution.
        /// </summary>
        [CommandMethod("JrxCommand", "SmxMLine", CommandFlags.Modal | CommandFlags.NoNewStack)]
        public override void OnCommand()
        {
            if (!Init())
            {
                return;
            }
            try
            {
                var startPoint = ChangeCurrentSettingsAndGetStartPoint();
                //Check if the starting point is invalid
                if (startPoint == null)
                {
                    return;
                }

                DrawMLine((Point3d)startPoint);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        /// <summary>
        /// Change the current setting of justification, scale and style, then return the starting point.
        /// </summary>
        /// <returns>The 3D starting point.</returns>
        private Point3d? ChangeCurrentSettingsAndGetStartPoint()
        {
            var optStartPoint = new PromptPointOptions($"\n{MLineRes.UI_P1}")
            {
                AllowNone = true,
            };
            optStartPoint.Keywords.Add("J", MLineRes.UI_P1_Justification, MLineRes.UI_P1_Justification);
            optStartPoint.Keywords.Add("S", MLineRes.UI_P1_Scale, MLineRes.UI_P1_Scale);
            optStartPoint.Keywords.Add("ST", MLineRes.UI_P1_Style, MLineRes.UI_P1_Style);
            while (true)
            {
                var cmlScale = Converter.DistanceToString(SystemVariable.GetDouble("CMLSCALE"));
                var cmlStyle = SystemVariable.GetString("CMLSTYLE");
                WriteMessage(MLineRes.CM_M1, GetMLineJustStr(), cmlScale, cmlStyle);

                _notify?.AppendMLineCmM1(MLineRes.CM_M1);

                var resStartPoint = _userInput.GetPoint(optStartPoint);

                switch (resStartPoint.Status)
                {
                    case PromptStatus.OK:
                        return resStartPoint.Value;
                    case PromptStatus.Keyword:
                        switch (resStartPoint.StringResult)
                        {
                            case "J":
                                SetJustification();
                                break;
                            case "S":
                                SetScale();
                                break;
                            case "ST":
                                SetStyle();
                                break;
                        }
                        break;
                    default:
                        return null;
                }
            }
        }

        /// <summary>
        /// Draw the MLine based on the points entered by the user.
        /// </summary>
        private void DrawMLine(Point3d startPoint)
        {
            var database = Util.Database();
            var mLine = new Mline()
            {
                Normal = CoordConverter.UcsZAxis(),
                Style = database.CmlstyleID,
                Scale = database.Cmlscale,
                Justification = (MlineJustification)database.Cmljust,
                LinetypeScale = database.Celtscale,
                LineWeight = database.Celweight,
            };
#if _AutoCAD_
            mLine.Transparency = database.Cetransparency;
#elif _IJCAD_
            var transparency = SystemVariable.GetString("CETRANSPARENCY");
            if (byte.TryParse(transparency, out byte resTransparency))
            {
                mLine.Transparency = new Transparency((byte)(255 * (100 - resTransparency) / 100));
            }
            else
            {
                if (transparency.ToLower() == "bylayer")
                {
                    mLine.Transparency = new Transparency(TransparencyMethod.ByLayer);
                }
                else
                {
                    mLine.Transparency = new Transparency(TransparencyMethod.ByBlock);
                }
            }
#endif
            startPoint = startPoint.TransformBy(CoordConverter.UcsToWcs());
            // To draw MLine we must initial mline with two vertices
            // 2nd(or last) vertex is temporary vertex because it will change according to the mouse pointer.
            mLine.AppendSegment(startPoint);
            mLine.AppendSegment(startPoint);
            while (true)
            {
#if _IJCAD_
                PromptResult resNextPoint;
                if (IsLispAndNoCmdEcho)
                {
                    PromptPointOptions optNextPoint;
                    var minVerticesToUndo = 3;
                    var minVerticesToClose = 4;
                    if (mLine.NumberOfVertices < minVerticesToUndo)
                    {
                        optNextPoint = new PromptPointOptions($"\n{MLineRes.UI_NP1}")
                        {
                            AppendKeywordsToMessage = false,
                        };
                        optNextPoint.Keywords.Add("U", MLineRes.UI_Key_Undo, MLineRes.UI_Key_Undo);
                    }
                    else if (mLine.NumberOfVertices >= minVerticesToClose)
                    {
                        optNextPoint = new PromptPointOptions($"\n{MLineRes.UI_NP3}");
                        optNextPoint.Keywords.Add("C", MLineRes.UI_Key_Close, MLineRes.UI_Key_Close);
                        optNextPoint.Keywords.Add("U", MLineRes.UI_Key_Undo, MLineRes.UI_Key_Undo);
                    }
                    else
                    {
                        optNextPoint = new PromptPointOptions($"\n{MLineRes.UI_NP2}");
                        optNextPoint.Keywords.Add("U", MLineRes.UI_Key_Undo, MLineRes.UI_Key_Undo);
                    }

                    resNextPoint = _userInput.GetPoint(optNextPoint);

                    if (resNextPoint.Status == PromptStatus.OK)
                    {
                        mLine.MoveVertexAt(mLine.NumberOfVertices - 1, ((PromptPointResult)resNextPoint).Value.TransformBy(CoordConverter.UcsToWcs()));
                    }
                }
                else
                {
                    var jigEntity = new MLineJigActions(mLine);
                    resNextPoint = _userInput.AcquirePoint(jigEntity);
                }
#elif _AutoCAD_
                var jigEntity = new MLineJigActions(mLine);
                var resNextPoint = _userInput.AcquirePoint(jigEntity);
#endif
                switch (resNextPoint.Status)
                {
                    case PromptStatus.OK:

                        mLine.AppendSegment(mLine.VertexAt(mLine.NumberOfVertices - 1));
                        break;
                    case PromptStatus.Keyword:
                        switch (resNextPoint.StringResult)
                        {
                            case "C":
#if _IJCAD_
                                mLine.RemoveLastSegment(out _);
#elif _AutoCAD_
                                mLine.RemoveLastSegment(mLine.VertexAt(mLine.NumberOfVertices - 1));
#endif
                                mLine.IsClosed = true;
                                using (var tr = Util.StartTransaction())
                                {
                                    tr.AddNewlyCreatedDBObject(mLine, true, tr.CurrentSpace());
                                    _notify?.Appended(mLine.ObjectId);
                                    tr.Commit();
                                }
                                return;
                            case "U":
                                var minVerticesToUndo = 3;
                                if (mLine.NumberOfVertices >= minVerticesToUndo)
                                {
#if _IJCAD_
                                    mLine.RemoveLastSegment(out _);
#elif _AutoCAD_
                                    mLine.RemoveLastSegment(mLine.VertexAt(mLine.NumberOfVertices - 1));
#endif
                                }
                                continue;
                        }
                        break;
                    default:
                        using (var tr = Util.StartTransaction())
                        {
                            // Remove temporary vertex(last vertex)
#if _IJCAD_
                            mLine.RemoveLastSegment(out _);
#elif _AutoCAD_
                            mLine.RemoveLastSegment(mLine.VertexAt(mLine.NumberOfVertices - 1));
#endif
                            var minVerticesToDrawMLine = 2;
                            if (mLine.NumberOfVertices >= minVerticesToDrawMLine)
                            {
                                tr.AddNewlyCreatedDBObject(mLine, true, tr.CurrentSpace());
                                _notify?.Appended(mLine.ObjectId);
                                tr.Commit();
                            }
                        }
                        return;

                }

            }
        }
    }
}
