﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JrxCad.Command.MLine {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class MLineRes {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal MLineRes() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("JrxCad.Command.MLine.MLineRes", typeof(MLineRes).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Multiline style {0} not found in {1}.
        ///You can use the &quot;MLSTYLE&quot; command to load it from another file..
        /// </summary>
        public static string CM_DLG1 {
            get {
                return ResourceManager.GetString("CM_DLG1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Current settings: Justification = {0}, Scale = {1}, Style = {2}.
        /// </summary>
        public static string CM_M1 {
            get {
                return ResourceManager.GetString("CM_M1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Loaded mline styles:      
        ///      Name          Description
        ///---------------- ------------------.
        /// </summary>
        public static string CM_OP4 {
            get {
                return ResourceManager.GetString("CM_OP4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Load multiline style from file.
        /// </summary>
        public static string DialogCaption {
            get {
                return ResourceManager.GetString("DialogCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Close.
        /// </summary>
        public static string UI_Key_Close {
            get {
                return ResourceManager.GetString("UI_Key_Close", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Undo.
        /// </summary>
        public static string UI_Key_Undo {
            get {
                return ResourceManager.GetString("UI_Key_Undo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Specify next point.
        /// </summary>
        public static string UI_NP1 {
            get {
                return ResourceManager.GetString("UI_NP1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Specify next point or.
        /// </summary>
        public static string UI_NP2 {
            get {
                return ResourceManager.GetString("UI_NP2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Specify next point or.
        /// </summary>
        public static string UI_NP3 {
            get {
                return ResourceManager.GetString("UI_NP3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter justification type.
        /// </summary>
        public static string UI_OP1 {
            get {
                return ResourceManager.GetString("UI_OP1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bottom.
        /// </summary>
        public static string UI_OP1_Bottom {
            get {
                return ResourceManager.GetString("UI_OP1_Bottom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Top.
        /// </summary>
        public static string UI_OP1_Top {
            get {
                return ResourceManager.GetString("UI_OP1_Top", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zero.
        /// </summary>
        public static string UI_OP1_Zero {
            get {
                return ResourceManager.GetString("UI_OP1_Zero", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter mline scale.
        /// </summary>
        public static string UI_OP2 {
            get {
                return ResourceManager.GetString("UI_OP2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter mline style name or [?].
        /// </summary>
        public static string UI_OP3 {
            get {
                return ResourceManager.GetString("UI_OP3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Specify start point or.
        /// </summary>
        public static string UI_P1 {
            get {
                return ResourceManager.GetString("UI_P1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Justification.
        /// </summary>
        public static string UI_P1_Justification {
            get {
                return ResourceManager.GetString("UI_P1_Justification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Scale.
        /// </summary>
        public static string UI_P1_Scale {
            get {
                return ResourceManager.GetString("UI_P1_Scale", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to STyle.
        /// </summary>
        public static string UI_P1_Style {
            get {
                return ResourceManager.GetString("UI_P1_Style", resourceCulture);
            }
        }
    }
}
