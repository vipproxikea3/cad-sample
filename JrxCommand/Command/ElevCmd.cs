﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.EditorInput;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;

#endif

using Exception = System.Exception;
using JrxCad.Utility;

namespace JrxCad.Command
{
    public class ElevCmd : BaseCommand
    {
        [CommandMethod("JrxCommand", "SmxJrxCadElev", CommandFlags.Modal)]
        public override void OnCommand()
        {
            try
            {
                var editor = Util.Editor();

                if (!Init())
                {
                    return;
                }

                var optElev = new PromptDistanceOptions("\n新しい既定の高度を指定")
                {
                    DefaultValue = SystemVariable.GetDouble("ELEVATION")
                };
                var resElev = editor.GetDistance(optElev);
                if (resElev.Status != PromptStatus.OK) return;

                var optThick = new PromptDistanceOptions("\n新しい既定の厚さを指定")
                {
                    DefaultValue = SystemVariable.GetDouble("THICKNESS")
                };
                var resThick = editor.GetDistance(optThick);
                if (resThick.Status != PromptStatus.OK) return;

                SystemVariable.SetValue("ELEVATION", resElev.Value);
                SystemVariable.SetValue("THICKNESS", resThick.Value);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }
    }
}
