﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using Polyline = GrxCAD.DatabaseServices.Polyline;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
using Autodesk.AutoCAD.GraphicsInterface;
#endif

namespace JrxCad.Command.Dist.Utitily
{
    public partial class PolylineDrawJig
    {
        /// <summary>
        /// Get Angle
        /// </summary>
        private void GetAngle(ArcJig jigArc)
        {
            var angle = _userInput.GetAngle(new PromptAngleOptions($"{DistRes.UI_A1}")
            {
                BasePoint = Polyline.GetPoint3dAt(Polyline.NumberOfVertices - 2),
                DefaultValue = 0,
                UseBasePoint = true,
                UseDashedLine = true
            });
            if (angle.Status != PromptStatus.OK) return;
            jigArc.Angle = angle.Value;
            jigArc.HasAngle = true;
        }

        /// <summary>
        /// Drawing Angle
        /// </summary>
        private void DrawJigAngle(ArcJig jigArc, Plane plane)
        {
            var jigAngle = new AngleJig(plane, Polyline, Hatch, jigArc.Center);
            var drag = _userInput.GetPointResultAngle(jigAngle);
            if (drag.Status != PromptStatus.OK) return;

            jigArc.KeywordsReset();
            jigArc.AddDummyVertex();
        }

        public class AngleJig : PolylineBaseJig
        {
            private readonly Point3d _center;
            private readonly JigPromptAngleOptions _jigOpt;

            private Point3d _lastVertex;
            private double _angle;

            public AngleJig(Plane plane, Polyline polyline, Hatch hatch, Point3d center)
            {
                _center = center;
                _plane = plane;
                _pline = polyline;
                _hatch = hatch;
                _lastVertex = _pline.GetPoint3dAt(_pline.NumberOfVertices - 2);

                _jigOpt = new JigPromptAngleOptions
                {
                    BasePoint = _center,
                    UseBasePoint = true,
                    Cursor = CursorType.RubberBand,
                    UserInputControls = UserInputControls.NullResponseAccepted |
                        UserInputControls.GovernedByOrthoMode,
                    Message = $"\n{DistRes.UI_A1}",
                };
            }

            protected override SamplerStatus Sampler(JigPrompts prompts)
            {
                var res = prompts.AcquireAngle(_jigOpt);
                if (res.Status == PromptStatus.OK)
                {
                    _angle = res.Value;
                    return SamplerStatus.OK;
                }

                return SamplerStatus.Cancel;
            }

            protected override bool WorldDraw(WorldDraw draw)
            {
                SetBulge(_angle * 0.5);

                var point = _lastVertex.RotateBy(_angle, Vector3d.ZAxis, _center);
                _pline.SetPointAt(_pline.NumberOfVertices - 1, point.Convert2d(_plane));

                return base.WorldDraw(draw);
            }
        }
    }
}
