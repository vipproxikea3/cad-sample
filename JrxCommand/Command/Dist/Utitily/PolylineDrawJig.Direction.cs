﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using Polyline = GrxCAD.DatabaseServices.Polyline;
using GrxCAD.GraphicsInterface;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
using Autodesk.AutoCAD.GraphicsInterface;
#endif
using System;

namespace JrxCad.Command.Dist.Utitily
{
    public partial class PolylineDrawJig
    {
        /// <summary>
        /// previous direction
        /// </summary>
        private static double _prevDirect;

        /// <summary>
        /// Draw direction
        /// </summary>
        private void DrawJigDirect(ArcJig jigArc, Plane plane)
        {
            var jigDirect = new ArcDirectJig(plane, Polyline, Hatch)
            {
                Angle = jigArc.Angle,
                Radius = jigArc.Radius
            };
            var dragDirect = _doc.Editor.Drag(jigDirect);
            if (dragDirect.Status != PromptStatus.OK) return;

            jigArc.KeywordsReset("ALL");
            jigArc.AddDummyVertex();
        }

        private class ArcDirectJig : PolylineBaseJig
        {
            public double Angle { get; set; }
            public double Radius { get; set; }

            private Point3d _pickPoint;
            private Point3d _lastVertex;
            private readonly JigPromptAngleOptions _jigOpt;

            public ArcDirectJig(Plane plane, Polyline polyline, Hatch hatch)
            {
                _plane = plane;
                _pline = polyline;
                _hatch = hatch;

                _jigOpt = new JigPromptAngleOptions()
                {
                    BasePoint = _pline.GetPoint3dAt(_pline.NumberOfVertices - 2),
                    UseBasePoint = true,
                    Cursor = CursorType.RubberBand,
                    DefaultValue = _prevDirect,
                    UserInputControls = UserInputControls.NullResponseAccepted |
                        UserInputControls.GovernedByOrthoMode,
                    Message = $"\n{DistRes.UI_ARC}\n",
                };
            }

            protected override SamplerStatus Sampler(JigPrompts prompts)
            {
                var res = prompts.AcquireAngle(_jigOpt);
                if (res.Status == PromptStatus.OK)
                {
                    _prevDirect = res.Value;
                    _lastVertex = _pline.GetPoint3dAt(_pline.NumberOfVertices - 2);
                    _pickPoint = new Point3d(
                        Radius * Math.Cos(_prevDirect) + _lastVertex.X,
                        Radius * Math.Sin(_prevDirect) + _lastVertex.Y,
                        _lastVertex.Z);

                    return SamplerStatus.OK;
                }

                return SamplerStatus.Cancel;
            }

            protected override bool WorldDraw(WorldDraw draw)
            {
                
                SetBulge(Angle * 0.5);

                var center2 = _pickPoint.RotateBy((Math.PI - Angle) * 0.5, Vector3d.ZAxis, _lastVertex);
                var center = _lastVertex.TransformBy(
                    Matrix3d.Displacement((center2 - _lastVertex).GetNormal() * Radius));

                var point = _lastVertex.RotateBy(Angle, Vector3d.ZAxis, center);
                _pline.SetPointAt(_pline.NumberOfVertices - 1, point.Convert2d(_plane));

                return base.WorldDraw(draw);
            }
        }
    }
}
