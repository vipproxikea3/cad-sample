﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using Polyline = GrxCAD.DatabaseServices.Polyline;
using GrxCAD.GraphicsInterface;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
using Autodesk.AutoCAD.GraphicsInterface;
#endif

using JrxCad.Utility;
using System;

namespace JrxCad.Command.Dist.Utitily
{
    public partial class PolylineDrawJig
    {
        /// <summary>
        /// Specify Arc
        /// </summary>
        private bool DrawArc(PolylineJig jig, Plane plane)
        {
            var jigArc = new ArcJig(plane, Polyline, Hatch);
            while (true)
            {
                var dragArc = _userInput.GetPointPomptResultArc(jigArc);
                switch (dragArc.Status)
                {
                    case PromptStatus.Keyword:
                        switch (dragArc.StringResult)
                        {
                            case "A": //specify angle
                                if (jigArc.HasCenter)
                                {
                                    DrawJigAngle(jigArc, plane);
                                }
                                else
                                {
                                    GetAngle(jigArc);

                                    if (jigArc.HasRadius)
                                    {
                                        DrawJigDirect(jigArc, plane);
                                    }
                                    else
                                    {
                                        jigArc.Message($"\n{DistRes.UI_ARC}");
                                        jigArc.KeywordsReset("CE", "R");
                                        jigArc.UseAngle = true;
                                    }
                                }

                                break;

                            case "CE":
                                if (jigArc.HasAngle)
                                {
                                    jigArc.Message($"\n{DistRes.UI_CE1}");
                                    jigArc.KeywordsReset();
                                    jigArc.UseCenter = true;
                                }
                                else
                                {
                                    var center = _userInput.GetCenterPoint(DistRes.UI_A3);
                                    if (center.Status != PromptStatus.OK) return false;
                                    jigArc.Center = center.Value;
                                    jigArc.HasCenter = true;

                                    jigArc.KeywordsReset("A", "L");
                                }

                                break;

                            case "CL":
                                WriteEditor(Polyline, _doc);
                                return true;

                            case "D":
                                DrawJigDirect(jigArc, plane);
                                break;

                            case "L":
                                if (!jigArc.HasCenter)
                                {
                                    jig.IsArc = false;
                                    return false;
                                }
                                break;

                            case "R":
                                var options = new PromptDistanceOptions($"\n{DistRes.UI_A4}");
                                var radius = _userInput.GetDistance(options);
                                if (radius.Status != PromptStatus.OK) return false;
                                jigArc.Radius = radius.Value;
                                jigArc.HasRadius = true;

                                if (jigArc.HasAngle)
                                {
                                    DrawJigDirect(jigArc, plane);
                                }
                                else
                                {
                                    jigArc.KeywordsReset("A");
                                }

                                break;

                            case "S":
                                var secondPointArc = new PromptPointOptions($"\n{DistRes.UI_SP1}");
                                var rest = _userInput.GetPoint(secondPointArc);
                                if (rest.Status != PromptStatus.OK) return false;

                                var endPointArc = new PromptPointOptions($"\n{DistRes.UI_P2}");
                                var endPoint = _userInput.GetPoint(endPointArc);
                                WriteEditor(Polyline, _doc);
                                break;

                            case "U":
                                jigArc.RemoveLastVertex();
                                jig.IsArc = jigArc.IsArc;
                                break;
                        }

                        break;

                    case PromptStatus.OK:
                        WriteEditor(Polyline, _doc);
                        jigArc.AddDummyVertex();
                        jigArc.Message($"\n{DistRes.UI_ARC}\n");
                        jigArc.KeywordsReset("ALL");
                        break;

                    case PromptStatus.None:
                        Polyline.RemoveVertexAt(Polyline.NumberOfVertices - 1); 
                        WriteEditor(Polyline, _doc);
                        return true;

                    default:
                        return true;
                }
            }
        }

        public class ArcJig : PolylineBaseJig
        {
            public bool UseAngle { get; set; }
            public bool HasAngle { get; set; }
            public bool UseCenter { get; set; }
            public bool HasCenter { get; set; }
            public bool HasRadius { get; set; }

            public double Angle { get; set; }
            public Point3d Center { get; set; }
            public double Radius { get; set; }

            private Point3d _pickPoint;
            private Point3d _lastVertex;
            private readonly JigPromptPointOptions _jigOpt;

            public ArcJig(Plane plane, Polyline polyline, Hatch hatch)
            {
                IsArc = true;

                _plane = plane;
                _pline = polyline;
                _hatch = hatch;
                AddDummyVertex();

                _jigOpt = new JigPromptPointOptions
                {
                    UseBasePoint = true,
                    Cursor = CursorType.RubberBand,
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                        UserInputControls.NullResponseAccepted |
                        UserInputControls.NoNegativeResponseAccepted |
                        UserInputControls.GovernedByOrthoMode,
                    Message = $"\n{DistRes.UI_ARC}\n"
                };
                KeywordsReset("A", "CE", "D", "L", "R", "S", "U");
            }

            public void SetMessage(string message)
            {
                _jigOpt.Message = message;
            }

            protected override SamplerStatus Sampler(JigPrompts prompts)
            {
                _jigOpt.BasePoint = HasCenter && !HasAngle
                    ? Center
                    : _pline.GetPoint3dAt(_pline.NumberOfVertices - 2);
                _lastVertex = _pline.GetPoint3dAt(_pline.NumberOfVertices - 2);

                var res = prompts.AcquirePoint(_jigOpt);
                switch (res.Status)
                {
                    case PromptStatus.Keyword:
                        return SamplerStatus.OK;

                    case PromptStatus.OK when _pickPoint == res.Value:
                        return SamplerStatus.NoChange;

                    case PromptStatus.OK:
                        _pickPoint = res.Value;
                        return SamplerStatus.OK;

                    default:
                        return SamplerStatus.Cancel;
                }
            }

            protected override bool WorldDraw(WorldDraw draw)
            {
                double angle;
                if (HasAngle)
                {
                    angle = Angle * 0.5;
                }
                else if (UseAngle)
                {
                    angle = (_pickPoint - Center).AngleOnPlane(_plane) * 0.5;
                }
                else
                {
                    var tangentDir = new Vector3d(1.0, 1.0, 0.0);

                    if (_pline.NumberOfVertices > 2)
                    {
                        if (_pline.GetBulgeAt(_pline.NumberOfVertices - 3) != 0)
                        {
                            var arc = _pline.GetArcSegmentAt(_pline.NumberOfVertices - 3);
                            var tangent = arc.GetTangent(_lastVertex);
                            tangentDir = tangent.Direction.MultiplyBy(-1.0);
                        }
                        else
                        {
                            var pt = _pline.GetPoint3dAt(_pline.NumberOfVertices - 3);
                            tangentDir = _lastVertex - pt;
                        }
                    }

                    angle = JigUtils.ComputeAngle(_lastVertex, _pickPoint, tangentDir, _ucs);
                }

                SetBulge(angle);

                if (HasCenter)
                {
                    var point = _lastVertex.RotateBy(angle * 2.0, Vector3d.ZAxis, Center);
                    _pline.SetPointAt(_pline.NumberOfVertices - 1, point.Convert2d(_plane));
                }
                else if (HasAngle)
                {
                    Point3d point;
                    if (UseCenter)
                    {
                        point = _lastVertex.RotateBy(Angle, Vector3d.ZAxis, _pickPoint);
                    }
                    else
                    {
                        point = _pickPoint;
                    }

                    _pline.SetPointAt(_pline.NumberOfVertices - 1, point.Convert2d(_plane));
                }
                else if (HasRadius)
                {
                    var center2 = _pickPoint.RotateBy((Math.PI - angle * 2) * 0.5, Vector3d.ZAxis, _lastVertex);
                    var center = _lastVertex.TransformBy(
                        Matrix3d.Displacement((center2 - _lastVertex).GetNormal() * Radius));
                    var point = _lastVertex.RotateBy(angle * 2, Vector3d.ZAxis, center);
                    _pline.SetPointAt(_pline.NumberOfVertices - 1, point.Convert2d(_plane));
                }
                else
                {
                    _pline.SetPointAt(_pline.NumberOfVertices - 1, _pickPoint.Convert2d(_plane));
                }

                return base.WorldDraw(draw);
            }

            /// <summary>
            /// Close
            /// </summary>
            public void Close()
            {
                _pline.RemoveVertexAt(_pline.NumberOfVertices - 1); 
                _pline.AddVertexAt(_pline.NumberOfVertices, _pline.GetPoint2dAt(0), 0, 0, 0);
                _lastVertex = _pline.GetPoint3dAt(_pline.NumberOfVertices - 2);
                _pickPoint = _pline.GetPoint3dAt(_pline.NumberOfVertices - 1);

                Vector3d tangentDir;
                if (_pline.GetBulgeAt(_pline.NumberOfVertices - 3) != 0)
                {
                    var arc = _pline.GetArcSegmentAt(_pline.NumberOfVertices - 3);
                    var tangent = arc.GetTangent(_lastVertex);
                    tangentDir = tangent.Direction.MultiplyBy(-1.0);
                }
                else
                {
                    var pt = _pline.GetPoint3dAt(_pline.NumberOfVertices - 3);
                    tangentDir = _lastVertex - pt;
                }

                var angle = JigUtils.ComputeAngle(_lastVertex, _pickPoint, tangentDir, _ucs);

               
                var bulge = MathEx.IsEqual(Math.Abs(angle), Math.PI) ? 1.0 : Math.Tan(angle * 0.5);
                _pline.SetBulgeAt(_pline.NumberOfVertices - 2, bulge);
            }

            /// <summary>
            /// Set message
            /// </summary>
            public void Message(string message)
            {
                _jigOpt.Message = message;
            }

            /// <summary>
            /// reset keyword
            /// </summary>
            public void KeywordsReset(params string[] globalNames)
            {
                _jigOpt.Keywords.Clear();
                if (globalNames.Length == 0) return;

                if (globalNames[0] == "ALL")
                {
                    var hasClose = _pline.NumberOfVertices >= 3;
                    _jigOpt.Keywords.Add("A", "A", $"{DistRes.Angle_Key}");
                    _jigOpt.Keywords.Add("CE", "CE", $"{DistRes.Center_Key}");
                    _jigOpt.Keywords.Add("CL", "CL", $"{ DistRes.Close_Key}");
                    _jigOpt.Keywords.Add("D", "D", $"{DistRes.Direction_Key}");
                    _jigOpt.Keywords.Add("L", "L", $"{DistRes.Line_Key}");
                    _jigOpt.Keywords.Add("R", "R", $"{DistRes.Radius_Key}");
                    _jigOpt.Keywords.Add("S", "S", $"{DistRes.SecondPt_Key}");
                    _jigOpt.Keywords.Add("U", "U", $"{DistRes.Undo_Key}");

                    UseAngle = false;
                    HasAngle = false;
                    HasRadius = false;
                    UseCenter = false;
                    HasCenter = false;
                }
                else
                {
                    foreach (var globalName in globalNames)
                    {
                        switch (globalName)
                        {
                            case "A":
                                _jigOpt.Keywords.Add("A", "A", $"{DistRes.Angle_Key}");
                                break;
                            case "CE":
                                _jigOpt.Keywords.Add("CE", "CE", $"{DistRes.Center_Key}");
                                break;
                            case "D":
                                _jigOpt.Keywords.Add("D", "D", $"{DistRes.Direction_Key}");
                                break;
                            case "L":
                                _jigOpt.Keywords.Add("L", "L", $"{DistRes.Line_Key}");
                                break;
                            case "R":
                                _jigOpt.Keywords.Add("R", "R", $"{DistRes.Radius_Key}");
                                break;
                            case "S":
                                _jigOpt.Keywords.Add("S", "S", $"{DistRes.SecondPt_Key}");
                                break;
                            case "U":
                                _jigOpt.Keywords.Add("U", "U", $"{DistRes.Undo_Key}");
                                break;
                        }
                    }
                }
            }

            private class JigUtils
            {
                private static double Atan(double y, double x)
                {
                    if (x > 0) return Math.Atan(y / x);
                    if (x < 0) return Math.Atan(y / x) - Math.PI;
                    if (y > 0) return Math.PI;
                    if (y < 0) return -Math.PI;
                    return 0.0;
                }

                /// <summary>
                /// (lastVertex→pickPoint) と　lastVertexの角度(接線)　間の角度
                /// </summary>
                public static double ComputeAngle(Point3d lastVertex, Point3d pickPoint, Vector3d tan, Matrix3d ucs)
                {
                    var v = new Vector3d(
                        (pickPoint.X - lastVertex.X) / 2,
                        (pickPoint.Y - lastVertex.Y) / 2,
                        (pickPoint.Z - lastVertex.Z) / 2);

                    var cos = v.DotProduct(tan);
                    var sin = v.DotProduct(Vector3d.ZAxis.TransformBy(ucs).CrossProduct(tan));
                    return Atan(sin, cos);
                }
            }
        }
    }
}
