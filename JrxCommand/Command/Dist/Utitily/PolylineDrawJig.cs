﻿#if _IJCAD_
using Application = GrxCAD.ApplicationServices.Application;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.ApplicationServices;
using Polyline = GrxCAD.DatabaseServices.Polyline;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.GraphicsInterface;
#endif
using JrxCad.Utility;
using System;
using static JrxCad.Command.Dist.DistCmd;

namespace JrxCad.Command.Dist.Utitily
{
    public partial class PolylineDrawJig
    {

        public Polyline Polyline { get; set; }
        public Hatch Hatch { get; set; }
        private static Document _doc;
        private static Matrix3d _ucs;
        private static IUserInputDist _userInput;

        /// <summary>
        /// Constructor
        /// </summary>
        public PolylineDrawJig(IUserInputDist userInput)
        {
            _doc = Application.DocumentManager.MdiActiveDocument;
            _ucs = _doc.Editor.CurrentUserCoordinateSystem;
            _userInput = userInput;
        }

        /// <summary>
        /// write command message line
        /// </summary>
        public virtual void WriteEditor(Curve curve, Document doc)
        {
        }

        /// <summary>
        /// Get second point
        /// </summary>
        public void GetSecondPoint()
        {
            var normal = Vector3d.ZAxis.TransformBy(_doc.Editor.CurrentUserCoordinateSystem);
            var plane = new Plane(Point3d.Origin, normal);
            var jig = new PolylineJig(plane, Polyline, Hatch, string.Empty);
            jig.SetPromptMessage(DistRes.UI_P2);
            jig.KeywordsReset("M");

            var result = _userInput.GetPointPromptResult(jig);
            switch (result.Status)
            {
                case PromptStatus.Keyword when result.StringResult == "M":
                    GetMultiplePointByMode();
                    break;
                case PromptStatus.OK:
                    GetAngleToXaxe(jig.PolyLine);
                    break;
            };
        }

        private void GetAngleToXaxe(Curve curve)
        {
            //var ucs = Util.Editor().CurrentUserCoordinateSystem.CoordinateSystem3d;
            //var plane = new Plane(Point3d.Origin, ucs.Zaxis);
            //var ocs = Matrix3d.PlaneToWorld(plane).CoordinateSystem3d;
            //var ucsRotation = ocs.Xaxis.GetAngleTo(ucs.Xaxis);

            //var vect1 = curve.StartPoint.GetVectorTo(curve.EndPoint);
            //var angleXYFromPlane = Math.Round(vect1.AngleOnPlane(plane), MidpointRounding.AwayFromZero);

            var radian = Math.Atan2((curve.EndPoint.Y - curve.StartPoint.Y), (curve.EndPoint.X - curve.StartPoint.X));
            var angleXYInPlane = Math.Round((radian * (180 / Math.PI) + 360) % 360, MidpointRounding.AwayFromZero);

            var distance = $"{curve.StartPoint.DistanceTo(curve.EndPoint):F4}";
            var deltaX = $"{curve.EndPoint.X - curve.StartPoint.X:F4}";
            var deltaY = $"{curve.EndPoint.Y - curve.StartPoint.Y:F4}";
            var deltaZ = $"{curve.EndPoint.Z - curve.StartPoint.Z:F4}";
            Util.Editor().WriteMessage($"\n{string.Format(DistRes.UI_P1_CM1, distance, angleXYInPlane, 0)}\n{string.Format(DistRes.UI_P1_CM2, deltaX, deltaY, deltaZ)}");
        }

        /// <summary>
        /// Specify coordinate
        /// </summary>
        public double GetMultiplePointByMode(string header = null)
        {
            var normal = Vector3d.ZAxis.TransformBy(_doc.Editor.CurrentUserCoordinateSystem);
            var plane = new Plane(Point3d.Origin, normal);
            var jig = new PolylineJig(plane, Polyline, Hatch, header);
            // specify multiple point without close key
            jig.SetPromptMessage(DistRes.UI_MP);
            jig.KeywordsReset("A", "L", "U", "T");

            while (true)
            {
                var drag = _userInput.GetPointPromptResult(jig);
                switch (drag.Status)
                {
                    case PromptStatus.Keyword:
                        switch (drag.StringResult)
                        {
                            case "A":
                                // get arc
                                Polyline.RemoveVertexAt(Polyline.NumberOfVertices - 1);
                                if (DrawArc(jig, plane))
                                {
                                    WriteEditor(Polyline, _doc);
                                    return Polyline.Area;
                                }
                                break;

                            case "C": // 
                                WriteEditor(Polyline, _doc);
                                return -1;

                            case "L":
                                // get length
                                var isHasLength = GetLength(Polyline.GetPoint3dAt(Polyline.NumberOfVertices - 2));
                                if (!isHasLength)
                                   return -1;
                                break;

                            case "U":
                                jig.RemoveLastVertex();
                                // undo
                                if (jig.IsArc && DrawArc(jig, plane)) return Polyline.Area;
                                break;

                            case "T":
                                // get total
                                WriteEditor(Polyline, _doc);
                                return Polyline.Area;
                        }
                        break;

                    case PromptStatus.OK:
                        // show distance into command message line
                        WriteEditor(Polyline, _doc);
                        // specify multiple point has close key
                        jig.SetPromptMessage(DistRes.UI_MP);
                        jig.KeywordsReset("ALL");
                        break;

                    case PromptStatus.None:
                        return Polyline.Area;

                    default:
                        return -1;
                }
            }
        }


        public class PolylineJig : PolylineBaseJig
        {
            public Polyline PolyLine;
            public Point3d LastPoint;
            private JigPromptPointOptions _jigOpt;
            private string _header;

            /// <summary>
            /// contructor for Multiple point
            /// </summary>
            public PolylineJig(Plane plane, Polyline polyline, Hatch hatch, string header)
            {
                _plane = plane;
                _pline = polyline;
                _hatch = hatch;
                _header = header;
                AddDummyVertex();
                _jigOpt = new JigPromptPointOptions
                {
                    UseBasePoint = true,
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.NoNegativeResponseAccepted |
                                        UserInputControls.GovernedByOrthoMode,
                };

            }

            protected override SamplerStatus Sampler(JigPrompts prompts)
            {
                var result = prompts.AcquirePoint(_jigOpt);
                switch (result.Status)
                {
                    case PromptStatus.Keyword:
                        return SamplerStatus.OK;
                    case PromptStatus.OK when LastPoint == result.Value:
                        return SamplerStatus.NoChange;
                    case PromptStatus.OK:
                        LastPoint = result.Value;
                        return SamplerStatus.OK;
                    default:
                        return SamplerStatus.Cancel;
                }
            }

            protected override bool WorldDraw(WorldDraw draw)
            {
                // Line mode. Need to remove last bulge if there was one
                if (_pline.NumberOfVertices > 1)
                    _pline.SetBulgeAt(_pline.NumberOfVertices - 2, 0);

                _pline.SetPointAt(_pline.NumberOfVertices - 1, LastPoint.Convert2d(_plane));

                PolyLine = _pline;
                return base.WorldDraw(draw);
            }

            /// <summary>
            /// Set message
            /// </summary>
            /// <param name="cmdMessage"></param>
            public void SetPromptMessage(string cmdMessage)
            {
                _jigOpt.Message = $"\n{cmdMessage}";
            }

            /// <summary>
            /// reset keyword
            /// </summary>
            public void KeywordsReset(params string[] globalNames)
            {
                if (globalNames.Length == 0) return;
                _jigOpt.Keywords.Clear();

                if (globalNames[0] == "ALL")
                {
                    _jigOpt.Keywords.Add("A", "A", $"{DistRes.Arc_Key}");
                    _jigOpt.Keywords.Add("C", "C", $"{DistRes.Close_Key}");
                    _jigOpt.Keywords.Add("L", "L", $"{DistRes.Length_Key}");
                    _jigOpt.Keywords.Add("U", "U", $"{DistRes.Undo_Key}");
                    _jigOpt.Keywords.Add("T", "T", $"{DistRes.Total_Key}");
                }
                else
                {
                    foreach (var globalName in globalNames)
                    {
                        switch (globalName)
                        {
                            case "M":
                                _jigOpt.Keywords.Add("M", "M", $"{DistRes.UI_P2_MultiplePoint}");
                                break;
                            case "A":
                                _jigOpt.Keywords.Add("A", "A", $"{DistRes.Arc_Key}");
                                break;
                            case "L":
                                _jigOpt.Keywords.Add("L", "L", $"{DistRes.Length_Key}");
                                break;
                            case "U":
                                _jigOpt.Keywords.Add("U", "U", $"{DistRes.Undo_Key}");
                                break;
                            case "T":
                                _jigOpt.Keywords.Add("T", "T", $"{DistRes.Total_Key}");
                                break;
                        }
                    }
                }

                // set default keyword
                if (_jigOpt.Keywords.Count > 1)
                {
                    _jigOpt.Keywords.Default = "T";
                }
            }
        }

        public class PolylineBaseJig : DrawJig
        {
            public bool IsArc;

            protected Polyline _pline;
            protected Plane _plane;
            protected Hatch _hatch;

            protected override SamplerStatus Sampler(JigPrompts prompts)
            {
                return SamplerStatus.OK;
            }

            protected override bool WorldDraw(WorldDraw draw)
            {
                if (_pline.NumberOfVertices > 2)
                {
                    _pline.Closed = true;
                    if (_hatch != null) AppendLoop();
                }
                if (!draw.RegenAbort)
                {
                    draw.Geometry.Draw(_pline);
                    if (_hatch != null && _pline.NumberOfVertices > 2)
                    {
                        _hatch.EvaluateHatch(true);
                        draw.Geometry.Draw(_hatch);
                        _hatch.RemoveLoopAt(0);
                    }
                }
                return true;
            }

            /// <summary>
            /// 
            /// </summary>
            protected void SetBulge(double angle)
            {
                // bulge = Tan(angle * 2 * 0.25)  
                var bulge = MathEx.IsEqual(Math.Abs(angle), Math.PI)
                    ? 1.0
                    : Math.Tan(angle * 0.5);
                _pline.SetBulgeAt(_pline.NumberOfVertices - 2, bulge);
            }

            /// <summary>
            /// 
            /// </summary>
            public void AddDummyVertex()
            {
                _pline.AddVertexAt(_pline.NumberOfVertices, new Point2d(0, 0), 0, 0, 0);
            }

            /// <summary>
            /// 
            /// </summary>
            public void RemoveLastVertex()
            {
                if (_pline.NumberOfVertices < 2) return;

                _pline.RemoveVertexAt(_pline.NumberOfVertices - 2);
                var bulge = _pline.GetBulgeAt(_pline.NumberOfVertices - 2);
                IsArc = !MathEx.IsEqual(bulge, 0);
            }

            /// <summary>
            /// 
            /// </summary>
            private void AppendLoop()
            {
                var ids = new ObjectIdCollection { _pline.ObjectId };
                _hatch.Associative = true;
                _hatch.AppendLoop(HatchLoopTypes.Default, ids);
            }
        }
    }
}
