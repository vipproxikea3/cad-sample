﻿#if _IJCAD_
using CADApp = GrxCAD.ApplicationServices.Application;
using Application = GrxCAD.ApplicationServices.Application;
using CADException = GrxCAD.Runtime.Exception;
using CADColors = GrxCAD.Colors;
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using Polyline = GrxCAD.DatabaseServices.Polyline;

#elif _AutoCAD_
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using Application = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using CADException = Autodesk.AutoCAD.Runtime.Exception;
using CADColors = Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
#endif

using JrxCad.Utility;

namespace JrxCad.Command.Dist.Utitily
{
    public partial class PolylineDrawJig
    {
        /// <summary>
        /// Specify length.
        /// </summary>
        private bool GetLength(Point3d basePoint)
        {
            var jig = new LengthJig(Polyline, Hatch, basePoint);
            var result = _doc.Editor.Drag(jig);
            if (result.Status != PromptStatus.OK) return false;

            var length = jig.Length;
            if (MathEx.IsEqual(length, 0.0)) return false;

            var vertex = Polyline.NumberOfVertices;
            Point2d newPoint;
            switch (vertex)
            {
                case 1:
                    break;

                case 2:
                        newPoint = Polyline.GetPoint2dAt(0)
                            .TransformBy(Matrix2d.Displacement(new Vector2d(length, 0)));
                        Polyline.AddVertexAt(1, newPoint, 0, 0, 0);
                        break;
                default:
                        var line = Polyline.GetLineSegment2dAt(vertex - 3);
                        var vector = line.EndPoint - line.StartPoint;
                        newPoint = line.StartPoint.TransformBy(
                            Matrix2d.Displacement(new Vector2d(vector.X, vector.Y).GetNormal() *
                                (length + line.EndPoint.GetDistanceTo(line.StartPoint))));
                        Polyline.SetPointAt(vertex - 2, newPoint);
                        break;
            }

            return true;
        }

        private class LengthJig : PolylineBaseJig
        {
            public double Length { get; private set; }

            private JigPromptDistanceOptions _jigOpt;

            public LengthJig(Polyline polyline, Hatch hatch, Point3d basePoint)
            {
                _pline = polyline;
                _hatch = hatch;

                _jigOpt = new JigPromptDistanceOptions
                {
                    UseBasePoint = true,
                    BasePoint = basePoint,
                    Cursor = CursorType.RubberBand,
                    UserInputControls = UserInputControls.NoZeroResponseAccepted |
                        UserInputControls.GovernedByOrthoMode,
                    Message = $"\n{DistRes.UI_L1}",
                };
            }

            protected override SamplerStatus Sampler(JigPrompts prompts)
            {
                var res = prompts.AcquireDistance(_jigOpt);
                switch (res.Status)
                {
                    case PromptStatus.OK:
                        Length = res.Value;
                        return SamplerStatus.OK;

                    default:
                        return SamplerStatus.Cancel;
                }
            }
        }
    }
}
