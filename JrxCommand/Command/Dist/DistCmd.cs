﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
#endif
using JrxCad.Utility;

namespace JrxCad.Command.Dist
{
    public partial class DistCmd : BaseCommand
    {

        private enum DistCommandFlag
        {
            Finish = 0,
            Cancel = 1
        }

        public DistCmd() : this(new UserInputDist())
        {

        }

        public DistCmd(IUserInputDist userInput)
        {
            _userInput = userInput;
        }

        [CommandMethod("JrxCommand", "SmxDist", CommandFlags.Modal | CommandFlags.NoNewStack)]
        public override void OnCommand()
        {
            try
            {
                if (!Init()) return;

                // init first point
                var firstPointOptions = new PromptPointOptions($"\n{DistRes.UI_P1}")
                {
                    AllowNone = true,
                };
                var firstPointResult = _userInput.GetPoint(firstPointOptions);
                switch (firstPointResult.Status)
                {
                    case PromptStatus.OK:
                        GetPoints(firstPointResult.Value);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }


        /// <summary>
        /// specify cooridnate
        /// </summary>
        /// <param name="firstPoint"></param>
        private void GetPoints(Point3d point1)
        {
            using (var tr = Util.StartTransaction())
            {
                var jig = new DistJig(_userInput);

                var blockTable = (BlockTableRecord)tr.GetObject(Util.CurDoc().Database.CurrentSpaceId, OpenMode.ForWrite);
                var normal = Vector3d.ZAxis.TransformBy(Util.Editor().CurrentUserCoordinateSystem);
                var plane = new Plane(Point3d.Origin, normal);

                // append poly to calculate distance
                jig.Polyline = new Polyline { Normal = normal };
                jig.Polyline.AddVertexAt(0, point1.Convert2d(plane), 0, 0, 0);

                blockTable.AppendEntity(jig.Polyline);
                tr.AddNewlyCreatedDBObject(jig.Polyline, true);

                jig.GetSecondPoint();
            }
        }
    }
}
