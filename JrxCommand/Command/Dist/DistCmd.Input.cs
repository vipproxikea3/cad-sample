﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.DatabaseServices;
using GrxCAD.ApplicationServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
#endif

using JrxCad.Utility;
using JrxCad.Command.Dist.Utitily;
using static JrxCad.Command.Dist.Utitily.PolylineDrawJig;

namespace JrxCad.Command.Dist
{
    public partial class DistCmd
    {
        private readonly IUserInputDist _userInput;
        public interface IUserInputDist : IUserInputBase
        {
            PromptResult GetPointPromptResult(PolylineJig jig);
            PromptResult GetPointPomptResultArc(ArcJig jig);
            PromptResult GetPointResultAngle(AngleJig jig);
            PromptPointResult GetCenterPoint(string message);
        }

        public class UserInputDist : UserInputBase, IUserInputDist
        {
            public PromptPointResult GetCenterPoint(string message)
            {
                return Util.Editor().GetPoint($"\n{message}");
            }

            public PromptResult GetPointPomptResultArc(ArcJig jig)
            {
                return Util.Editor().Drag(jig);
            }

            public PromptResult GetPointPromptResult(PolylineJig jig)
            {
                return Util.Editor().Drag(jig);
            }

            public PromptResult GetPointResultAngle(AngleJig jig)
            {
                return Util.Editor().Drag(jig);
            }
        }

        private class DistJig : PolylineDrawJig
        {
            private readonly IUserInputDist _userInput;

            public DistJig(IUserInputDist userInput) : base(userInput)
            {
                _userInput = userInput;
            }

            /// <summary>
            /// write message into command message line
            /// </summary>
            public override void WriteEditor(Curve curve, Document doc)
            {
                var length = $"{(curve.GetDistanceAtParameter(curve.EndParam) - curve.GetDistanceAtParameter(curve.StartParam)):F4}";
                doc.Editor.WriteMessage($"\n{string.Format(DistRes.UI_P2_CM1, length)}");
            }
        }
    }
}
