﻿#if _IJCAD_
using CADApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;

#elif _AutoCAD_
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;

#endif
using System;
using System.Collections.Generic;
using System.Linq;
using JrxCad.FileIO;
using JrxCad.Helpers;
using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.Command.ShapeCmd
{
    public partial class ShapeCmd : BaseCommand
    {
        [CommandMethod("JrxCommand", "SmxJrxCadShape", CommandFlags.Modal)]
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }

                var shapeRecords = GetShapeRecords();
                var shpName = SystemVariable.GetString("SHPNAME");

                var targetShapeItem = GetShapeName(shapeRecords, shpName);
                if (targetShapeItem == null) return;

                if (!CreateShape(targetShapeItem)) return;

                SystemVariable.SetString("SHPNAME", targetShapeItem.Name);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        private class ShapeItem
        {
            public string Name { get; set; }
            public ObjectId TextStyleId { get; set; }
        }

        private class ShapeFileItem
        {
            public string FileName { get; set; }
            public ObjectId TextStyleId { get; set; }
            public List<string> Names { get; set; }
        }

        private List<ShapeFileItem> GetShapeRecords()
        {
            var shapeRecords = new List<ShapeFileItem>();
            using (var tr = Util.StartTransaction())
            using (var textStyleTable = (TextStyleTable)tr.GetObject(Util.CurDoc().Database.TextStyleTableId, OpenMode.ForRead))
            {
                foreach (var id in textStyleTable)
                {
                    var textStyle = (TextStyleTableRecord)tr.GetObject(id, OpenMode.ForRead);
                    if (textStyle.IsShapeFile)
                    {
                        // シェイプ一覧抽出
                        var tool = new ShapeFileTool();
                        if (tool.GetShapeNameList(textStyle.FileName, out var shapeNames))
                        {
                            shapeRecords.Add(new ShapeFileItem
                            {
                                FileName = textStyle.FileName,
                                TextStyleId = textStyle.ObjectId,
                                Names = shapeNames
                            });
                        }
                    }
                }
            }

            return shapeRecords;
        }

        private ShapeItem GetShapeName(List<ShapeFileItem> shapeRecords, string defaultShapeName)
        {
            var opt = new PromptStringOptions($"\nシェイプ名を入力 または [一覧(?)]")
            {
                UseDefaultValue = true,
                DefaultValue = string.IsNullOrEmpty(defaultShapeName) ? null : defaultShapeName,
            };

            while (true)
            {
                // シェイプ名を入力 または [一覧(?)]
                var res = Util.Editor().GetString(opt);
                if (res.Status != PromptStatus.OK)
                {
                    return null;
                }

                if (string.IsNullOrEmpty(res.StringResult))
                {
                    Util.Editor().WriteMessage($"まだ既定がありません。");
                    continue;
                }

                if (res.StringResult == "?")
                {
                    PromptList(shapeRecords);
                    // "?"から一覧表示した後は終了
                    return null;
                }

                var record = shapeRecords.FirstOrDefault(a =>
                    a.Names.Any(b =>
                        b.Equals(res.StringResult, StringComparison.CurrentCultureIgnoreCase)));
                if (record == null || string.IsNullOrEmpty(record.FileName))
                {
                    Util.Editor().WriteMessage($"{res.StringResult.ToUpper()} という名前のシェイプは見つかりません。");
                    continue;
                }

                return new ShapeItem
                {
                    Name = res.StringResult.ToUpper(),
                    TextStyleId = record.TextStyleId
                };
            }
        }

        private void PromptList(List<ShapeFileItem> shapeRecords)
        {
            // 一覧表示するシェイプ名を入力 <*>
            var opt2 = new PromptStringOptions($"\n一覧表示するシェイプ名を入力")
            {
                UseDefaultValue = true,
                DefaultValue = "*",
            };
            var res2 = Util.Editor().GetString(opt2);
            if (res2.Status != PromptStatus.OK)
            {
                return;
            }

            CADApp.DisplayTextScreen = true;

            var found = false;
            Util.Editor().WriteMessage($"\n使用可能なシェイプ:");
            foreach (var shapeRecord in shapeRecords)
            {
                Util.Editor().WriteMessage($"\nファイル: {shapeRecord.FileName}");
                if (res2.StringResult == "*")
                {
                    Util.Editor().WriteMessage($"\n");
                    foreach (var shapeName in shapeRecord.Names)
                    {
                        Util.Editor().WriteMessage($"  {shapeName.PadRight(16, ' ')}");
                    }
                    found = true;
                }
                else
                {
                    if (shapeRecord.Names.Any(a =>
                        string.Compare(a, res2.StringResult, StringComparison.CurrentCultureIgnoreCase) == 0))
                    {
                        Util.Editor().WriteMessage($"\n  {res2.StringResult.PadRight(16, ' ')}");
                        found = true;
                    }
                }
            }
            if (!found)
            {
                Util.Editor().WriteMessage($"\n該当するシェイプが見つかりません。");
            }
        }

        private bool CreateShape(ShapeItem target)
        {
            using (var tr = Util.StartTransaction())
            {
                var shape = new Shape(Point3d.Origin, 1.0, 0.0, 1.0);
                shape.SetDatabaseDefaults(Util.CurDoc().Database);
                shape.StyleId = target.TextStyleId;

                tr.AddNewlyCreatedDBObject(shape, true, tr.CurrentSpace());

                // Nameを設定しておかないとjigでプレビュー表示されない。
                // また、BlockTableRecordに登録してからでないと例外が発生する。理由は不明
                shape.Name = target.Name;

                // 挿入点を指定
                {
                    var jig = new ShapePointJig(shape);
                    var drag = Util.Editor().Drag(jig);
                    if (drag.Status != PromptStatus.OK) return false;
                }

                // 高さを指定 <1.0000>
                {
                    var jig = new ShapeScaleJig(shape);
                    var drag = Util.Editor().Drag(jig);
                    if (drag.Status != PromptStatus.OK) return false;
                }

                // 回転角度を指定 <0>
                {
                    var jig = new ShapeRotateJig(shape);
                    var drag = Util.Editor().Drag(jig);
                    if (drag.Status != PromptStatus.OK) return false;
                }

                tr.Commit();
            }

            return true;
        }
    }
}
