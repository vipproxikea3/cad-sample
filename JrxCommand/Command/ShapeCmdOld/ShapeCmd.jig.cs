﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;

#endif

using JrxCad.Helpers;

namespace JrxCad.Command.ShapeCmd
{
    public partial class ShapeCmd
    {
        private class ShapePointJig : EntityJig
        {
            private readonly JigPromptPointOptions _jigOpt;
            private Point3d _dragPt;

            public ShapePointJig(Shape shape) : base(shape)
            {
                _jigOpt = new JigPromptPointOptions
                {
                    UserInputControls = UserInputControls.Accept3dCoordinates,
                    Message = $"\n挿入点を指定"
                };
            }

            protected override SamplerStatus Sampler(JigPrompts prompts)
            {
                var result = prompts.AcquirePoint(_jigOpt); // W
                if (result.Value.IsEqualTo(_dragPt)) return SamplerStatus.NoChange;
                _dragPt = result.Value; // W
                return SamplerStatus.OK;
            }

            protected override bool Update()
            {
                var shape = (Shape)Entity;
#if _CheckTool_
                if (!shape.IsWriteEnabled)
                {
                    shape.UpgradeOpen();
                }
#endif
                shape.Position = _dragPt;
                return true;
            }
        }

        private class ShapeScaleJig : EntityJig
        {

            private readonly JigPromptDistanceOptions _jigOpt;
            private double _dragPt;

            public ShapeScaleJig(Shape shape) : base(shape)
            {
                _jigOpt = new JigPromptDistanceOptions()
                {
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.NullResponseAccepted,
                    Cursor = CursorType.RubberBand,
                    Message = $"\n高さを指定"
                };
                _jigOpt.DefaultValue = 1.0;
                _jigOpt.UseBasePoint = true;
                _jigOpt.BasePoint = shape.Position;
            }

            protected override SamplerStatus Sampler(JigPrompts prompts)
            {
                var result = prompts.AcquireDistance(_jigOpt);
                if (result.Value.IsEqual(_dragPt)) return SamplerStatus.NoChange;
                _dragPt = result.Value;
                return SamplerStatus.OK;
            }

            protected override bool Update()
            {
                var shape = (Shape)Entity;
#if _CheckTool_
                if (!shape.IsWriteEnabled)
                {
                    shape.UpgradeOpen();
                }
#endif
                shape.Size = _dragPt;
                return true;
            }
        }

        private class ShapeRotateJig : EntityJig
        {
            private readonly JigPromptAngleOptions _jigOpt;
            private double _dragPt;

            public ShapeRotateJig(Shape shape) : base(shape)
            {
                _jigOpt = new JigPromptAngleOptions()
                {
                    UserInputControls = UserInputControls.Accept3dCoordinates |
                                        UserInputControls.NullResponseAccepted,
                    Cursor = CursorType.RubberBand,
                    Message = $"\n回転角度を指定"
                };
                _jigOpt.DefaultValue = 0.0;
                _jigOpt.UseBasePoint = true;
                _jigOpt.BasePoint = shape.Position;
            }

            protected override SamplerStatus Sampler(JigPrompts prompts)
            {
                var result = prompts.AcquireAngle(_jigOpt);
                if (result.Value.IsEqual(_dragPt)) return SamplerStatus.NoChange;
                _dragPt = result.Value;
                return SamplerStatus.OK;
            }

            protected override bool Update()
            {
                var shape = (Shape)Entity;
#if _CheckTool_
                if (!shape.IsWriteEnabled)
                {
                    shape.UpgradeOpen();
                }
#endif
                shape.Rotation = _dragPt;
                return true;
            }
        }
    }
}
