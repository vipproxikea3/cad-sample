﻿#if _IJCAD_
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;

#endif

using JrxCad.Enum;
using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.Command.Model
{
    public class ModelCmd : BaseCommand
    {
#if DEBUG
        [CommandMethod("JrxCommand", "MD", CommandFlags.Modal | CommandFlags.NoNewStack)]
#else 
        [CommandMethod("JrxCommand", "SmxModel", CommandFlags.Modal | CommandFlags.NoNewStack)]
#endif
        public override void OnCommand()
        {
            if (!Init())
            {
                return;
            }

            try
            {
                ExecuteCommand();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        private void ExecuteCommand()
        {
            SystemVariable.SetInt("TILEMODE", (int)TILEMODE.ModelSpace);
        }
    }
}