﻿#if _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;
using Polyline = GrxCAD.DatabaseServices.Polyline;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
using Autodesk.AutoCAD.GraphicsInterface;

#endif
using JrxCad.Command.PLine;
using JrxCad.Utility;

// ReSharper disable once CheckNamespace
namespace JrxCad.Command.PLineCmd
{
    public partial class PLineCmd
    {
        private readonly IUserInput _userInput;

        public interface IUserInput : IUserInputBase
        {
        }

        private class UserInput : UserInputBase, IUserInput
        {
        }

        private class PolylineBaseJig : PointDrawJigN.JigActions
        {
            private readonly Polyline _pline;
            private readonly Plane _plane;

            internal PolylineBaseJig(Polyline entity)
            {
                _pline = entity;
                _plane = new Plane(Point3d.Origin, Vector3d.ZAxis);
                AddDummyVertex();

                Options = new JigPromptPointOptions()
                {
                    UserInputControls = UserInputControls.Accept3dCoordinates
                };

                if (_pline.NumberOfVertices == 0)
                {
                    Options.Message = $"{PLineRes.UI_P1}";
                }
                else if (_pline.NumberOfVertices > 0)
                {
                    Options.Message = $"\n {PLineRes.UI_P3}";
                }
            }

            public Polyline GetEntity()
            {
                return _pline;
            }

            public void AddDummyVertex()
            {
                _pline.AddVertexAt(_pline.NumberOfVertices, new Point2d(0.0, 0.0), 0.0, 0.0, 0.0);
            }

            public override bool Draw(WorldGeometry geometry)
            {
                if (_pline.NumberOfVertices > 0)
                {
                    _pline.SetBulgeAt(_pline.NumberOfVertices - 1, 0.0);
                    _pline.SetPointAt(_pline.NumberOfVertices - 1, LastValue.Convert2d(_plane));
                }
                geometry.Draw(_pline);
                return true;
            }

            public override void OnUpdate()
            {
            }

            public sealed override JigPromptPointOptions Options { get; set; }
        }
    }
}
