﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.EditorInput;
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;

#endif
using JrxCad.Utility;
using JrxCad.Helpers;
using Exception = System.Exception;

// ReSharper disable once CheckNamespace
namespace JrxCad.Command.PLineCmd
{
    public partial class PLineCmd : BaseCommand
    {
        public PLineCmd() : this(new UserInput())
        {
        }

        public PLineCmd(IUserInput userInput)
        {
            _userInput = userInput;
        }

#if DEBUG
        [CommandMethod("JrxCommand", "PL", CommandFlags.Modal | CommandFlags.NoNewStack)]
#else
        [CommandMethod("JrxCommand", "SmxPLine", CommandFlags.Modal | CommandFlags.NoNewStack)]
#endif
        public override void OnCommand()
        {
            if (!Init())
            {
                return;
            }

            try
            {
                var jig = new PolylineBaseJig(new Polyline());
                var breakFlag = true;
                while (breakFlag)
                {
                    var res = _userInput.AcquirePoint(jig);
                    var status = res.Status;

                    if (status == PromptStatus.OK)
                    {
                        jig.AddDummyVertex();
                    }
                    else if (status == PromptStatus.None)
                    {
                        breakFlag = false;
                    }
                    else if (status == PromptStatus.Cancel)
                    {
                        break;
                    }
                }

                using (var tr = Util.StartTransaction())
                {
                    tr.AddNewlyCreatedDBObject(jig.GetEntity(), true, tr.CurrentSpace());
                    tr.Commit();
                }

            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }
    }
}