﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
#endif

using JrxCad.Utility;
using JrxCad.Enum;
using Exception = System.Exception;
using JrxCad.Command.MinusArray;

// ReSharper disable once UnusedMember.Global
namespace JrxCad.Command.ArrayClassic
{
    public partial class ArrayClassicCmd : BaseCommand
    {
        public ArrayClassicCmd() : this(new WindowInput())
        {
        }

        public ArrayClassicCmd(IWindowInput windowInput)
        {
            _windowInput = windowInput;
        }
#if DEBUG
        [CommandMethod("JrxCommand", "SmxAC", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.NoNewStack | CommandFlags.Session)]
#else
        [CommandMethod("JrxCommand", "SmxARRAYCLASSIC", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.NoNewStack)]
#endif
        public override void OnCommand()
        {
            ObjectIdCollection sourceEntities = null;
            try
            {
                if (!Init())
                {
                    return;
                }

                var cmdActive = SystemVariable.GetInt("CMDACTIVE");
                if (cmdActive == (int)ActiveCommand.Script || cmdActive == (int)ActiveCommand.AutoLISP)
                {
                    var minusArray = new MinusArrayCmd();
                    minusArray.ExecuteCommand(out sourceEntities);
                }
                else
                {
                    _ = _windowInput.ArrayClassicShowDialog();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                sourceEntities?.Dispose();
                Exit();
            }
        }
    }
}
