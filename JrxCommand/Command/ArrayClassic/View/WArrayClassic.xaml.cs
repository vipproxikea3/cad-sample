﻿using JrxCad.View.CustomWpf;
using JrxCad.Command.ArrayClassic.ViewModel;
using System;
using System.Windows;
using JrxCad.Utility;
// ReSharper disable once CheckNamespace
namespace JrxCad.Command.ArrayClassic
{
    // ReSharper disable once RedundantExtendsListEntry
    public partial class WArrayClassic : CustomWindow
    {
        public WArrayClassic()
        {
            InitializeComponent();
            var viewModel = new WArrayClassicViewModel(this, RectangularFrame, PolarFrame, PreviewFrame);
            DataContext = viewModel;
        }

        public void NumericControls_OnValidated(object sender, bool result, string message)
        {
            if (sender == null) throw new ArgumentNullException(nameof(sender));
            if (message == null) throw new ArgumentNullException(nameof(message));
            // Skip control validation
            //if (result) return;

            //NumericUpDown numericUpDown = sender as NumericUpDown;
            //if (numericUpDown.Tag?.ToString() == "tblRowOffset")
            //{
            //    MsgBox.Info.Show(ArrayClassicRes.DM_RowOffsetValidationMessage);
            //}
            //else if (numericUpDown?.Tag.ToString() == "tblColumnOffset")
            //{
            //    MsgBox.Info.Show(ArrayClassicRes.DM_ColumnOffsetValidationMessage);
            //}
            //else if (numericUpDown.Tag?.ToString() == "tblAngleOfArray")
            //{
            //    MsgBox.Info.Show(ArrayClassicRes.DM_AngleValidationMessage);
            //}
            //else
            //{
            //    MsgBox.Info.Show(message);
            //}
        }

        /// <summary>
        /// CommandAction
        /// </summary>
        /// <param name="callback">function execution</param>
        /// <param name="dialogAttack">true if you specify the dialog would be temporarily hidden and re-display after the callback function is done</param>
        private void CommandAction(Action<WArrayClassicViewModel> callback, bool dialogAttack)
        {
            try
            {
                if (!(DataContext is WArrayClassicViewModel model))
                {
                    return;
                }
                if (dialogAttack)
                {
                    using (Util.Editor().StartUserInteraction(this))
                    {
                        callback(model);
                    }
                }
                else
                {
                    callback(model);
                }
            }
            catch (Exception ex)
            {
                MsgBox.Error.Show(ex.Message);
            }
        }

        #region Rectangular Array
        private void BtnPickBothOffset_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.GetDistanceByCorner(), true);
        }

        private void BtnPickRowOffset_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.GetDistanceBetweenRows(), true);
        }

        private void BtnPickColumnOffset_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.GetDistanceBetweenColumns(), true);
        }

        private void BtnPickAngle_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.GetAngle(), true);
        }
        #endregion

        #region Polar Array
        private void BtnPickCenterPoint_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.GetCenterPoint(), true);
        }

        private void BtnPickAngleToFill_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.GetAngleToFill(), true);
        }

        private void BtnPickAngleBetweenItems_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.GetAngleBetweenItems(), true);
        }

        private void BtnMoreInfo_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.ExpandMoreInfo(this), false);
        }

        private void BtnPickBasePoint_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.GetBasePoint(), true);
        }
        #endregion

        #region Common event
        private void BtnSelectObjects_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.SelectEntities(), true);
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.ArrayClassicExecute(this, false), false);
        }

        private void BtnPreview_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.ArrayClassicExecute(this, true), false);
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void BtnHelp_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.OpenHelpWindow(), false);
        }

        private void WArrayClassicWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            CommandAction((model) => model.WindowClosing(), false);
        }

        private void WArrayClassicWindow_Loaded(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.WindowLoaded(this), false);
            //ActualHeight & ActualWidth are available when window was loaded.
            CenterToOwner();
        }
        #endregion
    }
}
