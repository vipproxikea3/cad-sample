﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

#endif
using JrxCad.Utility;
using JrxCad.Helpers;

using Exception = System.Exception;

namespace JrxCad.Command.ArrayClassic.Model
{
    public class RectangularArray : BaseArray
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public double RowOffset { get; set; }
        public double ClmOffset { get; set; }
        public double Angle { get; set; }

        public RectangularArray()
        {
            SourceEntities = new ObjectIdCollection();
            Row = 4;
            Column = 4;
            RowOffset = 1.0;
            ClmOffset = 1.0;
            Angle = 0.0;
        }

        public bool Draw(bool clockwise, bool isPreview)
        {
            bool completed = true;
            using (var normalEntities = new ObjectIdCollection())
            using (var proxyEntities = new ObjectIdCollection())
            {
                using (var trans = Util.StartTransaction())
                {
                    for (int i = 0; i < SourceEntities.Count; i++)
                    {
                        var dbObject = trans.GetObject<DBObject>(SourceEntities[i], OpenMode.ForRead);
                        if (dbObject is null) continue;
                        if (dbObject.IsAProxy) proxyEntities.Add(SourceEntities[i]);
                        else normalEntities.Add(SourceEntities[i]);
                    }
                }

                if (normalEntities.Count > 0) completed &= DrawNormalEntity(normalEntities, clockwise, isPreview);
                if (proxyEntities.Count > 0) completed &= DrawSpecifiedEntity(proxyEntities, clockwise, isPreview);
            }

            return completed;
        }

        public bool DrawSpecifiedEntity(ObjectIdCollection proxyEntities, bool clockwise, bool isPreview)
        {
            try
            {
                var primaryAngle = (clockwise ? -1 : 1) * Angle;

                using (Util.CurDoc().LockDocument())
                using (var objectPreview = new ObjectIdCollection())
                {
                    using (var trans = Util.StartTransaction())
                    {
                        using (new WaitCursor())
                        {
                            var primaryIds = new ObjectIdCollection();
                            foreach (ObjectId objectId in proxyEntities)
                            {
                                _ = primaryIds.Add(objectId);
                            }
                            var database = Util.Database();
                            //copy by direction of col
                            for (var ic = 0; ic < Column; ic++)
                            {
                                for (var ir = 0; ir < Row; ir++)
                                {
                                    if (ic == 0 && ir == 0) continue;
                                    var itemVector = new Vector3d(ic * ClmOffset, ir * RowOffset, 0);
                                    itemVector = itemVector.TransformBy(CoordConverter.UcsToWcs());
                                    itemVector = itemVector.RotateBy(primaryAngle, CoordConverter.UcsToWcs().CoordinateSystem3d.Zaxis);
                                    using (var icMapping = new IdMapping())
                                    {
                                        database.DeepCloneObjects(proxyEntities, database.CurrentSpaceId, icMapping, false);
                                        foreach (IdPair pair in icMapping)
                                        {
                                            if (pair.IsPrimary && pair.IsCloned)
                                            {
                                                using (var cloneEntity = trans.GetObject<Entity>(pair.Value, OpenMode.ForWrite))
                                                {
                                                    if (cloneEntity != null)
                                                    {
                                                        cloneEntity.TransformBy(Matrix3d.Displacement(itemVector));
                                                        _ = primaryIds.Add(pair.Value);
                                                        _ = objectPreview.Add(pair.Value);
                                                        _ = EntityEx.ExplodeFromGroupIfAny(trans, cloneEntity);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        trans.Commit();
                    }
                    Util.Editor().UpdateScreen();
                    if (isPreview)
                    {
                        var optPoint = new PromptPointOptions($"\n{ArrayClassicRes.UI_Preview}")
                        {
                            AllowNone = true,
                        };
                        var input = Util.Editor().GetPoint(optPoint);
                        if (input.Status != PromptStatus.None)
                        {
                            return !Util.EraseObjects(objectPreview);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MsgBox.Error.Show(ex.Message);
                return false;
            }
        }
        public bool DrawNormalEntity(ObjectIdCollection normalEntities, bool clockwise, bool isPreview)
        {
            try
            {
                var primaryVector = new Vector3d(ClmOffset, 0, 0);
                var secondaryVector = new Vector3d(0, RowOffset, 0);
                var primaryAngle = (clockwise ? -1 : 1) * Angle;
                //handle angle by UCS
                primaryVector = primaryVector.TransformBy(CoordConverter.UcsToWcs());
                secondaryVector = secondaryVector.TransformBy(CoordConverter.UcsToWcs());
                var numOfPrimary = Column;
                var numOfSecondary = Row;
                if (Column < Row)
                {
                    (numOfPrimary, numOfSecondary) = (numOfSecondary, numOfPrimary);
                    (primaryVector, secondaryVector) = (secondaryVector, primaryVector);
                }
                using (Util.CurDoc().LockDocument())
                using (var groupListPreview = new ObjectIdCollection())
                using (var objectPreview = new ObjectIdCollection())
                {
                    using (var trans = Util.StartTransaction())
                    {
                        using (new WaitCursor())
                        {
                            var primaryIds = new ObjectIdCollection();
                            foreach (ObjectId objectId in normalEntities)
                            {
                                _ = primaryIds.Add(objectId);
                            }
                            var database = Util.Database();
                            //copy by direction of col
                            primaryVector = primaryVector.RotateBy(primaryAngle, CoordConverter.UcsToWcs().CoordinateSystem3d.Zaxis);
                            for (var i = 1; i < numOfPrimary; i++)
                            {
                                GroupList.Clear();
                                var disVector = primaryVector.MultiplyBy(i);
                                using (var icMapping = new IdMapping())
                                {
                                    database.DeepCloneObjects(normalEntities, database.CurrentSpaceId, icMapping, false);
                                    foreach (IdPair pair in icMapping)
                                    {
                                        if (pair.IsPrimary && pair.IsCloned)
                                        {
                                            using (var cloneEntity = trans.GetObject<Entity>(pair.Value, OpenMode.ForWrite))
                                            {
                                                if (cloneEntity != null)
                                                {
                                                    cloneEntity.TransformBy(Matrix3d.Displacement(disVector));
                                                    _ = primaryIds.Add(pair.Value);
                                                    _ = objectPreview.Add(pair.Value);
                                                    _ = EntityEx.ExplodeFromGroupIfAny(trans, cloneEntity);
                                                    FetchGroup(trans, pair, ref GroupList);
                                                }
                                            }
                                        }
                                    }
                                    //create new group if any
                                    var retGroupIds = CloneGroup(trans, GroupList);
                                    foreach (ObjectId retGroupId in retGroupIds)
                                    {
                                        groupListPreview.Add(retGroupId);
                                    }
                                }
                            }
                            secondaryVector = secondaryVector.RotateBy(primaryAngle, CoordConverter.UcsToWcs().CoordinateSystem3d.Zaxis);
                            for (var i = 1; i < numOfSecondary; i++)
                            {
                                GroupList.Clear();
                                var disVector = secondaryVector.MultiplyBy(i);
                                using (var icMapping = new IdMapping())
                                {
                                    database.DeepCloneObjects(primaryIds, database.CurrentSpaceId, icMapping, false);
                                    foreach (IdPair pair in icMapping)
                                    {
                                        if (pair.IsPrimary && pair.IsCloned)
                                        {
                                            using (var cloneEntity = trans.GetObject<Entity>(pair.Value, OpenMode.ForWrite))
                                            {
                                                if (cloneEntity != null)
                                                {
                                                    cloneEntity.TransformBy(Matrix3d.Displacement(disVector));
                                                    _ = objectPreview.Add(pair.Value);
                                                    _ = EntityEx.ExplodeFromGroupIfAny(trans, cloneEntity);

                                                    FetchGroup(trans, pair, ref GroupList);
                                                }
                                            }
                                        }
                                    }
                                    //create new group if any
                                    var retGroupIds = CloneGroup(trans, GroupList);
                                    foreach (ObjectId retGroupId in retGroupIds)
                                    {
                                        groupListPreview.Add(retGroupId);
                                    }
                                }
                            }
                        }
                        trans.Commit();
                    }
                    Util.Editor().UpdateScreen();
                    if (isPreview)
                    {
                        var optPoint = new PromptPointOptions($"\n{ArrayClassicRes.UI_Preview}")
                        {
                            AllowNone = true,
                        };
                        var input = Util.Editor().GetPoint(optPoint);
                        if (input.Status != PromptStatus.None)
                        {
                            return !(Util.EraseObjects(objectPreview) && Util.EraseObjects(groupListPreview));
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MsgBox.Error.Show(ex.Message);
                return false;
            }
        }
    }
}
