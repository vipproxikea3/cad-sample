﻿#if _IJCAD_
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;

#endif
using System;
using System.Collections.Generic;
using JrxCad.Helpers;
using JrxCad.Utility;

namespace JrxCad.Command.ArrayClassic.Model
{
    public class BaseArray : IDisposable
    {
        public ObjectIdCollection SourceEntities { get; set; }
        protected Dictionary<ObjectId, ObjectIdCollection> GroupList = new Dictionary<ObjectId, ObjectIdCollection>();

        protected ObjectIdCollection CloneGroup(Transaction trans, Dictionary<ObjectId, ObjectIdCollection> groupList)
        {
            var retGroupIds = new ObjectIdCollection();
            if (groupList.Count <= 0) return retGroupIds;
            try
            {
                var lt = trans.GetObject<DBDictionary>(Util.Database().GroupDictionaryId, OpenMode.ForRead);
                foreach (var objIdCol in groupList)
                {
                    using (var newGroup = new Group(string.Empty, true))
                    {
                        lt.UpgradeOpen();
                        retGroupIds.Add(lt.SetAt("*", newGroup));
                        trans.AddNewlyCreatedDBObject(newGroup, true);
                        newGroup.InsertAt(0, objIdCol.Value);
                    }
                }

                return retGroupIds;
            }
            catch (Exception)
            {
                return retGroupIds;
            }
        }

        protected void FetchGroup(Transaction trans, IdPair idPair, ref Dictionary<ObjectId, ObjectIdCollection> groupList)
        {
            try
            {
                using (var keyEntity = trans.GetObject<Entity>(idPair.Key, OpenMode.ForRead))
                {
                    var reactors = keyEntity.GetPersistentReactorIds();
                    if (reactors is null || reactors.Count < 1) return;
                    foreach (ObjectId objectId in reactors)
                    {
                        using (var group = trans.GetObject<Group>(objectId, OpenMode.ForWrite))
                        {
                            if (group == null) continue;
                            if (!groupList.ContainsKey(group.Id))
                            {
                                var childIds = new ObjectIdCollection
                                {
                                    idPair.Value
                                };
                                groupList.Add(group.Id, childIds);
                            }
                            else
                            {
                                groupList[group.Id].Add(idPair.Value);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public void Dispose()
        {
            SourceEntities?.Dispose();
        }
    }
}