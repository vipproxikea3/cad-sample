﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

#endif
using JrxCad.Utility;
using System;
using JrxCad.Helpers;

using Exception = System.Exception;

namespace JrxCad.Command.ArrayClassic.Model
{
    /// <summary>
    /// implement logic of a polar array
    /// </summary>
    public class PolarArray : BaseArray
    {
        public Point3d CenterPoint { get; set; }
        public Point3d BasePoint { get; set; }
        public int Count { get; set; }
        public double FillAngle { get; set; }
        public double ItemAngle { get; set; }
        public bool IsRotate { get; set; }

        /// <summary>
        /// the number for speed up polar array execution
        /// </summary>
        public readonly int MagicNumber = 2000;

        public PolarArray()
        {
            SourceEntities = new ObjectIdCollection();
            CenterPoint = Point3d.Origin;
            BasePoint = Point3d.Origin;
            Count = 4;
            FillAngle = Math.PI * 2;
            ItemAngle = Math.PI / 2;
        }

        public void SetCenterPointX(double x)
        {
            CenterPoint = new Point3d(x, CenterPoint.Y, CenterPoint.Z);
        }

        public string CenterPointXDisplay()
        {
            return Converter.DistanceToString(CenterPoint.X);
        }

        public void SetCenterPointY(double y)
        {
            CenterPoint = new Point3d(CenterPoint.X, y, CenterPoint.Z);
        }

        public string CenterPointYDisplay()
        {
            return Converter.DistanceToString(CenterPoint.Y);
        }

        public string AngleToFillDisplay()
        {
            return Converter.RawAngleToString(FillAngle);
        }

        public string AngleBetweenItemsDisplay()
        {
            return Converter.RawAngleToString(ItemAngle);
        }

        public bool Draw(bool clockwise, bool isPreview)
        {
            bool completed = true;
            using (var normalEntities = new ObjectIdCollection())
            using (var proxyEntities = new ObjectIdCollection())
            {
                using (var trans = Util.StartTransaction())
                {
                    for (int i = 0; i < SourceEntities.Count; i++)
                    {
                        var dbObject = trans.GetObject<DBObject>(SourceEntities[i], OpenMode.ForRead);
                        if (dbObject is null) continue;
                        if (dbObject.IsAProxy) proxyEntities.Add(SourceEntities[i]);
                        else normalEntities.Add(SourceEntities[i]);
                    }
                }

                if (normalEntities.Count > 0) completed &= DrawNormalEntity(normalEntities, clockwise, isPreview);
                if (proxyEntities.Count > 0) completed &= DrawSpecifiedEntity(proxyEntities, clockwise, isPreview);
            }

            return completed;
        }
        public bool DrawNormalEntity(ObjectIdCollection normalEntities, bool clockwise, bool isPreview)
        {
            try
            {
                var angleBetweenItems = (FillAngle > 0 ? 1 : -1) * ItemAngle;
                angleBetweenItems = (clockwise ? -1 : 1) * angleBetweenItems;
                var basePointU = BasePoint.TransformBy(CoordConverter.WcsToUcs());
                var vecByCenterPtAndBasePt = CenterPoint.GetVectorTo(basePointU);
                vecByCenterPtAndBasePt = vecByCenterPtAndBasePt.TransformBy(CoordConverter.UcsToWcs());
                //execution
                using (Util.CurDoc().LockDocument())
                using (var groupListPreview = new ObjectIdCollection())
                using (var objectPreview = new ObjectIdCollection())
                {
                    using (var baseGroupObject = new ObjectIdCollection())
                    using (var trans = Util.StartTransaction())
                    {
                        using (new WaitCursor())
                        {
                            var database = Util.Database();
                            if (Count / MagicNumber > 1 && IsRotate)
                            {
                                var factorDraw = Count / MagicNumber;
                                var remaining = Count % MagicNumber;
                                foreach (ObjectId objectId in normalEntities)
                                {
                                    _ = baseGroupObject.Add(objectId);
                                }
                                //draw magic objects
                                for (var i = 1; i < MagicNumber; i++)
                                {
                                    GroupList.Clear();
                                    var itemVector = vecByCenterPtAndBasePt.RotateBy(angleBetweenItems * i, CoordConverter.UcsZAxis());
                                    var itemPolarVector = itemVector - vecByCenterPtAndBasePt;
                                    using (var icMapping = new IdMapping())
                                    {
                                        database.DeepCloneObjects(normalEntities, database.CurrentSpaceId, icMapping, false);
                                        foreach (IdPair pair in icMapping)
                                        {
                                            if (pair.IsPrimary && pair.IsCloned)
                                            {
                                                using (var clonedEntity = trans.GetObject<Entity>(pair.Value, OpenMode.ForWrite))
                                                {
                                                    if (clonedEntity != null)
                                                    {
                                                        if (IsRotate)
                                                        {
                                                            clonedEntity.TransformBy(Matrix3d.Rotation(angleBetweenItems * i, CoordConverter.UcsZAxis(), BasePoint));
                                                        }
                                                        clonedEntity.TransformBy(Matrix3d.Displacement(itemPolarVector));
                                                        _ = baseGroupObject.Add(pair.Value);
                                                        _ = objectPreview.Add(pair.Value);
                                                        _ = EntityEx.ExplodeFromGroupIfAny(trans, clonedEntity);

                                                        FetchGroup(trans, pair, ref GroupList);
                                                    }
                                                }
                                            }
                                        }
                                        //create new group if any
                                        var retGroupIds = CloneGroup(trans, GroupList);
                                        foreach (ObjectId retGroupId in retGroupIds)
                                        {
                                            groupListPreview.Add(retGroupId);
                                        }
                                    }
                                }

                                //multi magic objects
                                for (var i = 1; i < factorDraw; i++)
                                {
                                    GroupList.Clear();
                                    var itemVector = vecByCenterPtAndBasePt.RotateBy(angleBetweenItems * MagicNumber * i, CoordConverter.UcsZAxis());
                                    var itemPolarVector = itemVector - vecByCenterPtAndBasePt;
                                    using (var icMapping = new IdMapping())
                                    {
                                        database.DeepCloneObjects(baseGroupObject, database.CurrentSpaceId, icMapping, false);
                                        foreach (IdPair pair in icMapping)
                                        {
                                            if (pair.IsPrimary && pair.IsCloned)
                                            {
                                                using (var cloneEntity = trans.GetObject<Entity>(pair.Value, OpenMode.ForWrite))
                                                {
                                                    if (cloneEntity != null)
                                                    {
                                                        cloneEntity.TransformBy(Matrix3d.Rotation(angleBetweenItems * MagicNumber * i, CoordConverter.UcsZAxis(), BasePoint));
                                                        cloneEntity.TransformBy(Matrix3d.Displacement(itemPolarVector));
                                                        _ = objectPreview.Add(pair.Value);
                                                        _ = EntityEx.ExplodeFromGroupIfAny(trans, cloneEntity);

                                                        FetchGroup(trans, pair, ref GroupList);
                                                    }
                                                }
                                            }
                                        }
                                        //create new group if any
                                        var retGroupIds = CloneGroup(trans, GroupList);
                                        foreach (ObjectId retGroupId in retGroupIds)
                                        {
                                            groupListPreview.Add(retGroupId);
                                        }
                                    }
                                }

                                //draw remaining objects
                                if (remaining > 0)
                                {
                                    GroupList.Clear();
                                    var startAngleRemaining = angleBetweenItems * MagicNumber * factorDraw;
                                    for (var i = 0; i < remaining; i++)
                                    {
                                        var itemVector = vecByCenterPtAndBasePt.RotateBy(startAngleRemaining + (angleBetweenItems * i), CoordConverter.UcsZAxis());
                                        var itemPolarVector = itemVector - vecByCenterPtAndBasePt;
                                        using (var icMapping = new IdMapping())
                                        {
                                            database.DeepCloneObjects(normalEntities, database.CurrentSpaceId, icMapping, false);
                                            foreach (IdPair pair in icMapping)
                                            {
                                                if (pair.IsPrimary && pair.IsCloned)
                                                {
                                                    using (var cloneEntity = trans.GetObject<Entity>(pair.Value, OpenMode.ForWrite))
                                                    {
                                                        if (cloneEntity != null)
                                                        {
                                                            if (IsRotate)
                                                            {
                                                                cloneEntity.TransformBy(Matrix3d.Rotation(startAngleRemaining + (angleBetweenItems * i), CoordConverter.UcsZAxis(), BasePoint));
                                                            }
                                                            cloneEntity.TransformBy(Matrix3d.Displacement(itemPolarVector));
                                                            _ = baseGroupObject.Add(pair.Value);
                                                            _ = objectPreview.Add(pair.Value);
                                                            _ = EntityEx.ExplodeFromGroupIfAny(trans, cloneEntity);

                                                            FetchGroup(trans, pair, ref GroupList);
                                                        }
                                                    }
                                                }
                                            }
                                            //create new group if any
                                            var retGroupIds = CloneGroup(trans, GroupList);
                                            foreach (ObjectId retGroupId in retGroupIds)
                                            {
                                                groupListPreview.Add(retGroupId);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            { //the number of selected entities less than Magic number
                                for (var i = 1; i < Count; i++)
                                {
                                    GroupList.Clear();
                                    var itemVector = vecByCenterPtAndBasePt.RotateBy(angleBetweenItems * i, CoordConverter.UcsZAxis());
                                    var itemPolarVector = itemVector - vecByCenterPtAndBasePt;
                                    using (var icMapping = new IdMapping())
                                    {
                                        database.DeepCloneObjects(normalEntities, database.CurrentSpaceId, icMapping, false);
                                        foreach (IdPair pair in icMapping)
                                        {
                                            if (pair.IsPrimary && pair.IsCloned)
                                            {
                                                using (var clonedEntity = trans.GetObject<Entity>(pair.Value, OpenMode.ForWrite))
                                                {
                                                    if (clonedEntity != null)
                                                    {
                                                        if (IsRotate)
                                                        {
                                                            clonedEntity.TransformBy(Matrix3d.Rotation(angleBetweenItems * i, CoordConverter.UcsZAxis(), BasePoint));
                                                        }
                                                        clonedEntity.TransformBy(Matrix3d.Displacement(itemPolarVector));
                                                        _ = objectPreview.Add(pair.Value);
                                                        _ = EntityEx.ExplodeFromGroupIfAny(trans, clonedEntity);

                                                        FetchGroup(trans, pair, ref GroupList);
                                                    }
                                                }
                                            }
                                        }
                                        //create new group if any
                                        var retGroupIds = CloneGroup(trans, GroupList);
                                        foreach (ObjectId retGroupId in retGroupIds)
                                        {
                                            groupListPreview.Add(retGroupId);
                                        }
                                    }
                                }
                            }
                        }
                        trans.Commit();
                    }
                    Util.Editor().UpdateScreen();
                    if (isPreview)
                    {
                        var optPoint = new PromptPointOptions($"\n{ArrayClassicRes.UI_Preview}")
                        {
                            AllowNone = true,
                        };
                        var input = Util.Editor().GetPoint(optPoint);
                        if (input.Status != PromptStatus.None)
                        {
                            return !(Util.EraseObjects(objectPreview) && Util.EraseObjects(groupListPreview));
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.ToString());
                return false;
            }
        }

        public bool DrawSpecifiedEntity(ObjectIdCollection proxyEntities, bool clockwise, bool isPreview)
        {
            try
            {
                var angleBetweenItems = (FillAngle > 0 ? 1 : -1) * ItemAngle;
                angleBetweenItems = (clockwise ? -1 : 1) * angleBetweenItems;
                var basePointU = BasePoint.TransformBy(CoordConverter.WcsToUcs());
                var vecByCenterPtAndBasePt = CenterPoint.GetVectorTo(basePointU);
                vecByCenterPtAndBasePt = vecByCenterPtAndBasePt.TransformBy(CoordConverter.UcsToWcs());

                //execution
                using (Util.CurDoc().LockDocument())
                using (var objectPreview = new ObjectIdCollection())
                {
                    using (var trans = Util.StartTransaction())
                    {
                        using (new WaitCursor())
                        {
                            var database = Util.Database();
                            for (var i = 0; i < Count - 1; i++)
                            {
                                var itemVector = vecByCenterPtAndBasePt.RotateBy(angleBetweenItems * (i + 1), CoordConverter.UcsZAxis());
                                var itemPolarVector = itemVector - vecByCenterPtAndBasePt;
                                using (var icMapping = new IdMapping())
                                {
                                    database.DeepCloneObjects(proxyEntities, database.CurrentSpaceId, icMapping, false);
                                    foreach (IdPair pair in icMapping)
                                    {
                                        if (pair.IsPrimary && pair.IsCloned)
                                        {
                                            using (var cloneEntity = trans.GetObject<Entity>(pair.Value, OpenMode.ForWrite))
                                            {
                                                if (cloneEntity != null)
                                                {
                                                    if (IsRotate)
                                                    {
                                                        cloneEntity.TransformBy(Matrix3d.Rotation(angleBetweenItems * (i + 1), CoordConverter.UcsZAxis(), BasePoint));
                                                    }
                                                    cloneEntity.TransformBy(Matrix3d.Displacement(itemPolarVector));
                                                    _ = objectPreview.Add(pair.Value);
                                                    _ = EntityEx.ExplodeFromGroupIfAny(trans, cloneEntity);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        trans.Commit();
                    }
                    Util.Editor().UpdateScreen();
                    if (isPreview)
                    {
                        var optPoint = new PromptPointOptions($"\n{ArrayClassicRes.UI_Preview}")
                        {
                            AllowNone = true,
                        };
                        var input = Util.Editor().GetPoint(optPoint);
                        if (input.Status != PromptStatus.None)
                        {
                            return !Util.EraseObjects(objectPreview);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.ToString());
                return false;
            }
        }
    }
}
