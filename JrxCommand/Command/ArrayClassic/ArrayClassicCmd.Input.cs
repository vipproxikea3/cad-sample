﻿using System;
using JrxCad.Utility;

// ReSharper disable once UnusedMember.Global
namespace JrxCad.Command.ArrayClassic
{
    public partial class ArrayClassicCmd
    {
        private readonly IWindowInput _windowInput;

        /// <summary>
        /// IWindowInput Interface
        /// </summary>
        public interface IWindowInput
        {
            bool? ArrayClassicShowDialog();
        }

        /// <summary>
        /// WindowInput
        /// </summary>
        private class WindowInput : IWindowInput
        {
            public bool? ArrayClassicShowDialog()
            {
                try
                {
                    var form = new WArrayClassic();
                    return form.ShowDialog();
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex);
                    return false;
                }
            }
        }
    }
}
