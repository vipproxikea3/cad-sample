﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
#endif

using JrxCad.Utility;
using JrxCad.Command.MinusArray;
using Exception = System.Exception;

// ReSharper disable once CommentTypo
// ReSharper disable once UnusedMember.Global
namespace JrxCad.Command.ArrayClassic
{
    public class MinusArrayClassicCmd : BaseCommand
    {
#if DEBUG
        [CommandMethod("JrxCommand", "-SmxAC", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.NoNewStack)]
#else
        [CommandMethod("JrxCommand", "-SmxARRAYCLASSIC", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.NoNewStack)]
#endif
        public override void OnCommand()
        {
            ObjectIdCollection sourceEntities = null;
            try
            {
                if (!Init())
                {
                    return;
                }
                var minusArray = new MinusArrayCmd();
                minusArray.ExecuteCommand(out sourceEntities);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                sourceEntities?.Dispose();
                Exit();
            }
        }
    }
}
