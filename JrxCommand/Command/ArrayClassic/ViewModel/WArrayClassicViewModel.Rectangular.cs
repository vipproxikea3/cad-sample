﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

#endif
using System;
using System.Globalization;
using JrxCad.View.CustomWpf;
using JrxCad.Utility;
using JrxCad.Command.ArrayClassic.Model;
using JrxCad.Helpers;
using Exception = System.Exception;

//Rectangular Array
namespace JrxCad.Command.ArrayClassic.ViewModel
{
    public partial class WArrayClassicViewModel
    {
        #region Fields
        private RectangularArray RectArr { get; set; }

        /// <summary>
        /// the number of rows of rectangular array
        /// </summary>
        private string _txtNumOfRows;
        public string TxtNumOfRows
        {
            get => _txtNumOfRows;
            set
            {
                try
                {
                    RectArr.Row = int.Parse(value);
                    _txtNumOfRows = value;
                    OnPropertyChanged();
                    if (RectCurrentChange == RectFactor.NeedToUpdatePreview)
                    {
                        RectPreviewUpdate();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex.Message);
                }
            }
        }

        /// <summary>
        /// the number of columns of rectangular array
        /// </summary>
        private string _txtNumOfColumns;
        public string TxtNumOfColumns
        {
            get => _txtNumOfColumns;
            set
            {
                try
                {
                    RectArr.Column = int.Parse(value);
                    _txtNumOfColumns = value;
                    OnPropertyChanged();
                    if (RectCurrentChange == RectFactor.NeedToUpdatePreview)
                    {
                        RectPreviewUpdate();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex.Message);
                }
            }
        }

        /// <summary>
        /// row offset
        /// </summary>
        private string _txtRowOffset;
        public string TxtRowOffset
        {
            get => _txtRowOffset;
            set
            {
                try
                {
                    var temp = double.Parse(value);
                    RectArr.RowOffset = temp;
                    _txtRowOffset = Converter.DistanceToString(temp);
                    OnPropertyChanged();
                    if (RectCurrentChange == RectFactor.NeedToUpdatePreview)
                    {
                        RectPreviewUpdate();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex.Message);
                }
            }
        }

        /// <summary>
        /// column offset
        /// </summary>
        private string _txtColumnOffset;
        public string TxtColumnOffset
        {
            get => _txtColumnOffset;
            set
            {
                try
                {
                    var temp = double.Parse(value);
                    RectArr.ClmOffset = temp;
                    _txtColumnOffset = Converter.DistanceToString(temp);
                    OnPropertyChanged();
                    if (RectCurrentChange == RectFactor.NeedToUpdatePreview)
                    {
                        RectPreviewUpdate();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex.Message);
                }
            }
        }

        /// <summary>
        /// angle of rectangular array
        /// </summary>
        private string _txtAngle;
        public string TxtAngle
        {
            get => _txtAngle;
            set
            {
                try
                {
                    RectArr.Angle = Converter.StringToRawAngle(value);
                    _txtAngle = value;
                    OnPropertyChanged();
                    if (RectCurrentChange == RectFactor.NeedToUpdatePreview)
                    {
                        RectPreviewUpdate();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex.Message);
                }
            }
        }

        /// <summary>
        /// Flags for property changing logic
        /// </summary>
        private enum RectFactor
        {
            NeedToUpdatePreview,
            Other
        }

        /// <summary>
        /// Storage flag for property changing logic
        /// </summary>
        private RectFactor RectCurrentChange { get; set; }
        #endregion

        #region RectangularInitilize
        private void RectangularInitialize()
        {
            //properties initialize
            RectArr = new RectangularArray();
            //set default properties
            RectCurrentChange = RectFactor.Other;
            TxtNumOfRows = DefaultProperties.Rectangular.NumOfRows.ToString();
            TxtNumOfColumns = DefaultProperties.Rectangular.NumOfColumns.ToString();
            TxtRowOffset = DefaultProperties.Rectangular.RowOffset.ToString(CultureInfo.CurrentCulture);
            TxtColumnOffset = DefaultProperties.Rectangular.ColumnOffset.ToString(CultureInfo.CurrentCulture);
            TxtAngle = Converter.AngleToString(DefaultProperties.Rectangular.AngleOfArray);
            RectCurrentChange = RectFactor.NeedToUpdatePreview;
        }
        #endregion

        #region Relay Command Functions
        /// <summary>
        /// command get both row offset and column offset
        /// </summary>
        public void GetDistanceByCorner()
        {
            var promptStartPointOptions = new PromptPointOptions($"\n{ArrayClassicRes.UI_R_Both1}")
            {
                AllowNone = true,
            };
            var promptStartPointResult = Util.Editor().GetPoint(promptStartPointOptions);
            if (promptStartPointResult.Status != PromptStatus.OK)
            {
                return;
            }
            var startPoint = promptStartPointResult.Value;
            var promptCornerOptions = new PromptCornerOptions($"\n{ArrayClassicRes.UI_R_Both2}", startPoint)
            {
                AllowNone = true,
                UseDashedLine = true,
                BasePoint = startPoint,
            };
            var pcr = Util.Editor().GetCorner(promptCornerOptions);

            if (pcr.Status != PromptStatus.OK)
            {
                return;
            }
            var endPoint = pcr.Value;
            var distance = startPoint.DistanceTo(endPoint);
            var angle = Vector3d.XAxis.GetAngleTo(endPoint - startPoint, Vector3d.ZAxis);
            var rectHeight = Math.Sin(angle) * distance;
            var rectWidth = Math.Cos(angle) * distance;

            TxtRowOffset = rectHeight.ToString(CultureInfo.CurrentCulture);
            TxtColumnOffset = rectWidth.ToString(CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// command pick angle of array
        /// </summary>
        public void GetAngle()
        {
            var promptAngleOptions = new PromptAngleOptions($"\n{ArrayClassicRes.UI_R_Angle}")
            {
                AllowNone = true,
                UseAngleBase = true,
                UseDashedLine = true
            };
            var promptDoubleResult = Util.Editor().GetAngle(promptAngleOptions);
            if (promptDoubleResult.Status != PromptStatus.OK)
            {
                return;
            }
            var angle = promptDoubleResult.Value;
            TxtAngle = Converter.RawAngleToString(angle);
        }

        /// <summary>
        /// command pick row offset
        /// </summary>
        public void GetDistanceBetweenRows()
        {
            var promptStartPtOptions = new PromptPointOptions($"\n{ArrayClassicRes.UI_R_Row}")
            {
                AllowNone = true,
            };
            var promptStartPtResult = Util.Editor().GetPoint(promptStartPtOptions);
            if (promptStartPtResult.Status != PromptStatus.OK)
            {
                return;
            }
            var startPoint = promptStartPtResult.Value;

            var promptEndPtOptions = new PromptPointOptions($"\n{ArrayClassicRes.UI_R_Row}")
            {
                AllowNone = true,
                UseBasePoint = true,
                BasePoint = startPoint,
            };
            var promptEndPtResult = Util.Editor().GetPoint(promptEndPtOptions);
            if (promptEndPtResult.Status != PromptStatus.OK)
            {
                return;
            }
            var endPoint = promptEndPtResult.Value;
            var distance = startPoint.DistanceTo(endPoint);
            var angle = Vector3d.XAxis.GetAngleTo(endPoint - startPoint, Vector3d.ZAxis);
            if (angle > Math.PI)
            {
                distance *= -1;
            }
            TxtRowOffset = Converter.DistanceToString(distance);
        }

        /// <summary>
        /// command pick column offset
        /// </summary>
        public void GetDistanceBetweenColumns()
        {
            var promptStartPtOptions = new PromptPointOptions($"\n{ArrayClassicRes.UI_R_Column}")
            {
                AllowNone = true,
            };
            var promptStartPointResult = Util.Editor().GetPoint(promptStartPtOptions);
            if (promptStartPointResult.Status != PromptStatus.OK)
            {
                return;
            }
            var startPoint = promptStartPointResult.Value;

            var promptEndPointOptions = new PromptPointOptions($"\n{ArrayClassicRes.UI_R_Column}")
            {
                AllowNone = true,
                UseBasePoint = true,
                BasePoint = startPoint,
            };
            var promptEndPointResult = Util.Editor().GetPoint(promptEndPointOptions);
            if (promptEndPointResult.Status != PromptStatus.OK)
            {
                return;
            }
            var endPoint = promptEndPointResult.Value;
            var distance = startPoint.DistanceTo(endPoint);
            var angle = Vector3d.YAxis.GetAngleTo(endPoint - startPoint, Vector3d.ZAxis);
            if (angle < Math.PI)
            {
                distance *= -1;
            }
            TxtColumnOffset = Converter.DistanceToString(distance);
        }

        private bool RectArrayValidation()
        {
            try
            {
                var maxArray = EnvironmentVariable.GetInt("MaxArray");
                var totalObjectsWillBeCreated = RectArr.Row * RectArr.Column * RectArr.SourceEntities.Count;
                if (totalObjectsWillBeCreated > maxArray)
                {
                    var error = string.Format(ArrayClassicRes.DM_TooMany, maxArray, totalObjectsWillBeCreated, maxArray);
                    MsgBox.Error.Show(error);
                    return false;
                }

                if (RectArr.Row == 1 && RectArr.Column == 1)
                {
                    var error = string.Format(ArrayClassicRes.DM_Rect1, maxArray);
                    MsgBox.Error.Show(error);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
                return false;
            }
        }

        /// <summary>
        /// command OK to rectangular array
        /// </summary>
        /// <param name="currentWindow"></param>
        /// <param name="isPreview"></param>
        private void ExecuteRectangularArray(CustomWindow currentWindow, bool isPreview)
        {
            try
            {
                RectArr.SourceEntities = CurObjectIds;
                //IJ-CAD: dot not use StartUserInteraction
                currentWindow.Hide();
                var completed = RectArrayValidation() && RectArr.Draw(AngDir, isPreview);
                if (completed)
                {
                    //save successfully state
                    DefaultProperties.PolarActive = 0;
                    DefaultProperties.Rectangular.Collect(
                        RectArr.Row,
                        RectArr.Column,
                        RectArr.RowOffset,
                        RectArr.ClmOffset,
                        RectArr.Angle);

                    currentWindow.Close();
                }
                else
                {
                    currentWindow.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
            }
        }
        #endregion

        #region Preview Frame
        public void RectPreviewUpdate()
        {
            try
            {
                if (PolarActive != 0) return;
                var addDownward = RectArr.RowOffset < 0 ? -1 : 1;
                var addLeft = RectArr.ClmOffset < 0 ? -1 : 1;
                CanvasHelper.CalcRectArray(
                    _previewFrame,
                    RectArr,
                    addDownward,
                    addLeft,
                    AngDir);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
            }
        }
        #endregion
    }
}
