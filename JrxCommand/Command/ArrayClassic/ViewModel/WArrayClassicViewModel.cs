﻿#if _IJCAD_
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;

#endif
using System;
using System.Windows.Controls;
using System.Windows.Input;
using Autodesk.AutoCAD.Geometry;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCad.View.CustomWpf;
using Visibility = System.Windows.Visibility;
using Exception = System.Exception;

//ArrayClassic
namespace JrxCad.Command.ArrayClassic.ViewModel
{
    public partial class WArrayClassicViewModel : BindableBase
    {
        #region Fields
        protected override string HelpName => "ArrayClassic";

        private bool _windowLoaded;
        public bool AngDir { get; set; }
        /// <summary>
        /// declare height of window support for button more/less of polar array
        /// </summary>
        private const int Height = 380;
        private const int HeightPlus = 480;

        /// <summary>
        /// UI frame
        /// </summary>
        private readonly StackPanel _rectangularFrame;
        private readonly StackPanel _polarFrame;

        /// <summary>
        /// storage value for radiobutton
        /// </summary>
        private int _polarActive;
        public int PolarActive
        {
            get => _polarActive;
            set
            {
                _ = SetProperty(ref _polarActive, value);
                if (value == 0)
                {
                    //First of all:
                    //rectangularFrame: Visibility <default>, polarFrame: Hidden <do not set Collapse>
                    HandleMoreLessBehavior(That, false);
                    _polarFrame.Visibility = Visibility.Hidden;
                    _rectangularFrame.Visibility = Visibility.Visible;
                    CanvasHelper.RectAxisInitialization(_previewFrame);
                    RectPreviewUpdate();
                }
                else
                {
                    _rectangularFrame.Visibility = Visibility.Hidden;
                    _polarFrame.Visibility = Visibility.Visible;
                    HandleMoreLessBehavior(That, IsMoreInfo);
                    CanvasHelper.PolarAxisInitialization(_previewFrame);
                    PolarPreviewUpdate();
                }
            }
        }

        /// <summary>
        /// Is button OK & Preview enable?
        /// </summary>
        private bool _isReadyForExecution;
        public bool IsReadyForExecution
        {
            get => _isReadyForExecution;
            set => SetProperty(ref _isReadyForExecution, value);
        }

        /// <summary>
        /// for label's text that expresses the number of objects selected
        /// </summary>
        private string _currentNumberObjectIdsString;
        public string CurrentNumberObjectIdsString
        {
            get => _currentNumberObjectIdsString;
            set => SetProperty(ref _currentNumberObjectIdsString, value);
        }

        /// <summary>
        /// storage the objects selected
        /// </summary>
        private ObjectIdCollection _curObjectIds;
        public ObjectIdCollection CurObjectIds
        {
            get => _curObjectIds;
            set
            {
                var numOfObjects = value == null ? 0 : value.Count;
                IsReadyForExecution = numOfObjects > 0;
                CurrentNumberObjectIdsString = string.Format(ArrayClassicRes.Lbl_ObjectSelected, numOfObjects);
                _ = SetProperty(ref _curObjectIds, value);
            }
        }
        public WArrayClassic That;
        private readonly Canvas _previewFrame;
        #endregion

        #region Constructors
        public WArrayClassicViewModel(WArrayClassic that,
            StackPanel rectangularFrame,
            StackPanel polarFrame,
            Canvas canvas)
        {
            That = that;
            _previewFrame = canvas;
            _rectangularFrame = rectangularFrame;
            _polarFrame = polarFrame;

            ArrayInitialize();
            //handle current direction of drawing
            AngDir = SystemVariable.GetBool("ANGDIR");
            if (AngDir)
            {
                SystemVariable.SetBool("ANGDIR", false);
            }
        }
        private void ArrayInitialize()
        {
            //do not re-order the code below
            RectangularInitialize();
            PolarInitialize();
            PolarActive = DefaultProperties.PolarActive;

            //SelectImplied
            SelectObject((entity, tr) => entity.SelectImplied(Util.CurDoc(), tr));
        }

        #endregion

        #region Relay Command Functions

        public void WindowLoaded(CustomWindow currentWindow)
        {
            _windowLoaded = true;
            try
            {
                var okButton = currentWindow.FindChildrenByName<Button>("BtnOk");
                if (okButton != null && okButton.IsEnabled)
                {
                    _ = Keyboard.Focus(okButton);
                }

                MethodSelected = PolarMethod[DefaultProperties.Polar.MethodIndex];
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        public void ArrayClassicExecute(CustomWindow currentWindow, bool isPreview)
        {
            if (PolarActive == 0)
            {
                ExecuteRectangularArray(currentWindow, isPreview);
            }
            else
            {
                ExecutePolarArray(currentWindow, isPreview);
            }
        }

        /// <summary>
        /// Closing event
        /// </summary>
        public void WindowClosing()
        {
            //recovery angle dir
            SystemVariable.SetBool("ANGDIR", AngDir);
            _curObjectIds?.Dispose();
            RectArr?.Dispose();
            PolarArr?.Dispose();
        }
        #endregion

        #region Common Functions
        public void SelectEntities()
        {
            SelectObject((entity, tr) => entity.SelectEntities(Util.CurDoc(), tr));
        }

        private void SelectObject(Func<EntityEx, Transaction, ObjectIdCollection> select)
        {
            try
            {
                using (Util.CurDoc().LockDocument())
                using (var transaction = Util.StartTransaction())
                {
                    //図形選択
                    using (var entityEx = new EntityEx())
                    {
                        CurObjectIds = select(entityEx, transaction);
                        if (entityEx.ObjectOnLockedLayer > 0)
                        {
                            MsgBox.Info.Show(string.Format(ArrayClassicRes.DM_Locked, entityEx.ObjectOnLockedLayer));
                        }

                        if (CurObjectIds == null || CurObjectIds.Count == 0)
                        {
                            return;
                        }
                        DefaultBasePoint = entityEx.GetBasePoint();
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                MsgBox.Error.Show(ex.Message);
            }
        }

        #endregion

        #region Static Class - Save data in the success state
        private static class DefaultProperties
        {
            public static int PolarActive;

            public static class Rectangular
            {
                //rectangular properties
                public static int NumOfRows = 4;
                public static int NumOfColumns = 4;
                public static double RowOffset = 1.0;
                public static double ColumnOffset = 1.0;
                public static double AngleOfArray;

                public static void Collect(
                    int numOfRows,
                    int numOfColumns,
                    double rowOffset,
                    double columnOffset,
                    double angleOfArray
                    )
                {
                    NumOfRows = numOfRows;
                    NumOfColumns = numOfColumns;
                    RowOffset = rowOffset;
                    ColumnOffset = columnOffset;
                    AngleOfArray = angleOfArray;
                }
            }

            public static class Polar
            {
                //polar properties
                public static double CenterPointX;
                public static double CenterPointY;
                public static int MethodIndex;
                public static int TotalNumberOfItems = 4;
                public static double FillAngle = Math.PI * 2;
                public static double ItemAngle = Math.PI / 2;
                public static bool RotateItemsAsCopied = true;

                public static void Collect(
                    double centerPointX,
                    double centerPointY,
                    int methodId,
                    int totalNumberOfItems,
                    double fillAngle,
                    double itemAngle,
                    bool rotateItemsAsCopied
                    )
                {
                    CenterPointX = centerPointX;
                    CenterPointY = centerPointY;
                    MethodIndex = methodId;
                    TotalNumberOfItems = totalNumberOfItems;
                    FillAngle = fillAngle;
                    ItemAngle = itemAngle;
                    RotateItemsAsCopied = rotateItemsAsCopied;
                }
            }
        }
        #endregion
    }
}
