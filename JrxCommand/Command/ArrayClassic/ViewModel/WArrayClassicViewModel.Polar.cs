﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

#endif
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using JrxCad.Command.ArrayClassic.Model;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCad.View.CustomWpf;
using Exception = System.Exception;

//Polar Array
namespace JrxCad.Command.ArrayClassic.ViewModel
{
    public partial class WArrayClassicViewModel
    {
        #region Fields
        private PolarArray PolarArr { get; set; }

        private string _txtCenterX;
        public string TxtCenterX
        {
            get => _txtCenterX;
            set
            {
                try
                {
                    var temp = double.Parse(value);
                    PolarArr.SetCenterPointX(temp);
                    _ = SetProperty(ref _txtCenterX, PolarArr.CenterPointXDisplay());
                    if (PolarCurrentChange != PolarFactor.CenterPointX)
                    {
                        PolarPreviewUpdate();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex.Message);
                }
            }
        }

        private string _txtCenterY;
        public string TxtCenterY
        {
            get => _txtCenterY;
            set
            {
                try
                {
                    var temp = double.Parse(value);
                    PolarArr.SetCenterPointY(temp);
                    _ = SetProperty(ref _txtCenterY, PolarArr.CenterPointYDisplay());
                    if (PolarCurrentChange != PolarFactor.CenterPointY)
                    {
                        PolarPreviewUpdate();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex.Message);
                }
            }
        }

        private bool _rotateItemsAsCopied = true;
        public bool RotateItemsAsCopied
        {
            get => _rotateItemsAsCopied;
            set
            {
                PolarArr.IsRotate = value;
                _ = SetProperty(ref _rotateItemsAsCopied, value);
                if (PolarCurrentChange != PolarFactor.RotateItemsAsCopied)
                {
                    PolarPreviewUpdate();
                }
            }
        }

        private string _txtTotalNumOfItems; //range: 2 -> 32767
        public string TxtTotalNumOfItems
        {
            get => _txtTotalNumOfItems;
            set
            {
                try
                {
                    var temp = int.Parse(value);
                    PolarArr.Count = temp;
                    _ = SetProperty(ref _txtTotalNumOfItems, value);
                    if (PolarCurrentChange != PolarFactor.TotalNumOfItems)
                    {
                        CalculateItemsChanged();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex.Message);
                }
            }
        }

        private string _txtAngleToFill;
        public string TxtAngleToFill
        {
            get => _txtAngleToFill;
            set
            {
                try
                {
                    if (PolarCurrentChange == PolarFactor.AngleToFill)
                    {
                        var temp = double.Parse(value);
                        PolarArr.FillAngle = temp;
                        _ = SetProperty(ref _txtAngleToFill, PolarArr.AngleToFillDisplay());
                    }
                    else
                    {
                        using (new SystemVariable.Temp("ANGBASE", 0.0))
                        {
                            var preValue = Converter.StringToRawAngle(value);
                            var direction = Util.AngleNormalize(ref preValue);
                            //The Angle to fill not equal 0
                            if (preValue.IsEqual(0.0))
                            {
                                preValue = Math.PI * 2 * direction;
                            }
                            PolarArr.FillAngle = preValue;
                            _ = SetProperty(ref _txtAngleToFill, PolarArr.AngleToFillDisplay());
                            CalculateFillAngleChanged();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex.Message);
                }
            }
        }

        private string _txtAngleBetweenItems;
        public string TxtAngleBetweenItems
        {
            get => _txtAngleBetweenItems;
            set
            {
                try
                {
                    if (PolarCurrentChange == PolarFactor.AngleBetweenItems)
                    {
                        var temp = double.Parse(value);
                        PolarArr.ItemAngle = temp;
                        _ = SetProperty(ref _txtAngleBetweenItems, PolarArr.AngleBetweenItemsDisplay());
                    }
                    else
                    {
                        using (new SystemVariable.Temp("ANGBASE", 0.0))
                        {
                            var preValue = Converter.StringToAngle(value);
                            _ = Util.AngleNormalize(ref preValue);
                            if (preValue.Less(0.0))
                            {
                                MsgBox.Info.Show(ArrayClassicRes.DM_ItemAngleGtZero);
                                return;
                            }
                            if (preValue.IsEqual(0.0))
                            {
                                preValue = Math.PI * 2;
                            }
                            PolarArr.ItemAngle = preValue;
                            _ = SetProperty(ref _txtAngleBetweenItems, PolarArr.AngleBetweenItemsDisplay());
                            CalculateAngleBetweenItemsChanged();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex.Message);
                }
            }
        }

        private enum MethodValue
        {
            TotalNumberOfItemAndAngleToFill = 0,
            TotalNumberOfItemAndAngleBetweenItems = 1,
            AngleToFillAndAngleBetweenItems = 2
        }

        private Method _methodSelected;
        public Method MethodSelected
        {
            get => _methodSelected;
            set
            {
                //do not re-order the code below
                _ = SetProperty(ref _methodSelected, value);
                HandleMethodChanged(value.Id);
            }
        }

        private enum PolarFactor
        {
            TotalNumOfItems,
            AngleToFill,
            AngleBetweenItems,
            CenterPointX,
            CenterPointY,
            RotateItemsAsCopied,
            Other
        }

        private PolarFactor PolarCurrentChange { get; set; }

        public ObservableCollection<Method> PolarMethod { get; set; }

        private bool _useDefaultBasePoint = true;
        public bool UseDefaultBasePoint
        {
            get => _useDefaultBasePoint;
            set
            {
                if (value)
                {
                    TxtBasePointX = DefaultBasePoint.X.ToString(CultureInfo.CurrentCulture);
                    TxtBasePointY = DefaultBasePoint.Y.ToString(CultureInfo.CurrentCulture);
                }
                _ = SetProperty(ref _useDefaultBasePoint, value);
                ToggleUseAnotherBasePoint(value);
            }
        }

        /// <summary>
        /// Default base point of selected objects
        /// </summary>
        private Point3d _defaultBasePoint = Point3d.Origin;
        public Point3d DefaultBasePoint
        {
            get => _defaultBasePoint;
            set
            {
                if (UseDefaultBasePoint)
                {
                    TxtBasePointX = value.X.ToString(CultureInfo.CurrentCulture);
                    TxtBasePointY = value.Y.ToString(CultureInfo.CurrentCulture);
                }
                _ = SetProperty(ref _defaultBasePoint, value);
                PolarPreviewUpdate();
            }
        }

        /// <summary>
        /// The secondary base point
        /// </summary>
        private Point3d SecondaryBasePoint { get; set; }

        /// <summary>
        /// coordinate X of base point
        /// </summary>
        private string _txtBasePointX;
        public string TxtBasePointX
        {
            get => _txtBasePointX;
            set
            {
                try
                {
                    var temp = double.Parse(value);
                    SecondaryBasePoint = new Point3d(temp, SecondaryBasePoint.Y, SecondaryBasePoint.Z);
                    _ = SetProperty(ref _txtBasePointX, Converter.DistanceToString(temp));
                    if (!UseDefaultBasePoint)
                    {
                        PolarPreviewUpdate();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex.Message);
                }
            }
        }

        private string _txtBasePointY;
        public string TxtBasePointY
        {
            get => _txtBasePointY;
            set
            {
                try
                {
                    var temp = double.Parse(value);
                    SecondaryBasePoint = new Point3d(SecondaryBasePoint.X, temp, SecondaryBasePoint.Z);
                    _ = SetProperty(ref _txtBasePointY, Converter.DistanceToString(temp));
                    if (!UseDefaultBasePoint)
                    {
                        PolarPreviewUpdate();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex.Message);
                }
            }
        }

        private bool _isMoreInfo;
        public bool IsMoreInfo
        {
            get => _isMoreInfo;
            set => SetProperty(ref _isMoreInfo, value);
        }
        #endregion

        #region PolarInitialize
        private void PolarInitialize()
        {
            //properties initialize - do not fix below code
            PolarArr = new PolarArray();
            TxtBasePointX = "0";
            TxtBasePointY = "0";
            RetrieveDataOfMethod();

            //set default properties
            PolarCurrentChange = PolarFactor.CenterPointX;
            TxtCenterX = DefaultProperties.Polar.CenterPointX.ToString(CultureInfo.CurrentCulture);

            PolarCurrentChange = PolarFactor.CenterPointY;
            TxtCenterY = DefaultProperties.Polar.CenterPointY.ToString(CultureInfo.CurrentCulture);

            PolarCurrentChange = PolarFactor.TotalNumOfItems;
            TxtTotalNumOfItems = DefaultProperties.Polar.TotalNumberOfItems.ToString(CultureInfo.CurrentCulture);

            PolarCurrentChange = PolarFactor.AngleToFill;
            TxtAngleToFill = DefaultProperties.Polar.FillAngle.ToString(CultureInfo.CurrentCulture);

            PolarCurrentChange = PolarFactor.AngleBetweenItems;
            TxtAngleBetweenItems = DefaultProperties.Polar.ItemAngle.ToString(CultureInfo.CurrentCulture);

            PolarCurrentChange = PolarFactor.RotateItemsAsCopied;
            RotateItemsAsCopied = DefaultProperties.Polar.RotateItemsAsCopied;

            PolarCurrentChange = PolarFactor.Other;
            IsMoreInfo = false;
        }
        #endregion

        #region Relay Command Functions
        /// <summary>
        /// command pick angle to fill
        /// </summary>
        public void GetAngleToFill()
        {
            var promptAngleOptions = new PromptAngleOptions($"\n{ArrayClassicRes.UI_P_AngleFill}")
            {
                UseBasePoint = true,
                BasePoint = PolarArr.CenterPoint,
                UseDashedLine = true,
                UseAngleBase = true,
                AllowNone = true,
            };
            var promptDoubleResult = Util.Editor().GetAngle(promptAngleOptions);
            if (promptDoubleResult.Status != PromptStatus.OK)
            {
                return;
            }
            var angle = promptDoubleResult.Value;
            TxtAngleToFill = Converter.RawAngleToString(angle);
        }

        /// <summary>
        /// command pick angle between items
        /// </summary>
        public void GetAngleBetweenItems()
        {
            var promptAngleOptions = new PromptAngleOptions($"\n{ArrayClassicRes.UI_P_AngleItem}")
            {
                UseBasePoint = true,
                BasePoint = PolarArr.CenterPoint,
                UseDashedLine = true,
            };
            var promptDoubleResult = Util.Editor().GetAngle(promptAngleOptions);
            if (promptDoubleResult.Status != PromptStatus.OK)
            {
                return;
            }
            var angle = promptDoubleResult.Value;
            //AngleToString included ANGBASE
            TxtAngleBetweenItems = Converter.AngleToString(angle);
        }

        /// <summary>
        /// command pick center point
        /// </summary>
        public void GetCenterPoint()
        {
            var promptStartPointOptions = new PromptPointOptions($"\n{ArrayClassicRes.UI_P_Center}")
            {
                AllowNone = true,
            };
            var promptCenterPointResult = Util.Editor().GetPoint(promptStartPointOptions);
            if (promptCenterPointResult.Status != PromptStatus.OK)
            {
                return;
            }
            TxtCenterX = promptCenterPointResult.Value.X.ToString(CultureInfo.CurrentCulture);
            TxtCenterY = promptCenterPointResult.Value.Y.ToString(CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// command pick base point
        /// </summary>
        public void GetBasePoint()
        {
            var promptBasePointOptions = new PromptPointOptions($"\n{ArrayClassicRes.UI_BasePoint}")
            {
                AllowNone = true,
            };
            var promptBasePointResult = Util.Editor().GetPoint(promptBasePointOptions);
            if (promptBasePointResult.Status != PromptStatus.OK)
            {
                return;
            }
            TxtBasePointX = promptBasePointResult.Value.X.ToString(CultureInfo.CurrentCulture);
            TxtBasePointY = promptBasePointResult.Value.Y.ToString(CultureInfo.CurrentCulture);
        }

        private bool PolarArrayValidation()
        {
            try
            {
                var maxArray = EnvironmentVariable.GetInt("MaxArray");
                var totalObjectsWillBeCreated = PolarArr.Count * PolarArr.SourceEntities.Count;
                if (totalObjectsWillBeCreated > maxArray)
                {
                    var error = string.Format(ArrayClassicRes.DM_TooMany, maxArray, totalObjectsWillBeCreated, maxArray);
                    MsgBox.Error.Show(error);
                    return false;
                }

                if (PolarArr.Count < 2)
                {
                    var error = ArrayClassicRes.DM_Pola1;
                    MsgBox.Error.Show(error);
                    return false;
                }

                //Case validation: Angle between items can not exceed angle to fill
                if (Math.Abs(PolarArr.ItemAngle).More(Math.Abs(PolarArr.FillAngle)))
                {
                    var error = ArrayClassicRes.DM_PolarArray_AngleBwItemsCantGtAngleToFill;
                    MsgBox.Error.Show(error);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
                return false;
            }
        }

        /// <summary>
        /// command OK to polar array
        /// </summary>
        /// <param name="currentWindow"></param>
        /// <param name="isPreview"></param>
        public void ExecutePolarArray(CustomWindow currentWindow, bool isPreview)
        {
            if (currentWindow == null)
            {
                return;
            }
            try
            {
                var primaryBasePoint = UseDefaultBasePoint ? DefaultBasePoint : SecondaryBasePoint;
                PolarArr.SourceEntities = CurObjectIds;
                PolarArr.IsRotate = RotateItemsAsCopied;
                PolarArr.BasePoint = primaryBasePoint;
                //IJ-CAD: dot not use StartUserInteraction
                currentWindow.Hide();
                var completed = PolarArrayValidation() && PolarArr.Draw(AngDir, isPreview);
                if (completed)
                {
                    //save successfully state
                    DefaultProperties.PolarActive = 1;
                    DefaultProperties.Polar.Collect(
                        PolarArr.CenterPoint.X,
                        PolarArr.CenterPoint.Y,
                        MethodSelected.Id,
                        PolarArr.Count,
                        PolarArr.FillAngle,
                        PolarArr.ItemAngle,
                        PolarArr.IsRotate);

                    currentWindow.Close();
                }
                else
                {
                    currentWindow.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
            }
        }
        #endregion

        #region handle event of UI
        /// <summary>
        /// toggle accessibility of using object's base point group
        /// </summary>
        /// <param name="isActive"></param>
        private void ToggleUseAnotherBasePoint(bool isActive)
        {
            try
            {
                var grdObjectBase = That.FindChildrenByName<Grid>("GrdObjectBase");
                if (grdObjectBase != null)
                {
                    grdObjectBase.IsEnabled = !isActive;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
            }
        }

        /// <summary>
        /// calculate remain values when number of items changed
        /// </summary>
        private void CalculateItemsChanged()
        {
            if (!_windowLoaded)
            {
                return;
            }
            try
            {
                var itemCount = PolarArr.Count;
                var fillAngleBefore = PolarArr.FillAngle;
                var itemAngleBefore = PolarArr.ItemAngle;
                switch (MethodSelected.Id)
                {
                    case (int)MethodValue.TotalNumberOfItemAndAngleToFill:
                        PolarCurrentChange = PolarFactor.AngleBetweenItems;
                        TxtAngleBetweenItems = Math.Abs(fillAngleBefore).IsEqual(Math.PI * 2)
                            ? Math.Abs(fillAngleBefore.Division(itemCount)).ToString(CultureInfo.CurrentCulture)
                            : Math.Abs(fillAngleBefore.Division(itemCount - 1)).ToString(CultureInfo.CurrentCulture);
                        break;
                    case (int)MethodValue.TotalNumberOfItemAndAngleBetweenItems:
                        var direction = fillAngleBefore < 0 ? -1 : 1;
                        if ((itemCount * itemAngleBefore).OrMore(Math.PI * 2))
                        {
                            PolarCurrentChange = PolarFactor.AngleToFill;
                            TxtAngleToFill = (Math.PI * 2 * direction).ToString(CultureInfo.CurrentCulture);
                            PolarCurrentChange = PolarFactor.AngleBetweenItems;
                            TxtAngleBetweenItems = Math.Abs((Math.PI * 2).Division(itemCount)).ToString(CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            PolarCurrentChange = PolarFactor.AngleToFill;
                            TxtAngleToFill = ((itemCount - 1) * itemAngleBefore * direction).ToString(CultureInfo.CurrentCulture);
                        }
                        break;
                }
                PolarCurrentChange = PolarFactor.Other;
                PolarPreviewUpdate();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
            }
        }

        /// <summary>
        /// calculate remain values when fill angle changed
        /// </summary>
        private void CalculateFillAngleChanged()
        {
            if (!_windowLoaded)
            {
                return;
            }
            try
            {
                var fillAngle = PolarArr.FillAngle;
                var itemCountBefore = PolarArr.Count;
                var itemAngleBefore = PolarArr.ItemAngle;
                switch (MethodSelected.Id)
                {
                    case (int)MethodValue.TotalNumberOfItemAndAngleToFill:
                        PolarCurrentChange = PolarFactor.AngleBetweenItems;
                        TxtAngleBetweenItems = Math.Abs(fillAngle).IsEqual(Math.PI * 2)
                            ? Math.Abs(fillAngle.Division(itemCountBefore)).ToString(CultureInfo.CurrentCulture)
                            : Math.Abs(fillAngle.Division(itemCountBefore - 1)).ToString(CultureInfo.CurrentCulture);
                        break;
                    case (int)MethodValue.AngleToFillAndAngleBetweenItems:
                        var absFillAngle = Math.Abs(fillAngle);
                        if (itemAngleBefore.More(absFillAngle))
                        {
                            PolarCurrentChange = PolarFactor.AngleBetweenItems;
                            TxtAngleBetweenItems = absFillAngle.ToString(CultureInfo.CurrentCulture);
                            itemAngleBefore = absFillAngle;
                        }
                        PolarCurrentChange = PolarFactor.TotalNumOfItems;
                        TxtTotalNumOfItems = absFillAngle.IsEqual(Math.PI * 2)
                            ? Math.Abs(absFillAngle.Division(itemAngleBefore).ToFloorValue()).ToString(CultureInfo.CurrentCulture)
                            : Math.Abs(absFillAngle.Division(itemAngleBefore).ToFloorValue() + 1).ToString(CultureInfo.CurrentCulture);
                        break;
                }
                PolarCurrentChange = PolarFactor.Other;
                PolarPreviewUpdate();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
            }
        }

        /// <summary>
        /// calculate remain values when angle between items changed
        /// </summary>
        private void CalculateAngleBetweenItemsChanged()
        {
            if (!_windowLoaded)
            {
                return;
            }
            try
            {
                var itemAngle = PolarArr.ItemAngle;
                var fillAngleBefore = PolarArr.FillAngle;
                var itemCountBefore = PolarArr.Count;
                var direction = fillAngleBefore < 0 ? -1 : 1;
                switch (MethodSelected.Id)
                {
                    case (int)MethodValue.TotalNumberOfItemAndAngleBetweenItems:
                        if ((itemCountBefore * itemAngle).OrMore(Math.PI * 2))
                        {
                            PolarCurrentChange = PolarFactor.AngleToFill;
                            TxtAngleToFill = (Math.PI * 2 * direction).ToString(CultureInfo.CurrentCulture);
                            PolarCurrentChange = PolarFactor.TotalNumOfItems;
                            TxtTotalNumOfItems = Math.Abs((Math.PI * 2).Division(itemAngle).ToFloorValue()).ToString(CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            PolarCurrentChange = PolarFactor.AngleToFill;
                            TxtAngleToFill = ((itemCountBefore - 1) * itemAngle * direction).ToString(CultureInfo.CurrentCulture);
                        }
                        break;
                    case (int)MethodValue.AngleToFillAndAngleBetweenItems:
                        var absFillAngleBefore = Math.Abs(fillAngleBefore);
                        if (itemAngle.More(absFillAngleBefore))
                        {
                            PolarCurrentChange = PolarFactor.AngleToFill;
                            absFillAngleBefore = Math.Abs(itemAngle);
                            TxtAngleToFill = (direction * absFillAngleBefore).ToString(CultureInfo.CurrentCulture);
                        }
                        PolarCurrentChange = PolarFactor.TotalNumOfItems;
                        TxtTotalNumOfItems = absFillAngleBefore.IsEqual(Math.PI * 2)
                            ? Math.Abs(absFillAngleBefore.Division(itemAngle).ToFloorValue()).ToString(CultureInfo.CurrentCulture)
                            : Math.Abs(absFillAngleBefore.Division(itemAngle).ToFloorValue() + 1).ToString(CultureInfo.CurrentCulture);
                        break;
                }
                PolarCurrentChange = PolarFactor.Other;
                PolarPreviewUpdate();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
            }
        }

        /// <summary>
        /// handle event combobox of method changed
        /// </summary>
        /// <param name="id">
        /// is 0: Total number of items & Angle to fill
        /// is 1: Total number of items & Angle between items
        /// is 2: Angle to fill & Angle between items
        /// </param>
        private void HandleMethodChanged(int id)
        {
            try
            {
                var txtTotalNumOfItems = That.FindChildrenByName<NumericUpDown>("TxtTotalNumOfItems");
                var txtAngleToFill = That.FindChildrenByName<NumericAngleControl>("TxtAngleToFill");
                var btnPickAngleToFill = That.FindChildrenByName<Button>("BtnPickAngleToFill");
                var txtAngleBetweenItems = That.FindChildrenByName<NumericAngleControl>("TxtAngleBetweenItems");
                var btnPickAngleBetweenItems = That.FindChildrenByName<Button>("BtnPickAngleBetweenItems");
                txtTotalNumOfItems.IsEnabled = false;
                txtAngleToFill.IsEnabled = false;
                btnPickAngleToFill.IsEnabled = false;
                txtAngleBetweenItems.IsEnabled = false;
                btnPickAngleBetweenItems.IsEnabled = false;
                switch (id)
                {
                    case (int)MethodValue.TotalNumberOfItemAndAngleToFill:
                        txtTotalNumOfItems.IsEnabled = true;
                        txtAngleToFill.IsEnabled = true;
                        btnPickAngleToFill.IsEnabled = true;
                        break;
                    case (int)MethodValue.TotalNumberOfItemAndAngleBetweenItems:
                        txtTotalNumOfItems.IsEnabled = true;
                        txtAngleBetweenItems.IsEnabled = true;
                        btnPickAngleBetweenItems.IsEnabled = true;

                        var angleBetweenItems = PolarArr.ItemAngle;
                        var totalNumOfItemsBefore = PolarArr.Count;

                        PolarCurrentChange = PolarFactor.AngleToFill;
                        var temp = totalNumOfItemsBefore < 2 ? 1 : totalNumOfItemsBefore - 1;
                        TxtAngleToFill = (temp * angleBetweenItems).ToString(CultureInfo.CurrentCulture); //D17 = (D16 - 1) * D19
                        PolarCurrentChange = PolarFactor.Other;
                        break;
                    case (int)MethodValue.AngleToFillAndAngleBetweenItems:
                        txtAngleToFill.IsEnabled = true;
                        btnPickAngleToFill.IsEnabled = true;
                        txtAngleBetweenItems.IsEnabled = true;
                        btnPickAngleBetweenItems.IsEnabled = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
            }
        }

        /// <summary>
        /// handle event button more/less
        /// </summary>
        /// <param name="currentWindow"></param>
        public void ExpandMoreInfo(CustomWindow currentWindow)
        {
            try
            {
                IsMoreInfo = !IsMoreInfo;
                HandleMoreLessBehavior(currentWindow, IsMoreInfo);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
            }
        }

        private void HandleMoreLessBehavior(FrameworkElement currentWindow, bool isMoreInfo)
        {
            var grxMoreInfo = currentWindow.FindChildrenByName<GroupBox>("GrbObjectBase");
            var btnMoreInfo = currentWindow.FindChildrenByName<Button>("BtnMoreInfo");
            var imgMoreLess = currentWindow.FindChildrenByName<Image>("ImgMoreLess");
            if (grxMoreInfo == null || btnMoreInfo == null || imgMoreLess == null)
            {
                return;
            }
            if (!isMoreInfo)
            {
                grxMoreInfo.Visibility = Visibility.Collapsed;
                btnMoreInfo.Content = ArrayClassicRes.Btn_More;
                var moreIcon = new BitmapImage(new Uri("pack://application:,,,/JrxCommand;component/Command/ArrayClassic/View/Resources/icons-more.ico"));
                imgMoreLess.Source = moreIcon;
                currentWindow.Height = Height;
            }
            else
            {
                grxMoreInfo.Visibility = Visibility.Visible;
                btnMoreInfo.Content = ArrayClassicRes.Btn_Less;
                var lessIcon = new BitmapImage(new Uri("pack://application:,,,/JrxCommand;component/Command/ArrayClassic/View/Resources/icons-less.ico"));
                imgMoreLess.Source = lessIcon;
                currentWindow.Height = HeightPlus;
            }
        }

        /// <summary>
        /// assign combobox data
        /// </summary>
        private void RetrieveDataOfMethod()
        {
            try
            {
                PolarMethod = new ObservableCollection<Method>
                {
                    new Method(0, ArrayClassicRes.Lst_MethodFirstOption),
                    new Method(1, ArrayClassicRes.Lst_MethodSecondOption),
                    new Method(2, ArrayClassicRes.Lst_MethodThirdOption)
                };
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
            }
        }
        /// <summary>
        /// struct method of polar array for combobox data
        /// </summary>
        public class Method
        {
            public Method(int id, string messages)
            {
                Id = id;
                Message = messages;
            }
            public int Id { get; set; }
            public string Message { get; set; }
        }
        #endregion

        #region Preview frame
        public void PolarPreviewUpdate()
        {
            try
            {
                if (PolarActive != 1) return;
                var primaryBasePoint = UseDefaultBasePoint ? DefaultBasePoint : SecondaryBasePoint;
                var baseVector = primaryBasePoint - PolarArr.CenterPoint;
                var startAngle = baseVector.GetAngleTo(Vector3d.XAxis, Vector3d.ZAxis);
                //check clockwise
                CanvasHelper.CalcPolarArray(
                    _previewFrame,
                    PolarArr,
                    startAngle,
                    AngDir
                );
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }
        #endregion
    }
}
