﻿#if _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
#elif _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
#endif
using JrxCad.Command.WBlock.Model;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCad.View.CustomWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using static JrxCad.Command.WBlock.Model.WBlockModel;
using static JrxCad.Command.WBlock.WBlockCmd;

namespace JrxCad.Command.WBlock.ViewModel
{
    public class WWBlockViewModel : BindableBase
    {
        #region Declare

        #region Enum
        public struct SaveFormType
        {
            public double Width;
        }
        #endregion

        #region Private properties
        private readonly string _dwgExtension = ".DWG";
        private readonly string _dxfExtension = ".DXF";
        private readonly string _cboPathName = "cmbPath";
        private readonly string _blockReferenceType = "BlockReference";
        private readonly string _hiddenStatus = "Hidden";
        private readonly string _visibleStatus = "Visible";
        private readonly string _quickSelect = "Quick Select - No Object Found";
        private readonly string _dialogFileName = "WblockFileDialog";
#if _AutoCAD_
        private readonly string _wBlockCreateMode = "WBlockCreateMode";
#endif

        protected override string HelpName => "WBlock";
        private readonly WBlockModel _wBlockModel;
        private readonly IUserInputWBlock _userInput;
        #endregion

        #region Public properties
        public bool? IsSavePositionWindow { get; set; }
        public ObjectConversion CreateMode { get; set; }

        private bool _isBlockActive;
        public bool IsBlockActive
        {
            get { return _isBlockActive; }
            set
            {
                SetProperty(ref _isBlockActive, value);
                if (_isBlockActive)
                {
                    VisibilityStatus = _hiddenStatus;
                    PickPointSource = WBlockRes.PickPointDisableSource;
                    SelectObjectsSource = WBlockRes.PickPointDisableSource;
                    QuickSelectSource = WBlockRes.QuickSelectDisableSource;
                }
            }
        }

        private bool _isEnableRadioBlock;
        public bool IsEnableRadioBlock
        {
            get { return _isEnableRadioBlock; }
            set { SetProperty(ref _isEnableRadioBlock, value); }
        }

        private bool _isConvertToBlock;
        public bool IsConvertToBlock
        {
            get { return _isConvertToBlock; }
            set { SetProperty(ref _isConvertToBlock, value); }
        }

        private string _currentNumberObjectIdsString;
        public string CurrentNumberObjectIdsString
        {
            get => _currentNumberObjectIdsString;
            set => SetProperty(ref _currentNumberObjectIdsString, value);
        }

        private ObjectIdCollection _currentObjectIds;
        public ObjectIdCollection CurrentObjectIds
        {
            get => _currentObjectIds;
            set
            {
                SetProperty(ref _currentObjectIds, value);
                var numOfObjects = value == null ? 0 : value.Count;
                NoObjectsSelected = numOfObjects > 0 ? null : WBlockRes.NoObjectsSelectedSource;
                VisibilityStatus = numOfObjects > 0 ? _hiddenStatus : _visibilityStatus;
                switch (numOfObjects)
                {
                    case 0:
                        CurrentNumberObjectIdsString = string.Format(WBlockRes.Lbl_ObjectSelected, "No");
                        break;
                    case 1:
                        _wBlockModel.SourceEntities = value;
                        CurrentNumberObjectIdsString = string.Format(WBlockRes.Lbl_ObjectSelected, numOfObjects);
                        break;
                    default:
                        _wBlockModel.SourceEntities = value;
                        CurrentNumberObjectIdsString = string.Format(WBlockRes.Lbl_ObjectsSelected, numOfObjects);
                        break;
                }
            }
        }

        private bool _isDeleteFromDrawingActive;
        public bool IsDeleteFromDrawingActive
        {
            get { return _isDeleteFromDrawingActive; }
            set { SetProperty(ref _isDeleteFromDrawingActive, value); }
        }

        private bool _isEntireDrawingActive;
        public bool IsEntireDrawingActive
        {
            get { return _isEntireDrawingActive; }
            set
            {
                SetProperty(ref _isEntireDrawingActive, value);
                if (_isEntireDrawingActive)
                {
                    VisibilityStatus = _hiddenStatus;
                    PickPointSource = WBlockRes.PickPointDisableSource;
                    SelectObjectsSource = WBlockRes.PickPointDisableSource;
                    QuickSelectSource = WBlockRes.QuickSelectDisableSource;
                }
            }
        }

        private string _noObjectsSelected;
        public string NoObjectsSelected
        {
            get { return _noObjectsSelected; }
            set { SetProperty(ref _noObjectsSelected, value); }
        }

        private bool _isObjectsActive;
        public bool IsObjectsActive
        {
            get { return _isObjectsActive; }
            set
            {
                SetProperty(ref _isObjectsActive, value);
                if (_isObjectsActive)
                {
                    VisibilityStatus = _visibleStatus;
                    PickPointSource = WBlockRes.PickPointSource;
                    SelectObjectsSource = WBlockRes.PickPointSource;
                    QuickSelectSource = WBlockRes.QuickSelectSource;
                }
            }
        }

        private string _pickPointSource;
        public string PickPointSource
        {
            get { return _pickPointSource; }
            set { SetProperty(ref _pickPointSource, value); }
        }

        private ObservableCollection<string> _destinations;
        public ObservableCollection<string> Destinations
        {
            get { return _destinations; }
            set { SetProperty(ref _destinations, value); }
        }

        private List<string> _insertUnits;
        public List<string> InsertUnits
        {
            get { return _insertUnits; }
            set { SetProperty(ref _insertUnits, value); }
        }

        private ObservableCollection<string> _blockNames;
        public ObservableCollection<string> BlockNames
        {
            get { return _blockNames; }
            set { SetProperty(ref _blockNames, value); }
        }

        private string _quickSelectSource;
        public string QuickSelectSource
        {
            get { return _quickSelectSource; }
            set { SetProperty(ref _quickSelectSource, value); }
        }

        private bool _isRetainActive;
        public bool IsRetainActive
        {
            get { return _isRetainActive; }
            set { SetProperty(ref _isRetainActive, value); }
        }

        private string _selectBlockName;
        public string SelectBlockName
        {
            get { return _selectBlockName; }
            set
            {
                SetProperty(ref _selectBlockName, value);
                HandleAfterSelectBlock();
            }
        }

        private int _selectInsertUnits;
        public int SelectInsertUnits
        {
            get { return _selectInsertUnits; }
            set { SetProperty(ref _selectInsertUnits, value); }
        }

        private string _selectObjectsSource;
        public string SelectObjectsSource
        {
            get { return _selectObjectsSource; }
            set { SetProperty(ref _selectObjectsSource, value); }
        }

        private string _selectPath;
        public string SelectPath
        {
            get { return _selectPath; }
            set { SetProperty(ref _selectPath, value); }
        }

        private string _textPath;
        public string TextPath
        {
            get { return _textPath; }
            set
            {
                SetProperty(ref _textPath, value);
                _wBlockModel.FilePath = value;
            }
        }

        private double _txbX;
        public double TxbX
        {
            get { return _txbX; }
            set { SetProperty(ref _txbX, value); }
        }

        private double _txbY;
        public double TxbY
        {
            get { return _txbY; }
            set { SetProperty(ref _txbY, value); }
        }

        private double _txbZ;
        public double TxbZ
        {
            get { return _txbZ; }
            set { SetProperty(ref _txbZ, value); }
        }

        private string _visibilityStatus;
        public string VisibilityStatus
        {
            get { return _visibilityStatus; }
            set { SetProperty(ref _visibilityStatus, value); }
        }
        #endregion

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public WWBlockViewModel(IUserInputWBlock userInput)
        {
            _userInput = userInput;
            _wBlockModel = new WBlockModel(userInput);
            InsertUnits = UnitsValueToList();
            Initialize();

            //PathDefault
            Destinations = new ObservableCollection<string>();
            InitPathFromRegistry();

            BlockNames = new ObservableCollection<string>();
            GetAllBlock();

            SelectInsertUnits = _wBlockModel.GetUnit();
            if (!IsBlockActive)
            {
                IsObjectsActive = true;
            }
        }

        /// <summary>
        /// Initialize properties for Wblock Dialog
        /// </summary>
        public void Initialize()
        {
            QuickSelectSource = WBlockRes.QuickSelectSource;
            PickPointSource = WBlockRes.PickPointSource;
            SelectObjectsSource = WBlockRes.PickPointSource;

            PreSelectObject();
            Util.Editor().SetImpliedSelection(new ObjectId[0]);
            CreateMode = _userInput.GetValueConfig();
            switch (CreateMode)
            {
                case ObjectConversion.Convert:
                    IsConvertToBlock = true;
                    break;
                case ObjectConversion.Retain:
                    IsRetainActive = true;
                    break;
                case ObjectConversion.Delete:
                    IsDeleteFromDrawingActive = true;
                    break;
                default:
                    IsRetainActive = true;
                    break;
            }
        }

        #region Command Functions

        /// <summary>
        /// select entity in current document
        /// </summary>
        public void SelectEntities()
        {
            using (var tr = Util.StartTransaction())
            {
                CurrentObjectIds = _wBlockModel.SelectEntities(Util.CurDoc(), tr);
                tr.Commit();
            }
        }

        /// <summary>
        /// after showdialog, then loaded UI dialog
        /// </summary>
        public void WindowLoaded(CustomWindow currentWindow)
        {
            var comboBox = FindChildrenHelper.FindChildrenByName<System.Windows.Controls.ComboBox>(currentWindow, _cboPathName);
            if (comboBox != null)
            {
                comboBox.Focus();
            }

            ExecuteUnitTest(currentWindow);
        }
        #endregion

        /// <summary>
        /// Erase object
        /// </summary>
        public void EraseEntities()
        {
            if (IsDeleteFromDrawingActive)
            {
                _wBlockModel.EraseObjects(CurrentObjectIds);
                _wBlockModel.SetRegistryValue(ObjectConversion.Delete);
            }
            else if (IsConvertToBlock)
            {
                var blockName = Path.GetFileNameWithoutExtension(TextPath);
                var blockUnits = SelectInsertUnits == 0 ? UnitsValue.Undefined :
                                 (UnitsValue)System.Enum.Parse(typeof(UnitsValue), InsertUnits[SelectInsertUnits]);
                _wBlockModel.ConvertObjectsToBlock(CurrentObjectIds, blockName, blockUnits);
                _wBlockModel.EraseObjects(CurrentObjectIds);
                _wBlockModel.SetRegistryValue(ObjectConversion.Convert);
            }
            else
            {
                _wBlockModel.SetRegistryValue(ObjectConversion.Retain);
            }
        }

        /// <summary>
        /// Close Dialog
        /// </summary>
        public void CloseWindow()
        {
#if _AutoCAD_
            if (IsRetainActive)
            {
                SystemVariable.SetInt(_wBlockCreateMode, (int)ObjectConversion.Retain);
            }
            else if (IsConvertToBlock)
            {
                SystemVariable.SetInt(_wBlockCreateMode, (int)ObjectConversion.Convert);
            }
            else
            {
                SystemVariable.SetInt(_wBlockCreateMode, (int)ObjectConversion.Delete);
            }
#endif
            if (CurrentObjectIds != null)
            {
                CurrentObjectIds.Dispose();
            }
        }

        /// <summary>
        /// Confirm save block, save entire drawing
        /// </summary>
        private bool ConfirmSaveOption(string textPath)
        {
            if (!File.Exists(textPath))
            {
                return true;
            }
            // file was exist
            var dialogResult = MsgBox.Warning.Show(string.Format(WBlockRes.DM5, TextPath), string.Format(WBlockRes.TitleMessageBox));
            return dialogResult == DialogResult.Yes;
        }

        /// <summary>
        /// Save (Block, Entire Drawing, ObjectId)
        /// </summary>
        public void ExecuteSaveEvent(CustomWindow currentWindow)
        {
            IsSavePositionWindow = null;
            if (CurrentObjectIds == null)
            {
                var dialogResult = MsgBox.Warning.Show(string.Format(WBlockRes.DM4), string.Format(WBlockRes.TitleMessageBox));
                if (dialogResult == DialogResult.Yes)
                {
                    currentWindow.Hide();
                    SelectEntities();
                    currentWindow.ShowDialog();
                }
                else
                {
                    // cancel selected object
                    IsSavePositionWindow = false;
                    currentWindow.Close();
                }
                return;
            }
            if (IsObjectsActive && CurrentObjectIds.Count == 0)
            {
                var dialogResult = MsgBox.Warning.Show(string.Format(WBlockRes.DM4), string.Format(WBlockRes.TitleMessageBox));
                if (dialogResult == DialogResult.Yes)
                {
                    currentWindow.Hide();
                    SelectEntities();
                    currentWindow.ShowDialog();
                }
                return;
            }

            // validate path 
            var valid = _wBlockModel.IsValidPath(TextPath);

            // set block unit
            var blockUnits = SelectInsertUnits == 0 ? UnitsValue.Undefined :
                (UnitsValue)System.Enum.Parse(typeof(UnitsValue), InsertUnits[SelectInsertUnits]);

            switch (valid)
            {
                case ValidPath.OK:
                    if (IsBlockActive)
                    {
                        var isConfirmSaveBlock = ConfirmSaveOption(TextPath);
                        if (isConfirmSaveBlock)
                        {
                            _wBlockModel.SaveBlock(SelectBlockName, blockUnits);
                            IsSavePositionWindow = true; // detect save position after click [OK] button
                            currentWindow.Close();
                        }
                    }
                    else if (IsEntireDrawingActive)
                    {
                        var isConfirmSaveEntireDrawing = ConfirmSaveOption(TextPath);
                        if (isConfirmSaveEntireDrawing)
                        {
                            _wBlockModel.SaveEntireDrawing(ref _currentObjectIds);
                            IsSavePositionWindow = true; // detect save position after click [OK] button
                            currentWindow.Close();
                        }
                    }
                    else if (IsObjectsActive)
                    {
                        var isConfirmSaveObject = ConfirmSaveOption(TextPath);
                        if (isConfirmSaveObject)
                        {
                            _wBlockModel.SaveObjects(true, blockUnits);
                            EraseEntities();
                            Util.Editor().UpdateScreen();
                            IsSavePositionWindow = true; // detect save position after click [OK] button
                            currentWindow.Close();
                        }
                    }
                    break;
                case ValidPath.FileNameAndPathNotSpecified:
                    MsgBox.Error.Show(string.Format(WBlockRes.DM3_1), string.Format(WBlockRes.TitleMessageBox));
                    break;
                case ValidPath.NotValid:
                    MsgBox.Error.Show(string.Format(WBlockRes.DM3_2), string.Format(WBlockRes.TitleMessageBox));
                    break;
                case ValidPath.NotExist:
                    MsgBox.Error.Show(string.Format(WBlockRes.DM3_3, Path.GetDirectoryName(TextPath), string.Format(WBlockRes.TitleMessageBox)));
                    break;
                case ValidPath.Unauthorized:
                    MsgBox.Error.Show(string.Format(WBlockRes.DM3_4, Path.GetDirectoryName(TextPath)), string.Format(WBlockRes.TitleMessageBox));
                    break;
                case ValidPath.TooLong:
                    MsgBox.Error.Show(string.Format(WBlockRes.DM3_5, TextPath.Length), string.Format(WBlockRes.TitleMessageBox));
                    break;
                case ValidPath.ReadOnly:
                    MsgBox.Error.Show(string.Format(WBlockRes.DM3_8, TextPath), string.Format(WBlockRes.TitleMessageBox));
                    break;
                case ValidPath.FileNameOrPathNotSpecified:
                    MsgBox.Error.Show(string.Format(WBlockRes.DM3_9, TextPath), string.Format(WBlockRes.TitleMessageBox));
                    break;
                case ValidPath.InUseByOtherProcesses:
                    MsgBox.Error.Show(string.Format(WBlockRes.DM3_7, TextPath), string.Format(WBlockRes.TitleMessageBox));
                    break;
                case ValidPath.InUseByCAD:
                    MsgBox.Error.Show(string.Format(WBlockRes.DM3_6, TextPath), string.Format(WBlockRes.TitleMessageBox));
                    break;
                default:
                    MsgBox.Error.Show(WBlockRes.UnknowError, string.Format(WBlockRes.TitleMessageBox));
                    break;
            }
            var comboBox = FindChildrenHelper.FindChildrenByName<System.Windows.Controls.ComboBox>(currentWindow, _cboPathName);
            if (comboBox != null)
            {
                comboBox.Focus();
            }
        }

        /// <summary>
        /// Get all block
        /// </summary>
        private void GetAllBlock()
        {
            using (var db = Util.Database())
            using (var tr = Util.StartTransaction())
            {
                // Get Block Table and Block Table Record
                var blockTable = tr.GetObject<BlockTable>(db.BlockTableId, OpenMode.ForRead);
                if (blockTable != null)
                {
                    foreach (var objectId in blockTable)
                    {
                        var blockTableRecord = tr.GetObject<BlockTableRecord>(objectId, OpenMode.ForRead);
                        if (blockTableRecord != null && !blockTableRecord.Name.Substring(0, 1).Equals("*"))
                        {
                            BlockNames.Add(blockTableRecord.Name);
                        }
                    }
                }

                tr.Commit();
                BlockNames = new ObservableCollection<string>(BlockNames.OrderBy(o => o).ToList());
            }
            if (BlockNames.Count > 0)
            {
                IsEnableRadioBlock = true;
            }
        }

        /// <summary>
        /// Get the file history from the registry and set it in the list
        /// </summary>
        private void InitPathFromRegistry()
        {
            var registryKey = new Hkey.CurrentUser();
            var keyPath = registryKey.GetProfileSubKey() + "\\Dialogs\\write_block_dialog";
            var isNewBlockDwg = true;
            var listItemCount = registryKey.GetValue(keyPath, "MRUPathListItemCount");
            if (listItemCount != null)
            {
                if (int.Parse(listItemCount) > 0)
                {
                    var listItemCountValue = int.Parse(listItemCount);
                    for (var i = 0; i < listItemCountValue; i++)
                    {
                        Destinations.Add(registryKey.GetValue(keyPath, "MRUPathListItem" + i));
                    }

                    if (string.IsNullOrEmpty(SelectBlockName))
                    {
                        TextPath = Path.GetDirectoryName(registryKey.GetValue(keyPath, "MRUPathListItem0")) + string.Format(WBlockRes.NewBlockDwg);
                    }
                    else
                    {
                        TextPath = Path.Combine(Path.GetDirectoryName(registryKey.GetValue(keyPath, "MRUPathListItem0")) ?? string.Empty,
                                                SelectBlockName + _dwgExtension.ToLower());
                    }
                    isNewBlockDwg = false;
                }
            }

            if (isNewBlockDwg)
            {
                TextPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + string.Format(WBlockRes.NewBlockDwg);
            }
        }

        /// <summary>
        /// Set file path and insert units
        /// </summary>
        public void HandleAfterSelectBlock()
        {
            if (string.IsNullOrEmpty(SelectBlockName))
            {
                return;
            }
            if (!string.IsNullOrEmpty(TextPath))
            {
                var extension = !string.IsNullOrEmpty(Path.GetExtension(TextPath)) &&
                                (Path.GetExtension(TextPath).ToLower() == _dwgExtension.ToLower() ||
                                 Path.GetExtension(TextPath).ToLower() == _dxfExtension.ToLower()) ?
                                 Path.GetExtension(TextPath) : _dwgExtension.ToLower();
                if (!string.IsNullOrEmpty(Path.GetDirectoryName(TextPath)))
                {
                    TextPath = Path.Combine(Path.GetDirectoryName(TextPath) ?? string.Empty, SelectBlockName + extension);
                }
                else
                {
                    TextPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), SelectBlockName + extension);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(SelectBlockName))
                {
                    TextPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                                SelectBlockName + _dwgExtension.ToLower());
                }
            }

            using (var db = Util.Database())
            using (var tr = Util.StartTransaction())
            {
                var blockTable = tr.GetObject<BlockTable>(db.BlockTableId, OpenMode.ForRead);
                if (blockTable != null)
                {
                    foreach (var objectId in blockTable)
                    {
                        var blockTableRecord = tr.GetObject<BlockTableRecord>(objectId, OpenMode.ForRead);
                        if (blockTableRecord != null && blockTableRecord.Name == _selectBlockName)
                        {
                            if (InsertUnits != null && blockTableRecord.Units.GetHashCode() >= InsertUnits.Count)
                            {
                                SelectInsertUnits = 0;
                            }
                            else
                            {
                                SelectInsertUnits = blockTableRecord.Units.GetHashCode();
                            }
                        }
                    }
                }
                tr.Commit();
            }
        }

        /// <summary>
        /// Prepare Select Objects
        /// </summary>
        private void PreSelectObject()
        {
            using (Util.CurDoc().LockDocument())
            using (var tr = Util.StartTransaction())
            {
                var selectionSetCount = 0;
                var objectOnLockedLayerCount = 0;
                CurrentObjectIds = _wBlockModel.SelectImplied(tr, ref selectionSetCount, ref objectOnLockedLayerCount);
                if (objectOnLockedLayerCount > 0)
                {
                    MsgBox.Info.Show(string.Format(WBlockRes.DM1, objectOnLockedLayerCount));
                }
                if (CurrentObjectIds.Count < 2)
                {
                    foreach (ObjectId item in CurrentObjectIds)
                    {
                        var entity = tr.GetObject<Entity>(item, OpenMode.ForWrite, false, true);
                        if (entity is null)
                        {
                            continue;
                        }
                        if (entity.GetType().Name == _blockReferenceType)
                        {
                            var blockReferenceName = (entity as BlockReference)?.Name;
                            IsEnableRadioBlock = true;
                            IsBlockActive = true;
                            SelectBlockName = blockReferenceName;
                            VisibilityStatus = _hiddenStatus;
                            PickPointSource = WBlockRes.PickPointDisableSource;
                            SelectObjectsSource = WBlockRes.PickPointDisableSource;
                            QuickSelectSource = WBlockRes.QuickSelectDisableSource;
                            break;
                        }
                    }
                }
                tr.Commit();
            }
        }

        /// <summary>
        /// Pick Base Point
        /// </summary>
        public void PickPoint(CustomWindow currentWindow)
        {
            if (currentWindow == null) return;

            var promptBasePointOptions = new PromptPointOptions(string.Format($"\n{WBlockRes.UI_1}"))
            {
                AllowNone = true,
            };

            var promptBasePointResult = Util.Editor().GetPoint(promptBasePointOptions);
            if (promptBasePointResult.Status != PromptStatus.OK) return;

            TxbX = promptBasePointResult.Value.X;
            TxbY = promptBasePointResult.Value.Y;
            TxbZ = promptBasePointResult.Value.Z;
            _wBlockModel.BasePoint = new Point3d(TxbX, TxbY, TxbZ);
        }

        //TODO: QSELECT
        public void QuickSelectObjectsWindow()
        {
            using (var tr = Util.StartTransaction())
            {
                Util.Editor().SelectionAdded += HandleEntLockedLayer;
                var selectionSet = Util.Editor().SelectAll().Value;
                Util.Editor().SelectionAdded -= HandleEntLockedLayer;
                var currentObjectIds = new ObjectIdCollection();
                if (selectionSet == null)
                {
                    MsgBox.Warning.Show(string.Format(WBlockRes.NoObjectsInDrawing), _quickSelect);
                    return;
                }
                foreach (var objId in selectionSet.GetObjectIds())
                {
                    currentObjectIds.Add(objId);
                }
                CurrentObjectIds = currentObjectIds;
                tr.Commit();
            }
        }

        /// <summary>
        /// Handle Locked layer before select all
        /// </summary>
        private void HandleEntLockedLayer(object sender, SelectionAddedEventArgs e)
        {
            using (var db = Util.Database())
            using (var tr = db.TransactionManager.StartOpenCloseTransaction())
            {
                var layerTable = tr.GetObject<LayerTable>(db.LayerTableId, OpenMode.ForRead);
                var ids = e.AddedObjects.GetObjectIds();
                for (var i = 0; i < ids.Length; i++)
                {
                    var ent = tr.GetObject<Entity>(ids[i], OpenMode.ForRead);
                    var ltr = tr.GetObject<LayerTableRecord>(layerTable[ent.Layer], OpenMode.ForRead);
                    if (ltr.IsLocked)
                    {
                        e.Remove(i);
                    }
                }
                tr.Commit();
            }
        }

        /// <summary>
        /// Open Save File Dialog
        /// </summary>
        public void ShowDialogSaveFile()
        {
            using (new SystemVariable.Temp("FILEDIA", 1))
            {
                // get directory
                var directory = _wBlockModel.IsValidPath(TextPath) == ValidPath.OK ?
                    Path.GetDirectoryName(TextPath) : Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                // init PromptSaveFileOptions
                var openFileOpt = new PromptSaveFileOptions(string.Format(WBlockRes.SaveFileDialogTitle))
                {
                    DialogName = _dialogFileName,
                    Filter = WBlockRes.FilterStringSaveFileDialog,
                    InitialDirectory = directory,
                    AllowUrls = false,
                };

                // set default file name.
                if (_wBlockModel.IsValidPath(TextPath) == ValidPath.OK)
                {
                    openFileOpt.InitialFileName = Path.GetFileName(TextPath);
                }

                var promptFileNameResult = Util.Editor().GetFileNameForSave(openFileOpt);
                if (promptFileNameResult.Status != PromptStatus.OK)
                {
                    return;
                }
                Destinations.Add(promptFileNameResult.StringResult);
                SelectPath = Destinations[Destinations.Count - 1];
            }
        }

        /// <summary>
        /// Convert and handle enum UnitsValue to list
        /// </summary>
        private List<string> UnitsValueToList()
        {
            var unitsValue = System.Enum.GetValues(typeof(UnitsValue)).Cast<UnitsValue>().Select(x => UnitIns.GetName(x)).ToList();
            unitsValue[0] = "Unitless";
#if _AutoCAD_
            unitsValue.Remove("US Survey Inch");
            unitsValue.Remove("US Survey Yard");
            unitsValue.Remove("US Survey Mile");
#endif
            return unitsValue;
        }

        #region unit test

        /// <summary>
        /// execute unit test
        /// </summary>
        private void ExecuteUnitTest(CustomWindow window)
        {
            if (_userInput.IsExecuteUnitTest())
            {
                // validate path
                TextPath = _userInput.SetFilePath();

                IsObjectsActive = _userInput.IsObjectActive();

                // execute ok button
                ExecuteSaveEvent(window);
            }
        }
        #endregion
    }
}
