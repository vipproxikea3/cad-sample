﻿using JrxCad.Command.WBlock.ViewModel;
using JrxCad.Utility;
using JrxCad.View.CustomWpf;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using static JrxCad.Command.WBlock.WBlockCmd;

namespace JrxCad.Command.WBlock
{
    /// <summary>
    /// Interaction logic for WpfWBlock.xaml
    /// </summary>
    public partial class WpfWBlock : CustomWindow
    {
        /// <summary>
        /// constructor
        /// </summary>
        public WpfWBlock(IUserInputWBlock userInput)
        {
            InitializeComponent();
            this.DataContext = new WWBlockViewModel(userInput);
        }

        /// <summary>
        /// Restore size and position for window.
        /// </summary>
        public void SetWBlockSize(double windowWidth, double windowHeight)
        {
            Width = windowWidth;
            Height = windowHeight;
        }

        /// <summary>
        /// CommandAction
        /// </summary>
        private void CommandAction(Action<WWBlockViewModel> callback, bool dialogAttack)
        {
            try
            {
                if (!(DataContext is WWBlockViewModel model))
                {
                    return;
                }
                if (dialogAttack)
                {
                    using (Util.Editor().StartUserInteraction(this))
                    {
                        callback(model);
                    }
                }
                else
                {
                    callback(model);
                }
            }
            catch (Exception ex)
            {
                MsgBox.Error.Show(ex.Message);
            }
        }

        #region Common event
        /// <summary>
        /// Execute Write Block
        /// </summary>
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.ExecuteSaveEvent(this), false);
        }

        /// <summary>
        /// Select all objects
        /// </summary>
        private void BtnQuickSelect_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.QuickSelectObjectsWindow(), false);
        }

        /// <summary>
        /// Pick base point
        /// </summary>
        private void BtnPickPoint_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.PickPoint(this), true);
        }

        /// <summary>
        /// Select Objects
        /// </summary>
        private void BtnSelectObjects_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.SelectEntities(), true);
        }

        /// <summary>
        /// Show dialog path
        /// </summary>
        private void BtnPath_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.ShowDialogSaveFile(), true);
        }

        /// <summary>
        /// Close dialog, WBlock session is end
        /// </summary>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }

        /// <summary>
        /// Open help window
        /// </summary>
        private void BtnHelp_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.OpenHelpWindow(), false);
        }

        /// <summary>
        /// Window closing
        /// </summary>
        private void WBlockWindow_Closing(object sender, CancelEventArgs e)
        {
            CommandAction((model) => model.CloseWindow(), false);
        }

        /// <summary>
        /// Window loaded
        /// </summary>
        private void WBlockWindow_Loaded(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.WindowLoaded(this), false);
        }

        /// <summary>
        /// select block name
        /// </summary>
        private void ComboBoxItem_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            CommandAction((model) => model.HandleAfterSelectBlock(), false);
        }
        #endregion
    }
}
