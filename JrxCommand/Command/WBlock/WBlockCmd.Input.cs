#if _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;
#elif _IJCAD_
using GrxCAD.EditorInput;
using GrxCAD.DatabaseServices;
#endif
using JrxCad.Command.MinusWBlock;
using JrxCad.Helpers;
using JrxCad.Utility;
using static JrxCad.Command.WBlock.Model.WBlockModel;

namespace JrxCad.Command.WBlock
{
    public partial class WBlockCmd
    {
        private readonly IUserInputWBlock _userInput;

        /// <summary>
        /// IWindowInput Interface
        /// </summary>
        public interface IUserInputWBlock : IUserInputBase
        {
            int GetActiveCommand();
            int GetPickFirst();
            PromptSelectionResult GetPromptSelectionResult();
            bool EntityOnLockedLayer(Transaction tr, Entity entity);
            bool IsExecuteUnitTest();
            string SetFilePath();
            bool IsObjectActive();
            ObjectConversion GetValueConfig();
            PromptIntegerResult GetInteger(MinusWBlockCmd.IntegerOpts opts);
        }

        /// <summary>
        /// WindowInput
        /// </summary>
        public class UserInputWBlock : UserInputBase, IUserInputWBlock
        {
            public int GetPickFirst()
            {
                return SystemVariable.GetInt("PICKFIRST");
            }

            public PromptSelectionResult GetPromptSelectionResult()
            {
                return Util.Editor().SelectImplied();
            }

            public bool EntityOnLockedLayer(Transaction tr, Entity entity)
            {
                using (var record = tr.GetObject<LayerTableRecord>(entity.LayerId, OpenMode.ForRead))
                {
                    if (record is null)
                    {
                        return false;
                    }
                    return record.IsLocked;
                }
            }

            public int GetActiveCommand()
            {
                return SystemVariable.GetInt("CMDACTIVE");
            }

            public bool IsExecuteUnitTest()
            {
                return false;
            }

            public string SetFilePath()
            {
                return string.Empty;
            }

            public bool IsObjectActive()
            {
                return false;
            }

            public ObjectConversion GetValueConfig()
            {
#if _AutoCAD_
                var createMode = (ObjectConversion)SystemVariable.GetInt("WBlockCreateMode");
#elif _IJCAD_
                var createMode = GetRegistryValue();
#endif
                return createMode;
            }

            #region common method
            /// <summary>
            /// Get object conversion mode
            /// </summary>
            public ObjectConversion GetRegistryValue()
            {
                var registryKey = new Hkey.CurrentUser();
                var subKey = registryKey.GetProfileSubKey() + "\\Dialogs\\write_block_dialog\\Values";

                var retainType = registryKey.GetValue(subKey, "Button#1213_Checked");
                var convertType = registryKey.GetValue(subKey, "Button#1214_Checked");

                if (retainType != null && retainType.Equals("True"))
                {
                    return ObjectConversion.Retain;
                }

                return convertType != null && convertType.Equals("True") ? ObjectConversion.Convert : ObjectConversion.Delete;
            }

            public PromptIntegerResult GetInteger(MinusWBlockCmd.IntegerOpts opts)
            {
                Util.Editor().PromptedForInteger += opts.GetKeyWord;
                var promptIntegerResult = Util.Editor().GetInteger(opts.Options);
                Util.Editor().PromptedForInteger -= opts.GetKeyWord;
                return opts.IntegerResult;
            }
            #endregion
        }
    }
}
