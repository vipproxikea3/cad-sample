﻿#if _AutoCAD_
using Autodesk.AutoCAD.Runtime;
#elif _IJCAD_
using GrxCAD.Runtime;
#endif
using JrxCad.Command.MinusWBlock;
using JrxCad.Command.WBlock.ViewModel;
using JrxCad.Enum;
using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.Command.WBlock
{
    public partial class WBlockCmd : BaseCommand
    {
        private double _wBlockWidth, _wBlockHeight;

        public WBlockCmd() : this(new UserInputWBlock())
        {
        }

        public WBlockCmd(IUserInputWBlock userInput)
        {
            _userInput = userInput;
        }

#if DEBUG
#if _AutoCAD_
        [CommandMethod("JrxCommand", "W1", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#elif _IJCAD_
        [CommandMethod("JrxCommand", "W1", CommandFlags.Session | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
#else
        [CommandMethod("JrxCommand", "SmxWBLOCK", CommandFlags.Modal | CommandFlags.UsePickSet | CommandFlags.Redraw | CommandFlags.NoBlockEditor)]
#endif
        public override void OnCommand()
        {
            try
            {
                var cmdActive = _userInput.GetActiveCommand();
                if (cmdActive == (int)ActiveCommand.Script || cmdActive == (int)ActiveCommand.AutoLISP)
                {
                    var minusWBlock = new MinusWBlockCmd(_userInput);
                    minusWBlock.ExecuteCommand();
                }
                else
                {
                    // open WBlock dialog box
                    var form = new WpfWBlock(_userInput);

                    // set size for WBlock dialog
                    if (_wBlockWidth > 0 && _wBlockHeight > 0)
                    {
                        form.SetWBlockSize(_wBlockWidth, _wBlockHeight);
                    }
                    form.ShowDialog();
                    if (form.DataContext is WWBlockViewModel model && model.IsSavePositionWindow == true)
                    {
                        // storage size and position dialog
                        _wBlockWidth = form.Width;
                        _wBlockHeight = form.Height;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }
    }
}
