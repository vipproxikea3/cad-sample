﻿#if _AutoCAD_
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
#elif _IJCAD_
using CADApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.ApplicationServices;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
#endif
using JrxCad.Command.MinusWBlock;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCad.View.CustomWpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using static JrxCad.Command.WBlock.WBlockCmd;

namespace JrxCad.Command.WBlock.Model
{
    public class WBlockModel : IDisposable
    {
        private readonly string _dwgExtension = ".DWG";
        private readonly string _dxfExtension = ".DXF";
        private readonly string _modelSpaceName = "*MODEL_SPACE";
        private readonly string _paperSpaceName = "*PAPER_SPACE";
        private readonly IUserInputWBlock _userInput;

        #region Enum
        public enum ValidPath
        {
            OK = 0,
            FileNameAndPathNotSpecified = 1,
            NotValid = 2,
            NotExist = 3,
            Unauthorized = 4,
            TooLong = 5,
            IsOpened = 6,
            CanNotAccess = 7,
            ReadOnly = 8,
            FileNameOrPathNotSpecified = 9,
            InUseByOtherProcesses = 10,
            InUseByCAD = 11,
        }
        public enum ProcessFlag
        {
            CancelOrDefault = 0,
            Finished = 1
        }

        public enum ObjectConversion
        {
            Convert = 0,
            Retain = 1,
            Delete = 2,
        }

        public enum ThumbSize
        {
            x64 = 0,
            x128 = 1,
            x256 = 2,
            x512 = 3,
            x1024 = 4,
            x1440 = 5,
            x1600 = 6,
            x1920 = 7,
            x2560 = 8,
        }
        #endregion

        /// <summary>
        /// Base Point
        /// </summary>
        public Point3d BasePoint { get; set; }

        /// <summary>
        /// File name and path
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// selected objects
        /// </summary>
        public ObjectIdCollection SourceEntities { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public WBlockModel(IUserInputWBlock userInput)
        {
            _userInput = userInput;
        }

        /// <summary>
        /// Append extension file name
        /// </summary>
        public string AppendExtension(string fileName)
        {
            if (!string.IsNullOrEmpty(fileName) &&
                Path.GetExtension(fileName).ToUpper() != _dwgExtension &&
                Path.GetExtension(fileName).ToUpper() != _dxfExtension)
            {
                return $"{fileName}.dwg";
            }

            return fileName;
        }

        /// <summary>
        /// Convert Objects to Block
        /// </summary>
        public void ConvertObjectsToBlock(ObjectIdCollection objectIdCollection, string blockName,
                                          UnitsValue unitsValue = UnitsValue.Inches)
        {
            using (var db = Util.Database())
            using (var tr = Util.StartTransaction())
            {
                // Create our new block table record
                var blockTable = tr.GetObject<BlockTable>(db.BlockTableId, OpenMode.ForRead);
                ObjectId blockId;
                // check BlockTableRecord is exist or not.
                BlockTableRecord blockTableRecord;
                if (blockTable.Has(blockName))
                {
                    blockId = blockTable[blockName];
                    blockTableRecord = tr.GetObject<BlockTableRecord>(blockId, OpenMode.ForRead);

                    blockTableRecord.UpgradeOpen();
                    CopyEntityToBlock(objectIdCollection, tr, ref blockTableRecord);
                    tr.Commit();
                    return;
                }
                blockTableRecord = new BlockTableRecord() { Name = blockName };
                // open BlockTable to write file.
                blockTable.UpgradeOpen();

                blockId = blockTable.Add(blockTableRecord);
                tr.AddNewlyCreatedDBObject(blockTableRecord, true);

                CopyEntityToBlock(objectIdCollection, tr, ref blockTableRecord);

                // add block table record to current space
                var blockRef = new BlockReference(Point3d.Origin, blockId)
                {
                    BlockUnit = unitsValue
                };
                var scale = blockRef.UnitFactor;
                blockRef.ScaleFactors = new Scale3d(scale, scale, scale);

                var blockModelSpace = tr.GetObject<BlockTableRecord>(blockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite);
                blockModelSpace.AppendEntity(blockRef);
                tr.AddNewlyCreatedDBObject(blockRef, true);
                // commit transaction
                tr.Commit();
            }
        }

        /// <summary>
        /// Copy object to block
        /// </summary>
        public void CopyEntityToBlock(ObjectIdCollection collection, Transaction tr, ref BlockTableRecord blockTableRecord)
        {
            foreach (ObjectId id in collection)
            {
                var ent = tr.GetObject<Entity>(id, OpenMode.ForRead);
                // create a object copy
                var entityCopy = ent.Clone() as Entity;

                // append new entity to BlockTableRecord
                blockTableRecord.AppendEntity(entityCopy);
                tr.AddNewlyCreatedDBObject(entityCopy, true);
            }
        }

        /// <summary>
        /// Object unmanaged disposed
        /// </summary>
        public void Dispose()
        {
            if (SourceEntities != null)
            {
                SourceEntities.Dispose();
            }
        }

        /// <summary>
        /// Delete Objects before OK
        /// </summary>
        public void EraseObjects(ObjectIdCollection objectIdCollection)
        {
            using (var tr = Util.StartTransaction())
            {
                for (var i = 0; i < objectIdCollection.Count; i++)
                {
                    var ent = tr.GetObject<Entity>(objectIdCollection[i], OpenMode.ForWrite, false, true);
                    if (!ent.IsAProxy)
                    {
                        if (!ent.IsErased)
                        {
                            ent.Erase();
                        }
                    }
                }
                tr.Commit();
            }
        }

        /// <summary>
        /// Get file opened in AutoCAD
        /// </summary>
        public bool GetDocNames(string path)
        {
            var docs = CADApp.DocumentManager;
            foreach (Document doc in docs)
            {
                if (doc.Name == path)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Get thumbnail size from system variable
        /// </summary>
        public int GetThumbnailSize()
        {
            var thumbSize = (ThumbSize)SystemVariable.GetInt("THUMBSIZE");
            switch (thumbSize)
            {
                case ThumbSize.x64:
                    return 64;
                case ThumbSize.x128:
                    return 128;
                case ThumbSize.x256:
                    return 256;
                case ThumbSize.x512:
                    return 512;
                case ThumbSize.x1024:
                    return 1024;
                case ThumbSize.x1440:
                    return 1440;
                case ThumbSize.x1600:
                    return 1600;
                case ThumbSize.x1920:
                    return 1920;
                case ThumbSize.x2560:
                    return 2560;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Get INSUNIT
        /// </summary>
        public int GetUnit()
        {
            return SystemVariable.GetInt("INSUNITS");
        }

        /// <summary>
        /// Check authorized file
        /// </summary>
        public bool HasFolderWritePermission(string folder)
        {
            try
            {
                using (File.Create(Path.Combine(folder, Path.GetRandomFileName()), 1, FileOptions.DeleteOnClose))
                { }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Validate path
        /// </summary>
        public ValidPath IsValidPath(string path, bool isMinus = false)
        {
            // DM-1 file name and path is not specified
            if (string.IsNullOrEmpty(path.Trim()))
            {
                return ValidPath.FileNameAndPathNotSpecified;
            }

            // DM-2 the specified path is not valid
            if (isMinus)
            {
                if (path.Length < 3)
                {
                    if (path.StartsWith("."))
                    {
                        return ValidPath.NotValid;
                    }
                }
                else
                {
                    var strTheseAreInvalidFileNameChars = new string(Path.GetInvalidPathChars());
                    strTheseAreInvalidFileNameChars += @":/?*" + "\"";
                    var containsABadCharacter = new Regex("[" + Regex.Escape(strTheseAreInvalidFileNameChars) + "]");
                    if (containsABadCharacter.IsMatch(path.Substring(3, path.Length - 3)) ||
                        path.StartsWith("."))
                    {
                        return ValidPath.NotValid;
                    }
                }
            }
            else
            {
                if (path.Length < 3)
                {
                    return ValidPath.NotValid;
                }
                var strTheseAreInvalidFileNameChars = new string(Path.GetInvalidPathChars());
                strTheseAreInvalidFileNameChars += @":/?*" + "\"";
                var containsABadCharacter = new Regex("[" + Regex.Escape(strTheseAreInvalidFileNameChars) + "]");
                if (containsABadCharacter.IsMatch(path.Substring(3, path.Length - 3)) ||
                    path.StartsWith("."))
                {
                    return ValidPath.NotValid;
                }
            }

            // DM-5 the specified file is long
            if (path.Length > 259)
            {
                return ValidPath.TooLong;
            }

            // DM-3 the specified directory does not exist
            // condition 1 => check input didn't directory
            // condition 2 => check input directory is not exist in disk
            var tempPath = path.EndsWith(@"\") || !string.IsNullOrEmpty(Path.GetExtension(path)) ? path : path + "\\";
            if (!string.IsNullOrEmpty(Path.GetDirectoryName(path)) && !Directory.Exists(Path.GetDirectoryName(tempPath)))
            {
                // DM-9 the file name or full path is not specified.
                if ((string.IsNullOrEmpty(Path.GetPathRoot(path)) ||
                     string.IsNullOrEmpty(Path.GetFileName(path)) ||
                     (!string.IsNullOrEmpty(Path.GetFileName(path)) && string.IsNullOrEmpty(Path.GetExtension(path)))) && !isMinus)
                {
                    return ValidPath.FileNameOrPathNotSpecified;
                }
                return ValidPath.NotExist;
            }

            // DM-9 the file name or full path is not specified.
            if ((string.IsNullOrEmpty(Path.GetPathRoot(path)) ||
                string.IsNullOrEmpty(Path.GetFileName(path)) ||
                (!string.IsNullOrEmpty(Path.GetFileName(path)) && string.IsNullOrEmpty(Path.GetExtension(path)))) && !isMinus)
            {
                return ValidPath.FileNameOrPathNotSpecified;
            }

            // DM-4 Unauthorized
            if (!HasFolderWritePermission(Path.GetDirectoryName(path)))
            {
                return ValidPath.Unauthorized;
            }

            if (File.Exists(path))
            {
                FileStream fs = null;
                try
                {
                    fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.None);
                }
                catch (IOException)
                {
                    // DM-6 the specified file is opened
                    // DM-7 the proces can't acces file
                    return GetDocNames(path) ? ValidPath.InUseByCAD : ValidPath.InUseByOtherProcesses;
                }
                finally
                {
                    fs?.Close();
                }

                var fi = new FileInfo(path);
                // DM-8 the specified file is read-only
                if (fi.IsReadOnly)
                {
                    return ValidPath.ReadOnly;
                }
            }

            return ValidPath.OK;
        }

        /// <summary>
        /// Save with Objects mode
        /// </summary>
        public void SaveObjects(bool thumbnail = true, UnitsValue unitsValue = UnitsValue.Inches, int precision = 16,
            bool isBinary = false)
        {
            using (var db = Util.Database())
            {
                // Adding extension file path if wrong
                FilePath = AppendExtension(FilePath);
                // Save the active drawing
                db.SaveAs(FilePath, DwgVersion.Current);

                var newDb = new Database(false, false);
                newDb.ReadDwgFile(FilePath, FileShare.ReadWrite, false, null);
                using (var tr = newDb.TransactionManager.StartTransaction())
                {
                    var blockTable = tr.GetObject<BlockTable>(newDb.BlockTableId, OpenMode.ForRead);
                    var modelSpace = tr.GetObject<BlockTableRecord>(blockTable[BlockTableRecord.ModelSpace], OpenMode.ForRead);
                    var paperSpace = tr.GetObject<BlockTableRecord>(blockTable[BlockTableRecord.PaperSpace], OpenMode.ForRead);

                    var purgeIds = new ObjectIdCollection();
                    // erase BlockTable
                    foreach (var blockId in blockTable)
                    {
                        var blockTableRecord = tr.GetObject<BlockTableRecord>(blockId, OpenMode.ForWrite, false, true);
                        if (blockTableRecord != null)
                        {
                            EraseAllObject(blockTableRecord, tr, ref purgeIds);
                            if (blockTableRecord.Name.ToUpper() == _modelSpaceName || blockTableRecord.Name.ToUpper() == _paperSpaceName) continue;
                            if (!blockTableRecord.IsErased)
                            {
                                blockTableRecord.Erase();
                                blockTableRecord.Dispose();
                            }
                        }
                    }

                    // Erase MODEL_SPACE (BlockTableRecord)
                    EraseAllObject(modelSpace, tr, ref purgeIds);
                    // Erase PAPER_SPACE (BlockTableRecord)
                    EraseAllObject(paperSpace, tr, ref purgeIds);

                    var layoutIds = new ObjectIdCollection();
                    // remove layout
                    var layoutDict = tr.GetObject<DBDictionary>(db.LayoutDictionaryId, OpenMode.ForWrite, false, true);
                    foreach (DBDictionaryEntry entry in layoutDict)
                    {
                        layoutIds.Add(entry.Value);
                    }

                    foreach (ObjectId layoutId in layoutIds)
                    {
                        var layout = tr.GetObject<Layout>(layoutId, OpenMode.ForWrite, false, true);
                        if (layout != null && !layout.IsErased)
                        {
                            layout.Erase();
                        }
                    }

                    tr.Commit();
                    newDb.Purge(purgeIds);
                    //save data
                    switch (Path.GetExtension(FilePath).ToUpper())
                    {
                        case ".DXF":
                            if (!isBinary)
                            {
                                newDb.DxfOut(FilePath, precision, DwgVersion.Current);
                            }
                            else
                            {
                                newDb.DxfOut(FilePath, -1, DwgVersion.Current);
                            }
                            break;
                        default:
                            newDb.SaveAs(FilePath, DwgVersion.Current);
                            break;
                    }
                }
                // Start a transaction in the new database
                using (var acTr = newDb.TransactionManager.StartTransaction())
                {
                    var blockTable = acTr.GetObject<BlockTable>(newDb.BlockTableId, OpenMode.ForRead);

                    // Clone the objects to the new database
                    var acIdMap = new IdMapping();
                    newDb.WblockCloneObjects(SourceEntities, blockTable[BlockTableRecord.ModelSpace], acIdMap, DuplicateRecordCloning.Ignore, false);
                    acTr.Commit();
                }

                newDb.Purge(SourceEntities);
#if _AutoCAD_
                //TODO: Check API Database.EraseEmptyObjects IJCAD
                newDb.EraseEmptyObjects((int)EraseFlags.ZeroLengthCurve);
                newDb.EraseEmptyObjects((int)EraseFlags.EmptyText);
#endif
                newDb.Insunits = unitsValue;
                newDb.Insbase = BasePoint;

                if (thumbnail)
                {
                    using (var tr = Util.StartTransaction())
                    using (var entityEx = new EntityEx())
                    {
                        foreach (ObjectId id in SourceEntities)
                        {
                            entityEx.DbObjects.Add(tr.GetObject<Entity>(id, OpenMode.ForRead));
                        }
#if _AutoCAD_
                        if (SystemVariable.GetBool("ThumbSave"))
                        {
                            var thumbSize = GetThumbnailSize();
                            var image = new DwgImage()
                            {
                                ImageWidth = thumbSize,
                                ImageHeight = thumbSize,
                            };
                            newDb.ThumbnailBitmap = image.Create(entityEx);
                        }
#elif _IJCAD_
                        var thumbSize = GetThumbnailSize();
                        var image = new DwgImage()
                        {
                            ImageWidth = thumbSize,
                            ImageHeight = thumbSize,
                        };
                        newDb.ThumbnailBitmap = image.Create(entityEx);
#endif
                        tr.Commit();
                    }
                }

                switch (Path.GetExtension(FilePath))
                {
                    case ".dxf":
#if _AutoCAD_
                        newDb.DxfOut(FilePath, precision, DwgVersion.Current);
#elif _IJCAD_
                        newDb.DxfOut(FilePath, precision, true);
#endif

                        break;
                    default:
#if _AutoCAD_
                        newDb.SaveAs(FilePath, DwgVersion.Current);
#elif _IJCAD_
                        newDb.SaveAs(FilePath, DwgVersion.Current, true);
#endif
                        break;
                }
                SetPathToRegistry(FilePath);
            }
        }

        /// <summary>
        /// Save with Entire Drawing mode
        /// </summary>
        public void SaveEntireDrawing(ref ObjectIdCollection sourceEntities, int precision = 16, bool isBinary = false)
        {
            using (var db = Util.Database())
            using (var tr = Util.StartTransaction())
            using (var entityEx = new EntityEx())
            {
                // Adding extension file path if wrong
                FilePath = AppendExtension(FilePath);
                var selectionSet = Util.Editor().SelectAll().Value;
                if (selectionSet != null)
                {
                    foreach (var objectId in selectionSet.GetObjectIds())
                    {
                        entityEx.DbObjects.Add(tr.GetObject<Entity>(objectId, OpenMode.ForRead));
                        sourceEntities.Add(objectId);
                    }
                }

                using (var blockTableRecord = tr.GetObject<BlockTableRecord>(Util.Database().CurrentSpaceId, OpenMode.ForRead))
                {
#if _AutoCAD_
                    if (SystemVariable.GetBool("ThumbSave"))
                    {
                        var thumbSize = GetThumbnailSize();
                        var image = new BlockImage()
                        {
                            ImageWidth = thumbSize,
                            ImageHeight = thumbSize
                        };
                        db.ThumbnailBitmap = image.Create(blockTableRecord, true);
                    }
#elif _IJCAD_
                    var thumbSize = GetThumbnailSize();
                    var image = new BlockImage()
                    {
                        ImageWidth = thumbSize,
                        ImageHeight = thumbSize
                    };
                    db.ThumbnailBitmap = image.Create(blockTableRecord, false);
#endif
                }

                switch (Path.GetExtension(FilePath).ToUpper())
                {
                    case ".DXF":
                        if (!isBinary)
                            db.DxfOut(FilePath, precision, true);
                        else
                            db.DxfOut(FilePath, -1, true);
                        break;
                    default:

#if _AutoCAD_
                        db.SaveAs(FilePath, DwgVersion.Current);
#elif _IJCAD_
                    db.SaveAs(FilePath, DwgVersion.Current, true);
#endif
                        break;
                }
                tr.Commit();
            }
            SetPathToRegistry(FilePath);
        }

        /// <summary>
        /// Erase BlockTable Record (ObjectId)
        /// </summary>
        private void EraseAllObject(BlockTableRecord blockTableRecord, Transaction tr, ref ObjectIdCollection purgeIds)
        {
            if (blockTableRecord != null)
            {
                var iter = blockTableRecord.GetEnumerator();
                while (iter.MoveNext())
                {
                    var objectId = iter.Current;
                    var entity = tr.GetObject<Entity>(objectId, OpenMode.ForWrite, false, true);
                    if (entity is BlockReference)
                    {
                        // get BlockReference
                        var blockRef = tr.GetObject<BlockReference>(entity.ObjectId, OpenMode.ForWrite, false, true);
                        // get BlockTableRecord of BlockReference
                        var blockRec = tr.GetObject<BlockTableRecord>(blockRef.BlockTableRecord, OpenMode.ForWrite, false, true);
                        EraseAllObject(blockRec, tr, ref purgeIds);

                        // erase block reference
                        if (blockRec != null && !blockRec.IsAProxy)
                        {
                            blockRec.Erase();
                        }
                        if (!blockRef.IsAProxy)
                        {
                            blockRef.Erase();
                        }
                    }
                    else
                    {
                        if (entity.IsAProxy) continue;
                        // erase other entity
                        purgeIds.Add(entity.ObjectId);
                        entity.Erase();
                        entity.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// Save with Block mode
        /// </summary>
        public bool SaveBlock(string blockName, UnitsValue unitsValue = UnitsValue.Inches, int precision = 16, bool isBinary = false)
        {
            using (var db = Util.Database())
            {
                // Adding extension file path if wrong
                FilePath = AppendExtension(FilePath);
                // Save the active drawing
                db.SaveAs(FilePath, DwgVersion.Current);

                var newDb = new Database(false, false);
                newDb.ReadDwgFile(FilePath, FileShare.ReadWrite, false, null);
                using (var tr = newDb.TransactionManager.StartTransaction())
                {
                    var blockTable = tr.GetObject<BlockTable>(newDb.BlockTableId, OpenMode.ForRead);
                    var modelSpace = tr.GetObject<BlockTableRecord>(blockTable[BlockTableRecord.ModelSpace], OpenMode.ForRead);
                    var paperSpace = tr.GetObject<BlockTableRecord>(blockTable[BlockTableRecord.PaperSpace], OpenMode.ForRead);
                    var purgeIds = new ObjectIdCollection();

                    // erase BlockTable
                    foreach (var blockId in blockTable)
                    {
                        var blockTableRecord = tr.GetObject<BlockTableRecord>(blockId, OpenMode.ForWrite, false, true);
                        if (blockTableRecord != null)
                        {
                            EraseAllObject(blockTableRecord, tr, ref purgeIds);
                            if (blockTableRecord.Name.ToUpper() == "*MODEL_SPACE" || blockTableRecord.Name.ToUpper() == "*PAPER_SPACE") continue;
                            if (blockTableRecord.IsErased || blockTableRecord.IsAProxy) continue;
                            blockTableRecord.Erase();
                            blockTableRecord.Dispose();
                        }
                    }

                    // Erase MODEL_SPACE (BlockTableRecord)
                    EraseAllObject(modelSpace, tr, ref purgeIds);
                    // Erase PAPER_SPACE (BlockTableRecord)
                    EraseAllObject(paperSpace, tr, ref purgeIds);

                    var layoutIds = new ObjectIdCollection();
                    // remove layout
                    var layoutDict = tr.GetObject<DBDictionary>(db.LayoutDictionaryId, OpenMode.ForWrite, false, true);
                    foreach (DBDictionaryEntry entry in layoutDict)
                    {
                        layoutIds.Add(entry.Value);
                    }

                    foreach (ObjectId layoutId in layoutIds)
                    {
                        var layout = tr.GetObject<Layout>(layoutId, OpenMode.ForWrite, false, true);
                        if (layout != null && !layout.IsErased && !layout.IsAProxy)
                        {
                            layout.Erase();
                        }
                    }

                    tr.Commit();
                    newDb.Purge(purgeIds);

                    //save data
                    switch (Path.GetExtension(FilePath).ToUpper())
                    {
                        case ".DXF":
                            if (!isBinary)
                                newDb.DxfOut(FilePath, precision, DwgVersion.Current);
                            else
                                newDb.DxfOut(FilePath, -1, DwgVersion.Current);
                            break;
                        default:
                            newDb.SaveAs(FilePath, DwgVersion.Current);
                            break;
                    }
                }

                SourceEntities = new ObjectIdCollection();
                using (var tr = Util.StartTransaction())
                {
                    var blockTable = tr.GetObject<BlockTable>(db.BlockTableId, OpenMode.ForWrite);

                    BlockTableRecord blockTableRecord = null;
                    foreach (ObjectId objectId in blockTable)
                    {
                        var entity = tr.GetObject<BlockTableRecord>(objectId, OpenMode.ForRead);
                        if (entity.Name == blockName)
                        {
                            foreach (ObjectId objId in entity)
                            {
                                SourceEntities.Add(objId);
                            }
                            blockTableRecord = entity;
                        }
                    }

                    if (SourceEntities.Count == 0) return false;
                    using (var acTr = newDb.TransactionManager.StartTransaction())
                    {
                        var bt = acTr.GetObject<BlockTable>(newDb.BlockTableId, OpenMode.ForRead);

                        // Clone the objects to the new database
                        var acIdMap = new IdMapping();
                        newDb.WblockCloneObjects(SourceEntities, bt[BlockTableRecord.ModelSpace], acIdMap, DuplicateRecordCloning.Ignore, false);
                        acTr.Commit();
                    }

                    newDb.Purge(SourceEntities);

#if _AutoCAD_
                    //TODO: Check API Database.EraseEmptyObjects IJCAD
                    //newDb.EraseEmptyObjects((int)EraseFlags.ZeroLengthCurve);
                    //newDb.EraseEmptyObjects((int)EraseFlags.EmptyText);
#endif
                    newDb.Insunits = unitsValue;
#if _AutoCAD_
                    if (SystemVariable.GetBool("ThumbSave"))
                    {
                        var thumbSize = GetThumbnailSize();
                        var image = new BlockImage
                        {
                            ImageWidth = thumbSize,
                            ImageHeight = thumbSize,
                        };
                        newDb.ThumbnailBitmap = image.Create(blockTableRecord, true);
                    }
#elif _IJCAD_
                    var thumbSize = GetThumbnailSize();
                    var image = new BlockImage
                    {
                        ImageWidth = thumbSize,
                        ImageHeight = thumbSize,
                    };
                    //TODO Fix difference thumbnail block titleblock in WBLOCK01.dwg
                    newDb.ThumbnailBitmap = image.Create(blockTableRecord, true);
#endif

                    switch (Path.GetExtension(FilePath)?.ToUpper())
                    {
                        case ".DXF":
                            if (!isBinary)
                                newDb.DxfOut(FilePath, precision, true);
                            else
                                newDb.DxfOut(FilePath, -1, true);
                            break;
                        default:
#if _AutoCAD_
                            newDb.SaveAs(FilePath, DwgVersion.Current);
#elif _IJCAD_
                            newDb.SaveAs(FilePath, DwgVersion.Current, true);
#endif
                            break;
                    }
                    SetPathToRegistry(FilePath);
                    tr.Commit();
                    return true;
                }
            }
        }

        /// <summary>
        /// Set object conversion mode to Registry
        /// </summary>
        public void SetRegistryValue(ObjectConversion objConversion)
        {
            var registryKey = new Hkey.CurrentUser();
            var subKey = registryKey.GetProfileSubKey() + "\\Dialogs\\write_block_dialog\\Values";

            switch (objConversion)
            {
                case ObjectConversion.Retain:
                    registryKey.SetValue(subKey, "Button#1213_Checked", "True", RegistryValueKind.String);
                    registryKey.SetValue(subKey, "Button#1214_Checked", "False", RegistryValueKind.String);
                    registryKey.SetValue(subKey, "Button#1215_Checked", "False", RegistryValueKind.String);
                    break;
                case ObjectConversion.Convert:
                    registryKey.SetValue(subKey, "Button#1213_Checked", "False", RegistryValueKind.String);
                    registryKey.SetValue(subKey, "Button#1214_Checked", "True", RegistryValueKind.String);
                    registryKey.SetValue(subKey, "Button#1215_Checked", "False", RegistryValueKind.String);
                    break;
                case ObjectConversion.Delete:
                    registryKey.SetValue(subKey, "Button#1213_Checked", "False", RegistryValueKind.String);
                    registryKey.SetValue(subKey, "Button#1214_Checked", "False", RegistryValueKind.String);
                    registryKey.SetValue(subKey, "Button#1215_Checked", "True", RegistryValueKind.String);
                    break;
            }
        }

        /// <summary>
        /// Set history to registry
        /// </summary>
        private void SetPathToRegistry(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return;
            }
            var registryKey = new Hkey.CurrentUser();
            var subKey = registryKey.GetProfileSubKey() + "\\Dialogs\\write_block_dialog";

            var listItemCount = registryKey.GetValue(subKey, "MRUPathListItemCount");
            if (listItemCount != null)
            {
                var listItemCountInt = int.Parse(listItemCount);
                if (listItemCountInt <= 0)
                {
                    return;
                }
                registryKey.SetValue(subKey, "MRUPathListItemCount", listItemCountInt + 1, RegistryValueKind.DWord);

                var pathListItem = new List<string>();

                for (var i = 0; i < listItemCountInt; i++)
                {
                    pathListItem.Add(registryKey.GetValue(subKey, "MRUPathListItem" + i));
                }

                if (!string.IsNullOrEmpty(Path.GetExtension(path)))
                {
                    registryKey.SetValue(subKey, "MRUPathListItem0", path, RegistryValueKind.String);
                }
                else
                {
                    var mRuPathListItem0 = registryKey.GetValue(subKey, "MRUPathListItem0");
                    if (!string.IsNullOrEmpty(mRuPathListItem0))
                    {
                        registryKey.SetValue(subKey, "MRUPathListItem0", path + Path.GetExtension(mRuPathListItem0), RegistryValueKind.String);
                    }
                }

                registryKey.SetValue(subKey, "MRUFormatListItem0", 36, RegistryValueKind.DWord);

                var countPath = 1;
                foreach (var item in pathListItem)
                {
                    registryKey.SetValue(subKey, "MRUPathListItem" + countPath, item, RegistryValueKind.String);
                    registryKey.SetValue(subKey, "MRUFormatListItem" + countPath, 36, RegistryValueKind.DWord);
                    countPath++;
                }
            }
            else
            {
                registryKey.SetValue(subKey, "MRUPathListItemCount", 1, RegistryValueKind.DWord);
                registryKey.SetValue(subKey, "MRUPathListItem0",
                                Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + string.Format(WBlockRes.NewBlock),
                                RegistryValueKind.String);
                registryKey.SetValue(subKey, "MRUFormatListItem0", 36, RegistryValueKind.DWord);
            }
        }

        /// <summary>
        /// 複数オブジェクト選択
        /// 　・ロック画層除外
        /// 　・事前選択有効
        /// 　・CommandFlags.Modal用
        /// </summary>
        public ObjectIdCollection SelectEntities(Document doc, Transaction tr, int selectionSetCount = 0)
        {
            var objectIds = new ObjectIdCollection();

            //Clear array of object selected before if any (Autocad-ArrayClassic Rule)
            var idArrayEmpty = new ObjectId[0];
            doc.Editor.SetImpliedSelection(idArrayEmpty);

            //オブジェクトを選択:
            PromptSelectionOptions opt;
            if (selectionSetCount != 0)
            {
                opt = new PromptSelectionOptions()
                {
                    RejectObjectsOnLockedLayers = true,
                    SingleOnly = true,
                    MessageForAdding = $"\n{WBlockRes.UI_2}"
                };
            }
            else
            {
                opt = new PromptSelectionOptions()
                {
                    RejectObjectsOnLockedLayers = true,
                    MessageForAdding = $"\n{WBlockRes.UI_2}"
                };
            }

            var objectOnLockedLayerCount = 0;
            var selectedEntities = _userInput.GetSelection(opt);
            if (selectedEntities.Status != PromptStatus.OK)
            {
                return null;
            }
            var set = selectedEntities.Value;
            using (var entityEx = new EntityEx())
            {
                doc.LockDocument();
                foreach (SelectedObject selectedObject in set)
                {
                    var id = selectedObject.ObjectId;
                    if (id == ObjectId.Null) continue;

                    var entity = tr.GetObject<Entity>(id, OpenMode.ForWrite, true, true);
                    if (entity is null) continue;

                    if (_userInput.EntityOnLockedLayer(tr, entity))
                    {
                        objectOnLockedLayerCount++;
                    }
                    else
                    {
                        entityEx.DbObjects.Add(entity);
                        objectIds.Add(id);
                    }
                }

                if (objectOnLockedLayerCount == 1)
                {
                    entityEx.ObjectOnLockedLayer = objectOnLockedLayerCount;
                    new MinusWBlockCmd().WriteMessage($"\n{string.Format(MinusWBlockRes.CM_2_2_1, objectOnLockedLayerCount)}");
                }
                else if (objectOnLockedLayerCount > 1)
                {
                    entityEx.ObjectOnLockedLayer = objectOnLockedLayerCount;
                    new MinusWBlockCmd().WriteMessage($"\n{string.Format(MinusWBlockRes.CM_2_2_2, objectOnLockedLayerCount)}");
                }
            }
            return objectIds;
        }

        /// <summary>
        /// 事前選択 取得
        /// 　・ロック画層除外
        /// 　・CommandFlags.Modal用
        /// </summary>
        public ObjectIdCollection SelectImplied(Transaction tr, ref int selectionSetCount, ref int objectOnLockedLayerCount)
        {
            var objectIds = new ObjectIdCollection();
            if (_userInput.GetPickFirst() != 1)
            {
                return objectIds;
            }
            var retPick = _userInput.GetPromptSelectionResult();
            if (retPick.Status != PromptStatus.OK)
            {
                return objectIds;
            }

            var set = retPick.Value;
            objectOnLockedLayerCount = 0;
            using (var entityEx = new EntityEx())
            {
                foreach (SelectedObject selectedObject in set)
                {
                    var id = selectedObject.ObjectId;
                    if (id == ObjectId.Null) continue;

                    var entity = tr.GetObject<Entity>(id, OpenMode.ForWrite, false, true);
                    if (entity is null) continue;

                    if (_userInput.EntityOnLockedLayer(tr, entity))
                    {
                        objectOnLockedLayerCount++;
                    }
                    else
                    {
                        entityEx.DbObjects.Add(entity);
                        objectIds.Add(id);
                    }
                }
            }
            selectionSetCount = set.Count;
            return objectIds;
        }
    }
}
