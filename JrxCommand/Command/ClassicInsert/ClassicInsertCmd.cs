﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;
using GrxCAD.ApplicationServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
#endif
using JrxCad.Utility;
using Exception = System.Exception;
using JrxCad.Command.MinusInsert;
using JrxCad.Enum;

namespace JrxCad.Command.ClassicInsert
{
    public partial class ClassicInsertCmd : BaseCommand
    {
        public ClassicInsertCmd() : this(new WindowInput()) { }
        public ClassicInsertCmd(IWindowInput userInput)
        {
            WindowUserInput = userInput;
        }

#if DEBUG
        [CommandMethod("JrxCommand", "C1", CommandFlags.Modal)]
#else
        [CommandMethod("JrxCommand", "SmxCLASSICINSERT", CommandFlags.Modal)]
#endif
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }
                var cmdActive = SystemVariable.GetInt("CMDACTIVE");
                if (cmdActive == (int)ActiveCommand.Script || cmdActive == (int)ActiveCommand.AutoLISP)
                {
                    var minusInsert = new MinusInsertCmd();
                    minusInsert.ExcuteCommand();
                }
                else
                {
                    WindowUserInput.ClassicInsertShowDialog();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }
    }
    class ClassicInsertResProxy : ClassicInsertRes
    {
        /// <summary>
        /// resolves the problem of internal constructor in resources.designer.cs
        /// in conjunction with xaml usage
        /// </summary>
        public ClassicInsertResProxy() : base()
        {
        }
    }
}
