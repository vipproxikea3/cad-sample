﻿#if _IJCAD_
using GrxCAD.Geometry;
using GrxCAD.ApplicationServices;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Runtime;
using Exception = GrxCAD.Runtime.Exception;
using CursorType = GrxCAD.EditorInput.CursorType;
using CADApp = GrxCAD.ApplicationServices.Application;
using Application = GrxCAD.ApplicationServices.Application;
using GrxCAD.ApplicationServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using Exception = Autodesk.AutoCAD.Runtime.Exception;
using CursorType = Autodesk.AutoCAD.EditorInput.CursorType;
using CADApp = Autodesk.AutoCAD.ApplicationServices.Application;
using CADPreferences = Autodesk.AutoCAD.Interop.AcadPreferences;
using Autodesk.AutoCAD.GraphicsInterface;

#endif
using System;
using System.IO;
using System.Drawing;
using JrxCad.Helpers;
using JrxCad.Utility;
using JrxCad.View.CustomWpf;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media.Imaging;
using System.Text.RegularExpressions;
using static JrxCad.Command.ClassicInsert.ClassicInsertCmd;
using Autodesk.AutoCAD.Internal;
using System.Windows.Forms;
using JrxCad.Enum;

namespace JrxCad.Command.ClassicInsert.ViewModel
{
    public class ClassicInsertViewModel : BindableBase
    {
        #region Enum And Struct
        public enum Valid
        {
            Ok = 0,
            InvalidCharacterBlockName = 1,
            FileWasNotFound = 2,
            BlockNameInvalid = 3,
            BlockNameNotSpecified = 4,
            BlockDefinitionChanged = 5,
            BlockAlreadyDefined = 6,
            UnKnowError = 7,
        }
        public struct SaveFormType
        {
            public string BlockName;

            public bool BasePointActive;

            public bool ScaleActive;
            public bool UniformScaleActive;

            public bool RotationActive;

            public bool ExplodeActive;
        }
        public enum TypeCursor
        {
            Normal = 0,
            Scale = 1,
            Rotation = 2,
            Corner = 3,
        }
        #endregion

        #region Private properties
        protected override string HelpName => "INSERT";
        private const string DynamicIconPath = "Images/icon2.ico";
        private const string AnnotativeIconPath = "Images/icon1.ico";
        private const double RotationDefault = -1; //Set -1 because rotation value can't be negative
        private BlockReference _blockRef;
        #endregion

        #region Public properties

        public static SaveFormType SavedForm;
        public bool RepeatActive;
#if _AutoCAD_
        //private ImageBGRA32 _img;
#endif
        public bool BasePointActive
        {
            get => _basePointActive;
            set
            {
                _basePointActive = value;
                EnableBasePoint = !value;
                OnPropertyChanged();
            }
        }
        private bool _basePointActive;

        public string BlockName
        {
            get => _blockName;
            set
            {
                try
                {
                    _blockName = value;
                    var idBlockRed = GetBlockForName(_blockName);

                    if (idBlockRed != null)
                    {
                        if (_blockName != Path.GetFileNameWithoutExtension(TxbPath))
                        {
                            TxbPath = null;
                        }
                        else
                        {
                            _previousPath = TxbPath;
                        }

                        if (_blockName == Path.GetFileNameWithoutExtension(_previousPath))
                        {
                            TxbPath = _previousPath;
                        }
                        _blockRef = new BlockReference(Point3d.Origin, idBlockRed.Value);
                    }
                    else
                    {
                        _blockRef = null;
                    }
                    OnPropertyChanged();
                }
                catch (Exception ex)
                {
                    Logger.ErrorShow(ex);
                }
            }
        }
        private string _blockName;

        public bool EnableExplode
        {
            get => _enableExplode;
            set => SetProperty(ref _enableExplode, value);
        }
        private bool _enableExplode;

        public bool EnableBasePoint
        {
            get => _enableBasePoint;
            set => SetProperty(ref _enableBasePoint, value);
        }
        private bool _enableBasePoint;

        public bool EnableRotation
        {
            get => _enableRotation;
            set => SetProperty(ref _enableRotation, value);
        }
        private bool _enableRotation;

        public bool EnableScaleX
        {
            get => _enableScaleX;
            set => SetProperty(ref _enableScaleX, value);
        }
        private bool _enableScaleX;

        public bool EnableScaleYz
        {
            get => _enableScaleYz;
            set => SetProperty(ref _enableScaleYz, value);
        }
        private bool _enableScaleYz;

        public bool ExplodeActive
        {
            get => _explodeActive;
            set
            {
                _explodeActive = value;
                if (ExplodeActive && EnableExplode)
                {
                    _tempVarUniformActive = UniformScaleActive;
                    EnableUniform = false;
                    UniformScaleActive = true;
                }
                else if (EnableExplode && !ExplodeActive)
                {
                    UniformScaleActive = _tempVarUniformActive;
                    EnableUniform = true;
                }
                OnPropertyChanged();
            }
        }
        private bool _tempVarUniformActive;
        private bool _explodeActive;

        public bool EnableUniform
        {
            get => _enableUniform;
            set => SetProperty(ref _enableUniform, value);
        }
        private bool _enableUniform;

        public string IconPreview1
        {
            get => _iconPreview1;
            set
            {
                _iconPreview1 = value;
                OnPropertyChanged();
            }
        }
        private string _iconPreview1;

        public string IconPreview2
        {
            get => _iconPreview2;
            set
            {
                _iconPreview2 = value;
                OnPropertyChanged();
            }
        }
        private string _iconPreview2;
        public int ImageSourceHeight
        {
            get => _imageSourceHeight;
            set
            {
                _imageSourceHeight = value;
                OnPropertyChanged();
            }
        }
        private int _imageSourceHeight;

        public virtual BitmapSource ImageSource
        {
            get => _imageSource;
            set
            {
                _imageSource = value;
                OnPropertyChanged();
            }
        }
        private BitmapSource _imageSource;

        public int ImageSourceWidth
        {
            get => _imageSourceWidth;
            set
            {
                _imageSourceWidth = value;
                OnPropertyChanged();
            }
        }
        private int _imageSourceWidth;

        public ObservableCollection<string> ListBlock
        {
            get => _listBlock;
            set
            {
                _listBlock = value;
                OnPropertyChanged();
            }
        }
        private ObservableCollection<string> _listBlock;

        public string SelectedItem
        {
            get => _selectedItem;
            set
            {
                _selectedItem = value;
                OnPropertyChanged();
            }
        }
        private string _selectedItem;

        public bool RotationActive
        {
            get => _rotationActive;
            set
            {
                _rotationActive = value;
                EnableRotation = !value;
                OnPropertyChanged();
            }
        }
        private bool _rotationActive;
        public bool ScaleActive
        {
            get => _scaleActive;
            set
            {
                _scaleActive = value;
                EnableScaleX = !value;
                EnableScaleYz = !value;
                if (UniformScaleActive && !_scaleActive)
                {
                    EnableScaleYz = value;
                }
                OnPropertyChanged();
            }
        }
        private bool _scaleActive;
        public double TxbAngle
        {
            get => _txbAngle;
            set
            {
                _txbAngle = value;
                OnPropertyChanged();
            }
        }
        private double _txbAngle;
        public double TxbBasePointX
        {
            get => _txbBasePointX;
            set
            {
                _txbBasePointX = value;
                OnPropertyChanged();
            }
        }
        private double _txbBasePointX;

        public double TxbBasePointY
        {
            get => _txbBasePointY;
            set
            {
                _txbBasePointY = value;
                OnPropertyChanged();
            }
        }
        private double _txbBasePointY;

        public double TxbBasePointZ
        {
            get => _txbBasePointZ;
            set
            {
                _txbBasePointZ = value;
                OnPropertyChanged();
            }
        }
        private double _txbBasePointZ;
        public string TxbFactor
        {
            get => _txbFactor;
            set
            {
                _txbFactor = value;
                var intPart = string.Empty;
                var expPart = string.Empty;
                if (double.Parse(_txbFactor) >= Math.Pow(10, 10) && double.Parse(_txbFactor) <= Math.Pow(10, 15) && !_txbFactor.Contains("E"))
                {
                    var exp = Math.Round(double.Parse(_txbFactor)).ToString().Length - 1;
                    intPart = (double.Parse(_txbFactor) / Math.Pow(10, exp)).ToString();
                    expPart = "E+" + exp.ToString();
                    var tempIntPart = double.Parse(intPart);
                    intPart = Converter.DistanceToString(tempIntPart, DistanceUnitFormat.Decimal, Util.Luprec());
                    _txbFactor = intPart + expPart;
                }
                else
                {
                    for (var i = 0; i < _txbFactor.Length; i++)
                    {
                        if (_txbFactor[i] != 'E')
                        {
                            continue;
                        }
                        intPart = _txbFactor.Substring(0, i);
                        expPart = _txbFactor.Substring(i);
                        break;
                    }
                    var tempIntPart = double.Parse(intPart.Length > 0 ? intPart : _txbFactor);
                    _txbFactor = Converter.DistanceToString(tempIntPart, DistanceUnitFormat.Decimal, Util.Luprec());
                    _txbFactor += expPart;
                }
                OnPropertyChanged();
            }
        }
        private string _txbFactor;
        public string TxbPath
        {
            get => _txbPath;
            set
            {
                try
                {
                    _txbPath = value;
                    if (File.Exists(_txbPath))
                    {
                        GetFactorAndUnit();
                    }
                    OnPropertyChanged();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw;
                }
            }
        }
        private string _txbPath;
        public double TxbScaleX
        {
            get => _txbScaleX;
            set
            {
                _txbScaleX = value;
                if (UniformScaleActive)
                {
                    TxbScaleY = value;
                    TxbScaleZ = value;
                }
                OnPropertyChanged();
            }
        }
        private double _txbScaleX;
        public double TxbScaleY
        {
            get => _txbScaleY;
            set
            {
                _txbScaleY = value;
                OnPropertyChanged();
            }
        }
        private double _txbScaleY;
        public double TxbScaleZ
        {
            get => _txbScaleZ;
            set
            {
                _txbScaleZ = value;
                OnPropertyChanged();
            }
        }
        private double _txbScaleZ;
        public string TxbUnit
        {
            get => _txbUnit;
            set => SetProperty(ref _txbUnit, value);
        }
        private string _txbUnit;
        public bool UniformScaleActive
        {
            get => _uniformScaleActive;
            set
            {
                _uniformScaleActive = value;
                EnableScaleYz = !value;
                if (!_uniformScaleActive && ScaleActive)
                {
                    EnableScaleYz = value;
                }
                if (_uniformScaleActive)
                {
                    if (TxbScaleY != 0 && TxbScaleZ != 0)
                    {
                        _previousScaleY = TxbScaleY;
                        _previousScaleZ = TxbScaleZ;
                    }
                    TxbScaleY = TxbScaleX;
                    TxbScaleZ = TxbScaleX;
                }
                if (!_uniformScaleActive)
                {
                    TxbScaleY = _previousScaleY;
                    TxbScaleZ = _previousScaleZ;
                }
                OnPropertyChanged();
            }
        }
        private bool _uniformScaleActive;
        private string _previousPath;
        private double _previousScaleY = 1;
        private double _previousScaleZ = 1;
        #endregion

        public void Initialize()
        {
            if (SavedForm.BlockName != null)
            {
                ListBlock = new ObservableCollection<string>();
                GetListBlock();

                SelectedItem = SavedForm.BlockName;

                BasePointActive = SavedForm.BasePointActive;

                ScaleActive = SavedForm.ScaleActive;
                UniformScaleActive = SavedForm.UniformScaleActive;

                RotationActive = SavedForm.RotationActive;

                ExplodeActive = SavedForm.ExplodeActive;
                TxbScaleX = 1;
                TxbScaleY = 1;
                TxbScaleZ = 1;
            }
            else
            {
                ListBlock = new ObservableCollection<string>();
                GetListBlock();
                if (ListBlock.Count != 0)
                {
                    SelectedItem = ListBlock[0];
                }
                BasePointActive = true;
                EnableBasePoint = !BasePointActive;
                EnableRotation = !RotationActive;
                EnableScaleX = !ScaleActive;
                EnableScaleYz = !UniformScaleActive;
                EnableUniform = true;
                EnableExplode = true;
                TxbScaleX = 1;
                TxbScaleY = 1;
                TxbScaleZ = 1;
                TxbFactor = Converter.DistanceToString(1.0);
                TxbUnit = "Unitless";
            }
            TxbAngle = 360 - double.Parse(SystemVariable.GetAngle("ANGBASE").RadiansToDegrees().ToString());
            if (TxbAngle == 360)
            {
                TxbAngle = 0;
            }

            RepeatActive = false;
            EnableUniform = true;
            EnableExplode = true;
            ImageSourceHeight = 200;
            ImageSourceWidth = 160;
        }

        public ClassicInsertViewModel()
        {
            Initialize();
        }

        /// <summary>
        /// Insert block
        /// </summary>
        private void AddBlockOrDwg(string name, string path)
        {
            ObjectId idBlockDef;
            if (!string.IsNullOrEmpty(path) && Path.GetDirectoryName(path) != "")
            {
                idBlockDef = GetBlockFromDwg(name, path);
            }
            else
            {
                var objectIdTemp = GetBlockForName(name);
                if (objectIdTemp == null)
                {
                    return;
                }
                idBlockDef = objectIdTemp.Value;
            }

            using (var blockRef = new BlockReference(Point3d.Origin, idBlockDef))
            {
                var isScaled = false;
                var isRotated = false;
                var baseVector = new Vector3d();
                System.Enum.TryParse(TxbUnit, out UnitsValue blockUnit);
                blockRef.BlockUnit = blockUnit;
                if (blockRef.Annotative == AnnotativeStates.True)
                {
                    var objectContextManager = Util.Database().ObjectContextManager;
                    var objectContextCollection = objectContextManager.GetContextCollection("ACDB_ANNOTATIONSCALES");

#if _AutoCAD_
                    ObjectContexts.AddContext(blockRef, objectContextCollection.CurrentContext);
#elif _IJCAD_
                    //TODO: IJCAD does not suppot ObjectContexts
#endif
                }
                if (!BasePointActive)
                {
                    blockRef.Position = new Point3d(TxbBasePointX, TxbBasePointY, TxbBasePointZ);
                    var ucs = CoordConverter.UcsToWcs();
                    blockRef.Position = blockRef.Position.TransformBy(ucs);
                }
                if (!ScaleActive)
                {
                    blockRef.ScaleFactors = UniformScaleActive
                        ? new Scale3d(TxbScaleX * blockRef.UnitFactor)
                        : new Scale3d(TxbScaleX * blockRef.UnitFactor, TxbScaleY * blockRef.UnitFactor, TxbScaleZ * blockRef.UnitFactor);
                }
                else
                {
                    blockRef.ScaleFactors = UniformScaleActive
                       ? new Scale3d(blockRef.UnitFactor)
                       : new Scale3d(blockRef.UnitFactor, blockRef.UnitFactor, blockRef.UnitFactor);
                }
                if (!RotationActive)
                {
                    blockRef.Rotation = TxbAngle.DegreesToRadians();
                }
                if (ExplodeActive)
                {
                    if (BasePointActive || ScaleActive || RotationActive)
                    {
                        if (InsertionExplodeBlock(blockRef) == PromptStatus.Cancel)
                        {
                            return;
                        }
                    }
                    var oldSegment = new DBObjectCollection();
                    blockRef.Explode(oldSegment);
                    foreach (var obj in oldSegment)
                    {
                        using (var tr = Util.StartTransaction())
                        {
                            var entity = (Entity)obj;
                            tr.AddNewlyCreatedDBObject(entity, true, tr.CurrentSpace());
                            tr.Commit();
                        }
                    }
                    ExplodeActive = false;
                    return;
                }
                if (!BasePointActive && ScaleActive && RotationActive)
                {
                    if (ScaleUi(blockRef, null) == PromptStatus.Cancel)
                    {
                        return;
                    }
                    if (RotationUi(blockRef, null) == PromptStatus.Cancel)
                    {
                        return;
                    }
                }
                else if (!BasePointActive && ScaleActive)
                {
                    if (ScaleUi(blockRef, null) == PromptStatus.Cancel)
                    {
                        return;
                    }
                }
                else if (!BasePointActive && RotationActive)
                {
                    if (RotationUi(blockRef, null) == PromptStatus.Cancel)
                    {
                        return;
                    }
                }
                else if (BasePointActive)
                {
                    if (InsertionPointUi(blockRef, ref isScaled, ref isRotated, ref baseVector) == PromptStatus.Cancel)
                    {
                        return;
                    }
                    if (ScaleActive && !isScaled)
                    {
                        if (ScaleUi(blockRef, baseVector) == PromptStatus.Cancel)
                        {
                            return;
                        }
                    }
                    if (RotationActive && !isRotated)
                    {
                        if (RotationUi(blockRef, baseVector) == PromptStatus.Cancel)
                        {
                            return;
                        }
                    }
                }
                if (ExplodeActive)
                {
                    var oldSegment = new DBObjectCollection();
                    blockRef.Explode(oldSegment);
                    foreach (DBObject obj in oldSegment)
                    {
                        using (var tr = Util.StartTransaction())
                        {
                            var entity = (Entity)obj;
                            tr.AddNewlyCreatedDBObject(entity, true, tr.CurrentSpace());
                            tr.Commit();
                        }
                    }
                }
                else
                {
                    // TODO: ATTEDIT
                    using (var tr = Util.StartTransaction())
                    {
                        tr.AddNewlyCreatedDBObject(blockRef, true, tr.CurrentSpace());
                        tr.Commit();
                    }
                }
                var currentScale = new Scale3d();
                var currentBaseVector = new Vector3d();
                var currentRotate = RotationDefault;
                while (RepeatActive)
                {
                    var blockRefRepeat = new BlockReference(Point3d.Origin, idBlockDef)
                    {
                        ScaleFactors = (currentScale == new Scale3d()) ? blockRef.ScaleFactors : currentScale,
                        Rotation = (currentRotate.IsEqual(RotationDefault)) ? blockRef.Rotation : currentRotate
                    };
                    currentBaseVector = (currentBaseVector == new Vector3d()) ? baseVector : currentBaseVector;
                    if (InsertionPointUi(blockRefRepeat, ref isScaled, ref isRotated, ref currentBaseVector) == PromptStatus.Cancel)
                    {
                        return;
                    }
                    if (ExplodeActive)
                    {
                        var oldSegment = new DBObjectCollection();
                        blockRefRepeat.Explode(oldSegment);
                        foreach (DBObject obj in oldSegment)
                        {
                            using (var tr = Util.StartTransaction())
                            {
                                var entity = (Entity)obj;
                                tr.AddNewlyCreatedDBObject(entity, true, tr.CurrentSpace());
                                tr.Commit();
                            }
                        }
                    }
                    else
                    {
                        using (var tr = Util.StartTransaction())
                        {
                            tr.AddNewlyCreatedDBObject(blockRefRepeat, true, tr.CurrentSpace());
                            tr.Commit();
                        }
                    }
                    currentScale = blockRefRepeat.ScaleFactors;
                    currentRotate = blockRefRepeat.Rotation;
                }
                SystemVariable.SetString("INSNAME", blockRef.Name);
            }
        }

        /// <summary>
        /// Handle Base point in Insertion point option
        /// </summary>
        private PromptStatus BasePoint(InsertionPointOpts optInsertionPt)
        {
            var optBasePt = new BasePointOpts(optInsertionPt.BlockReference)
            {
                Options = new JigPromptPointOptions($"\n{ClassicInsertRes.UI_BasePoint}")
                {
                    UserInputControls = UserInputControls.Accept3dCoordinates,
                }
            };

            var resBasePt = (PromptPointResult)WindowUserInput.AcquirePoint(optBasePt);

            switch (resBasePt.Status)
            {
                case PromptStatus.OK:
                    {
                        var vector = resBasePt.Value - optInsertionPt.BlockReference.Position;
                        var baseVector = new Vector3d(vector.X
                                                        / optInsertionPt.BlockReference.ScaleFactors.X, vector.Y
                                                        / optInsertionPt.BlockReference.ScaleFactors.Y, 0);
                        optInsertionPt.BaseVector = baseVector;
                    }
                    return PromptStatus.OK;
                default:
                    return PromptStatus.Cancel;
            }
        }

        /// <summary>
        /// Handle button OK
        /// </summary>
        public void ButtonOk(CustomWindow currentWindow)
        {
            try
            {
                if (!string.IsNullOrEmpty(BlockName))
                {
                    BlockName = BlockName.Trim();
                }

                var valid = IsValid(BlockName, TxbPath);
                DialogResult result;

                switch (valid)
                {
                    // TODO: Task Dialog
                    case Valid.Ok:
                        currentWindow.Hide();
                        AddBlockOrDwg(BlockName, TxbPath);
                        GetAlreadyInsertedValue(ref SavedForm);
                        break;
                    case Valid.InvalidCharacterBlockName:
                        MsgBox.Error.Show(string.Format(ClassicInsertRes.DM1), string.Format(ClassicInsertRes.Title_MessageBox));
                        break;
                    case Valid.FileWasNotFound:
                        MsgBox.Error.Show(string.Format(ClassicInsertRes.DM2_1), string.Format(ClassicInsertRes.Title_MessageBox));
                        break;
                    case Valid.BlockNameInvalid:
                        MsgBox.Error.Show(string.Format(ClassicInsertRes.DM2_2), string.Format(ClassicInsertRes.Title_MessageBox));
                        break;
                    case Valid.BlockNameNotSpecified:
                        MsgBox.Error.Show(string.Format(ClassicInsertRes.DM2_3), string.Format(ClassicInsertRes.Title_MessageBox));
                        break;
                    case Valid.BlockDefinitionChanged:
                        result = MsgBox.Warning.Show(string.Format(ClassicInsertRes.DM3_1), string.Format(ClassicInsertRes.Title_MessageBox));
                        switch (result)
                        {
                            case DialogResult.Yes:
                                currentWindow.Hide();
                                RedefineBlock(BlockName, TxbPath);
                                AddBlockOrDwg(BlockName, TxbPath);
                                break;
                            case DialogResult.No:
                                currentWindow.Hide();
                                AddBlockOrDwg(BlockName, null);
                                break;
                        }
                        GetAlreadyInsertedValue(ref SavedForm);
                        break;
                    case Valid.BlockAlreadyDefined:
                        var countOfBlock = ValidateIsDwgFileInDrawing(BlockName, TxbPath);
                        result = MsgBox.Warning.Show(string.Format(ClassicInsertRes.DM3_2, BlockName, countOfBlock),
                                                        string.Format(ClassicInsertRes.Title_MessageBox));

                        switch (result)
                        {
                            case DialogResult.Yes:
                                currentWindow.Hide();
                                RedefineBlock(BlockName, TxbPath);
                                AddBlockOrDwg(BlockName, TxbPath);
                                break;
                            case DialogResult.No:
                                currentWindow.Hide();
                                AddBlockOrDwg(BlockName, null);
                                break;
                        }
                        GetAlreadyInsertedValue(ref SavedForm);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
            }
        }

        /// <summary>
        /// Get background color
        /// </summary>
        protected Color GetBackgroundColor()
        {
#if _AutoCAD_
            var preference = (CADPreferences)CADApp.Preferences;
            var backgroundColor = preference.Display.GraphicsWinModelBackgrndColor;
            return Color.FromArgb(
                (int)(backgroundColor & 0x000000FF),
                (int)((backgroundColor & 0x0000FF00) >> 8),
                (int)((backgroundColor & 0x00FF0000) >> 16)
            );
#elif _IJCAD_
            return Color.FromArgb(255, 255, 255);
#endif
        }

        /// <summary>
        /// Get ObjectId by BlockName
        /// </summary>
        private ObjectId? GetBlockForName(string blockName)
        {
            using (var tr = Util.StartTransaction())
            {
                var blockTable = tr.GetObject<BlockTable>(Util.Database().BlockTableId, OpenMode.ForRead);
                var image = new BlockImage()
                {
                    ImageWidth = ImageSourceWidth,
                    ImageHeight = ImageSourceHeight,
                    Background = GetBackgroundColor(),
                };
                foreach (var blockDefId in blockTable)
                {
                    var blockDef = tr.GetObject<BlockTableRecord>(blockDefId, OpenMode.ForRead, false);
                    if (blockDef == null ||
                        blockDef.IsFromExternalReference ||
                        blockDef.IsLayout ||
                        blockDef.IsAnonymous ||
                        blockDef.Name.Contains("|"))
                    {
                        continue;
                    }

                    if (blockDef.Name != blockName)
                    {
                        continue;
                    }
                    _blockRef = new BlockReference(Point3d.Origin, blockDefId);

                    TxbUnit = UnitIns.GetName(_blockRef.BlockUnit);
                    TxbFactor = _blockRef.UnitFactor.ToString();
                    if (_blockRef == null)
                    {
                        return null;
                    }

                    IconPreview2 = _blockRef.IsDynamicBlock ? DynamicIconPath : string.Empty;
                    IconPreview1 = _blockRef.Annotative == AnnotativeStates.True ? AnnotativeIconPath : string.Empty;

                    if (blockDef.BlockScaling == BlockScaling.Uniform)
                    {
                        EnableUniform = false;
                        UniformScaleActive = true;
                    }
                    else
                    {
                        EnableUniform = true;
                    }

                    ImageSource = image.CreateWpf(blockDef, true);
                    EnableExplode = blockDef.Explodable;
                    if (!EnableExplode)
                    {
                        ExplodeActive = EnableExplode;
                    }

                    return blockDefId;
                }
            }

            ImageSource = null;
            IconPreview1 = string.Empty;
            IconPreview2 = string.Empty;
            return null;
        }

        /// <summary>
        /// Get block by path
        /// </summary>
        private ObjectId GetBlockFromDwg(string name, string path)
        {
            using (var insertDatabase = new Database(false, false))
            {
                var isDwg = string.IsNullOrEmpty(path) || Path.GetExtension(path).ToUpper() == ".DWG";

                if (isDwg)
                {
                    insertDatabase.ReadDwgFile(path, FileOpenMode.OpenForReadAndAllShare, false, null);
                }
                else
                {
                    var logPath = Path.GetTempFileName();
                    insertDatabase.DxfIn(path, logPath);
                    File.Delete(logPath);
                }
                return Util.Database().Insert(name, insertDatabase, true);
            }
        }

        /// <summary>
        /// Get factor and unit in Database
        /// </summary>
        private void GetFactorAndUnit()
        {
            if (string.IsNullOrEmpty(TxbPath))
            {
                return;
            }

            using (var db = new Database(false, false))
            {
                if (Path.GetExtension(TxbPath).ToUpper() == ".DWG")
                {
                    db.ReadDwgFile(TxbPath, FileShare.ReadWrite, true, "");
                }
                else
                {
                    var logPath = Path.GetTempFileName();
                    db.DxfIn(TxbPath, logPath);
                    File.Delete(logPath);
                }

                TxbUnit = UnitIns.GetName(db.Insunits);
                TxbFactor = UnitIns.GetValueFactor(Util.Database().Insunits, db.Insunits).ToString();
            }
        }

        /// <summary>
        /// Get list block in DB
        /// </summary>
        private void GetListBlock()
        {
            using (var tr = Util.StartTransaction())
            {
                var blockTable = tr.GetObject<BlockTable>(Util.Database().BlockTableId, OpenMode.ForRead);

                foreach (var blockDefId in blockTable)
                {
                    var blockDef = tr.GetObject<BlockTableRecord>(blockDefId, OpenMode.ForRead, false);
                    if (blockDef == null ||
                        blockDef.IsFromExternalReference ||
                        blockDef.IsLayout ||
                        blockDef.IsAnonymous ||
                        blockDef.Name.Contains("|"))
                    {
                        continue;
                    }
                    ListBlock.Add(blockDef.Name);
                    OnPropertyChanged();
                }

                ListBlock = new ObservableCollection<string>(ListBlock.OrderBy(x => x));
            }
        }

        /// <summary>
        /// Get all the value of the inserted block
        /// </summary>
        private void GetAlreadyInsertedValue(ref SaveFormType saveFormType)
        {
            saveFormType.BlockName = BlockName;
            saveFormType.BasePointActive = BasePointActive;
            saveFormType.ScaleActive = ScaleActive;
            saveFormType.UniformScaleActive = UniformScaleActive;
            saveFormType.RotationActive = RotationActive;
            saveFormType.ExplodeActive = ExplodeActive;
        }

        /// <summary>
        /// Handle Insertion Explode Block
        /// </summary>
        private PromptStatus InsertionExplodeBlock(BlockReference blockReference)
        {
            if (BasePointActive)
            {
                var optPoint = new PromptPointOptions($"\n{ClassicInsertRes.UI_InsertionExplode}");
                var resPoint = Util.Editor().GetPoint(optPoint);

                switch (resPoint.Status)
                {
                    case PromptStatus.OK:
                        blockReference.Position = resPoint.Value;
                        break;
                    default:
                        return PromptStatus.Cancel;
                }
            }

            if (ScaleActive)
            {
                var optScale = new PromptDoubleOptions($"\n{ClassicInsertRes.UI_ScaleExplode}")
                {
                    DefaultValue = 1,
                    AllowZero = false
                };

                var resScale = Util.Editor().GetDouble(optScale);

                switch (resScale.Status)
                {
                    case PromptStatus.OK:
                        blockReference.ScaleFactors = new Scale3d(resScale.Value * blockReference.UnitFactor,
                                                        resScale.Value * blockReference.UnitFactor,
                                                        resScale.Value * blockReference.UnitFactor);
                        break;
                    default:
                        return PromptStatus.Cancel;
                }
            }

            if (!RotationActive)
            {
                return PromptStatus.OK;
            }

            var angBase = SystemVariable.GetAngle("ANGBASE");
            var optRotation = new PromptAngleOptions($"\n{ClassicInsertRes.UI_RotationExplode}")
            {
                DefaultValue = angBase,
                BasePoint = blockReference.Position,
                UseBasePoint = true,
            };
            var resRotation = Util.Editor().GetAngle(optRotation);

            switch (resRotation.Status)
            {
                case PromptStatus.OK:
                    blockReference.Rotation = resRotation.Value;
                    break;
                default:
                    return PromptStatus.Cancel;
            }

            return PromptStatus.OK;
        }

        /// <summary>
        /// Handle Insertion point Option
        /// </summary>
        private PromptStatus InsertionPointUi(BlockReference blockReference, ref bool isScaled, ref bool isRotated, ref Vector3d baseVector)
        {
            var baseVector1 = new Vector3d(baseVector.X, baseVector.Y, baseVector.Z);
            var optInsertionPt = new InsertionPointOpts(blockReference, baseVector1);
            {
                if (UniformScaleActive)
                {
                    optInsertionPt.Options = new JigPromptPointOptions($"\n{ClassicInsertRes.UI_InsertionPointUniformScale}")
                    {
                        AppendKeywordsToMessage = false,
                        UserInputControls = UserInputControls.Accept3dCoordinates,
                        Keywords =
                        {
                            {"B", $"{ClassicInsertRes.BasePoint_Key}"},
                            {"S", $"{ClassicInsertRes.Scale_Key}"},
                            {"R", $"{ClassicInsertRes.Rotate_Key}"},
                            {"E", $"{ClassicInsertRes.Explode_Key}"},
                            {"RE", $"{ClassicInsertRes.REpeat_Key}"},
                            {"P", $"{ClassicInsertRes.P_Key}"},
                            {"PS", $"{ClassicInsertRes.PScale_Key}"},
                            {"PR", $"{ClassicInsertRes.PRotate_Key}"},
                        }
                    };
                }
                else
                {
                    optInsertionPt.Options = new JigPromptPointOptions($"\n{ClassicInsertRes.UI_InsertionPoint}")
                    {
                        AppendKeywordsToMessage = false,
                        UserInputControls = UserInputControls.Accept3dCoordinates,
                        Keywords =
                        {
                            {"B", $"{ClassicInsertRes.BasePoint_Key}"},
                            {"S", $"{ClassicInsertRes.Scale_Key}"},
                            {"X", $"{ClassicInsertRes.X_Key}"},
                            {"Y", $"{ClassicInsertRes.Y_Key}"},
                            {"Z", $"{ClassicInsertRes.Z_Key}"},
                            {"R", $"{ClassicInsertRes.Rotate_Key}"},
                            {"E", $"{ClassicInsertRes.Explode_Key}"},
                            {"RE", $"{ClassicInsertRes.REpeat_Key}"},
                            {"P", $"{ClassicInsertRes.P_Key}"},
                            {"PS", $"{ClassicInsertRes.PScale_Key}"},
                            {"PX", $"{ClassicInsertRes.PXscale_Key}"},
                            {"PY", $"{ClassicInsertRes.PYscale_Key}"},
                            {"PZ", $"{ClassicInsertRes.PZscale_Key}"},
                            {"PR", $"{ClassicInsertRes.PRotate_Key}"},
                        }
                    };
                }
            }

            while (true)
            {
                var resInsertionPt = (PromptPointResult)WindowUserInput.AcquirePoint(optInsertionPt);
                if (resInsertionPt.Status != PromptStatus.OK && resInsertionPt.Status != PromptStatus.Keyword)
                {
                    return PromptStatus.Cancel;
                }

                switch (resInsertionPt.Status)
                {
                    case PromptStatus.OK:
                        baseVector = optInsertionPt.BaseVector;
                        return PromptStatus.OK;
                    case PromptStatus.Keyword:
                        switch (resInsertionPt.StringResult)
                        {
                            case "B":
                                {
                                    if (BasePoint(optInsertionPt) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                            case "S":
                                {
                                    if (ScaleS(optInsertionPt.BlockReference, false) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                    isScaled = true;
                                }
                                break;
                            case "X":
                                {
                                    if (Scale(optInsertionPt.BlockReference, ScaleType.X, false) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                    isScaled = true;
                                }
                                break;
                            case "Y":
                                {
                                    if (Scale(optInsertionPt.BlockReference, ScaleType.Y, false) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                    isScaled = true;
                                }
                                break;
                            case "Z":
                                {
                                    if (Scale(optInsertionPt.BlockReference, ScaleType.Z, false) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                    isScaled = true;
                                }
                                break;
                            case "R":
                                {
                                    if (Rotation(optInsertionPt, false) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                    isRotated = true;
                                }
                                break;
                            case "E":
                                {
                                    var optExplode = new PromptKeywordOptions($"\n{ClassicInsertRes.UI_Explode}")
                                    {
                                        AppendKeywordsToMessage = false,
                                        Keywords =
                                        {
                                            {"Y", $"{ClassicInsertRes.Yes_Key}"},
                                            {"N", $"{ClassicInsertRes.No_Key}"},
                                        }
                                    };
                                    optExplode.Keywords.Default = "Y";
                                    var resExplode = Util.Editor().GetKeywords(optExplode);
                                    switch (resExplode.Status)
                                    {
                                        case PromptStatus.Cancel:
                                            return PromptStatus.Cancel;
                                        case PromptStatus.OK when resExplode.StringResult == "Y":
                                            ExplodeActive = true;
                                            break;
                                        case PromptStatus.OK:
                                            ExplodeActive = false;
                                            break;
                                    }
                                }
                                break;
                            case "RE":
                                {
                                    var optRepeat = new PromptKeywordOptions($"{ClassicInsertRes.UI_Repeat}")
                                    {
                                        AppendKeywordsToMessage = false,
                                        Keywords =
                                        {
                                            {"Y", $"{ClassicInsertRes.Yes_Key}"},
                                            {"N", $"{ClassicInsertRes.No_Key}"},
                                        }
                                    };
                                    optRepeat.Keywords.Default = "Y";
                                    var resRepeat = Util.Editor().GetKeywords(optRepeat);
                                    switch (resRepeat.Status)
                                    {
                                        case PromptStatus.Cancel:
                                            return PromptStatus.Cancel;
                                        case PromptStatus.OK when resRepeat.StringResult == "Y":
                                            RepeatActive = true;
                                            break;
                                        case PromptStatus.OK:
                                            RepeatActive = false;
                                            break;
                                    }
                                }
                                break;
                            case "P":
                                {
                                    if (PreviewScale(optInsertionPt) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                    break;
                                }
                            case "PS":
                                {
                                    if (ScaleS(optInsertionPt.BlockReference, true) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                            case "PX":
                                {
                                    if (Scale(optInsertionPt.BlockReference, ScaleType.X, true) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                            case "PY":
                                {
                                    if (Scale(optInsertionPt.BlockReference, ScaleType.Y, true) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                            case "PZ":
                                {
                                    if (Scale(optInsertionPt.BlockReference, ScaleType.Z, true) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                            case "PR":
                                {
                                    if (Rotation(optInsertionPt, true) == PromptStatus.Cancel)
                                        return PromptStatus.Cancel;
                                }
                                break;
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Validate string name
        /// </summary>
        private Valid IsValid(string name, string path)
        {
            try
            {
                var invalidFileNameChars = new string(Path.GetInvalidFileNameChars());
                invalidFileNameChars += @"<>|:*?/;,=`\";
                var containsABadCharacter = new Regex("[" + Regex.Escape(invalidFileNameChars) + "]");

                //Validation 4, block name is empty or null
                if (string.IsNullOrEmpty(name))
                {
                    return Valid.BlockNameNotSpecified;
                }

                // Validation 2, path is not exist
                if (Path.GetExtension(name).ToUpper() == ".DWG")
                {
                    if (File.Exists(name))
                    {
                        TxbPath = name;
                        BlockName = Path.GetFileNameWithoutExtension(name);
                    }
                    else
                    {
                        path = name;
                        name = Path.GetFileName(name);
                        var flag = false;
                        var currentDirectory = Util.Database().OriginalFileName;
                        currentDirectory = Path.GetDirectoryName(currentDirectory);
                        if (currentDirectory != null)
                        {
                            var fileDirectory = Path.Combine(currentDirectory, name);
                            if (!File.Exists(fileDirectory))
                            {
                                var supportPaths = SystemVariable.GetString("ACADPREFIX").Split(';');
                                foreach (var supportPath in supportPaths)
                                {
                                    fileDirectory = Path.Combine(supportPath, name);
                                    if (File.Exists(fileDirectory))
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                                if (!flag)
                                {
                                    BlockName = Path.GetFileNameWithoutExtension(name);
                                    TxbPath = path;
                                    return Valid.FileWasNotFound;
                                }
                            }
                            TxbPath = fileDirectory;
                        }

                        path = TxbPath;
                        BlockName = Path.GetFileNameWithoutExtension(name);
                    }
                }
                if (!File.Exists(path) && !string.IsNullOrEmpty(Path.GetDirectoryName(path)))
                {
                    return Valid.FileWasNotFound;
                }

                //Validation 1, block name include bad character
                if (containsABadCharacter.IsMatch(name) && Path.GetExtension(name).ToUpper() != ".DWG")
                {
                    return Valid.InvalidCharacterBlockName;
                }

                // Validation 3, block name is not include in database
                if (!ListBlock.Contains(name) && (path == null || Path.GetDirectoryName(path) == "") && Path.GetExtension(name).ToUpper() != ".DWG")
                {
                    if (ListBlock.Count == 0)
                    {
                        return Valid.BlockNameInvalid;
                    }
                    var currentDirectory = Path.GetDirectoryName(Util.Database().OriginalFileName);
                    if (currentDirectory != null)
                    {
                        TxbPath = Path.Combine(currentDirectory, name) + ".dwg";
                    }
                    return Valid.BlockNameInvalid;
                }

                // Validation 5, redefine a block has already included in database but not in drawing
                if (ValidateIsDwgFileInDatabase(name, path) && ValidateIsDwgFileInDrawing(name, path) == 0)
                {
                    return Valid.BlockDefinitionChanged;
                }

                // Validation 6, redefine a block has already included in drawing
                if (ValidateIsDwgFileInDrawing(name, path) != 0 && ValidateIsDwgFileInDatabase(name, path))
                {
                    return Valid.BlockAlreadyDefined;
                }
                return Valid.Ok;
            }
            catch
            {
                return Valid.InvalidCharacterBlockName;
            }
        }

        /// <summary>
        /// Preview Scale when insert point
        /// </summary>
        private PromptStatus PreviewScale(InsertionPointOpts optInsertionPt)
        {
            PromptKeywordOptions optPreview;
            if (UniformScaleActive)
            {
                optPreview = new PromptKeywordOptions($"{ClassicInsertRes.UI_PreviewUniform}")
                {
                    AppendKeywordsToMessage = false,
                    Keywords =
                                            {
                                                {"PS", $"{ClassicInsertRes.PScale_Key}"},
                                                {"PR", $"{ClassicInsertRes.PRotate_Key}"},
                                            }
                };
            }
            else
            {
                optPreview = new PromptKeywordOptions($"{ClassicInsertRes.UI_Preview}")
                {
                    AppendKeywordsToMessage = false,
                    Keywords =
                                            {
                                                {"PS", $"{ClassicInsertRes.PScale_Key}"},
                                                {"PX", $"{ClassicInsertRes.PXscale_Key}"},
                                                {"PY", $"{ClassicInsertRes.PYscale_Key}"},
                                                {"PZ", $"{ClassicInsertRes.PZscale_Key}"},
                                                {"PR", $"{ClassicInsertRes.PRotate_Key}"},
                                            }
                };
            }
            var resRepeat = Util.Editor().GetKeywords(optPreview);
            if (resRepeat.Status != PromptStatus.OK)
            {
                return PromptStatus.Cancel;
            }

            switch (resRepeat.Status)
            {
                case PromptStatus.OK:
                    switch (resRepeat.StringResult)
                    {
                        case "PS":
                            {
                                if (ScaleS(optInsertionPt.BlockReference, true) == PromptStatus.Cancel)
                                    return PromptStatus.Cancel;
                            }
                            break;
                        case "PX":
                            {
                                if (Scale(optInsertionPt.BlockReference, ScaleType.X, true) == PromptStatus.Cancel)
                                    return PromptStatus.Cancel;
                            }
                            break;
                        case "PY":
                            {
                                if (Scale(optInsertionPt.BlockReference, ScaleType.Y, true) == PromptStatus.Cancel)
                                    return PromptStatus.Cancel;
                            }
                            break;
                        case "PZ":
                            {
                                if (Scale(optInsertionPt.BlockReference, ScaleType.Z, true) == PromptStatus.Cancel)
                                    return PromptStatus.Cancel;
                            }
                            break;
                        case "PR":
                            {
                                if (Rotation(optInsertionPt, true) == PromptStatus.Cancel)
                                    return PromptStatus.Cancel;
                            }
                            break;
                    }
                    break;
            }
            return PromptStatus.OK;
        }

        /// <summary>
        /// Redefine a block
        /// </summary>
        private void RedefineBlock(string name, string path)
        {
            using (var tr = Util.StartTransaction())
            {
                var dwgBlkId = GetBlockFromDwg(name, path);
                var acBlkTblRec = tr.GetObject<BlockTableRecord>(dwgBlkId, OpenMode.ForWrite);
                foreach (var objId in acBlkTblRec.GetBlockReferenceIds(false, true))
                {
                    var acBlkRef = tr.GetObject<BlockReference>((ObjectId)objId, OpenMode.ForWrite);
                    acBlkRef.RecordGraphicsModified(true);
                }
                Util.Editor().Regen();
                tr.Commit();
            }
        }

        /// <summary>
        /// Handle resize form
        /// </summary>
        public void Resized(CustomWindow currentWindow)
        {
            using (var tr = Util.StartTransaction())
            {
                ImageSourceWidth = (int)currentWindow.Width - 520;
                ImageSourceHeight = (int)currentWindow.Height - 140;
                var blockImage = new BlockImage()
                {
                    ImageWidth = ImageSourceWidth,
                    ImageHeight = ImageSourceHeight,
                    Background = GetBackgroundColor(),
                };
                var blockTable = tr.GetObject<BlockTable>(Util.Database().BlockTableId, OpenMode.ForRead);

                foreach (var blockDefId in blockTable)
                {
                    var blockDef = tr.GetObject<BlockTableRecord>(blockDefId, OpenMode.ForRead, false);
                    if (blockDef == null ||
                        blockDef.IsFromExternalReference ||
                        blockDef.IsLayout ||
                        blockDef.IsAnonymous ||
                        blockDef.Name.Contains("|"))
                    {
                        continue;
                    }
                    if (blockDef.Name == BlockName)
                    {
                        ImageSource = blockImage.CreateWpf(blockDef, true);
                    }
                }
            }
        }

        /// <summary>
        /// Handle Rotation in Insertion point option
        /// </summary>
        public PromptStatus Rotation(InsertionPointOpts optInsertionPt, bool isPreview)
        {
            var angBase = SystemVariable.GetAngle("ANGBASE");
            var message = isPreview ? ClassicInsertRes.UI_PreviewRotation : ClassicInsertRes.UI_Rotation;
            var optAngle = new PromptAngleOptions($"\n{message}")
            {
                DefaultValue = angBase
            };

            var resAngle = Util.Editor().GetAngle(optAngle);

            switch (resAngle.Status)
            {
                case PromptStatus.OK:
                    {
                        optInsertionPt.BlockReference.Rotation = resAngle.Value;
                        optInsertionPt.ResetVector();
                    }
                    return PromptStatus.OK;
                default:
                    return PromptStatus.Cancel;
            }
        }

        /// <summary>
        /// Handle Rotation Option
        /// </summary>
        private PromptStatus RotationUi(BlockReference blockReference, Vector3d? baseVector)
        {
            RotationOpts optRotation;
            var angBase = SystemVariable.GetAngle("ANGBASE");

            if (baseVector != null)
            {
                var scale = new Vector3d(baseVector.Value.X * blockReference.ScaleFactors.X,
                    baseVector.Value.Y * blockReference.ScaleFactors.Y,
                    baseVector.Value.Z * blockReference.ScaleFactors.Z);
                var baseVector1 = scale;
                var basePoint = blockReference.Position.Add(baseVector1);

                optRotation = new RotationOpts(blockReference, baseVector1, basePoint)
                {
                    Options = new JigPromptAngleOptions($"\n{ClassicInsertRes.UI_Rotation}")
                    {
                        BasePoint = basePoint,
                        UseBasePoint = true,
                        DefaultValue = angBase,
                        Cursor = CursorType.RubberBand,
                        UserInputControls = UserInputControls.NullResponseAccepted,
                    }
                };
            }
            else
            {
                optRotation = new RotationOpts(blockReference)
                {
                    Options = new JigPromptAngleOptions($"\n{ClassicInsertRes.UI_Rotation}")
                    {
                        BasePoint = blockReference.Position,
                        UseBasePoint = true,
                        DefaultValue = angBase,
                        Cursor = CursorType.RubberBand,
                        UserInputControls = UserInputControls.NullResponseAccepted,
                    }
                };
            }

            BitmapImageHelper.AddCursorBadge(ClassicInsertRes.IconRotationPath);
            var resRotation = (PromptDoubleResult)WindowUserInput.AcquireAngle(optRotation);
            BitmapImageHelper.RemoveCursorBadge();

            switch (resRotation.Status)
            {
                case PromptStatus.OK:
                    return PromptStatus.OK;
                case PromptStatus.Keyword:
                    break;
                default:
                    return PromptStatus.Cancel;
            }

            return PromptStatus.OK;
        }

        /// <summary>
        /// Show open file dialog
        /// </summary>
        public void ShowDialogOpenFile()
        {
            try
            {
                var filterString = $@"{ClassicInsertRes.DialogOpenFile_FilterString}";

                var openFileOpt = new PromptOpenFileOptions($"{ClassicInsertRes.DialogOpenFile_Browse}")
                {
                    DialogCaption = $"{ClassicInsertRes.DialogOpenFile_Caption}",
                    DialogName = $"{ClassicInsertRes.DialogOpenFile_Name}",
                    Filter = filterString,
                    AllowUrls = false,
                };

                var promptFileNameResult = Util.Editor().GetFileNameForOpen(openFileOpt);
                if (promptFileNameResult.Status != PromptStatus.OK)
                {
                    return;
                }
                TxbPath = promptFileNameResult.StringResult;
                BlockName = Path.GetFileNameWithoutExtension(TxbPath);
                ListBlock.Add(BlockName);
                OnPropertyChanged();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex.Message);
            }
        }

        /// <summary>
        /// Handle Scale Option
        /// </summary>
        /// XorYorZ 方向の尺度を指定 ＜1＞
        public PromptStatus Scale(BlockReference blockReference, ScaleType direct, bool isPreview)
        {
            var scaleFactors = blockReference.ScaleFactors;
            var message = string.Empty;
            if (isPreview)
            {
                switch (direct)
                {
                    case ScaleType.X:
                        message = $"\n{ClassicInsertRes.UI_PreviewScaleX}";
                        break;
                    case ScaleType.Y:
                        message = $"\n{ClassicInsertRes.UI_PreviewScaleY}";
                        break;
                    case ScaleType.Z:
                        message = $"\n{ClassicInsertRes.UI_PreviewScaleZ}";
                        break;
                }
            }
            else
            {
                switch (direct)
                {
                    case ScaleType.X:
                        message = $"\n{ClassicInsertRes.UI_ScaleX}";
                        break;
                    case ScaleType.Y:
                        message = $"\n{ClassicInsertRes.UI_ScaleY}";
                        break;
                    case ScaleType.Z:
                        message = $"\n{ClassicInsertRes.UI_ScaleZ}";
                        break;
                }
            }

            var optScale = new PromptDistanceOptions(message)
            {
                DefaultValue = 1,
                AllowZero = false,
            };

            var resScale = Util.Editor().GetDistance(optScale);
            if (resScale.Status != PromptStatus.OK)
            {
                return PromptStatus.Cancel;
            }
            switch (direct)
            {
                case ScaleType.X:
                    blockReference.ScaleFactors = new Scale3d(resScale.Value * blockReference.UnitFactor, scaleFactors.Y, scaleFactors.Z);
                    break;
                case ScaleType.Y:
                    blockReference.ScaleFactors = new Scale3d(scaleFactors.X, resScale.Value * blockReference.UnitFactor, scaleFactors.Z);
                    break;
                case ScaleType.Z:
                    blockReference.ScaleFactors = new Scale3d(scaleFactors.X, scaleFactors.Y, resScale.Value * blockReference.UnitFactor);
                    break;
            }
            return PromptStatus.OK;
        }

        /// <summary>
        /// XYZ 軸に対する尺度を指定 ＜1＞
        /// </summary>
        public PromptStatus ScaleS(BlockReference blockReference, bool isPreview)
        {
            var message = isPreview ? ClassicInsertRes.UI_PreviewScaleS : ClassicInsertRes.UI_ScaleXYZAxes;
            var optScaleS = new PromptDistanceOptions($"\n{message}")
            {
                DefaultValue = 1.0,
                AllowZero = false,
            };

            var resScaleS = Util.Editor().GetDistance(optScaleS);
            if (resScaleS.Status != PromptStatus.OK)
            {
                return PromptStatus.Cancel;
            }

            blockReference.ScaleFactors = new Scale3d(resScaleS.Value * blockReference.UnitFactor,
                resScaleS.Value * blockReference.UnitFactor, resScaleS.Value * blockReference.UnitFactor);

            return PromptStatus.OK;
        }

        /// <summary>
        /// Handle Scale Option
        /// </summary>
        private PromptStatus ScaleUi(BlockReference blockReference, Vector3d? baseVector)
        {
            Point3d basePointDefault;
            if (baseVector != null)
            {
                var baseVectorDefault = new Vector3d(baseVector.Value.X, baseVector.Value.Y, baseVector.Value.Z);
                basePointDefault = blockReference.Position.Add(baseVectorDefault);
            }
            else
            {
                basePointDefault = blockReference.Position;
            }
            if (UniformScaleActive)
            {
                ScaleOpts optScaleUniform;
                if (baseVector != null)
                {
                    var baseVector1 = new Vector3d(baseVector.Value.X, baseVector.Value.Y, baseVector.Value.Z);
                    var basePoint = blockReference.Position.Add(baseVector1);
                    optScaleUniform = new ScaleOpts(blockReference, baseVector1, basePoint)
                    {
                        Options = new JigPromptDistanceOptions($"\n{ClassicInsertRes.UI_ScaleUniform}")
                        {
                            BasePoint = basePoint,
                            UseBasePoint = true,
                            Cursor = CursorType.RubberBand,
                            DefaultValue = 1,
                            UserInputControls = UserInputControls.NullResponseAccepted |
                                                UserInputControls.UseBasePointElevation |
                                                UserInputControls.NoZeroResponseAccepted
                        }
                    };
                }
                else
                {
                    optScaleUniform = new ScaleOpts(blockReference)
                    {
                        Options = new JigPromptDistanceOptions($"\n{ClassicInsertRes.UI_ScaleUniform}")
                        {
                            BasePoint = blockReference.Position,
                            UseBasePoint = true,
                            Cursor = CursorType.RubberBand,
                            DefaultValue = 1,
                            UserInputControls = UserInputControls.NullResponseAccepted |
                                                UserInputControls.UseBasePointElevation |
                                                UserInputControls.NoZeroResponseAccepted,
                        }
                    };
                }

                Util.Editor().PromptedForPoint += optScaleUniform.UpdateScale;
                BitmapImageHelper.AddCursorBadge(ClassicInsertRes.IconCornerPath);
                var resScaleUniform = WindowUserInput.AcquireDistance(optScaleUniform);
                BitmapImageHelper.RemoveCursorBadge();
                Util.Editor().PromptedForPoint -= optScaleUniform.UpdateScale;

                if (optScaleUniform.UserStatus.Status == PromptStatus.Cancel)
                {
                    return PromptStatus.Cancel;
                }

                switch (resScaleUniform.Status)
                {
                    case PromptStatus.OK:
                    case PromptStatus.Keyword:
                        break;
                    default:
                        return PromptStatus.Cancel;
                }
            }
            else
            {
                ScaleCornerOpts optScaleCorner;
                if (baseVector != null)
                {
                    var baseVector1 = new Vector3d(baseVector.Value.X, baseVector.Value.Y, baseVector.Value.Z);
                    var basePoint = blockReference.Position.Add(baseVector1);

                    optScaleCorner = new ScaleCornerOpts(blockReference, baseVector1, basePoint)
                    {
                        Options = new JigPromptStringOptions($"\n{ClassicInsertRes.UI_ScaleCornerX}")
                        {
                            DefaultValue = "1",
                            AppendKeywordsToMessage = false,
                            UserInputControls = UserInputControls.Accept3dCoordinates |
                                                UserInputControls.NoZeroResponseAccepted |
                                                UserInputControls.NullResponseAccepted,
                            Keywords =
                            {
                                {"XYZ", $"{ClassicInsertRes.XYZ_Key}"},
                            }
                        }
                    };
                }
                else
                {
                    var baseVectorTemp = new Vector3d();
                    optScaleCorner = new ScaleCornerOpts(blockReference, baseVectorTemp)
                    {
                        Options = new JigPromptStringOptions($"\n{ClassicInsertRes.UI_ScaleCornerX}")
                        {
                            DefaultValue = "1",
                            AppendKeywordsToMessage = false,
                            UserInputControls = UserInputControls.Accept3dCoordinates |
                                                UserInputControls.NoZeroResponseAccepted |
                                                UserInputControls.NullResponseAccepted,
                            Keywords =
                            {
                                {"XYZ", $"{ClassicInsertRes.XYZ_Key}"},
                            },
                        },
                    };
                }

                if (TypographicScale(optScaleCorner, blockReference, baseVector, basePointDefault) == PromptStatus.Cancel)
                {
                    return PromptStatus.Cancel;
                }
            }
            return PromptStatus.OK;
        }

        /// <summary>
        /// Typographic scale processing
        /// </summary>
        private PromptStatus TypographicScale(ScaleCornerOpts optScaleCorner, BlockReference blockReference, Vector3d? baseVector, Point3d basePointDefault)
        {
            while (true)
            {
                Util.Editor().PromptedForPoint += optScaleCorner.UpdateScale;
                BitmapImageHelper.AddCursorBadge(ClassicInsertRes.IconCornerPath);
                var resScale = WindowUserInput.AcquireString(optScaleCorner);
                BitmapImageHelper.RemoveCursorBadge();
                Util.Editor().PromptedForPoint -= optScaleCorner.UpdateScale;

                if (optScaleCorner.UserStatus == PromptStatus.Cancel)
                {
                    return PromptStatus.Cancel;
                }

                if (resScale.StringResult == "0")
                {
                    new ClassicInsertCmd().WriteMessage(string.Format(ClassicInsertRes.NonzeroMessage) + "\n");
                    continue;
                }
                if (resScale.StringResult.ToUpper() != "X" && resScale.StringResult.ToUpper() != "XYZ")
                {
                    if (resScale.StringResult != "\n" && !resScale.StringResult.Contains(","))
                    {
                        var optScaleY = new PromptDoubleOptions($"\n{ClassicInsertRes.UI_ScaleY_DefaultStringValue}")
                        {
                            AllowNone = true,
                            AllowNegative = true,
                            AllowZero = false,
                        };
                        var resScaleY = Util.Editor().GetDouble(optScaleY);

                        switch (resScaleY.Status)
                        {
                            case PromptStatus.OK:
                            case PromptStatus.None:
                                var scaleX = (resScale.StringResult == "") ? 1 : double.Parse(resScale.StringResult);
                                var scaleY = (resScaleY.Value == 0) ? scaleX : resScaleY.Value;
                                optScaleCorner.SetScale(scaleX, scaleY, scaleX);
                                if (baseVector != null)
                                {
                                    var baseVectorScale = new Vector3d(baseVector.Value.X * scaleX,
                                                                    baseVector.Value.Y * scaleY,
                                                                    baseVector.Value.Z * scaleX);
                                    var vector = -baseVectorScale.RotateBy(-blockReference.Rotation, Vector3d.ZAxis);
                                    blockReference.Position = basePointDefault.Add(vector);
                                }
                                break;
                            default:
                                return PromptStatus.Cancel;
                        }
                    }
                    break;
                }
                if (resScale.StringResult.ToUpper() == "X" || resScale.StringResult.ToUpper() == "XYZ")
                {
                    var optScaleX = new PromptDoubleOptions($"\n{ClassicInsertRes.UI_ScaleX}")
                    {
                        DefaultValue = 1,
                        AllowNegative = true,
                        AllowZero = false,
                        AllowNone = false,
                    };
                    var resScaleX = Util.Editor().GetDouble(optScaleX);
                    if (resScaleX.Status != PromptStatus.OK)
                    {
                        return PromptStatus.Cancel;
                    }
                    var optScaleY = new PromptDoubleOptions($"\n{ClassicInsertRes.UI_ScaleY_DefaultStringValue}")
                    {
                        AllowNone = true,
                        AllowNegative = true,
                        AllowZero = false,
                    };
                    var resScaleY = Util.Editor().GetDouble(optScaleY);

                    switch (resScaleY.Status)
                    {
                        case PromptStatus.OK:
                        case PromptStatus.None:
                            var optScaleZ = new PromptDoubleOptions(string.Format(ClassicInsertRes.UI_ScaleZ_DefaultStringValue))
                            {
                                AllowNone = true,
                                AllowNegative = true,
                                AllowZero = false,
                            };
                            var resScaleZ = Util.Editor().GetDouble(optScaleZ);

                            switch (resScaleZ.Status)
                            {
                                case PromptStatus.OK:
                                case PromptStatus.None:
                                    var scaleX = (resScaleX.Value == 0) ? 1 : resScaleX.Value;
                                    var scaleY = (resScaleY.Value == 0) ? scaleX : resScaleY.Value;
                                    var scaleZ = (resScaleZ.Value == 0) ? scaleX : resScaleZ.Value;
                                    optScaleCorner.SetScale(scaleX, scaleY, scaleZ);
                                    if (baseVector != null)
                                    {
                                        var baseVectorScale = new Vector3d(baseVector.Value.X * scaleX,
                                                                        baseVector.Value.Y * scaleY,
                                                                        baseVector.Value.Z * scaleZ);
                                        var vector = -baseVectorScale.RotateBy(-blockReference.Rotation, Vector3d.ZAxis);
                                        blockReference.Position = basePointDefault.Add(vector);
                                    }
                                    break;
                                default:
                                    return PromptStatus.Cancel;
                            }
                            break;
                        default:
                            return PromptStatus.Cancel;
                    }

                    break;
                }
            }
            return PromptStatus.OK;
        }
        /// <summary>
        /// Check if block is in Database
        /// </summary>
        private bool ValidateIsDwgFileInDatabase(string name, string path)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(path))
            {
                return false;
            }
            using (var tr = Util.StartTransaction())
            {
                var blockTable = tr.GetObject<BlockTable>(Util.Database().BlockTableId, OpenMode.ForRead);
                foreach (var blockDefId in blockTable)
                {
                    var blockDef = tr.GetObject<BlockTableRecord>(blockDefId, OpenMode.ForRead, false);
                    if (blockDef.Name == name && blockDef.PathName != path)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Check if block is in current Drawing
        /// </summary>
        private int ValidateIsDwgFileInDrawing(string name, string path)
        {
            var count = 0;
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(path))
            {
                return 0;
            }
            using (var db = Util.Database())
            using (var tr = Util.StartTransaction())
            {
                var bt = tr.GetObject<BlockTable>(db.BlockTableId, OpenMode.ForRead);
                var ms = tr.GetObject<BlockTableRecord>(bt[BlockTableRecord.ModelSpace], OpenMode.ForRead);
                foreach (var id in ms)
                {
                    var ent = tr.GetObject<Entity>(id, OpenMode.ForRead);
                    if (ent.GetType().Name != "BlockReference")
                    {
                        continue;
                    }
                    if (name == ((BlockReference)ent).Name)
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        /// <summary>
        /// Load Dialog
        /// </summary>
        public void WindowLoaded(CustomWindow currentWindow)
        {
            try
            {
                _ = FindChildrenHelper.FindChildrenByName<System.Windows.Controls.ComboBox>(currentWindow, "cmbName")?.Focus();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }
    }
}