﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
#endif

using JrxCad.Utility;
using Exception = System.Exception;

namespace JrxCad.Command.ClassicInsert
{
    public class DDInsertCmd : BaseCommand
    {
#if DEBUG
        [CommandMethod("JrxCommand", "DD1", CommandFlags.Modal)]
#else
        [CommandMethod("JrxCommand", "SmxDDINSERT", CommandFlags.Modal)]
#endif
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }
                var ddInsert = new ClassicInsertCmd();
                ddInsert.OnCommand();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }
    }
}
