﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;

#endif
using JrxCad.Utility;
using System;

namespace JrxCad.Command.ClassicInsert
{
    public partial class ClassicInsertCmd
    {
        public static IWindowInput WindowUserInput;

        /// <summary>
        /// IWindowInput Interface
        /// </summary>
        public interface IWindowInput : IUserInputBase
        {
            bool ClassicInsertShowDialog();
        }

        /// <summary>
        /// WindowInput
        /// </summary>
        private class WindowInput : UserInputBase, IWindowInput
        {
            public bool ClassicInsertShowDialog()
            {
                var form = new WClassicInsert();
                var boolForm = form.ShowDialog();
                return boolForm.Value;
            }
        }

        public class InsertionPointOpts : PointDrawJigN.JigActions
        {
            public BlockReference BlockReference;
            public Vector3d BaseVector;
            private readonly double _prevAngle;

            public InsertionPointOpts(BlockReference blockReference, Vector3d baseVector)
            {
                BlockReference = blockReference;
                BaseVector = baseVector;
                _prevAngle = blockReference.Rotation;
            }

            public override JigPromptPointOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                geometry.Draw(BlockReference);
                return true;
            }

            public override void OnUpdate()
            {
                var scale = new Vector3d(-BaseVector.X * BlockReference.ScaleFactors.X, -BaseVector.Y * BlockReference.ScaleFactors.Y, 0.0);
                BlockReference.Position = LastValue.Add(scale);
            }

            public void ResetVector()
            {
                if (MathEx.IsEqual(_prevAngle, BlockReference.Rotation)) return;
                var angle = BlockReference.Rotation - _prevAngle;
                var prevVector = BaseVector;
                BaseVector = prevVector.RotateBy(angle, Vector3d.ZAxis);
            }
        }
        public class RotationOpts : AngleDrawJigN.JigActions
        {
            private readonly BlockReference _blockReference;
            private Vector3d _baseVector;
            private Point3d _basePoint;

            public RotationOpts(BlockReference blockReference)
            {
                _blockReference = blockReference;
            }

            public RotationOpts(BlockReference blockReference, Vector3d baseVector, Point3d basePoint)
            {
                _blockReference = blockReference;
                _baseVector = baseVector;
                _basePoint = basePoint;
            }

            public override JigPromptAngleOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                geometry.Draw(_blockReference);
                return true;
            }

            public override void OnUpdate()
            {
                _blockReference.Rotation = LastValue;
                var vector = -_baseVector.RotateBy(_blockReference.Rotation, Vector3d.ZAxis);
                _blockReference.Position = _basePoint.Add(vector);
            }
        }

        public class ScaleOpts : DistanceDrawJigN.JigActions
        {
            private readonly BlockReference _blockReference;
            private Vector3d _baseVector;
            private Point3d _basePoint;
            public PromptPointResult UserStatus;
            public Vector3d Scale;
            private Point3d _tempPoint = new Point3d();

            public ScaleOpts(BlockReference blockReference, Vector3d baseVector, Point3d basePoint)
            {
                _blockReference = blockReference;
                _baseVector = baseVector;
                _basePoint = basePoint;
            }

            public ScaleOpts(BlockReference blockReference)
            {
                _blockReference = blockReference;
            }

            public override JigPromptDistanceOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                geometry.Draw(_blockReference);
                return true;
            }

            public override void OnUpdate()
            {
            }

            public void UpdateScale(object sender, PromptPointResultEventArgs e)
            {
                var pt = e.Result.Value;
                UserStatus = e.Result;
                int caseFlip = 0;

                if (pt.X != 0 && pt.Y != 0 && UserStatus.Status != PromptStatus.Keyword)
                {
                    _tempPoint = pt;
                }

                var dist = pt.DistanceTo(_basePoint);

                if (LastValue == 0 && dist != 0)
                {
                    LastValue = dist;
                }

                if (UserStatus.Status == PromptStatus.Keyword)
                {
                    var stringResult = UserStatus.StringResult;
                    double value = 0;
                    var isNumeric = double.TryParse(stringResult, out double result);

                    if (isNumeric || stringResult == "")
                    {
                        value = (stringResult == "") ? 1 : result;

                        LastValue = value * _blockReference.UnitFactor;

                        if (value < 0)
                        {
                            caseFlip = 1;
                        }
                        else
                        {
                            caseFlip = 2;
                        }
                    }
                    else
                    {
                        new ClassicInsertCmd().WriteMessage(string.Format(ClassicInsertRes.NonzeroMessage) + "\n");
                    }   
                }

                switch(caseFlip)
                {
                    //use 0 to UI
                    case 0:
                        if (_tempPoint.X < _basePoint.X && _tempPoint.Y < _basePoint.Y)
                        {
                            _blockReference.TransformBy(Matrix3d.Mirroring(_basePoint));
                        }
                        break;
                    case 1:
                        _blockReference.Rotation = Math.PI;
                        _blockReference.TransformBy(Matrix3d.Mirroring(_basePoint));
                        break;
                }

                _blockReference.ScaleFactors = new Scale3d(LastValue);

                var baseVector1 = new Vector3d(_baseVector.X * LastValue,
                    _baseVector.Y * LastValue,
                    _baseVector.Z * LastValue);

                if (caseFlip!=0)
                {
                    LastValue = LastValue / _blockReference.UnitFactor;
                    baseVector1 = new Vector3d(_baseVector.X * LastValue,
                    _baseVector.Y * LastValue,
                    _baseVector.Z * LastValue);
                }

                // use for userinput
                switch (caseFlip)
                {
                    case 1:
                        _blockReference.TransformBy(Matrix3d.Mirroring(_basePoint));
                        break;
                    case 2:
                        _blockReference.Rotation = 0;
                        break;
                    default:
                        // must have to keep blockReference position after update call continuous
                        if (_tempPoint.X < _basePoint.X && _tempPoint.Y < _basePoint.Y)
                        {
                            _blockReference.Rotation = Math.PI;
                            _blockReference.TransformBy(Matrix3d.Mirroring(_basePoint));
                        }
                        break;
                }

                var vector = -baseVector1.RotateBy(-_blockReference.Rotation, Vector3d.ZAxis);
                _blockReference.Position = _basePoint.Add(baseVector1);
            }
        }
        public class ScaleCornerOpts : StringDrawJigN.JigActions
        {
            private readonly BlockReference _blockReference;
            private Vector3d _baseVector;
            public Vector3d Scale;
            private Point3d _basePoint;
            public PromptStatus UserStatus;

            public ScaleCornerOpts(BlockReference blockReference, Vector3d baseVector)
            {
                _blockReference = blockReference;
                _baseVector = baseVector.RotateBy(-blockReference.Rotation, Vector3d.ZAxis);
                _basePoint = blockReference.Position.Add(baseVector);
            }

            public ScaleCornerOpts(BlockReference blockReference, Vector3d baseVector, Point3d basePoint)
            {
                _blockReference = blockReference;
                _baseVector = baseVector.RotateBy(-blockReference.Rotation, Vector3d.ZAxis);
                _basePoint = basePoint;
            }

            public override JigPromptStringOptions Options { get; set; }

            public override void UpdateString(string str)
            {
            }

            public override bool Draw(WorldGeometry geometry)
            {
                geometry.Draw(_blockReference);
                return true;
            }

            public override void OnUpdate()
            {
            }

            public void UpdateScale(object sender, PromptPointResultEventArgs e)
            {
                var pt = e.Result.Value;
                UserStatus = e.Result.Status;

                Scale = pt - _basePoint;
                var x = (Scale.X == 0) ? _blockReference.UnitFactor : Scale.X;
                var y = (Scale.Y == 0) ? _blockReference.UnitFactor : Scale.Y;
                _blockReference.ScaleFactors = new Scale3d(x, y, Math.Abs(x));

                var baseVectorScale = new Vector3d(_baseVector.X * x,
                                                 _baseVector.Y * y,
                                                 _baseVector.Z * x);
                var vector = -baseVectorScale.RotateBy(-_blockReference.Rotation, Vector3d.ZAxis);
                _blockReference.Position = _basePoint.Add(vector);
            }

            public void SetScale(double v1, double v2, double v3)
            {
                var scale = new Scale3d(v1 * _blockReference.UnitFactor,
                                        v2 * _blockReference.UnitFactor,
                                        v3 * _blockReference.UnitFactor);
                _blockReference.ScaleFactors = scale;
            }
        }

        public class BasePointOpts : PointDrawJigN.JigActions
        {
            private readonly BlockReference _blockReference;

            public BasePointOpts(BlockReference blockReference)
            {
                _blockReference = blockReference;
            }

            public override JigPromptPointOptions Options { get; set; }

            public override bool Draw(WorldGeometry geometry)
            {
                if (geometry != null)
                {
                    using (var drawEnt = (BlockReference)_blockReference.Clone())
                    {
                        geometry.Draw(drawEnt);
                    }
                }

                return true;
            }

            public override void OnUpdate()
            {
            }
        }
    }
}
