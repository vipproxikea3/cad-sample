﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
#endif

using JrxCad.Utility;
using Exception = System.Exception;
using JrxCad.Command.MinusInsert;

namespace JrxCad.Command.ClassicInsert
{
    public class MinusDDInsert : BaseCommand
    {
#if DEBUG
        [CommandMethod("JrxCommand", "-DD1", CommandFlags.Modal)]
#else
        [CommandMethod("JrxCommand", "-SmxDDINSERT", CommandFlags.Modal)]
#endif
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }
                var minusDdInsert = new MinusInsertCmd();
                minusDdInsert.ExcuteCommand();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }
    }
}
