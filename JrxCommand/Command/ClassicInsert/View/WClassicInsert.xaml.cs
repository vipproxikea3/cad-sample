﻿using JrxCad.Command.ClassicInsert.ViewModel;
using JrxCad.View.CustomWpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using JrxCad.Command.MinusInsert;
using JrxCad.Helpers;
using JrxCad.Utility;

namespace JrxCad.Command.ClassicInsert
{
    /// <summary>
    /// Interaction logic for WClassicInsert.xaml
    /// </summary>
    public partial class WClassicInsert : CustomWindow
    {
        public WClassicInsert()
        {
            InitializeComponent();
            DataContext = new ClassicInsertViewModel();
        }

        /// <summary>
        /// CommandAction
        /// </summary>
        private void CommandAction(Action<ClassicInsertViewModel> callback, bool dialogAttack)
        {
            try
            {
                if (!(DataContext is ClassicInsertViewModel model))
                {
                    return;
                }
                if (dialogAttack)
                {
                    Hide();
                    callback(model);
                    _ = ShowDialog();
                }
                else
                {
                    callback(model);
                }
            }
            catch (Exception ex)
            {
                MsgBox.Error.Show(ex.Message);
            }
        }

        #region Common event
        /// <summary>
        /// Close form
        /// </summary>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Execute Write Block
        /// </summary>
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.ButtonOk(this), false);
        }
        /// <summary>
        /// Show dialog path
        /// </summary>
        private void BtnPath_Click(object sender, RoutedEventArgs e)
        {
            btnOK.Focus();
            CommandAction((model) => model.ShowDialogOpenFile(), true);
        }
        /// <summary>
        /// Open help window
        /// </summary>
        private void BtnHelp_Click(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.OpenHelpWindow(), false);
        }
        /// <summary>
        /// Window loaded
        /// </summary>
        private void ClassicInsertWindow_Loaded(object sender, RoutedEventArgs e)
        {
            CommandAction((model) => model.WindowLoaded(this), false);
        }
        private void ClassicInsertWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            CommandAction((model) => model.Resized(this), false);
        }
        #endregion
    }
}
