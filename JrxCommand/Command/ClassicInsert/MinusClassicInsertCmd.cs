﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
#endif

using JrxCad.Utility;
using Exception = System.Exception;
using JrxCad.Command.MinusInsert;

namespace JrxCad.Command.ClassicInsert
{
    public class MinusClassicInsert : BaseCommand
    {
#if DEBUG
        [CommandMethod("JrxCommand", "-C1", CommandFlags.Modal)]
#else
        [CommandMethod("JrxCommand", "-SmxCLASSICINSERT", CommandFlags.Modal)]
#endif
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }
                var minusClassicInsert = new MinusInsertCmd();
                minusClassicInsert.ExcuteCommand();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }
    }
}
