﻿#if _IJCAD_
using Exception = System.Exception;
using GrxCAD.Runtime;
using GrxCAD.EditorInput;

#elif _AutoCAD_
using Exception = System.Exception;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;

#endif
using JrxCad.Utility;

namespace JrxCad.Command.Elev
{
    public partial class ElevCmd : BaseCommand
    {
        public ElevCmd() : this(new UserInput())
        {
        }
        public ElevCmd(IElevUserInput userInput)
        {
            _userInput = userInput;
        }

        [CommandMethod("JrxCommand", "SmxElev", CommandFlags.Transparent | CommandFlags.NoNewStack)]
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }

                var optElevation = new PromptDistanceOptions($"\n{ElevRes.UI_P1}")
                {
                    AllowNegative = true,
                    AllowZero = true,
                    DefaultValue = SystemVariable.GetDouble("ELEVATION"),
                };
                var resElevation = _userInput.GetDistance(optElevation);
                if (resElevation.Status != PromptStatus.OK)
                {
                    return;
                }

                var optThickness = new PromptDistanceOptions($"\n{ElevRes.UI_P2}")
                {
                    AllowNegative = true,
                    AllowZero = true,
                    DefaultValue = SystemVariable.GetDouble("THICKNESS"),
                };
                var resThickness = _userInput.GetDistance(optThickness);
                if (resThickness.Status != PromptStatus.OK)
                {
                    return;
                }

                SystemVariable.SetValue("ELEVATION", resElevation.Value);
                SystemVariable.SetValue("THICKNESS", resThickness.Value);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }
    }
}
