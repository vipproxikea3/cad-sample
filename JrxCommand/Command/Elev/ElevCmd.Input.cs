﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;

#elif _AutoCAD_

using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.GraphicsInterface;

#endif
using JrxCad.Utility;

namespace JrxCad.Command.Elev
{
    public partial class ElevCmd
    {
        private readonly IElevUserInput _userInput;
        public interface IElevUserInput : IUserInputBase
        {
        }
        private class UserInput : UserInputBase, IElevUserInput
        {
        }
    }
}
