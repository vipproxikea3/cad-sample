﻿#if _IJCAD_
using CADApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.Runtime;
using GrxCAD.Colors;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;

#elif _AutoCAD_
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

#endif

using Exception = System.Exception;
using JrxCad.Utility;
using JrxCad.FileIO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using JrxCad.Helpers;

namespace JrxCad.Command
{
    public class ChpropCmd : BaseCommand
    {
        [CommandMethod("JrxCommand", "SmxJrxCadChprop", CommandFlags.UsePickSet)]
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }

                // PickSet
                var res1 = Util.Editor().SelectImplied();
                if (res1.Status != PromptStatus.OK)
                {
                    // オブジェクトを選択
                    var opt1 = new PromptSelectionOptions
                    {
                        MessageForAdding = $"\nオブジェクトを選択"
                    };
                    res1 = Util.Editor().GetSelection(opt1);
                    if (res1.Status != PromptStatus.OK) return;
                }

                ChangeProperty(res1.Value);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        /// <summary>
        /// プロパティ変更
        /// </summary>
        protected void ChangeProperty(SelectionSet ss)
        {
            try
            {
                // 変更するプロパティ(一度変更するとコマンド終了まで保持するためwhileの外に出している。)
                Color newColor = null;
                double? newElevation = null;
                (ObjectId, string)? newLayer = null;
                (ObjectId, string)? newLineType = null;
                double? newLineTypeScale = null;
                LineWeight? newLineWeight = null;
                double? newThickness = null;
                Transparency? newTransparency = null;
                AnnotativeStates? newAnnotativeStates = null;

                while (true)
                {
                    // 変更するプロパティを入力 [色(C)/画層(LA)/線種(LT)/線種尺度(S)/線の太さ(LW)/厚さ(T)/透過性(TR)/マテリアル(M)/印刷スタイル(PL)/異尺度対応(A)]
                    var opt2 = new PromptKeywordOptions($"\n変更するプロパティを入力")
                    {
                        AllowNone = true,
                        AppendKeywordsToMessage = true
                    };
                    opt2.Keywords.Add("C", "C", "色(C)");
                    // TODO: ChangeCmd対応後にコメントを外す
                    //if (GetType() == typeof(ChangeCmd))
                    //{
                    //    opt2.Keywords.Add("E", "E", "高度(E)");
                    //}
                    opt2.Keywords.Add("LA", "LA", "画層(LA)");
                    opt2.Keywords.Add("LT", "LT", "線種(LT)");
                    opt2.Keywords.Add("S", "S", "線種尺度(S)");
                    opt2.Keywords.Add("LW", "LW", "線の太さ(LW)");
                    opt2.Keywords.Add("T", "T", "厚さ(T)");
                    opt2.Keywords.Add("TR", "TR", "透過性(TR)");
                    opt2.Keywords.Add("M", "M", "マテリアル(M)");
                    if (!SystemVariable.GetBool("PSTYLEMODE"))
                    {
                        opt2.Keywords.Add("PL", "PL", "印刷スタイル(PL)");
                    }
                    opt2.Keywords.Add("A", "A", "異尺度対応(A)");
                    var res2 = Util.Editor().GetKeywords(opt2);
                    if (res2.Status == PromptStatus.None)
                    {
                        UpdateAll(ss,
                            newColor, newElevation, newLayer, newLineType, newLineTypeScale,
                            newLineWeight, newThickness, newTransparency, newAnnotativeStates);
                        return;
                    }

                    if (res2.Status != PromptStatus.OK) return;

                    switch (res2.StringResult)
                    {
                        // 色(C)
                        case "C":
                            if (!KeyC(ss, ref newColor)) return;
                            break;
                        // 色(E)
                        case "E":
                            if (!KeyE(ss, ref newElevation)) return;
                            break;
                        // 画層(LA)
                        case "LA":
                            if (!KeyLA(ss, ref newLayer)) return;
                            break;
                        // 線種(LT)
                        case "LT":
                            if (!KeyLT(ss, ref newLineType)) return;
                            break;
                        // 線種尺度(S)
                        case "S":
                            if (!KeyS(ss, ref newLineTypeScale)) return;
                            break;
                        // 線の太さ(LW)
                        case "LW":
                            if (!KeyLW(ss, ref newLineWeight)) return;
                            break;
                        // 厚さ(T)
                        case "T":
                            if (!KeyT(ss, ref newThickness)) return;
                            break;
                        // 透過性(TR)
                        case "TR":
                            if (!KeyTR(ss, ref newTransparency)) return;
                            break;
                        // マテリアル(M)
                        case "M":
                            // TODO: 優先度Aでは対応しない
                            break;
                        // 印刷スタイル(PL)
                        case "PL":
                            // TODO: 優先度Aでは対応しない
                            break;
                        // 異尺度対応(A)
                        case "A":
                            if (!KeyA(ss, ref newAnnotativeStates)) return;
                            break;
                        default:
                            return;
                    }
                }
            }
            finally
            {
                _colorBookFile = null;
            }
        }

        /// <summary>
        /// 全て更新
        /// </summary>
        private void UpdateAll(SelectionSet ss,
            Color newColor, double? newElevation, (ObjectId, string)? newLayer, (ObjectId, string)? newLineType, double? newLineTypeScale,
            LineWeight? newLineWeight, double? newThickness, Transparency? newTransparency, AnnotativeStates? newAnnotativeStates)
        {
            using (var tr = Util.StartTransaction())
            {
                var messages = new List<string>();
                foreach (SelectedObject selectedObject in ss)
                {
                    using (var obj = tr.GetObject(selectedObject.ObjectId, OpenMode.ForWrite))
                    {
                        if (obj is Entity ent)
                        {
                            UpdateC(ent, newColor);
                            UpdateE(ent, newElevation, messages);
                            UpdateLA(ent, newLayer);
                            UpdateLT(ent, newLineType);
                            UpdateS(ent, newLineTypeScale);
                            UpdateLW(ent, newLineWeight);
                            UpdateT(ent, newThickness, messages);
                            UpdateTR(ent, newTransparency);
                            //UpdateM(ent);
                            //UpdatePL(ent);
                            UpdateA(ent, newAnnotativeStates);
                        }
                    }
                }

                foreach (var message in messages.Distinct())
                {
                    Util.Editor().WriteMessage(message);
                }

                tr.Commit();
            }
        }

        #region 色(C)

        /// <summary>
        /// 色(C)
        /// </summary>
        private bool KeyC(SelectionSet ss, ref Color newColor)
        {
            var colors = new List<Color>();
            if (newColor != null)
            {
                colors.Add(newColor);
            }
            else
            {
                using (var tr = Util.StartTransaction())
                {
                    foreach (SelectedObject selectedObject in ss)
                    {
                        using (var obj = tr.GetObject(selectedObject.ObjectId, OpenMode.ForRead))
                        {
                            if (obj is Entity ent)
                            {
                                colors.Add(ent.Color);
                            }
                        }
                    }
                }
            }

            if (colors.Count == 0) return false;

            // デフォルト値
            var values = colors.Select(a => a.ColorNameForDisplay).Distinct().ToList();
            var defVal = values.Count == 1 ? values[0] : "各種";

            // 新しい色 [TrueColor(T)/カラー ブック(CO)] <BYLAYER>
            var opt1 = new PromptKeywordOptions($"\n新しい色 <{defVal}>")
            {
                AllowNone = true,
                AppendKeywordsToMessage = true,
                Keywords =
                {
                    {"T", "T", "TrueColor(T)"},
                    {"CO", "CO", "カラー ブック(CO)"},
                },
            };
            var res1 = Util.Editor().GetKeywords(opt1);
            if (res1.Status == PromptStatus.None)
            {
                return true;
            }

            if (res1.Status != PromptStatus.OK) return false;

            switch (res1.StringResult)
            {
                case "T":
                    if (!KeyC_T(ref newColor)) return false;
                    break;
                case "CO":
                    if (!KeyC_CO(colors, ref newColor)) return false;
                    break;
            }


            return true;
        }

        private void UpdateC(Entity ent, Color newColor)
        {
            if (newColor == null) return;
            ent.Color = newColor;
        }

        private bool KeyC_T(ref Color newColor)
        {
            while (true)
            {
                // R,G,B 値
                var opt1 = new PromptStringOptions($"R,G,B 値");
                var res1 = Util.Editor().GetString(opt1);
                if (res1.Status != PromptStatus.OK) return false;

                if (!ColorEx.ParseRgb(res1.StringResult, out var r, out var g, out var b))
                {
                    Util.Editor().WriteMessage($"\n無効な色です。0 から 255 までの 3 つの数値をカンマで区切って入力してください。");
                    continue;
                }

                newColor = Color.FromRgb(r, g, b);
                break;
            }

            return true;
        }

        private ColorBookFile _colorBookFile;

        private bool KeyC_CO(List<Color> colors, ref Color newColor)
        {
            if (_colorBookFile == null)
            {
                _colorBookFile = new ColorBookFile();
                _colorBookFile.Read();
            }

            // デフォルト値
            // 現在カラーブックで設定されている場合、現在値
            // そうでない場合、最後の設定値
            var bookNames = colors.Select(a => a.BookName).Distinct().ToList();
            var bookDefVal = bookNames.Count == 1 ? bookNames[0] : newColor?.BookName ?? "";

            var colorNames = colors.Select(a => a.ColorName).Distinct().ToList();
            var colorDefVal = colorNames.Count == 1 ? colorNames[0] : newColor?.ColorName ?? "";

            while (true)
            {
                // カラー ブック名を入力
                var opt1 = new PromptStringOptions($"カラー ブック名を入力");
                if (!string.IsNullOrEmpty(bookDefVal))
                {
                    opt1.DefaultValue = bookDefVal;
                }
                var res1 = Util.Editor().GetString(opt1);
                if (res1.Status != PromptStatus.OK) return false;

                var bookName0 = Regex.Replace(res1.StringResult, @"\s", "");
                var book = _colorBookFile.BookMap.FirstOrDefault(
                    a => Regex.Replace(a.Key, @"\s", "").Contains(bookName0));

                if (book.Key == null)
                {
                    Util.Editor().WriteMessage($"\nカラー ブックが見つかりません。");
                    continue;
                }

                // 色名を入力
                var opt2 = new PromptStringOptions($"色名を入力");
                if (!string.IsNullOrEmpty(colorDefVal))
                {
                    opt2.DefaultValue = colorDefVal;
                }
                var res2 = Util.Editor().GetString(opt2);
                if (res2.Status != PromptStatus.OK) return false;

                var colorName0 = Regex.Replace(res2.StringResult, @"\s", "");
                string colorName = null;
                foreach (var colorPage in book.Value)
                {
                    colorName = colorPage.ColorNames.FirstOrDefault(
                        a => Regex.Replace(a, @"\s", "").Contains(colorName0));
                    if (colorName != null)
                    {
                        break;
                    }
                }

                if (colorName == null)
                {
                    Util.Editor().WriteMessage($"\n色が見つかりません。");
                    continue;
                }

                newColor = Color.FromNames(colorName, book.Key);
                break;
            }

            return true;
        }

        #endregion

        #region 高度(E)

        /// <summary>
        /// 高度(E)
        /// </summary>
        private bool KeyE(SelectionSet ss, ref double? newElevation)
        {
            var elevations = new List<double>();
            if (newElevation != null)
            {
                elevations.Add(newElevation.Value);
            }
            else
            {
                using (var tr = Util.StartTransaction())
                {
                    foreach (SelectedObject selectedObject in ss)
                    {
                        using (var obj = tr.GetObject(selectedObject.ObjectId, OpenMode.ForRead))
                        {
                            var ucsToWcs = Util.Editor().CurrentUserCoordinateSystem;
                            switch (obj)
                            {
                                case Arc arc:
                                    elevations.Add(arc.Center.GetElevation(ucsToWcs));
                                    break;
                                case Circle circle:
                                    elevations.Add(circle.Center.GetElevation(ucsToWcs));
                                    break;
                                case Ellipse ellipse:
                                    elevations.Add(ellipse.Center.GetElevation(ucsToWcs));
                                    break;
                                case DBPoint point:
                                    elevations.Add(point.Position.GetElevation(ucsToWcs));
                                    break;
                                case DBText text:
                                    elevations.Add(text.Position.GetElevation(ucsToWcs));
                                    break;
                                case MText mText:
                                    elevations.Add(mText.Location.GetElevation(ucsToWcs));
                                    break;
                                case Line line:
                                    elevations.Add(line.StartPoint.GetElevation(ucsToWcs));
                                    elevations.Add(line.EndPoint.GetElevation(ucsToWcs));
                                    break;
                                case Polyline polyline:
                                    elevations.Add(polyline.Elevation);
                                    break;
                                case Polyline2d polyline2d:
                                    elevations.Add(polyline2d.Elevation);
                                    break;
                                case Shape shape:
                                    elevations.Add(shape.Position.GetElevation(ucsToWcs));
                                    break;
                                case Solid solid:
                                    elevations.Add(solid.GetPointAt(0).GetElevation(ucsToWcs));
                                    break;
                                case Trace trace:
                                    elevations.Add(trace.GetPointAt(0).GetElevation(ucsToWcs));
                                    break;
                                // TODO: Splineは、UCS判定方法不明。(Normalなし、IsPlanerあり)
                                case Spline spline:
                                    if (spline is Helix)
                                    {
                                        elevations.Add(0.0);
                                    }
                                    else
                                    {
                                        for (var i = 0; i < spline.NumControlPoints; i++)
                                        {
                                            elevations.Add(spline.GetControlPointAt(i).GetElevation(ucsToWcs));
                                        }
                                        for (var i = 0; i < spline.NumFitPoints; i++)
                                        {
                                            elevations.Add(spline.GetFitPointAt(i).GetElevation(ucsToWcs));
                                        }
                                    }
                                    break;
                                case Xline xline:
                                    elevations.Add(xline.BasePoint.GetElevation(ucsToWcs));
                                    elevations.Add(xline.SecondPoint.GetElevation(ucsToWcs));
                                    break;
                                case Ray ray:
                                    elevations.Add(ray.BasePoint.GetElevation(ucsToWcs));
                                    elevations.Add(ray.SecondPoint.GetElevation(ucsToWcs));
                                    break;
                                case Dimension dim:
                                    elevations.Add(dim.Elevation);
                                    break;
                                case Hatch hatch:
                                    elevations.Add(hatch.Elevation);
                                    break;
                                case Leader leader:
                                    for (var i = 0; i < leader.NumVertices; i++)
                                    {
                                        elevations.Add(leader.VertexAt(i).GetElevation(ucsToWcs));
                                    }
                                    break;
                                // ブロック参照(Table,Array含む)
                                case BlockReference br:
                                    elevations.Add(br.Position.GetElevation(ucsToWcs));
                                    break;
                                case Region region:
                                    elevations.Add(region.GetElevation(ucsToWcs));
                                    break;
                                default:
                                    elevations.Add(0.0);
                                    break;
                            }
                        }
                    }
                }
            }

            if (elevations.Count == 0) return false;

            // デフォルト値
            var values = elevations.Distinct().ToList();
            var defVal = values.Count == 1 ? string.Format($"{(double)values[0]:F4}") : "各種";

            while (true)
            {
                // 新しい 高度 を指定 <0.0000>
                var opt1 = new PromptDistanceOptions($"\n新しい 高度 を指定 <{defVal}>")
                {
                    AllowNone = true,
                    AllowZero = false,
                    AllowNegative = true,
                };
                var res1 = Util.Editor().GetDistance(opt1);
                if (res1.Status == PromptStatus.None) return true;
                if (res1.Status != PromptStatus.OK) return false;

                newElevation = res1.Value;
                break;
            }

            return true;
        }

        private void UpdateE(Entity ent, double? newElevation, List<string> messages)
        {
            if (newElevation == null) return;

            var ucsToWcs = Util.Editor().CurrentUserCoordinateSystem;
            var elevation = 0.0;
            switch (ent)
            {
                case Arc arc:
                    elevation = arc.Center.GetElevation(ucsToWcs);
                    break;
                case Circle circle:
                    elevation = circle.Center.GetElevation(ucsToWcs);
                    break;
                case Ellipse ellipse:
                    elevation = ellipse.Center.GetElevation(ucsToWcs);
                    break;
                case DBPoint point:
                    elevation = point.Position.GetElevation(ucsToWcs);
                    break;
                case DBText text:
                    elevation = text.Position.GetElevation(ucsToWcs);
                    break;
                case MText mText:
                    elevation = mText.Location.GetElevation(ucsToWcs);
                    break;
                case Line line:
                    {
                        var elevationS = line.StartPoint.GetElevation(ucsToWcs);
                        var elevationE = line.EndPoint.GetElevation(ucsToWcs);
                        if (elevationS.IsEqual(elevationE))
                        {
                            elevation = elevationS;
                        }
                        else
                        {
                            messages.Add($"\nZ 座標値が異なるオブジェクトの高度は変更できません。");
                            return;
                        }
                        break;
                    }
                case Polyline polyline:
                    elevation = polyline.Elevation;
                    break;
                case Polyline2d polyline2d:
                    elevation = polyline2d.Elevation;
                    break;
                case Shape shape:
                    elevation = shape.Position.GetElevation(ucsToWcs);
                    break;
                case Solid solid:
                    elevation = solid.GetPointAt(0).GetElevation(ucsToWcs);
                    break;
                // Trace(太線)は、頂点数が不明。(そもそも古いオブジェクトかも。今は使われてなさそう。)
                //case Trace trace:
                //    break;
                // TODO: Splineは、IsPlaner=falseの場合の挙動が不明
                case Spline spline:
                    if (spline is Helix)
                    {
                        messages.Add($"\n{ent.GetType().Name.ToUpper()} の高度は変更できません。");
                        return;
                    }
                    else
                    {
                        var elevation0 = spline.GetControlPointAt(0).GetElevation(ucsToWcs);
                        for (var i = 1; i < spline.NumControlPoints; i++)
                        {
                            var elevationC = spline.GetControlPointAt(i).GetElevation(ucsToWcs);
                            if (!elevationC.IsEqual(elevation0))
                            {
                                messages.Add($"\nZ 制御点の座標値が異なるオブジェクトの高度は変更できません。");
                                return;
                            }
                        }
                        for (var i = 0; i < spline.NumFitPoints; i++)
                        {
                            var elevationF = spline.GetFitPointAt(i).GetElevation(ucsToWcs);
                            if (!elevationF.IsEqual(elevation0))
                            {
                                messages.Add($"\nZ フィット点の座標値が異なるオブジェクトの高度は変更できません。");
                                return;
                            }
                        }

                        elevation = elevation0;
                    }
                    break;
                case Xline xline:
                    elevation = xline.BasePoint.GetElevation(ucsToWcs);
                    break;
                case Ray ray:
                    elevation = ray.BasePoint.GetElevation(ucsToWcs);
                    break;
                case Dimension dim:
                    elevation = newElevation.Value;
                    break;
                case Hatch hatch:
                    elevation = newElevation.Value;
                    break;
                case Leader leader:
                    elevation = leader.VertexAt(0).GetElevation(ucsToWcs);
                    break;
                // ブロック参照(Table,Array含む)
                case BlockReference br:
                    elevation = br.Position.GetElevation(ucsToWcs);
                    break;
                case Region region:
                    elevation = region.GetElevation(ucsToWcs);
                    break;
                default:
                    messages.Add($"\n{ent.GetType().Name.ToUpper()} の高度は変更できません。");
                    return;
            }

            ent.TransformBy(Matrix3d.Displacement(Vector3d.ZAxis.TransformBy(ucsToWcs) * (newElevation.Value - elevation)));
        }

        #endregion

        #region 画層(LA)

        /// <summary>
        /// 画層(LA)
        /// </summary>
        private bool KeyLA(SelectionSet ss, ref (ObjectId, string)? newLayer)
        {
            var layers = new List<(ObjectId, string)>();
            if (newLayer != null)
            {
                layers.Add(newLayer.Value);
            }
            else
            {
                using (var tr = Util.StartTransaction())
                {
                    foreach (SelectedObject selectedObject in ss)
                    {
                        using (var obj = tr.GetObject(selectedObject.ObjectId, OpenMode.ForRead))
                        {
                            if (obj is Entity ent)
                            {
                                layers.Add((ent.LayerId, ent.Layer));
                            }
                        }
                    }
                }
            }

            if (layers.Count == 0) return false;

            // デフォルト値
            var values = layers.Distinct().ToList();
            var defVal = values.Count == 1 ? values[0].Item2 : "各種";

            while (true)
            {
                // 新しい画層名を入力 <0>
                var opt1 = new PromptStringOptions($"\n新しい画層名を入力")
                {
                    DefaultValue = defVal
                };
                var res1 = Util.Editor().GetString(opt1);
                if (res1.Status != PromptStatus.OK) return false;

                var layerName0 = res1.StringResult;
                if (layerName0 == defVal) return true;

                using (var tr = Util.StartTransaction())
                {
                    var acLayerTable = (LayerTable)tr.GetObject(Util.CurDoc().Database.LayerTableId, OpenMode.ForRead);
                    if (!acLayerTable.Has(layerName0))
                    {
                        Util.Editor().WriteMessage($"\n画層 \"{layerName0}\" が見つかりません。");
                        continue;
                    }

                    newLayer = (acLayerTable[layerName0], layerName0);
                    break;
                }
            }

            return true;
        }

        private void UpdateLA(Entity ent, (ObjectId, string)? newLayer)
        {
            if (newLayer == null) return;
            ent.LayerId = newLayer.Value.Item1;
        }

        #endregion

        #region 線種(LT)

        /// <summary>
        /// 線種(LT)
        /// </summary>
        private bool KeyLT(SelectionSet ss, ref (ObjectId, string)? newLineType)
        {
            var lineTypes = new List<(ObjectId, string)>();
            if (newLineType != null)
            {
                lineTypes.Add(newLineType.Value);
            }
            else
            {
                using (var tr = Util.StartTransaction())
                {
                    foreach (SelectedObject selectedObject in ss)
                    {
                        using (var obj = tr.GetObject(selectedObject.ObjectId, OpenMode.ForRead))
                        {
                            if (obj is Entity ent)
                            {
                                lineTypes.Add((ent.LinetypeId, ent.Linetype));
                            }
                        }
                    }
                }
            }

            if (lineTypes.Count == 0) return false;

            // デフォルト値
            var values = lineTypes.Distinct().ToList();
            var defVal = values.Count == 1 ? values[0].Item2 : "各種";

            while (true)
            {
                // 新しい線種名を入力 <ByLayer>
                var opt1 = new PromptStringOptions($"\n新しい線種名を入力")
                {
                    DefaultValue = defVal
                };
                var res1 = Util.Editor().GetString(opt1);
                if (res1.Status != PromptStatus.OK) return false;

                var lineTypeName0 = res1.StringResult;
                if (lineTypeName0 == defVal) return true;

                if (!GetLineType(lineTypeName0, out var lt))
                {
                    continue;
                }

                newLineType = lt;
                break;
            }

            return true;
        }

        private void UpdateLT(Entity ent, (ObjectId, string)? newLineType)
        {
            if (newLineType == null) return;
            ent.LinetypeId = newLineType.Value.Item1;
        }

        private bool GetLineType(string s, out (ObjectId, string) lt)
        {
            lt = (ObjectId.Null, "");

            while (true)
            {
                if (string.IsNullOrEmpty(s) ||
                    "BYBLOCK".StartsWith(s.ToUpper()) && "BYLAYER".StartsWith(s.ToUpper()))
                {
                    Util.Editor().WriteMessage($"\n正確に入力してください...");

                    // BYBlock または BYLayer?
                    var opt1 = new PromptStringOptions($"\nBYBlock または BYLayer?");
                    var res1 = Util.Editor().GetString(opt1);
                    if (res1.Status != PromptStatus.OK) return false;
                    s = res1.StringResult;
                    continue;
                }

                if ("BYBLOCK".StartsWith(s.ToUpper()))
                {
                    s = "ByBlock";
                    break;
                }

                if ("BYLAYER".StartsWith(s.ToUpper()))
                {
                    s = "ByLayer";
                    break;
                }
            }

            using (var tr = Util.StartTransaction())
            {
                var acLineTypeTable = (LinetypeTable)tr.GetObject(Util.CurDoc().Database.LinetypeTableId, OpenMode.ForRead);
                if (!acLineTypeTable.Has(s))
                {
                    Util.Editor().WriteMessage($"\n線種 \"{s}\" が見つかりません。LINETYPE[線種設定]コマンドを使用してロードしてください。");
                    return false;
                }

                lt = (acLineTypeTable[s], s);
                return true;
            }
        }

        #endregion

        #region 線種尺度(S)

        /// <summary>
        /// 線種尺度(S)
        /// </summary>
        private bool KeyS(SelectionSet ss, ref double? newLineTypeScale)
        {
            var lineTypeScales = new List<double>();
            if (newLineTypeScale != null)
            {
                lineTypeScales.Add(newLineTypeScale.Value);
            }
            else
            {
                using (var tr = Util.StartTransaction())
                {
                    foreach (SelectedObject selectedObject in ss)
                    {
                        using (var obj = tr.GetObject(selectedObject.ObjectId, OpenMode.ForRead))
                        {
                            if (obj is Entity ent)
                            {
                                lineTypeScales.Add(ent.LinetypeScale);
                            }
                        }
                    }
                }
            }

            if (lineTypeScales.Count == 0) return false;

            // デフォルト値
            var values = lineTypeScales.Distinct().ToList();
            var defVal = values.Count == 1 ? values[0].ToString("F4") : "各種";

            while (true)
            {
                // 新しい 線種尺度 を指定 <1.0000>
                var opt1 = new PromptDistanceOptions($"\n新しい 線種尺度 を指定 <{defVal}>")
                {
                    AllowNone = true,
                    AllowZero = false,
                    AllowNegative = true,
                };
                var res1 = Util.Editor().GetDistance(opt1);
                if (res1.Status == PromptStatus.None) return true;
                if (res1.Status != PromptStatus.OK) return false;

                newLineTypeScale = res1.Value;
                break;
            }

            return true;
        }

        private void UpdateS(Entity ent, double? newLineTypeScale)
        {
            if (newLineTypeScale == null) return;
            ent.LinetypeScale = newLineTypeScale.Value;
        }

        #endregion

        #region 線の太さ(LW)

        /// <summary>
        /// 線の太さ(LW)
        /// </summary>
        private bool KeyLW(SelectionSet ss, ref LineWeight? newLineWeight)
        {
            var lineWeights = new List<LineWeight>();
            if (newLineWeight != null)
            {
                lineWeights.Add(newLineWeight.Value);
            }
            else
            {
                using (var tr = Util.StartTransaction())
                {
                    foreach (SelectedObject selectedObject in ss)
                    {
                        using (var obj = tr.GetObject(selectedObject.ObjectId, OpenMode.ForRead))
                        {
                            if (obj is Entity ent)
                            {
                                lineWeights.Add(ent.LineWeight);
                            }
                        }
                    }
                }
            }

            if (lineWeights.Count == 0) return false;

            // デフォルト値
            var values = lineWeights.Distinct().ToList();
            var defVal = "各種";
            if (values.Count == 1)
            {
                switch (values[0])
                {
                    case LineWeight.ByLineWeightDefault:
                        defVal = "規定";
                        break;
                    case LineWeight.ByBlock:
                        defVal = "ByBlock";
                        break;
                    case LineWeight.ByLayer:
                        defVal = "ByLayer";
                        break;
                    default:
                        defVal = string.Format($"{(double)values[0] / 100.0:F2} mm");
                        break;
                }
            }

            // enum LineWeight の値を取得
            var weights = new List<LineWeight>();
            foreach (var weight in System.Enum.GetValues(typeof(LineWeight)))
            {
                weights.Add((LineWeight)weight);
            }

            while (true)
            {
                // 新しい線の太さを入力 <ByLayer>
                var opt1 = new PromptStringOptions($"\n新しい線の太さを入力")
                {
                    DefaultValue = defVal
                };
                var res1 = Util.Editor().GetString(opt1);
                if (res1.Status != PromptStatus.OK) return false;
                if (res1.StringResult == defVal) return true;

                if (!GetLineWeight(res1.StringResult, weights, out var lw))
                {
                    continue;
                }

                newLineWeight = lw;
                break;
            }

            return true;
        }

        private void UpdateLW(Entity ent, LineWeight? newLineWeight)
        {
            if (newLineWeight == null) return;
            ent.LineWeight = newLineWeight.Value;
        }

        private bool GetLineWeight(string s, List<LineWeight> weights, out LineWeight lw)
        {
            lw = LineWeight.ByLineWeightDefault;

            while (true)
            {
                if (string.IsNullOrEmpty(s) ||
                    "BYBLOCK".StartsWith(s.ToUpper()) && "BYLAYER".StartsWith(s.ToUpper()))
                {
                    Util.Editor().WriteMessage($"\n正確に入力してください...");

                    // BYBlock または BYLayer?
                    var opt1 = new PromptStringOptions($"\nBYBlock または BYLayer?");
                    var res1 = Util.Editor().GetString(opt1);
                    if (res1.Status != PromptStatus.OK) return false;
                    s = res1.StringResult;
                    continue;
                }

                if ("BYBLOCK".StartsWith(s.ToUpper()))
                {
                    lw = LineWeight.ByBlock;
                    return true;
                }

                if ("BYLAYER".StartsWith(s.ToUpper()))
                {
                    lw = LineWeight.ByLayer;
                    return true;
                }

                if ("DEFAULT".StartsWith(s.ToUpper())) return true;

                if (double.TryParse(s, out var val))
                {
                    if (val < 0.0)
                    {
                        Util.Editor().WriteMessage($"\n無効な線の太さ番号。");
                        return false;
                    }

                    lw = weights.OrderBy(a => Math.Abs((double)a / 100.0 - val - Const.EpsValue)).First();
                    if (!((double)lw).IsEqual(val))
                    {
                        Util.Editor().WriteMessage($"\n線の太さは最も近い有効な値に丸められます - {(double)lw / 100.0:f2} mm");
                    }

                    return true;
                }

                Util.Editor().WriteMessage($"\n線の太さとして定められた値が必要です。");
                return false;
            }
        }

        #endregion

        #region 厚さ(T)

        /// <summary>
        /// 厚さ(T)
        /// </summary>
        private bool KeyT(SelectionSet ss, ref double? newThickness)
        {
            var thicknesses = new List<double>();
            if (newThickness != null)
            {
                thicknesses.Add(newThickness.Value);
            }
            else
            {
                using (var tr = Util.StartTransaction())
                {
                    foreach (SelectedObject selectedObject in ss)
                    {
                        using (var obj = tr.GetObject(selectedObject.ObjectId, OpenMode.ForRead))
                        {
                            switch (obj)
                            {
                                case Arc arc:
                                    thicknesses.Add(arc.Thickness);
                                    break;
                                case Circle circle:
                                    thicknesses.Add(circle.Thickness);
                                    break;
                                case DBPoint point:
                                    thicknesses.Add(point.Thickness);
                                    break;
                                case DBText text:
                                    thicknesses.Add(text.Thickness);
                                    break;
                                case Line line:
                                    thicknesses.Add(line.Thickness);
                                    break;
                                case Polyline polyline:
                                    thicknesses.Add(polyline.Thickness);
                                    break;
                                case Polyline2d polyline2d:
                                    thicknesses.Add(polyline2d.Thickness);
                                    break;
                                case Shape shape:
                                    thicknesses.Add(shape.Thickness);
                                    break;
                                case Solid solid:
                                    thicknesses.Add(solid.Thickness);
                                    break;
                                case Trace trace:
                                    thicknesses.Add(trace.Thickness);
                                    break;
                                default:
                                    thicknesses.Add(0.0);
                                    break;
                            }
                        }
                    }
                }
            }

            if (thicknesses.Count == 0) return false;

            // デフォルト値
            var values = thicknesses.Distinct().ToList();
            var defVal = values.Count == 1 ? string.Format($"{(double)values[0]:F4}") : "各種";

            while (true)
            {
                // 新しい 厚さ を指定 <0.0000>
                var opt1 = new PromptDistanceOptions($"\n新しい 厚さ を指定 <{defVal}>")
                {
                    AllowNone = true,
                    AllowZero = false,
                    AllowNegative = true,
                };
                var res1 = Util.Editor().GetDistance(opt1);
                if (res1.Status == PromptStatus.None) return true;
                if (res1.Status != PromptStatus.OK) return false;

                newThickness = res1.Value;
                break;
            }

            return true;
        }

        private void UpdateT(Entity ent, double? newThickness, List<string> messages)
        {
            if (newThickness == null) return;
            switch (ent)
            {
                case Arc arc:
                    arc.Thickness = newThickness.Value;
                    break;
                case Circle circle:
                    circle.Thickness = newThickness.Value;
                    break;
                case DBPoint point:
                    point.Thickness = newThickness.Value;
                    break;
                case DBText text:
                    text.Thickness = newThickness.Value;
                    break;
                case Line line:
                    line.Thickness = newThickness.Value;
                    break;
                case Polyline polyline:
                    polyline.Thickness = newThickness.Value;
                    break;
                case Polyline2d polyline2d:
                    polyline2d.Thickness = newThickness.Value;
                    break;
                case Shape shape:
                    shape.Thickness = newThickness.Value;
                    break;
                case Solid solid:
                    solid.Thickness = newThickness.Value;
                    break;
                case Trace trace:
                    trace.Thickness = newThickness.Value;
                    break;
                default:
                    messages.Add($"\n{ent.GetType().Name.ToUpper()} の厚さは変更できません。");
                    break;
            }
        }

        #endregion

        #region 透過性(TR)

        /// <summary>
        /// // 透過性(TR)
        /// </summary>
        private bool KeyTR(SelectionSet ss, ref Transparency? newTransparency)
        {
            var transparencies = new List<Transparency>();
            if (newTransparency != null)
            {
                transparencies.Add(newTransparency.Value);
            }
            else
            {
                using (var tr = Util.StartTransaction())
                {
                    foreach (SelectedObject selectedObject in ss)
                    {
                        using (var obj = tr.GetObject(selectedObject.ObjectId, OpenMode.ForRead))
                        {
                            if (obj is Entity ent)
                            {
                                transparencies.Add(ent.Transparency);
                            }
                        }
                    }
                }
            }

            if (transparencies.Count == 0) return false;

            // デフォルト値
            var values = transparencies.Distinct().ToList();
            var defVal = "各種";
            if (values.Count == 1)
            {
                if (values[0].IsByBlock)
                {
                    defVal = "ByBlock";
                }
                else if (values[0].IsByLayer)
                {
                    defVal = "ByLayer";
                }
                else
                {
                    defVal = values[0].Alpha.ToString();
                }
            }

            while (true)
            {
                // 透過性の値を指定 <ByBlock>
                var opt1 = new PromptStringOptions($"\n透過性の値を指定")
                {
                    DefaultValue = defVal
                };
                var res1 = Util.Editor().GetString(opt1);
                if (res1.Status != PromptStatus.OK) return false;
                if (res1.StringResult == defVal) return true;

                if (!GetTransparency(res1.StringResult, out var tp))
                {
                    continue;
                }

                newTransparency = tp;
                break;
            }

            return true;
        }

        private void UpdateTR(Entity ent, Transparency? newTransparency)
        {
            if (newTransparency == null) return;
            ent.Transparency = newTransparency.Value;
        }

        private bool GetTransparency(string s, out Transparency? tp)
        {
            tp = null;

            while (true)
            {
                if (string.IsNullOrEmpty(s) ||
                    "BYBLOCK".StartsWith(s.ToUpper()) && "BYLAYER".StartsWith(s.ToUpper()))
                {
                    Util.Editor().WriteMessage($"\n正確に入力してください...");

                    // BYBlock または BYLayer?
                    var opt1 = new PromptStringOptions($"\nBYBlock または BYLayer?");
                    var res1 = Util.Editor().GetString(opt1);
                    if (res1.Status != PromptStatus.OK) return false;
                    s = res1.StringResult;
                    continue;
                }

                if ("BYBLOCK".StartsWith(s.ToUpper()))
                {
                    tp = new Transparency(TransparencyMethod.ByBlock);
                    return true;
                }

                if ("BYLAYER".StartsWith(s.ToUpper()))
                {
                    tp = new Transparency(TransparencyMethod.ByLayer);
                    return true;
                }

                if (byte.TryParse(s, out var val))
                {
                    if (val <= 90)
                    {
                        tp = new Transparency(val);
                        return true;
                    }
                }

                Util.Editor().WriteMessage($"\n0 から 90 までの整数を入力してください。");
                return false;
            }
        }

        #endregion

        #region 異尺度対応(A)

        /// <summary>
        /// 異尺度対応(A)
        /// </summary>
        private bool KeyA(SelectionSet ss, ref AnnotativeStates? newAnnotativeStates)
        {
            var annotativeStates = new List<AnnotativeStates>();
            if (newAnnotativeStates != null)
            {
                annotativeStates.Add(newAnnotativeStates.Value);
            }
            else
            {
                using (var tr = Util.StartTransaction())
                {
                    foreach (SelectedObject selectedObject in ss)
                    {
                        using (var obj = tr.GetObject(selectedObject.ObjectId, OpenMode.ForRead))
                        {
                            if (obj is Entity ent)
                            {
                                annotativeStates.Add(ent.Annotative);
                            }
                        }
                    }
                }
            }

            if (annotativeStates.Count == 0) return false;

            // デフォルト値
            // ※全て「True」の場合のみ「はい」
            // 　それ以外は「いいえ」(混在していても「各種」にしない。)
            var values = annotativeStates.Distinct().ToList();
            var defVal = values.Count == 1 && values[0] == AnnotativeStates.True
                ? AnnotativeStates.True : AnnotativeStates.False;

            // 異尺度対応にしますか? [はい(Y)/いいえ(N)] <はい>
            var defValStr = defVal == AnnotativeStates.True ? "はい" : "いいえ";
            var opt1 = new PromptKeywordOptions($"\n異尺度対応にしますか? <{defValStr}>")
            {
                AllowNone = true,
                Keywords =
                {
                    {"Y", "Y", "はい(Y)"},
                    {"N", "N", "いいえ(N)"},
                },
            };
            var res1 = Util.Editor().GetKeywords(opt1);
            if (res1.Status == PromptStatus.None)
            {
                newAnnotativeStates = defVal;
                return true;
            }
            if (res1.Status != PromptStatus.OK) return false;

            newAnnotativeStates = res1.StringResult == "Y" ? AnnotativeStates.True : AnnotativeStates.False;

            return true;
        }

        private void UpdateA(Entity ent, AnnotativeStates? newAnnotativeStates)
        {
            if (newAnnotativeStates == null) return;
            if (ent.Annotative == AnnotativeStates.NotApplicable) return;       // 異尺度対応不可のエンティティはスキップ
            ent.Annotative = newAnnotativeStates.Value;
        }

        #endregion
    }
}
