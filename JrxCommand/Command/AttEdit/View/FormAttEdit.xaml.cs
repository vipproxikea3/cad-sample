﻿using JrxCad.Command.AttEdit.ViewModel;
using JrxCad.View.CustomWpf;
using System.Windows;
using Microsoft.Xaml.Behaviors;

namespace JrxCad.Command.AttEdit.View
{
    public partial class FormAttEdit : CustomWindow
    {
        public FormAttEdit(FormAttEditViewModel viewModel)
        {
            DataContext = viewModel;
            this.IsHorizontalScale = true;
            InitializeComponent();
            var _ = new Microsoft.Xaml.Behaviors.DefaultTriggerAttribute(typeof(Trigger), typeof(Microsoft.Xaml.Behaviors.TriggerBase), null);
        }
    }
}
