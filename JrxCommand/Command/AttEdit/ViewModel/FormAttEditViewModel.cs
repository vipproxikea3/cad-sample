﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
#endif
using JrxCad.Utility;
using JrxCad.View.CustomWpf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using static JrxCad.Command.AttEdit.FormAttEdit;
using JrxCad.Helpers;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Documents;

namespace JrxCad.Command.AttEdit.ViewModel
{
    public class FormAttEditViewModel : BindableBase
    {
        public FormAttEditViewModel(ObjectId blockRefId)
        {
            BlockRefId = blockRefId;
            BtnOkCommand = new RelayCommand<CustomWindow>((p) => { return p != null; }, (p) => { BtnOk_Click(p); });
            CloseWindowCommand = new RelayCommand<CustomWindow>((p) => { return p != null; }, (p) => { CloseWindow(p); });
            BtnPrevCommand = new RelayCommand<CustomWindow>((p) => { return p != null; }, (p) => { BtnPrev_Click(p); });
            BtnNextCommand = new RelayCommand<CustomWindow>((p) => { return p != null; }, (p) => { BtnNext_Click(p); });
            LoadedWindow = new RelayCommand<CustomWindow>((p) => { return p != null; }, (p) => { ExecuteLoadedWindow(p); });
            foreach (var UserControlAttEditViewModel in UserControlAttEditViewModels)
            {
                UserControlAttEditViewModel.KeyDownCommand = new ActionCommand<KeyEventArgs>(OnEnterKeyDown);
            }
            Initialize();

        }
        private void Initialize()
        {
            Tr = Util.StartTransaction();
            BlockTable = Tr.GetObject<BlockTable>(Util.CurDoc().Database.BlockTableId, OpenMode.ForRead | OpenMode.ForWrite);
            BlockRef = Tr.GetObject<BlockReference>(BlockRefId, OpenMode.ForRead | OpenMode.ForWrite);
            BlockDef = Tr.GetObject<BlockTableRecord>(BlockTable[BlockRef.Name], OpenMode.ForRead | OpenMode.ForWrite);
            // block name
            LblBlockName = BlockRef.Name;

            // attribute references
            var attRefList = BlockRef.AttributeCollection;
            foreach (ObjectId attRefId in attRefList)
            {
                var attRef = Tr.GetObject<AttributeReference>(attRefId, OpenMode.ForRead | OpenMode.ForWrite);
                if (attRef.HasFields)
                {
                    var fieldId = attRef.GetField();
                    using (var field = Tr.GetObject<Field>(fieldId, OpenMode.ForRead | OpenMode.ForWrite))
                    {
                        AttList.Add(new AttributeItem
                        {
                            Tag = attRef.Tag,
                            HasFields = attRef.HasFields,
                            TextString = attRef.TextString,
                            IsMText = attRef.IsMTextAttribute,
                            Id = attRefId,
                            AttRef = attRef,
                            FieldList = field.GetChildren(),
                        });
                    }
                }
                else
                {
                    AttList.Add(new AttributeItem
                    {
                        Tag = attRef.Tag,
                        HasFields = attRef.HasFields,
                        TextString = attRef.TextString,
                        IsMText = attRef.IsMTextAttribute,
                        Id = attRefId,
                        AttRef = attRef,
                        MTextAttContents = attRef.IsMTextAttribute ? attRef.MTextAttribute.Contents : null,
                    });
                }
            }

            foreach (var id in BlockDef)
            {
                var attDef = Tr.GetObject<AttributeDefinition>(id, OpenMode.ForRead);
                if (attDef == null) continue;

                var att = AttList.FirstOrDefault(r => r.Tag == attDef.Tag);
                if (att == null) continue;
                att.Prompt = attDef.Prompt;
            }
            ReadAtt(isPrevClick: false);
            if (AttList.Count > AttMaxCountInPage) IsBtnNextEnabled = true;
            UserControlAttEditViewModels[0].Focus();

        }
        public ICommand BtnOkCommand { get; set; }
        public ICommand BtnCancelCommand { get; set; }
        public ICommand BtnPrevCommand { get; set; }
        public ICommand BtnNextCommand { get; set; }
        public ICommand CloseWindowCommand { get; set; }
        public ICommand LoadedWindow { get; set; }


        private void CloseWindow(CustomWindow currentWindow)
        {
            if (currentWindow == null) return;
            DisposeAll();
        }

        public ObjectId BlockRefId;
        public Transaction Tr;
        public BlockTable BlockTable;
        public BlockReference BlockRef;
        public BlockTableRecord BlockDef;

        public int CurrentPage = 1;
        public readonly int AttMaxCountInPage = 15;
        protected override string HelpName => "ATTEDIT";
        public int StartPosition;//starting position get attribute from list attribute to display in current page
        private int _currentFocus = 0;

        public List<AttributeItem> AttList = new List<AttributeItem>();
        private List<UserControlAttEditViewModel> _userControlAttEditViewModels = new List<UserControlAttEditViewModel>()
                {
                    new UserControlAttEditViewModel(){ IndexInPage = 0},
                    new UserControlAttEditViewModel(){ IndexInPage = 1},
                    new UserControlAttEditViewModel(){ IndexInPage = 2},
                    new UserControlAttEditViewModel(){ IndexInPage = 3},
                    new UserControlAttEditViewModel(){ IndexInPage = 4},
                    new UserControlAttEditViewModel(){ IndexInPage = 5},
                    new UserControlAttEditViewModel(){ IndexInPage = 6},
                    new UserControlAttEditViewModel(){ IndexInPage = 7},
                    new UserControlAttEditViewModel(){ IndexInPage = 8},
                    new UserControlAttEditViewModel(){ IndexInPage = 9},
                    new UserControlAttEditViewModel(){ IndexInPage = 10},
                    new UserControlAttEditViewModel(){ IndexInPage = 11},
                    new UserControlAttEditViewModel(){ IndexInPage = 12},
                    new UserControlAttEditViewModel(){ IndexInPage = 13},
                    new UserControlAttEditViewModel(){ IndexInPage = 14},
                };
        public List<UserControlAttEditViewModel> UserControlAttEditViewModels
        {
            get
            {
                return _userControlAttEditViewModels;
            }
            set
            {
                _userControlAttEditViewModels = value;
            }
        }

        private string _lblBlockName;
        public string LblBlockName
        {
            get => _lblBlockName;
            set => SetProperty(ref _lblBlockName, value);
        }

        private bool _isBtnNextEnabled;
        public bool IsBtnNextEnabled
        {
            get => _isBtnNextEnabled;
            set => SetProperty(ref _isBtnNextEnabled, value);
        }

        private bool _isBtnPrevEnabled;
        public bool IsBtnPrevEnabled
        {
            get => _isBtnPrevEnabled;
            set => SetProperty(ref _isBtnPrevEnabled, value);
        }

        void ExecuteLoadedWindow(CustomWindow customWindow)
        {
            var firstTxtD = FindChildrenHelper.FindChildrenByName<CustomRichTextBox>(customWindow, "TxtD");
            var firstBtnMtextEdit = FindChildrenHelper.FindChildrenByName<Button>(customWindow, "BtnMtextEdit");
            if (UserControlAttEditViewModels[0].Attribute.IsMText && firstBtnMtextEdit != null)
            {
                firstBtnMtextEdit.Focus();
            }
            else
            {
                if (firstTxtD != null) firstTxtD.Focus();
            }
        }

        private void OnEnterKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (UserControlAttEditViewModels[AttMaxCountInPage - 1].TxtD.IsFocused ||
                    UserControlAttEditViewModels[AttMaxCountInPage - 1].BtnMtextEdit.IsFocused
                    )
                {
                    if (IsBtnNextEnabled)
                    {
                        var nextPosition = StartPosition + AttMaxCountInPage;
                        SetPage(isPrevClick: false, CurrentPage + 1);
                        var index = nextPosition - StartPosition;
                        UserControlAttEditViewModels[index].Focus();
                        _currentFocus = index;
                        return;
                    }
                    else
                    {
                        SetPage(isPrevClick: false, 1);
                        UserControlAttEditViewModels[0].Focus();
                        _currentFocus = 0;
                        return;
                    }
                }
                else
                {
                    if (AttList.Count < AttMaxCountInPage && _currentFocus == AttList.Count - 1)
                    {
                        UserControlAttEditViewModels[0].Focus();
                        _currentFocus = 0;
                        return;
                    }
                    else
                    {
                        var focusDirection = FocusNavigationDirection.Next;
                        var request = new TraversalRequest(focusDirection);
                        var elementWithFocus = Keyboard.FocusedElement as UIElement;
                        if (elementWithFocus != null)
                        {
                            elementWithFocus.MoveFocus(request);
                        }
                    }
                }
                _currentFocus++;
            }
        }

        /// <summary>
        /// Prev
        /// </summary>
        private void BtnPrev_Click(CustomWindow currentWindow)
        {
            try
            {
                if (currentWindow == null) return;
                SetPage(isPrevClick: true, CurrentPage - 1);
                UserControlAttEditViewModels[0].Focus();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        /// <summary>
        /// Next
        /// </summary>
        private void BtnNext_Click(CustomWindow currentWindow)
        {
            try
            {
                if (currentWindow == null) return;
                SetPage(isPrevClick: false, CurrentPage + 1);
                UserControlAttEditViewModels[0].Focus();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }

        /// <summary>
        /// Set Page
        /// </summary>
        public void SetPage(bool isPrevClick, int page)
        {
            UpdateAtt();
            CurrentPage = page;
            ReadAtt(isPrevClick);
            IsBtnPrevEnabled = CurrentPage > 1;
            IsBtnNextEnabled = AttList.Count > CurrentPage * AttMaxCountInPage;
        }

        /// <summary>
        /// Update attribute
        /// </summary>
        public void UpdateAtt()
        {
            for (var i = 0; i < AttMaxCountInPage; i++)
            {
                var index = StartPosition + i;
                if (AttList.Count > i)
                {
                    var att = AttList.ElementAt(index);
                    if (att.IsMText)
                    {
                        att.MTextAttContents = UserControlAttEditViewModels[i].Attribute.MTextAttContents;
                        att.TextString = UserControlAttEditViewModels[i].TxtMValue;
                    }
                    else
                    {
                        var text = new TextRange(UserControlAttEditViewModels[i].TxtD.Document.ContentStart, UserControlAttEditViewModels[i].TxtD.Document.ContentEnd).Text;
                        att.TextString = text;
                    }
                }
            }
        }

        /// <summary>
        /// Read attribute
        /// </summary>
        public void ReadAtt(bool isPrevClick)
        {
            //updating StartPosition
            var beforeLastPage = AttList.Count / AttMaxCountInPage;
            if (CurrentPage > beforeLastPage && AttList.Count % AttMaxCountInPage != 0 && AttList.Count > AttMaxCountInPage)
            {
                StartPosition = ((CurrentPage - 1) * AttMaxCountInPage) - (AttMaxCountInPage - AttList.Count % AttMaxCountInPage);
            }
            else
            {
                if (StartPosition > AttMaxCountInPage && isPrevClick)
                {
                    StartPosition -= AttMaxCountInPage;
                }
                else
                {
                    StartPosition = (CurrentPage - 1) * AttMaxCountInPage;
                }
            }

            for (var i = 0; i < AttMaxCountInPage; i++)
            {
                var index = StartPosition + i;
                if (AttList.Count > index)
                {
                    UserControlAttEditViewModels[i].Attribute = AttList[index];
                    UserControlAttEditViewModels[i].Attribute.TextString = AttList[index].TextString;
                    UserControlAttEditViewModels[i].Attribute.MTextAttContents = AttList[index].MTextAttContents;
                    UserControlAttEditViewModels[i].Set();
                }
                else
                {
                    UserControlAttEditViewModels[i].SetInvisible();
                }
            }
        }

        /// <summary>
        /// OK
        /// </summary>
        private void BtnOk_Click(CustomWindow currentWindow)
        {
            try
            {
                if (currentWindow == null) return;
                UpdateAtt();
                var attRefList = BlockRef.AttributeCollection;
                var index = 0;
                foreach (ObjectId attRefId in attRefList)
                {
                    var attRef = Tr.GetObject<AttributeReference>(attRefId, OpenMode.ForWrite);
                    var att = AttList.ElementAt(index);
                    attRef.TextString = att.IsMText ? att.MTextAttContents : att.TextString;
                    index++;
                }
                Tr.Commit();
                Util.Editor().UpdateScreen();
                currentWindow.Close();
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
        }


        public void DisposeAll()
        {
            if (Tr != null)
            {
                Tr.Dispose();
                Tr = null;
            }

            if (BlockTable != null)
            {
                BlockTable.Dispose();
                BlockTable = null;
            }

            if (BlockRef != null)
            {
                BlockRef.Dispose();
                BlockRef = null;
            }

            if (BlockDef != null)
            {
                BlockDef.Dispose();
                BlockDef = null;
            }
        }
    }

    public class ElementListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var viewModels = (List<UserControlAttEditViewModel>)value;
                var index = Int32.Parse(parameter + "");
                return (object)viewModels[index];
            }
            catch (Exception)
            {
                return DependencyProperty.UnsetValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
