﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.ApplicationServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.ApplicationServices;
#endif
using JrxCad.Command.AttEdit.View;
using JrxCad.View.CustomWpf;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Diagnostics;
using System.IO;
using JrxCad.Helpers;
using JrxCad.Utility;
using System.Windows.Input;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using Visibility = System.Windows.Visibility;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Interop;

namespace JrxCad.Command.AttEdit.ViewModel
{
    public class UserControlAttEditViewModel : BindableBase
    {
        public UserControlAttEditViewModel()
        {
            Initialize();
            LoadedWindow = new RelayCommand<UserControlAttEdit>((p) => p != null, ExecuteLoadedWindow);
            BtnMtextEditCommand = new RelayCommand<UserControlAttEdit>((p) => p != null, BtnMtextEdit_Click);
        }
        private void Initialize()
        {
        }

        public ActionCommand<KeyEventArgs> KeyDownCommand { get; set; }
        public ICommand LoadedWindow { get; set; }
        public ICommand BtnMtextEditCommand { get; set; }
        public int IndexInPage { get; set; }

        public CustomRichTextBox TxtD { get; set; } = new CustomRichTextBox();
        public CustomTextBox TxtM = new CustomTextBox();
        public Button BtnMtextEdit { get; set; } = new Button();

        private FormAttEdit.AttributeItem _attribute;
        public FormAttEdit.AttributeItem Attribute
        {
            get => _attribute;
            set => SetProperty(ref _attribute, value);
        }

        //EventArgs
        private string _lblName;
        public string LblName
        {
            get => _lblName;
            set => SetProperty(ref _lblName, value);
        }

        private string _txtDValue = "";
        public string TxtDValue
        {
            get => _txtDValue;
            set => SetProperty(ref _txtDValue, value);
        }

        private string _txtMValue;
        public string TxtMValue
        {
            get => _txtMValue;
            set => SetProperty(ref _txtMValue, value);
        }

        public Visibility IsLblNameVisible;

        private Visibility _isTxtDVisible;
        public Visibility IsTxtDVisible
        {
            get => _isTxtDVisible;
            set => SetProperty(ref _isTxtDVisible, value);
        }
        private Visibility _isTxtMVisible;
        public Visibility IsTxtMVisible
        {
            get => _isTxtMVisible;
            set => SetProperty(ref _isTxtMVisible, value);
        }

        private Visibility _isBtnMtextEditVisible;
        public Visibility IsBtnMtextEditVisible
        {
            get => _isBtnMtextEditVisible;
            set => SetProperty(ref _isBtnMtextEditVisible, value);
        }

        private bool _isTxtDEnabled = true;
        public bool IsTxtDEnabled
        {
            get => _isTxtDEnabled;
            set => SetProperty(ref _isTxtDEnabled, value);
        }
        private SolidColorBrush _txtDBackground = Brushes.White;
        public SolidColorBrush TxtDBackground
        {
            get => _txtDBackground;
            set => SetProperty(ref _txtDBackground, value);
        }

        /// <summary>
        /// Set Invisible
        /// </summary>
        public void SetInvisible()
        {
            IsLblNameVisible = IsTxtMVisible = IsBtnMtextEditVisible = Visibility.Hidden;
            IsTxtDEnabled = false;
            TxtDBackground = (SolidColorBrush)new BrushConverter().ConvertFrom("#F0F0F0");
        }

        /// <summary>
        /// Set Value
        /// </summary>
        public void Set()
        {
            LblName = Attribute.Title;
            var isMText = Attribute.IsMText;
            IsTxtDVisible = !isMText ? Visibility.Visible : Visibility.Hidden;
            IsTxtMVisible = IsBtnMtextEditVisible = isMText ? Visibility.Visible : Visibility.Hidden;
            if (isMText)
            {
                TxtMValue = Attribute.TextString;
            }
            else
            {
                TxtD.Document.Blocks.Clear();
                TxtD.AppendText(Attribute.TextString);
            }
        }

        private void ExecuteLoadedWindow(UserControlAttEdit userCtrlAttEdit)
        {
            TxtD = userCtrlAttEdit.FindChildrenByName<CustomRichTextBox>("TxtD");
            TxtM = userCtrlAttEdit.FindChildrenByName<CustomTextBox>("TxtM");
            BtnMtextEdit = userCtrlAttEdit.FindChildrenByName<Button>("BtnMtextEdit");
        }

        public void Focus()
        {
            if (this.Attribute.IsMText)
            {
                BtnMtextEdit.Focus();
            }
            else
            {
                TxtD.Focus();
            }
        }

        /// <summary>
        /// MText editor
        /// </summary>
        public void BtnMtextEdit_Click(UserControlAttEdit userControlAttedit)
        {
#if _IJCAD_
            try
            {
                MessageBox.Show("This feature is not available yet");
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
#elif _AutoCAD_

            try
            {
                var parent = (CustomWindow)Window.GetWindow(userControlAttedit);
                var attId = (ObjectId)Attribute.Id;
                using (parent != null ? Util.Editor().StartUserInteraction(parent) : null)
                using (var tr = Util.StartTransaction())
                using (var attRef = tr.GetObject<AttributeReference>(attId, OpenMode.ForWrite))
                using (var mText = attRef.MTextAttribute)
                {
                    var currentMTexted = SystemVariable.GetValue("MTEXTED").ToString().ToUpper();

                    if ((File.Exists(currentMTexted) && currentMTexted.Contains("NOTEPAD.EXE")) || currentMTexted == "NOTEPAD.EXE")
                    {
                        var process = new Process();
                        var file = Path.GetTempFileName();
                        process = Process.Start("NOTEPAD.EXE", file);
                        var contents = "";
                        if (Attribute.MTextAttContents == null)
                        {
                            Attribute.MTextAttContents = mText.Contents;
                        }
                        using (var sw = new StreamWriter(file))
                        {
                            sw.Write(Attribute.MTextAttContents.Replace(@"\P", Environment.NewLine));
                        }
                        while (!process.HasExited) { }
                        using (var sr = new StreamReader(file))
                        {
                            contents = sr.ReadToEnd();
                        }
                        UpdateMtextEdit(attRef, text: "", contents, isNotepad: true);
                    }
                    else
                    {
                        switch (currentMTexted)
                        {
                            case "INTERNAL":
                            case "":
                            case "OLDEDITOR":
                                var currentAttipe = Convert.ToBoolean(SystemVariable.GetValue("ATTIPE"));
                                var editor = new InplaceTextEditorSettings
                                {
                                    Type = InplaceTextEditorSettings.EntityType.MultiAttribute,
                                    SimpleMText = !currentAttipe
                                };
                                if (Attribute.MTextAttContents != null)
                                {
                                    mText.Contents = Attribute.MTextAttContents;
                                }
                                InplaceTextEditor.Invoke(mText, editor);
                                UpdateMtextEdit(attRef, mText.Text, mText.Contents, isNotepad: false);
                                break;
                            default:
                                Util.Editor().WriteMessage("\nCannot find shell program");
                                break;
                        }
                    }
                }
                var e = new KeyEventArgs(
                            Keyboard.PrimaryDevice,
                            new HwndSource(0, 0, 0, 0, 0, "", IntPtr.Zero),
                            0,
                            Key.Enter);
                KeyDownCommand?.Execute(e);
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
#endif
        }

        private void UpdateMtextEdit(AttributeReference attRef, string text, string contents, bool isNotepad)
        {
            if (isNotepad)
            {
                var mtext = new MText()
                {
                    Contents = contents
                };
                text = mtext.Text.Replace("\r", " ");
                mtext.Dispose();
            }
            TxtMValue = text.Replace("\r\n", " ");
            Attribute.TextString = TxtMValue;
            Attribute.MTextAttContents = contents;
            attRef.TextString = contents;
        }
    }
}
