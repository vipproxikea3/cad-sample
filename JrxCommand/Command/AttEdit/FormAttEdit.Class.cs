﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
#endif

namespace JrxCad.Command.AttEdit
{
    public class FormAttEdit
    {
        public class AttributeItem
        {
            /// <summary>
            /// Attribute name to be displayed on the screen
            /// </summary>
            public string Title => string.IsNullOrEmpty(Prompt) ? Tag : Prompt;

            /// <summary>
            /// Tag
            /// </summary>
            public string Tag { get; set; }

            /// <summary>
            /// Attribute definition of prompt
            /// </summary>
            public string Prompt { get; set; }

            /// <summary>
            /// Attribute reference of TextString
            /// </summary>
            public string TextString { get; set; }

            /// <summary>
            /// Field
            /// </summary>
            public bool HasFields { get; set; }
            public Field[] FieldList { get; set; }

            public AttributeReference AttRef { get; set; }

            /// <summary>
            /// MText
            /// </summary>
            public bool IsMText { get; set; }

            /// <summary>
            /// MText attribute contents
            /// </summary>
            public string MTextAttContents { get; set; }

            /// <summary>
            /// Id
            /// </summary>
            public ObjectId Id { get; set; }
        }
    }
}
