﻿using JrxCad.View.CustomWpf;
using System.Windows;

namespace JrxCad.Command.AttEdit
{
    public class UserControlAttEditViewModel : BindableBase
    {

        public UserControlAttEditViewModel()
        {
            Initialize();
        }
        private void Initialize()
        {
        }
        private FormAttEdit.AttributeItem _attribute;
        public FormAttEdit.AttributeItem Attribute
        {
            get => _attribute;
            set => SetProperty(ref _attribute, value);
        }


        private string _lblName;
        public string LblName
        {
            get => _lblName;
            set => SetProperty(ref _lblName, value);
        }

        private string _txtValue;
        public string TxtValue
        {
            get => _txtValue;
            set => SetProperty(ref _txtValue, value);
        }
    }
}
