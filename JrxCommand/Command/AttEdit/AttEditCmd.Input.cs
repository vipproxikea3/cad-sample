﻿#if _IJCAD_
using GrxCAD.EditorInput;
#elif _AutoCAD_
using Autodesk.AutoCAD.EditorInput;
#endif
using JrxCad.Utility;
using JrxCad.Command.AttEdit.ViewModel;

namespace JrxCad.Command.AttEdit
{
    public partial class AttEditCmd
    {
        private readonly IUserInput _userInput;
        public interface IUserInput
        {
            PromptEntityResult SelectBlockRef(PromptEntityOptions options);
        }

        /// <summary>
        /// UserInput
        /// </summary>
        private class UserInput : IUserInput
        {
            public PromptEntityResult SelectBlockRef(PromptEntityOptions options)
            {
                return Util.Editor().GetEntity(options);
            }
        }

        private readonly IWindowInput _windowInput;

        /// <summary>
        /// IWindowInput Interface
        /// </summary>
        public interface IWindowInput
        {
            bool FormAttEditShowDialog(FormAttEditViewModel viewModel);
        }

        /// <summary>
        /// WindowInput
        /// </summary>
        private class WindowInput : IWindowInput
        {
            public bool FormAttEditShowDialog(FormAttEditViewModel viewModel)
            {
                var form = new View.FormAttEdit(viewModel);
                return (bool)form.ShowDialog();
            }
        }
    }
}
