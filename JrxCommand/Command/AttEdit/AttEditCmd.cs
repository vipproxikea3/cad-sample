﻿#if _IJCAD_
using GrxCAD.Runtime;
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
#endif
using JrxCad.Helpers;
using JrxCad.Utility;
using Exception = System.Exception;
using JrxCad.Command.AttEdit.ViewModel;

namespace JrxCad.Command.AttEdit
{
    public partial class AttEditCmd : BaseCommand
    {
        public AttEditCmd() : this(new UserInput(), new WindowInput())
        {
        }

        public AttEditCmd(IUserInput userInput, IWindowInput windowInput)
        {
            _userInput = userInput;
            _windowInput = windowInput;
        }

#if DEBUG
        [CommandMethod("JrxCommand", "SmxAE", CommandFlags.Modal | CommandFlags.NoNewStack)]
#else 
        [CommandMethod("JrxCommand", "SmxATTEDIT", CommandFlags.Modal | CommandFlags.NoNewStack)]
#endif
        public override void OnCommand()
        {
            try
            {
                if (!Init())
                {
                    return;
                }

                var blockRefId = SelectBlockRef();
                if (blockRefId == ObjectId.Null) return;

                var viewModel = new FormAttEditViewModel(blockRefId)
                {
                };
                if (!_windowInput.FormAttEditShowDialog(viewModel))
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        /// <summary>
        /// Select block reference
        /// </summary>
        private ObjectId SelectBlockRef()
        {
            ObjectId blockRefId;
            var optBlockRef = new PromptEntityOptions("\nSelect block reference")
            {
                AllowNone = true //set the prompt accepts ENTER as sole input
            };
            using (var tr = Util.StartTransaction())
            {
                while (true)
                {
                    var resBlockRef = _userInput.SelectBlockRef(optBlockRef);
                    if (resBlockRef == null) return ObjectId.Null;
                    //check pressing "Enter"
                    if (resBlockRef.Status == PromptStatus.None)
                    {
                        Util.Editor().WriteMessage("\nNo object found.");
                        continue;
                    }

                    if (resBlockRef.Status != PromptStatus.OK) return ObjectId.Null;

                    using (var blockRef = tr.GetObject<BlockReference>(resBlockRef.ObjectId, OpenMode.ForRead))
                    {
                        if (blockRef == null)
                        {
                            Util.Editor().WriteMessage("\nThat object is not a block");
                            continue;
                        }

                        if (blockRef.AttributeCollection == null)
                        {
                            Util.Editor().WriteMessage("\nThat block has no editable attributes");
                            continue;
                        }

                        var isEditable = false;
                        var attRefList = blockRef.AttributeCollection;
                        foreach (ObjectId attRefId in attRefList)
                        {
                            var attRef = tr.GetObject<AttributeReference>(attRefId, OpenMode.ForRead);
                            if (attRef.IsConstant) continue;
                            isEditable = true;
                        }

                        if (!isEditable)
                        {
                            Util.Editor().WriteMessage("\nThat block has no editable attributes");
                            continue;
                        }

                        blockRefId = blockRef.Id;
                        break;
                    }
                }
            }
            return blockRefId;
        }
    }
}
