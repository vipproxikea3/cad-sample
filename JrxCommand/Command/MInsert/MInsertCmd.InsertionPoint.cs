﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.Internal;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
#endif
using JrxCad.Enum;
using JrxCad.Helpers;
using JrxCad.Utility;

//TODO: [MInsert] please re-check source below with re-sharper
namespace JrxCad.Command.MInsert
{
    public partial class MInsertCmd
    {
        #region Handles Insertion Point UI
        /// <summary>
        /// Handle Insertion point Option
        /// </summary>
        private PromptStatus InsertionPointUi(BlockReference blockReference)
        {
            var isScaled = false;
            var isRotated = false;
            var baseVector = new Vector3d();
            var optInsertionPt = new InsertionPointOpts(blockReference, baseVector)
            {
                Options = new JigPromptPointOptions()
                {
                    AppendKeywordsToMessage = false,
                    UserInputControls = UserInputControls.Accept3dCoordinates,
                    Keywords =
                        {
                            {"B", $"{MInsertRes.BasePoint_Key}"},
                            {"S", $"{MInsertRes.Scale_Key}"},
                            {"R", $"{MInsertRes.Rotate_Key}"},
                            {"P", $"{MInsertRes.P_Key}"},
                            {"PS", $"{MInsertRes.PScale_Key}"},
                            {"PR", $"{MInsertRes.PRotate_Key}"},
                        }
                }
            };

            if (_uniformScaleActive)
            {
                optInsertionPt.Options.Message = $"\n{MInsertRes.UI_InsertionPointUniformScale}";
            }
            else
            {
                optInsertionPt.Options.Message = $"\n{MInsertRes.UI_InsertionPoint}";
                optInsertionPt.Options.Keywords.Add("X", $"{MInsertRes.X_Key}");
                optInsertionPt.Options.Keywords.Add("Y", $"{MInsertRes.Y_Key}");
                optInsertionPt.Options.Keywords.Add("Z", $"{MInsertRes.Z_Key}");
                optInsertionPt.Options.Keywords.Add("PX", $"{MInsertRes.PXscale_Key}");
                optInsertionPt.Options.Keywords.Add("PY", $"{MInsertRes.PYscale_Key}");
                optInsertionPt.Options.Keywords.Add("PZ", $"{MInsertRes.PZscale_Key}");
            }

            while (true)
            {
                var resInsertionPt = _userInput.AcquirePoint(optInsertionPt) as PromptPointResult;
                if (resInsertionPt.Status != PromptStatus.OK && resInsertionPt.Status != PromptStatus.Keyword) return PromptStatus.Cancel;
                switch (resInsertionPt.Status)
                {
                    case PromptStatus.OK:
                        baseVector = optInsertionPt.BaseVector;
                        break;
                    case PromptStatus.Keyword:
                        ScaleType direct;
                        switch (resInsertionPt.StringResult)
                        {
                            case "B":
                                if (BasePoint(optInsertionPt) == PromptStatus.Cancel) return PromptStatus.Cancel;
                                break;
                            case "S":
                                if (ScaleS(optInsertionPt.BlockReference, false) == PromptStatus.Cancel) return PromptStatus.Cancel;
                                isScaled = true;
                                break;
                            case "X":
                            case "Y":
                            case "Z":
                                direct = (ScaleType)System.Enum.Parse(typeof(ScaleType), resInsertionPt.StringResult);
                                if (Scale(optInsertionPt.BlockReference, direct, false) == PromptStatus.Cancel) return PromptStatus.Cancel;
                                isScaled = true;
                                break;
                            case "R":
                                if (Rotation(optInsertionPt, false) == PromptStatus.Cancel) return PromptStatus.Cancel;
                                isRotated = true;
                                break;
                            case "P":
                                if (PreviewInsertion(optInsertionPt) == PromptStatus.Cancel) return PromptStatus.Cancel;
                                break;
                            case "PS":
                                if (ScaleS(optInsertionPt.BlockReference, true) == PromptStatus.Cancel) return PromptStatus.Cancel;
                                break;
                            case "PX":
                            case "PY":
                            case "PZ":
                                direct = (ScaleType)System.Enum.Parse(typeof(ScaleType), resInsertionPt.StringResult.Replace("P", string.Empty));
                                if (Scale(optInsertionPt.BlockReference, direct, true) == PromptStatus.Cancel) return PromptStatus.Cancel;
                                break;
                            case "PR":
                                if (Rotation(optInsertionPt, true) == PromptStatus.Cancel) return PromptStatus.Cancel;
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }

                if (resInsertionPt.Status == PromptStatus.OK) break;
            }

            if (!isScaled && ScaleUi(blockReference, baseVector) == PromptStatus.Cancel) return PromptStatus.Cancel;
            if (!isRotated && RotationUi(blockReference, baseVector) == PromptStatus.Cancel) return PromptStatus.Cancel;

            return PromptStatus.OK;
        }

        /// <summary>
        /// Preview Insertion
        /// </summary>
        private PromptStatus PreviewInsertion(InsertionPointOpts optInsertionPt)
        {
            var message = _uniformScaleActive ? $"{MInsertRes.UI_PreviewUniform}" : $"{MInsertRes.UI_Preview}";
            var optPreview = new PromptKeywordOptions(message)
            {
                AppendKeywordsToMessage = false,
                Keywords =
                {
                    {"PS", $"{MInsertRes.PScale_Key}"},
                    {"PR", $"{MInsertRes.PRotate_Key}"},
                }
            };
            if (!_uniformScaleActive)
            {
                optPreview.Keywords.Add("PX", $"{MInsertRes.PXscale_Key}");
                optPreview.Keywords.Add("PY", $"{MInsertRes.PYscale_Key}");
                optPreview.Keywords.Add("PZ", $"{MInsertRes.PZscale_Key}");
            }

            var resRepeat = _userInput.GetKeyword(optPreview);
            if (resRepeat.Status == PromptStatus.Cancel) return PromptStatus.Cancel;
            if (resRepeat.Status == PromptStatus.OK)
            {
                switch (resRepeat.StringResult)
                {
                    case "PS":
                        if (ScaleS(optInsertionPt.BlockReference, true) == PromptStatus.Cancel) return PromptStatus.Cancel;
                        break;
                    case "PX":
                    case "PY":
                    case "PZ":
                        var direct = (ScaleType)System.Enum.Parse(typeof(ScaleType), resRepeat.StringResult.Replace("P", string.Empty));
                        if (Scale(optInsertionPt.BlockReference, direct, true) == PromptStatus.Cancel) return PromptStatus.Cancel;
                        break;
                    case "PR":
                        if (Rotation(optInsertionPt, true) == PromptStatus.Cancel) return PromptStatus.Cancel;
                        break;
                    default:
                        break;
                }
            }
            return PromptStatus.OK;
        }

        /// <summary>
        /// Handle Base point in Insertion point option
        /// </summary>
        private PromptStatus BasePoint(InsertionPointOpts optInsertionPt)
        {
            var optBasePt = new BasePointOpts(optInsertionPt.BlockReference)
            {
                Options = new JigPromptPointOptions($"\n{MInsertRes.UI_BasePoint}")
                {
                    UserInputControls = UserInputControls.Accept3dCoordinates,
                }
            };

            var resBasePt = (PromptPointResult)_userInput.AcquirePoint(optBasePt);
            if (resBasePt.Status != PromptStatus.OK) return PromptStatus.Cancel;

            var vector = resBasePt.Value - optInsertionPt.BlockReference.Position;
            var baseVector = new Vector3d(vector.X / optInsertionPt.BlockReference.ScaleFactors.X, vector.Y / optInsertionPt.BlockReference.ScaleFactors.Y, 0);
            optInsertionPt.BaseVector = baseVector;
            return PromptStatus.OK;
        }

        /// <summary>
        /// XYZ 軸に対する尺度を指定 ＜1＞
        /// </summary>
        private PromptStatus ScaleS(BlockReference blockReference, bool isPreview)
        {
            var message = isPreview ? MInsertRes.UI_PreviewScaleS : MInsertRes.UI_ScaleXYZAxes;
            var optScaleS = new PromptDistanceOptions($"\n{message}")
            {
                DefaultValue = 1.0,
                AllowZero = false,
            };

            var resScaleS = _userInput.GetDistance(optScaleS);
            if (resScaleS.Status != PromptStatus.OK) return PromptStatus.Cancel;

            blockReference.ScaleFactors = new Scale3d(resScaleS.Value, resScaleS.Value, resScaleS.Value);
            return PromptStatus.OK;
        }

        /// <summary>
        /// Handle Rotation in Insertion point option
        /// </summary>
        private PromptStatus Rotation(InsertionPointOpts optInsertionPt, bool isPreview)
        {
            var angBase = SystemVariable.GetAngle("ANGBASE");
            var message = isPreview ? MInsertRes.UI_PreviewRotation : MInsertRes.UI_Rotation;
            var optAngle = new PromptAngleOptions($"\n{message}")
            {
                DefaultValue = angBase
            };

            var resAngle = _userInput.GetAngle(optAngle);
            if (resAngle.Status != PromptStatus.OK) return PromptStatus.Cancel;
            optInsertionPt.BlockReference.Rotation = resAngle.Value;
            optInsertionPt.ResetVector();
            return PromptStatus.OK;
        }

        /// <summary>
        /// XorYorZ 方向の尺度を指定 ＜1＞
        /// </summary>
        private PromptStatus Scale(BlockReference blockReference, ScaleType direct, bool isPreview)
        {
            var scaleFactors = blockReference.ScaleFactors;
            var message = string.Empty;
            switch (direct)
            {
                case ScaleType.X:
                    message = isPreview ? $"\n{MInsertRes.UI_PreviewScaleX}" : $"\n{MInsertRes.UI_ScaleX}";
                    break;
                case ScaleType.Y:
                    message = isPreview ? $"\n{MInsertRes.UI_PreviewScaleY}" : $"\n{MInsertRes.UI_ScaleY}";
                    break;
                case ScaleType.Z:
                    message = isPreview ? $"\n{MInsertRes.UI_PreviewScaleZ}" : $"\n{MInsertRes.UI_ScaleZ}";
                    break;
            }

            var optScale = new PromptDistanceOptions(message)
            {
                DefaultValue = 1,
                AllowZero = false,
            };

            var resScale = _userInput.GetDistance(optScale);
            if (resScale.Status != PromptStatus.OK) return PromptStatus.Cancel;

            switch (resScale.Status)
            {
                case PromptStatus.OK:
                    switch (direct)
                    {
                        case ScaleType.X:
                            blockReference.ScaleFactors = new Scale3d(resScale.Value, scaleFactors.Y, scaleFactors.Z);
                            break;
                        case ScaleType.Y:
                            blockReference.ScaleFactors = new Scale3d(scaleFactors.X, resScale.Value, scaleFactors.Z);
                            break;
                        case ScaleType.Z:
                            blockReference.ScaleFactors = new Scale3d(scaleFactors.X, scaleFactors.Y, resScale.Value);
                            break;
                    }
                    return PromptStatus.OK;

                default:
                    return PromptStatus.Cancel;
            }
        }

        /// <summary>
        /// Handle Rotation Option
        /// </summary>
        private PromptStatus RotationUi(BlockReference blockReference, Vector3d? baseVector)
        {
            var angBase = SystemVariable.GetAngle("ANGBASE");

            var scale = new Vector3d(baseVector.Value.X * blockReference.ScaleFactors.X,
                                    baseVector.Value.Y * blockReference.ScaleFactors.Y,
                                    baseVector.Value.Z * blockReference.ScaleFactors.Z);
            var basePoint = blockReference.Position.Add(scale);

            var optRotation = new RotationOpts(blockReference, scale, basePoint)
            {
                Options = new JigPromptAngleOptions($"\n{MInsertRes.UI_Rotation}")
                {
                    BasePoint = basePoint,
                    UseBasePoint = true,
                    DefaultValue = 0 + angBase,
                    Cursor = CursorType.RubberBand,
                    UserInputControls = UserInputControls.NullResponseAccepted,
                }
            };
            while (true)
            {
                BitmapImageHelper.AddCursorBadge(MInsertRes.IconRotationPath);
                var resRotation = _userInput.AcquireAngle(optRotation) as PromptDoubleResult;
                BitmapImageHelper.RemoveCursorBadge();
                if (resRotation.Status != PromptStatus.OK && resRotation.Status != PromptStatus.Keyword) return PromptStatus.Cancel;
                if (resRotation.Status == PromptStatus.OK) return PromptStatus.OK;
            }
        }

        /// <summary>
        /// Handle Scale Option
        /// </summary>
        private PromptStatus ScaleUi(BlockReference blockReference, Vector3d? baseVector)
        {
            if (!baseVector.HasValue) return PromptStatus.OK;

            var baseVectorDefault = new Vector3d(baseVector.Value.X, baseVector.Value.Y, baseVector.Value.Z);
            var basePointDefault = blockReference.Position.Add(baseVectorDefault);

            if (_uniformScaleActive)
            {
                var basePoint = blockReference.Position.Add(baseVector.Value);
                var optScaleUniform = new ScaleOpts(blockReference, baseVector.Value, basePoint)
                {
                    Options = new JigPromptDistanceOptions($"\n{MInsertRes.UI_ScaleXYZAxes}")
                    {
                        BasePoint = basePoint,
                        UseBasePoint = true,
                        Cursor = CursorType.RubberBand,
                        AppendKeywordsToMessage = false,
                        DefaultValue = 1,
                        UserInputControls = UserInputControls.NullResponseAccepted |
                                            UserInputControls.UseBasePointElevation |
                                            UserInputControls.NoZeroResponseAccepted,
                    }
                };

                BitmapImageHelper.AddCursorBadge(MInsertRes.IconScalePath);
                var resScaleUniform = _userInput.AcquireDistance(optScaleUniform);
                BitmapImageHelper.RemoveCursorBadge();
                if (resScaleUniform.Status != PromptStatus.OK && resScaleUniform.Status != PromptStatus.Keyword) return PromptStatus.Cancel;
            }
            else
            {
                var basePoint = blockReference.Position.Add(baseVector.Value);

                var optScaleCorner = new ScaleCornerOpts(blockReference, baseVector.Value, basePoint)
                {
                    Options = new JigPromptStringOptions($"\n{MInsertRes.UI_ScaleCornerX}")
                    {
                        DefaultValue = "1",
                        AppendKeywordsToMessage = false,
                        UserInputControls = UserInputControls.Accept3dCoordinates |
                                            UserInputControls.NullResponseAccepted |
                                            UserInputControls.NoZeroResponseAccepted,
                        Keywords =
                            {
                                {"XYZ", $"{MInsertRes.XYZ_Key}"},
                            }
                    }
                };
                while (true)
                {
                    Util.Editor().PromptedForPoint += optScaleCorner.UpdateScale;
                    BitmapImageHelper.AddCursorBadge(MInsertRes.IconCornerPath);
                    var resScale = _userInput.AcquireString(optScaleCorner);
                    BitmapImageHelper.RemoveCursorBadge();
                    Util.Editor().PromptedForPoint -= optScaleCorner.UpdateScale;

                    if (optScaleCorner.UserStatus == PromptStatus.Cancel) return PromptStatus.Cancel;

                    if (resScale.StringResult == "0")
                    {
                        WriteMessage(string.Format(MInsertRes.NonzeroMessage) + "\n");
                        continue;
                    }
                    if (resScale.StringResult.ToUpper() != "X" && resScale.StringResult.ToUpper() != "XYZ")
                    {
                        if (resScale.StringResult != "\n" && !resScale.StringResult.Contains(","))
                        {
                            var optScaleY = new PromptDoubleOptions($"\n{MInsertRes.UI_ScaleY_DefaultStringValue}")
                            {
                                AllowNone = true,
                                AllowNegative = true,
                                AllowZero = false,
                            };
                            var resScaleY = _userInput.GetDouble(optScaleY);
                            if (resScaleY.Status != PromptStatus.OK && resScaleY.Status != PromptStatus.None) return PromptStatus.Cancel;

                            var scaleX = (string.IsNullOrEmpty(resScale.StringResult)) ? 1 : double.Parse(resScale.StringResult);
                            var scaleY = (resScaleY.Value == 0) ? scaleX : resScaleY.Value;
                            var scaleZ = scaleX;
                            optScaleCorner.SetScale(scaleX, scaleY, scaleZ);
                            var baseVectorScale = new Vector3d(baseVector.Value.X * scaleX,
                                                            baseVector.Value.Y * scaleY,
                                                            baseVector.Value.Z * scaleZ);
                            var vector = -baseVectorScale.RotateBy(-blockReference.Rotation, Vector3d.ZAxis);
                            blockReference.Position = basePointDefault.Add(vector);
                        }
                        break;
                    }
                    if (resScale.StringResult.ToUpper() == "X" || resScale.StringResult.ToUpper() == "XYZ")
                    {
                        var optScaleX = new PromptDoubleOptions($"\n{MInsertRes.UI_ScaleX}")
                        {
                            DefaultValue = 1,
                            AllowNegative = true,
                            AllowZero = false,
                            AllowNone = false,
                        };
                        var resScaleX = _userInput.GetDouble(optScaleX);
                        if (resScaleX.Status != PromptStatus.OK) return PromptStatus.Cancel;

                        var optScaleY = new PromptDoubleOptions($"\n{MInsertRes.UI_ScaleY_DefaultStringValue}")
                        {
                            AllowNone = true,
                            AllowNegative = true,
                            AllowZero = false,
                        };
                        var resScaleY = _userInput.GetDouble(optScaleY);
                        if (resScaleY.Status != PromptStatus.OK && resScaleY.Status != PromptStatus.None) return PromptStatus.Cancel;

                        var optScaleZ = new PromptDoubleOptions(string.Format(MInsertRes.UI_ScaleZ_DefaultStringValue))
                        {
                            AllowNone = true,
                            AllowNegative = true,
                            AllowZero = false,
                        };
                        var resScaleZ = _userInput.GetDouble(optScaleZ);
                        if (resScaleZ.Status == PromptStatus.OK || resScaleZ.Status == PromptStatus.None)
                        {
                            var scaleX = (resScaleX.Value == 0) ? 1 : resScaleX.Value;
                            var scaleY = (resScaleY.Value == 0) ? scaleX : resScaleY.Value;
                            var scaleZ = (resScaleZ.Value == 0) ? scaleX : resScaleZ.Value;
                            optScaleCorner.SetScale(scaleX, scaleY, scaleZ);
                            var baseVectorScale = new Vector3d(baseVector.Value.X * scaleX,
                                                                baseVector.Value.Y * scaleY,
                                                                baseVector.Value.Z * scaleZ);
                            var vector = -baseVectorScale.RotateBy(-blockReference.Rotation, Vector3d.ZAxis);
                            blockReference.Position = basePointDefault.Add(vector);
                        }
                        break;
                    }
                    return PromptStatus.Cancel;
                }
            }

            return PromptStatus.OK;
        }
        #endregion
    }
}
