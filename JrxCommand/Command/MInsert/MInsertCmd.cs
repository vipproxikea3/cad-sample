﻿#if _IJCAD_
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.Runtime;
using Exception = GrxCAD.Runtime.Exception;
using OpenFileDialog = GrxCAD.Windows.OpenFileDialog;
#elif _AutoCAD_
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Internal;
using Autodesk.AutoCAD.Runtime;
using Exception = System.Exception;
using OpenFileDialog = Autodesk.AutoCAD.Windows.OpenFileDialog;
#endif
using JrxCad.Helpers;
using JrxCad.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

//TODO: [MInsert] please re-check source below with re-sharper
namespace JrxCad.Command.MInsert
{
    public partial class MInsertCmd : BaseCommand
    {
        [CommandMethod("JrxCommand", "SmxMINSERT", CommandFlags.Modal | CommandFlags.NoNewStack | CommandFlags.Interruptible)]
        public override void OnCommand()
        {
            try
            {
                if (!Init()) return;

                //BN. ブロック名取得
                var blockName = GetBlockName(out var fullPath);
                if (string.IsNullOrWhiteSpace(blockName)) return;

                using (var blockRef = GetBlockReference(blockName, fullPath, out var blkTableRecId))
                {
                    blockRef.SetDatabaseDefaults();

                    if (blockRef.Annotative != AnnotativeStates.False)
                    {
                        //AN1. 異尺度不可メッセージ
                        WriteMessage($"\n{string.Format(MInsertRes.AN1)}");
                        return;
                    }

                    using (new SystemVariable.Temp("ATTREQ", 0))
                    {
                        //CM:Unit
                        ShowCMUnit(blockRef);

                        //InsertUI
                        using (new SystemVariable.Temp("LUPREC", 0))
                        {
                            if (InsertionPointUi(blockRef) == PromptStatus.Cancel) return;
                        }
                    }

                    //UI（Rect）
                    short rows = 1;
                    short columns = 1;
                    double colSpacing = 0;
                    double rowSpacing = 0;

                    if (InputArray(ref rows, ref columns, ref colSpacing, ref rowSpacing) == PromptStatus.Cancel) return;

                    //TODO: Attribute

                    if (rows > 1 || columns > 1)
                    {
                        var mInsertBlock = new MInsertBlock(blockRef.Position, blkTableRecId, columns, rows, colSpacing, rowSpacing)
                        {
                            ScaleFactors = blockRef.ScaleFactors,
                            Rotation = blockRef.Rotation,
                            LinetypeScale = SystemVariable.GetDouble("CELTSCALE"),
                        };

                        AddEntityIntoDatabase(mInsertBlock);
                    }
                    else
                    {
                        AddEntityIntoDatabase(blockRef);
                    }

                    SystemVariable.SetString("INSNAME", blockRef.Name);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorShow(ex);
            }
            finally
            {
                Exit();
            }
        }

        #region Declare
        private List<string> _listBlockName;
        private List<string> _listBlockAnonymous;
        private bool _uniformScaleActive;
        #endregion

        #region Constructor
        public MInsertCmd() : this(new UserInput()) { }

        public MInsertCmd(IMInsertUserInput userInput)
        {
            _userInput = userInput;
        }
        #endregion

        #region Handles Block Name
        /// <summary>
        /// Show write message info Unit
        /// </summary>
        private void ShowCMUnit(BlockReference blockRef)
        {
            var insUnits = SystemVariable.GetInt("INSUNITS");
            var insUnitsDefSource = SystemVariable.GetInt("INSUNITSDEFSOURCE");
            var insUnitsDefTarget = SystemVariable.GetInt("INSUNITSDEFTARGET");

            if (blockRef.BlockUnit > 0)
            {
                WriteMessage($"\n{string.Format(MInsertRes.CM_Unit_1, blockRef.BlockUnit)}");
            }
            else
            {
                WriteMessage($"\n{string.Format(MInsertRes.CM_Unit_1, (UnitsValue)insUnitsDefSource)}");
            }

            if (insUnits > 0)
            {
                WriteMessage($"\n{string.Format(MInsertRes.CM_Unit_2, (UnitsValue)insUnits)}");
            }
            else
            {
                WriteMessage($"\n{string.Format(MInsertRes.CM_Unit_2, (UnitsValue)insUnitsDefTarget)}");
            }

            double scaleFactor = 1;
            double factor = 1;

            if (blockRef.BlockUnit > 0 &&
                     insUnits == 0 &&
                     insUnitsDefTarget > 0)
            {
                scaleFactor = UnitsConverter.GetConversionFactor(blockRef.BlockUnit, (UnitsValue)insUnitsDefTarget);
            }
            else if (blockRef.BlockUnit == 0 &&
                     insUnitsDefSource > 0 &&
                     insUnits == 0 &&
                     insUnitsDefTarget > 0)
            {
                scaleFactor = UnitsConverter.GetConversionFactor((UnitsValue)insUnitsDefSource, (UnitsValue)insUnitsDefTarget);
            }
            else if (blockRef.BlockUnit == 0 &&
                     insUnitsDefSource > 0 &&
                     insUnits > 0)
            {
                scaleFactor = UnitsConverter.GetConversionFactor((UnitsValue)insUnitsDefSource, (UnitsValue)insUnits);
            }

            if (blockRef.BlockUnit > 0 && insUnits > 0)
            {
                factor = UnitsConverter.GetConversionFactor(blockRef.BlockUnit, (UnitsValue)insUnits);
            }

            WriteMessage($"\n{string.Format(MInsertRes.CM_Unit_3, ConvertFromDoubleToExp(scaleFactor.ToString()))}");
            WriteMessage($"\n{string.Format(MInsertRes.CM_Unit_4, ConvertFromDoubleToExp(factor.ToString()))}");

            blockRef.ScaleFactors = new Scale3d(factor);
        }

        /// <summary>
        /// ブロック名取得
        /// </summary>
        private string GetBlockName(out string fullPath)
        {
            fullPath = string.Empty;
            SetListBlock();
            var defaultBlockName = SystemVariable.GetString("INSNAME");

            //BN1. ブロック名入力
            var optBlockname = new PromptStringOptions($"\n{MInsertRes.BN1}" + (string.IsNullOrWhiteSpace(defaultBlockName) ? string.Empty : $" <{defaultBlockName}>"))
            {
                AllowSpaces = true,
            };

            var resBlockname = Util.Editor().GetString(optBlockname);
            if (resBlockname.Status != PromptStatus.OK) return string.Empty;

            var blockName = resBlockname.StringResult.Trim();
            switch (blockName)
            {
                case "~":
                    var dlg = new OpenFileDialog(MInsertRes.Dialog_Select_Title, null, "dwg;dxf;dwt;dws", "SelectShapeFile", OpenFileDialog.OpenFileDialogFlags.DoNotTransferRemoteFiles);
                    if (dlg.ShowDialog() != DialogResult.OK) return null;
                    blockName = dlg.Filename;
                    return GetValidBlockName(blockName, ref fullPath);
                case "?":
                    ShowBlockList(_listBlockName, _listBlockAnonymous);
                    return string.Empty;
                case "":
                    if (string.IsNullOrEmpty(defaultBlockName))
                    {
                        WriteMessage($"\n{string.Format(MInsertRes.BN3)}");
                        return string.Empty;
                    }
                    return GetValidBlockName(defaultBlockName, ref fullPath);

                default:
                    return GetValidBlockName(blockName, ref fullPath);
            }
        }

        /// <summary>
        /// Validate Block Name and replacement character
        /// </summary>
        private string GetValidBlockName(string blockName, ref string fullPath)
        {
            //分解挿入
            if (blockName.StartsWith("*"))
            {
                WriteMessage($"\n{MInsertRes.BN2}");
                return string.Empty;
            }

            //通常挿入
            if (blockName.Contains("="))
            {
                fullPath = blockName.Substring(blockName.Split('=')[0].Length + 1);
                blockName = blockName.Split('=')[0];

                return AnalysisReplacement(blockName, ref fullPath);
            }
            return AnalysisBlockName(blockName, ref fullPath);
        }

        /// <summary>
        /// ブロック名解析
        /// </summary>
        private string AnalysisBlockName(string blockName, ref string fullPath)
        {
            if (IsContainsInvalidCharacter(ref blockName))
            {
                WriteMessage($"\n{string.Format(MInsertRes.BN3)}");
                return string.Empty;
            }

            //ブロック名DB検索
            if (Path.GetExtension(blockName).ToUpper() == ".DWG")
            {
                blockName = Path.GetFileName(blockName);
            }
            if (_listBlockName.Any(x => x.ToLower().Equals(blockName.ToLower())))
            {
                return blockName;
            }

            //ブロック名サポートパス検索
            fullPath = blockName;
            return AnalysisReplacement(string.Empty, ref fullPath, false);
        }

        /// <summary>
        /// ブロック置換構文解析
        /// </summary>
        private string AnalysisReplacement(string blockName, ref string fullPath, bool isFixedBlockName = true)
        {
            if (IsContainsInvalidCharacter(ref blockName))
            {
                WriteMessage($"\n{string.Format(MInsertRes.BN3)}");
                return string.Empty;
            }

            var lstExtFile = new List<string>() { ".dwg", ".dwt", ".dws", ".dxf" };

            if (!lstExtFile.Contains(Path.GetExtension(fullPath).ToLower()))
            {
                fullPath += ".dwg";
            }

            if (File.Exists(fullPath))
            {
                if (isFixedBlockName) return blockName;
                return Path.GetFileNameWithoutExtension(fullPath);
            }

            var fileName = Path.GetFileName(fullPath);
            if (!string.IsNullOrEmpty(fileName) && fileName.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
            {
                WriteMessage($"\n{string.Format(MInsertRes.BN5)}");
                return string.Empty;
            }

            var curDirectory = Path.GetDirectoryName(Util.Database().OriginalFileName);
            fullPath = Path.Combine(curDirectory, fileName);
            if (File.Exists(fullPath))
            {
                if (isFixedBlockName) return blockName;
                return Path.GetFileNameWithoutExtension(fullPath);
            }

            var supportPaths = SystemVariable.GetString("ACADPREFIX").Split(';');
            foreach (var supportPath in supportPaths)
            {
                fullPath = Path.Combine(supportPath, fileName);
                if (File.Exists(fullPath))
                {
                    if (isFixedBlockName) return blockName;
                    return Path.GetFileNameWithoutExtension(fullPath);
                }
            }

            var msgInfo = string.Format(MInsertRes.BN4, fileName, curDirectory);
            foreach (var supportPath in supportPaths)
            {
                msgInfo += $"{supportPath}\n";
            }

            MsgBox.Alert.Show(msgInfo);
            fullPath = string.Empty;
            return string.Empty;
        }

        /// <summary>
        /// ブロック名無効メッセージ
        /// </summary>
        private bool IsContainsInvalidCharacter(ref string blockName)
        {
            var illegalCharater = @"*:<>?|";
            if (new Regex("[" + Regex.Escape(illegalCharater) + "]").IsMatch(blockName)) return !Path.IsPathRooted(blockName);
            if (blockName.StartsWith("\"") != blockName.EndsWith("\"")) return true;
            if (blockName.StartsWith("\""))
            {
                blockName = blockName.Remove(blockName.Length - 1, 1).Remove(0, 1);
            }
            if (blockName.Contains("/"))
            {
                blockName = blockName.Substring(blockName.LastIndexOf('/') + 1);
                return IsContainsInvalidCharacter(ref blockName);
            }
            if (blockName.Contains(@"\"))
            {
                blockName = blockName.Substring(blockName.LastIndexOf('\\') + 1);
                return IsContainsInvalidCharacter(ref blockName);
            }
            return false;
        }

        /// <summary>
        /// Show List Block
        /// </summary>
        private void ShowBlockList(List<string> listBlock, List<string> listBlockAnonymous)
        {
            var optBlockList = new PromptStringOptions($"\n{MInsertRes.UI_BlockList}")
            {
                DefaultValue = "*",
                AllowSpaces = true,
                UseDefaultValue = true,
                AppendKeywordsToMessage = false,
            };

            var restBlockList = Util.Editor().GetString(optBlockList);
            if (restBlockList.Status != PromptStatus.OK) return;

            var keyWords = restBlockList.StringResult.Trim();

            switch (keyWords)
            {
                case "*":
                case "":
                    ShowCMList(listBlock, listBlockAnonymous);
                    return;
                default:
                    var searchListBlock = listBlock.Where(x => IsMatchingKeyWord(x, keyWords)).ToList();
                    ShowCMList(searchListBlock, listBlockAnonymous);
                    return;
            }
        }

        /// <summary>
        /// Filter with keyword
        /// </summary>
        private bool IsMatchingKeyWord(string value, string keyWord)
        {
            value = value.ToLower();
            keyWord = Regex.Escape(keyWord.ToLower());
            keyWord = keyWord.Replace("\\*", ".*").Insert(0, "^");
            var regex = new Regex(keyWord);
            return regex.IsMatch(value, 0);
        }

        /// <summary>
        /// Show write message info list block
        /// </summary>
        private void ShowCMList(List<string> listBlock, List<string> listBlockAnonymous)
        {
            WriteMessage($"\n{string.Format(MInsertRes.CM_List_1)}");
            foreach (var block in listBlock)
            {
                WriteMessage($"\n\t\"{block}\"");
            }
            WriteMessage($"\n{string.Format(MInsertRes.CM_List_2)}");
            WriteMessage($"\n{string.Format(MInsertRes.CM_List_3)}");
            if (listBlock.Count == listBlockAnonymous.Count)
                WriteMessage($"\n\t\t{listBlock.Count}\t\t\t{listBlockAnonymous.Count}");
            else
                WriteMessage($"\n\t\t{listBlock.Count}\t\t\t{0}");
        }

        /// <summary>
        /// Find all block in database
        /// </summary>
        private void SetListBlock()
        {
            _listBlockName = new List<string>();
            _listBlockAnonymous = new List<string>();

            using (var tr = Util.StartTransaction())
            {
                var blockTable = tr.GetObject<BlockTable>(Util.CurDoc().Database.BlockTableId, OpenMode.ForRead);

                foreach (var blockDefId in blockTable)
                {
                    var blockRefTable = tr.GetObject<BlockTableRecord>(blockDefId, OpenMode.ForRead);

                    if (blockRefTable == null ||
                        blockRefTable.IsFromExternalReference ||
                        blockRefTable.IsLayout ||
                        blockRefTable.IsAnonymous ||
                        blockRefTable.Name.Contains("|"))
                    {
                        _listBlockAnonymous.Add(blockRefTable.Name);

                    }
                    else
                    {
                        _listBlockName.Add(blockRefTable.Name);
                    }
                }
                _listBlockName.Sort();
            }
        }

        /// <summary>
        /// Change double to E+ form
        /// </summary>
        private string ConvertFromDoubleToExp(string input)
        {
            var intPart = string.Empty;
            var expPart = string.Empty;
            if (double.Parse(input) >= Math.Pow(10, 10) && double.Parse(input) <= Math.Pow(10, 15) && !input.Contains("E"))
            {
                var exp = Math.Round(double.Parse(input)).ToString().Length - 1;
                intPart = (double.Parse(input) / Math.Pow(10, exp)).ToString();
                expPart = "E+" + exp.ToString();
                var tempIntPart = double.Parse(intPart);
                intPart = Converter.DistanceToString(tempIntPart, DistanceUnitFormat.Decimal, Util.Luprec());
                input = intPart + expPart;
            }
            else
            {

                for (var i = 0; i < input.Length; i++)
                {
                    if (input[i] == 'E')
                    {
                        intPart = input.Substring(0, i);
                        expPart = input.Substring(i);
                        break;
                    }
                }
                var tempIntPart = 0.0;
                if (!string.IsNullOrEmpty(intPart))
                    tempIntPart = double.Parse(intPart);
                else
                    tempIntPart = double.Parse(input);
                input = Converter.DistanceToString(tempIntPart, DistanceUnitFormat.Decimal, Util.Luprec());
                input += expPart;
            }
            return input;
        }
        #endregion

        #region Handles Block Reference
        /// <summary>
        /// Get block reference from block name or path
        /// </summary>
        private BlockReference GetBlockReference(string blockName, string fullPath, out ObjectId blkTableRecId)
        {
            ////ブロック定義登録
            if (!string.IsNullOrWhiteSpace(fullPath)) return RegisterBlockReference(blockName, fullPath, out blkTableRecId);

            using (var tr = Util.StartTransaction())
            {
                var blockTable = tr.GetObject<BlockTable>(Util.Database().BlockTableId, OpenMode.ForRead);
                var blockTableRec = tr.GetObject<BlockTableRecord>(blockTable[blockName], OpenMode.ForRead);
                blkTableRecId = blockTableRec.Id;
                _uniformScaleActive = blockTableRec.BlockScaling == BlockScaling.Uniform;

                return new BlockReference(Point3d.Origin, blkTableRecId);
            }
        }

        /// <summary>
        /// ブロック定義登録
        /// </summary>
        private BlockReference RegisterBlockReference(string blockName, string fullPath, out ObjectId blkTableRecId)
        {
            blkTableRecId = ObjectId.Null;
            var blockDefIsAnnotative = false;

            using (var sourceDb = new Database(false, false))
            {
                if (Path.GetExtension(fullPath).ToUpper() == ".DXF")
                {
                    var logPath = Path.GetTempFileName();
                    sourceDb.DxfIn(fullPath, logPath);
                    System.IO.File.Delete(logPath);
                }
                else
                {
                    sourceDb.ReadDwgFile(fullPath, FileOpenMode.OpenForReadAndAllShare, false, string.Empty);
                }

                blkTableRecId = Util.Database().Insert(blockName, sourceDb, false);
                blockDefIsAnnotative = sourceDb.AnnotativeDwg;
            }

            //redefine
            using (var tr = Util.StartTransaction())
            {
                var blockTableRec = tr.GetObject<BlockTableRecord>(blkTableRecId, OpenMode.ForWrite);

                if (blockTableRec != null)
                {
                    var contextCollection = Util.Database().ObjectContextManager.GetContextCollection("ACDB_ANNOTATIONSCALES");

                    // Update existing block references
                    foreach (ObjectId objID in blockTableRec.GetBlockReferenceIds(false, true))
                    {
                        var acBlkRef = tr.GetObject<BlockReference>(objID, OpenMode.ForWrite);
                        if (blockDefIsAnnotative)
                        {
                            blockTableRec.Annotative = AnnotativeStates.False;
#if _AutoCAD_
                            ObjectContexts.AddContext(acBlkRef, contextCollection.CurrentContext);
#elif _IJCAD_
                            //TODO: IJCAD does not suppot ObjectContexts
#endif
                        }
                        else
                        {
                            blockTableRec.Annotative = AnnotativeStates.False;
                            foreach (var ctx in contextCollection)
                            {
#if _AutoCAD_
                                if (ObjectContexts.HasContext(acBlkRef, ctx))
                                {
                                    ObjectContexts.RemoveContext(acBlkRef, ctx);
                                }
#elif _IJCAD_
                                //TODO: IJCAD does not suppot ObjectContexts
#endif
                            }
                        }

                        acBlkRef.RecordGraphicsModified(true);
                    }

                    Util.Editor().Regen();
                    tr.Commit();
                }
            }

            return new BlockReference(Point3d.Origin, blkTableRecId);
        }

        /// <summary>
        /// Add block reference into Database
        /// </summary>
        private void AddEntityIntoDatabase(Entity entity)
        {
            using (var tr = Util.StartTransaction())
            {
                tr.AddNewlyCreatedDBObject(entity, true, tr.CurrentSpace());
                tr.Commit();
            }
        }
        #endregion

        #region Handles Input Array
        /// <summary>
        /// Input rectangle for insert
        /// </summary>
        private PromptStatus InputArray(ref short rows, ref short columns, ref double colSpacing, ref double rowSpacing)
        {
            var rowOptions = new PromptIntegerOptions($"{MInsertRes.P1}")
            {
                DefaultValue = 1,
                UseDefaultValue = true,
                UpperLimit = 32767,
                LowerLimit = 1
            };
            var rowRes = Util.Editor().GetInteger(rowOptions);
            if (rowRes.Status != PromptStatus.OK) return PromptStatus.Cancel;
            rows = (short)rowRes.Value;

            var colOptions = new PromptIntegerOptions($"{MInsertRes.P2}")
            {
                DefaultValue = 1,
                UseDefaultValue = true,
                UpperLimit = 32767,
                LowerLimit = 1
            };
            var colRes = Util.Editor().GetInteger(colOptions);
            if (colRes.Status != PromptStatus.OK) return PromptStatus.Cancel;
            columns = (short)colRes.Value;

            if (rows * columns == 1) return PromptStatus.OK;
            if (rows == 1) return InputColSpacing(ref colSpacing);
            return InputRowSpacing(columns > 1, ref rowSpacing, ref colSpacing);
        }

        /// <summary>
        /// Handle Input Row Spacing
        /// </summary>
        private PromptStatus InputRowSpacing(bool isMutilColumns, ref double rowSpacing, ref double colSpacing)
        {
            var pointOptions = new PromptPointOptions($"\n{MInsertRes.P3}")
            {
                AllowArbitraryInput = true,
            };

            while (true)
            {
                var startPointRes = _userInput.GetPoint(pointOptions);
                switch (startPointRes.Status)
                {
                    //acquire a point
                    case PromptStatus.OK:
                        var startPoint = startPointRes.Value;
                        var cornerOptions = new PromptCornerOptions($"\n{MInsertRes.UI_R_Corner}", startPoint)
                        {
                            AllowArbitraryInput = true
                        };
                        var cornerRes = _userInput.GetCorner(cornerOptions);
                        switch (cornerRes.Status)
                        {
                            case PromptStatus.Cancel:
                                return PromptStatus.Cancel;
                            case PromptStatus.OK:
                                var endPoint = cornerRes.Value;
                                var distance = startPoint.DistanceTo(endPoint);
                                var angle = Vector3d.XAxis.GetAngleTo(endPoint - startPoint, Vector3d.ZAxis);
                                rowSpacing = Math.Sin(angle) * distance;
                                colSpacing = isMutilColumns ? Math.Cos(angle) * distance : 0;

                                return PromptStatus.OK;
                            default:
                                continue;
                        }
                    case PromptStatus.Keyword:
                        if (double.TryParse(startPointRes.StringResult, out rowSpacing)) return InputColSpacing(ref colSpacing);

                        WriteMessage($"\n{MInsertRes.ER3}");
                        break;
                    default:
                        return PromptStatus.Cancel;
                }
            }
        }

        /// <summary>
        /// Handle Input column Spacing
        /// </summary>
        private PromptStatus InputColSpacing(ref double colSpacing)
        {
            var colSpacingOptions = new PromptDistanceOptions($"{MInsertRes.P4}");
            var colSpacingRes = _userInput.GetDistance(colSpacingOptions);
            if (colSpacingRes.Status != PromptStatus.OK) return PromptStatus.Cancel;
            colSpacing = colSpacingRes.Value;

            return PromptStatus.OK;
        }
        #endregion
    }
}
