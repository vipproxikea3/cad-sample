## Modules

- 設定されていなければ、AutoCAD 2022, IJCAD 2021 をダウンロードしてください。
- ユーザ環境を設定 (設定後に再起動が必要)

    | Name | Data |
    | ------ | ------ |
    | AcadDir | "C:\Program Files\Autodesk\AutoCAD 2022" |
    | IJcadDir | "C:\Program Files\ITJP\IJCAD 2021 PRO" |
    

- スタートアッププロジェクトを設定
   - AUTOCAD用のACAD_DEBUGを選択し、IJCAD用のIJCAD_DEBUGを選択
   - プロジェクト開始　→　ACAD, IJCADに自動的にロードする